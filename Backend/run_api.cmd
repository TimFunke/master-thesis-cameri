@echo off

set dataset_folder=%cd%\datasets\
call activate base

start /d CAMeRI_API cmd.exe /c uvicorn main:app --reload