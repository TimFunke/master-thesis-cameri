/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

use cameri;

-- Exportiere Daten aus Tabelle cameri.dataset: ~0 rows (ungefähr)
DELETE FROM `dataset`;
/*!40000 ALTER TABLE `dataset` DISABLE KEYS */;
INSERT INTO `dataset` (`DatasetID`, `Name`, `DataPath`, `MetaInfo`) VALUES
	(1, 'Titanic Dataset', '${dataset_folder}/titanic.csv', '{\r\n    "file_type": "csv",\r\n    "csv_delimiter": ";",\r\n    "columns": [\r\n        {\r\n            "index": 0,\r\n            "name": "survival",\r\n            "description": "if the person survived or not"\r\n        },\r\n        {\r\n            "index": 1,\r\n            "name": "pclass",\r\n            "description": "passenger class of the person"\r\n        },\r\n        {\r\n            "index": 2,\r\n            "name": "sex",\r\n            "description": "sex of the person"\r\n        },\r\n        {\r\n            "index": 3,\r\n            "name": "age",\r\n            "description": "age of the person"\r\n        },\r\n        {\r\n            "index": 4,\r\n            "name": "sibsp",\r\n            "description": "amount of siblings/spouses aboard"\r\n        },\r\n        {\r\n            "index": 5,\r\n            "name": "parch",\r\n            "description": "amount of parents/children aboard"\r\n        },\r\n        {\r\n            "index": 6,\r\n            "name": "ticket",\r\n            "description": "ticket number"\r\n        },\r\n        {\r\n            "index": 7,\r\n            "name": "fare",\r\n            "description": "passenger fare"\r\n        },\r\n        {\r\n            "index": 8,\r\n            "name": "cabin",\r\n            "description": "cabin number"\r\n        },\r\n        {\r\n            "index": 9,\r\n            "name": "embarked",\r\n            "description": "port of embarkation"\r\n        }\r\n    ]\r\n}');
/*!40000 ALTER TABLE `dataset` ENABLE KEYS */;

-- Exportiere Daten aus Tabelle cameri.data_annotation: ~0 rows (ungefähr)
DELETE FROM `data_annotation`;
/*!40000 ALTER TABLE `data_annotation` DISABLE KEYS */;
INSERT INTO `data_annotation` (`DatasetID`, `ColumnIndex`, `ColumnName`, `ScaleType`, `IsTarget`, `IsIgnore`) VALUES
	(1, 0, 'survival', 'CATEGORICAL', 1, 0),
	(1, 1, 'pclass', 'ORDINAL', 0, 0),
	(1, 2, 'sex', 'CATEGORICAL', 0, 0),
	(1, 3, 'age', 'NUMERIC', 0, 0),
	(1, 4, 'sibsp', 'NUMERIC', 0, 0),
	(1, 5, 'parch', 'NUMERIC', 0, 0),
	(1, 6, 'ticket', 'CATEGORICAL', 0, 0),
	(1, 7, 'fare', 'NUMERIC', 0, 0),
	(1, 8, 'cabin', 'CATEGORICAL', 0, 0),
	(1, 9, 'embarked', 'CATEGORICAL', 0, 0);
/*!40000 ALTER TABLE `data_annotation` ENABLE KEYS */;

-- Exportiere Daten aus Tabelle cameri.feature: ~0 rows (ungefähr)
DELETE FROM `feature`;
/*!40000 ALTER TABLE `feature` DISABLE KEYS */;
INSERT INTO `feature` (`FeatureID`, `QueryID`, `ColumnIndex`, `FeatureType`, `FeatureValue`) VALUES
	(88, 5, 0, 'MISSING_VALUES', 0),
	(89, 5, 0, 'STANDARDIZED', 0),
	(90, 5, 0, 'NORMALIZED', 1),
	(91, 5, 1, 'MISSING_VALUES', 0),
	(92, 5, 1, 'STANDARDIZED', 0),
	(93, 5, 1, 'NORMALIZED', 1),
	(94, 5, 2, 'MISSING_VALUES', 0),
	(95, 5, 2, 'STANDARDIZED', 0),
	(96, 5, 2, 'NORMALIZED', 0),
	(97, 5, 3, 'MISSING_VALUES', 1),
	(98, 5, 3, 'STANDARDIZED', 0),
	(99, 5, 3, 'NORMALIZED', 1),
	(100, 5, 4, 'MISSING_VALUES', 0),
	(101, 5, 4, 'STANDARDIZED', 0),
	(102, 5, 4, 'NORMALIZED', 1),
	(103, 5, 5, 'MISSING_VALUES', 0),
	(104, 5, 5, 'STANDARDIZED', 0),
	(105, 5, 5, 'NORMALIZED', 1),
	(106, 5, 6, 'MISSING_VALUES', 0),
	(107, 5, 6, 'STANDARDIZED', 0),
	(108, 5, 6, 'NORMALIZED', 0),
	(109, 5, 7, 'MISSING_VALUES', 0),
	(110, 5, 7, 'STANDARDIZED', 0),
	(111, 5, 7, 'NORMALIZED', 1),
	(112, 5, 8, 'MISSING_VALUES', 1),
	(113, 5, 8, 'STANDARDIZED', 0),
	(114, 5, 8, 'NORMALIZED', 0),
	(115, 5, 9, 'MISSING_VALUES', 1),
	(116, 5, 9, 'STANDARDIZED', 0),
	(117, 5, 9, 'NORMALIZED', 0);
/*!40000 ALTER TABLE `feature` ENABLE KEYS */;

-- Exportiere Daten aus Tabelle cameri.knowledge: ~0 rows (ungefähr)
DELETE FROM `knowledge`;
/*!40000 ALTER TABLE `knowledge` DISABLE KEYS */;
INSERT INTO `knowledge` (`KnowledgeID`, `KnowledgeName`, `KnowledgeGroup`, `Expression`) VALUES
	(1, 'K Means', 'Analysis Methods', 'AM_KM <= PC_NO_MISSING_VALUES, PC_STANDARDIZED_ATTR, PC_NUMERIC_ATTR.'),
	(2, 'Principal Component Analysis', 'Analysis Methods', 'AM_PCA <= PC_NO_MISSING_VALUES, PC_STANDARDIZED_ATTR, PC_NUMERIC_ATTR.'),
	(3, 'Linear Regression', 'Analysis Methods', 'AM_LR <= PC_SUPERVISED, PC_NO_MISSING_VALUES, PC_NUMERIC_ATTR.'),
	(4, 'K Nearest Neighbor', 'Analysis Methods', 'AM_KNN <= PC_SUPERVISED, PC_NO_MISSING_VALUES, PC_NUMERIC_ATTR, PC_CATEGORICAL_TARGET_ATTR, PC_NORMALIZED_SOURCE_ATTR.'),
	(5, 'Support Vector Machine', 'Analysis Methods', 'AM_SVM <= PC_SUPERVISED, PC_NO_MISSING_VALUES, PC_CATEGORICAL_TARGET_ATTR, PC_NORMALIZED_SOURCE_ATTR, PC_NUMERIC_SOURCE_ATTR.'),
	(6, 'Random Forest', 'Analysis Methods', 'AM_RF <= PC_SUPERVISED, PC_CATEGORICAL_TARGET_ATTR.'),
	(7, 'Supervised 1', 'Preconditions', 'PC_SUPERVISED <= DA_TARGET_ATTR(<single_column>).'),
	(8, 'Supervised 2', 'Preconditions', 'PC_SUPERVISED <= DA_NOT_TARGET_ATTR(<all_columns>), IMPOSSIBLE(<single_column>).'),
	(9, 'Standardized Attributes 1', 'Preconditions', 'PC_STANDARDIZED_ATTR <= C_STANDARDIZED_ATTR(<all_columns>).'),
	(10, 'Standardized Attributes 2', 'Preconditions', 'C_STANDARDIZED_ATTR(<single_column>) <= F_STANDARDIZED(<single_column>).'),
	(11, 'No Missing Values 1', 'Preconditions', 'PC_NO_MISSING_VALUES <= C_NO_MISSING_VALUES(<all_columns>).'),
	(12, 'No Missing Values 2', 'Preconditions', 'C_NO_MISSING_VALUES(<single_column>) <= F_NOT_MISSING_VALUES(<single_column>).'),
	(13, 'Numeric Attributes 1', 'Preconditions', 'PC_NUMERIC_ATTR <= C_NUMERIC_ATTR(<all_columns>).'),
	(14, 'Numeric Attributes 2', 'Preconditions', 'C_NUMERIC_ATTR(<single_column>) <= DA_TYPE_NUMERIC(<single_column>).'),
	(15, 'Normalized Source Attributes 1', 'Preconditions', 'PC_NORMALIZED_SOURCE_ATTR <= C_NORMALIZED_SOURCE_ATTR(<all_columns>).'),
	(16, 'Normalized Source Attributes 2', 'Preconditions', 'C_NORMALIZED_SOURCE_ATTR(<single_column>) <= DA_TARGET_ATTR(<single_column>).'),
	(17, 'Normalized Source Attributes 3', 'Preconditions', 'C_NORMALIZED_SOURCE_ATTR(<single_column>) <= DA_NOT_TARGET_ATTR(<single_column>), F_NORMALIZED(<single_column>).'),
	(18, 'Numeric Source Attributes 1', 'Preconditions', 'PC_NUMERIC_SOURCE_ATTR <= C_NUMERIC_SOURCE_ATTR(<all_columns>).'),
	(19, 'Numeric Source Attributes 2', 'Preconditions', 'C_NUMERIC_SOURCE_ATTR(<single_column>) <= DA_TARGET_ATTR(<single_column>).'),
	(20, 'Numeric Source Attributes 3', 'Preconditions', 'C_NUMERIC_SOURCE_ATTR(<single_column>) <= DA_NOT_TARGET_ATTR(<single_column>), DA_TYPE_NUMERIC(<single_column>).'),
	(21, 'Categorical Target Attribute 1', 'Preconditions', 'PC_CATEGORICAL_TARGET_ATTR <= C_CATEGORICAL_TARGET_ATTR(<all_columns>).'),
	(22, 'Categorical Target Attribute 2', 'Preconditions', 'C_CATEGORICAL_TARGET_ATTR(<single_column>) <= DA_NOT_TARGET_ATTR(<single_column>).'),
	(23, 'Categorical Target Attribute 3', 'Preconditions', 'C_CATEGORICAL_TARGET_ATTR(<single_column>) <= DA_TARGET_ATTR(<single_column>), DA_TYPE_CATEGORICAL(<single_column>).'),
	(24, 'Fix Standardized Attributes', 'Preprocessings', 'C_STANDARDIZED_ATTR(<single_column>) <= F_NOT_STANDARDIZED(<single_column>), PP_STANDARDIZE(<single_column>).'),
	(25, 'Fix No Missing Values', 'Preprocessings', 'C_NO_MISSING_VALUES(<single_column>) <= F_MISSING_VALUES(<single_column>), PP_MISS_VAL_HANDLING(<single_column>).'),
	(26, 'Fix Numeric Attributes', 'Preprocessings', 'C_NUMERIC_ATTR(<single_column>) <= DA_NOT_TYPE_NUMERIC(<single_column>), PP_IGNORE(<single_column>).'),
	(27, 'Fix Normalized Source Attributes', 'Preprocessings', 'C_NORMALIZED_SOURCE_ATTR(<single_column>) <= DA_NOT_TARGET_ATTR(<single_column>), F_NOT_NORMALIZED(<single_column>), PP_NORMALIZE(<single_column>).'),
	(28, 'Fix Numeric Source Attributes', 'Preprocessings', 'C_NUMERIC_SOURCE_ATTR(<single_column>) <= DA_NOT_TARGET_ATTR(<single_column>), DA_NOT_TYPE_NUMERIC(<single_column>), PP_IGNORE(<single_column>).'),
	(29, 'Fix Categorical Target Attribute', 'Preprocessings', 'C_CATEGORICAL_TARGET_ATTR(<single_column>) <= DA_TARGET_ATTR(<single_column>), DA_NOT_TYPE_CATEGORICAL(<single_column>), IMPOSSIBLE(<single_column>).');
/*!40000 ALTER TABLE `knowledge` ENABLE KEYS */;

-- Exportiere Daten aus Tabelle cameri.mediation: ~0 rows (ungefähr)
DELETE FROM `mediation`;
/*!40000 ALTER TABLE `mediation` DISABLE KEYS */;
INSERT INTO `mediation` (`MediationID`, `EntityName`, `EntityType`, `KnowledgeTerm`, `EntityDescription`) VALUES
	(1, 'K Means', 'ANALYSIS_METHOD', 'AM_KM', 'some description for k means'),
	(2, 'PCA', 'ANALYSIS_METHOD', 'AM_PCA', 'some description for pca'),
	(3, 'Linear Regression', 'ANALYSIS_METHOD', 'AM_LR', 'some description for lr'),
	(4, 'K Nearest Neighbor', 'ANALYSIS_METHOD', 'AM_KNN', 'some description for knn'),
	(5, 'SVM', 'ANALYSIS_METHOD', 'AM_SVM', 'some description for svm'),
	(6, 'Random Forest', 'ANALYSIS_METHOD', 'AM_RF', 'some description for rf'),
	(7, 'Missing Value Handling', 'PREPROCESSING', 'PP_MISS_VAL_HANDLING', 'Description for MVH'),
	(8, 'Standardize', 'PREPROCESSING', 'PP_STANDARDIZE', 'Description for Standardization'),
	(9, 'Normalize', 'PREPROCESSING', 'PP_NORMALIZE', 'Description for Normalization'),
	(10, 'Ignore Attribute', 'PREPROCESSING', 'PP_IGNORE', 'Description for Ignore Column'),
	(11, 'No Missing Values', 'PRECONDITION', 'PC_NO_MISSING_VALUES', 'Description for No Missing Values'),
	(12, 'Standardized Attributes', 'PRECONDITION', 'PC_STANDARDIZED_ATTR', 'Description for Standardized Attributes'),
	(13, 'Numeric Attributes', 'PRECONDITION', 'PC_NUMERIC_ATTR', 'Description for Numeric Attributes'),
	(14, 'Supervised', 'PRECONDITION', 'PC_SUPERVISED', 'Description for Supervised'),
	(15, 'Categorical Target Attribute', 'PRECONDITION', 'PC_CATEGORICAL_TARGET_ATTR', 'Description for Categorical Target Attribute'),
	(16, 'Normalized Source Attributes', 'PRECONDITION', 'PC_NORMALIZED_SOURCE_ATTR', 'Description for Normalized Source Attributes'),
	(17, 'Numeric Source Attributes', 'PRECONDITION', 'PC_NUMERIC_SOURCE_ATTR', 'Description for Numeric Source Attributes');
/*!40000 ALTER TABLE `mediation` ENABLE KEYS */;

-- Exportiere Daten aus Tabelle cameri.query: ~0 rows (ungefähr)
DELETE FROM `query`;
/*!40000 ALTER TABLE `query` DISABLE KEYS */;
INSERT INTO `query` (`QueryID`, `DatasetID`, `Name`, `Status`, `CeleryTask`) VALUES
	(5, 1, 'Titanic Analysis', 'FINISHED', NULL);
/*!40000 ALTER TABLE `query` ENABLE KEYS */;

-- Exportiere Daten aus Tabelle cameri.query_knowledge: ~0 rows (ungefähr)
DELETE FROM `query_knowledge`;
/*!40000 ALTER TABLE `query_knowledge` DISABLE KEYS */;
INSERT INTO `query_knowledge` (`QueryID`, `KnowledgeID`) VALUES
	(5, 1),
	(5, 2),
	(5, 3),
	(5, 4),
	(5, 5),
	(5, 6),
	(5, 7),
	(5, 8),
	(5, 9),
	(5, 10),
	(5, 11),
	(5, 12),
	(5, 13),
	(5, 14),
	(5, 15),
	(5, 16),
	(5, 17),
	(5, 18),
	(5, 19),
	(5, 20),
	(5, 21),
	(5, 22),
	(5, 23),
	(5, 24),
	(5, 25),
	(5, 26),
	(5, 27),
	(5, 28),
	(5, 29);
/*!40000 ALTER TABLE `query_knowledge` ENABLE KEYS */;

-- Exportiere Daten aus Tabelle cameri.query_settings: ~0 rows (ungefähr)
DELETE FROM `query_settings`;
/*!40000 ALTER TABLE `query_settings` DISABLE KEYS */;
INSERT INTO `query_settings` (`QueryID`, `NormalizedMin`, `NormalizedMax`, `StandardizedThresholdMean`, `StandardizedThresholdStd`) VALUES
	(5, -1, 1, 0.1, 0.1);
/*!40000 ALTER TABLE `query_settings` ENABLE KEYS */;

-- Exportiere Daten aus Tabelle cameri.recommendation: ~0 rows (ungefähr)
DELETE FROM `recommendation`;
/*!40000 ALTER TABLE `recommendation` DISABLE KEYS */;
INSERT INTO `recommendation` (`RecommendationID`, `QueryID`, `MethodID`, `RecType`) VALUES
	(19, 5, 6, 'PRACTICABLE'),
	(20, 5, 1, 'PREPROCESSING_NEEDED'),
	(21, 5, 2, 'PREPROCESSING_NEEDED'),
	(22, 5, 3, 'PREPROCESSING_NEEDED'),
	(23, 5, 4, 'PREPROCESSING_NEEDED'),
	(24, 5, 5, 'PREPROCESSING_NEEDED');
/*!40000 ALTER TABLE `recommendation` ENABLE KEYS */;

-- Exportiere Daten aus Tabelle cameri.recommendation_precondition: ~0 rows (ungefähr)
DELETE FROM `recommendation_precondition`;
/*!40000 ALTER TABLE `recommendation_precondition` DISABLE KEYS */;
INSERT INTO `recommendation_precondition` (`RecommendationPreconditionID`, `RecommendationID`, `PreconditionID`, `HarmedByColumns`, `PreprocessingID`) VALUES
	(64, 19, 14, '[]', NULL),
	(65, 19, 15, '[]', NULL),
	(66, 20, 11, '[3, 8, 9]', 7),
	(67, 20, 12, '[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]', 8),
	(68, 20, 13, '[0, 1, 2, 6, 8, 9]', 10),
	(69, 21, 11, '[3, 8, 9]', 7),
	(70, 21, 12, '[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]', 8),
	(71, 21, 13, '[0, 1, 2, 6, 8, 9]', 10),
	(72, 22, 14, '[]', NULL),
	(73, 22, 11, '[3, 8, 9]', 7),
	(74, 22, 13, '[0, 1, 2, 6, 8, 9]', 10),
	(75, 23, 14, '[]', NULL),
	(76, 23, 11, '[3, 8, 9]', 7),
	(77, 23, 13, '[0, 1, 2, 6, 8, 9]', 10),
	(78, 23, 15, '[]', NULL),
	(79, 23, 16, '[2, 6, 8, 9]', 9),
	(80, 24, 14, '[]', NULL),
	(81, 24, 11, '[3, 8, 9]', 7),
	(82, 24, 15, '[]', NULL),
	(83, 24, 16, '[2, 6, 8, 9]', 9),
	(84, 24, 17, '[1, 2, 6, 8, 9]', 10);
/*!40000 ALTER TABLE `recommendation_precondition` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
