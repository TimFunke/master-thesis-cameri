# ---------------------------------------------------------------------------------------------------------
#   Handling of the extraction and format of the configuration (config file "config.cfg")
# ---------------------------------------------------------------------------------------------------------

from __future__ import annotations
import configparser
from dataclasses import dataclass, asdict


class Config:
    ''' Container class for all configuration information '''

    # Config for Database (MariaDB)
    Database: DatabaseConfig
    # Config for Redis (Message Broker)
    RedisBroker: RedisConfig

    @classmethod
    def read_config(cls, path: str):
        ''' Read the given config file, extract the content and fill the class variables '''
        config = configparser.ConfigParser()
        config.read(path)
        cls.Database = DatabaseConfig(
            Host=config["database"]["host"],
            Port=int(config["database"]["port"]),
            Username=config["database"]["user"],
            Password=config["database"]["password"],
            DatabaseName=config["database"]["db_name"],
        )
        cls.RedisBroker = RedisConfig(
            Host=config["redis-broker"]["host"],
            Port=int(config["redis-broker"]["port"]),
            DatabaseIndex=int(config["redis-broker"]["db_index"]),
        )


@dataclass(frozen=True)
class DatabaseConfig:
    ''' Dataclass for all database (MariaDB) configuration '''
    Host: str
    Port: int
    Username: str
    Password: str
    DatabaseName: str

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass(frozen=True)
class RedisConfig:
    ''' Dataclass for all redis (Message Broker) configuration '''
    Host: str
    Port: int
    DatabaseIndex: int

    def to_url(self) -> str:
        return f"redis://{self.Host}:{self.Port}/{self.DatabaseIndex}"
