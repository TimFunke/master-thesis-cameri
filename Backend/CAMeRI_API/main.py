# ---------------------------------------------------------------------------------------------------------
#   Main file for creating and configuring the API with fastapi
# ---------------------------------------------------------------------------------------------------------

import os
from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware

# Load Config from file
from config import Config
Config.read_config("config.cfg")

from routers import queries, data_annotations, recommender_system, knowledge
from database import DBConnection

# Setup the database
DBConnection.setup(
    host=Config.Database.Host,
    port=Config.Database.Port,
    user=Config.Database.Username,
    password=Config.Database.Password,
    db_name=Config.Database.DatabaseName,
)


# instantiate FastAPI object to hold the app
app = FastAPI()

# CORS (Cross-Origin Resource Sharing) configuration
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:8080"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Mount local folder "resources" to api route "/resources"
app.mount("/resources", StaticFiles(directory="resources"), name="resources")


@app.get("/styles")
async def get_styles():
    ''' Endpoint for a list of all css files in the resources '''
    files = os.listdir("resources/css")
    return [f"resources/css/{f}" for f in files if f.endswith(".css")]


@app.get("/scripts")
async def get_scripts():
    ''' Endpoint for a list of all javascript files in the resources '''
    files = os.listdir("resources/js")
    return [f"resources/js/{f}" for f in files if f.endswith(".js")]


# catch Exceptions in endpoint functions an wrap them in HTTP JSON responses
@app.exception_handler(Exception)
async def custom_exception_handler(request: Request, exc: Exception):
    return JSONResponse(
        status_code=400,
        content={"detail": str(exc)},
    )


# Include all subrouters
app.include_router(queries.router)
app.include_router(data_annotations.router)
app.include_router(recommender_system.router)
app.include_router(knowledge.router)
