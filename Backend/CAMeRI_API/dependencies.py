# ---------------------------------------------------------------------------------------------------------
#   Dependencies needed by the API routes to work (database connection, communication with celery service)
# ---------------------------------------------------------------------------------------------------------

from celery import Celery
from celery.result import AsyncResult

from database import DBConnection
from config import Config


class CeleryWrapper:
    ''' Wrapper for communication with the celery service '''

    # Define celery object with the given config for redis message broker
    __app = Celery('celery-app', broker=Config.RedisBroker.to_url())

    @__app.task(bind=True, name="complete-workflow")
    def __execute_query(self, query_id: int):
        ''' this function only links to the celery task with name "complete-workflow" --> empty function body '''
        pass

    @classmethod
    def execute_query(cls, query_id) -> str:
        ''' execute the query with the given id '''
        started_task = cls.__execute_query.delay(query_id)
        return started_task.task_id

    @classmethod
    def cancel_task(cls, task_id):
        ''' stop the execution of the query with the given id '''
        celery_task = AsyncResult(task_id)
        if celery_task.ready():
            # not running -> do nothing
            return
        celery_task.revoke(terminate=True)


# create singleton instance of CeleryWrapper
__celery_wrapper = CeleryWrapper()


def get_db():
    ''' provide database session and close it after use '''
    session = DBConnection.getSession()
    try:
        yield session
    finally:
        session.close()


def get_celery_wrapper():
    ''' provide CeleryWrapper instance '''
    yield __celery_wrapper
