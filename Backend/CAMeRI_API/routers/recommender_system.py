# ---------------------------------------------------------------------------------------------------------
#   Definition of API Endpoints for the communication with der recommender system (celery service)
# ---------------------------------------------------------------------------------------------------------

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException

from dependencies import get_db, get_celery_wrapper, CeleryWrapper
from database.util import get_data_annotation_status, get_query_by_id
from database.crud import receive_query_status
from database.models import QueryStatus
from database.enums import E_DATA_ANNOTATION_STATUS, E_QUERY_STATUS
from config import Config
from .util import catchDBExceptions

# instantiate APIRouter object to hold the routes (endpoints)
router = APIRouter(
    prefix="/recommender_system",
    tags=["recommender_system"],
)


@router.put("/execute_query/{query_id}")
@catchDBExceptions
def execute_query(query_id: int, db: Session = Depends(get_db), celery: CeleryWrapper = Depends(get_celery_wrapper)):
    '''
    Endpoint to start the execution of a query by id (param query_id)
    - find the query in the database
    - check the data annotation status
    - trigger celery execution
    - save celery task id
    '''
    query = get_query_by_id(db, query_id)
    annotation_status = get_data_annotation_status(db, query.DatasetID)
    if annotation_status != E_DATA_ANNOTATION_STATUS.COMPLETED:
        raise HTTPException(status_code=400, detail=f"Data Annotation for query with id {query_id} not completed!")
    task_id = celery.execute_query(query_id)
    query.CeleryTask = task_id
    db.commit()
    return {"message": "query started"}


@router.put("/cancel_execution/{query_id}")
@catchDBExceptions
def cancel_execution(query_id: int, db: Session = Depends(get_db), celery: CeleryWrapper = Depends(get_celery_wrapper)):
    '''
    Endpoint to stop the execution of a query by id (param query_id)
    - find the query in the database
    - check the query status
    - reset the query status in the db
    - try to stop the celery task
    '''
    query = get_query_by_id(db, query_id)
    if query.Status == E_QUERY_STATUS.CREATED:
        return {"message": "query not started yet"}
    if query.Status == E_QUERY_STATUS.FINISHED:
        return {"message": "query already finished"}
    query.Status = E_QUERY_STATUS.CREATED
    db.commit()
    try:
        if query.CeleryTask is not None:
            celery.cancel_task(query.CeleryTask)
    except:
        pass
    return {"message": "query cancelled"}


@router.get("/query_status/{query_id}", response_model=QueryStatus)
@catchDBExceptions
def get_query_status(query_id: int, db: Session = Depends(get_db)):
    ''' Endpoint to get the status of a query by id (param query_id) '''
    result = receive_query_status(db, query_id)
    return QueryStatus(**result)
