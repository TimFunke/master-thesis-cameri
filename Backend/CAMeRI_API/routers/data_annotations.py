# ---------------------------------------------------------------------------------------------------------
#   Definition of API Endpoints for data annotations (list datasets, list annotations, update, get data sample)
# ---------------------------------------------------------------------------------------------------------

from typing import List, Optional
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends
from os.path import expandvars
import pandas as pd
import json

from dependencies import get_db
from database.crud import receive_data_annotation, receive_data_annotations, update_data_annotation, get_dataset_by_id
from database.models import DataAnnotationUpdate, DataAnnotationSingle, DataAnnotationList
from .util import catchDBExceptions

# instantiate APIRouter object to hold the routes (endpoints)
router = APIRouter(
    prefix="/data_annotations",
    tags=["data_annotations"],
)


@router.get("/", response_model=List[DataAnnotationList])
@catchDBExceptions
def get_data_annotation_list(db: Session = Depends(get_db)):
    ''' Endpoint to get all exisiting datasets with their data annotation status '''
    results = receive_data_annotations(db)
    return [DataAnnotationList(**result) for result in results]


@router.get("/{dataset_id}", response_model=DataAnnotationSingle)
@catchDBExceptions
def get_single_data_annotation(dataset_id: int, db: Session = Depends(get_db)):
    ''' Endpoint to get the all data annotation for all columns of a given dataset (param dataset_id) '''
    result = receive_data_annotation(db, dataset_id)
    return DataAnnotationSingle(**result)


@router.patch("/{dataset_id}")
@catchDBExceptions
def update_single_data_annotation(dataset_id: int, data_annotation_data: List[DataAnnotationUpdate], db: Session = Depends(get_db)):
    ''' 
    Endpoint to override the data annotation of columns for a given dataset (param dataset_id) with the given data (param data_annotation_data) 
    - extract update data
    - stop of nothing to update
    - run update
    '''
    update_data = []
    for entry in data_annotation_data:
        to_update = entry.dict(exclude_unset=True)
        if len(to_update.keys()) > 1:
            update_data.append(to_update)
    if len(update_data) == 0:
        return "nothing to update"
    update_data_annotation(db, dataset_id, update_data)
    return {"message": "success"}


@router.get("/{dataset_id}/peek")
@catchDBExceptions
def peek_dataset(dataset_id: int, db: Session = Depends(get_db), rows: Optional[int] = 10):
    ''' 
    Endpoint to get sample entries of a given dataset (param dataset_id) 
    - find dataset entry in the database
    - check dataset file type
    - get dataset path
    - read first n columns (amount is set by param rows)
    - format result as json and return 
    '''
    dataset = get_dataset_by_id(db, dataset_id)
    if dataset.MetaInfo["file_type"] != "csv":
        raise Exception("only csv datasets supported yet!")
    path = expandvars(dataset.DataPath)
    df: pd.DataFrame = pd.read_csv(path, sep=dataset.MetaInfo["csv_delimiter"], nrows=rows)
    data_dict = json.loads(df.to_json(orient="split"))
    del data_dict["index"]
    data_dict["datasetName"] = dataset.Name
    return data_dict
