# ---------------------------------------------------------------------------------------------------------
#   Definition of API Endpoints for queries (get list, get single, get results, update, create, delete)
# ---------------------------------------------------------------------------------------------------------

from typing import List
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException, Response

from dependencies import get_db
from database.crud import create_query, receive_queries, receive_query, receive_query_results, update_query, delete_query
from database.models import QueryCreate, QueryUpdate, QuerySingle, QueryList, QueryResults
from .util import catchDBExceptions

# instantiate APIRouter object to hold the routes (endpoints)
router = APIRouter(
    prefix="/queries",
    tags=["queries"],
)


@router.get("/", response_model=List[QueryList])
@catchDBExceptions
def get_query_list(db: Session = Depends(get_db)):
    ''' Endpoint to get all exisiting queries '''
    results = receive_queries(db)
    return [QueryList(**result) for result in results]


@router.get("/{query_id}", response_model=QuerySingle)
@catchDBExceptions
def get_single_query(query_id: int, db: Session = Depends(get_db)):
    ''' Endpoint to get the attributes of a single query (param query_id) '''
    result = receive_query(db, query_id)
    return QuerySingle(**result)


@router.get("/{query_id}/results", response_model=QueryResults)
@catchDBExceptions
def get_query_results(query_id: int, db: Session = Depends(get_db)):
    ''' Endpoint to get the results of a single query (param query_id) '''
    results = receive_query_results(db, query_id)
    return QueryResults(**results)


@router.patch("/{query_id}")
@catchDBExceptions
def update_single_query(query_id: int, query_data: QueryUpdate, db: Session = Depends(get_db)):
    ''' Endpoint to override attributes of a given query (param query_id) with the given data (param query_data) '''
    update_data = query_data.dict(exclude_unset=True)
    if len(update_data.keys()) == 0:
        return "nothing to update"
    update_query(db, query_id, update_data)
    return {"message": "success"}


@router.post("/")
@catchDBExceptions
def create_new_query(query_data: QueryCreate, db: Session = Depends(get_db)):
    ''' Endpoint to create a new query with the given data (param query_data) '''
    query_id = create_query(db, query_data.dict())
    return {"message": "success", "QueryID": query_id}


@router.delete("/{query_id}")
@catchDBExceptions
def delete_single_query(query_id: int, db: Session = Depends(get_db)):
    ''' Endpoint to delete a single query by id (param query_id) '''
    delete_query(db, query_id)
    return {"message": "success"}
