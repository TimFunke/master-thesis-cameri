# ---------------------------------------------------------------------------------------------------------
#   Helper functions for the api routes
# ---------------------------------------------------------------------------------------------------------

from functools import wraps
from fastapi import HTTPException

from database import DatabaseException


def catchDBExceptions(func):
    ''' Decorator for catching any DatabaseException and raising it again as HTTPException '''
    @wraps(func)
    def wrapped_function(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except DatabaseException as ex:
            raise HTTPException(status_code=400, detail=str(ex))
    return wrapped_function
