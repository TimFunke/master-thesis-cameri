# ---------------------------------------------------------------------------------------------------------
#   Definition of API Endpoints for knowledge entries (get list, get single, update, create, delete)
# ---------------------------------------------------------------------------------------------------------

from typing import List
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException, Response

from dependencies import get_db
from database.crud import create_knowledge, receive_knowledge_list, receive_knowledge_single, update_knowledge, delete_knowledge
from database.models import KnowledgeList, KnowledgeSingle, KnowledgeUpdate, KnowledgeCreate
from .util import catchDBExceptions

# instantiate APIRouter object to hold the routes (endpoints)
router = APIRouter(
    prefix="/knowledge",
    tags=["knowledge"],
)


@router.get("/", response_model=List[KnowledgeList])
@catchDBExceptions
def get_knowledge_list(db: Session = Depends(get_db)):
    ''' Endpoint to get all exisiting knowledge entries '''
    results = receive_knowledge_list(db)
    return [KnowledgeList(**result) for result in results]


@router.get("/{knowledge_id}", response_model=KnowledgeSingle)
@catchDBExceptions
def get_single_knowledge(knowledge_id: int, db: Session = Depends(get_db)):
    ''' Endpoint to get the attributes of a single knowledge entry (param knowledge_id) '''
    result = receive_knowledge_single(db, knowledge_id)
    return KnowledgeSingle(**result)


@router.patch("/{knowledge_id}")
@catchDBExceptions
def update_single_knowledge(knowledge_id: int, knowledge_data: KnowledgeUpdate, db: Session = Depends(get_db)):
    ''' Endpoint to override attributes of a given knowledge entry (param knowledge_id) with the given data (param knowledge_data) '''
    update_data = knowledge_data.dict(exclude_unset=True)
    if len(update_data.keys()) == 0:
        return "nothing to update"
    update_knowledge(db, knowledge_id, update_data)
    return {"message": "success"}


@router.post("/")
@catchDBExceptions
def create_new_knowledge(knowledge_data: KnowledgeCreate, db: Session = Depends(get_db)):
    ''' Endpoint to create a new knowledge entry with the given data (param knowledge_data) '''
    knowledge_id = create_knowledge(db, knowledge_data.dict())
    return {"message": "success", "KnowledgeID": knowledge_id}


@router.delete("/{knowledge_id}")
@catchDBExceptions
def delete_single_knowledge(knowledge_id: int, db: Session = Depends(get_db)):
    ''' Endpoint to delete a single knowledge entry by id (param knowledge_id) '''
    delete_knowledge(db, knowledge_id)
    return {"message": "success"}
