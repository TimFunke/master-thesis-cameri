# ---------------------------------------------------------------------------------------------------------
#   Defintion of all enumerations used in the backend
# ---------------------------------------------------------------------------------------------------------

import enum


class E_DATA_ANNOTATION_STATUS(str, enum.Enum):
    ''' possible status values for the data annotation of a single dataset '''
    EMPTY = 'EMPTY'
    PARTIAL = 'PARTIAL'
    COMPLETED = 'COMPLETED'

    def __str__(self) -> str:
        return self.value


class E_SCALE_TYPE(str, enum.Enum):
    ''' possible scale types of attribute (column) '''
    CATEGORICAL = 'CATEGORICAL'
    ORDINAL = 'ORDINAL'
    NUMERIC = 'NUMERIC'

    def __str__(self) -> str:
        return self.value


class E_FEATURE_TYPE(str, enum.Enum):
    ''' possible feature types of calculated features '''
    MISSING_VALUES = 'MISSING_VALUES'
    STANDARDIZED = 'STANDARDIZED'
    NORMALIZED = 'NORMALIZED'

    def __str__(self) -> str:
        return self.value


class E_QUERY_STATUS(str, enum.Enum):
    ''' possible status values of a single query '''
    CREATED = 'CREATED'
    RUNNING_FEATURE_EXTRACTION = 'RUNNING_FEATURE_EXTRACTION'
    RUNNING_INFERENCE = 'RUNNING_INFERENCE'
    FINISHED = 'FINISHED'

    def __str__(self) -> str:
        return self.value


class E_MEDIATION_TYPE(str, enum.Enum):
    ''' possible types of entries in table mediation '''
    ANALYSIS_METHOD = 'ANALYSIS_METHOD'
    PREPROCESSING = 'PREPROCESSING'
    PRECONDITION = 'PRECONDITION'

    def __str__(self) -> str:
        return self.value


class E_RECOMMENDATION_TYPE(str, enum.Enum):
    ''' possible types of recommendation for a analysis method '''
    PRACTICABLE = 'PRACTICABLE'
    PREPROCESSING_NEEDED = 'PREPROCESSING_NEEDED'
    IMPRACTICABLE = 'IMPRACTICABLE'

    def __str__(self) -> str:
        return self.value
