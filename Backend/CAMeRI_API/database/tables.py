# ---------------------------------------------------------------------------------------------------------
#   Defintion of all table models via sqlalchemy (ORM)
# ---------------------------------------------------------------------------------------------------------

from __future__ import annotations
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Enum, JSON, Float
from sqlalchemy.orm import relationship
from typing import List
from .connection import Base
from .enums import E_FEATURE_TYPE, E_SCALE_TYPE, E_QUERY_STATUS, E_MEDIATION_TYPE, E_RECOMMENDATION_TYPE


class QueryTable(Base):
    """Storing all information related to a query"""
    __tablename__ = "query"

    # Columns:
    QueryID: int = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    DatasetID: int = Column(Integer, ForeignKey('dataset.DatasetID', ondelete="SET NULL"), nullable=False)
    Name: str = Column(String, nullable=False)
    Status: E_QUERY_STATUS = Column(Enum(E_QUERY_STATUS), nullable=False)
    CeleryTask: str = Column(String, nullable=True)

    # Relationships:
    dataset: DatasetTable = relationship("DatasetTable", lazy="select", passive_deletes="all")
    settings: QuerySettingsTable = relationship("QuerySettingsTable", lazy="select", innerjoin=True, uselist=False, passive_deletes="all")
    features: List[FeatureTable] = relationship("FeatureTable", lazy="select", passive_deletes="all")
    recommendations: List[RecommendationTable] = relationship("RecommendationTable", lazy="select", passive_deletes="all")
    knowledge: List[QueryKnowledgeTable] = relationship("QueryKnowledgeTable", lazy="select", passive_deletes="all")

# -------------------------------------------------------------------------------------------------------------------------------------------------------------


class QuerySettingsTable(Base):
    """Storing all information related to a query settings"""
    __tablename__ = "query_settings"

    # Columns:
    QueryID: int = Column(Integer, ForeignKey('query.QueryID', ondelete="CASCADE"), nullable=False, primary_key=True)
    NormalizedMin: float = Column(Float, nullable=False, default=-1)
    NormalizedMax: float = Column(Float, nullable=False, default=1)
    StandardizedThresholdMean: float = Column(Float, nullable=False, default=0.1)
    StandardizedThresholdStd: float = Column(Float, nullable=False, default=0.1)

# -------------------------------------------------------------------------------------------------------------------------------------------------------------


class DatasetTable(Base):
    """Storing all information related to a dataset"""
    __tablename__ = "dataset"

    # Columns:
    DatasetID: int = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    Name: str = Column(String, nullable=False)
    DataPath: str = Column(String, nullable=True)
    MetaInfo: dict = Column(JSON, nullable=True)

    # Relationships:
    annotations: List[DataAnnotationTable] = relationship("DataAnnotationTable", lazy="joined", passive_deletes="all")

# -------------------------------------------------------------------------------------------------------------------------------------------------------------


class DataAnnotationTable(Base):
    """Storing all information related to a data_annotation"""
    __tablename__ = "data_annotation"

    # Columns:
    DatasetID: int = Column(Integer, ForeignKey('dataset.DatasetID', ondelete="CASCADE"), nullable=False, primary_key=True)
    ColumnIndex: int = Column(Integer, nullable=False, primary_key=True)
    ColumnName: str = Column(String, nullable=False)
    ScaleType: E_SCALE_TYPE = Column(Enum(E_SCALE_TYPE), nullable=True)
    IsTarget: bool = Column(Boolean, nullable=False)
    IsIgnore: bool = Column(Boolean, nullable=False)

# -------------------------------------------------------------------------------------------------------------------------------------------------------------


class FeatureTable(Base):
    """Storing all information related to a feature"""
    __tablename__ = "feature"

    # Columns:
    FeatureID: int = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    QueryID: int = Column(Integer, ForeignKey('query.QueryID', ondelete="CASCADE"), nullable=False)
    ColumnIndex: int = Column(Integer, nullable=False)
    FeatureType: E_FEATURE_TYPE = Column(Enum(E_FEATURE_TYPE), nullable=False)
    FeatureValue: bool = Column(Boolean, nullable=False)

# -------------------------------------------------------------------------------------------------------------------------------------------------------------


class KnowledgeTable(Base):
    """Storing all information related to a knowledge"""
    __tablename__ = "knowledge"

    # Columns:
    KnowledgeID: int = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    KnowledgeName: str = Column(String, nullable=False)
    KnowledgeGroup: str = Column(String, nullable=False)
    Expression: str = Column(String, nullable=False)

# -------------------------------------------------------------------------------------------------------------------------------------------------------------


class MediationTable(Base):
    """Storing all information related to a mediation"""
    __tablename__ = "mediation"

    # Columns:
    MediationID: int = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    EntityName: str = Column(String, nullable=False)
    EntityType: E_MEDIATION_TYPE = Column(Enum(E_MEDIATION_TYPE), nullable=False)
    KnowledgeTerm: str = Column(String, nullable=False)
    EntityDescription: str = Column(String, nullable=False)

# -------------------------------------------------------------------------------------------------------------------------------------------------------------


class QueryKnowledgeTable(Base):
    """N:M connection for query and knowledge"""
    __tablename__ = "query_knowledge"

    # Columns:
    QueryID: int = Column(Integer, ForeignKey('query.QueryID', ondelete="CASCADE"), nullable=False, primary_key=True)
    KnowledgeID: int = Column(Integer, ForeignKey('knowledge.KnowledgeID', ondelete="CASCADE"), nullable=False, primary_key=True)

    # Relationships:
    knowledge_data: KnowledgeTable = relationship("KnowledgeTable", lazy="joined", innerjoin=True, passive_deletes="all")

# -------------------------------------------------------------------------------------------------------------------------------------------------------------


class RecommendationTable(Base):
    """Storing all information related to a recommendation"""
    __tablename__ = "recommendation"

    # Columns:
    RecommendationID: int = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    QueryID: int = Column(Integer, ForeignKey('query.QueryID', ondelete="CASCADE"), nullable=False)
    MethodID: int = Column(Integer, ForeignKey('mediation.MediationID', ondelete="CASCADE"), nullable=False)
    RecType: E_RECOMMENDATION_TYPE = Column(Enum(E_RECOMMENDATION_TYPE), nullable=False)

    # Relationships:
    method: MediationTable = relationship("MediationTable", lazy="joined", passive_deletes="all")
    preconditions: List[RecommendationPreconditionTable] = relationship(
        "RecommendationPreconditionTable", lazy="joined", passive_deletes="all")

# -------------------------------------------------------------------------------------------------------------------------------------------------------------


class RecommendationPreconditionTable(Base):
    """Storing all information related to a recommendation_precondition"""
    __tablename__ = "recommendation_precondition"

    # Columns:
    RecommendationPreconditionID: int = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    RecommendationID: int = Column(Integer, ForeignKey('recommendation.RecommendationID', ondelete="CASCADE"), nullable=False)
    PreconditionID: int = Column(Integer, ForeignKey('mediation.MediationID', ondelete="CASCADE"), nullable=False)
    HarmedByColumns: str = Column(String, nullable=True)
    PreprocessingID: int = Column(Integer, ForeignKey('mediation.MediationID', ondelete="CASCADE"), nullable=True)

    # Relationships:
    precondition: MediationTable = relationship("MediationTable", foreign_keys=[PreconditionID], lazy="joined", passive_deletes="all")
    preprocessing: MediationTable = relationship("MediationTable", foreign_keys=[PreprocessingID], lazy="joined", passive_deletes="all")
