# ---------------------------------------------------------------------------------------------------------
#   Helper functions for database queries and result formatting
# ---------------------------------------------------------------------------------------------------------

from .tables import Base
from sqlalchemy.orm import Session
from .tables import QueryTable, DatasetTable, DataAnnotationTable, KnowledgeTable
from .models import E_DATA_ANNOTATION_STATUS


class DatabaseException(Exception):
    ''' custom exception for all exceptions raised by database calls or queries '''
    pass


def row2dict(row) -> dict:
    ''' helper function to transform database result object (sqlalchemy Base) to python dictionary '''
    if isinstance(row, Base):
        return {c.name: getattr(row, c.name)
                for c in row.__table__.columns}
    return row._asdict()


def get_query_by_id(session: Session, query_id: int, columns=None) -> QueryTable:
    ''' search a query by id 
    - define and execute db query
    - check result
    - return
    '''
    if columns == None:
        result = session.query(QueryTable).filter(QueryTable.QueryID == query_id).first()
    else:
        result = session.query(*columns).filter(QueryTable.QueryID == query_id).first()
    if result is None:
        raise DatabaseException(f"No query with id {query_id} found")
    return result


def get_knowledge_by_id(session: Session, knowledge_id: int) -> KnowledgeTable:
    ''' search a knowledge entry by id 
    - define and execute db query
    - check result
    - return
    '''
    result = session.query(KnowledgeTable).filter(KnowledgeTable.KnowledgeID == knowledge_id).first()
    if result is None:
        raise DatabaseException(f"No knowledge with id {knowledge_id} found")
    return result


def get_dataset_by_id(session: Session, dataset_id: int) -> DatasetTable:
    ''' search a dataset by id 
    - define and execute db query
    - check result
    - return
    '''
    result = session.query(DatasetTable).filter(DatasetTable.DatasetID == dataset_id).first()
    if result is None:
        raise DatabaseException(f"No dataset with id {dataset_id} found")
    return result


def get_data_annotation_by_id(session: Session, dataset_id: int, column_index: int) -> DataAnnotationTable:
    ''' search a data annotation by id 
    - define and execute db query
    - check result
    - return
    '''
    result = session.query(DataAnnotationTable)\
        .filter((DataAnnotationTable.DatasetID == dataset_id) & (DataAnnotationTable.ColumnIndex == column_index))\
        .first()
    if result is None:
        raise DatabaseException(f"No data annotation for dataset {dataset_id} and column index {column_index} found")
    return result


def get_data_annotation_status(session: Session, dataset_id: int) -> E_DATA_ANNOTATION_STATUS:
    ''' calculate the data annotation status for a given dataset (param dataset_id) 
    - find the dataset in the database
    - count the amount of non ignore columns and annotated columns
    - build data annotation status and return
    '''
    annotations = get_dataset_by_id(session, dataset_id).annotations
    amount_all = 0
    amount_done = 0
    for annotation in annotations:
        if annotation.IsIgnore:
            continue
        amount_all += 1
        if annotation.ScaleType is not None:
            amount_done += 1
    if amount_done == 0:
        return E_DATA_ANNOTATION_STATUS.EMPTY
    if amount_done < amount_all:
        return E_DATA_ANNOTATION_STATUS.PARTIAL
    return E_DATA_ANNOTATION_STATUS.COMPLETED
