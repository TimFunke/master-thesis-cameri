# ---------------------------------------------------------------------------------------------------------
#   CRUD (create, receive, update, delete) functions for knowledge entries
# ---------------------------------------------------------------------------------------------------------

import json
from sqlalchemy.orm import Session
from ..tables import KnowledgeTable
from ..util import row2dict, get_knowledge_by_id


def create_knowledge(session: Session, knowledge_data: dict) -> int:
    ''' create new knowledge entry and return its id'''
    knowledge = KnowledgeTable(**knowledge_data)
    session.add(knowledge)
    session.commit()
    session.refresh(knowledge)
    return knowledge.KnowledgeID


def receive_knowledge_single(session: Session, knowledge_id: int):
    ''' receive a single knowledge entry '''
    result = get_knowledge_by_id(session, knowledge_id)
    return_data = row2dict(result)
    return return_data


def receive_knowledge_list(session: Session):
    ''' receive all knowledge entries '''
    result = session.query(KnowledgeTable).all()
    return [row2dict(row) for row in result]


def update_knowledge(session: Session, knowledge_id: int, knowledge_data: dict):
    ''' update a knowledge entry '''
    result = get_knowledge_by_id(session, knowledge_id)
    for key, value in knowledge_data.items():
        setattr(result, key, value)
    session.commit()


def delete_knowledge(session: Session, knowledge_id: int):
    ''' delte a knowledge entry '''
    result = get_knowledge_by_id(session, knowledge_id)
    session.delete(result)
    session.commit()
