# ---------------------------------------------------------------------------------------------------------
#   CRUD (create, receive, update, delete) functions for data annotations
# ---------------------------------------------------------------------------------------------------------

from typing import List
from sqlalchemy.orm import Session
from ..tables import DatasetTable
from ..util import row2dict, get_data_annotation_by_id, get_dataset_by_id, get_data_annotation_status


def receive_data_annotation(session: Session, dataset_id: int):
    ''' receive data annotations for a single dataset '''
    result: DatasetTable = get_dataset_by_id(session, dataset_id)
    annotations = [row2dict(row) for row in result.annotations]
    return_data = {"DatasetName": result.Name}
    return_data["Annotations"] = annotations
    return return_data


def receive_data_annotations(session: Session):
    ''' receive all datasets with their data annotation status '''
    datasets = session.query(DatasetTable).all()
    return_data = []
    for dataset in datasets:
        return_data.append({
            "DatasetID": dataset.DatasetID,
            "DatasetName": dataset.Name,
            "DataAnnotationStatus": get_data_annotation_status(session, dataset.DatasetID),
        })
    return return_data


def update_data_annotation(session: Session, dataset_id: int, data_annotation_data: List[dict]):
    ''' update data annotations for a single dataset '''
    try:
        for entry in data_annotation_data:
            result = get_data_annotation_by_id(session, dataset_id, entry["ColumnIndex"])
            for key, value in entry.items():
                if key == "ColumnIndex":
                    continue
                setattr(result, key, value)
        session.commit()
    except Exception:
        from traceback import print_exc
        print_exc()
        session.rollback()
