# ---------------------------------------------------------------------------------------------------------
#   Defintion of all input and output data models via pydantic
# ---------------------------------------------------------------------------------------------------------

from typing import Optional, List

from pydantic import BaseModel
from .enums import E_DATA_ANNOTATION_STATUS, E_QUERY_STATUS, E_SCALE_TYPE, E_RECOMMENDATION_TYPE


class QuerySingle(BaseModel):
    ''' Response data for a single query '''
    Name: str
    DatasetID: int
    DatasetName: str
    DataAnnotationStatus: E_DATA_ANNOTATION_STATUS
    Knowledge: List[int]


class QueryList(BaseModel):
    ''' Response data for a list of queries '''
    QueryID: int
    Name: str
    Status: E_QUERY_STATUS


class QueryStatus(BaseModel):
    ''' Response data for the status of a single query '''
    QueryID: int
    Status: E_QUERY_STATUS


class QueryResultPreconditions(BaseModel):
    ''' Building block for QueryResult - Response data for a precondition '''
    PreconditionName: str
    PreconditionDescription: str
    HarmedByColumns: List[str]
    PreprocessingName: Optional[str]
    PreprocessingDescription: Optional[str]


class QueryResult(BaseModel):
    ''' Building block for QueryResults - Response data for a analysis method '''
    MethodName: str
    MethodDescription: str
    Type: E_RECOMMENDATION_TYPE
    Preconditions: List[QueryResultPreconditions]


class QueryResults(BaseModel):
    ''' Response data for the results data of a single query '''
    QueryName: str
    Results: List[QueryResult]


class QueryCreate(BaseModel):
    ''' Request data for the creation of a new query '''
    Name: str
    DatasetID: int
    Knowledge: List[int]


class QueryUpdate(BaseModel):
    ''' Request data for the update of a query '''
    Name: Optional[str] = None


class DataAnnotationList(BaseModel):
    ''' Response data for a list of datasets with their data annotation status '''
    DatasetID: str
    DatasetName: str
    DataAnnotationStatus: E_DATA_ANNOTATION_STATUS


class DataAnnotationEntry(BaseModel):
    ''' Building block for DataAnnotationSingle - Response data for the data annotation of a column '''
    ColumnIndex: int
    ColumnName: str
    ScaleType: Optional[E_SCALE_TYPE]
    IsTarget: bool
    IsIgnore: bool


class DataAnnotationSingle(BaseModel):
    ''' Response data for the data annotations of a single dataset '''
    DatasetName: str
    Annotations: List[DataAnnotationEntry]


class DataAnnotationUpdate(BaseModel):
    ''' Request data for the update of a data annotation '''
    ColumnIndex: int
    ScaleType: Optional[E_SCALE_TYPE] = None
    IsTarget: Optional[bool] = None
    IsIgnore: Optional[bool] = None


class KnowledgeSingle(BaseModel):
    ''' Response data for a single knowledge entry '''
    KnowledgeName: str
    KnowledgeGroup: str
    Expression: str


class KnowledgeList(BaseModel):
    ''' Response data for a list of knowledge entries '''
    KnowledgeID: int
    KnowledgeName: str
    KnowledgeGroup: str


class KnowledgeCreate(BaseModel):
    ''' Request data for the creation of a new knowledge entry '''
    KnowledgeName: str
    KnowledgeGroup: str
    Expression: str


class KnowledgeUpdate(BaseModel):
    ''' Request data for the update of a knowledge entry '''
    KnowledgeName: Optional[str] = None
    KnowledgeGroup: Optional[str] = None
    Expression: Optional[str] = None
