/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Exportiere Datenbank Struktur für cameri
DROP DATABASE IF EXISTS `cameri`;
CREATE DATABASE IF NOT EXISTS `cameri`;
CREATE USER IF NOT EXISTS 'cameri-user'@'%' IDENTIFIED BY 'cameri-pw';
GRANT ALL PRIVILEGES ON cameri.* TO 'cameri-user'@'%';
USE `cameri`;

-- Exportiere Struktur von Tabelle cameri.data_annotation
DROP TABLE IF EXISTS `data_annotation`;
CREATE TABLE IF NOT EXISTS `data_annotation` (
  `DatasetID` int(11) NOT NULL,
  `ColumnIndex` int(11) NOT NULL,
  `ColumnName` text NOT NULL,
  `ScaleType` enum('CATEGORICAL','ORDINAL','NUMERIC') DEFAULT NULL,
  `IsTarget` tinyint(1) NOT NULL,
  `IsIgnore` tinyint(1) NOT NULL,
  PRIMARY KEY (`DatasetID`,`ColumnIndex`),
  CONSTRAINT `data_annotation_ibfk_1` FOREIGN KEY (`DatasetID`) REFERENCES `dataset` (`DatasetID`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Exportiere Struktur von Tabelle cameri.dataset
DROP TABLE IF EXISTS `dataset`;
CREATE TABLE IF NOT EXISTS `dataset` (
  `DatasetID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `DataPath` text DEFAULT NULL,
  `MetaInfo` text DEFAULT NULL,
  PRIMARY KEY (`DatasetID`)
);

-- Exportiere Struktur von Tabelle cameri.feature
DROP TABLE IF EXISTS `feature`;
CREATE TABLE IF NOT EXISTS `feature` (
  `FeatureID` int(11) NOT NULL AUTO_INCREMENT,
  `QueryID` int(11) NOT NULL,
  `ColumnIndex` int(11) NOT NULL,
  `FeatureType` enum('MISSING_VALUES','STANDARDIZED','NORMALIZED') NOT NULL,
  `FeatureValue` tinyint(1) NOT NULL,
  PRIMARY KEY (`FeatureID`),
  KEY `QueryID` (`QueryID`),
  CONSTRAINT `feature_ibfk_1` FOREIGN KEY (`QueryID`) REFERENCES `query` (`QueryID`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Exportiere Struktur von Tabelle cameri.knowledge
DROP TABLE IF EXISTS `knowledge`;
CREATE TABLE IF NOT EXISTS `knowledge` (
  `KnowledgeID` int(11) NOT NULL AUTO_INCREMENT,
  `KnowledgeName` text NOT NULL,
  `KnowledgeGroup` text NOT NULL,
  `Expression` text NOT NULL,
  PRIMARY KEY (`KnowledgeID`)
);

-- Exportiere Struktur von Tabelle cameri.mediation
DROP TABLE IF EXISTS `mediation`;
CREATE TABLE IF NOT EXISTS `mediation` (
  `MediationID` int(11) NOT NULL AUTO_INCREMENT,
  `EntityName` text NOT NULL,
  `EntityType` enum('ANALYSIS_METHOD','PREPROCESSING','PRECONDITION') NOT NULL,
  `KnowledgeTerm` text NOT NULL,
  `EntityDescription` text NOT NULL,
  PRIMARY KEY (`MediationID`)
);

-- Exportiere Struktur von Tabelle cameri.query
DROP TABLE IF EXISTS `query`;
CREATE TABLE IF NOT EXISTS `query` (
  `QueryID` int(11) NOT NULL AUTO_INCREMENT,
  `DatasetID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Status` enum('CREATED','RUNNING_FEATURE_EXTRACTION','RUNNING_INFERENCE','FINISHED') NOT NULL,
  `CeleryTask` text DEFAULT NULL,
  PRIMARY KEY (`QueryID`),
  KEY `DatasetID` (`DatasetID`),
  CONSTRAINT `query_ibfk_1` FOREIGN KEY (`DatasetID`) REFERENCES `dataset` (`DatasetID`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Exportiere Struktur von Tabelle cameri.query_settings
DROP TABLE IF EXISTS `query_settings`;
CREATE TABLE IF NOT EXISTS `query_settings` (
  `QueryID` int(11) NOT NULL,
  `NormalizedMin` float NOT NULL,
  `NormalizedMax` float NOT NULL,
  `StandardizedThresholdMean` float NOT NULL,
  `StandardizedThresholdStd` float NOT NULL,
  PRIMARY KEY (`QueryID`),
  CONSTRAINT `query_settings_ibfk_1` FOREIGN KEY (`QueryID`) REFERENCES `query` (`QueryID`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Exportiere Struktur von Tabelle cameri.query_knowledge
DROP TABLE IF EXISTS `query_knowledge`;
CREATE TABLE IF NOT EXISTS `query_knowledge` (
  `QueryID` int(11) NOT NULL,
  `KnowledgeID` int(11) NOT NULL,
  PRIMARY KEY (`QueryID`,`KnowledgeID`),
  KEY `KnowledgeID` (`KnowledgeID`),
  CONSTRAINT `query_knowledge_ibfk_1` FOREIGN KEY (`QueryID`) REFERENCES `query` (`QueryID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `query_knowledge_ibfk_2` FOREIGN KEY (`KnowledgeID`) REFERENCES `knowledge` (`KnowledgeID`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Exportiere Struktur von Tabelle cameri.recommendation
DROP TABLE IF EXISTS `recommendation`;
CREATE TABLE IF NOT EXISTS `recommendation` (
  `RecommendationID` int(11) NOT NULL AUTO_INCREMENT,
  `QueryID` int(11) NOT NULL,
  `MethodID` int(11) NOT NULL,
  `RecType` enum('PRACTICABLE','PREPROCESSING_NEEDED','IMPRACTICABLE') NOT NULL,
  PRIMARY KEY (`RecommendationID`),
  KEY `QueryID` (`QueryID`),
  KEY `MethodID` (`MethodID`),
  CONSTRAINT `recommendation_ibfk_1` FOREIGN KEY (`QueryID`) REFERENCES `query` (`QueryID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `recommendation_ibfk_2` FOREIGN KEY (`MethodID`) REFERENCES `mediation` (`MediationID`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Exportiere Struktur von Tabelle cameri.recommendation_precondition
DROP TABLE IF EXISTS `recommendation_precondition`;
CREATE TABLE IF NOT EXISTS `recommendation_precondition` (
  `RecommendationPreconditionID` int(11) NOT NULL AUTO_INCREMENT,
  `RecommendationID` int(11) NOT NULL,
  `PreconditionID` int(11) NOT NULL,
  `HarmedByColumns` text DEFAULT NULL,
  `PreprocessingID` int(11) DEFAULT NULL,
  PRIMARY KEY (`RecommendationPreconditionID`),
  KEY `RecommendationID` (`RecommendationID`),
  KEY `PreconditionID` (`PreconditionID`),
  KEY `PreprocessingID` (`PreprocessingID`),
  CONSTRAINT `recommendation_precondition_ibfk_1` FOREIGN KEY (`RecommendationID`) REFERENCES `recommendation` (`RecommendationID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `recommendation_precondition_ibfk_2` FOREIGN KEY (`PreconditionID`) REFERENCES `mediation` (`MediationID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `recommendation_precondition_ibfk_3` FOREIGN KEY (`PreprocessingID`) REFERENCES `mediation` (`MediationID`) ON DELETE CASCADE ON UPDATE CASCADE
);

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;