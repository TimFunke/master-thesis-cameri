@echo off

set dataset_folder=%cd%\datasets\
call activate base

start /d Celery_Queue cmd.exe /c celery -A main worker --pool=solo --loglevel=INFO