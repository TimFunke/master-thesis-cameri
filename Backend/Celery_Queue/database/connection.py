# ---------------------------------------------------------------------------------------------------------
#   Utility for the Connection with the database (MariaDB) and Setup of SQLAlchemy
# ---------------------------------------------------------------------------------------------------------

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# create singleton instance of SQLAlchemy declarative_base
Base = declarative_base()


class DBConnection:
    ''' Wrapper class to manage the connection to the database (MariaDB) '''

    engine = None
    Session = None

    @classmethod
    def setup(cls, full_uri: str = None, user: str = None, password: str = None,
              host: str = None, port: int = None, db_name: str = None):
        ''' Use config information to setup the database connection objects
        - determine the URI string for the connection
        - create an instance of the SQLAlchemy Database engine the the connection string
        - create an instance of the SQLAlchemy Sessionmaker for creating sessions later (function getSession)
        '''
        if full_uri is not None:
            SQLALCHEMY_DATABASE_URI = full_uri
        else:
            SQLALCHEMY_DATABASE_URI = f"mysql://{user}:{password}@{host}:{port}/{db_name}"
        cls.engine = create_engine(SQLALCHEMY_DATABASE_URI)
        cls.Session = sessionmaker(autocommit=False, autoflush=False, bind=cls.engine)

    @classmethod
    def getSession(cls):
        ''' provide a sqlalchemy session with the database '''
        return cls.Session()
