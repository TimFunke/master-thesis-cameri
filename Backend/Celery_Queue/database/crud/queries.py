# ---------------------------------------------------------------------------------------------------------
#   CRUD (create, receive, update, delete) functions for knowledge queries
# ---------------------------------------------------------------------------------------------------------

import json
from sqlalchemy.orm import Session
from ..tables import E_QUERY_STATUS, QueryTable, QuerySettingsTable, KnowledgeTable, QueryKnowledgeTable
from ..util import row2dict, get_query_by_id, get_dataset_by_id, get_data_annotation_status


def create_query(session: Session, query_data: dict) -> int:
    ''' create new query and return its id'''
    query = QueryTable(Name=query_data["Name"], Status=E_QUERY_STATUS.CREATED)
    dataset = get_dataset_by_id(session, query_data["DatasetID"])
    query.dataset = dataset
    settings = QuerySettingsTable()
    query.settings = settings
    knowledge = set(query_data["Knowledge"])
    complete_knowledge = session.query(KnowledgeTable).all()
    complete_knowledge_ids = set([entry.KnowledgeID for entry in complete_knowledge])
    missing_knowledge_ids = knowledge - complete_knowledge_ids
    if len(missing_knowledge_ids) > 0:
        raise Exception(f"unknown knowledge IDs: {', '.join([str(kid) for kid in sorted(missing_knowledge_ids)])}")
    query.knowledge = [QueryKnowledgeTable(KnowledgeID=knowledge_id) for knowledge_id in knowledge]
    session.add(query)
    session.commit()
    session.refresh(query)
    return query.QueryID


def receive_query(session: Session, query_id: int):
    ''' receive a single query '''
    result = get_query_by_id(session, query_id)
    return_data = row2dict(result)
    return_data["DatasetName"] = result.dataset.Name
    return_data["DataAnnotationStatus"] = get_data_annotation_status(session, result.dataset.DatasetID)
    return_data["Knowledge"] = [entry.KnowledgeID for entry in result.knowledge]
    return return_data


def receive_query_status(session: Session, query_id: int):
    ''' receive the status of a query '''
    result = get_query_by_id(session, query_id, columns=[QueryTable.QueryID, QueryTable.Status])
    return_data = row2dict(result)
    return return_data


def receive_query_results(session: Session, query_id: int):
    ''' receive the results of a query '''
    return_data = {}
    query = get_query_by_id(session, query_id)
    return_data["QueryName"] = query.Name
    column_index_name = {entry["index"]: entry["name"] for entry in query.dataset.MetaInfo["columns"]}
    column_index_name[-1] = "<general>"
    results = []
    for recommendation in query.recommendations:
        recommendation_entry = {}
        recommendation_entry["Type"] = recommendation.RecType
        analysis_method = recommendation.method
        recommendation_entry["MethodName"] = analysis_method.EntityName
        recommendation_entry["MethodDescription"] = analysis_method.EntityDescription
        preconditions = []
        for precondition in recommendation.preconditions:
            precondition_entry = {}
            precondition_method = precondition.precondition
            precondition_entry["PreconditionName"] = precondition_method.EntityName
            precondition_entry["PreconditionDescription"] = precondition_method.EntityDescription
            preprocessing_method = precondition.preprocessing
            if preprocessing_method is None:
                precondition_entry["PreprocessingName"] = None
                precondition_entry["PreprocessingDescription"] = None
            else:
                precondition_entry["PreprocessingName"] = preprocessing_method.EntityName
                precondition_entry["PreprocessingDescription"] = preprocessing_method.EntityDescription
            harmed_by_columns = json.loads(precondition.HarmedByColumns)
            precondition_entry["HarmedByColumns"] = [column_index_name.get(column_index, '???') for column_index in harmed_by_columns]
            preconditions.append(precondition_entry)
        recommendation_entry["Preconditions"] = preconditions
        results.append(recommendation_entry)
    return_data["Results"] = results
    return return_data


def receive_queries(session: Session):
    ''' receive a list of all queries '''
    result = session.query(QueryTable).all()
    return [row2dict(row) for row in result]


def update_query(session: Session, query_id: int, query_data: dict):
    ''' update a query '''
    result = get_query_by_id(session, query_id)
    for key, value in query_data.items():
        setattr(result, key, value)
    session.commit()


def delete_query(session: Session, query_id: int):
    ''' delete a query '''
    result = get_query_by_id(session, query_id)
    session.delete(result)
    session.commit()
