# ---------------------------------------------------------------------------------------------------------
#   Definition of the Workflow for the inference step
# ---------------------------------------------------------------------------------------------------------

from __future__ import annotations
from typing import List
from os import path, rmdir
from shutil import rmtree

from database import DBConnection
from database.tables import QueryTable, RecommendationTable, MediationTable
from database.enums import E_QUERY_STATUS, E_MEDIATION_TYPE
from .mediator_db_symAI import MediatorDBSymAI
from .prolog_wrapper import PrologWrapper


class InferenceSystemController:
    ''' Class to perform the workflow of the inference step '''

    def __init__(self, query_id: int, database_config: dict, inference_config: dict):
        self.query_id = query_id
        self.cleanup_workspace = False
        self.workspace_folder = path.join(inference_config["PrologWorkspaceFolder"], f"query_{self.query_id}")
        self.cleanup_workspace = inference_config["CleanupWorkspace"]
        DBConnection.setup(
            host=database_config["Host"],
            port=database_config["Port"],
            user=database_config["Username"],
            password=database_config["Password"],
            db_name=database_config["DatabaseName"]
        )
        self.prolog_wrapper = PrologWrapper(
            runtime_executable=inference_config["PrologRuntimeExecutable"],
            workspace_folder=self.workspace_folder,
        )
        self.mediator = MediatorDBSymAI(
            workspace_folder=self.workspace_folder,
            impossible_predicate=inference_config["ImpossiblePredicate"],
        )

    def execute(self):
        ''' execution of the inference workflow (highest level)
        - get database session
        - update query status to "RUNNING_INFERENCE"
        - create knowledge files (data annotations, features, knowledge, preprocessing, impossible)
        - execute prolog
        - read prolog results
        - update query status to "FINISHED"
        - (cleanup workspace)
        '''

        try:
            self.db_session = DBConnection.getSession()
            self.update_query_status(is_finished=False)
            self.create_knowledge_files()
            self.execute_prolog()
            results = self.read_results()
            self.write_results(results)
            self.update_query_status(is_finished=True)
            if self.cleanup_workspace:
                try:
                    rmtree(self.workspace_folder, ignore_errors=True)
                    rmdir(self.workspace_folder)
                except Exception:
                    pass

        except:
            raise

        finally:
            self.db_session.close()

    # ------------------------------------------------------------------------------------------------

    def update_query_status(self, is_finished: bool):
        ''' set the query status to "RUNNING_FEATURE_EXTRACTION" or "FINISHED" depending on the param is_finished '''

        status = E_QUERY_STATUS.FINISHED if is_finished else E_QUERY_STATUS.RUNNING_INFERENCE
        self.db_session.query(QueryTable)\
            .filter(QueryTable.QueryID == self.query_id)\
            .update({QueryTable.Status: status})
        self.db_session.commit()

    # ------------------------------------------------------------------------------------------------

    def create_knowledge_files(self):
        ''' creates all knowledge files (prefix input) by calling the corresponding mediator functions
        - features (calculated features by feature extraction)
        - data annotations (given features by the user)
        - knowledge (knowledge base)
        - preprocessing (possible preprocessings)
        - impossible (single predicate that indicates that a precondition is not fulfilled)
        '''

        query: QueryTable = self.db_session\
            .query(QueryTable)\
            .filter(QueryTable.QueryID == self.query_id).first()

        self.mediator.mediate_features(query.features)
        self.mediator.mediate_data_annotation(query.dataset.annotations)

        knowledge = [knowledege_entry.knowledge_data for knowledege_entry in query.knowledge]
        column_indices = [annotation.ColumnIndex for annotation in query.dataset.annotations if not annotation.IsIgnore]
        self.mediator.mediate_knowledge(knowledge, column_indices)

        preprocessings: List[MediationTable] = self.db_session\
            .query(MediationTable)\
            .filter(MediationTable.EntityType == E_MEDIATION_TYPE.PREPROCESSING).all()
        self.mediator.mediate_preprocessing(preprocessings)

        self.mediator.mediate_impossible_predicate()

    # ------------------------------------------------------------------------------------------------

    def execute_prolog(self):
        ''' executes prolog by calling the corresponding prolog wrapper function 
        - gather the predicates to trace from the database (preprocessings, preconditions and impossible)
        - gather the target predicates from the database (all analysis methods)
        - call prolog wrapper execute
        '''

        to_trace: List[MediationTable] = self.db_session.query(MediationTable).filter(
            (MediationTable.EntityType == E_MEDIATION_TYPE.PREPROCESSING) | (MediationTable.EntityType == E_MEDIATION_TYPE.PRECONDITION)).all()
        targets: List[MediationTable] = self.db_session\
            .query(MediationTable)\
            .filter((MediationTable.EntityType == E_MEDIATION_TYPE.ANALYSIS_METHOD)).all()
        trace_predicates = self.mediator.get_raw_predicates(to_trace)
        trace_predicates = self.mediator.append_impossible_predicate(trace_predicates)
        target_predicates = self.mediator.get_raw_predicates(targets)
        self.prolog_wrapper.execute(trace_predicates, target_predicates)

    # ------------------------------------------------------------------------------------------------

    def read_results(self) -> List[RecommendationTable]:
        ''' reads the results from prolog and translates them to database entries '''

        raw_results = self.prolog_wrapper.read_results()
        mediations: List[MediationTable] = self.db_session.query(MediationTable).all()
        mediated_results = self.mediator.mediate_results(self.query_id, mediations, raw_results)
        return mediated_results

    # ------------------------------------------------------------------------------------------------

    def write_results(self, results: List[RecommendationTable]):
        ''' writes the results to the database '''

        self.db_session.query(RecommendationTable).filter(
            RecommendationTable.QueryID == self.query_id
        ).delete()  # ON DELETE CASCADE removes old preconditions
        self.db_session.add_all(results)
        self.db_session.commit()
