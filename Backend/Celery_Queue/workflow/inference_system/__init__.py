# ---------------------------------------------------------------------------------------------------------
#   Export function to execute inference
# ---------------------------------------------------------------------------------------------------------

from .inference_system import InferenceSystemController


def executeInferenceSystem(query_id: int, database_config: dict, inference_config: dict):
    InferenceSystemController(query_id, database_config, inference_config).execute()
