# ---------------------------------------------------------------------------------------------------------
#   Export function to execute feature extraction
# ---------------------------------------------------------------------------------------------------------

from .feature_extraction import FeatureExtraction


def executeFeatureExtraction(query_id: int, database_config: dict):
    FeatureExtraction(query_id, database_config).execute()
