# ---------------------------------------------------------------------------------------------------------
#   Definition of the feature extraction functions
# ---------------------------------------------------------------------------------------------------------

import pandas as pd
from functools import wraps
from typing import List, Tuple, Callable

# create list to hold all registered feature extraction functions
registered_features: List[Tuple[str, Callable]] = []


# decorator to mark feature extraction functions and automatically add them to the list registered_features
def feature(db_identificator: str, return_value_on_error: bool = False):
    def feature_decorator(func):
        @wraps(func)
        def wrapped_function(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception:
                return return_value_on_error
        registered_features.append((db_identificator, wrapped_function))
        return wrapped_function
    return feature_decorator

# -----------------------------------------------------------------------------------------


@feature("MISSING_VALUES", return_value_on_error=False)
def check_missing_values(column_data: pd.Series, config: dict) -> bool:
    ''' feature extraction function for feature missing values
    - checks if the amount of missing values is greater than 0 (True if it is, False if not)
    '''
    return column_data.isna().sum() > 0


@feature("STANDARDIZED", return_value_on_error=False)
def check_standardized(column_data: pd.Series, config: dict) -> bool:
    ''' feature extraction function for feature standardized
    - checks if the mean is 0 and the standard deviation is 1 (including a threshold)
    - uses the thresholds given by the feature extraction configuration
    '''
    threshold_mean = config.get("StandardizedThresholdMean", 0)
    threshold_mean = float(threshold_mean)
    threshold_std = config.get("StandardizedThresholdStd", 0)
    threshold_std = float(threshold_std)
    mean, std = column_data.mean(), column_data.std()
    if mean < -threshold_mean or mean > threshold_mean:
        return False
    if std < 1 - threshold_std or std > 1 + threshold_std:
        return False
    return True


@feature("NORMALIZED", return_value_on_error=False)
def check_normalized(column_data: pd.Series, config: dict) -> bool:
    ''' feature extraction function for feature normalized
    - checks if the min and max values are inside a given interval
    - uses the interval given by the feature extraction configuration
    '''
    target_min = config.get("NormalizedMin", -1)
    target_min = float(target_min)
    target_max = config.get("NormalizedMax", 1)
    target_max = float(target_max)
    column_min, column_max = column_data.min(), column_data.max()
    if column_min < target_min:
        return False
    if column_max < target_max:
        return False
    return True
