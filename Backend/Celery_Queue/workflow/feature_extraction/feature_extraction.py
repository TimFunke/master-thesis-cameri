# ---------------------------------------------------------------------------------------------------------
#   Definition of the Workflow for the feature extraction step
# ---------------------------------------------------------------------------------------------------------

import pandas as pd
from typing import List, Tuple
from os import path

from database import DBConnection
from database.tables import QueryTable, DatasetTable, DataAnnotationTable, FeatureTable, QuerySettingsTable
from database.enums import E_QUERY_STATUS
from .features import registered_features


class FeatureExtraction:
    ''' Class to perform the workflow of the feature extraction step '''

    def __init__(self, query_id: int, database_config: dict):
        self.query_id = query_id
        DBConnection.setup(
            host=database_config["Host"],
            port=database_config["Port"],
            user=database_config["Username"],
            password=database_config["Password"],
            db_name=database_config["DatabaseName"]
        )
        self.data_loaders = {
            "csv": self.loadDatasetCSV
        }

    def execute(self):
        ''' execution of the feature extraction workflow (highest level)
        - get database session
        - update query status to "RUNNING_FEATURE_EXTRACTION"
        - load and validate the dataset
        - load the extraction configuration
        - calculate (extract) the features
        - write the results to the database
        '''

        try:
            self.db_session = DBConnection.getSession()
            self.update_query_status()
            self.load_dataset_id()
            df, column_meta_info = self.load_dataset()
            df = self.validate_dataset(df, column_meta_info)
            configuration = self.get_extraction_configuration()
            features = self.calculate_features(df, configuration)
            self.write_features(features)

        except:
            raise

        finally:
            self.db_session.close()

    # ------------------------------------------------------------------------------------------------

    def update_query_status(self):
        ''' set the query status to "RUNNING_FEATURE_EXTRACTION" '''

        self.db_session.query(QueryTable)\
            .filter(QueryTable.QueryID == self.query_id)\
            .update({QueryTable.Status: E_QUERY_STATUS.RUNNING_FEATURE_EXTRACTION})

        self.db_session.commit()

    # ------------------------------------------------------------------------------------------------

    def load_dataset_id(self):
        ''' get the query from the database and extract the database id from it '''

        result: QueryTable = self.db_session\
            .query(QueryTable.DatasetID)\
            .filter(QueryTable.QueryID == self.query_id).first()

        if result is None:
            raise Exception(f"No dataset for query id {self.query_id} found")

        self.dataset_id = result.DatasetID

    # ------------------------------------------------------------------------------------------------

    def load_dataset(self) -> Tuple[pd.DataFrame, List[dict]]:
        ''' load the dataset with pandas '''

        # Read metadata of the dataset from the database
        dataset_metadata: DatasetTable = self.db_session\
            .query(DatasetTable.DataPath, DatasetTable.MetaInfo)\
            .filter(DatasetTable.DatasetID == self.dataset_id).first()

        # Check if file type is supported
        file_type = dataset_metadata.MetaInfo["file_type"]
        if file_type not in self.data_loaders.keys():
            raise Exception(
                f"file type {file_type} not yet supported. Please provide one of the following: {', '.join(self.data_loaders.keys())}")

        # Read the dataset with the corresponding data_loader function for the file type
        return self.data_loaders[file_type](dataset_metadata.DataPath, dataset_metadata.MetaInfo), dataset_metadata.MetaInfo["columns"]

    # ------------------------------------------------------------------------------------------------

    def loadDatasetCSV(self, data_path: str, meta_info: dict) -> pd.DataFrame:
        ''' Load CSV Dataset as pandas DataFrame '''

        df = pd.read_csv(path.expandvars(data_path), sep=meta_info["csv_delimiter"])
        return df

    # ------------------------------------------------------------------------------------------------

    def validate_dataset(self, df: pd.DataFrame, column_meta_info: List[dict]) -> pd.DataFrame:
        ''' validates the dataset
        - check if all columns exist (check vs the "column_meta_info" of the dataset)
        - remove all columns that are not in "column_meta_info"
        - order columns with the index given in "column_meta_info"
        '''

        # get ordered list of columns that are expected
        target_columns = [entry["name"] for entry in sorted(column_meta_info, key=lambda x: x["index"])]

        # check for missing columns
        missing_columns = []
        for target_column in target_columns:
            if target_column not in df.columns:
                missing_columns.append(target_column)

        if len(missing_columns) > 0:
            raise Exception(f"missing following columns: {', '.join(missing_columns)}")

        # return dataframe with ordered columns
        return df[target_columns]

    # ------------------------------------------------------------------------------------------------

    def get_extraction_configuration(self) -> dict:
        ''' reads the feature extraction config from the database '''

        query: QueryTable = self.db_session\
            .query(QueryTable)\
            .filter(QueryTable.QueryID == self.query_id).first()

        settings: QuerySettingsTable = query.settings
        return settings.__dict__

    # ------------------------------------------------------------------------------------------------

    def calculate_features(self, df: pd.DataFrame, configuration: dict) -> List[Tuple[int, str, bool]]:
        ''' calculated the registered features for all non ignore columns '''

        # load list of all columns from the data annotation
        column_annotations: List[DataAnnotationTable] = self.db_session\
            .query(DataAnnotationTable.ColumnIndex, DataAnnotationTable.IsIgnore)\
            .filter(DataAnnotationTable.DatasetID == self.dataset_id).all()

        # create result object
        calculated_features: List[Tuple[int, str, bool]] = []

        # loop over all columns
        for column_annotation in column_annotations:

            # skip is column is to ignore
            if column_annotation.IsIgnore == 1:
                continue

            # save current column to variable
            column_data = df.iloc[:, column_annotation.ColumnIndex]

            # loop over all registered features and calculate each feature for the current column
            for feature_identificator, feature_function in registered_features:
                feature_value = feature_function(column_data, configuration)
                calculated_features.append((column_annotation.ColumnIndex, feature_identificator, feature_value))

        return calculated_features

    # ------------------------------------------------------------------------------------------------

    def write_features(self, features: List[Tuple[int, str, bool]]):
        ''' writes the calculated features to the database '''

        # remove exisiting, if any
        self.db_session.query(FeatureTable).filter(FeatureTable.QueryID == self.query_id).delete()

        # create list of rows to add
        to_add = []
        for column_index, feature_identificator, feature_value in features:
            to_add.append(FeatureTable(QueryID=self.query_id, ColumnIndex=column_index,
                          FeatureType=feature_identificator, FeatureValue=feature_value))

        # add rows
        self.db_session.add_all(to_add)
        self.db_session.commit()
