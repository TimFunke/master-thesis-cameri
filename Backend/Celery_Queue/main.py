# ---------------------------------------------------------------------------------------------------------
#   Main file for creating and configuring the celery service to execute queries
# ---------------------------------------------------------------------------------------------------------

from celery import Celery, utils
from workflow import executeFeatureExtraction, executeInferenceSystem
from config import Config

# Load Config from file
Config.read_config("config.cfg")

# Define celery object with the given config for redis message broker
app = Celery('celery-app', broker=Config.RedisBroker.to_url())

# get the logger of the celery service
logger = utils.log.get_task_logger(__name__)


@app.task(bind=True, name="complete-workflow")
def execute_workflow(self, query_id: int):
    ''' celery task for execution a query by their id (param query_id) '''

    # Get config and log
    database_config = Config.Database.to_dict()
    inference_config = Config.Inference.to_dict()
    logger.info(f"query {query_id}: workflow started")

    # Start execution of feature extraction and log
    executeFeatureExtraction(query_id, database_config)
    logger.info(f"query {query_id}: done feature extraction")

    # Start execution of inference system and log
    executeInferenceSystem(query_id, database_config, inference_config)
    logger.info(f"query {query_id}: done inference")
