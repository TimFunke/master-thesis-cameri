(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendor"],{

/***/ "./assets/js/Common/Angular.js":
/*!*************************************!*\
  !*** ./assets/js/Common/Angular.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! angular */ "./node_modules/angular/index.js");
/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(angular__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Common */ "./assets/js/Common/Common.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/



var Eid = /*#__PURE__*/function () {
  /**
   * Initiate
   *
   * @param $http
   */
  function Eid($http) {
    _classCallCheck(this, Eid);

    this.$http = $http;
    this.loading = false;
  }
  /**
   * send normal POST
   *
   * @param url
   * @param input
   * @param event
   * @returns {Promise<any> | Promise<T> | *}
   */


  _createClass(Eid, [{
    key: "change",
    value: function change(url, input, event) {
      this.loading = true;

      if (!input) {
        input = {};
      }

      var headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      return this.send('POST', url, $.param(input), headers, event);
    }
    /**
     * Send a GET request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "get",
    value: function get(url, event) {
      return this.send('GET', url, null, null, event);
    }
    /**
     * Send a POST request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "post",
    value: function post(url, input, event) {
      return this.send('POST', url, input, null, event);
    }
    /**
     * Send a DELETE request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "delete",
    value: function _delete(url, event) {
      return this.send('DELETE', url, null, null, event);
    }
    /**
     * Send a PATCH request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "patch",
    value: function patch(url, input, event) {
      var headers = {
        'Content-Type': 'application/merge-patch+json',
        'accept': 'application/json'
      };
      return this.send('PATCH', url, input, headers, event);
    }
    /**
     * Send a request
     *
     * @param method
     * @param url
     * @param input
     * @param headers
     * @param event
     * @returns {Promise<any> | Promise<T> | *}
     */

  }, {
    key: "send",
    value: function send(method, url, input, headers, event) {
      var btn = null;

      if (!input) {
        input = {};
      }

      if (!headers) {
        headers = {
          'Content-Type': 'application/json',
          'accept': 'application/ld+json'
        };
      }

      if (event) {
        btn = $(event.currentTarget);
        btn.addClass('running');
        btn.prop("disabled", true);
      }

      return this.$http({
        method: method,
        url: url,
        data: input,
        headers: headers
      })["catch"](function (error) {
        if (error.data.detail) {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', error.data.detail);
        } else {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', 'Action cannot be performed');
        }
      })["finally"](function () {
        $(document).trigger('loading.stop');

        if (btn) {
          var modal = btn.data('close');

          if (modal) {
            $('#' + modal).modal('hide');
          }

          btn.removeClass('running');
          btn.prop("disabled", false);
        }
      });
    }
  }]);

  return Eid;
}();

var app = angular__WEBPACK_IMPORTED_MODULE_0___default.a.module('Ecosystem', []) // change Angular default {{ to {[{ to avoid conflict with Twig
.config(['$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false,
    rewriteLinks: false
  });
}]).filter('unsafe', ['$sce', function ($sce) {
  return function (val) {
    return $sce.trustAsHtml(val);
  };
}]).filter('range', function () {
  return function (input, total) {
    total = parseInt(total);

    for (var i = 0; i < total; i++) {
      input.push(i);
    }

    return input;
  };
}).service('eid', ['$http', Eid]).directive('form', ['$location', function ($location) {
  return {
    restrict: 'E',
    priority: 999,
    compile: function compile() {
      return {
        pre: function pre(scope, element, attrs) {
          if (attrs.noaction === '') return;

          if (attrs.action === undefined || attrs.action === '') {
            attrs.action = $location.absUrl();
          }
        }
      };
    }
  };
}]).directive('initTooltip', function () {
  return {
    restrict: 'A',
    link: function link(scope, element, attrs) {
      $(element).tooltip({
        trigger: 'hover'
      });
    }
  };
});
/* harmony default export */ __webpack_exports__["default"] = (app);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/Library/DataTables.js":
/*!*****************************************!*\
  !*** ./assets/js/Library/DataTables.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var datatables_net__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! datatables.net */ "./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var datatables_net__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(datatables_net__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var datatables_net_bs4_css_dataTables_bootstrap4_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! datatables.net-bs4/css/dataTables.bootstrap4.min.css */ "./node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css");
/* harmony import */ var datatables_net_bs4_css_dataTables_bootstrap4_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(datatables_net_bs4_css_dataTables_bootstrap4_min_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var datatables_net_bs4_js_dataTables_bootstrap4_min_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! datatables.net-bs4/js/dataTables.bootstrap4.min.js */ "./node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js");
/* harmony import */ var datatables_net_bs4_js_dataTables_bootstrap4_min_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(datatables_net_bs4_js_dataTables_bootstrap4_min_js__WEBPACK_IMPORTED_MODULE_2__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/






/***/ }),

/***/ "./assets/js/Library/JsTree.js":
/*!*************************************!*\
  !*** ./assets/js/Library/JsTree.js ***!
  \*************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jstree__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jstree */ "./node_modules/jstree/dist/jstree.js");
/* harmony import */ var jstree__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jstree__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jstree_dist_themes_default_style_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jstree/dist/themes/default/style.min.css */ "./node_modules/jstree/dist/themes/default/style.min.css");
/* harmony import */ var jstree_dist_themes_default_style_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jstree_dist_themes_default_style_min_css__WEBPACK_IMPORTED_MODULE_1__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/





/***/ }),

/***/ "./assets/js/vendor.js":
/*!*****************************!*\
  !*** ./assets/js/vendor.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Common_Common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Common/Common */ "./assets/js/Common/Common.js");
/* harmony import */ var _Common_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Common/Layout */ "./assets/js/Common/Layout.js");
/* harmony import */ var _Common_Angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Common/Angular */ "./assets/js/Common/Angular.js");
/* harmony import */ var _Library_DataTables__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Library/DataTables */ "./assets/js/Library/DataTables.js");
/* harmony import */ var _Library_Selectize__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Library/Selectize */ "./assets/js/Library/Selectize.js");
/* harmony import */ var _Library_JsTree__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Library/JsTree */ "./assets/js/Library/JsTree.js");
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
// require jQuery normally
 // for inline script

window.$ = jquery__WEBPACK_IMPORTED_MODULE_0___default.a; // Init Components








/***/ })

},[["./assets/js/vendor.js","runtime","vendors~Admin/EmailCtrl~Admin/FileAudit~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Recor~b3f38238","vendors~Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/SearchCtrl~BigData/Se~6c26450f","vendors~Archive/CategorizationCtrl~Archive/SearchCtrl~BigData/SearchCtrl~Competence/CategoriesAssign~65b3e25b","vendors~Admin/FileAuditCtrl~Archive/Record~Authentication/User~Authentication/UserGroup~Content/Cont~c2893b9d","vendors~Archive/Record~Authentication/User~Common/Layout~Content/ContentList~Content/Display~Core/Fe~2684777e","Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Record~Archive/SearchCtrl~Aut~d45f3fc6","Archive/Record~Authentication/User~Common/Layout~Content/ContentList~Content/Display~Core/Feedback~E~7511650d","Admin/EmailCtrl~Common/PrototypeCtrl~Content/IdentifierCtrl~Content/PwDSickActReportCtrl~Content/PwD~d97d5f95"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQ29tbW9uL0FuZ3VsYXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL0xpYnJhcnkvRGF0YVRhYmxlcy5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvTGlicmFyeS9Kc1RyZWUuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3ZlbmRvci5qcyJdLCJuYW1lcyI6WyJFaWQiLCIkaHR0cCIsImxvYWRpbmciLCJ1cmwiLCJpbnB1dCIsImV2ZW50IiwiaGVhZGVycyIsInNlbmQiLCIkIiwicGFyYW0iLCJtZXRob2QiLCJidG4iLCJjdXJyZW50VGFyZ2V0IiwiYWRkQ2xhc3MiLCJwcm9wIiwiZGF0YSIsImVycm9yIiwiZGV0YWlsIiwiQ29tbW9uIiwic2hvd0FsZXJ0RGlhbG9nIiwiZG9jdW1lbnQiLCJ0cmlnZ2VyIiwibW9kYWwiLCJyZW1vdmVDbGFzcyIsImFwcCIsImFuZ3VsYXIiLCJtb2R1bGUiLCJjb25maWciLCIkaW50ZXJwb2xhdGVQcm92aWRlciIsIiRsb2NhdGlvblByb3ZpZGVyIiwic3RhcnRTeW1ib2wiLCJlbmRTeW1ib2wiLCJodG1sNU1vZGUiLCJlbmFibGVkIiwicmVxdWlyZUJhc2UiLCJyZXdyaXRlTGlua3MiLCJmaWx0ZXIiLCIkc2NlIiwidmFsIiwidHJ1c3RBc0h0bWwiLCJ0b3RhbCIsInBhcnNlSW50IiwiaSIsInB1c2giLCJzZXJ2aWNlIiwiZGlyZWN0aXZlIiwiJGxvY2F0aW9uIiwicmVzdHJpY3QiLCJwcmlvcml0eSIsImNvbXBpbGUiLCJwcmUiLCJzY29wZSIsImVsZW1lbnQiLCJhdHRycyIsIm5vYWN0aW9uIiwiYWN0aW9uIiwidW5kZWZpbmVkIiwiYWJzVXJsIiwibGluayIsInRvb2x0aXAiLCJ3aW5kb3ciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBOztJQUVNQSxHO0FBQ0Y7Ozs7O0FBS0EsZUFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUNmLFNBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNBLFNBQUtDLE9BQUwsR0FBZSxLQUFmO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7OzJCQVFPQyxHLEVBQUtDLEssRUFBT0MsSyxFQUFPO0FBQ3RCLFdBQUtILE9BQUwsR0FBZSxJQUFmOztBQUVBLFVBQUksQ0FBQ0UsS0FBTCxFQUFZO0FBQ1JBLGFBQUssR0FBRyxFQUFSO0FBQ0g7O0FBRUQsVUFBSUUsT0FBTyxHQUFHO0FBQ1Ysd0JBQWdCO0FBRE4sT0FBZDtBQUlBLGFBQU8sS0FBS0MsSUFBTCxDQUFVLE1BQVYsRUFBa0JKLEdBQWxCLEVBQXVCSyxDQUFDLENBQUNDLEtBQUYsQ0FBUUwsS0FBUixDQUF2QixFQUF1Q0UsT0FBdkMsRUFBZ0RELEtBQWhELENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7O3dCQU9JRixHLEVBQUtFLEssRUFBTztBQUNaLGFBQU8sS0FBS0UsSUFBTCxDQUFVLEtBQVYsRUFBaUJKLEdBQWpCLEVBQXNCLElBQXRCLEVBQTRCLElBQTVCLEVBQWtDRSxLQUFsQyxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7eUJBUUtGLEcsRUFBS0MsSyxFQUFPQyxLLEVBQU87QUFDcEIsYUFBTyxLQUFLRSxJQUFMLENBQVUsTUFBVixFQUFrQkosR0FBbEIsRUFBdUJDLEtBQXZCLEVBQThCLElBQTlCLEVBQW9DQyxLQUFwQyxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs0QkFPT0YsRyxFQUFLRSxLLEVBQU87QUFDZixhQUFPLEtBQUtFLElBQUwsQ0FBVSxRQUFWLEVBQW9CSixHQUFwQixFQUF5QixJQUF6QixFQUErQixJQUEvQixFQUFxQ0UsS0FBckMsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7OzBCQVFNRixHLEVBQUtDLEssRUFBT0MsSyxFQUFPO0FBQ3JCLFVBQUlDLE9BQU8sR0FBRztBQUNWLHdCQUFnQiw4QkFETjtBQUVWLGtCQUFVO0FBRkEsT0FBZDtBQUtBLGFBQU8sS0FBS0MsSUFBTCxDQUFVLE9BQVYsRUFBbUJKLEdBQW5CLEVBQXdCQyxLQUF4QixFQUErQkUsT0FBL0IsRUFBd0NELEtBQXhDLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7O3lCQVVLSyxNLEVBQVFQLEcsRUFBS0MsSyxFQUFPRSxPLEVBQVNELEssRUFBTztBQUNyQyxVQUFJTSxHQUFHLEdBQUcsSUFBVjs7QUFFQSxVQUFJLENBQUNQLEtBQUwsRUFBWTtBQUNSQSxhQUFLLEdBQUcsRUFBUjtBQUNIOztBQUVELFVBQUksQ0FBQ0UsT0FBTCxFQUFjO0FBQ1ZBLGVBQU8sR0FBRztBQUNOLDBCQUFnQixrQkFEVjtBQUVOLG9CQUFVO0FBRkosU0FBVjtBQUlIOztBQUVELFVBQUlELEtBQUosRUFBVztBQUNQTSxXQUFHLEdBQUdILENBQUMsQ0FBQ0gsS0FBSyxDQUFDTyxhQUFQLENBQVA7QUFDQUQsV0FBRyxDQUFDRSxRQUFKLENBQWEsU0FBYjtBQUNBRixXQUFHLENBQUNHLElBQUosQ0FBUyxVQUFULEVBQXFCLElBQXJCO0FBQ0g7O0FBRUQsYUFBTyxLQUFLYixLQUFMLENBQVc7QUFDZFMsY0FBTSxFQUFFQSxNQURNO0FBRWRQLFdBQUcsRUFBRUEsR0FGUztBQUdkWSxZQUFJLEVBQUVYLEtBSFE7QUFJZEUsZUFBTyxFQUFFQTtBQUpLLE9BQVgsV0FLRSxVQUFDVSxLQUFELEVBQVc7QUFDaEIsWUFBSUEsS0FBSyxDQUFDRCxJQUFOLENBQVdFLE1BQWYsRUFBdUI7QUFDbkJDLHlEQUFNLENBQUNDLGVBQVAsQ0FBdUIsUUFBdkIsRUFBaUNILEtBQUssQ0FBQ0QsSUFBTixDQUFXRSxNQUE1QztBQUNILFNBRkQsTUFFTztBQUNIQyx5REFBTSxDQUFDQyxlQUFQLENBQXVCLFFBQXZCLEVBQWlDLDRCQUFqQztBQUNIO0FBQ0osT0FYTSxhQVdJLFlBQU07QUFDYlgsU0FBQyxDQUFDWSxRQUFELENBQUQsQ0FBWUMsT0FBWixDQUFvQixjQUFwQjs7QUFDQSxZQUFJVixHQUFKLEVBQVM7QUFDTCxjQUFNVyxLQUFLLEdBQUdYLEdBQUcsQ0FBQ0ksSUFBSixDQUFTLE9BQVQsQ0FBZDs7QUFDQSxjQUFJTyxLQUFKLEVBQVc7QUFDUGQsYUFBQyxDQUFDLE1BQU1jLEtBQVAsQ0FBRCxDQUFlQSxLQUFmLENBQXFCLE1BQXJCO0FBQ0g7O0FBRURYLGFBQUcsQ0FBQ1ksV0FBSixDQUFnQixTQUFoQjtBQUNBWixhQUFHLENBQUNHLElBQUosQ0FBUyxVQUFULEVBQXFCLEtBQXJCO0FBQ0g7QUFDSixPQXRCTSxDQUFQO0FBdUJIOzs7Ozs7QUFHTCxJQUFNVSxHQUFHLEdBQUdDLDhDQUFPLENBQUNDLE1BQVIsQ0FBZSxXQUFmLEVBQTRCLEVBQTVCLEVBQ1I7QUFEUSxDQUVQQyxNQUZPLENBRUEsQ0FBQyxzQkFBRCxFQUF5QixtQkFBekIsRUFBOEMsVUFBQ0Msb0JBQUQsRUFBdUJDLGlCQUF2QixFQUE2QztBQUMvRkQsc0JBQW9CLENBQUNFLFdBQXJCLENBQWlDLEtBQWpDLEVBQXdDQyxTQUF4QyxDQUFrRCxLQUFsRDtBQUNBRixtQkFBaUIsQ0FBQ0csU0FBbEIsQ0FBNEI7QUFDeEJDLFdBQU8sRUFBRSxJQURlO0FBRXhCQyxlQUFXLEVBQUUsS0FGVztBQUd4QkMsZ0JBQVksRUFBRTtBQUhVLEdBQTVCO0FBS0gsQ0FQTyxDQUZBLEVBVVBDLE1BVk8sQ0FVQSxRQVZBLEVBVVUsQ0FBQyxNQUFELEVBQVMsVUFBQ0MsSUFBRCxFQUFVO0FBQ2pDLFNBQU8sVUFBQ0MsR0FBRCxFQUFTO0FBQ1osV0FBT0QsSUFBSSxDQUFDRSxXQUFMLENBQWlCRCxHQUFqQixDQUFQO0FBQ0gsR0FGRDtBQUdILENBSmlCLENBVlYsRUFlUEYsTUFmTyxDQWVBLE9BZkEsRUFlUyxZQUFNO0FBQ25CLFNBQU8sVUFBQ2hDLEtBQUQsRUFBUW9DLEtBQVIsRUFBa0I7QUFDckJBLFNBQUssR0FBR0MsUUFBUSxDQUFDRCxLQUFELENBQWhCOztBQUVBLFNBQUssSUFBSUUsQ0FBQyxHQUFDLENBQVgsRUFBY0EsQ0FBQyxHQUFDRixLQUFoQixFQUF1QkUsQ0FBQyxFQUF4QixFQUE0QjtBQUN4QnRDLFdBQUssQ0FBQ3VDLElBQU4sQ0FBV0QsQ0FBWDtBQUNIOztBQUVELFdBQU90QyxLQUFQO0FBQ0gsR0FSRDtBQVNILENBekJPLEVBMEJQd0MsT0ExQk8sQ0EwQkMsS0ExQkQsRUEwQlEsQ0FBQyxPQUFELEVBQVU1QyxHQUFWLENBMUJSLEVBMkJQNkMsU0EzQk8sQ0EyQkcsTUEzQkgsRUEyQlcsQ0FBQyxXQUFELEVBQWMsVUFBQ0MsU0FBRCxFQUFlO0FBQzVDLFNBQU87QUFDSEMsWUFBUSxFQUFDLEdBRE47QUFFSEMsWUFBUSxFQUFFLEdBRlA7QUFHSEMsV0FBTyxFQUFFLG1CQUFNO0FBQ1gsYUFBTztBQUNIQyxXQUFHLEVBQUUsYUFBQ0MsS0FBRCxFQUFRQyxPQUFSLEVBQWlCQyxLQUFqQixFQUEyQjtBQUM1QixjQUFJQSxLQUFLLENBQUNDLFFBQU4sS0FBbUIsRUFBdkIsRUFBMkI7O0FBQzNCLGNBQUlELEtBQUssQ0FBQ0UsTUFBTixLQUFpQkMsU0FBakIsSUFBOEJILEtBQUssQ0FBQ0UsTUFBTixLQUFpQixFQUFuRCxFQUFzRDtBQUNsREYsaUJBQUssQ0FBQ0UsTUFBTixHQUFlVCxTQUFTLENBQUNXLE1BQVYsRUFBZjtBQUNIO0FBQ0o7QUFORSxPQUFQO0FBUUg7QUFaRSxHQUFQO0FBY0gsQ0Fma0IsQ0EzQlgsRUEyQ1BaLFNBM0NPLENBMkNHLGFBM0NILEVBMkNrQixZQUFNO0FBQzVCLFNBQU87QUFDSEUsWUFBUSxFQUFFLEdBRFA7QUFFSFcsUUFBSSxFQUFFLGNBQUNQLEtBQUQsRUFBUUMsT0FBUixFQUFpQkMsS0FBakIsRUFBMkI7QUFDN0I3QyxPQUFDLENBQUM0QyxPQUFELENBQUQsQ0FBV08sT0FBWCxDQUFtQjtBQUNmdEMsZUFBTyxFQUFHO0FBREssT0FBbkI7QUFHSDtBQU5FLEdBQVA7QUFRSCxDQXBETyxDQUFaO0FBdURlRyxrRUFBZixFOzs7Ozs7Ozs7Ozs7O0FDcE5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUFjYTs7QUFFYjtBQUNBOzs7Ozs7Ozs7Ozs7O0FDakJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQWNhOztBQUViOzs7Ozs7Ozs7Ozs7O0FDaEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FBY0E7Q0FFQTs7QUFDQW9DLE1BQU0sQ0FBQ3BELENBQVAsR0FBV0EsNkNBQVgsQyxDQUVBOztBQUNBO0FBRUE7QUFFQTtBQUVBO0FBRUEiLCJmaWxlIjoidmVuZG9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKiAgQ29weXJpZ2h0IG5vdGljZVxyXG4gKlxyXG4gKiAgKGMpIDIwMTggRlRLIGUuVi4gPGZ0ay5kZT5cclxuICogIEFsbCByaWdodHMgcmVzZXJ2ZWRcclxuICpcclxuICogIFRoaXMgc2NyaXB0IGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAqICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gKiAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gKiAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuICpcclxuICogIFRoaXMgY29weXJpZ2h0IG5vdGljZSBNVVNUIEFQUEVBUiBpbiBhbGwgY29waWVzIG9mIHRoZSBzY3JpcHQhXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG5pbXBvcnQgYW5ndWxhciBmcm9tICdhbmd1bGFyJztcclxuaW1wb3J0IENvbW1vbiBmcm9tICcuL0NvbW1vbic7XHJcblxyXG5jbGFzcyBFaWQge1xyXG4gICAgLyoqXHJcbiAgICAgKiBJbml0aWF0ZVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSAkaHR0cFxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcigkaHR0cCkge1xyXG4gICAgICAgIHRoaXMuJGh0dHAgPSAkaHR0cDtcclxuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIHNlbmQgbm9ybWFsIFBPU1RcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gaW5wdXRcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PiB8IFByb21pc2U8VD4gfCAqfVxyXG4gICAgICovXHJcbiAgICBjaGFuZ2UodXJsLCBpbnB1dCwgZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xyXG5cclxuICAgICAgICBpZiAoIWlucHV0KSB7XHJcbiAgICAgICAgICAgIGlucHV0ID0ge307XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaGVhZGVycyA9IHtcclxuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VuZCgnUE9TVCcsIHVybCwgJC5wYXJhbShpbnB1dCksIGhlYWRlcnMsIGV2ZW50KVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBhIEdFVCByZXF1ZXN0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fFByb21pc2U8VD58Kn1cclxuICAgICAqL1xyXG4gICAgZ2V0KHVybCwgZXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZW5kKCdHRVQnLCB1cmwsIG51bGwsIG51bGwsIGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbmQgYSBQT1NUIHJlcXVlc3Qgd2l0aCBKU09OIGRhdGFcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gaW5wdXRcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PnxQcm9taXNlPFQ+fCp9XHJcbiAgICAgKi9cclxuICAgIHBvc3QodXJsLCBpbnB1dCwgZXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZW5kKCdQT1NUJywgdXJsLCBpbnB1dCwgbnVsbCwgZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBhIERFTEVURSByZXF1ZXN0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fFByb21pc2U8VD58Kn1cclxuICAgICAqL1xyXG4gICAgZGVsZXRlKHVybCwgZXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZW5kKCdERUxFVEUnLCB1cmwsIG51bGwsIG51bGwsIGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbmQgYSBQQVRDSCByZXF1ZXN0IHdpdGggSlNPTiBkYXRhXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGlucHV0XHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT58UHJvbWlzZTxUPnwqfVxyXG4gICAgICovXHJcbiAgICBwYXRjaCh1cmwsIGlucHV0LCBldmVudCkge1xyXG4gICAgICAgIGxldCBoZWFkZXJzID0ge1xyXG4gICAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL21lcmdlLXBhdGNoK2pzb24nLFxyXG4gICAgICAgICAgICAnYWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbmQoJ1BBVENIJywgdXJsLCBpbnB1dCwgaGVhZGVycywgZXZlbnQpXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIGEgcmVxdWVzdFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBtZXRob2RcclxuICAgICAqIEBwYXJhbSB1cmxcclxuICAgICAqIEBwYXJhbSBpbnB1dFxyXG4gICAgICogQHBhcmFtIGhlYWRlcnNcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PiB8IFByb21pc2U8VD4gfCAqfVxyXG4gICAgICovXHJcbiAgICBzZW5kKG1ldGhvZCwgdXJsLCBpbnB1dCwgaGVhZGVycywgZXZlbnQpIHtcclxuICAgICAgICBsZXQgYnRuID0gbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKCFpbnB1dCkge1xyXG4gICAgICAgICAgICBpbnB1dCA9IHt9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFoZWFkZXJzKSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxyXG4gICAgICAgICAgICAgICAgJ2FjY2VwdCc6ICdhcHBsaWNhdGlvbi9sZCtqc29uJyxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgICAgICBidG4gPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xyXG4gICAgICAgICAgICBidG4uYWRkQ2xhc3MoJ3J1bm5pbmcnKTtcclxuICAgICAgICAgICAgYnRuLnByb3AoXCJkaXNhYmxlZFwiLCB0cnVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLiRodHRwKHtcclxuICAgICAgICAgICAgbWV0aG9kOiBtZXRob2QsXHJcbiAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICBkYXRhOiBpbnB1dCxcclxuICAgICAgICAgICAgaGVhZGVyczogaGVhZGVyc1xyXG4gICAgICAgIH0pLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZXJyb3IuZGF0YS5kZXRhaWwpIHtcclxuICAgICAgICAgICAgICAgIENvbW1vbi5zaG93QWxlcnREaWFsb2coJ2RhbmdlcicsIGVycm9yLmRhdGEuZGV0YWlsKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIENvbW1vbi5zaG93QWxlcnREaWFsb2coJ2RhbmdlcicsICdBY3Rpb24gY2Fubm90IGJlIHBlcmZvcm1lZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICQoZG9jdW1lbnQpLnRyaWdnZXIoJ2xvYWRpbmcuc3RvcCcpO1xyXG4gICAgICAgICAgICBpZiAoYnRuKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtb2RhbCA9IGJ0bi5kYXRhKCdjbG9zZScpO1xyXG4gICAgICAgICAgICAgICAgaWYgKG1vZGFsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnIycgKyBtb2RhbCkubW9kYWwoJ2hpZGUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBidG4ucmVtb3ZlQ2xhc3MoJ3J1bm5pbmcnKTtcclxuICAgICAgICAgICAgICAgIGJ0bi5wcm9wKFwiZGlzYWJsZWRcIiwgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IGFwcCA9IGFuZ3VsYXIubW9kdWxlKCdFY29zeXN0ZW0nLCBbXSlcclxuICAgIC8vIGNoYW5nZSBBbmd1bGFyIGRlZmF1bHQge3sgdG8ge1t7IHRvIGF2b2lkIGNvbmZsaWN0IHdpdGggVHdpZ1xyXG4gICAgLmNvbmZpZyhbJyRpbnRlcnBvbGF0ZVByb3ZpZGVyJywgJyRsb2NhdGlvblByb3ZpZGVyJywgKCRpbnRlcnBvbGF0ZVByb3ZpZGVyLCAkbG9jYXRpb25Qcm92aWRlcikgPT4ge1xyXG4gICAgICAgICRpbnRlcnBvbGF0ZVByb3ZpZGVyLnN0YXJ0U3ltYm9sKCd7W3snKS5lbmRTeW1ib2woJ31dfScpO1xyXG4gICAgICAgICRsb2NhdGlvblByb3ZpZGVyLmh0bWw1TW9kZSh7XHJcbiAgICAgICAgICAgIGVuYWJsZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHJlcXVpcmVCYXNlOiBmYWxzZSxcclxuICAgICAgICAgICAgcmV3cml0ZUxpbmtzOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgfV0pXHJcbiAgICAuZmlsdGVyKCd1bnNhZmUnLCBbJyRzY2UnLCAoJHNjZSkgPT4ge1xyXG4gICAgICAgIHJldHVybiAodmFsKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiAkc2NlLnRydXN0QXNIdG1sKHZhbCk7XHJcbiAgICAgICAgfTtcclxuICAgIH1dKVxyXG4gICAgLmZpbHRlcigncmFuZ2UnLCAoKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIChpbnB1dCwgdG90YWwpID0+IHtcclxuICAgICAgICAgICAgdG90YWwgPSBwYXJzZUludCh0b3RhbCk7XHJcblxyXG4gICAgICAgICAgICBmb3IgKGxldCBpPTA7IGk8dG90YWw7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgaW5wdXQucHVzaChpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGlucHV0O1xyXG4gICAgICAgIH07XHJcbiAgICB9KVxyXG4gICAgLnNlcnZpY2UoJ2VpZCcsIFsnJGh0dHAnLCBFaWRdKVxyXG4gICAgLmRpcmVjdGl2ZSgnZm9ybScsIFsnJGxvY2F0aW9uJywgKCRsb2NhdGlvbikgPT4ge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJlc3RyaWN0OidFJyxcclxuICAgICAgICAgICAgcHJpb3JpdHk6IDk5OSxcclxuICAgICAgICAgICAgY29tcGlsZTogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICBwcmU6IChzY29wZSwgZWxlbWVudCwgYXR0cnMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGF0dHJzLm5vYWN0aW9uID09PSAnJykgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXR0cnMuYWN0aW9uID09PSB1bmRlZmluZWQgfHwgYXR0cnMuYWN0aW9uID09PSAnJyl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRycy5hY3Rpb24gPSAkbG9jYXRpb24uYWJzVXJsKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XSlcclxuICAgIC5kaXJlY3RpdmUoJ2luaXRUb29sdGlwJywgKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnQScsXHJcbiAgICAgICAgICAgIGxpbms6IChzY29wZSwgZWxlbWVudCwgYXR0cnMpID0+IHtcclxuICAgICAgICAgICAgICAgICQoZWxlbWVudCkudG9vbHRpcCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJpZ2dlciA6ICdob3ZlcidcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgIH0pXHJcbjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGFwcDsiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnZGF0YXRhYmxlcy5uZXQnO1xyXG5pbXBvcnQgJ2RhdGF0YWJsZXMubmV0LWJzNC9jc3MvZGF0YVRhYmxlcy5ib290c3RyYXA0Lm1pbi5jc3MnO1xyXG5pbXBvcnQgJ2RhdGF0YWJsZXMubmV0LWJzNC9qcy9kYXRhVGFibGVzLmJvb3RzdHJhcDQubWluLmpzJzsiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnanN0cmVlJztcclxuaW1wb3J0ICdqc3RyZWUvZGlzdC90aGVtZXMvZGVmYXVsdC9zdHlsZS5taW4uY3NzJzsiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbi8vIHJlcXVpcmUgalF1ZXJ5IG5vcm1hbGx5XHJcbmltcG9ydCAkIGZyb20gJ2pxdWVyeSc7XHJcbi8vIGZvciBpbmxpbmUgc2NyaXB0XHJcbndpbmRvdy4kID0gJDtcclxuXHJcbi8vIEluaXQgQ29tcG9uZW50c1xyXG5pbXBvcnQgJy4vQ29tbW9uL0NvbW1vbic7XHJcblxyXG5pbXBvcnQgJy4vQ29tbW9uL0xheW91dCc7XHJcblxyXG5pbXBvcnQgJy4vQ29tbW9uL0FuZ3VsYXInO1xyXG5cclxuaW1wb3J0ICcuL0xpYnJhcnkvRGF0YVRhYmxlcyc7XHJcblxyXG5pbXBvcnQgJy4vTGlicmFyeS9TZWxlY3RpemUnO1xyXG5cclxuaW1wb3J0ICcuL0xpYnJhcnkvSnNUcmVlJzsiXSwic291cmNlUm9vdCI6IiJ9