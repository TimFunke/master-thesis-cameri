(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Archive/CategorizationCtrl"],{

/***/ "./assets/js/Archive/CategorizationCtrl.js":
/*!*************************************************!*\
  !*** ./assets/js/Archive/CategorizationCtrl.js ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _Library_JsTree__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Library/JsTree */ "./assets/js/Library/JsTree.js");
/* harmony import */ var _Common_Angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Common/Angular */ "./assets/js/Common/Angular.js");
/* harmony import */ var _Common_Common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Common/Common */ "./assets/js/Common/Common.js");
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016-2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }





var CategorizationController = /*#__PURE__*/function () {
  function CategorizationController($scope, $timeout, eid) {
    var _this = this;

    _classCallCheck(this, CategorizationController);

    this.eid = eid;
    this.treeDiv = $('#archive-categorization-tree');
    this.messageDiv = $('#archive-categorization-msg');
    this.tree = null;
    this.loading = true;
    this.taxonomy = '';
    this.search = '';
    this.record = 0;
    $scope.$watch('categorization.taxonomy', function (newValue, oldValue) {
      if (newValue && newValue !== oldValue) {
        _this.loading = true;
        var url = Routing.generate('archive_categorization_select', {
          id: _this.record,
          taxonomy: newValue
        });

        _this.eid.change(url).then(function (response) {
          _this.search = '';

          if (response.data.success) {
            _this.tree.settings.core.data = response.data.msg; // "load_node" keep checked box
            // meanwhile "refresh" keeps last state of tree

            _this.tree.load_node('#', function () {
              $timeout(function () {
                _this.loading = false;
              }, 0);
            });
          } else {
            _this.messageDiv.trigger('action.fail', ['Dialog cannot be loaded']);
          }
        });
      }
    });
    $scope.$watch('categorization.search', function (newValue, oldValue) {
      if (newValue && newValue !== oldValue) {
        if (newValue.length > 2) {
          _this.tree.search(newValue);
        } else {
          _this.tree.search('');
        }
      }
    });
    _Common_Common__WEBPACK_IMPORTED_MODULE_2__["default"].initMessage(this.messageDiv);
    this.initTree();
  }

  _createClass(CategorizationController, [{
    key: "assignAction",
    value: function assignAction() {
      var _this2 = this;

      if (!this.taxonomy) {
        return;
      }

      var selected = [];
      var disabled = [];
      $.each(this.tree.get_selected(true), function (index, node) {
        selected.push(node.data.uuid);
      });
      this.treeDiv.find('.jstree-disabled').each(function (index, item) {
        var node = _this2.tree.get_node($(item).parent().attr('id'));

        disabled.push(node.data.uuid);
      });
      var params = {
        selected: selected,
        disabled: disabled
      };
      var url = Routing.generate('archive_categorization_update', {
        id: this.record,
        taxonomy: this.taxonomy
      });
      this.eid.change(url, params).then(function (response) {
        if (response.data.success) {
          _this2.messageDiv.trigger('action.success', ['Your setting was successfully saved', true]);
        } else {
          _this2.messageDiv.trigger('action.fail', ['Your setting cannot be saved', true]);
        }
      });
    }
  }, {
    key: "initTree",
    value: function initTree() {
      var _this3 = this;

      var icon = _Common_Common__WEBPACK_IMPORTED_MODULE_2__["default"].iconPath();
      this.treeDiv.jstree({
        "plugins": ["types", "checkbox", "contextmenu", "search"],
        "core": {
          "themes": {
            "dots": false // no connecting dots between dots

          },
          "data": {}
        },
        "types": _Common_Common__WEBPACK_IMPORTED_MODULE_2__["default"].getTaxonomyIcon(),
        "checkbox": {
          "three_state": false,
          "cascade": 'undetermined',
          //"whole_node": false,
          "tie_selection": true
        },
        "contextmenu": {
          items: function items(node) {
            // The default set of all items
            var items = {
              enable: {
                "label": "Applicable",
                "icon": icon + "plugin.png",
                "action": function action() {
                  _this3.tree.enable_node(node);
                }
              },
              disable: {
                "label": "Not Applicable",
                "icon": icon + "plugin_disabled.png",
                "action": function action() {
                  _this3.tree.deselect_node(node);

                  _this3.tree.disable_node(node);

                  _this3.tree.close_node(node);
                }
              }
            };

            if (_this3.tree.is_disabled(node)) {
              delete items.disable;
            } else {
              delete items.enable;
            }

            return items;
          }
        }
      });
      this.tree = this.treeDiv.jstree(true);
      this.treeDiv.bind('deselect_node.jstree', function (event, data) {
        $.each(_this3.tree.get_selected(true), function (index, node) {
          if (node.data.uuid === data.node.data.uuid) {
            _this3.tree.deselect_node(node);
          }
        });
      }).bind('open_node.jstree', function (event, data) {
        var node = data.node;

        if (_this3.tree.is_disabled(node)) {
          _this3.tree.close_node(node);
        }
      });
    }
  }]);

  return CategorizationController;
}();

_Common_Angular__WEBPACK_IMPORTED_MODULE_1__["default"].controller('CategorizationController', ['$scope', '$timeout', 'eid', CategorizationController]);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/Common/Angular.js":
/*!*************************************!*\
  !*** ./assets/js/Common/Angular.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! angular */ "./node_modules/angular/index.js");
/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(angular__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Common */ "./assets/js/Common/Common.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/



var Eid = /*#__PURE__*/function () {
  /**
   * Initiate
   *
   * @param $http
   */
  function Eid($http) {
    _classCallCheck(this, Eid);

    this.$http = $http;
    this.loading = false;
  }
  /**
   * send normal POST
   *
   * @param url
   * @param input
   * @param event
   * @returns {Promise<any> | Promise<T> | *}
   */


  _createClass(Eid, [{
    key: "change",
    value: function change(url, input, event) {
      this.loading = true;

      if (!input) {
        input = {};
      }

      var headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      return this.send('POST', url, $.param(input), headers, event);
    }
    /**
     * Send a GET request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "get",
    value: function get(url, event) {
      return this.send('GET', url, null, null, event);
    }
    /**
     * Send a POST request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "post",
    value: function post(url, input, event) {
      return this.send('POST', url, input, null, event);
    }
    /**
     * Send a DELETE request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "delete",
    value: function _delete(url, event) {
      return this.send('DELETE', url, null, null, event);
    }
    /**
     * Send a PATCH request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "patch",
    value: function patch(url, input, event) {
      var headers = {
        'Content-Type': 'application/merge-patch+json',
        'accept': 'application/json'
      };
      return this.send('PATCH', url, input, headers, event);
    }
    /**
     * Send a request
     *
     * @param method
     * @param url
     * @param input
     * @param headers
     * @param event
     * @returns {Promise<any> | Promise<T> | *}
     */

  }, {
    key: "send",
    value: function send(method, url, input, headers, event) {
      var btn = null;

      if (!input) {
        input = {};
      }

      if (!headers) {
        headers = {
          'Content-Type': 'application/json',
          'accept': 'application/ld+json'
        };
      }

      if (event) {
        btn = $(event.currentTarget);
        btn.addClass('running');
        btn.prop("disabled", true);
      }

      return this.$http({
        method: method,
        url: url,
        data: input,
        headers: headers
      })["catch"](function (error) {
        if (error.data.detail) {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', error.data.detail);
        } else {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', 'Action cannot be performed');
        }
      })["finally"](function () {
        $(document).trigger('loading.stop');

        if (btn) {
          var modal = btn.data('close');

          if (modal) {
            $('#' + modal).modal('hide');
          }

          btn.removeClass('running');
          btn.prop("disabled", false);
        }
      });
    }
  }]);

  return Eid;
}();

var app = angular__WEBPACK_IMPORTED_MODULE_0___default.a.module('Ecosystem', []) // change Angular default {{ to {[{ to avoid conflict with Twig
.config(['$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false,
    rewriteLinks: false
  });
}]).filter('unsafe', ['$sce', function ($sce) {
  return function (val) {
    return $sce.trustAsHtml(val);
  };
}]).filter('range', function () {
  return function (input, total) {
    total = parseInt(total);

    for (var i = 0; i < total; i++) {
      input.push(i);
    }

    return input;
  };
}).service('eid', ['$http', Eid]).directive('form', ['$location', function ($location) {
  return {
    restrict: 'E',
    priority: 999,
    compile: function compile() {
      return {
        pre: function pre(scope, element, attrs) {
          if (attrs.noaction === '') return;

          if (attrs.action === undefined || attrs.action === '') {
            attrs.action = $location.absUrl();
          }
        }
      };
    }
  };
}]).directive('initTooltip', function () {
  return {
    restrict: 'A',
    link: function link(scope, element, attrs) {
      $(element).tooltip({
        trigger: 'hover'
      });
    }
  };
});
/* harmony default export */ __webpack_exports__["default"] = (app);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/Library/JsTree.js":
/*!*************************************!*\
  !*** ./assets/js/Library/JsTree.js ***!
  \*************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jstree__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jstree */ "./node_modules/jstree/dist/jstree.js");
/* harmony import */ var jstree__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jstree__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jstree_dist_themes_default_style_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jstree/dist/themes/default/style.min.css */ "./node_modules/jstree/dist/themes/default/style.min.css");
/* harmony import */ var jstree_dist_themes_default_style_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jstree_dist_themes_default_style_min_css__WEBPACK_IMPORTED_MODULE_1__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/





/***/ })

},[["./assets/js/Archive/CategorizationCtrl.js","runtime","vendors~Admin/EmailCtrl~Admin/FileAudit~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Recor~b3f38238","vendors~Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/SearchCtrl~BigData/Se~6c26450f","vendors~Archive/CategorizationCtrl~Archive/SearchCtrl~BigData/SearchCtrl~Competence/CategoriesAssign~65b3e25b","Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Record~Archive/SearchCtrl~Aut~d45f3fc6"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQXJjaGl2ZS9DYXRlZ29yaXphdGlvbkN0cmwuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL0NvbW1vbi9Bbmd1bGFyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9MaWJyYXJ5L0pzVHJlZS5qcyJdLCJuYW1lcyI6WyJDYXRlZ29yaXphdGlvbkNvbnRyb2xsZXIiLCIkc2NvcGUiLCIkdGltZW91dCIsImVpZCIsInRyZWVEaXYiLCIkIiwibWVzc2FnZURpdiIsInRyZWUiLCJsb2FkaW5nIiwidGF4b25vbXkiLCJzZWFyY2giLCJyZWNvcmQiLCIkd2F0Y2giLCJuZXdWYWx1ZSIsIm9sZFZhbHVlIiwidXJsIiwiUm91dGluZyIsImdlbmVyYXRlIiwiaWQiLCJjaGFuZ2UiLCJ0aGVuIiwicmVzcG9uc2UiLCJkYXRhIiwic3VjY2VzcyIsInNldHRpbmdzIiwiY29yZSIsIm1zZyIsImxvYWRfbm9kZSIsInRyaWdnZXIiLCJsZW5ndGgiLCJDb21tb24iLCJpbml0TWVzc2FnZSIsImluaXRUcmVlIiwic2VsZWN0ZWQiLCJkaXNhYmxlZCIsImVhY2giLCJnZXRfc2VsZWN0ZWQiLCJpbmRleCIsIm5vZGUiLCJwdXNoIiwidXVpZCIsImZpbmQiLCJpdGVtIiwiZ2V0X25vZGUiLCJwYXJlbnQiLCJhdHRyIiwicGFyYW1zIiwiaWNvbiIsImljb25QYXRoIiwianN0cmVlIiwiZ2V0VGF4b25vbXlJY29uIiwiaXRlbXMiLCJlbmFibGUiLCJlbmFibGVfbm9kZSIsImRpc2FibGUiLCJkZXNlbGVjdF9ub2RlIiwiZGlzYWJsZV9ub2RlIiwiY2xvc2Vfbm9kZSIsImlzX2Rpc2FibGVkIiwiYmluZCIsImV2ZW50IiwiYXBwIiwiY29udHJvbGxlciIsIkVpZCIsIiRodHRwIiwiaW5wdXQiLCJoZWFkZXJzIiwic2VuZCIsInBhcmFtIiwibWV0aG9kIiwiYnRuIiwiY3VycmVudFRhcmdldCIsImFkZENsYXNzIiwicHJvcCIsImVycm9yIiwiZGV0YWlsIiwic2hvd0FsZXJ0RGlhbG9nIiwiZG9jdW1lbnQiLCJtb2RhbCIsInJlbW92ZUNsYXNzIiwiYW5ndWxhciIsIm1vZHVsZSIsImNvbmZpZyIsIiRpbnRlcnBvbGF0ZVByb3ZpZGVyIiwiJGxvY2F0aW9uUHJvdmlkZXIiLCJzdGFydFN5bWJvbCIsImVuZFN5bWJvbCIsImh0bWw1TW9kZSIsImVuYWJsZWQiLCJyZXF1aXJlQmFzZSIsInJld3JpdGVMaW5rcyIsImZpbHRlciIsIiRzY2UiLCJ2YWwiLCJ0cnVzdEFzSHRtbCIsInRvdGFsIiwicGFyc2VJbnQiLCJpIiwic2VydmljZSIsImRpcmVjdGl2ZSIsIiRsb2NhdGlvbiIsInJlc3RyaWN0IiwicHJpb3JpdHkiLCJjb21waWxlIiwicHJlIiwic2NvcGUiLCJlbGVtZW50IiwiYXR0cnMiLCJub2FjdGlvbiIsImFjdGlvbiIsInVuZGVmaW5lZCIsImFic1VybCIsImxpbmsiLCJ0b29sdGlwIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQWNhOzs7Ozs7OztBQUViO0FBRUE7QUFDQTs7SUFFTUEsd0I7QUFDRixvQ0FBWUMsTUFBWixFQUFvQkMsUUFBcEIsRUFBOEJDLEdBQTlCLEVBQW1DO0FBQUE7O0FBQUE7O0FBQy9CLFNBQUtBLEdBQUwsR0FBV0EsR0FBWDtBQUVBLFNBQUtDLE9BQUwsR0FBZUMsQ0FBQyxDQUFDLDhCQUFELENBQWhCO0FBQ0EsU0FBS0MsVUFBTCxHQUFrQkQsQ0FBQyxDQUFDLDZCQUFELENBQW5CO0FBQ0EsU0FBS0UsSUFBTCxHQUFZLElBQVo7QUFFQSxTQUFLQyxPQUFMLEdBQWUsSUFBZjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxTQUFLQyxNQUFMLEdBQWMsRUFBZDtBQUNBLFNBQUtDLE1BQUwsR0FBYyxDQUFkO0FBRUFWLFVBQU0sQ0FBQ1csTUFBUCxDQUFjLHlCQUFkLEVBQXlDLFVBQUNDLFFBQUQsRUFBV0MsUUFBWCxFQUF3QjtBQUM3RCxVQUFJRCxRQUFRLElBQUlBLFFBQVEsS0FBS0MsUUFBN0IsRUFBdUM7QUFDbkMsYUFBSSxDQUFDTixPQUFMLEdBQWUsSUFBZjtBQUVBLFlBQU1PLEdBQUcsR0FBR0MsT0FBTyxDQUFDQyxRQUFSLENBQWlCLCtCQUFqQixFQUFrRDtBQUFDQyxZQUFFLEVBQUUsS0FBSSxDQUFDUCxNQUFWO0FBQWtCRixrQkFBUSxFQUFFSTtBQUE1QixTQUFsRCxDQUFaOztBQUNBLGFBQUksQ0FBQ1YsR0FBTCxDQUFTZ0IsTUFBVCxDQUFnQkosR0FBaEIsRUFBcUJLLElBQXJCLENBQTBCLFVBQUNDLFFBQUQsRUFBYztBQUNwQyxlQUFJLENBQUNYLE1BQUwsR0FBYyxFQUFkOztBQUVBLGNBQUlXLFFBQVEsQ0FBQ0MsSUFBVCxDQUFjQyxPQUFsQixFQUEyQjtBQUN2QixpQkFBSSxDQUFDaEIsSUFBTCxDQUFVaUIsUUFBVixDQUFtQkMsSUFBbkIsQ0FBd0JILElBQXhCLEdBQStCRCxRQUFRLENBQUNDLElBQVQsQ0FBY0ksR0FBN0MsQ0FEdUIsQ0FFdkI7QUFDQTs7QUFDQSxpQkFBSSxDQUFDbkIsSUFBTCxDQUFVb0IsU0FBVixDQUFvQixHQUFwQixFQUF5QixZQUFNO0FBQzNCekIsc0JBQVEsQ0FBQyxZQUFNO0FBQ1gscUJBQUksQ0FBQ00sT0FBTCxHQUFlLEtBQWY7QUFDSCxlQUZPLEVBRUwsQ0FGSyxDQUFSO0FBR0gsYUFKRDtBQUtILFdBVEQsTUFTTztBQUNILGlCQUFJLENBQUNGLFVBQUwsQ0FBZ0JzQixPQUFoQixDQUF3QixhQUF4QixFQUF1QyxDQUFDLHlCQUFELENBQXZDO0FBQ0g7QUFDSixTQWZEO0FBZ0JIO0FBQ0osS0F0QkQ7QUF3QkEzQixVQUFNLENBQUNXLE1BQVAsQ0FBYyx1QkFBZCxFQUFzQyxVQUFDQyxRQUFELEVBQVdDLFFBQVgsRUFBd0I7QUFDMUQsVUFBSUQsUUFBUSxJQUFJQSxRQUFRLEtBQUtDLFFBQTdCLEVBQXVDO0FBQ25DLFlBQUlELFFBQVEsQ0FBQ2dCLE1BQVQsR0FBa0IsQ0FBdEIsRUFBeUI7QUFDckIsZUFBSSxDQUFDdEIsSUFBTCxDQUFVRyxNQUFWLENBQWlCRyxRQUFqQjtBQUNILFNBRkQsTUFFTztBQUNILGVBQUksQ0FBQ04sSUFBTCxDQUFVRyxNQUFWLENBQWlCLEVBQWpCO0FBQ0g7QUFDSjtBQUNKLEtBUkQ7QUFVQW9CLDBEQUFNLENBQUNDLFdBQVAsQ0FBbUIsS0FBS3pCLFVBQXhCO0FBQ0EsU0FBSzBCLFFBQUw7QUFDSDs7OzttQ0FFYztBQUFBOztBQUNYLFVBQUksQ0FBQyxLQUFLdkIsUUFBVixFQUFvQjtBQUNoQjtBQUNIOztBQUVELFVBQUl3QixRQUFRLEdBQUcsRUFBZjtBQUNBLFVBQUlDLFFBQVEsR0FBRyxFQUFmO0FBRUE3QixPQUFDLENBQUM4QixJQUFGLENBQU8sS0FBSzVCLElBQUwsQ0FBVTZCLFlBQVYsQ0FBdUIsSUFBdkIsQ0FBUCxFQUFxQyxVQUFDQyxLQUFELEVBQVFDLElBQVIsRUFBaUI7QUFDbERMLGdCQUFRLENBQUNNLElBQVQsQ0FBY0QsSUFBSSxDQUFDaEIsSUFBTCxDQUFVa0IsSUFBeEI7QUFDSCxPQUZEO0FBSUEsV0FBS3BDLE9BQUwsQ0FBYXFDLElBQWIsQ0FBa0Isa0JBQWxCLEVBQXNDTixJQUF0QyxDQUEyQyxVQUFDRSxLQUFELEVBQVFLLElBQVIsRUFBaUI7QUFDeEQsWUFBSUosSUFBSSxHQUFHLE1BQUksQ0FBQy9CLElBQUwsQ0FBVW9DLFFBQVYsQ0FBbUJ0QyxDQUFDLENBQUNxQyxJQUFELENBQUQsQ0FBUUUsTUFBUixHQUFpQkMsSUFBakIsQ0FBc0IsSUFBdEIsQ0FBbkIsQ0FBWDs7QUFDQVgsZ0JBQVEsQ0FBQ0ssSUFBVCxDQUFjRCxJQUFJLENBQUNoQixJQUFMLENBQVVrQixJQUF4QjtBQUNILE9BSEQ7QUFLQSxVQUFNTSxNQUFNLEdBQUc7QUFDWGIsZ0JBQVEsRUFBRUEsUUFEQztBQUVYQyxnQkFBUSxFQUFFQTtBQUZDLE9BQWY7QUFLQSxVQUFNbkIsR0FBRyxHQUFHQyxPQUFPLENBQUNDLFFBQVIsQ0FBaUIsK0JBQWpCLEVBQWtEO0FBQUNDLFVBQUUsRUFBRSxLQUFLUCxNQUFWO0FBQWtCRixnQkFBUSxFQUFFLEtBQUtBO0FBQWpDLE9BQWxELENBQVo7QUFDQSxXQUFLTixHQUFMLENBQVNnQixNQUFULENBQWdCSixHQUFoQixFQUFxQitCLE1BQXJCLEVBQTZCMUIsSUFBN0IsQ0FBa0MsVUFBQ0MsUUFBRCxFQUFjO0FBQzVDLFlBQUlBLFFBQVEsQ0FBQ0MsSUFBVCxDQUFjQyxPQUFsQixFQUEyQjtBQUN2QixnQkFBSSxDQUFDakIsVUFBTCxDQUFnQnNCLE9BQWhCLENBQXdCLGdCQUF4QixFQUEwQyxDQUFDLHFDQUFELEVBQXdDLElBQXhDLENBQTFDO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsZ0JBQUksQ0FBQ3RCLFVBQUwsQ0FBZ0JzQixPQUFoQixDQUF3QixhQUF4QixFQUF1QyxDQUFDLDhCQUFELEVBQWlDLElBQWpDLENBQXZDO0FBQ0g7QUFDSixPQU5EO0FBT0g7OzsrQkFFVTtBQUFBOztBQUNQLFVBQU1tQixJQUFJLEdBQUdqQixzREFBTSxDQUFDa0IsUUFBUCxFQUFiO0FBRUEsV0FBSzVDLE9BQUwsQ0FBYTZDLE1BQWIsQ0FBb0I7QUFDaEIsbUJBQVksQ0FBQyxPQUFELEVBQVUsVUFBVixFQUFzQixhQUF0QixFQUFxQyxRQUFyQyxDQURJO0FBR2hCLGdCQUFTO0FBQ0wsb0JBQVc7QUFDUCxvQkFBUyxLQURGLENBQ1E7O0FBRFIsV0FETjtBQUlMLGtCQUFTO0FBSkosU0FITztBQVVoQixpQkFBVW5CLHNEQUFNLENBQUNvQixlQUFQLEVBVk07QUFZaEIsb0JBQWE7QUFDVCx5QkFBZSxLQUROO0FBRVQscUJBQVcsY0FGRjtBQUdUO0FBQ0EsMkJBQWlCO0FBSlIsU0FaRztBQW1CaEIsdUJBQWU7QUFDWEMsZUFBSyxFQUFFLGVBQUNiLElBQUQsRUFBVTtBQUNiO0FBQ0EsZ0JBQUlhLEtBQUssR0FBRztBQUNSQyxvQkFBTSxFQUFFO0FBQ0oseUJBQVMsWUFETDtBQUVKLHdCQUFTTCxJQUFJLEdBQUcsWUFGWjtBQUlKLDBCQUFVLGtCQUFNO0FBQ1osd0JBQUksQ0FBQ3hDLElBQUwsQ0FBVThDLFdBQVYsQ0FBc0JmLElBQXRCO0FBQ0g7QUFORyxlQURBO0FBU1JnQixxQkFBTyxFQUFFO0FBQ0wseUJBQVMsZ0JBREo7QUFFTCx3QkFBU1AsSUFBSSxHQUFHLHFCQUZYO0FBSUwsMEJBQVUsa0JBQU07QUFDWix3QkFBSSxDQUFDeEMsSUFBTCxDQUFVZ0QsYUFBVixDQUF3QmpCLElBQXhCOztBQUNBLHdCQUFJLENBQUMvQixJQUFMLENBQVVpRCxZQUFWLENBQXVCbEIsSUFBdkI7O0FBQ0Esd0JBQUksQ0FBQy9CLElBQUwsQ0FBVWtELFVBQVYsQ0FBcUJuQixJQUFyQjtBQUNIO0FBUkk7QUFURCxhQUFaOztBQXFCQSxnQkFBSSxNQUFJLENBQUMvQixJQUFMLENBQVVtRCxXQUFWLENBQXNCcEIsSUFBdEIsQ0FBSixFQUFpQztBQUM3QixxQkFBT2EsS0FBSyxDQUFDRyxPQUFiO0FBQ0gsYUFGRCxNQUVPO0FBQ0gscUJBQU9ILEtBQUssQ0FBQ0MsTUFBYjtBQUNIOztBQUVELG1CQUFPRCxLQUFQO0FBQ0g7QUEvQlU7QUFuQkMsT0FBcEI7QUFzREEsV0FBSzVDLElBQUwsR0FBWSxLQUFLSCxPQUFMLENBQWE2QyxNQUFiLENBQW9CLElBQXBCLENBQVo7QUFFQSxXQUFLN0MsT0FBTCxDQUFhdUQsSUFBYixDQUFrQixzQkFBbEIsRUFBeUMsVUFBQ0MsS0FBRCxFQUFRdEMsSUFBUixFQUFpQjtBQUN0RGpCLFNBQUMsQ0FBQzhCLElBQUYsQ0FBTyxNQUFJLENBQUM1QixJQUFMLENBQVU2QixZQUFWLENBQXVCLElBQXZCLENBQVAsRUFBcUMsVUFBQ0MsS0FBRCxFQUFRQyxJQUFSLEVBQWlCO0FBQ2xELGNBQUlBLElBQUksQ0FBQ2hCLElBQUwsQ0FBVWtCLElBQVYsS0FBbUJsQixJQUFJLENBQUNnQixJQUFMLENBQVVoQixJQUFWLENBQWVrQixJQUF0QyxFQUE0QztBQUN4QyxrQkFBSSxDQUFDakMsSUFBTCxDQUFVZ0QsYUFBVixDQUF3QmpCLElBQXhCO0FBQ0g7QUFDSixTQUpEO0FBS0gsT0FORCxFQU1HcUIsSUFOSCxDQU1RLGtCQU5SLEVBTTJCLFVBQUNDLEtBQUQsRUFBUXRDLElBQVIsRUFBaUI7QUFDeEMsWUFBSWdCLElBQUksR0FBR2hCLElBQUksQ0FBQ2dCLElBQWhCOztBQUNBLFlBQUksTUFBSSxDQUFDL0IsSUFBTCxDQUFVbUQsV0FBVixDQUFzQnBCLElBQXRCLENBQUosRUFBaUM7QUFDN0IsZ0JBQUksQ0FBQy9CLElBQUwsQ0FBVWtELFVBQVYsQ0FBcUJuQixJQUFyQjtBQUNIO0FBQ0osT0FYRDtBQVlIOzs7Ozs7QUFHTHVCLHVEQUFHLENBQ0VDLFVBREwsQ0FDZ0IsMEJBRGhCLEVBQzRDLENBQUMsUUFBRCxFQUFXLFVBQVgsRUFBdUIsS0FBdkIsRUFBOEI5RCx3QkFBOUIsQ0FENUMsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsTEE7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBOztJQUVNK0QsRztBQUNGOzs7OztBQUtBLGVBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFDZixTQUFLQSxLQUFMLEdBQWFBLEtBQWI7QUFDQSxTQUFLeEQsT0FBTCxHQUFlLEtBQWY7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7MkJBUU9PLEcsRUFBS2tELEssRUFBT0wsSyxFQUFPO0FBQ3RCLFdBQUtwRCxPQUFMLEdBQWUsSUFBZjs7QUFFQSxVQUFJLENBQUN5RCxLQUFMLEVBQVk7QUFDUkEsYUFBSyxHQUFHLEVBQVI7QUFDSDs7QUFFRCxVQUFJQyxPQUFPLEdBQUc7QUFDVix3QkFBZ0I7QUFETixPQUFkO0FBSUEsYUFBTyxLQUFLQyxJQUFMLENBQVUsTUFBVixFQUFrQnBELEdBQWxCLEVBQXVCVixDQUFDLENBQUMrRCxLQUFGLENBQVFILEtBQVIsQ0FBdkIsRUFBdUNDLE9BQXZDLEVBQWdETixLQUFoRCxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozt3QkFPSTdDLEcsRUFBSzZDLEssRUFBTztBQUNaLGFBQU8sS0FBS08sSUFBTCxDQUFVLEtBQVYsRUFBaUJwRCxHQUFqQixFQUFzQixJQUF0QixFQUE0QixJQUE1QixFQUFrQzZDLEtBQWxDLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozt5QkFRSzdDLEcsRUFBS2tELEssRUFBT0wsSyxFQUFPO0FBQ3BCLGFBQU8sS0FBS08sSUFBTCxDQUFVLE1BQVYsRUFBa0JwRCxHQUFsQixFQUF1QmtELEtBQXZCLEVBQThCLElBQTlCLEVBQW9DTCxLQUFwQyxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs0QkFPTzdDLEcsRUFBSzZDLEssRUFBTztBQUNmLGFBQU8sS0FBS08sSUFBTCxDQUFVLFFBQVYsRUFBb0JwRCxHQUFwQixFQUF5QixJQUF6QixFQUErQixJQUEvQixFQUFxQzZDLEtBQXJDLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7OzswQkFRTTdDLEcsRUFBS2tELEssRUFBT0wsSyxFQUFPO0FBQ3JCLFVBQUlNLE9BQU8sR0FBRztBQUNWLHdCQUFnQiw4QkFETjtBQUVWLGtCQUFVO0FBRkEsT0FBZDtBQUtBLGFBQU8sS0FBS0MsSUFBTCxDQUFVLE9BQVYsRUFBbUJwRCxHQUFuQixFQUF3QmtELEtBQXhCLEVBQStCQyxPQUEvQixFQUF3Q04sS0FBeEMsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7Ozs7eUJBVUtTLE0sRUFBUXRELEcsRUFBS2tELEssRUFBT0MsTyxFQUFTTixLLEVBQU87QUFDckMsVUFBSVUsR0FBRyxHQUFHLElBQVY7O0FBRUEsVUFBSSxDQUFDTCxLQUFMLEVBQVk7QUFDUkEsYUFBSyxHQUFHLEVBQVI7QUFDSDs7QUFFRCxVQUFJLENBQUNDLE9BQUwsRUFBYztBQUNWQSxlQUFPLEdBQUc7QUFDTiwwQkFBZ0Isa0JBRFY7QUFFTixvQkFBVTtBQUZKLFNBQVY7QUFJSDs7QUFFRCxVQUFJTixLQUFKLEVBQVc7QUFDUFUsV0FBRyxHQUFHakUsQ0FBQyxDQUFDdUQsS0FBSyxDQUFDVyxhQUFQLENBQVA7QUFDQUQsV0FBRyxDQUFDRSxRQUFKLENBQWEsU0FBYjtBQUNBRixXQUFHLENBQUNHLElBQUosQ0FBUyxVQUFULEVBQXFCLElBQXJCO0FBQ0g7O0FBRUQsYUFBTyxLQUFLVCxLQUFMLENBQVc7QUFDZEssY0FBTSxFQUFFQSxNQURNO0FBRWR0RCxXQUFHLEVBQUVBLEdBRlM7QUFHZE8sWUFBSSxFQUFFMkMsS0FIUTtBQUlkQyxlQUFPLEVBQUVBO0FBSkssT0FBWCxXQUtFLFVBQUNRLEtBQUQsRUFBVztBQUNoQixZQUFJQSxLQUFLLENBQUNwRCxJQUFOLENBQVdxRCxNQUFmLEVBQXVCO0FBQ25CN0MseURBQU0sQ0FBQzhDLGVBQVAsQ0FBdUIsUUFBdkIsRUFBaUNGLEtBQUssQ0FBQ3BELElBQU4sQ0FBV3FELE1BQTVDO0FBQ0gsU0FGRCxNQUVPO0FBQ0g3Qyx5REFBTSxDQUFDOEMsZUFBUCxDQUF1QixRQUF2QixFQUFpQyw0QkFBakM7QUFDSDtBQUNKLE9BWE0sYUFXSSxZQUFNO0FBQ2J2RSxTQUFDLENBQUN3RSxRQUFELENBQUQsQ0FBWWpELE9BQVosQ0FBb0IsY0FBcEI7O0FBQ0EsWUFBSTBDLEdBQUosRUFBUztBQUNMLGNBQU1RLEtBQUssR0FBR1IsR0FBRyxDQUFDaEQsSUFBSixDQUFTLE9BQVQsQ0FBZDs7QUFDQSxjQUFJd0QsS0FBSixFQUFXO0FBQ1B6RSxhQUFDLENBQUMsTUFBTXlFLEtBQVAsQ0FBRCxDQUFlQSxLQUFmLENBQXFCLE1BQXJCO0FBQ0g7O0FBRURSLGFBQUcsQ0FBQ1MsV0FBSixDQUFnQixTQUFoQjtBQUNBVCxhQUFHLENBQUNHLElBQUosQ0FBUyxVQUFULEVBQXFCLEtBQXJCO0FBQ0g7QUFDSixPQXRCTSxDQUFQO0FBdUJIOzs7Ozs7QUFHTCxJQUFNWixHQUFHLEdBQUdtQiw4Q0FBTyxDQUFDQyxNQUFSLENBQWUsV0FBZixFQUE0QixFQUE1QixFQUNSO0FBRFEsQ0FFUEMsTUFGTyxDQUVBLENBQUMsc0JBQUQsRUFBeUIsbUJBQXpCLEVBQThDLFVBQUNDLG9CQUFELEVBQXVCQyxpQkFBdkIsRUFBNkM7QUFDL0ZELHNCQUFvQixDQUFDRSxXQUFyQixDQUFpQyxLQUFqQyxFQUF3Q0MsU0FBeEMsQ0FBa0QsS0FBbEQ7QUFDQUYsbUJBQWlCLENBQUNHLFNBQWxCLENBQTRCO0FBQ3hCQyxXQUFPLEVBQUUsSUFEZTtBQUV4QkMsZUFBVyxFQUFFLEtBRlc7QUFHeEJDLGdCQUFZLEVBQUU7QUFIVSxHQUE1QjtBQUtILENBUE8sQ0FGQSxFQVVQQyxNQVZPLENBVUEsUUFWQSxFQVVVLENBQUMsTUFBRCxFQUFTLFVBQUNDLElBQUQsRUFBVTtBQUNqQyxTQUFPLFVBQUNDLEdBQUQsRUFBUztBQUNaLFdBQU9ELElBQUksQ0FBQ0UsV0FBTCxDQUFpQkQsR0FBakIsQ0FBUDtBQUNILEdBRkQ7QUFHSCxDQUppQixDQVZWLEVBZVBGLE1BZk8sQ0FlQSxPQWZBLEVBZVMsWUFBTTtBQUNuQixTQUFPLFVBQUMxQixLQUFELEVBQVE4QixLQUFSLEVBQWtCO0FBQ3JCQSxTQUFLLEdBQUdDLFFBQVEsQ0FBQ0QsS0FBRCxDQUFoQjs7QUFFQSxTQUFLLElBQUlFLENBQUMsR0FBQyxDQUFYLEVBQWNBLENBQUMsR0FBQ0YsS0FBaEIsRUFBdUJFLENBQUMsRUFBeEIsRUFBNEI7QUFDeEJoQyxXQUFLLENBQUMxQixJQUFOLENBQVcwRCxDQUFYO0FBQ0g7O0FBRUQsV0FBT2hDLEtBQVA7QUFDSCxHQVJEO0FBU0gsQ0F6Qk8sRUEwQlBpQyxPQTFCTyxDQTBCQyxLQTFCRCxFQTBCUSxDQUFDLE9BQUQsRUFBVW5DLEdBQVYsQ0ExQlIsRUEyQlBvQyxTQTNCTyxDQTJCRyxNQTNCSCxFQTJCVyxDQUFDLFdBQUQsRUFBYyxVQUFDQyxTQUFELEVBQWU7QUFDNUMsU0FBTztBQUNIQyxZQUFRLEVBQUMsR0FETjtBQUVIQyxZQUFRLEVBQUUsR0FGUDtBQUdIQyxXQUFPLEVBQUUsbUJBQU07QUFDWCxhQUFPO0FBQ0hDLFdBQUcsRUFBRSxhQUFDQyxLQUFELEVBQVFDLE9BQVIsRUFBaUJDLEtBQWpCLEVBQTJCO0FBQzVCLGNBQUlBLEtBQUssQ0FBQ0MsUUFBTixLQUFtQixFQUF2QixFQUEyQjs7QUFDM0IsY0FBSUQsS0FBSyxDQUFDRSxNQUFOLEtBQWlCQyxTQUFqQixJQUE4QkgsS0FBSyxDQUFDRSxNQUFOLEtBQWlCLEVBQW5ELEVBQXNEO0FBQ2xERixpQkFBSyxDQUFDRSxNQUFOLEdBQWVULFNBQVMsQ0FBQ1csTUFBVixFQUFmO0FBQ0g7QUFDSjtBQU5FLE9BQVA7QUFRSDtBQVpFLEdBQVA7QUFjSCxDQWZrQixDQTNCWCxFQTJDUFosU0EzQ08sQ0EyQ0csYUEzQ0gsRUEyQ2tCLFlBQU07QUFDNUIsU0FBTztBQUNIRSxZQUFRLEVBQUUsR0FEUDtBQUVIVyxRQUFJLEVBQUUsY0FBQ1AsS0FBRCxFQUFRQyxPQUFSLEVBQWlCQyxLQUFqQixFQUEyQjtBQUM3QnRHLE9BQUMsQ0FBQ3FHLE9BQUQsQ0FBRCxDQUFXTyxPQUFYLENBQW1CO0FBQ2ZyRixlQUFPLEVBQUc7QUFESyxPQUFuQjtBQUdIO0FBTkUsR0FBUDtBQVFILENBcERPLENBQVo7QUF1RGVpQyxrRUFBZixFOzs7Ozs7Ozs7Ozs7O0FDcE5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQWNhOztBQUViIiwiZmlsZSI6IkFyY2hpdmUvQ2F0ZWdvcml6YXRpb25DdHJsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKiAgQ29weXJpZ2h0IG5vdGljZVxyXG4gKlxyXG4gKiAgKGMpIDIwMTYtMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnLi4vTGlicmFyeS9Kc1RyZWUnO1xyXG5cclxuaW1wb3J0IGFwcCBmcm9tICcuLi9Db21tb24vQW5ndWxhcic7XHJcbmltcG9ydCBDb21tb24gZnJvbSAnLi4vQ29tbW9uL0NvbW1vbidcclxuXHJcbmNsYXNzIENhdGVnb3JpemF0aW9uQ29udHJvbGxlciB7XHJcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsICR0aW1lb3V0LCBlaWQpIHtcclxuICAgICAgICB0aGlzLmVpZCA9IGVpZDtcclxuXHJcbiAgICAgICAgdGhpcy50cmVlRGl2ID0gJCgnI2FyY2hpdmUtY2F0ZWdvcml6YXRpb24tdHJlZScpO1xyXG4gICAgICAgIHRoaXMubWVzc2FnZURpdiA9ICQoJyNhcmNoaXZlLWNhdGVnb3JpemF0aW9uLW1zZycpO1xyXG4gICAgICAgIHRoaXMudHJlZSA9IG51bGw7XHJcblxyXG4gICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy50YXhvbm9teSA9ICcnO1xyXG4gICAgICAgIHRoaXMuc2VhcmNoID0gJyc7XHJcbiAgICAgICAgdGhpcy5yZWNvcmQgPSAwO1xyXG5cclxuICAgICAgICAkc2NvcGUuJHdhdGNoKCdjYXRlZ29yaXphdGlvbi50YXhvbm9teScsIChuZXdWYWx1ZSwgb2xkVmFsdWUpID0+IHtcclxuICAgICAgICAgICAgaWYgKG5ld1ZhbHVlICYmIG5ld1ZhbHVlICE9PSBvbGRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCB1cmwgPSBSb3V0aW5nLmdlbmVyYXRlKCdhcmNoaXZlX2NhdGVnb3JpemF0aW9uX3NlbGVjdCcsIHtpZDogdGhpcy5yZWNvcmQsIHRheG9ub215OiBuZXdWYWx1ZX0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5laWQuY2hhbmdlKHVybCkudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaCA9ICcnO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuZGF0YS5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudHJlZS5zZXR0aW5ncy5jb3JlLmRhdGEgPSByZXNwb25zZS5kYXRhLm1zZztcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gXCJsb2FkX25vZGVcIiBrZWVwIGNoZWNrZWQgYm94XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIG1lYW53aGlsZSBcInJlZnJlc2hcIiBrZWVwcyBsYXN0IHN0YXRlIG9mIHRyZWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50cmVlLmxvYWRfbm9kZSgnIycsICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIDApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1lc3NhZ2VEaXYudHJpZ2dlcignYWN0aW9uLmZhaWwnLCBbJ0RpYWxvZyBjYW5ub3QgYmUgbG9hZGVkJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICRzY29wZS4kd2F0Y2goJ2NhdGVnb3JpemF0aW9uLnNlYXJjaCcsKG5ld1ZhbHVlLCBvbGRWYWx1ZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAobmV3VmFsdWUgJiYgbmV3VmFsdWUgIT09IG9sZFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAobmV3VmFsdWUubGVuZ3RoID4gMikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHJlZS5zZWFyY2gobmV3VmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRyZWUuc2VhcmNoKCcnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBDb21tb24uaW5pdE1lc3NhZ2UodGhpcy5tZXNzYWdlRGl2KTtcclxuICAgICAgICB0aGlzLmluaXRUcmVlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgYXNzaWduQWN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy50YXhvbm9teSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgc2VsZWN0ZWQgPSBbXTtcclxuICAgICAgICBsZXQgZGlzYWJsZWQgPSBbXTtcclxuXHJcbiAgICAgICAgJC5lYWNoKHRoaXMudHJlZS5nZXRfc2VsZWN0ZWQodHJ1ZSksIChpbmRleCwgbm9kZSkgPT4ge1xyXG4gICAgICAgICAgICBzZWxlY3RlZC5wdXNoKG5vZGUuZGF0YS51dWlkKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy50cmVlRGl2LmZpbmQoJy5qc3RyZWUtZGlzYWJsZWQnKS5lYWNoKChpbmRleCwgaXRlbSkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgbm9kZSA9IHRoaXMudHJlZS5nZXRfbm9kZSgkKGl0ZW0pLnBhcmVudCgpLmF0dHIoJ2lkJykpO1xyXG4gICAgICAgICAgICBkaXNhYmxlZC5wdXNoKG5vZGUuZGF0YS51dWlkKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc3QgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICBzZWxlY3RlZDogc2VsZWN0ZWQsXHJcbiAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlZCxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCB1cmwgPSBSb3V0aW5nLmdlbmVyYXRlKCdhcmNoaXZlX2NhdGVnb3JpemF0aW9uX3VwZGF0ZScsIHtpZDogdGhpcy5yZWNvcmQsIHRheG9ub215OiB0aGlzLnRheG9ub215fSk7XHJcbiAgICAgICAgdGhpcy5laWQuY2hhbmdlKHVybCwgcGFyYW1zKS50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzcG9uc2UuZGF0YS5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1lc3NhZ2VEaXYudHJpZ2dlcignYWN0aW9uLnN1Y2Nlc3MnLCBbJ1lvdXIgc2V0dGluZyB3YXMgc3VjY2Vzc2Z1bGx5IHNhdmVkJywgdHJ1ZV0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tZXNzYWdlRGl2LnRyaWdnZXIoJ2FjdGlvbi5mYWlsJywgWydZb3VyIHNldHRpbmcgY2Fubm90IGJlIHNhdmVkJywgdHJ1ZV0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdFRyZWUoKSB7XHJcbiAgICAgICAgY29uc3QgaWNvbiA9IENvbW1vbi5pY29uUGF0aCgpO1xyXG5cclxuICAgICAgICB0aGlzLnRyZWVEaXYuanN0cmVlKHtcclxuICAgICAgICAgICAgXCJwbHVnaW5zXCIgOiBbXCJ0eXBlc1wiLCBcImNoZWNrYm94XCIsIFwiY29udGV4dG1lbnVcIiwgXCJzZWFyY2hcIl0sXHJcblxyXG4gICAgICAgICAgICBcImNvcmVcIiA6IHtcclxuICAgICAgICAgICAgICAgIFwidGhlbWVzXCIgOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgXCJkb3RzXCIgOiBmYWxzZSAvLyBubyBjb25uZWN0aW5nIGRvdHMgYmV0d2VlbiBkb3RzXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgXCJkYXRhXCIgOiB7fSxcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIFwidHlwZXNcIiA6IENvbW1vbi5nZXRUYXhvbm9teUljb24oKSxcclxuXHJcbiAgICAgICAgICAgIFwiY2hlY2tib3hcIiA6IHtcclxuICAgICAgICAgICAgICAgIFwidGhyZWVfc3RhdGVcIjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBcImNhc2NhZGVcIjogJ3VuZGV0ZXJtaW5lZCcsXHJcbiAgICAgICAgICAgICAgICAvL1wid2hvbGVfbm9kZVwiOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIFwidGllX3NlbGVjdGlvblwiOiB0cnVlLFxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgXCJjb250ZXh0bWVudVwiOiB7XHJcbiAgICAgICAgICAgICAgICBpdGVtczogKG5vZGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBUaGUgZGVmYXVsdCBzZXQgb2YgYWxsIGl0ZW1zXHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGl0ZW1zID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbmFibGU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGFiZWxcIjogXCJBcHBsaWNhYmxlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImljb25cIiA6IGljb24gKyBcInBsdWdpbi5wbmdcIixcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImFjdGlvblwiOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50cmVlLmVuYWJsZV9ub2RlKG5vZGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCI6IFwiTm90IEFwcGxpY2FibGVcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaWNvblwiIDogaWNvbiArIFwicGx1Z2luX2Rpc2FibGVkLnBuZ1wiLFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYWN0aW9uXCI6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRyZWUuZGVzZWxlY3Rfbm9kZShub2RlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRyZWUuZGlzYWJsZV9ub2RlKG5vZGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudHJlZS5jbG9zZV9ub2RlKG5vZGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRyZWUuaXNfZGlzYWJsZWQobm9kZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsZXRlIGl0ZW1zLmRpc2FibGU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsZXRlIGl0ZW1zLmVuYWJsZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBpdGVtcztcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMudHJlZSA9IHRoaXMudHJlZURpdi5qc3RyZWUodHJ1ZSk7XHJcblxyXG4gICAgICAgIHRoaXMudHJlZURpdi5iaW5kKCdkZXNlbGVjdF9ub2RlLmpzdHJlZScsKGV2ZW50LCBkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICQuZWFjaCh0aGlzLnRyZWUuZ2V0X3NlbGVjdGVkKHRydWUpLCAoaW5kZXgsIG5vZGUpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChub2RlLmRhdGEudXVpZCA9PT0gZGF0YS5ub2RlLmRhdGEudXVpZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHJlZS5kZXNlbGVjdF9ub2RlKG5vZGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pLmJpbmQoJ29wZW5fbm9kZS5qc3RyZWUnLChldmVudCwgZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgbm9kZSA9IGRhdGEubm9kZTtcclxuICAgICAgICAgICAgaWYgKHRoaXMudHJlZS5pc19kaXNhYmxlZChub2RlKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50cmVlLmNsb3NlX25vZGUobm9kZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG5cclxuYXBwXHJcbiAgICAuY29udHJvbGxlcignQ2F0ZWdvcml6YXRpb25Db250cm9sbGVyJywgWyckc2NvcGUnLCAnJHRpbWVvdXQnLCAnZWlkJywgQ2F0ZWdvcml6YXRpb25Db250cm9sbGVyXSk7IiwiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKiAgQ29weXJpZ2h0IG5vdGljZVxyXG4gKlxyXG4gKiAgKGMpIDIwMTggRlRLIGUuVi4gPGZ0ay5kZT5cclxuICogIEFsbCByaWdodHMgcmVzZXJ2ZWRcclxuICpcclxuICogIFRoaXMgc2NyaXB0IGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAqICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gKiAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gKiAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuICpcclxuICogIFRoaXMgY29weXJpZ2h0IG5vdGljZSBNVVNUIEFQUEVBUiBpbiBhbGwgY29waWVzIG9mIHRoZSBzY3JpcHQhXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG5pbXBvcnQgYW5ndWxhciBmcm9tICdhbmd1bGFyJztcclxuaW1wb3J0IENvbW1vbiBmcm9tICcuL0NvbW1vbic7XHJcblxyXG5jbGFzcyBFaWQge1xyXG4gICAgLyoqXHJcbiAgICAgKiBJbml0aWF0ZVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSAkaHR0cFxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcigkaHR0cCkge1xyXG4gICAgICAgIHRoaXMuJGh0dHAgPSAkaHR0cDtcclxuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIHNlbmQgbm9ybWFsIFBPU1RcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gaW5wdXRcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PiB8IFByb21pc2U8VD4gfCAqfVxyXG4gICAgICovXHJcbiAgICBjaGFuZ2UodXJsLCBpbnB1dCwgZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xyXG5cclxuICAgICAgICBpZiAoIWlucHV0KSB7XHJcbiAgICAgICAgICAgIGlucHV0ID0ge307XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaGVhZGVycyA9IHtcclxuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VuZCgnUE9TVCcsIHVybCwgJC5wYXJhbShpbnB1dCksIGhlYWRlcnMsIGV2ZW50KVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBhIEdFVCByZXF1ZXN0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fFByb21pc2U8VD58Kn1cclxuICAgICAqL1xyXG4gICAgZ2V0KHVybCwgZXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZW5kKCdHRVQnLCB1cmwsIG51bGwsIG51bGwsIGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbmQgYSBQT1NUIHJlcXVlc3Qgd2l0aCBKU09OIGRhdGFcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gaW5wdXRcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PnxQcm9taXNlPFQ+fCp9XHJcbiAgICAgKi9cclxuICAgIHBvc3QodXJsLCBpbnB1dCwgZXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZW5kKCdQT1NUJywgdXJsLCBpbnB1dCwgbnVsbCwgZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBhIERFTEVURSByZXF1ZXN0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fFByb21pc2U8VD58Kn1cclxuICAgICAqL1xyXG4gICAgZGVsZXRlKHVybCwgZXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZW5kKCdERUxFVEUnLCB1cmwsIG51bGwsIG51bGwsIGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbmQgYSBQQVRDSCByZXF1ZXN0IHdpdGggSlNPTiBkYXRhXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGlucHV0XHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT58UHJvbWlzZTxUPnwqfVxyXG4gICAgICovXHJcbiAgICBwYXRjaCh1cmwsIGlucHV0LCBldmVudCkge1xyXG4gICAgICAgIGxldCBoZWFkZXJzID0ge1xyXG4gICAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL21lcmdlLXBhdGNoK2pzb24nLFxyXG4gICAgICAgICAgICAnYWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbmQoJ1BBVENIJywgdXJsLCBpbnB1dCwgaGVhZGVycywgZXZlbnQpXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIGEgcmVxdWVzdFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBtZXRob2RcclxuICAgICAqIEBwYXJhbSB1cmxcclxuICAgICAqIEBwYXJhbSBpbnB1dFxyXG4gICAgICogQHBhcmFtIGhlYWRlcnNcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PiB8IFByb21pc2U8VD4gfCAqfVxyXG4gICAgICovXHJcbiAgICBzZW5kKG1ldGhvZCwgdXJsLCBpbnB1dCwgaGVhZGVycywgZXZlbnQpIHtcclxuICAgICAgICBsZXQgYnRuID0gbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKCFpbnB1dCkge1xyXG4gICAgICAgICAgICBpbnB1dCA9IHt9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFoZWFkZXJzKSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxyXG4gICAgICAgICAgICAgICAgJ2FjY2VwdCc6ICdhcHBsaWNhdGlvbi9sZCtqc29uJyxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgICAgICBidG4gPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xyXG4gICAgICAgICAgICBidG4uYWRkQ2xhc3MoJ3J1bm5pbmcnKTtcclxuICAgICAgICAgICAgYnRuLnByb3AoXCJkaXNhYmxlZFwiLCB0cnVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLiRodHRwKHtcclxuICAgICAgICAgICAgbWV0aG9kOiBtZXRob2QsXHJcbiAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICBkYXRhOiBpbnB1dCxcclxuICAgICAgICAgICAgaGVhZGVyczogaGVhZGVyc1xyXG4gICAgICAgIH0pLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZXJyb3IuZGF0YS5kZXRhaWwpIHtcclxuICAgICAgICAgICAgICAgIENvbW1vbi5zaG93QWxlcnREaWFsb2coJ2RhbmdlcicsIGVycm9yLmRhdGEuZGV0YWlsKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIENvbW1vbi5zaG93QWxlcnREaWFsb2coJ2RhbmdlcicsICdBY3Rpb24gY2Fubm90IGJlIHBlcmZvcm1lZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICQoZG9jdW1lbnQpLnRyaWdnZXIoJ2xvYWRpbmcuc3RvcCcpO1xyXG4gICAgICAgICAgICBpZiAoYnRuKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtb2RhbCA9IGJ0bi5kYXRhKCdjbG9zZScpO1xyXG4gICAgICAgICAgICAgICAgaWYgKG1vZGFsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnIycgKyBtb2RhbCkubW9kYWwoJ2hpZGUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBidG4ucmVtb3ZlQ2xhc3MoJ3J1bm5pbmcnKTtcclxuICAgICAgICAgICAgICAgIGJ0bi5wcm9wKFwiZGlzYWJsZWRcIiwgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IGFwcCA9IGFuZ3VsYXIubW9kdWxlKCdFY29zeXN0ZW0nLCBbXSlcclxuICAgIC8vIGNoYW5nZSBBbmd1bGFyIGRlZmF1bHQge3sgdG8ge1t7IHRvIGF2b2lkIGNvbmZsaWN0IHdpdGggVHdpZ1xyXG4gICAgLmNvbmZpZyhbJyRpbnRlcnBvbGF0ZVByb3ZpZGVyJywgJyRsb2NhdGlvblByb3ZpZGVyJywgKCRpbnRlcnBvbGF0ZVByb3ZpZGVyLCAkbG9jYXRpb25Qcm92aWRlcikgPT4ge1xyXG4gICAgICAgICRpbnRlcnBvbGF0ZVByb3ZpZGVyLnN0YXJ0U3ltYm9sKCd7W3snKS5lbmRTeW1ib2woJ31dfScpO1xyXG4gICAgICAgICRsb2NhdGlvblByb3ZpZGVyLmh0bWw1TW9kZSh7XHJcbiAgICAgICAgICAgIGVuYWJsZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHJlcXVpcmVCYXNlOiBmYWxzZSxcclxuICAgICAgICAgICAgcmV3cml0ZUxpbmtzOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgfV0pXHJcbiAgICAuZmlsdGVyKCd1bnNhZmUnLCBbJyRzY2UnLCAoJHNjZSkgPT4ge1xyXG4gICAgICAgIHJldHVybiAodmFsKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiAkc2NlLnRydXN0QXNIdG1sKHZhbCk7XHJcbiAgICAgICAgfTtcclxuICAgIH1dKVxyXG4gICAgLmZpbHRlcigncmFuZ2UnLCAoKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIChpbnB1dCwgdG90YWwpID0+IHtcclxuICAgICAgICAgICAgdG90YWwgPSBwYXJzZUludCh0b3RhbCk7XHJcblxyXG4gICAgICAgICAgICBmb3IgKGxldCBpPTA7IGk8dG90YWw7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgaW5wdXQucHVzaChpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGlucHV0O1xyXG4gICAgICAgIH07XHJcbiAgICB9KVxyXG4gICAgLnNlcnZpY2UoJ2VpZCcsIFsnJGh0dHAnLCBFaWRdKVxyXG4gICAgLmRpcmVjdGl2ZSgnZm9ybScsIFsnJGxvY2F0aW9uJywgKCRsb2NhdGlvbikgPT4ge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJlc3RyaWN0OidFJyxcclxuICAgICAgICAgICAgcHJpb3JpdHk6IDk5OSxcclxuICAgICAgICAgICAgY29tcGlsZTogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICBwcmU6IChzY29wZSwgZWxlbWVudCwgYXR0cnMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGF0dHJzLm5vYWN0aW9uID09PSAnJykgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXR0cnMuYWN0aW9uID09PSB1bmRlZmluZWQgfHwgYXR0cnMuYWN0aW9uID09PSAnJyl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRycy5hY3Rpb24gPSAkbG9jYXRpb24uYWJzVXJsKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XSlcclxuICAgIC5kaXJlY3RpdmUoJ2luaXRUb29sdGlwJywgKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnQScsXHJcbiAgICAgICAgICAgIGxpbms6IChzY29wZSwgZWxlbWVudCwgYXR0cnMpID0+IHtcclxuICAgICAgICAgICAgICAgICQoZWxlbWVudCkudG9vbHRpcCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJpZ2dlciA6ICdob3ZlcidcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgIH0pXHJcbjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGFwcDsiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnanN0cmVlJztcclxuaW1wb3J0ICdqc3RyZWUvZGlzdC90aGVtZXMvZGVmYXVsdC9zdHlsZS5taW4uY3NzJzsiXSwic291cmNlUm9vdCI6IiJ9