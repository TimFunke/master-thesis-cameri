(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Archive/SearchCtrl"],{

/***/ "./assets/js/Archive/SearchCtrl.js":
/*!*****************************************!*\
  !*** ./assets/js/Archive/SearchCtrl.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _Library_Star__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Library/Star */ "./assets/js/Library/Star.js");
/* harmony import */ var _Library_IonRangeSlider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Library/IonRangeSlider */ "./assets/js/Library/IonRangeSlider.js");
/* harmony import */ var _Library_JsTree__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Library/JsTree */ "./assets/js/Library/JsTree.js");
/* harmony import */ var _library_jstree_themes_proton_style_min_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../library/jstree/themes/proton/style.min.css */ "./assets/library/jstree/themes/proton/style.min.css");
/* harmony import */ var _library_jstree_themes_proton_style_min_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_library_jstree_themes_proton_style_min_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _library_jstree_custom_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../library/jstree/custom.css */ "./assets/library/jstree/custom.css");
/* harmony import */ var _library_jstree_custom_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_library_jstree_custom_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _Common_Angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Common/Angular */ "./assets/js/Common/Angular.js");
/* harmony import */ var _Common_Common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Common/Common */ "./assets/js/Common/Common.js");
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }









var SearchController = /*#__PURE__*/function () {
  function SearchController(eid) {
    _classCallCheck(this, SearchController);

    this.eid = eid;
    this.loading = true;
  }

  _createClass(SearchController, [{
    key: "init",
    value: function init(taxonomy, repositories) {
      this.repositories = angular.fromJson(repositories);

      if (!window.location.hash) {
        window.history.replaceState('', '', window.location.pathname);
        window.location.hash = Date.now().toString(16);
      }

      var input,
          cookie = _Common_Common__WEBPACK_IMPORTED_MODULE_6__["default"].getCookie('archive' + window.location.hash);

      if (cookie) {
        input = angular.fromJson(cookie);
      } //console.log(input);


      if (input) {
        this.input = angular.copy(input);
        this.change(true, true);
      } else {
        this.input = {
          taxonomy: taxonomy,
          fq: {}
        };
        this.change(true);
      }
    }
  }, {
    key: "changeTaxonomy",
    value: function changeTaxonomy() {
      // needed ?
      //this.input.fq['category'] = [];
      this.change(true);
    }
  }, {
    key: "setTerm",
    value: function setTerm(e) {
      e.preventDefault();
      this.input.q = this.term;
      this.change(true);
    }
  }, {
    key: "increaseStart",
    value: function increaseStart(value) {
      this.input.start += value;
      this.change(false);
    }
  }, {
    key: "getURL",
    value: function getURL(doc) {
      return Routing.generate('archive_record_show', {
        id: doc.id
      });
    }
  }, {
    key: "getTitle",
    value: function getTitle(doc) {
      if (!doc['dc.title_ws']) {
        return 'No Title';
      } else {
        var title = doc['dc.title_ws'];

        if (title.length > 200) {
          title = title.substr(0, 200) + '...';
        }

        return title;
      }
    }
    /**
     * add a facet value
     *
     * @param parameter
     * @param value
     */

  }, {
    key: "add",
    value: function add(parameter, value) {
      if (!this.input.fq[parameter]) {
        this.input.fq[parameter] = [];
      }

      var index = this.input.fq[parameter].indexOf(value + '');

      if (index == -1) {
        this.input.fq[parameter].push(value);
      } else {
        this.input.fq[parameter].splice(index, 1);
      }

      this.change(true);
    }
    /**
     * update a facet value
     *
     * @param parameter
     * @param value
     */

  }, {
    key: "update",
    value: function update(parameter, value) {
      if (value === this.input.fq[parameter]) {
        value = '';
      }

      this.input.fq[parameter] = value;
      this.change(true);
    }
  }, {
    key: "change",
    value: function change(resetParam, keepStart) {
      var _this = this;

      this.loading = true;

      if (resetParam && !keepStart) {
        this.input.start = 0;
      }

      _Common_Common__WEBPACK_IMPORTED_MODULE_6__["default"].setCookie('archive' + window.location.hash, JSON.stringify(this.input), 1 / 24);
      this.eid.change(Routing.generate('archive_search_request'), this.input).then(function (response) {
        if (response.data) {
          var data = response.data;
          _this.result = data.result;

          if (resetParam) {
            _this.rows = data.params.rows;
            _this.queryTime = data.queryTime;
            _this.facet = data.facet;
            _this.input = {
              q: _this.term,
              sort: data.params.sort,
              taxonomy: data.taxonomy,
              fq: Array.isArray(data.fq) ? {} : data.fq
            };
          }

          _this.input.start = data.params.start;
          _this.currentPage = _this.result.numFound > 0 ? window.Math.ceil((_this.input.start + 1) / _this.rows) : 0;
          _this.totalPage = window.Math.ceil(_this.result.numFound / _this.rows);
          _this.loading = false;
        }
      });
    }
  }]);

  return SearchController;
}();

var YearSliderDirective = /*#__PURE__*/function () {
  function YearSliderDirective() {
    _classCallCheck(this, YearSliderDirective);
  }

  _createClass(YearSliderDirective, [{
    key: "link",
    value: function link($scope, element) {
      var config = {
        skin: "modern",
        type: "double",
        values_separator: " to ",
        grid: false,
        prettify_enabled: false,
        onFinish: function onFinish(data) {
          $scope.$apply(function () {
            $scope.search.update('dc.date_s', '[' + data.from + ' TO ' + data.to + ']');
          });
        }
      };
      $(element).ionRangeSlider(config);
      var slider = $(element).data("ionRangeSlider");
      $scope.$watch("search.facet['dc.date_s'].min", function (value) {
        slider.update({
          min: value
        });

        if (!$scope.search.input.fq['dc.date_s']) {
          slider.update({
            from: value
          });
        }
      }, true);
      $scope.$watch("search.facet['dc.date_s'].max", function (value) {
        slider.update({
          max: value
        });

        if (!$scope.search.input.fq['dc.date_s']) {
          slider.update({
            to: value
          });
        }
      }, true);
    }
  }]);

  return YearSliderDirective;
}();

var JsTreeDirective = /*#__PURE__*/function () {
  function JsTreeDirective() {
    _classCallCheck(this, JsTreeDirective);
  }

  _createClass(JsTreeDirective, [{
    key: "link",
    value: function link($scope, element) {
      var ready = false;
      $(element).jstree({
        plugins: ["checkbox"],
        checkbox: {
          keep_selected_style: false,
          three_state: false
        },
        core: {
          themes: {
            name: 'proton',
            responsive: true,
            icons: false,
            dots: false
          },
          check_callback: true,
          data: []
        }
      });
      var tree = $(element).jstree(true);
      $(element).on('select_node.jstree', function (event, data) {
        if (ready) {
          $scope.search.add('category', data.node.data.uuid);
        }
      });
      $(element).on('deselect_node.jstree', function (event, data) {
        if (ready) {
          $scope.search.add('category', data.node.data.uuid);
        }
      });
      $scope.$watch("search.facet['category']", function (value) {
        if (value) {
          ready = false;
          tree.settings.core.data = value;
          tree.load_node('#', function () {
            ready = true;
            tree.open_all();
          });
        }
      }, true);
    }
  }]);

  return JsTreeDirective;
}();

_Common_Angular__WEBPACK_IMPORTED_MODULE_5__["default"].controller('SearchController', ['eid', SearchController]).directive('yearSlider', function () {
  return new YearSliderDirective();
}).directive('filterTree', function () {
  return new JsTreeDirective();
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

},[["./assets/js/Archive/SearchCtrl.js","runtime","vendors~Admin/EmailCtrl~Admin/FileAudit~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Recor~b3f38238","vendors~Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/SearchCtrl~BigData/Se~6c26450f","vendors~Archive/CategorizationCtrl~Archive/SearchCtrl~BigData/SearchCtrl~Competence/CategoriesAssign~65b3e25b","vendors~Archive/SearchCtrl~BigData/SearchCtrl~Library/IonRangeSlider","Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Record~Archive/SearchCtrl~Aut~d45f3fc6","Archive/SearchCtrl~BigData/SearchCtrl"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQXJjaGl2ZS9TZWFyY2hDdHJsLmpzIl0sIm5hbWVzIjpbIlNlYXJjaENvbnRyb2xsZXIiLCJlaWQiLCJsb2FkaW5nIiwidGF4b25vbXkiLCJyZXBvc2l0b3JpZXMiLCJhbmd1bGFyIiwiZnJvbUpzb24iLCJ3aW5kb3ciLCJsb2NhdGlvbiIsImhhc2giLCJoaXN0b3J5IiwicmVwbGFjZVN0YXRlIiwicGF0aG5hbWUiLCJEYXRlIiwibm93IiwidG9TdHJpbmciLCJpbnB1dCIsImNvb2tpZSIsIkNvbW1vbiIsImdldENvb2tpZSIsImNvcHkiLCJjaGFuZ2UiLCJmcSIsImUiLCJwcmV2ZW50RGVmYXVsdCIsInEiLCJ0ZXJtIiwidmFsdWUiLCJzdGFydCIsImRvYyIsIlJvdXRpbmciLCJnZW5lcmF0ZSIsImlkIiwidGl0bGUiLCJsZW5ndGgiLCJzdWJzdHIiLCJwYXJhbWV0ZXIiLCJpbmRleCIsImluZGV4T2YiLCJwdXNoIiwic3BsaWNlIiwicmVzZXRQYXJhbSIsImtlZXBTdGFydCIsInNldENvb2tpZSIsIkpTT04iLCJzdHJpbmdpZnkiLCJ0aGVuIiwicmVzcG9uc2UiLCJkYXRhIiwicmVzdWx0Iiwicm93cyIsInBhcmFtcyIsInF1ZXJ5VGltZSIsImZhY2V0Iiwic29ydCIsIkFycmF5IiwiaXNBcnJheSIsImN1cnJlbnRQYWdlIiwibnVtRm91bmQiLCJNYXRoIiwiY2VpbCIsInRvdGFsUGFnZSIsIlllYXJTbGlkZXJEaXJlY3RpdmUiLCIkc2NvcGUiLCJlbGVtZW50IiwiY29uZmlnIiwic2tpbiIsInR5cGUiLCJ2YWx1ZXNfc2VwYXJhdG9yIiwiZ3JpZCIsInByZXR0aWZ5X2VuYWJsZWQiLCJvbkZpbmlzaCIsIiRhcHBseSIsInNlYXJjaCIsInVwZGF0ZSIsImZyb20iLCJ0byIsIiQiLCJpb25SYW5nZVNsaWRlciIsInNsaWRlciIsIiR3YXRjaCIsIm1pbiIsIm1heCIsIkpzVHJlZURpcmVjdGl2ZSIsInJlYWR5IiwianN0cmVlIiwicGx1Z2lucyIsImNoZWNrYm94Iiwia2VlcF9zZWxlY3RlZF9zdHlsZSIsInRocmVlX3N0YXRlIiwiY29yZSIsInRoZW1lcyIsIm5hbWUiLCJyZXNwb25zaXZlIiwiaWNvbnMiLCJkb3RzIiwiY2hlY2tfY2FsbGJhY2siLCJ0cmVlIiwib24iLCJldmVudCIsImFkZCIsIm5vZGUiLCJ1dWlkIiwic2V0dGluZ3MiLCJsb2FkX25vZGUiLCJvcGVuX2FsbCIsImFwcCIsImNvbnRyb2xsZXIiLCJkaXJlY3RpdmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBdUJhOzs7Ozs7OztBQUViO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOztJQUVNQSxnQjtBQUNGLDRCQUFZQyxHQUFaLEVBQWlCO0FBQUE7O0FBQ2IsU0FBS0EsR0FBTCxHQUFXQSxHQUFYO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLElBQWY7QUFDSDs7Ozt5QkFFSUMsUSxFQUFVQyxZLEVBQWM7QUFDekIsV0FBS0EsWUFBTCxHQUFvQkMsT0FBTyxDQUFDQyxRQUFSLENBQWlCRixZQUFqQixDQUFwQjs7QUFFQSxVQUFJLENBQUNHLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkMsSUFBckIsRUFBMkI7QUFDdkJGLGNBQU0sQ0FBQ0csT0FBUCxDQUFlQyxZQUFmLENBQTRCLEVBQTVCLEVBQWdDLEVBQWhDLEVBQW9DSixNQUFNLENBQUNDLFFBQVAsQ0FBZ0JJLFFBQXBEO0FBQ0FMLGNBQU0sQ0FBQ0MsUUFBUCxDQUFnQkMsSUFBaEIsR0FBdUJJLElBQUksQ0FBQ0MsR0FBTCxHQUFXQyxRQUFYLENBQW9CLEVBQXBCLENBQXZCO0FBQ0g7O0FBRUQsVUFBSUMsS0FBSjtBQUFBLFVBQVdDLE1BQU0sR0FBR0Msc0RBQU0sQ0FBQ0MsU0FBUCxDQUFpQixZQUFZWixNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLElBQTdDLENBQXBCOztBQUNBLFVBQUlRLE1BQUosRUFBWTtBQUNSRCxhQUFLLEdBQUdYLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQlcsTUFBakIsQ0FBUjtBQUNILE9BWHdCLENBYXpCOzs7QUFDQSxVQUFJRCxLQUFKLEVBQVc7QUFDUCxhQUFLQSxLQUFMLEdBQWFYLE9BQU8sQ0FBQ2UsSUFBUixDQUFhSixLQUFiLENBQWI7QUFDQSxhQUFLSyxNQUFMLENBQVksSUFBWixFQUFrQixJQUFsQjtBQUNILE9BSEQsTUFHTztBQUNILGFBQUtMLEtBQUwsR0FBYTtBQUNUYixrQkFBUSxFQUFFQSxRQUREO0FBRVRtQixZQUFFLEVBQUU7QUFGSyxTQUFiO0FBSUEsYUFBS0QsTUFBTCxDQUFZLElBQVo7QUFDSDtBQUNKOzs7cUNBRWdCO0FBQ2I7QUFDQTtBQUNBLFdBQUtBLE1BQUwsQ0FBWSxJQUFaO0FBQ0g7Ozs0QkFFT0UsQyxFQUFHO0FBQ1BBLE9BQUMsQ0FBQ0MsY0FBRjtBQUNBLFdBQUtSLEtBQUwsQ0FBV1MsQ0FBWCxHQUFlLEtBQUtDLElBQXBCO0FBQ0EsV0FBS0wsTUFBTCxDQUFZLElBQVo7QUFDSDs7O2tDQUVhTSxLLEVBQU87QUFDakIsV0FBS1gsS0FBTCxDQUFXWSxLQUFYLElBQW9CRCxLQUFwQjtBQUNBLFdBQUtOLE1BQUwsQ0FBWSxLQUFaO0FBQ0g7OzsyQkFFTVEsRyxFQUFLO0FBQ1IsYUFBT0MsT0FBTyxDQUFDQyxRQUFSLENBQWlCLHFCQUFqQixFQUF3QztBQUFDQyxVQUFFLEVBQUVILEdBQUcsQ0FBQ0c7QUFBVCxPQUF4QyxDQUFQO0FBQ0g7Ozs2QkFFUUgsRyxFQUFLO0FBQ1YsVUFBSSxDQUFDQSxHQUFHLENBQUMsYUFBRCxDQUFSLEVBQXlCO0FBQ3JCLGVBQU8sVUFBUDtBQUNILE9BRkQsTUFFTztBQUNILFlBQUlJLEtBQUssR0FBR0osR0FBRyxDQUFDLGFBQUQsQ0FBZjs7QUFDQSxZQUFJSSxLQUFLLENBQUNDLE1BQU4sR0FBZSxHQUFuQixFQUF3QjtBQUNwQkQsZUFBSyxHQUFHQSxLQUFLLENBQUNFLE1BQU4sQ0FBYSxDQUFiLEVBQWdCLEdBQWhCLElBQXVCLEtBQS9CO0FBQ0g7O0FBQ0QsZUFBT0YsS0FBUDtBQUNIO0FBQ0o7QUFFRDs7Ozs7Ozs7O3dCQU1JRyxTLEVBQVdULEssRUFBTztBQUNsQixVQUFJLENBQUMsS0FBS1gsS0FBTCxDQUFXTSxFQUFYLENBQWNjLFNBQWQsQ0FBTCxFQUErQjtBQUMzQixhQUFLcEIsS0FBTCxDQUFXTSxFQUFYLENBQWNjLFNBQWQsSUFBMkIsRUFBM0I7QUFDSDs7QUFFRCxVQUFJQyxLQUFLLEdBQUcsS0FBS3JCLEtBQUwsQ0FBV00sRUFBWCxDQUFjYyxTQUFkLEVBQXlCRSxPQUF6QixDQUFpQ1gsS0FBSyxHQUFDLEVBQXZDLENBQVo7O0FBRUEsVUFBSVUsS0FBSyxJQUFJLENBQUMsQ0FBZCxFQUFpQjtBQUNiLGFBQUtyQixLQUFMLENBQVdNLEVBQVgsQ0FBY2MsU0FBZCxFQUF5QkcsSUFBekIsQ0FBOEJaLEtBQTlCO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsYUFBS1gsS0FBTCxDQUFXTSxFQUFYLENBQWNjLFNBQWQsRUFBeUJJLE1BQXpCLENBQWdDSCxLQUFoQyxFQUF1QyxDQUF2QztBQUNIOztBQUNELFdBQUtoQixNQUFMLENBQVksSUFBWjtBQUNIO0FBRUQ7Ozs7Ozs7OzsyQkFNT2UsUyxFQUFXVCxLLEVBQU87QUFDckIsVUFBSUEsS0FBSyxLQUFLLEtBQUtYLEtBQUwsQ0FBV00sRUFBWCxDQUFjYyxTQUFkLENBQWQsRUFBd0M7QUFDcENULGFBQUssR0FBRyxFQUFSO0FBQ0g7O0FBRUQsV0FBS1gsS0FBTCxDQUFXTSxFQUFYLENBQWNjLFNBQWQsSUFBMkJULEtBQTNCO0FBQ0EsV0FBS04sTUFBTCxDQUFZLElBQVo7QUFDSDs7OzJCQUVNb0IsVSxFQUFZQyxTLEVBQVc7QUFBQTs7QUFDMUIsV0FBS3hDLE9BQUwsR0FBZSxJQUFmOztBQUVBLFVBQUl1QyxVQUFVLElBQUksQ0FBQ0MsU0FBbkIsRUFBOEI7QUFDMUIsYUFBSzFCLEtBQUwsQ0FBV1ksS0FBWCxHQUFtQixDQUFuQjtBQUNIOztBQUVEViw0REFBTSxDQUFDeUIsU0FBUCxDQUFpQixZQUFZcEMsTUFBTSxDQUFDQyxRQUFQLENBQWdCQyxJQUE3QyxFQUFtRG1DLElBQUksQ0FBQ0MsU0FBTCxDQUFlLEtBQUs3QixLQUFwQixDQUFuRCxFQUErRSxJQUFFLEVBQWpGO0FBRUEsV0FBS2YsR0FBTCxDQUFTb0IsTUFBVCxDQUFnQlMsT0FBTyxDQUFDQyxRQUFSLENBQWlCLHdCQUFqQixDQUFoQixFQUE0RCxLQUFLZixLQUFqRSxFQUF3RThCLElBQXhFLENBQTZFLFVBQUNDLFFBQUQsRUFBYztBQUN2RixZQUFJQSxRQUFRLENBQUNDLElBQWIsRUFBbUI7QUFDZixjQUFNQSxJQUFJLEdBQUdELFFBQVEsQ0FBQ0MsSUFBdEI7QUFDQSxlQUFJLENBQUNDLE1BQUwsR0FBY0QsSUFBSSxDQUFDQyxNQUFuQjs7QUFFQSxjQUFJUixVQUFKLEVBQWdCO0FBQ1osaUJBQUksQ0FBQ1MsSUFBTCxHQUFZRixJQUFJLENBQUNHLE1BQUwsQ0FBWUQsSUFBeEI7QUFDQSxpQkFBSSxDQUFDRSxTQUFMLEdBQWlCSixJQUFJLENBQUNJLFNBQXRCO0FBQ0EsaUJBQUksQ0FBQ0MsS0FBTCxHQUFhTCxJQUFJLENBQUNLLEtBQWxCO0FBRUEsaUJBQUksQ0FBQ3JDLEtBQUwsR0FBYTtBQUNUUyxlQUFDLEVBQUUsS0FBSSxDQUFDQyxJQURDO0FBRVQ0QixrQkFBSSxFQUFFTixJQUFJLENBQUNHLE1BQUwsQ0FBWUcsSUFGVDtBQUdUbkQsc0JBQVEsRUFBRTZDLElBQUksQ0FBQzdDLFFBSE47QUFJVG1CLGdCQUFFLEVBQUVpQyxLQUFLLENBQUNDLE9BQU4sQ0FBY1IsSUFBSSxDQUFDMUIsRUFBbkIsSUFBeUIsRUFBekIsR0FBOEIwQixJQUFJLENBQUMxQjtBQUo5QixhQUFiO0FBTUg7O0FBRUQsZUFBSSxDQUFDTixLQUFMLENBQVdZLEtBQVgsR0FBbUJvQixJQUFJLENBQUNHLE1BQUwsQ0FBWXZCLEtBQS9CO0FBQ0EsZUFBSSxDQUFDNkIsV0FBTCxHQUFtQixLQUFJLENBQUNSLE1BQUwsQ0FBWVMsUUFBWixHQUF1QixDQUF2QixHQUEyQm5ELE1BQU0sQ0FBQ29ELElBQVAsQ0FBWUMsSUFBWixDQUFpQixDQUFDLEtBQUksQ0FBQzVDLEtBQUwsQ0FBV1ksS0FBWCxHQUFpQixDQUFsQixJQUFxQixLQUFJLENBQUNzQixJQUEzQyxDQUEzQixHQUE4RSxDQUFqRztBQUNBLGVBQUksQ0FBQ1csU0FBTCxHQUFpQnRELE1BQU0sQ0FBQ29ELElBQVAsQ0FBWUMsSUFBWixDQUFpQixLQUFJLENBQUNYLE1BQUwsQ0FBWVMsUUFBWixHQUFxQixLQUFJLENBQUNSLElBQTNDLENBQWpCO0FBQ0EsZUFBSSxDQUFDaEQsT0FBTCxHQUFlLEtBQWY7QUFDSDtBQUNKLE9BdkJEO0FBd0JIOzs7Ozs7SUFHQzRELG1COzs7Ozs7O3lCQUNHQyxNLEVBQVFDLE8sRUFBUztBQUNsQixVQUFJQyxNQUFNLEdBQUc7QUFDVEMsWUFBSSxFQUFFLFFBREc7QUFFVEMsWUFBSSxFQUFFLFFBRkc7QUFHVEMsd0JBQWdCLEVBQUUsTUFIVDtBQUlUQyxZQUFJLEVBQUUsS0FKRztBQUtUQyx3QkFBZ0IsRUFBRSxLQUxUO0FBTVRDLGdCQUFRLEVBQUUsa0JBQUN2QixJQUFELEVBQVU7QUFDaEJlLGdCQUFNLENBQUNTLE1BQVAsQ0FBYyxZQUFNO0FBQ2hCVCxrQkFBTSxDQUFDVSxNQUFQLENBQWNDLE1BQWQsQ0FBcUIsV0FBckIsRUFBa0MsTUFBTTFCLElBQUksQ0FBQzJCLElBQVgsR0FBa0IsTUFBbEIsR0FBMkIzQixJQUFJLENBQUM0QixFQUFoQyxHQUFxQyxHQUF2RTtBQUNILFdBRkQ7QUFHSDtBQVZRLE9BQWI7QUFhQUMsT0FBQyxDQUFDYixPQUFELENBQUQsQ0FBV2MsY0FBWCxDQUEwQmIsTUFBMUI7QUFDQSxVQUFJYyxNQUFNLEdBQUdGLENBQUMsQ0FBQ2IsT0FBRCxDQUFELENBQVdoQixJQUFYLENBQWdCLGdCQUFoQixDQUFiO0FBRUFlLFlBQU0sQ0FBQ2lCLE1BQVAsQ0FBYywrQkFBZCxFQUErQyxVQUFDckQsS0FBRCxFQUFXO0FBQ3REb0QsY0FBTSxDQUFDTCxNQUFQLENBQWM7QUFBQ08sYUFBRyxFQUFFdEQ7QUFBTixTQUFkOztBQUNBLFlBQUksQ0FBQ29DLE1BQU0sQ0FBQ1UsTUFBUCxDQUFjekQsS0FBZCxDQUFvQk0sRUFBcEIsQ0FBdUIsV0FBdkIsQ0FBTCxFQUEwQztBQUN0Q3lELGdCQUFNLENBQUNMLE1BQVAsQ0FBYztBQUFDQyxnQkFBSSxFQUFFaEQ7QUFBUCxXQUFkO0FBQ0g7QUFDSixPQUxELEVBS0csSUFMSDtBQU9Bb0MsWUFBTSxDQUFDaUIsTUFBUCxDQUFjLCtCQUFkLEVBQStDLFVBQUNyRCxLQUFELEVBQVc7QUFDdERvRCxjQUFNLENBQUNMLE1BQVAsQ0FBYztBQUFDUSxhQUFHLEVBQUV2RDtBQUFOLFNBQWQ7O0FBQ0EsWUFBSSxDQUFDb0MsTUFBTSxDQUFDVSxNQUFQLENBQWN6RCxLQUFkLENBQW9CTSxFQUFwQixDQUF1QixXQUF2QixDQUFMLEVBQTBDO0FBQ3RDeUQsZ0JBQU0sQ0FBQ0wsTUFBUCxDQUFjO0FBQUNFLGNBQUUsRUFBRWpEO0FBQUwsV0FBZDtBQUNIO0FBQ0osT0FMRCxFQUtHLElBTEg7QUFNSDs7Ozs7O0lBSUN3RCxlOzs7Ozs7O3lCQUNHcEIsTSxFQUFRQyxPLEVBQVM7QUFDbEIsVUFBSW9CLEtBQUssR0FBRyxLQUFaO0FBRUFQLE9BQUMsQ0FBQ2IsT0FBRCxDQUFELENBQVdxQixNQUFYLENBQWtCO0FBQ2RDLGVBQU8sRUFBRyxDQUFDLFVBQUQsQ0FESTtBQUVkQyxnQkFBUSxFQUFHO0FBQ1BDLDZCQUFtQixFQUFHLEtBRGY7QUFFUEMscUJBQVcsRUFBRztBQUZQLFNBRkc7QUFNZEMsWUFBSSxFQUFHO0FBQ0hDLGdCQUFNLEVBQUc7QUFDTEMsZ0JBQUksRUFBRyxRQURGO0FBRUxDLHNCQUFVLEVBQUUsSUFGUDtBQUdMQyxpQkFBSyxFQUFHLEtBSEg7QUFJTEMsZ0JBQUksRUFBRTtBQUpELFdBRE47QUFPSEMsd0JBQWMsRUFBRyxJQVBkO0FBUUhoRCxjQUFJLEVBQUU7QUFSSDtBQU5PLE9BQWxCO0FBa0JBLFVBQUlpRCxJQUFJLEdBQUdwQixDQUFDLENBQUNiLE9BQUQsQ0FBRCxDQUFXcUIsTUFBWCxDQUFrQixJQUFsQixDQUFYO0FBRUFSLE9BQUMsQ0FBQ2IsT0FBRCxDQUFELENBQVdrQyxFQUFYLENBQWMsb0JBQWQsRUFBb0MsVUFBQ0MsS0FBRCxFQUFRbkQsSUFBUixFQUFpQjtBQUNqRCxZQUFJb0MsS0FBSixFQUFXO0FBQ1ByQixnQkFBTSxDQUFDVSxNQUFQLENBQWMyQixHQUFkLENBQWtCLFVBQWxCLEVBQThCcEQsSUFBSSxDQUFDcUQsSUFBTCxDQUFVckQsSUFBVixDQUFlc0QsSUFBN0M7QUFDSDtBQUNKLE9BSkQ7QUFNQXpCLE9BQUMsQ0FBQ2IsT0FBRCxDQUFELENBQVdrQyxFQUFYLENBQWMsc0JBQWQsRUFBc0MsVUFBQ0MsS0FBRCxFQUFRbkQsSUFBUixFQUFpQjtBQUNuRCxZQUFJb0MsS0FBSixFQUFXO0FBQ1ByQixnQkFBTSxDQUFDVSxNQUFQLENBQWMyQixHQUFkLENBQWtCLFVBQWxCLEVBQThCcEQsSUFBSSxDQUFDcUQsSUFBTCxDQUFVckQsSUFBVixDQUFlc0QsSUFBN0M7QUFDSDtBQUNKLE9BSkQ7QUFNQXZDLFlBQU0sQ0FBQ2lCLE1BQVAsQ0FBYywwQkFBZCxFQUEwQyxVQUFDckQsS0FBRCxFQUFXO0FBQ2pELFlBQUlBLEtBQUosRUFBVztBQUNQeUQsZUFBSyxHQUFHLEtBQVI7QUFDQWEsY0FBSSxDQUFDTSxRQUFMLENBQWNiLElBQWQsQ0FBbUIxQyxJQUFuQixHQUEwQnJCLEtBQTFCO0FBRUFzRSxjQUFJLENBQUNPLFNBQUwsQ0FBZSxHQUFmLEVBQW9CLFlBQU07QUFDdEJwQixpQkFBSyxHQUFHLElBQVI7QUFDQWEsZ0JBQUksQ0FBQ1EsUUFBTDtBQUNILFdBSEQ7QUFJSDtBQUNKLE9BVkQsRUFVRyxJQVZIO0FBV0g7Ozs7OztBQUtMQyx1REFBRyxDQUNFQyxVQURMLENBQ2dCLGtCQURoQixFQUNvQyxDQUFDLEtBQUQsRUFBUTNHLGdCQUFSLENBRHBDLEVBRUs0RyxTQUZMLENBRWUsWUFGZixFQUU2QjtBQUFBLFNBQU0sSUFBSTlDLG1CQUFKLEVBQU47QUFBQSxDQUY3QixFQUdLOEMsU0FITCxDQUdlLFlBSGYsRUFHNkI7QUFBQSxTQUFNLElBQUl6QixlQUFKLEVBQU47QUFBQSxDQUg3QixFIiwiZmlsZSI6IkFyY2hpdmUvU2VhcmNoQ3RybC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuICogIENvcHlyaWdodCBub3RpY2VcclxuICpcclxuICogIChjKSAyMDE1IEZUSyBlLlYuIDxmdGsuZGU+XHJcbiAqICBBbGwgcmlnaHRzIHJlc2VydmVkXHJcbiAqXHJcbiAqICBUaGlzIHNjcmlwdCBpcyBwYXJ0IG9mIHRoZSBUWVBPMyBwcm9qZWN0LiBUaGUgVFlQTzMgcHJvamVjdCBpc1xyXG4gKiAgZnJlZSBzb2Z0d2FyZTsgeW91IGNhbiByZWRpc3RyaWJ1dGUgaXQgYW5kL29yIG1vZGlmeVxyXG4gKiAgaXQgdW5kZXIgdGhlIHRlcm1zIG9mIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBhcyBwdWJsaXNoZWQgYnlcclxuICogIHRoZSBGcmVlIFNvZnR3YXJlIEZvdW5kYXRpb247IGVpdGhlciB2ZXJzaW9uIDIgb2YgdGhlIExpY2Vuc2UsIG9yXHJcbiAqICAoYXQgeW91ciBvcHRpb24pIGFueSBsYXRlciB2ZXJzaW9uLlxyXG4gKlxyXG4gKiAgVGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGNhbiBiZSBmb3VuZCBhdFxyXG4gKiAgaHR0cDovL3d3dy5nbnUub3JnL2NvcHlsZWZ0L2dwbC5odG1sLlxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnLi4vTGlicmFyeS9TdGFyJztcclxuaW1wb3J0ICcuLi9MaWJyYXJ5L0lvblJhbmdlU2xpZGVyJztcclxuXHJcbmltcG9ydCAnLi4vTGlicmFyeS9Kc1RyZWUnO1xyXG5pbXBvcnQgJy4uLy4uL2xpYnJhcnkvanN0cmVlL3RoZW1lcy9wcm90b24vc3R5bGUubWluLmNzcyc7XHJcbmltcG9ydCAnLi4vLi4vbGlicmFyeS9qc3RyZWUvY3VzdG9tLmNzcyc7XHJcblxyXG5pbXBvcnQgYXBwIGZyb20gJy4uL0NvbW1vbi9Bbmd1bGFyJztcclxuaW1wb3J0IENvbW1vbiBmcm9tICcuLi9Db21tb24vQ29tbW9uJ1xyXG5cclxuY2xhc3MgU2VhcmNoQ29udHJvbGxlciB7XHJcbiAgICBjb25zdHJ1Y3RvcihlaWQpIHtcclxuICAgICAgICB0aGlzLmVpZCA9IGVpZDtcclxuICAgICAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXQodGF4b25vbXksIHJlcG9zaXRvcmllcykge1xyXG4gICAgICAgIHRoaXMucmVwb3NpdG9yaWVzID0gYW5ndWxhci5mcm9tSnNvbihyZXBvc2l0b3JpZXMpO1xyXG5cclxuICAgICAgICBpZiAoIXdpbmRvdy5sb2NhdGlvbi5oYXNoKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5oaXN0b3J5LnJlcGxhY2VTdGF0ZSgnJywgJycsIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSk7XHJcbiAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5oYXNoID0gRGF0ZS5ub3coKS50b1N0cmluZygxNik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaW5wdXQsIGNvb2tpZSA9IENvbW1vbi5nZXRDb29raWUoJ2FyY2hpdmUnICsgd2luZG93LmxvY2F0aW9uLmhhc2gpO1xyXG4gICAgICAgIGlmIChjb29raWUpIHtcclxuICAgICAgICAgICAgaW5wdXQgPSBhbmd1bGFyLmZyb21Kc29uKGNvb2tpZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvL2NvbnNvbGUubG9nKGlucHV0KTtcclxuICAgICAgICBpZiAoaW5wdXQpIHtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dCA9IGFuZ3VsYXIuY29weShpbnB1dCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2hhbmdlKHRydWUsIHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXQgPSB7XHJcbiAgICAgICAgICAgICAgICB0YXhvbm9teTogdGF4b25vbXksXHJcbiAgICAgICAgICAgICAgICBmcToge30sXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuY2hhbmdlKHRydWUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2VUYXhvbm9teSgpIHtcclxuICAgICAgICAvLyBuZWVkZWQgP1xyXG4gICAgICAgIC8vdGhpcy5pbnB1dC5mcVsnY2F0ZWdvcnknXSA9IFtdO1xyXG4gICAgICAgIHRoaXMuY2hhbmdlKHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFRlcm0oZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB0aGlzLmlucHV0LnEgPSB0aGlzLnRlcm07XHJcbiAgICAgICAgdGhpcy5jaGFuZ2UodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5jcmVhc2VTdGFydCh2YWx1ZSkge1xyXG4gICAgICAgIHRoaXMuaW5wdXQuc3RhcnQgKz0gdmFsdWU7XHJcbiAgICAgICAgdGhpcy5jaGFuZ2UoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVSTChkb2MpIHtcclxuICAgICAgICByZXR1cm4gUm91dGluZy5nZW5lcmF0ZSgnYXJjaGl2ZV9yZWNvcmRfc2hvdycsIHtpZDogZG9jLmlkfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGl0bGUoZG9jKSB7XHJcbiAgICAgICAgaWYgKCFkb2NbJ2RjLnRpdGxlX3dzJ10pIHtcclxuICAgICAgICAgICAgcmV0dXJuICdObyBUaXRsZSc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGV0IHRpdGxlID0gZG9jWydkYy50aXRsZV93cyddO1xyXG4gICAgICAgICAgICBpZiAodGl0bGUubGVuZ3RoID4gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZSA9IHRpdGxlLnN1YnN0cigwLCAyMDApICsgJy4uLic7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHRpdGxlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIGFkZCBhIGZhY2V0IHZhbHVlXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHBhcmFtZXRlclxyXG4gICAgICogQHBhcmFtIHZhbHVlXHJcbiAgICAgKi9cclxuICAgIGFkZChwYXJhbWV0ZXIsIHZhbHVlKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmlucHV0LmZxW3BhcmFtZXRlcl0pIHtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dC5mcVtwYXJhbWV0ZXJdID0gW107XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaW5kZXggPSB0aGlzLmlucHV0LmZxW3BhcmFtZXRlcl0uaW5kZXhPZih2YWx1ZSsnJyk7XHJcblxyXG4gICAgICAgIGlmIChpbmRleCA9PSAtMSkge1xyXG4gICAgICAgICAgICB0aGlzLmlucHV0LmZxW3BhcmFtZXRlcl0ucHVzaCh2YWx1ZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dC5mcVtwYXJhbWV0ZXJdLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuY2hhbmdlKHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogdXBkYXRlIGEgZmFjZXQgdmFsdWVcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gcGFyYW1ldGVyXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgdXBkYXRlKHBhcmFtZXRlciwgdmFsdWUpIHtcclxuICAgICAgICBpZiAodmFsdWUgPT09IHRoaXMuaW5wdXQuZnFbcGFyYW1ldGVyXSkge1xyXG4gICAgICAgICAgICB2YWx1ZSA9ICcnO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5pbnB1dC5mcVtwYXJhbWV0ZXJdID0gdmFsdWU7XHJcbiAgICAgICAgdGhpcy5jaGFuZ2UodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlKHJlc2V0UGFyYW0sIGtlZXBTdGFydCkge1xyXG4gICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XHJcblxyXG4gICAgICAgIGlmIChyZXNldFBhcmFtICYmICFrZWVwU3RhcnQpIHtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dC5zdGFydCA9IDA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBDb21tb24uc2V0Q29va2llKCdhcmNoaXZlJyArIHdpbmRvdy5sb2NhdGlvbi5oYXNoLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0KSwgMS8yNCk7XHJcblxyXG4gICAgICAgIHRoaXMuZWlkLmNoYW5nZShSb3V0aW5nLmdlbmVyYXRlKCdhcmNoaXZlX3NlYXJjaF9yZXF1ZXN0JyksIHRoaXMuaW5wdXQpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXNwb25zZS5kYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkYXRhID0gcmVzcG9uc2UuZGF0YTtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVzdWx0ID0gZGF0YS5yZXN1bHQ7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHJlc2V0UGFyYW0pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvd3MgPSBkYXRhLnBhcmFtcy5yb3dzO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucXVlcnlUaW1lID0gZGF0YS5xdWVyeVRpbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mYWNldCA9IGRhdGEuZmFjZXQ7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaW5wdXQgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHE6IHRoaXMudGVybSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydDogZGF0YS5wYXJhbXMuc29ydCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGF4b25vbXk6IGRhdGEudGF4b25vbXksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZxOiBBcnJheS5pc0FycmF5KGRhdGEuZnEpID8ge30gOiBkYXRhLmZxLFxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnB1dC5zdGFydCA9IGRhdGEucGFyYW1zLnN0YXJ0O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZSA9IHRoaXMucmVzdWx0Lm51bUZvdW5kID4gMCA/IHdpbmRvdy5NYXRoLmNlaWwoKHRoaXMuaW5wdXQuc3RhcnQrMSkvdGhpcy5yb3dzKSA6IDA7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRvdGFsUGFnZSA9IHdpbmRvdy5NYXRoLmNlaWwodGhpcy5yZXN1bHQubnVtRm91bmQvdGhpcy5yb3dzKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNsYXNzIFllYXJTbGlkZXJEaXJlY3RpdmV7XHJcbiAgICBsaW5rKCRzY29wZSwgZWxlbWVudCkge1xyXG4gICAgICAgIGxldCBjb25maWcgPSB7XHJcbiAgICAgICAgICAgIHNraW46IFwibW9kZXJuXCIsXHJcbiAgICAgICAgICAgIHR5cGU6IFwiZG91YmxlXCIsXHJcbiAgICAgICAgICAgIHZhbHVlc19zZXBhcmF0b3I6IFwiIHRvIFwiLFxyXG4gICAgICAgICAgICBncmlkOiBmYWxzZSxcclxuICAgICAgICAgICAgcHJldHRpZnlfZW5hYmxlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIG9uRmluaXNoOiAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgJHNjb3BlLiRhcHBseSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnNlYXJjaC51cGRhdGUoJ2RjLmRhdGVfcycsICdbJyArIGRhdGEuZnJvbSArICcgVE8gJyArIGRhdGEudG8gKyAnXScpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgJChlbGVtZW50KS5pb25SYW5nZVNsaWRlcihjb25maWcpO1xyXG4gICAgICAgIGxldCBzbGlkZXIgPSAkKGVsZW1lbnQpLmRhdGEoXCJpb25SYW5nZVNsaWRlclwiKTtcclxuXHJcbiAgICAgICAgJHNjb3BlLiR3YXRjaChcInNlYXJjaC5mYWNldFsnZGMuZGF0ZV9zJ10ubWluXCIsICh2YWx1ZSkgPT4ge1xyXG4gICAgICAgICAgICBzbGlkZXIudXBkYXRlKHttaW46IHZhbHVlfSk7XHJcbiAgICAgICAgICAgIGlmICghJHNjb3BlLnNlYXJjaC5pbnB1dC5mcVsnZGMuZGF0ZV9zJ10pIHtcclxuICAgICAgICAgICAgICAgIHNsaWRlci51cGRhdGUoe2Zyb206IHZhbHVlfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCB0cnVlKTtcclxuXHJcbiAgICAgICAgJHNjb3BlLiR3YXRjaChcInNlYXJjaC5mYWNldFsnZGMuZGF0ZV9zJ10ubWF4XCIsICh2YWx1ZSkgPT4ge1xyXG4gICAgICAgICAgICBzbGlkZXIudXBkYXRlKHttYXg6IHZhbHVlfSk7XHJcbiAgICAgICAgICAgIGlmICghJHNjb3BlLnNlYXJjaC5pbnB1dC5mcVsnZGMuZGF0ZV9zJ10pIHtcclxuICAgICAgICAgICAgICAgIHNsaWRlci51cGRhdGUoe3RvOiB2YWx1ZX0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgdHJ1ZSk7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5jbGFzcyBKc1RyZWVEaXJlY3RpdmUge1xyXG4gICAgbGluaygkc2NvcGUsIGVsZW1lbnQpIHtcclxuICAgICAgICBsZXQgcmVhZHkgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgJChlbGVtZW50KS5qc3RyZWUoe1xyXG4gICAgICAgICAgICBwbHVnaW5zIDogW1wiY2hlY2tib3hcIl0sXHJcbiAgICAgICAgICAgIGNoZWNrYm94IDoge1xyXG4gICAgICAgICAgICAgICAga2VlcF9zZWxlY3RlZF9zdHlsZSA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgdGhyZWVfc3RhdGUgOiBmYWxzZSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY29yZSA6IHtcclxuICAgICAgICAgICAgICAgIHRoZW1lcyA6IHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lIDogJ3Byb3RvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBpY29ucyA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIGRvdHM6IGZhbHNlXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY2hlY2tfY2FsbGJhY2sgOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogW11cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsZXQgdHJlZSA9ICQoZWxlbWVudCkuanN0cmVlKHRydWUpO1xyXG5cclxuICAgICAgICAkKGVsZW1lbnQpLm9uKCdzZWxlY3Rfbm9kZS5qc3RyZWUnLCAoZXZlbnQsIGRhdGEpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlYWR5KSB7XHJcbiAgICAgICAgICAgICAgICAkc2NvcGUuc2VhcmNoLmFkZCgnY2F0ZWdvcnknLCBkYXRhLm5vZGUuZGF0YS51dWlkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkKGVsZW1lbnQpLm9uKCdkZXNlbGVjdF9ub2RlLmpzdHJlZScsIChldmVudCwgZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVhZHkpIHtcclxuICAgICAgICAgICAgICAgICRzY29wZS5zZWFyY2guYWRkKCdjYXRlZ29yeScsIGRhdGEubm9kZS5kYXRhLnV1aWQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICRzY29wZS4kd2F0Y2goXCJzZWFyY2guZmFjZXRbJ2NhdGVnb3J5J11cIiwgKHZhbHVlKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgcmVhZHkgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRyZWUuc2V0dGluZ3MuY29yZS5kYXRhID0gdmFsdWU7XHJcblxyXG4gICAgICAgICAgICAgICAgdHJlZS5sb2FkX25vZGUoJyMnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhZHkgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRyZWUub3Blbl9hbGwoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgdHJ1ZSk7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5cclxuYXBwXHJcbiAgICAuY29udHJvbGxlcignU2VhcmNoQ29udHJvbGxlcicsIFsnZWlkJywgU2VhcmNoQ29udHJvbGxlcl0pXHJcbiAgICAuZGlyZWN0aXZlKCd5ZWFyU2xpZGVyJywgKCkgPT4gbmV3IFllYXJTbGlkZXJEaXJlY3RpdmUpXHJcbiAgICAuZGlyZWN0aXZlKCdmaWx0ZXJUcmVlJywgKCkgPT4gbmV3IEpzVHJlZURpcmVjdGl2ZSk7Il0sInNvdXJjZVJvb3QiOiIifQ==