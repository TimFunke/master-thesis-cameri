(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Archive/Record~Authentication/User~Common/Layout~Content/ContentList~Content/Display~Core/Feedback~E~7511650d"],{

/***/ "./assets/js/Common/Layout.js":
/*!************************************!*\
  !*** ./assets/js/Common/Layout.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! html2canvas */ "./node_modules/html2canvas/dist/html2canvas.js");
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(html2canvas__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var bs_custom_file_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bs-custom-file-input */ "./node_modules/bs-custom-file-input/dist/bs-custom-file-input.js");
/* harmony import */ var bs_custom_file_input__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bs_custom_file_input__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Common */ "./assets/js/Common/Common.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
// Screenshot capture




var Layout = /*#__PURE__*/function () {
  function Layout() {
    _classCallCheck(this, Layout);
  }

  _createClass(Layout, null, [{
    key: "initFeedback",
    value: function initFeedback() {
      var banner = $('<a href="#" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Send your suggestion or report a bug/error" style="position: fixed; bottom: 10px; right: 10px; margin-bottom: 0; border-radius: 50%;"><span class="fas fa-envelope" style="top: 2px;"></span></a>');
      banner.click(function (event) {
        event.preventDefault();
        Layout.showFeedbackWindow();
      });

      if ($(".se-pre-con").length > 0) {
        $("body").append(banner);
      }
    }
  }, {
    key: "initChatbot",
    value: function initChatbot() {
      var url = $('.footer').attr('data-chatbot');

      if (url) {
        var banner = $('<a href="' + url + '" target="_blank" data-toggle="tooltip" data-placement="top" title="Chat with our bot" style="position: fixed; bottom: 80px; right: 13px;">' + '<img src="/images/robot.png" style="height: 44px;">' + '</a>');

        if ($(".se-pre-con").length > 0) {
          $("body").append(banner);
        }
      }
    }
  }, {
    key: "showFeedbackWindow",
    value: function showFeedbackWindow() {
      var newWindow = window.open(Routing.generate('core_feedback_new'), 'User Feedback', 'width=600, height=600');
      html2canvas__WEBPACK_IMPORTED_MODULE_0___default()(document.body).then(function (canvas) {
        var image = canvas.toDataURL("image/png");

        newWindow.onload = function () {
          var height = newWindow.document.getElementById('content').clientHeight;
          newWindow.resizeTo(600, height + 70);
          var input = newWindow.document.getElementById('core-feedback-screen');
          input.value = image;
        };
      });
    }
    /*
     * Delete confirmation dialog
     */

  }, {
    key: "initDeleteConfirmation",
    value: function initDeleteConfirmation(callback) {
      $(".delete-btn").click(function (event) {
        event.preventDefault();
        var btn = $(event.currentTarget);
        var title = btn.attr('title');

        if (title) {
          $('#deleteConfirmation-msg').html(btn.attr("title"));
        }

        _Common__WEBPACK_IMPORTED_MODULE_2__["default"].deleteBtn().html(btn.html());
        _Common__WEBPACK_IMPORTED_MODULE_2__["default"].deleteBtn().off('click').click(function (event) {
          btn.trigger('confirmed');
          $(event.currentTarget).prop('disabled', true);
          var url = btn.attr('href'); //var callback = btn.attr('data-callback');

          if (callback) {
            $.ajax({
              url: url,
              type: 'DELETE',
              error: function error() {
                _Common__WEBPACK_IMPORTED_MODULE_2__["default"].showAlertDialog('danger', 'Action cannot be performed');
              },
              success: function success(result) {
                callback(btn, result);
              },
              complete: function complete() {
                _Common__WEBPACK_IMPORTED_MODULE_2__["default"].hideConfirmation();
              }
            });
          } else {
            window.location.href = url;
          }
        });
        _Common__WEBPACK_IMPORTED_MODULE_2__["default"].deleteDiv().appendTo("body").modal('show');
      });
    }
  }, {
    key: "initDatepicker",
    // jquery ui
    value: function initDatepicker() {
      var element = $(".datepicker");

      if (element.length > 0) {
        element.datepicker({
          dateFormat: 'dd.mm.yy'
        });
      }
    }
  }, {
    key: "initSelectizeTag",
    value: function initSelectizeTag() {
      var element = $(".selectize-tag");

      if (element.length > 0) {
        // remove bootstrap3 form class
        element.removeClass('form-control');
        var options = {
          plugins: ['remove_button'],
          delimiter: '|',
          persist: false
        };

        if (element.attr('data-new') != 0) {
          options.create = function (input) {
            return {
              value: input,
              text: input
            };
          };
        }

        element.selectize(options);
      }
    }
  }, {
    key: "initTable",
    value: function initTable() {
      var element = $('.dataTable');

      if (element.length > 0) {
        element.DataTable({
          "order": [[0, "desc"]],
          "columnDefs": [{
            "orderable": false,
            "targets": -1
          }],
          "language": {
            search: "<span class='fas fa-search'></span> _INPUT_"
          }
        });
      }
    }
  }, {
    key: "initTooltip",
    value: function initTooltip() {
      $('[data-toggle="tooltip"]').tooltip();
    }
  }, {
    key: "initPopover",
    value: function initPopover() {
      $('[data-toggle="popover"]').popover();
    }
  }, {
    key: "initDropDown",
    value: function initDropDown() {
      $('.navbar-nav').smartmenus({
        'showTimeout': 0,
        'subIndicators': false
      });
    }
  }, {
    key: "_isDropDown",
    value: function _isDropDown(menu) {
      var parent = menu.parent();
      return parent.hasClass('dropdown') || parent.hasClass('dropdown-submenu');
    }
  }, {
    key: "cookieConsent",
    value: function cookieConsent() {
      var consent = _Common__WEBPACK_IMPORTED_MODULE_2__["default"].getCookie('consent');

      if (consent != 1 && $('#page').length > 0) {
        var banner = $('<div class="alert alert-warning alert-dismissible fade show" ' + 'role="alert" style="position: fixed; bottom: 0; right: 0;' + 'margin-bottom: 0;">This website uses cookies to ensure you ' + 'get the best experience on our website. <button type="button" ' + 'id="rage-cookie" class="btn btn-success" data-dismiss="alert" ' + 'aria-label="Close">I understand</button></div>');
        $("body").append(banner);
        $("#rage-cookie").click(function () {
          _Common__WEBPACK_IMPORTED_MODULE_2__["default"].setCookie('consent', 1, 365);
        });
      }
    }
    /*
     * setting for active collapse and tab
     */

  }, {
    key: "openCollapseAndTab",
    value: function openCollapseAndTab() {
      var hash = window.location.hash;
      var state = $('#page-active-tab');

      if (hash.length > 0) {
        hash = hash.substr(1);
        var anchor = $('#' + hash);

        if (anchor.length > 0) {
          if (anchor.hasClass('tab-pane')) {
            $('a[href="#' + hash + '"]').tab('show');
          } else if (anchor.hasClass('collapse')) {
            anchor.collapse('show');
          }

          if (state.length > 0) {
            state.val(hash);
          }
        }
      }

      $('.nav-tabs a').click(function (e) {
        hash = e.currentTarget.hash;
        window.location.hash = hash;

        if (state.length > 0) {
          state.val(hash.substr(1));
        }
      });
    }
  }, {
    key: "initLoadingDialog",
    value: function initLoadingDialog() {
      var element = $('#loadingDialog-div');

      if (element.length > 0) {
        $(document).on('loading.start', function () {
          element.appendTo("body").modal('show');
        });
        $(document).on('loading.stop', function () {
          element.modal('hide');
        });
      }
    }
  }, {
    key: "initSelectAll",
    value: function initSelectAll() {
      var selectAll = $('.checkbox-select-all');

      if (selectAll.length > 0) {
        selectAll.change(function (event) {
          var element = $(event.currentTarget);
          element.parent().parent().parent().parent().find('.checkbox-select').prop('checked', element.prop('checked'));
        });
      }
    }
  }, {
    key: "initImportTable",
    value: function initImportTable() {
      var importTable = $('.import-table');

      if (importTable.length > 0) {
        importTable.css('width', '100%');
        var columnDefs = [{
          "orderable": false,
          "targets": 0
        }];

        if (importTable.attr('data-button')) {
          columnDefs.push({
            "orderable": false,
            "targets": -1
          });
        }

        importTable.DataTable({
          "order": [[1, "desc"]],
          "columnDefs": columnDefs
        });
        importTable.on('page.dt', function (event) {
          $(event.currentTarget).find('.checkbox-select-all').prop('checked', false);
        });
      }
    }
  }, {
    key: "initLog",
    value: function initLog() {
      $('.log-click').mouseup(function (event) {
        _Common__WEBPACK_IMPORTED_MODULE_2__["default"].updateLog(_Common__WEBPACK_IMPORTED_MODULE_2__["default"].uuidv4(), 'click', $(event.currentTarget).text());
      });
      $('.log-download').mouseup(function (event) {
        _Common__WEBPACK_IMPORTED_MODULE_2__["default"].updateLog(_Common__WEBPACK_IMPORTED_MODULE_2__["default"].uuidv4(), 'download', $(event.currentTarget).text());
      });
    }
  }, {
    key: "stopLoading",
    value: function stopLoading() {
      var element = $('.se-pre-con');

      if (element.length > 0) {
        element.fadeOut("slow");
      }
    }
  }, {
    key: "initAlertDialog",
    value: function initAlertDialog() {
      var alertDiv = _Common__WEBPACK_IMPORTED_MODULE_2__["default"].alertDiv();
      alertDiv.find('.close').click(function () {
        alertDiv.animate({
          bottom: '-50px'
        }, 500, 'swing', function () {
          alertDiv.hide();
        });
      });
      var flash = _Common__WEBPACK_IMPORTED_MODULE_2__["default"].getCookie('flash');

      if (flash) {
        var json = JSON.parse(flash);

        if (json) {
          _Common__WEBPACK_IMPORTED_MODULE_2__["default"].showAlertDialog(json.type, json.text);
          _Common__WEBPACK_IMPORTED_MODULE_2__["default"].setCookie('flash', '');
        }
      }
    }
    /**
     * Fix the 'unfocusable' problem of collapsed-required input
     */

  }, {
    key: "initRequiredInput",
    value: function initRequiredInput() {
      $('.panel-collapse input').each(function (index, element) {
        element.addEventListener('invalid', function (e) {
          var input = $(e.currentTarget);
          var panel = input.closest(".panel-collapse");

          if (!panel.hasClass('in')) {
            panel.collapse('show');
          }
        });
      });
    }
  }]);

  return Layout;
}();

/* harmony default export */ __webpack_exports__["default"] = (Layout);
$(document).ready(function () {
  Layout.initFeedback();
  Layout.initChatbot();
  Layout.initDeleteConfirmation();
  Layout.initDatepicker();
  Layout.initSelectizeTag();
  Layout.initTable();
  Layout.initTooltip();
  Layout.initPopover(); //Layout.initDropDown();

  Layout.cookieConsent();
  Layout.openCollapseAndTab();
  Layout.initLoadingDialog();
  Layout.initSelectAll();
  Layout.initImportTable();
  Layout.initLog();
  Layout.initRequiredInput();
  bs_custom_file_input__WEBPACK_IMPORTED_MODULE_1___default.a.init();
});
$(window).on("load", function () {
  Layout.stopLoading();
  Layout.initAlertDialog();
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQ29tbW9uL0xheW91dC5qcyJdLCJuYW1lcyI6WyJMYXlvdXQiLCJiYW5uZXIiLCIkIiwiY2xpY2siLCJldmVudCIsInByZXZlbnREZWZhdWx0Iiwic2hvd0ZlZWRiYWNrV2luZG93IiwibGVuZ3RoIiwiYXBwZW5kIiwidXJsIiwiYXR0ciIsIm5ld1dpbmRvdyIsIndpbmRvdyIsIm9wZW4iLCJSb3V0aW5nIiwiZ2VuZXJhdGUiLCJodG1sMmNhbnZhcyIsImRvY3VtZW50IiwiYm9keSIsInRoZW4iLCJjYW52YXMiLCJpbWFnZSIsInRvRGF0YVVSTCIsIm9ubG9hZCIsImhlaWdodCIsImdldEVsZW1lbnRCeUlkIiwiY2xpZW50SGVpZ2h0IiwicmVzaXplVG8iLCJpbnB1dCIsInZhbHVlIiwiY2FsbGJhY2siLCJidG4iLCJjdXJyZW50VGFyZ2V0IiwidGl0bGUiLCJodG1sIiwiQ29tbW9uIiwiZGVsZXRlQnRuIiwib2ZmIiwidHJpZ2dlciIsInByb3AiLCJhamF4IiwidHlwZSIsImVycm9yIiwic2hvd0FsZXJ0RGlhbG9nIiwic3VjY2VzcyIsInJlc3VsdCIsImNvbXBsZXRlIiwiaGlkZUNvbmZpcm1hdGlvbiIsImxvY2F0aW9uIiwiaHJlZiIsImRlbGV0ZURpdiIsImFwcGVuZFRvIiwibW9kYWwiLCJlbGVtZW50IiwiZGF0ZXBpY2tlciIsImRhdGVGb3JtYXQiLCJyZW1vdmVDbGFzcyIsIm9wdGlvbnMiLCJwbHVnaW5zIiwiZGVsaW1pdGVyIiwicGVyc2lzdCIsImNyZWF0ZSIsInRleHQiLCJzZWxlY3RpemUiLCJEYXRhVGFibGUiLCJzZWFyY2giLCJ0b29sdGlwIiwicG9wb3ZlciIsInNtYXJ0bWVudXMiLCJtZW51IiwicGFyZW50IiwiaGFzQ2xhc3MiLCJjb25zZW50IiwiZ2V0Q29va2llIiwic2V0Q29va2llIiwiaGFzaCIsInN0YXRlIiwic3Vic3RyIiwiYW5jaG9yIiwidGFiIiwiY29sbGFwc2UiLCJ2YWwiLCJlIiwib24iLCJzZWxlY3RBbGwiLCJjaGFuZ2UiLCJmaW5kIiwiaW1wb3J0VGFibGUiLCJjc3MiLCJjb2x1bW5EZWZzIiwicHVzaCIsIm1vdXNldXAiLCJ1cGRhdGVMb2ciLCJ1dWlkdjQiLCJmYWRlT3V0IiwiYWxlcnREaXYiLCJhbmltYXRlIiwiYm90dG9tIiwiaGlkZSIsImZsYXNoIiwianNvbiIsIkpTT04iLCJwYXJzZSIsImVhY2giLCJpbmRleCIsImFkZEV2ZW50TGlzdGVuZXIiLCJwYW5lbCIsImNsb3Nlc3QiLCJyZWFkeSIsImluaXRGZWVkYmFjayIsImluaXRDaGF0Ym90IiwiaW5pdERlbGV0ZUNvbmZpcm1hdGlvbiIsImluaXREYXRlcGlja2VyIiwiaW5pdFNlbGVjdGl6ZVRhZyIsImluaXRUYWJsZSIsImluaXRUb29sdGlwIiwiaW5pdFBvcG92ZXIiLCJjb29raWVDb25zZW50Iiwib3BlbkNvbGxhcHNlQW5kVGFiIiwiaW5pdExvYWRpbmdEaWFsb2ciLCJpbml0U2VsZWN0QWxsIiwiaW5pdEltcG9ydFRhYmxlIiwiaW5pdExvZyIsImluaXRSZXF1aXJlZElucHV0IiwiYnNDdXN0b21GaWxlSW5wdXQiLCJpbml0Iiwic3RvcExvYWRpbmciLCJpbml0QWxlcnREaWFsb2ciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7OztBQWNBO0FBQ0E7QUFDQTtBQUNBOztJQUVNQSxNOzs7Ozs7O21DQUNvQjtBQUNsQixVQUFNQyxNQUFNLEdBQUdDLENBQUMsQ0FDWix5UkFEWSxDQUFoQjtBQUlBRCxZQUFNLENBQUNFLEtBQVAsQ0FBYSxVQUFDQyxLQUFELEVBQVc7QUFDcEJBLGFBQUssQ0FBQ0MsY0FBTjtBQUNBTCxjQUFNLENBQUNNLGtCQUFQO0FBQ0gsT0FIRDs7QUFLQSxVQUFJSixDQUFDLENBQUMsYUFBRCxDQUFELENBQWlCSyxNQUFqQixHQUEwQixDQUE5QixFQUFpQztBQUM3QkwsU0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVTSxNQUFWLENBQWlCUCxNQUFqQjtBQUNIO0FBQ0o7OztrQ0FFb0I7QUFDakIsVUFBTVEsR0FBRyxHQUFHUCxDQUFDLENBQUMsU0FBRCxDQUFELENBQWFRLElBQWIsQ0FBa0IsY0FBbEIsQ0FBWjs7QUFFQSxVQUFJRCxHQUFKLEVBQVM7QUFDTCxZQUFNUixNQUFNLEdBQUdDLENBQUMsQ0FDWixjQUFjTyxHQUFkLEdBQW9CLDZJQUFwQixHQUNBLHFEQURBLEdBRUEsTUFIWSxDQUFoQjs7QUFNQSxZQUFJUCxDQUFDLENBQUMsYUFBRCxDQUFELENBQWlCSyxNQUFqQixHQUEwQixDQUE5QixFQUFpQztBQUM3QkwsV0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVTSxNQUFWLENBQWlCUCxNQUFqQjtBQUNIO0FBQ0o7QUFDSjs7O3lDQUUyQjtBQUN4QixVQUFJVSxTQUFTLEdBQUdDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZQyxPQUFPLENBQUNDLFFBQVIsQ0FBaUIsbUJBQWpCLENBQVosRUFBbUQsZUFBbkQsRUFBb0UsdUJBQXBFLENBQWhCO0FBRUFDLHdEQUFXLENBQUNDLFFBQVEsQ0FBQ0MsSUFBVixDQUFYLENBQTJCQyxJQUEzQixDQUFnQyxVQUFDQyxNQUFELEVBQVk7QUFDeEMsWUFBTUMsS0FBSyxHQUFHRCxNQUFNLENBQUNFLFNBQVAsQ0FBaUIsV0FBakIsQ0FBZDs7QUFFQVgsaUJBQVMsQ0FBQ1ksTUFBVixHQUFtQixZQUFNO0FBQ3JCLGNBQU1DLE1BQU0sR0FBR2IsU0FBUyxDQUFDTSxRQUFWLENBQW1CUSxjQUFuQixDQUFrQyxTQUFsQyxFQUE2Q0MsWUFBNUQ7QUFDQWYsbUJBQVMsQ0FBQ2dCLFFBQVYsQ0FBbUIsR0FBbkIsRUFBd0JILE1BQU0sR0FBRyxFQUFqQztBQUVBLGNBQU1JLEtBQUssR0FBR2pCLFNBQVMsQ0FBQ00sUUFBVixDQUFtQlEsY0FBbkIsQ0FBa0Msc0JBQWxDLENBQWQ7QUFDQUcsZUFBSyxDQUFDQyxLQUFOLEdBQWNSLEtBQWQ7QUFDSCxTQU5EO0FBT0gsT0FWRDtBQVdIO0FBR0Q7Ozs7OzsyQ0FHOEJTLFEsRUFBVTtBQUNwQzVCLE9BQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJDLEtBQWpCLENBQXVCLFVBQUNDLEtBQUQsRUFBVztBQUM5QkEsYUFBSyxDQUFDQyxjQUFOO0FBRUEsWUFBSTBCLEdBQUcsR0FBRzdCLENBQUMsQ0FBQ0UsS0FBSyxDQUFDNEIsYUFBUCxDQUFYO0FBQ0EsWUFBTUMsS0FBSyxHQUFHRixHQUFHLENBQUNyQixJQUFKLENBQVMsT0FBVCxDQUFkOztBQUVBLFlBQUl1QixLQUFKLEVBQVc7QUFDUC9CLFdBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCZ0MsSUFBN0IsQ0FBa0NILEdBQUcsQ0FBQ3JCLElBQUosQ0FBUyxPQUFULENBQWxDO0FBQ0g7O0FBRUR5Qix1REFBTSxDQUFDQyxTQUFQLEdBQW1CRixJQUFuQixDQUF3QkgsR0FBRyxDQUFDRyxJQUFKLEVBQXhCO0FBQ0FDLHVEQUFNLENBQUNDLFNBQVAsR0FBbUJDLEdBQW5CLENBQXVCLE9BQXZCLEVBQWdDbEMsS0FBaEMsQ0FBc0MsVUFBQ0MsS0FBRCxFQUFXO0FBRTdDMkIsYUFBRyxDQUFDTyxPQUFKLENBQVksV0FBWjtBQUNBcEMsV0FBQyxDQUFDRSxLQUFLLENBQUM0QixhQUFQLENBQUQsQ0FBdUJPLElBQXZCLENBQTRCLFVBQTVCLEVBQXdDLElBQXhDO0FBRUEsY0FBTTlCLEdBQUcsR0FBR3NCLEdBQUcsQ0FBQ3JCLElBQUosQ0FBUyxNQUFULENBQVosQ0FMNkMsQ0FNN0M7O0FBQ0EsY0FBSW9CLFFBQUosRUFBYztBQUNWNUIsYUFBQyxDQUFDc0MsSUFBRixDQUFPO0FBQ0gvQixpQkFBRyxFQUFFQSxHQURGO0FBRUhnQyxrQkFBSSxFQUFFLFFBRkg7QUFHSEMsbUJBQUssRUFBRSxpQkFBTTtBQUNUUCwrREFBTSxDQUFDUSxlQUFQLENBQXVCLFFBQXZCLEVBQWlDLDRCQUFqQztBQUNILGVBTEU7QUFNSEMscUJBQU8sRUFBRSxpQkFBQ0MsTUFBRCxFQUFZO0FBQ2pCZix3QkFBUSxDQUFDQyxHQUFELEVBQU1jLE1BQU4sQ0FBUjtBQUNILGVBUkU7QUFTSEMsc0JBQVEsRUFBRSxvQkFBTTtBQUNaWCwrREFBTSxDQUFDWSxnQkFBUDtBQUNIO0FBWEUsYUFBUDtBQWFILFdBZEQsTUFjTztBQUNIbkMsa0JBQU0sQ0FBQ29DLFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCeEMsR0FBdkI7QUFDSDtBQUNKLFNBeEJEO0FBeUJBMEIsdURBQU0sQ0FBQ2UsU0FBUCxHQUFtQkMsUUFBbkIsQ0FBNEIsTUFBNUIsRUFBb0NDLEtBQXBDLENBQTBDLE1BQTFDO0FBQ0gsT0FyQ0Q7QUFzQ0g7OztBQUVEO3FDQUN3QjtBQUNwQixVQUFJQyxPQUFPLEdBQUduRCxDQUFDLENBQUMsYUFBRCxDQUFmOztBQUVBLFVBQUltRCxPQUFPLENBQUM5QyxNQUFSLEdBQWlCLENBQXJCLEVBQXdCO0FBQ3BCOEMsZUFBTyxDQUFDQyxVQUFSLENBQW1CO0FBQ2ZDLG9CQUFVLEVBQUU7QUFERyxTQUFuQjtBQUdIO0FBQ0o7Ozt1Q0FFeUI7QUFDdEIsVUFBSUYsT0FBTyxHQUFHbkQsQ0FBQyxDQUFFLGdCQUFGLENBQWY7O0FBRUEsVUFBSW1ELE9BQU8sQ0FBQzlDLE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7QUFDcEI7QUFDQThDLGVBQU8sQ0FBQ0csV0FBUixDQUFvQixjQUFwQjtBQUVBLFlBQUlDLE9BQU8sR0FBRztBQUNWQyxpQkFBTyxFQUFFLENBQUMsZUFBRCxDQURDO0FBRVZDLG1CQUFTLEVBQUUsR0FGRDtBQUdWQyxpQkFBTyxFQUFFO0FBSEMsU0FBZDs7QUFNQSxZQUFJUCxPQUFPLENBQUMzQyxJQUFSLENBQWEsVUFBYixLQUE0QixDQUFoQyxFQUFtQztBQUMvQitDLGlCQUFPLENBQUNJLE1BQVIsR0FBaUIsVUFBQ2pDLEtBQUQsRUFBVztBQUN4QixtQkFBTztBQUNIQyxtQkFBSyxFQUFFRCxLQURKO0FBRUhrQyxrQkFBSSxFQUFFbEM7QUFGSCxhQUFQO0FBSUgsV0FMRDtBQU1IOztBQUVEeUIsZUFBTyxDQUFDVSxTQUFSLENBQWtCTixPQUFsQjtBQUNIO0FBQ0o7OztnQ0FFa0I7QUFDZixVQUFJSixPQUFPLEdBQUduRCxDQUFDLENBQUMsWUFBRCxDQUFmOztBQUVBLFVBQUltRCxPQUFPLENBQUM5QyxNQUFSLEdBQWlCLENBQXJCLEVBQXdCO0FBQ3BCOEMsZUFBTyxDQUFDVyxTQUFSLENBQWtCO0FBQ2QsbUJBQVMsQ0FBQyxDQUFFLENBQUYsRUFBSyxNQUFMLENBQUQsQ0FESztBQUVkLHdCQUFjLENBQ1Y7QUFBRSx5QkFBYSxLQUFmO0FBQXNCLHVCQUFXLENBQUM7QUFBbEMsV0FEVSxDQUZBO0FBS2Qsc0JBQVk7QUFDUkMsa0JBQU0sRUFBRTtBQURBO0FBTEUsU0FBbEI7QUFTSDtBQUNKOzs7a0NBRW9CO0FBQ2pCL0QsT0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJnRSxPQUE3QjtBQUNIOzs7a0NBRW9CO0FBQ2pCaEUsT0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJpRSxPQUE3QjtBQUNIOzs7bUNBRXFCO0FBQ2xCakUsT0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQmtFLFVBQWpCLENBQTRCO0FBQ3hCLHVCQUFlLENBRFM7QUFFeEIseUJBQWlCO0FBRk8sT0FBNUI7QUFJSDs7O2dDQUVrQkMsSSxFQUFNO0FBQ3JCLFVBQUlDLE1BQU0sR0FBR0QsSUFBSSxDQUFDQyxNQUFMLEVBQWI7QUFDQSxhQUFRQSxNQUFNLENBQUNDLFFBQVAsQ0FBZ0IsVUFBaEIsS0FDREQsTUFBTSxDQUFDQyxRQUFQLENBQWdCLGtCQUFoQixDQURQO0FBRUg7OztvQ0FFc0I7QUFDbkIsVUFBTUMsT0FBTyxHQUFHckMsK0NBQU0sQ0FBQ3NDLFNBQVAsQ0FBaUIsU0FBakIsQ0FBaEI7O0FBQ0EsVUFBSUQsT0FBTyxJQUFJLENBQVgsSUFBZ0J0RSxDQUFDLENBQUMsT0FBRCxDQUFELENBQVdLLE1BQVgsR0FBb0IsQ0FBeEMsRUFBMkM7QUFDdkMsWUFBTU4sTUFBTSxHQUFHQyxDQUFDLENBQ1osa0VBQ0EsMkRBREEsR0FFQSw2REFGQSxHQUdBLGdFQUhBLEdBSUEsZ0VBSkEsR0FLQSxnREFOWSxDQUFoQjtBQVFBQSxTQUFDLENBQUMsTUFBRCxDQUFELENBQVVNLE1BQVYsQ0FBaUJQLE1BQWpCO0FBRUFDLFNBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JDLEtBQWxCLENBQXdCLFlBQU07QUFDMUJnQyx5REFBTSxDQUFDdUMsU0FBUCxDQUFpQixTQUFqQixFQUE0QixDQUE1QixFQUErQixHQUEvQjtBQUNILFNBRkQ7QUFHSDtBQUNKO0FBRUQ7Ozs7Ozt5Q0FHNEI7QUFDeEIsVUFBSUMsSUFBSSxHQUFHL0QsTUFBTSxDQUFDb0MsUUFBUCxDQUFnQjJCLElBQTNCO0FBQ0EsVUFBTUMsS0FBSyxHQUFHMUUsQ0FBQyxDQUFDLGtCQUFELENBQWY7O0FBRUEsVUFBSXlFLElBQUksQ0FBQ3BFLE1BQUwsR0FBYyxDQUFsQixFQUFxQjtBQUNqQm9FLFlBQUksR0FBR0EsSUFBSSxDQUFDRSxNQUFMLENBQVksQ0FBWixDQUFQO0FBRUEsWUFBTUMsTUFBTSxHQUFHNUUsQ0FBQyxDQUFDLE1BQU15RSxJQUFQLENBQWhCOztBQUNBLFlBQUlHLE1BQU0sQ0FBQ3ZFLE1BQVAsR0FBZ0IsQ0FBcEIsRUFBdUI7QUFDbkIsY0FBSXVFLE1BQU0sQ0FBQ1AsUUFBUCxDQUFnQixVQUFoQixDQUFKLEVBQWlDO0FBQzdCckUsYUFBQyxDQUFDLGNBQWF5RSxJQUFiLEdBQW9CLElBQXJCLENBQUQsQ0FBNEJJLEdBQTVCLENBQWdDLE1BQWhDO0FBQ0gsV0FGRCxNQUVPLElBQUlELE1BQU0sQ0FBQ1AsUUFBUCxDQUFnQixVQUFoQixDQUFKLEVBQWlDO0FBQ3BDTyxrQkFBTSxDQUFDRSxRQUFQLENBQWdCLE1BQWhCO0FBQ0g7O0FBRUQsY0FBSUosS0FBSyxDQUFDckUsTUFBTixHQUFlLENBQW5CLEVBQXNCO0FBQ2xCcUUsaUJBQUssQ0FBQ0ssR0FBTixDQUFVTixJQUFWO0FBQ0g7QUFDSjtBQUNKOztBQUVEekUsT0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQkMsS0FBakIsQ0FBdUIsVUFBQytFLENBQUQsRUFBTztBQUMxQlAsWUFBSSxHQUFHTyxDQUFDLENBQUNsRCxhQUFGLENBQWdCMkMsSUFBdkI7QUFDQS9ELGNBQU0sQ0FBQ29DLFFBQVAsQ0FBZ0IyQixJQUFoQixHQUF1QkEsSUFBdkI7O0FBQ0EsWUFBSUMsS0FBSyxDQUFDckUsTUFBTixHQUFlLENBQW5CLEVBQXNCO0FBQ2xCcUUsZUFBSyxDQUFDSyxHQUFOLENBQVVOLElBQUksQ0FBQ0UsTUFBTCxDQUFZLENBQVosQ0FBVjtBQUNIO0FBQ0osT0FORDtBQVNIOzs7d0NBRTBCO0FBQ3ZCLFVBQUl4QixPQUFPLEdBQUduRCxDQUFDLENBQUMsb0JBQUQsQ0FBZjs7QUFFQSxVQUFJbUQsT0FBTyxDQUFDOUMsTUFBUixHQUFpQixDQUFyQixFQUF3QjtBQUNwQkwsU0FBQyxDQUFDZSxRQUFELENBQUQsQ0FBWWtFLEVBQVosQ0FBZSxlQUFmLEVBQWdDLFlBQU07QUFDbEM5QixpQkFBTyxDQUFDRixRQUFSLENBQWlCLE1BQWpCLEVBQXlCQyxLQUF6QixDQUErQixNQUEvQjtBQUNILFNBRkQ7QUFJQWxELFNBQUMsQ0FBQ2UsUUFBRCxDQUFELENBQVlrRSxFQUFaLENBQWUsY0FBZixFQUErQixZQUFNO0FBQ2pDOUIsaUJBQU8sQ0FBQ0QsS0FBUixDQUFjLE1BQWQ7QUFDSCxTQUZEO0FBR0g7QUFDSjs7O29DQUVzQjtBQUNuQixVQUFJZ0MsU0FBUyxHQUFHbEYsQ0FBQyxDQUFDLHNCQUFELENBQWpCOztBQUVBLFVBQUlrRixTQUFTLENBQUM3RSxNQUFWLEdBQW1CLENBQXZCLEVBQTBCO0FBQ3RCNkUsaUJBQVMsQ0FBQ0MsTUFBVixDQUFpQixVQUFDakYsS0FBRCxFQUFXO0FBQ3hCLGNBQUlpRCxPQUFPLEdBQUduRCxDQUFDLENBQUNFLEtBQUssQ0FBQzRCLGFBQVAsQ0FBZjtBQUNBcUIsaUJBQU8sQ0FBQ2lCLE1BQVIsR0FBaUJBLE1BQWpCLEdBQTBCQSxNQUExQixHQUFtQ0EsTUFBbkMsR0FBNENnQixJQUE1QyxDQUFpRCxrQkFBakQsRUFBcUUvQyxJQUFyRSxDQUEwRSxTQUExRSxFQUFxRmMsT0FBTyxDQUFDZCxJQUFSLENBQWEsU0FBYixDQUFyRjtBQUNILFNBSEQ7QUFJSDtBQUNKOzs7c0NBRXdCO0FBQ3JCLFVBQUlnRCxXQUFXLEdBQUdyRixDQUFDLENBQUMsZUFBRCxDQUFuQjs7QUFFQSxVQUFJcUYsV0FBVyxDQUFDaEYsTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUN4QmdGLG1CQUFXLENBQUNDLEdBQVosQ0FBZ0IsT0FBaEIsRUFBeUIsTUFBekI7QUFFQSxZQUFNQyxVQUFVLEdBQUcsQ0FBRTtBQUNqQix1QkFBYSxLQURJO0FBRWpCLHFCQUFXO0FBRk0sU0FBRixDQUFuQjs7QUFLQSxZQUFJRixXQUFXLENBQUM3RSxJQUFaLENBQWlCLGFBQWpCLENBQUosRUFBcUM7QUFDakMrRSxvQkFBVSxDQUFDQyxJQUFYLENBQWdCO0FBQ1oseUJBQWEsS0FERDtBQUVaLHVCQUFXLENBQUM7QUFGQSxXQUFoQjtBQUlIOztBQUVESCxtQkFBVyxDQUFDdkIsU0FBWixDQUFzQjtBQUNsQixtQkFBUyxDQUFDLENBQUUsQ0FBRixFQUFLLE1BQUwsQ0FBRCxDQURTO0FBRWxCLHdCQUFjeUI7QUFGSSxTQUF0QjtBQUtBRixtQkFBVyxDQUFDSixFQUFaLENBQWdCLFNBQWhCLEVBQTJCLFVBQUMvRSxLQUFELEVBQVc7QUFDbENGLFdBQUMsQ0FBQ0UsS0FBSyxDQUFDNEIsYUFBUCxDQUFELENBQXVCc0QsSUFBdkIsQ0FBNEIsc0JBQTVCLEVBQW9EL0MsSUFBcEQsQ0FBeUQsU0FBekQsRUFBb0UsS0FBcEU7QUFDSCxTQUZEO0FBR0g7QUFDSjs7OzhCQUVnQjtBQUNickMsT0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQnlGLE9BQWhCLENBQXdCLFVBQUN2RixLQUFELEVBQVc7QUFDL0IrQix1REFBTSxDQUFDeUQsU0FBUCxDQUFpQnpELCtDQUFNLENBQUMwRCxNQUFQLEVBQWpCLEVBQWtDLE9BQWxDLEVBQTJDM0YsQ0FBQyxDQUFDRSxLQUFLLENBQUM0QixhQUFQLENBQUQsQ0FBdUI4QixJQUF2QixFQUEzQztBQUNILE9BRkQ7QUFJQTVELE9BQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJ5RixPQUFuQixDQUEyQixVQUFDdkYsS0FBRCxFQUFXO0FBQ2xDK0IsdURBQU0sQ0FBQ3lELFNBQVAsQ0FBaUJ6RCwrQ0FBTSxDQUFDMEQsTUFBUCxFQUFqQixFQUFrQyxVQUFsQyxFQUE4QzNGLENBQUMsQ0FBQ0UsS0FBSyxDQUFDNEIsYUFBUCxDQUFELENBQXVCOEIsSUFBdkIsRUFBOUM7QUFDSCxPQUZEO0FBR0g7OztrQ0FFb0I7QUFDakIsVUFBSVQsT0FBTyxHQUFHbkQsQ0FBQyxDQUFDLGFBQUQsQ0FBZjs7QUFFQSxVQUFJbUQsT0FBTyxDQUFDOUMsTUFBUixHQUFpQixDQUFyQixFQUF3QjtBQUNwQjhDLGVBQU8sQ0FBQ3lDLE9BQVIsQ0FBZ0IsTUFBaEI7QUFDSDtBQUNKOzs7c0NBRXdCO0FBQ3JCLFVBQUlDLFFBQVEsR0FBRzVELCtDQUFNLENBQUM0RCxRQUFQLEVBQWY7QUFFQUEsY0FBUSxDQUFDVCxJQUFULENBQWMsUUFBZCxFQUF3Qm5GLEtBQXhCLENBQThCLFlBQU07QUFDaEM0RixnQkFBUSxDQUFDQyxPQUFULENBQWlCO0FBQUNDLGdCQUFNLEVBQUM7QUFBUixTQUFqQixFQUFtQyxHQUFuQyxFQUF3QyxPQUF4QyxFQUFpRCxZQUFNO0FBQ25ERixrQkFBUSxDQUFDRyxJQUFUO0FBQ0gsU0FGRDtBQUdILE9BSkQ7QUFNQSxVQUFNQyxLQUFLLEdBQUdoRSwrQ0FBTSxDQUFDc0MsU0FBUCxDQUFpQixPQUFqQixDQUFkOztBQUNBLFVBQUkwQixLQUFKLEVBQVc7QUFDUCxZQUFNQyxJQUFJLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXSCxLQUFYLENBQWI7O0FBQ0EsWUFBSUMsSUFBSixFQUFVO0FBQ05qRSx5REFBTSxDQUFDUSxlQUFQLENBQXVCeUQsSUFBSSxDQUFDM0QsSUFBNUIsRUFBa0MyRCxJQUFJLENBQUN0QyxJQUF2QztBQUNBM0IseURBQU0sQ0FBQ3VDLFNBQVAsQ0FBaUIsT0FBakIsRUFBMEIsRUFBMUI7QUFDSDtBQUNKO0FBQ0o7QUFFRDs7Ozs7O3dDQUcyQjtBQUN2QnhFLE9BQUMsQ0FBQyx1QkFBRCxDQUFELENBQTJCcUcsSUFBM0IsQ0FBZ0MsVUFBQ0MsS0FBRCxFQUFRbkQsT0FBUixFQUFvQjtBQUNoREEsZUFBTyxDQUFDb0QsZ0JBQVIsQ0FBeUIsU0FBekIsRUFBb0MsVUFBQ3ZCLENBQUQsRUFBTztBQUN2QyxjQUFNdEQsS0FBSyxHQUFHMUIsQ0FBQyxDQUFDZ0YsQ0FBQyxDQUFDbEQsYUFBSCxDQUFmO0FBQ0EsY0FBTTBFLEtBQUssR0FBRzlFLEtBQUssQ0FBQytFLE9BQU4sQ0FBYyxpQkFBZCxDQUFkOztBQUNBLGNBQUksQ0FBQ0QsS0FBSyxDQUFDbkMsUUFBTixDQUFlLElBQWYsQ0FBTCxFQUEyQjtBQUN2Qm1DLGlCQUFLLENBQUMxQixRQUFOLENBQWUsTUFBZjtBQUNIO0FBQ0osU0FORDtBQU9ILE9BUkQ7QUFTSDs7Ozs7O0FBR1VoRixxRUFBZjtBQUVBRSxDQUFDLENBQUVlLFFBQUYsQ0FBRCxDQUFjMkYsS0FBZCxDQUFvQixZQUFNO0FBQ3RCNUcsUUFBTSxDQUFDNkcsWUFBUDtBQUNBN0csUUFBTSxDQUFDOEcsV0FBUDtBQUNBOUcsUUFBTSxDQUFDK0csc0JBQVA7QUFDQS9HLFFBQU0sQ0FBQ2dILGNBQVA7QUFDQWhILFFBQU0sQ0FBQ2lILGdCQUFQO0FBQ0FqSCxRQUFNLENBQUNrSCxTQUFQO0FBQ0FsSCxRQUFNLENBQUNtSCxXQUFQO0FBQ0FuSCxRQUFNLENBQUNvSCxXQUFQLEdBUnNCLENBU3RCOztBQUNBcEgsUUFBTSxDQUFDcUgsYUFBUDtBQUNBckgsUUFBTSxDQUFDc0gsa0JBQVA7QUFDQXRILFFBQU0sQ0FBQ3VILGlCQUFQO0FBQ0F2SCxRQUFNLENBQUN3SCxhQUFQO0FBQ0F4SCxRQUFNLENBQUN5SCxlQUFQO0FBQ0F6SCxRQUFNLENBQUMwSCxPQUFQO0FBQ0ExSCxRQUFNLENBQUMySCxpQkFBUDtBQUNBQyw2REFBaUIsQ0FBQ0MsSUFBbEI7QUFDSCxDQWxCRDtBQW9CQTNILENBQUMsQ0FBQ1UsTUFBRCxDQUFELENBQVV1RSxFQUFWLENBQWEsTUFBYixFQUFxQixZQUFNO0FBQ3ZCbkYsUUFBTSxDQUFDOEgsV0FBUDtBQUNBOUgsUUFBTSxDQUFDK0gsZUFBUDtBQUNILENBSEQsRSIsImZpbGUiOiJBcmNoaXZlL1JlY29yZH5BdXRoZW50aWNhdGlvbi9Vc2VyfkNvbW1vbi9MYXlvdXR+Q29udGVudC9Db250ZW50TGlzdH5Db250ZW50L0Rpc3BsYXl+Q29yZS9GZWVkYmFja35Ffjc1MTE2NTBkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKiAgQ29weXJpZ2h0IG5vdGljZVxyXG4gKlxyXG4gKiAgKGMpIDIwMTggRlRLIGUuVi4gPGZ0ay5kZT5cclxuICogIEFsbCByaWdodHMgcmVzZXJ2ZWRcclxuICpcclxuICogIFRoaXMgc2NyaXB0IGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAqICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gKiAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gKiAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuICpcclxuICogIFRoaXMgY29weXJpZ2h0IG5vdGljZSBNVVNUIEFQUEVBUiBpbiBhbGwgY29waWVzIG9mIHRoZSBzY3JpcHQhXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG4vLyBTY3JlZW5zaG90IGNhcHR1cmVcclxuaW1wb3J0IGh0bWwyY2FudmFzIGZyb20gJ2h0bWwyY2FudmFzJztcclxuaW1wb3J0IGJzQ3VzdG9tRmlsZUlucHV0IGZyb20gJ2JzLWN1c3RvbS1maWxlLWlucHV0JztcclxuaW1wb3J0IENvbW1vbiBmcm9tIFwiLi9Db21tb25cIjtcclxuXHJcbmNsYXNzIExheW91dCB7XHJcbiAgICBzdGF0aWMgaW5pdEZlZWRiYWNrKCkge1xyXG4gICAgICAgIGNvbnN0IGJhbm5lciA9ICQoXHJcbiAgICAgICAgICAgICc8YSBocmVmPVwiI1wiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIiB0aXRsZT1cIlNlbmQgeW91ciBzdWdnZXN0aW9uIG9yIHJlcG9ydCBhIGJ1Zy9lcnJvclwiIHN0eWxlPVwicG9zaXRpb246IGZpeGVkOyBib3R0b206IDEwcHg7IHJpZ2h0OiAxMHB4OyBtYXJnaW4tYm90dG9tOiAwOyBib3JkZXItcmFkaXVzOiA1MCU7XCI+PHNwYW4gY2xhc3M9XCJmYXMgZmEtZW52ZWxvcGVcIiBzdHlsZT1cInRvcDogMnB4O1wiPjwvc3Bhbj48L2E+J1xyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIGJhbm5lci5jbGljaygoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgTGF5b3V0LnNob3dGZWVkYmFja1dpbmRvdygpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAoJChcIi5zZS1wcmUtY29uXCIpLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgJChcImJvZHlcIikuYXBwZW5kKGJhbm5lcik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0Q2hhdGJvdCgpIHtcclxuICAgICAgICBjb25zdCB1cmwgPSAkKCcuZm9vdGVyJykuYXR0cignZGF0YS1jaGF0Ym90Jyk7XHJcblxyXG4gICAgICAgIGlmICh1cmwpIHtcclxuICAgICAgICAgICAgY29uc3QgYmFubmVyID0gJChcclxuICAgICAgICAgICAgICAgICc8YSBocmVmPVwiJyArIHVybCArICdcIiB0YXJnZXQ9XCJfYmxhbmtcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiBkYXRhLXBsYWNlbWVudD1cInRvcFwiIHRpdGxlPVwiQ2hhdCB3aXRoIG91ciBib3RcIiBzdHlsZT1cInBvc2l0aW9uOiBmaXhlZDsgYm90dG9tOiA4MHB4OyByaWdodDogMTNweDtcIj4nICtcclxuICAgICAgICAgICAgICAgICc8aW1nIHNyYz1cIi9pbWFnZXMvcm9ib3QucG5nXCIgc3R5bGU9XCJoZWlnaHQ6IDQ0cHg7XCI+JyArXHJcbiAgICAgICAgICAgICAgICAnPC9hPidcclxuICAgICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgICAgIGlmICgkKFwiLnNlLXByZS1jb25cIikubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgJChcImJvZHlcIikuYXBwZW5kKGJhbm5lcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIHNob3dGZWVkYmFja1dpbmRvdygpIHtcclxuICAgICAgICBsZXQgbmV3V2luZG93ID0gd2luZG93Lm9wZW4oUm91dGluZy5nZW5lcmF0ZSgnY29yZV9mZWVkYmFja19uZXcnKSwgJ1VzZXIgRmVlZGJhY2snLCAnd2lkdGg9NjAwLCBoZWlnaHQ9NjAwJyk7XHJcblxyXG4gICAgICAgIGh0bWwyY2FudmFzKGRvY3VtZW50LmJvZHkpLnRoZW4oKGNhbnZhcykgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBpbWFnZSA9IGNhbnZhcy50b0RhdGFVUkwoXCJpbWFnZS9wbmdcIik7XHJcblxyXG4gICAgICAgICAgICBuZXdXaW5kb3cub25sb2FkID0gKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaGVpZ2h0ID0gbmV3V2luZG93LmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjb250ZW50JykuY2xpZW50SGVpZ2h0O1xyXG4gICAgICAgICAgICAgICAgbmV3V2luZG93LnJlc2l6ZVRvKDYwMCwgaGVpZ2h0ICsgNzApO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IGlucHV0ID0gbmV3V2luZG93LmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjb3JlLWZlZWRiYWNrLXNjcmVlbicpO1xyXG4gICAgICAgICAgICAgICAgaW5wdXQudmFsdWUgPSBpbWFnZTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgLypcclxuICAgICAqIERlbGV0ZSBjb25maXJtYXRpb24gZGlhbG9nXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBpbml0RGVsZXRlQ29uZmlybWF0aW9uKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgJChcIi5kZWxldGUtYnRuXCIpLmNsaWNrKChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgbGV0IGJ0biA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHRpdGxlID0gYnRuLmF0dHIoJ3RpdGxlJyk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGl0bGUpIHtcclxuICAgICAgICAgICAgICAgICQoJyNkZWxldGVDb25maXJtYXRpb24tbXNnJykuaHRtbChidG4uYXR0cihcInRpdGxlXCIpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgQ29tbW9uLmRlbGV0ZUJ0bigpLmh0bWwoYnRuLmh0bWwoKSk7XHJcbiAgICAgICAgICAgIENvbW1vbi5kZWxldGVCdG4oKS5vZmYoJ2NsaWNrJykuY2xpY2soKGV2ZW50KSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgYnRuLnRyaWdnZXIoJ2NvbmZpcm1lZCcpO1xyXG4gICAgICAgICAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KS5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHVybCA9IGJ0bi5hdHRyKCdocmVmJyk7XHJcbiAgICAgICAgICAgICAgICAvL3ZhciBjYWxsYmFjayA9IGJ0bi5hdHRyKCdkYXRhLWNhbGxiYWNrJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICAgICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ0RFTEVURScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBDb21tb24uc2hvd0FsZXJ0RGlhbG9nKCdkYW5nZXInLCAnQWN0aW9uIGNhbm5vdCBiZSBwZXJmb3JtZWQnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzczogKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soYnRuLCByZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb21wbGV0ZTogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgQ29tbW9uLmhpZGVDb25maXJtYXRpb24oKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IHVybDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIENvbW1vbi5kZWxldGVEaXYoKS5hcHBlbmRUbyhcImJvZHlcIikubW9kYWwoJ3Nob3cnKTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgLy8ganF1ZXJ5IHVpXHJcbiAgICBzdGF0aWMgaW5pdERhdGVwaWNrZXIoKSB7XHJcbiAgICAgICAgbGV0IGVsZW1lbnQgPSAkKFwiLmRhdGVwaWNrZXJcIik7XHJcblxyXG4gICAgICAgIGlmIChlbGVtZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgZWxlbWVudC5kYXRlcGlja2VyKHtcclxuICAgICAgICAgICAgICAgIGRhdGVGb3JtYXQ6ICdkZC5tbS55eSdcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgaW5pdFNlbGVjdGl6ZVRhZygpIHtcclxuICAgICAgICBsZXQgZWxlbWVudCA9ICQoIFwiLnNlbGVjdGl6ZS10YWdcIiApO1xyXG5cclxuICAgICAgICBpZiAoZWxlbWVudC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIC8vIHJlbW92ZSBib290c3RyYXAzIGZvcm0gY2xhc3NcclxuICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVDbGFzcygnZm9ybS1jb250cm9sJyk7XHJcblxyXG4gICAgICAgICAgICBsZXQgb3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgIHBsdWdpbnM6IFsncmVtb3ZlX2J1dHRvbiddLFxyXG4gICAgICAgICAgICAgICAgZGVsaW1pdGVyOiAnfCcsXHJcbiAgICAgICAgICAgICAgICBwZXJzaXN0OiBmYWxzZSxcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGlmIChlbGVtZW50LmF0dHIoJ2RhdGEtbmV3JykgIT0gMCkge1xyXG4gICAgICAgICAgICAgICAgb3B0aW9ucy5jcmVhdGUgPSAoaW5wdXQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogaW5wdXQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6IGlucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZWxlbWVudC5zZWxlY3RpemUob3B0aW9ucyk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgaW5pdFRhYmxlKCkge1xyXG4gICAgICAgIGxldCBlbGVtZW50ID0gJCgnLmRhdGFUYWJsZScpO1xyXG5cclxuICAgICAgICBpZiAoZWxlbWVudC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGVsZW1lbnQuRGF0YVRhYmxlKHtcclxuICAgICAgICAgICAgICAgIFwib3JkZXJcIjogW1sgMCwgXCJkZXNjXCIgXV0sXHJcbiAgICAgICAgICAgICAgICBcImNvbHVtbkRlZnNcIjogW1xyXG4gICAgICAgICAgICAgICAgICAgIHsgXCJvcmRlcmFibGVcIjogZmFsc2UsIFwidGFyZ2V0c1wiOiAtMSB9LFxyXG4gICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgIFwibGFuZ3VhZ2VcIjoge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaDogXCI8c3BhbiBjbGFzcz0nZmFzIGZhLXNlYXJjaCc+PC9zcGFuPiBfSU5QVVRfXCJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBzdGF0aWMgaW5pdFRvb2x0aXAoKSB7XHJcbiAgICAgICAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoKTtcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGluaXRQb3BvdmVyKCkge1xyXG4gICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIl0nKS5wb3BvdmVyKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIHN0YXRpYyBpbml0RHJvcERvd24oKSB7XHJcbiAgICAgICAgJCgnLm5hdmJhci1uYXYnKS5zbWFydG1lbnVzKHtcclxuICAgICAgICAgICAgJ3Nob3dUaW1lb3V0JzogMCxcclxuICAgICAgICAgICAgJ3N1YkluZGljYXRvcnMnOiBmYWxzZSxcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgX2lzRHJvcERvd24obWVudSkge1xyXG4gICAgICAgIGxldCBwYXJlbnQgPSBtZW51LnBhcmVudCgpO1xyXG4gICAgICAgIHJldHVybiAocGFyZW50Lmhhc0NsYXNzKCdkcm9wZG93bicpXHJcbiAgICAgICAgICAgIHx8IHBhcmVudC5oYXNDbGFzcygnZHJvcGRvd24tc3VibWVudScpKTtcclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGNvb2tpZUNvbnNlbnQoKSB7XHJcbiAgICAgICAgY29uc3QgY29uc2VudCA9IENvbW1vbi5nZXRDb29raWUoJ2NvbnNlbnQnKTtcclxuICAgICAgICBpZiAoY29uc2VudCAhPSAxICYmICQoJyNwYWdlJykubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBiYW5uZXIgPSAkKFxyXG4gICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJhbGVydCBhbGVydC13YXJuaW5nIGFsZXJ0LWRpc21pc3NpYmxlIGZhZGUgc2hvd1wiICcgK1xyXG4gICAgICAgICAgICAgICAgJ3JvbGU9XCJhbGVydFwiIHN0eWxlPVwicG9zaXRpb246IGZpeGVkOyBib3R0b206IDA7IHJpZ2h0OiAwOycgK1xyXG4gICAgICAgICAgICAgICAgJ21hcmdpbi1ib3R0b206IDA7XCI+VGhpcyB3ZWJzaXRlIHVzZXMgY29va2llcyB0byBlbnN1cmUgeW91ICcgK1xyXG4gICAgICAgICAgICAgICAgJ2dldCB0aGUgYmVzdCBleHBlcmllbmNlIG9uIG91ciB3ZWJzaXRlLiA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiAnICtcclxuICAgICAgICAgICAgICAgICdpZD1cInJhZ2UtY29va2llXCIgY2xhc3M9XCJidG4gYnRuLXN1Y2Nlc3NcIiBkYXRhLWRpc21pc3M9XCJhbGVydFwiICcgK1xyXG4gICAgICAgICAgICAgICAgJ2FyaWEtbGFiZWw9XCJDbG9zZVwiPkkgdW5kZXJzdGFuZDwvYnV0dG9uPjwvZGl2PidcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgJChcImJvZHlcIikuYXBwZW5kKGJhbm5lcik7XHJcblxyXG4gICAgICAgICAgICAkKFwiI3JhZ2UtY29va2llXCIpLmNsaWNrKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIENvbW1vbi5zZXRDb29raWUoJ2NvbnNlbnQnLCAxLCAzNjUpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLypcclxuICAgICAqIHNldHRpbmcgZm9yIGFjdGl2ZSBjb2xsYXBzZSBhbmQgdGFiXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBvcGVuQ29sbGFwc2VBbmRUYWIoKSB7XHJcbiAgICAgICAgbGV0IGhhc2ggPSB3aW5kb3cubG9jYXRpb24uaGFzaDtcclxuICAgICAgICBjb25zdCBzdGF0ZSA9ICQoJyNwYWdlLWFjdGl2ZS10YWInKTtcclxuXHJcbiAgICAgICAgaWYgKGhhc2gubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBoYXNoID0gaGFzaC5zdWJzdHIoMSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhbmNob3IgPSAkKCcjJyArIGhhc2gpO1xyXG4gICAgICAgICAgICBpZiAoYW5jaG9yLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIGlmIChhbmNob3IuaGFzQ2xhc3MoJ3RhYi1wYW5lJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAkKCdhW2hyZWY9XCIjJysgaGFzaCArICdcIl0nKS50YWIoJ3Nob3cnKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoYW5jaG9yLmhhc0NsYXNzKCdjb2xsYXBzZScpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYW5jaG9yLmNvbGxhcHNlKCdzaG93Jyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHN0YXRlLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBzdGF0ZS52YWwoaGFzaCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQoJy5uYXYtdGFicyBhJykuY2xpY2soKGUpID0+IHtcclxuICAgICAgICAgICAgaGFzaCA9IGUuY3VycmVudFRhcmdldC5oYXNoO1xyXG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaGFzaCA9IGhhc2g7XHJcbiAgICAgICAgICAgIGlmIChzdGF0ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBzdGF0ZS52YWwoaGFzaC5zdWJzdHIoMSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG5cclxuICAgIH07XHJcblxyXG4gICAgc3RhdGljIGluaXRMb2FkaW5nRGlhbG9nKCkge1xyXG4gICAgICAgIGxldCBlbGVtZW50ID0gJCgnI2xvYWRpbmdEaWFsb2ctZGl2Jyk7XHJcblxyXG4gICAgICAgIGlmIChlbGVtZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgJChkb2N1bWVudCkub24oJ2xvYWRpbmcuc3RhcnQnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50LmFwcGVuZFRvKFwiYm9keVwiKS5tb2RhbCgnc2hvdycpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICQoZG9jdW1lbnQpLm9uKCdsb2FkaW5nLnN0b3AnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50Lm1vZGFsKCdoaWRlJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaW5pdFNlbGVjdEFsbCgpIHtcclxuICAgICAgICBsZXQgc2VsZWN0QWxsID0gJCgnLmNoZWNrYm94LXNlbGVjdC1hbGwnKTtcclxuXHJcbiAgICAgICAgaWYgKHNlbGVjdEFsbC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHNlbGVjdEFsbC5jaGFuZ2UoKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZWxlbWVudCA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50LnBhcmVudCgpLnBhcmVudCgpLnBhcmVudCgpLnBhcmVudCgpLmZpbmQoJy5jaGVja2JveC1zZWxlY3QnKS5wcm9wKCdjaGVja2VkJywgZWxlbWVudC5wcm9wKCdjaGVja2VkJykpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGluaXRJbXBvcnRUYWJsZSgpIHtcclxuICAgICAgICBsZXQgaW1wb3J0VGFibGUgPSAkKCcuaW1wb3J0LXRhYmxlJyk7XHJcblxyXG4gICAgICAgIGlmIChpbXBvcnRUYWJsZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGltcG9ydFRhYmxlLmNzcygnd2lkdGgnLCAnMTAwJScpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgY29sdW1uRGVmcyA9IFsge1xyXG4gICAgICAgICAgICAgICAgXCJvcmRlcmFibGVcIjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBcInRhcmdldHNcIjogMFxyXG4gICAgICAgICAgICB9IF07XHJcblxyXG4gICAgICAgICAgICBpZiAoaW1wb3J0VGFibGUuYXR0cignZGF0YS1idXR0b24nKSkge1xyXG4gICAgICAgICAgICAgICAgY29sdW1uRGVmcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBcIm9yZGVyYWJsZVwiOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBcInRhcmdldHNcIjogLTFcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpbXBvcnRUYWJsZS5EYXRhVGFibGUoe1xyXG4gICAgICAgICAgICAgICAgXCJvcmRlclwiOiBbWyAxLCBcImRlc2NcIiBdXSxcclxuICAgICAgICAgICAgICAgIFwiY29sdW1uRGVmc1wiOiBjb2x1bW5EZWZzLFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGltcG9ydFRhYmxlLm9uKCAncGFnZS5kdCcsIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KS5maW5kKCcuY2hlY2tib3gtc2VsZWN0LWFsbCcpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIH0gKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGluaXRMb2coKSB7XHJcbiAgICAgICAgJCgnLmxvZy1jbGljaycpLm1vdXNldXAoKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgIENvbW1vbi51cGRhdGVMb2coQ29tbW9uLnV1aWR2NCgpLCAnY2xpY2snLCAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLnRleHQoKSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICQoJy5sb2ctZG93bmxvYWQnKS5tb3VzZXVwKChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICBDb21tb24udXBkYXRlTG9nKENvbW1vbi51dWlkdjQoKSwgJ2Rvd25sb2FkJywgJChldmVudC5jdXJyZW50VGFyZ2V0KS50ZXh0KCkpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBzdG9wTG9hZGluZygpIHtcclxuICAgICAgICBsZXQgZWxlbWVudCA9ICQoJy5zZS1wcmUtY29uJyk7XHJcblxyXG4gICAgICAgIGlmIChlbGVtZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgZWxlbWVudC5mYWRlT3V0KFwic2xvd1wiKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHN0YXRpYyBpbml0QWxlcnREaWFsb2coKSB7XHJcbiAgICAgICAgbGV0IGFsZXJ0RGl2ID0gQ29tbW9uLmFsZXJ0RGl2KCk7XHJcblxyXG4gICAgICAgIGFsZXJ0RGl2LmZpbmQoJy5jbG9zZScpLmNsaWNrKCgpID0+IHtcclxuICAgICAgICAgICAgYWxlcnREaXYuYW5pbWF0ZSh7Ym90dG9tOictNTBweCd9LCA1MDAsICdzd2luZycsICgpID0+IHtcclxuICAgICAgICAgICAgICAgIGFsZXJ0RGl2LmhpZGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnN0IGZsYXNoID0gQ29tbW9uLmdldENvb2tpZSgnZmxhc2gnKTtcclxuICAgICAgICBpZiAoZmxhc2gpIHtcclxuICAgICAgICAgICAgY29uc3QganNvbiA9IEpTT04ucGFyc2UoZmxhc2gpO1xyXG4gICAgICAgICAgICBpZiAoanNvbikge1xyXG4gICAgICAgICAgICAgICAgQ29tbW9uLnNob3dBbGVydERpYWxvZyhqc29uLnR5cGUsIGpzb24udGV4dCk7XHJcbiAgICAgICAgICAgICAgICBDb21tb24uc2V0Q29va2llKCdmbGFzaCcsICcnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpeCB0aGUgJ3VuZm9jdXNhYmxlJyBwcm9ibGVtIG9mIGNvbGxhcHNlZC1yZXF1aXJlZCBpbnB1dFxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgaW5pdFJlcXVpcmVkSW5wdXQoKSB7XHJcbiAgICAgICAgJCgnLnBhbmVsLWNvbGxhcHNlIGlucHV0JykuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcclxuICAgICAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdpbnZhbGlkJywgKGUpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGlucHV0ID0gJChlLmN1cnJlbnRUYXJnZXQpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGFuZWwgPSBpbnB1dC5jbG9zZXN0KFwiLnBhbmVsLWNvbGxhcHNlXCIpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFwYW5lbC5oYXNDbGFzcygnaW4nKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHBhbmVsLmNvbGxhcHNlKCdzaG93Jyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBMYXlvdXQ7XHJcblxyXG4kKCBkb2N1bWVudCApLnJlYWR5KCgpID0+IHtcclxuICAgIExheW91dC5pbml0RmVlZGJhY2soKTtcclxuICAgIExheW91dC5pbml0Q2hhdGJvdCgpO1xyXG4gICAgTGF5b3V0LmluaXREZWxldGVDb25maXJtYXRpb24oKTtcclxuICAgIExheW91dC5pbml0RGF0ZXBpY2tlcigpO1xyXG4gICAgTGF5b3V0LmluaXRTZWxlY3RpemVUYWcoKTtcclxuICAgIExheW91dC5pbml0VGFibGUoKTtcclxuICAgIExheW91dC5pbml0VG9vbHRpcCgpO1xyXG4gICAgTGF5b3V0LmluaXRQb3BvdmVyKCk7XHJcbiAgICAvL0xheW91dC5pbml0RHJvcERvd24oKTtcclxuICAgIExheW91dC5jb29raWVDb25zZW50KCk7XHJcbiAgICBMYXlvdXQub3BlbkNvbGxhcHNlQW5kVGFiKCk7XHJcbiAgICBMYXlvdXQuaW5pdExvYWRpbmdEaWFsb2coKTtcclxuICAgIExheW91dC5pbml0U2VsZWN0QWxsKCk7XHJcbiAgICBMYXlvdXQuaW5pdEltcG9ydFRhYmxlKCk7XHJcbiAgICBMYXlvdXQuaW5pdExvZygpO1xyXG4gICAgTGF5b3V0LmluaXRSZXF1aXJlZElucHV0KCk7XHJcbiAgICBic0N1c3RvbUZpbGVJbnB1dC5pbml0KCk7XHJcbn0pO1xyXG5cclxuJCh3aW5kb3cpLm9uKFwibG9hZFwiLCAoKSA9PiB7XHJcbiAgICBMYXlvdXQuc3RvcExvYWRpbmcoKTtcclxuICAgIExheW91dC5pbml0QWxlcnREaWFsb2coKTtcclxufSk7Il0sInNvdXJjZVJvb3QiOiIifQ==