(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Archive/Record"],{

/***/ "./assets/js/Archive/Record.js":
/*!*************************************!*\
  !*** ./assets/js/Archive/Record.js ***!
  \*************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _Library_DataTables__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Library/DataTables */ "./assets/js/Library/DataTables.js");
/* harmony import */ var _Common_Common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Common/Common */ "./assets/js/Common/Common.js");
/* harmony import */ var _Common_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Common/Layout */ "./assets/js/Common/Layout.js");
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2021 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var dataTbl;

var Record = /*#__PURE__*/function () {
  function Record() {
    _classCallCheck(this, Record);
  }

  _createClass(Record, null, [{
    key: "showCheck",
    value: function showCheck() {
      return $('#content-showall');
    }
  }, {
    key: "initTable",
    value: function initTable(prefix) {
      dataTbl = _Common_Common__WEBPACK_IMPORTED_MODULE_1__["default"].createDataTable(prefix, ['id', 'title', 'author'], 'archive_record_table', null, null, function (d) {
        d.all = Record.showCheck().prop("checked") ? 1 : 0;
        d.repository = $('#' + prefix + '-tbl').data('repository');
      }, function () {
        _Common_Common__WEBPACK_IMPORTED_MODULE_1__["default"].reRenderTable(prefix, ['id'], function (id, row) {
          var title = row.find('.' + prefix + '-title');

          var _short = title.text();

          if (_short.length > 80) {
            _short = _short.substring(0, 80) + '...';
          } //render tittle


          title.html(function () {
            return "<a title='" + title.text() + "' href='" + Routing.generate('archive_record_show', {
              id: id
            }) + "' style='color: #000'>" + _short + "</a>";
          });
          var result = "<a href='" + Routing.generate('archive_record_edit', {
            id: id
          }) + "' class='btn btn-info'><span class='fas fa-pencil'></span>&nbsp;&nbsp;Edit </a>&nbsp;<a href='" + Routing.generate('archive_record_delete', {
            id: id
          }) + "'  class='btn btn-danger delete-btn' title='Do you really want to delete the Record with ID: " + id + " ?'> <span class='fas fa-trash'></span>&nbsp;&nbsp;Delete </a>"; //render id

          var colId = row.find('.' + prefix + '-id');
          colId.css('text-align', 'center');
          return result;
        });
        _Common_Layout__WEBPACK_IMPORTED_MODULE_2__["default"].initTooltip();
        _Common_Layout__WEBPACK_IMPORTED_MODULE_2__["default"].initDeleteConfirmation(Record.deleteCallBack);
      });
      Record.showCheck().change(function () {
        dataTbl.ajax.reload();
      });
    }
  }, {
    key: "deleteCallBack",
    value: function deleteCallBack(btn, result) {
      if (result.success) {
        dataTbl.row(btn.parents('tr')).remove().draw();
        _Common_Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('success', result.msg);
      } else {
        _Common_Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', result.msg);
      }
    }
  }]);

  return Record;
}();

$(document).ready(function () {
  Record.initTable('record-manager');
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/Library/DataTables.js":
/*!*****************************************!*\
  !*** ./assets/js/Library/DataTables.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var datatables_net__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! datatables.net */ "./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var datatables_net__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(datatables_net__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var datatables_net_bs4_css_dataTables_bootstrap4_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! datatables.net-bs4/css/dataTables.bootstrap4.min.css */ "./node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css");
/* harmony import */ var datatables_net_bs4_css_dataTables_bootstrap4_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(datatables_net_bs4_css_dataTables_bootstrap4_min_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var datatables_net_bs4_js_dataTables_bootstrap4_min_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! datatables.net-bs4/js/dataTables.bootstrap4.min.js */ "./node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js");
/* harmony import */ var datatables_net_bs4_js_dataTables_bootstrap4_min_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(datatables_net_bs4_js_dataTables_bootstrap4_min_js__WEBPACK_IMPORTED_MODULE_2__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/






/***/ })

},[["./assets/js/Archive/Record.js","runtime","vendors~Admin/EmailCtrl~Admin/FileAudit~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Recor~b3f38238","vendors~Admin/FileAuditCtrl~Archive/Record~Authentication/User~Authentication/UserGroup~Content/Cont~c2893b9d","vendors~Archive/Record~Authentication/User~Common/Layout~Content/ContentList~Content/Display~Core/Fe~2684777e","Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Record~Archive/SearchCtrl~Aut~d45f3fc6","Archive/Record~Authentication/User~Common/Layout~Content/ContentList~Content/Display~Core/Feedback~E~7511650d"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQXJjaGl2ZS9SZWNvcmQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL0xpYnJhcnkvRGF0YVRhYmxlcy5qcyJdLCJuYW1lcyI6WyJkYXRhVGJsIiwiUmVjb3JkIiwiJCIsInByZWZpeCIsIkNvbW1vbiIsImNyZWF0ZURhdGFUYWJsZSIsImQiLCJhbGwiLCJzaG93Q2hlY2siLCJwcm9wIiwicmVwb3NpdG9yeSIsImRhdGEiLCJyZVJlbmRlclRhYmxlIiwiaWQiLCJyb3ciLCJ0aXRsZSIsImZpbmQiLCJzaG9ydCIsInRleHQiLCJsZW5ndGgiLCJzdWJzdHJpbmciLCJodG1sIiwiUm91dGluZyIsImdlbmVyYXRlIiwicmVzdWx0IiwiY29sSWQiLCJjc3MiLCJMYXlvdXQiLCJpbml0VG9vbHRpcCIsImluaXREZWxldGVDb25maXJtYXRpb24iLCJkZWxldGVDYWxsQmFjayIsImNoYW5nZSIsImFqYXgiLCJyZWxvYWQiLCJidG4iLCJzdWNjZXNzIiwicGFyZW50cyIsInJlbW92ZSIsImRyYXciLCJzaG93QWxlcnREaWFsb2ciLCJtc2ciLCJkb2N1bWVudCIsInJlYWR5IiwiaW5pdFRhYmxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQWNhOzs7Ozs7OztBQUViO0FBQ0E7QUFDQTtBQUVBLElBQUlBLE9BQUo7O0lBRU1DLE07Ozs7Ozs7Z0NBRWlCO0FBQ2YsYUFBT0MsQ0FBQyxDQUFDLGtCQUFELENBQVI7QUFDSDs7OzhCQUVnQkMsTSxFQUFRO0FBQ3JCSCxhQUFPLEdBQUdJLHNEQUFNLENBQUNDLGVBQVAsQ0FDTkYsTUFETSxFQUVOLENBQUMsSUFBRCxFQUFPLE9BQVAsRUFBZ0IsUUFBaEIsQ0FGTSxFQUdOLHNCQUhNLEVBSU4sSUFKTSxFQUtOLElBTE0sRUFNTixVQUFDRyxDQUFELEVBQU87QUFDSEEsU0FBQyxDQUFDQyxHQUFGLEdBQVFOLE1BQU0sQ0FBQ08sU0FBUCxHQUFtQkMsSUFBbkIsQ0FBd0IsU0FBeEIsSUFBcUMsQ0FBckMsR0FBeUMsQ0FBakQ7QUFDQUgsU0FBQyxDQUFDSSxVQUFGLEdBQWVSLENBQUMsQ0FBQyxNQUFNQyxNQUFOLEdBQWUsTUFBaEIsQ0FBRCxDQUF5QlEsSUFBekIsQ0FBOEIsWUFBOUIsQ0FBZjtBQUNILE9BVEssRUFVTixZQUFNO0FBQ0ZQLDhEQUFNLENBQUNRLGFBQVAsQ0FDSVQsTUFESixFQUVJLENBQUMsSUFBRCxDQUZKLEVBR0ksVUFBQ1UsRUFBRCxFQUFLQyxHQUFMLEVBQWE7QUFDVCxjQUFJQyxLQUFLLEdBQUdELEdBQUcsQ0FBQ0UsSUFBSixDQUFTLE1BQU1iLE1BQU4sR0FBZSxRQUF4QixDQUFaOztBQUNBLGNBQUljLE1BQUssR0FBR0YsS0FBSyxDQUFDRyxJQUFOLEVBQVo7O0FBQ0EsY0FBSUQsTUFBSyxDQUFDRSxNQUFOLEdBQWUsRUFBbkIsRUFBdUI7QUFDbkJGLGtCQUFLLEdBQUdBLE1BQUssQ0FBQ0csU0FBTixDQUFnQixDQUFoQixFQUFtQixFQUFuQixJQUF5QixLQUFqQztBQUNILFdBTFEsQ0FPVDs7O0FBQ0FMLGVBQUssQ0FBQ00sSUFBTixDQUFXLFlBQVU7QUFDakIsbUJBQU8sZUFBZU4sS0FBSyxDQUFDRyxJQUFOLEVBQWYsR0FBOEIsVUFBOUIsR0FBMkNJLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixxQkFBakIsRUFBd0M7QUFBQ1YsZ0JBQUUsRUFBRUE7QUFBTCxhQUF4QyxDQUEzQyxHQUErRix3QkFBL0YsR0FBMEhJLE1BQTFILEdBQWtJLE1BQXpJO0FBQ0gsV0FGRDtBQUlBLGNBQU1PLE1BQU0sR0FBRyxjQUFjRixPQUFPLENBQUNDLFFBQVIsQ0FBaUIscUJBQWpCLEVBQXdDO0FBQUNWLGNBQUUsRUFBRUE7QUFBTCxXQUF4QyxDQUFkLEdBQW1FLGdHQUFuRSxHQUFzS1MsT0FBTyxDQUFDQyxRQUFSLENBQWlCLHVCQUFqQixFQUEwQztBQUFDVixjQUFFLEVBQUVBO0FBQUwsV0FBMUMsQ0FBdEssR0FBNE4sK0ZBQTVOLEdBQThUQSxFQUE5VCxHQUFtVSxnRUFBbFYsQ0FaUyxDQWNUOztBQUNBLGNBQUlZLEtBQUssR0FBR1gsR0FBRyxDQUFDRSxJQUFKLENBQVMsTUFBTWIsTUFBTixHQUFlLEtBQXhCLENBQVo7QUFDQXNCLGVBQUssQ0FBQ0MsR0FBTixDQUFVLFlBQVYsRUFBdUIsUUFBdkI7QUFFQSxpQkFBT0YsTUFBUDtBQUNILFNBdEJMO0FBeUJBRyw4REFBTSxDQUFDQyxXQUFQO0FBQ0FELDhEQUFNLENBQUNFLHNCQUFQLENBQThCNUIsTUFBTSxDQUFDNkIsY0FBckM7QUFDSCxPQXRDSyxDQUFWO0FBeUNBN0IsWUFBTSxDQUFDTyxTQUFQLEdBQW1CdUIsTUFBbkIsQ0FBMEIsWUFBVztBQUNqQy9CLGVBQU8sQ0FBQ2dDLElBQVIsQ0FBYUMsTUFBYjtBQUNILE9BRkQ7QUFHSDs7O21DQUdxQkMsRyxFQUFLVixNLEVBQVE7QUFDL0IsVUFBSUEsTUFBTSxDQUFDVyxPQUFYLEVBQW9CO0FBQ2hCbkMsZUFBTyxDQUFDYyxHQUFSLENBQVlvQixHQUFHLENBQUNFLE9BQUosQ0FBWSxJQUFaLENBQVosRUFBK0JDLE1BQS9CLEdBQXdDQyxJQUF4QztBQUNBbEMsOERBQU0sQ0FBQ21DLGVBQVAsQ0FBdUIsU0FBdkIsRUFBa0NmLE1BQU0sQ0FBQ2dCLEdBQXpDO0FBQ0gsT0FIRCxNQUdPO0FBQ0hwQyw4REFBTSxDQUFDbUMsZUFBUCxDQUF1QixRQUF2QixFQUFpQ2YsTUFBTSxDQUFDZ0IsR0FBeEM7QUFDSDtBQUNKOzs7Ozs7QUFHTHRDLENBQUMsQ0FBQ3VDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQU07QUFDcEJ6QyxRQUFNLENBQUMwQyxTQUFQLENBQWlCLGdCQUFqQjtBQUNILENBRkQsRTs7Ozs7Ozs7Ozs7OztBQ3RGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FBY2E7O0FBRWI7QUFDQSIsImZpbGUiOiJBcmNoaXZlL1JlY29yZC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuICogIENvcHlyaWdodCBub3RpY2VcclxuICpcclxuICogIChjKSAyMDIxIEZUSyBlLlYuIDxmdGsuZGU+XHJcbiAqICBBbGwgcmlnaHRzIHJlc2VydmVkXHJcbiAqXHJcbiAqICBUaGlzIHNjcmlwdCBpcyBkaXN0cmlidXRlZCBpbiB0aGUgaG9wZSB0aGF0IGl0IHdpbGwgYmUgdXNlZnVsLFxyXG4gKiAgYnV0IFdJVEhPVVQgQU5ZIFdBUlJBTlRZOyB3aXRob3V0IGV2ZW4gdGhlIGltcGxpZWQgd2FycmFudHkgb2ZcclxuICogIE1FUkNIQU5UQUJJTElUWSBvciBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRS4gIFNlZSB0aGVcclxuICogIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGZvciBtb3JlIGRldGFpbHMuXHJcbiAqXHJcbiAqICBUaGlzIGNvcHlyaWdodCBub3RpY2UgTVVTVCBBUFBFQVIgaW4gYWxsIGNvcGllcyBvZiB0aGUgc2NyaXB0IVxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxuaW1wb3J0ICcuLi9MaWJyYXJ5L0RhdGFUYWJsZXMnO1xyXG5pbXBvcnQgQ29tbW9uIGZyb20gJy4uL0NvbW1vbi9Db21tb24nO1xyXG5pbXBvcnQgTGF5b3V0IGZyb20gJy4uL0NvbW1vbi9MYXlvdXQnO1xyXG5cclxubGV0IGRhdGFUYmw7XHJcblxyXG5jbGFzcyBSZWNvcmQge1xyXG5cclxuICAgIHN0YXRpYyBzaG93Q2hlY2soKSB7XHJcbiAgICAgICAgcmV0dXJuICQoJyNjb250ZW50LXNob3dhbGwnKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaW5pdFRhYmxlKHByZWZpeCkge1xyXG4gICAgICAgIGRhdGFUYmwgPSBDb21tb24uY3JlYXRlRGF0YVRhYmxlKFxyXG4gICAgICAgICAgICBwcmVmaXgsXHJcbiAgICAgICAgICAgIFsnaWQnLCAndGl0bGUnLCAnYXV0aG9yJ10sXHJcbiAgICAgICAgICAgICdhcmNoaXZlX3JlY29yZF90YWJsZScsXHJcbiAgICAgICAgICAgIG51bGwsXHJcbiAgICAgICAgICAgIG51bGwsXHJcbiAgICAgICAgICAgIChkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBkLmFsbCA9IFJlY29yZC5zaG93Q2hlY2soKS5wcm9wKFwiY2hlY2tlZFwiKSA/IDEgOiAwO1xyXG4gICAgICAgICAgICAgICAgZC5yZXBvc2l0b3J5ID0gJCgnIycgKyBwcmVmaXggKyAnLXRibCcpLmRhdGEoJ3JlcG9zaXRvcnknKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgQ29tbW9uLnJlUmVuZGVyVGFibGUoXHJcbiAgICAgICAgICAgICAgICAgICAgcHJlZml4LFxyXG4gICAgICAgICAgICAgICAgICAgIFsnaWQnXSxcclxuICAgICAgICAgICAgICAgICAgICAoaWQsIHJvdykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgdGl0bGUgPSByb3cuZmluZCgnLicgKyBwcmVmaXggKyAnLXRpdGxlJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBzaG9ydCA9IHRpdGxlLnRleHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNob3J0Lmxlbmd0aCA+IDgwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG9ydCA9IHNob3J0LnN1YnN0cmluZygwLCA4MCkgKyAnLi4uJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy9yZW5kZXIgdGl0dGxlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlLmh0bWwoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBcIjxhIHRpdGxlPSdcIiArIHRpdGxlLnRleHQoKSArIFwiJyBocmVmPSdcIiArIFJvdXRpbmcuZ2VuZXJhdGUoJ2FyY2hpdmVfcmVjb3JkX3Nob3cnLCB7aWQ6IGlkfSkgKyBcIicgc3R5bGU9J2NvbG9yOiAjMDAwJz5cIiArIHNob3J0ICsgXCI8L2E+XCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcmVzdWx0ID0gXCI8YSBocmVmPSdcIiArIFJvdXRpbmcuZ2VuZXJhdGUoJ2FyY2hpdmVfcmVjb3JkX2VkaXQnLCB7aWQ6IGlkfSkgKyAgXCInIGNsYXNzPSdidG4gYnRuLWluZm8nPjxzcGFuIGNsYXNzPSdmYXMgZmEtcGVuY2lsJz48L3NwYW4+Jm5ic3A7Jm5ic3A7RWRpdCA8L2E+Jm5ic3A7PGEgaHJlZj0nXCIgKyBSb3V0aW5nLmdlbmVyYXRlKCdhcmNoaXZlX3JlY29yZF9kZWxldGUnLCB7aWQ6IGlkfSkgKyBcIicgIGNsYXNzPSdidG4gYnRuLWRhbmdlciBkZWxldGUtYnRuJyB0aXRsZT0nRG8geW91IHJlYWxseSB3YW50IHRvIGRlbGV0ZSB0aGUgUmVjb3JkIHdpdGggSUQ6IFwiICsgaWQgKyBcIiA/Jz4gPHNwYW4gY2xhc3M9J2ZhcyBmYS10cmFzaCc+PC9zcGFuPiZuYnNwOyZuYnNwO0RlbGV0ZSA8L2E+XCI7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL3JlbmRlciBpZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgY29sSWQgPSByb3cuZmluZCgnLicgKyBwcmVmaXggKyAnLWlkJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbElkLmNzcygndGV4dC1hbGlnbicsJ2NlbnRlcicpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgICAgICAgIExheW91dC5pbml0VG9vbHRpcCgpO1xyXG4gICAgICAgICAgICAgICAgTGF5b3V0LmluaXREZWxldGVDb25maXJtYXRpb24oUmVjb3JkLmRlbGV0ZUNhbGxCYWNrKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIFJlY29yZC5zaG93Q2hlY2soKS5jaGFuZ2UoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGRhdGFUYmwuYWpheC5yZWxvYWQoKTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG5cclxuICAgIHN0YXRpYyBkZWxldGVDYWxsQmFjayhidG4sIHJlc3VsdCkge1xyXG4gICAgICAgIGlmIChyZXN1bHQuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICBkYXRhVGJsLnJvdyhidG4ucGFyZW50cygndHInKSkucmVtb3ZlKCkuZHJhdygpO1xyXG4gICAgICAgICAgICBDb21tb24uc2hvd0FsZXJ0RGlhbG9nKCdzdWNjZXNzJywgcmVzdWx0Lm1zZyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgQ29tbW9uLnNob3dBbGVydERpYWxvZygnZGFuZ2VyJywgcmVzdWx0Lm1zZyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4kKGRvY3VtZW50KS5yZWFkeSgoKSA9PiB7XHJcbiAgICBSZWNvcmQuaW5pdFRhYmxlKCdyZWNvcmQtbWFuYWdlcicpO1xyXG59KTsiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnZGF0YXRhYmxlcy5uZXQnO1xyXG5pbXBvcnQgJ2RhdGF0YWJsZXMubmV0LWJzNC9jc3MvZGF0YVRhYmxlcy5ib290c3RyYXA0Lm1pbi5jc3MnO1xyXG5pbXBvcnQgJ2RhdGF0YWJsZXMubmV0LWJzNC9qcy9kYXRhVGFibGVzLmJvb3RzdHJhcDQubWluLmpzJzsiXSwic291cmNlUm9vdCI6IiJ9