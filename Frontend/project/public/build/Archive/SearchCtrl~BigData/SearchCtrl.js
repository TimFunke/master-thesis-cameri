(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Archive/SearchCtrl~BigData/SearchCtrl"],{

/***/ "./assets/css/Element/Star.css":
/*!*************************************!*\
  !*** ./assets/css/Element/Star.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/css/Library/IonRangeSlider.css":
/*!***********************************************!*\
  !*** ./assets/css/Library/IonRangeSlider.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/Common/Angular.js":
/*!*************************************!*\
  !*** ./assets/js/Common/Angular.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! angular */ "./node_modules/angular/index.js");
/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(angular__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Common */ "./assets/js/Common/Common.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/



var Eid = /*#__PURE__*/function () {
  /**
   * Initiate
   *
   * @param $http
   */
  function Eid($http) {
    _classCallCheck(this, Eid);

    this.$http = $http;
    this.loading = false;
  }
  /**
   * send normal POST
   *
   * @param url
   * @param input
   * @param event
   * @returns {Promise<any> | Promise<T> | *}
   */


  _createClass(Eid, [{
    key: "change",
    value: function change(url, input, event) {
      this.loading = true;

      if (!input) {
        input = {};
      }

      var headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      return this.send('POST', url, $.param(input), headers, event);
    }
    /**
     * Send a GET request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "get",
    value: function get(url, event) {
      return this.send('GET', url, null, null, event);
    }
    /**
     * Send a POST request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "post",
    value: function post(url, input, event) {
      return this.send('POST', url, input, null, event);
    }
    /**
     * Send a DELETE request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "delete",
    value: function _delete(url, event) {
      return this.send('DELETE', url, null, null, event);
    }
    /**
     * Send a PATCH request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "patch",
    value: function patch(url, input, event) {
      var headers = {
        'Content-Type': 'application/merge-patch+json',
        'accept': 'application/json'
      };
      return this.send('PATCH', url, input, headers, event);
    }
    /**
     * Send a request
     *
     * @param method
     * @param url
     * @param input
     * @param headers
     * @param event
     * @returns {Promise<any> | Promise<T> | *}
     */

  }, {
    key: "send",
    value: function send(method, url, input, headers, event) {
      var btn = null;

      if (!input) {
        input = {};
      }

      if (!headers) {
        headers = {
          'Content-Type': 'application/json',
          'accept': 'application/ld+json'
        };
      }

      if (event) {
        btn = $(event.currentTarget);
        btn.addClass('running');
        btn.prop("disabled", true);
      }

      return this.$http({
        method: method,
        url: url,
        data: input,
        headers: headers
      })["catch"](function (error) {
        if (error.data.detail) {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', error.data.detail);
        } else {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', 'Action cannot be performed');
        }
      })["finally"](function () {
        $(document).trigger('loading.stop');

        if (btn) {
          var modal = btn.data('close');

          if (modal) {
            $('#' + modal).modal('hide');
          }

          btn.removeClass('running');
          btn.prop("disabled", false);
        }
      });
    }
  }]);

  return Eid;
}();

var app = angular__WEBPACK_IMPORTED_MODULE_0___default.a.module('Ecosystem', []) // change Angular default {{ to {[{ to avoid conflict with Twig
.config(['$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false,
    rewriteLinks: false
  });
}]).filter('unsafe', ['$sce', function ($sce) {
  return function (val) {
    return $sce.trustAsHtml(val);
  };
}]).filter('range', function () {
  return function (input, total) {
    total = parseInt(total);

    for (var i = 0; i < total; i++) {
      input.push(i);
    }

    return input;
  };
}).service('eid', ['$http', Eid]).directive('form', ['$location', function ($location) {
  return {
    restrict: 'E',
    priority: 999,
    compile: function compile() {
      return {
        pre: function pre(scope, element, attrs) {
          if (attrs.noaction === '') return;

          if (attrs.action === undefined || attrs.action === '') {
            attrs.action = $location.absUrl();
          }
        }
      };
    }
  };
}]).directive('initTooltip', function () {
  return {
    restrict: 'A',
    link: function link(scope, element, attrs) {
      $(element).tooltip({
        trigger: 'hover'
      });
    }
  };
});
/* harmony default export */ __webpack_exports__["default"] = (app);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/Library/IonRangeSlider.js":
/*!*********************************************!*\
  !*** ./assets/js/Library/IonRangeSlider.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ion-rangeslider */ "./node_modules/ion-rangeslider/js/ion.rangeSlider.js");
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ion-rangeslider/css/ion.rangeSlider.css */ "./node_modules/ion-rangeslider/css/ion.rangeSlider.css");
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _css_Library_IonRangeSlider_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../css/Library/IonRangeSlider.css */ "./assets/css/Library/IonRangeSlider.css");
/* harmony import */ var _css_Library_IonRangeSlider_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_css_Library_IonRangeSlider_css__WEBPACK_IMPORTED_MODULE_2__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/



 // fix slider icon



/***/ }),

/***/ "./assets/js/Library/JsTree.js":
/*!*************************************!*\
  !*** ./assets/js/Library/JsTree.js ***!
  \*************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jstree__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jstree */ "./node_modules/jstree/dist/jstree.js");
/* harmony import */ var jstree__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jstree__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jstree_dist_themes_default_style_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jstree/dist/themes/default/style.min.css */ "./node_modules/jstree/dist/themes/default/style.min.css");
/* harmony import */ var jstree_dist_themes_default_style_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jstree_dist_themes_default_style_min_css__WEBPACK_IMPORTED_MODULE_1__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/





/***/ }),

/***/ "./assets/js/Library/Star.js":
/*!***********************************!*\
  !*** ./assets/js/Library/Star.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _css_Element_Star_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../css/Element/Star.css */ "./assets/css/Element/Star.css");
/* harmony import */ var _css_Element_Star_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_Element_Star_css__WEBPACK_IMPORTED_MODULE_0__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/




/***/ }),

/***/ "./assets/library/jstree/custom.css":
/*!******************************************!*\
  !*** ./assets/library/jstree/custom.css ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/library/jstree/themes/proton/style.min.css":
/*!***********************************************************!*\
  !*** ./assets/library/jstree/themes/proton/style.min.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL0VsZW1lbnQvU3Rhci5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2Nzcy9MaWJyYXJ5L0lvblJhbmdlU2xpZGVyLmNzcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQ29tbW9uL0FuZ3VsYXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL0xpYnJhcnkvSW9uUmFuZ2VTbGlkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL0xpYnJhcnkvSnNUcmVlLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9MaWJyYXJ5L1N0YXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2xpYnJhcnkvanN0cmVlL2N1c3RvbS5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2xpYnJhcnkvanN0cmVlL3RoZW1lcy9wcm90b24vc3R5bGUubWluLmNzcyJdLCJuYW1lcyI6WyJFaWQiLCIkaHR0cCIsImxvYWRpbmciLCJ1cmwiLCJpbnB1dCIsImV2ZW50IiwiaGVhZGVycyIsInNlbmQiLCIkIiwicGFyYW0iLCJtZXRob2QiLCJidG4iLCJjdXJyZW50VGFyZ2V0IiwiYWRkQ2xhc3MiLCJwcm9wIiwiZGF0YSIsImVycm9yIiwiZGV0YWlsIiwiQ29tbW9uIiwic2hvd0FsZXJ0RGlhbG9nIiwiZG9jdW1lbnQiLCJ0cmlnZ2VyIiwibW9kYWwiLCJyZW1vdmVDbGFzcyIsImFwcCIsImFuZ3VsYXIiLCJtb2R1bGUiLCJjb25maWciLCIkaW50ZXJwb2xhdGVQcm92aWRlciIsIiRsb2NhdGlvblByb3ZpZGVyIiwic3RhcnRTeW1ib2wiLCJlbmRTeW1ib2wiLCJodG1sNU1vZGUiLCJlbmFibGVkIiwicmVxdWlyZUJhc2UiLCJyZXdyaXRlTGlua3MiLCJmaWx0ZXIiLCIkc2NlIiwidmFsIiwidHJ1c3RBc0h0bWwiLCJ0b3RhbCIsInBhcnNlSW50IiwiaSIsInB1c2giLCJzZXJ2aWNlIiwiZGlyZWN0aXZlIiwiJGxvY2F0aW9uIiwicmVzdHJpY3QiLCJwcmlvcml0eSIsImNvbXBpbGUiLCJwcmUiLCJzY29wZSIsImVsZW1lbnQiLCJhdHRycyIsIm5vYWN0aW9uIiwiYWN0aW9uIiwidW5kZWZpbmVkIiwiYWJzVXJsIiwibGluayIsInRvb2x0aXAiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLHVDOzs7Ozs7Ozs7OztBQ0FBLHVDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBOztJQUVNQSxHO0FBQ0Y7Ozs7O0FBS0EsZUFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUNmLFNBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNBLFNBQUtDLE9BQUwsR0FBZSxLQUFmO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7OzJCQVFPQyxHLEVBQUtDLEssRUFBT0MsSyxFQUFPO0FBQ3RCLFdBQUtILE9BQUwsR0FBZSxJQUFmOztBQUVBLFVBQUksQ0FBQ0UsS0FBTCxFQUFZO0FBQ1JBLGFBQUssR0FBRyxFQUFSO0FBQ0g7O0FBRUQsVUFBSUUsT0FBTyxHQUFHO0FBQ1Ysd0JBQWdCO0FBRE4sT0FBZDtBQUlBLGFBQU8sS0FBS0MsSUFBTCxDQUFVLE1BQVYsRUFBa0JKLEdBQWxCLEVBQXVCSyxDQUFDLENBQUNDLEtBQUYsQ0FBUUwsS0FBUixDQUF2QixFQUF1Q0UsT0FBdkMsRUFBZ0RELEtBQWhELENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7O3dCQU9JRixHLEVBQUtFLEssRUFBTztBQUNaLGFBQU8sS0FBS0UsSUFBTCxDQUFVLEtBQVYsRUFBaUJKLEdBQWpCLEVBQXNCLElBQXRCLEVBQTRCLElBQTVCLEVBQWtDRSxLQUFsQyxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7eUJBUUtGLEcsRUFBS0MsSyxFQUFPQyxLLEVBQU87QUFDcEIsYUFBTyxLQUFLRSxJQUFMLENBQVUsTUFBVixFQUFrQkosR0FBbEIsRUFBdUJDLEtBQXZCLEVBQThCLElBQTlCLEVBQW9DQyxLQUFwQyxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs0QkFPT0YsRyxFQUFLRSxLLEVBQU87QUFDZixhQUFPLEtBQUtFLElBQUwsQ0FBVSxRQUFWLEVBQW9CSixHQUFwQixFQUF5QixJQUF6QixFQUErQixJQUEvQixFQUFxQ0UsS0FBckMsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7OzBCQVFNRixHLEVBQUtDLEssRUFBT0MsSyxFQUFPO0FBQ3JCLFVBQUlDLE9BQU8sR0FBRztBQUNWLHdCQUFnQiw4QkFETjtBQUVWLGtCQUFVO0FBRkEsT0FBZDtBQUtBLGFBQU8sS0FBS0MsSUFBTCxDQUFVLE9BQVYsRUFBbUJKLEdBQW5CLEVBQXdCQyxLQUF4QixFQUErQkUsT0FBL0IsRUFBd0NELEtBQXhDLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7O3lCQVVLSyxNLEVBQVFQLEcsRUFBS0MsSyxFQUFPRSxPLEVBQVNELEssRUFBTztBQUNyQyxVQUFJTSxHQUFHLEdBQUcsSUFBVjs7QUFFQSxVQUFJLENBQUNQLEtBQUwsRUFBWTtBQUNSQSxhQUFLLEdBQUcsRUFBUjtBQUNIOztBQUVELFVBQUksQ0FBQ0UsT0FBTCxFQUFjO0FBQ1ZBLGVBQU8sR0FBRztBQUNOLDBCQUFnQixrQkFEVjtBQUVOLG9CQUFVO0FBRkosU0FBVjtBQUlIOztBQUVELFVBQUlELEtBQUosRUFBVztBQUNQTSxXQUFHLEdBQUdILENBQUMsQ0FBQ0gsS0FBSyxDQUFDTyxhQUFQLENBQVA7QUFDQUQsV0FBRyxDQUFDRSxRQUFKLENBQWEsU0FBYjtBQUNBRixXQUFHLENBQUNHLElBQUosQ0FBUyxVQUFULEVBQXFCLElBQXJCO0FBQ0g7O0FBRUQsYUFBTyxLQUFLYixLQUFMLENBQVc7QUFDZFMsY0FBTSxFQUFFQSxNQURNO0FBRWRQLFdBQUcsRUFBRUEsR0FGUztBQUdkWSxZQUFJLEVBQUVYLEtBSFE7QUFJZEUsZUFBTyxFQUFFQTtBQUpLLE9BQVgsV0FLRSxVQUFDVSxLQUFELEVBQVc7QUFDaEIsWUFBSUEsS0FBSyxDQUFDRCxJQUFOLENBQVdFLE1BQWYsRUFBdUI7QUFDbkJDLHlEQUFNLENBQUNDLGVBQVAsQ0FBdUIsUUFBdkIsRUFBaUNILEtBQUssQ0FBQ0QsSUFBTixDQUFXRSxNQUE1QztBQUNILFNBRkQsTUFFTztBQUNIQyx5REFBTSxDQUFDQyxlQUFQLENBQXVCLFFBQXZCLEVBQWlDLDRCQUFqQztBQUNIO0FBQ0osT0FYTSxhQVdJLFlBQU07QUFDYlgsU0FBQyxDQUFDWSxRQUFELENBQUQsQ0FBWUMsT0FBWixDQUFvQixjQUFwQjs7QUFDQSxZQUFJVixHQUFKLEVBQVM7QUFDTCxjQUFNVyxLQUFLLEdBQUdYLEdBQUcsQ0FBQ0ksSUFBSixDQUFTLE9BQVQsQ0FBZDs7QUFDQSxjQUFJTyxLQUFKLEVBQVc7QUFDUGQsYUFBQyxDQUFDLE1BQU1jLEtBQVAsQ0FBRCxDQUFlQSxLQUFmLENBQXFCLE1BQXJCO0FBQ0g7O0FBRURYLGFBQUcsQ0FBQ1ksV0FBSixDQUFnQixTQUFoQjtBQUNBWixhQUFHLENBQUNHLElBQUosQ0FBUyxVQUFULEVBQXFCLEtBQXJCO0FBQ0g7QUFDSixPQXRCTSxDQUFQO0FBdUJIOzs7Ozs7QUFHTCxJQUFNVSxHQUFHLEdBQUdDLDhDQUFPLENBQUNDLE1BQVIsQ0FBZSxXQUFmLEVBQTRCLEVBQTVCLEVBQ1I7QUFEUSxDQUVQQyxNQUZPLENBRUEsQ0FBQyxzQkFBRCxFQUF5QixtQkFBekIsRUFBOEMsVUFBQ0Msb0JBQUQsRUFBdUJDLGlCQUF2QixFQUE2QztBQUMvRkQsc0JBQW9CLENBQUNFLFdBQXJCLENBQWlDLEtBQWpDLEVBQXdDQyxTQUF4QyxDQUFrRCxLQUFsRDtBQUNBRixtQkFBaUIsQ0FBQ0csU0FBbEIsQ0FBNEI7QUFDeEJDLFdBQU8sRUFBRSxJQURlO0FBRXhCQyxlQUFXLEVBQUUsS0FGVztBQUd4QkMsZ0JBQVksRUFBRTtBQUhVLEdBQTVCO0FBS0gsQ0FQTyxDQUZBLEVBVVBDLE1BVk8sQ0FVQSxRQVZBLEVBVVUsQ0FBQyxNQUFELEVBQVMsVUFBQ0MsSUFBRCxFQUFVO0FBQ2pDLFNBQU8sVUFBQ0MsR0FBRCxFQUFTO0FBQ1osV0FBT0QsSUFBSSxDQUFDRSxXQUFMLENBQWlCRCxHQUFqQixDQUFQO0FBQ0gsR0FGRDtBQUdILENBSmlCLENBVlYsRUFlUEYsTUFmTyxDQWVBLE9BZkEsRUFlUyxZQUFNO0FBQ25CLFNBQU8sVUFBQ2hDLEtBQUQsRUFBUW9DLEtBQVIsRUFBa0I7QUFDckJBLFNBQUssR0FBR0MsUUFBUSxDQUFDRCxLQUFELENBQWhCOztBQUVBLFNBQUssSUFBSUUsQ0FBQyxHQUFDLENBQVgsRUFBY0EsQ0FBQyxHQUFDRixLQUFoQixFQUF1QkUsQ0FBQyxFQUF4QixFQUE0QjtBQUN4QnRDLFdBQUssQ0FBQ3VDLElBQU4sQ0FBV0QsQ0FBWDtBQUNIOztBQUVELFdBQU90QyxLQUFQO0FBQ0gsR0FSRDtBQVNILENBekJPLEVBMEJQd0MsT0ExQk8sQ0EwQkMsS0ExQkQsRUEwQlEsQ0FBQyxPQUFELEVBQVU1QyxHQUFWLENBMUJSLEVBMkJQNkMsU0EzQk8sQ0EyQkcsTUEzQkgsRUEyQlcsQ0FBQyxXQUFELEVBQWMsVUFBQ0MsU0FBRCxFQUFlO0FBQzVDLFNBQU87QUFDSEMsWUFBUSxFQUFDLEdBRE47QUFFSEMsWUFBUSxFQUFFLEdBRlA7QUFHSEMsV0FBTyxFQUFFLG1CQUFNO0FBQ1gsYUFBTztBQUNIQyxXQUFHLEVBQUUsYUFBQ0MsS0FBRCxFQUFRQyxPQUFSLEVBQWlCQyxLQUFqQixFQUEyQjtBQUM1QixjQUFJQSxLQUFLLENBQUNDLFFBQU4sS0FBbUIsRUFBdkIsRUFBMkI7O0FBQzNCLGNBQUlELEtBQUssQ0FBQ0UsTUFBTixLQUFpQkMsU0FBakIsSUFBOEJILEtBQUssQ0FBQ0UsTUFBTixLQUFpQixFQUFuRCxFQUFzRDtBQUNsREYsaUJBQUssQ0FBQ0UsTUFBTixHQUFlVCxTQUFTLENBQUNXLE1BQVYsRUFBZjtBQUNIO0FBQ0o7QUFORSxPQUFQO0FBUUg7QUFaRSxHQUFQO0FBY0gsQ0Fma0IsQ0EzQlgsRUEyQ1BaLFNBM0NPLENBMkNHLGFBM0NILEVBMkNrQixZQUFNO0FBQzVCLFNBQU87QUFDSEUsWUFBUSxFQUFFLEdBRFA7QUFFSFcsUUFBSSxFQUFFLGNBQUNQLEtBQUQsRUFBUUMsT0FBUixFQUFpQkMsS0FBakIsRUFBMkI7QUFDN0I3QyxPQUFDLENBQUM0QyxPQUFELENBQUQsQ0FBV08sT0FBWCxDQUFtQjtBQUNmdEMsZUFBTyxFQUFHO0FBREssT0FBbkI7QUFHSDtBQU5FLEdBQVA7QUFRSCxDQXBETyxDQUFaO0FBdURlRyxrRUFBZixFOzs7Ozs7Ozs7Ozs7O0FDcE5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUFjYTs7QUFFYjtDQUVBOzs7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUFjYTs7QUFFYjs7Ozs7Ozs7Ozs7OztBQ2hCQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQWNhOzs7Ozs7Ozs7Ozs7O0FDZGIsdUM7Ozs7Ozs7Ozs7O0FDQUEsdUMiLCJmaWxlIjoiQXJjaGl2ZS9TZWFyY2hDdHJsfkJpZ0RhdGEvU2VhcmNoQ3RybC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuICogIENvcHlyaWdodCBub3RpY2VcclxuICpcclxuICogIChjKSAyMDE4IEZUSyBlLlYuIDxmdGsuZGU+XHJcbiAqICBBbGwgcmlnaHRzIHJlc2VydmVkXHJcbiAqXHJcbiAqICBUaGlzIHNjcmlwdCBpcyBkaXN0cmlidXRlZCBpbiB0aGUgaG9wZSB0aGF0IGl0IHdpbGwgYmUgdXNlZnVsLFxyXG4gKiAgYnV0IFdJVEhPVVQgQU5ZIFdBUlJBTlRZOyB3aXRob3V0IGV2ZW4gdGhlIGltcGxpZWQgd2FycmFudHkgb2ZcclxuICogIE1FUkNIQU5UQUJJTElUWSBvciBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRS4gIFNlZSB0aGVcclxuICogIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGZvciBtb3JlIGRldGFpbHMuXHJcbiAqXHJcbiAqICBUaGlzIGNvcHlyaWdodCBub3RpY2UgTVVTVCBBUFBFQVIgaW4gYWxsIGNvcGllcyBvZiB0aGUgc2NyaXB0IVxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuaW1wb3J0IGFuZ3VsYXIgZnJvbSAnYW5ndWxhcic7XHJcbmltcG9ydCBDb21tb24gZnJvbSAnLi9Db21tb24nO1xyXG5cclxuY2xhc3MgRWlkIHtcclxuICAgIC8qKlxyXG4gICAgICogSW5pdGlhdGVcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gJGh0dHBcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoJGh0dHApIHtcclxuICAgICAgICB0aGlzLiRodHRwID0gJGh0dHA7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBzZW5kIG5vcm1hbCBQT1NUXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGlucHV0XHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT4gfCBQcm9taXNlPFQ+IHwgKn1cclxuICAgICAqL1xyXG4gICAgY2hhbmdlKHVybCwgaW5wdXQsIGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgaWYgKCFpbnB1dCkge1xyXG4gICAgICAgICAgICBpbnB1dCA9IHt9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGhlYWRlcnMgPSB7XHJcbiAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJ1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbmQoJ1BPU1QnLCB1cmwsICQucGFyYW0oaW5wdXQpLCBoZWFkZXJzLCBldmVudClcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbmQgYSBHRVQgcmVxdWVzdFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB1cmxcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PnxQcm9taXNlPFQ+fCp9XHJcbiAgICAgKi9cclxuICAgIGdldCh1cmwsIGV2ZW50KSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VuZCgnR0VUJywgdXJsLCBudWxsLCBudWxsLCBldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIGEgUE9TVCByZXF1ZXN0IHdpdGggSlNPTiBkYXRhXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGlucHV0XHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT58UHJvbWlzZTxUPnwqfVxyXG4gICAgICovXHJcbiAgICBwb3N0KHVybCwgaW5wdXQsIGV2ZW50KSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VuZCgnUE9TVCcsIHVybCwgaW5wdXQsIG51bGwsIGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbmQgYSBERUxFVEUgcmVxdWVzdFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB1cmxcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PnxQcm9taXNlPFQ+fCp9XHJcbiAgICAgKi9cclxuICAgIGRlbGV0ZSh1cmwsIGV2ZW50KSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VuZCgnREVMRVRFJywgdXJsLCBudWxsLCBudWxsLCBldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIGEgUEFUQ0ggcmVxdWVzdCB3aXRoIEpTT04gZGF0YVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB1cmxcclxuICAgICAqIEBwYXJhbSBpbnB1dFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fFByb21pc2U8VD58Kn1cclxuICAgICAqL1xyXG4gICAgcGF0Y2godXJsLCBpbnB1dCwgZXZlbnQpIHtcclxuICAgICAgICBsZXQgaGVhZGVycyA9IHtcclxuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9tZXJnZS1wYXRjaCtqc29uJyxcclxuICAgICAgICAgICAgJ2FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5zZW5kKCdQQVRDSCcsIHVybCwgaW5wdXQsIGhlYWRlcnMsIGV2ZW50KVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBhIHJlcXVlc3RcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gbWV0aG9kXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gaW5wdXRcclxuICAgICAqIEBwYXJhbSBoZWFkZXJzXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT4gfCBQcm9taXNlPFQ+IHwgKn1cclxuICAgICAqL1xyXG4gICAgc2VuZChtZXRob2QsIHVybCwgaW5wdXQsIGhlYWRlcnMsIGV2ZW50KSB7XHJcbiAgICAgICAgbGV0IGJ0biA9IG51bGw7XHJcblxyXG4gICAgICAgIGlmICghaW5wdXQpIHtcclxuICAgICAgICAgICAgaW5wdXQgPSB7fTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghaGVhZGVycykge1xyXG4gICAgICAgICAgICBoZWFkZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcclxuICAgICAgICAgICAgICAgICdhY2NlcHQnOiAnYXBwbGljYXRpb24vbGQranNvbicsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgYnRuID0gJChldmVudC5jdXJyZW50VGFyZ2V0KTtcclxuICAgICAgICAgICAgYnRuLmFkZENsYXNzKCdydW5uaW5nJyk7XHJcbiAgICAgICAgICAgIGJ0bi5wcm9wKFwiZGlzYWJsZWRcIiwgdHJ1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy4kaHR0cCh7XHJcbiAgICAgICAgICAgIG1ldGhvZDogbWV0aG9kLFxyXG4gICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgZGF0YTogaW5wdXQsXHJcbiAgICAgICAgICAgIGhlYWRlcnM6IGhlYWRlcnNcclxuICAgICAgICB9KS5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgaWYgKGVycm9yLmRhdGEuZGV0YWlsKSB7XHJcbiAgICAgICAgICAgICAgICBDb21tb24uc2hvd0FsZXJ0RGlhbG9nKCdkYW5nZXInLCBlcnJvci5kYXRhLmRldGFpbCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBDb21tb24uc2hvd0FsZXJ0RGlhbG9nKCdkYW5nZXInLCAnQWN0aW9uIGNhbm5vdCBiZSBwZXJmb3JtZWQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAkKGRvY3VtZW50KS50cmlnZ2VyKCdsb2FkaW5nLnN0b3AnKTtcclxuICAgICAgICAgICAgaWYgKGJ0bikge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbW9kYWwgPSBidG4uZGF0YSgnY2xvc2UnKTtcclxuICAgICAgICAgICAgICAgIGlmIChtb2RhbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoJyMnICsgbW9kYWwpLm1vZGFsKCdoaWRlJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgYnRuLnJlbW92ZUNsYXNzKCdydW5uaW5nJyk7XHJcbiAgICAgICAgICAgICAgICBidG4ucHJvcChcImRpc2FibGVkXCIsIGZhbHNlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnRWNvc3lzdGVtJywgW10pXHJcbiAgICAvLyBjaGFuZ2UgQW5ndWxhciBkZWZhdWx0IHt7IHRvIHtbeyB0byBhdm9pZCBjb25mbGljdCB3aXRoIFR3aWdcclxuICAgIC5jb25maWcoWyckaW50ZXJwb2xhdGVQcm92aWRlcicsICckbG9jYXRpb25Qcm92aWRlcicsICgkaW50ZXJwb2xhdGVQcm92aWRlciwgJGxvY2F0aW9uUHJvdmlkZXIpID0+IHtcclxuICAgICAgICAkaW50ZXJwb2xhdGVQcm92aWRlci5zdGFydFN5bWJvbCgne1t7JykuZW5kU3ltYm9sKCd9XX0nKTtcclxuICAgICAgICAkbG9jYXRpb25Qcm92aWRlci5odG1sNU1vZGUoe1xyXG4gICAgICAgICAgICBlbmFibGVkOiB0cnVlLFxyXG4gICAgICAgICAgICByZXF1aXJlQmFzZTogZmFsc2UsXHJcbiAgICAgICAgICAgIHJld3JpdGVMaW5rczogZmFsc2VcclxuICAgICAgICB9KTtcclxuICAgIH1dKVxyXG4gICAgLmZpbHRlcigndW5zYWZlJywgWyckc2NlJywgKCRzY2UpID0+IHtcclxuICAgICAgICByZXR1cm4gKHZhbCkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gJHNjZS50cnVzdEFzSHRtbCh2YWwpO1xyXG4gICAgICAgIH07XHJcbiAgICB9XSlcclxuICAgIC5maWx0ZXIoJ3JhbmdlJywgKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiAoaW5wdXQsIHRvdGFsKSA9PiB7XHJcbiAgICAgICAgICAgIHRvdGFsID0gcGFyc2VJbnQodG90YWwpO1xyXG5cclxuICAgICAgICAgICAgZm9yIChsZXQgaT0wOyBpPHRvdGFsOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlucHV0LnB1c2goaSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiBpbnB1dDtcclxuICAgICAgICB9O1xyXG4gICAgfSlcclxuICAgIC5zZXJ2aWNlKCdlaWQnLCBbJyRodHRwJywgRWlkXSlcclxuICAgIC5kaXJlY3RpdmUoJ2Zvcm0nLCBbJyRsb2NhdGlvbicsICgkbG9jYXRpb24pID0+IHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICByZXN0cmljdDonRScsXHJcbiAgICAgICAgICAgIHByaW9yaXR5OiA5OTksXHJcbiAgICAgICAgICAgIGNvbXBpbGU6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJlOiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhdHRycy5ub2FjdGlvbiA9PT0gJycpIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGF0dHJzLmFjdGlvbiA9PT0gdW5kZWZpbmVkIHx8IGF0dHJzLmFjdGlvbiA9PT0gJycpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnMuYWN0aW9uID0gJGxvY2F0aW9uLmFic1VybCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfV0pXHJcbiAgICAuZGlyZWN0aXZlKCdpbml0VG9vbHRpcCcsICgpID0+IHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICByZXN0cmljdDogJ0EnLFxyXG4gICAgICAgICAgICBsaW5rOiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAkKGVsZW1lbnQpLnRvb2x0aXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRyaWdnZXIgOiAnaG92ZXInXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICB9KVxyXG47XHJcblxyXG5leHBvcnQgZGVmYXVsdCBhcHA7IiwiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKiAgQ29weXJpZ2h0IG5vdGljZVxyXG4gKlxyXG4gKiAgKGMpIDIwMTggRlRLIGUuVi4gPGZ0ay5kZT5cclxuICogIEFsbCByaWdodHMgcmVzZXJ2ZWRcclxuICpcclxuICogIFRoaXMgc2NyaXB0IGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAqICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gKiAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gKiAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuICpcclxuICogIFRoaXMgY29weXJpZ2h0IG5vdGljZSBNVVNUIEFQUEVBUiBpbiBhbGwgY29waWVzIG9mIHRoZSBzY3JpcHQhXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG4ndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgJ2lvbi1yYW5nZXNsaWRlcic7XHJcbmltcG9ydCAnaW9uLXJhbmdlc2xpZGVyL2Nzcy9pb24ucmFuZ2VTbGlkZXIuY3NzJztcclxuLy8gZml4IHNsaWRlciBpY29uXHJcbmltcG9ydCAnLi4vLi4vY3NzL0xpYnJhcnkvSW9uUmFuZ2VTbGlkZXIuY3NzJzsiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnanN0cmVlJztcclxuaW1wb3J0ICdqc3RyZWUvZGlzdC90aGVtZXMvZGVmYXVsdC9zdHlsZS5taW4uY3NzJzsiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnLi4vLi4vY3NzL0VsZW1lbnQvU3Rhci5jc3MnOyIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiJdLCJzb3VyY2VSb290IjoiIn0=