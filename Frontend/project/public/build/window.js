(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["window"],{

/***/ "./assets/css/Window.css":
/*!*******************************!*\
  !*** ./assets/css/Window.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/window.js":
/*!*****************************!*\
  !*** ./assets/js/window.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Library_Bootstrap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Library/Bootstrap */ "./assets/js/Library/Bootstrap.js");
/* harmony import */ var _css_Library_JsTree_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../css/Library/JsTree.css */ "./assets/css/Library/JsTree.css");
/* harmony import */ var _css_Library_JsTree_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_css_Library_JsTree_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _css_Style_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../css/Style.css */ "./assets/css/Style.css");
/* harmony import */ var _css_Style_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_css_Style_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _css_Window_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../css/Window.css */ "./assets/css/Window.css");
/* harmony import */ var _css_Window_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_css_Window_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var iframe_resizer_js_iframeResizer_contentWindow_min_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! iframe-resizer/js/iframeResizer.contentWindow.min.js */ "./node_modules/iframe-resizer/js/iframeResizer.contentWindow.min.js");
/* harmony import */ var iframe_resizer_js_iframeResizer_contentWindow_min_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(iframe_resizer_js_iframeResizer_contentWindow_min_js__WEBPACK_IMPORTED_MODULE_4__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
// Bootstrap
 // CSS



 // iFrame Resizer



/***/ })

},[["./assets/js/window.js","runtime","vendors~Admin/EmailCtrl~Admin/FileAudit~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Recor~b3f38238","vendors~Library/Bootstrap~layout~window","vendors~window","Library/Bootstrap~layout~window","layout~window"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL1dpbmRvdy5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3dpbmRvdy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSx1Qzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQWNBO0NBR0E7O0FBQ0E7QUFDQTtDQUdBIiwiZmlsZSI6IndpbmRvdy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuICogIENvcHlyaWdodCBub3RpY2VcclxuICpcclxuICogIChjKSAyMDE4IEZUSyBlLlYuIDxmdGsuZGU+XHJcbiAqICBBbGwgcmlnaHRzIHJlc2VydmVkXHJcbiAqXHJcbiAqICBUaGlzIHNjcmlwdCBpcyBkaXN0cmlidXRlZCBpbiB0aGUgaG9wZSB0aGF0IGl0IHdpbGwgYmUgdXNlZnVsLFxyXG4gKiAgYnV0IFdJVEhPVVQgQU5ZIFdBUlJBTlRZOyB3aXRob3V0IGV2ZW4gdGhlIGltcGxpZWQgd2FycmFudHkgb2ZcclxuICogIE1FUkNIQU5UQUJJTElUWSBvciBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRS4gIFNlZSB0aGVcclxuICogIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGZvciBtb3JlIGRldGFpbHMuXHJcbiAqXHJcbiAqICBUaGlzIGNvcHlyaWdodCBub3RpY2UgTVVTVCBBUFBFQVIgaW4gYWxsIGNvcGllcyBvZiB0aGUgc2NyaXB0IVxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLy8gQm9vdHN0cmFwXHJcbmltcG9ydCAnLi9MaWJyYXJ5L0Jvb3RzdHJhcCc7XHJcblxyXG4vLyBDU1NcclxuaW1wb3J0ICcuLi9jc3MvTGlicmFyeS9Kc1RyZWUuY3NzJztcclxuaW1wb3J0ICcuLi9jc3MvU3R5bGUuY3NzJztcclxuaW1wb3J0ICcuLi9jc3MvV2luZG93LmNzcyc7XHJcblxyXG4vLyBpRnJhbWUgUmVzaXplclxyXG5pbXBvcnQgJ2lmcmFtZS1yZXNpemVyL2pzL2lmcmFtZVJlc2l6ZXIuY29udGVudFdpbmRvdy5taW4uanMnOyJdLCJzb3VyY2VSb290IjoiIn0=