(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendors~STO/Display"],{

/***/ "./node_modules/iframe-resizer/js/iframeResizer.js":
/*!*********************************************************!*\
  !*** ./node_modules/iframe-resizer/js/iframeResizer.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__webpack_provided_window_dot_jQuery) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
 * File: iframeResizer.js
 * Desc: Force iframes to size to content.
 * Requires: iframeResizer.contentWindow.js to be loaded into the target frame.
 * Doc: https://github.com/davidjbradshaw/iframe-resizer
 * Author: David J. Bradshaw - dave@bradshaw.net
 * Contributor: Jure Mav - jure.mav@gmail.com
 * Contributor: Reed Dadoune - reed@dadoune.com
 */

(function(undefined) {
  'use strict';

  if (typeof window === 'undefined') return; // don't run for server side render

  var count = 0,
    logEnabled = false,
    hiddenCheckEnabled = false,
    msgHeader = 'message',
    msgHeaderLen = msgHeader.length,
    msgId = '[iFrameSizer]', //Must match iframe msg ID
    msgIdLen = msgId.length,
    pagePosition = null,
    requestAnimationFrame = window.requestAnimationFrame,
    resetRequiredMethods = {
      max: 1,
      scroll: 1,
      bodyScroll: 1,
      documentElementScroll: 1
    },
    settings = {},
    timer = null,
    logId = 'Host Page',
    defaults = {
      autoResize: true,
      bodyBackground: null,
      bodyMargin: null,
      bodyMarginV1: 8,
      bodyPadding: null,
      checkOrigin: true,
      inPageLinks: false,
      enablePublicMethods: true,
      heightCalculationMethod: 'bodyOffset',
      id: 'iFrameResizer',
      interval: 32,
      log: false,
      maxHeight: Infinity,
      maxWidth: Infinity,
      minHeight: 0,
      minWidth: 0,
      resizeFrom: 'parent',
      scrolling: false,
      sizeHeight: true,
      sizeWidth: false,
      warningTimeout: 5000,
      tolerance: 0,
      widthCalculationMethod: 'scroll',
      closedCallback: function() {},
      initCallback: function() {},
      messageCallback: function() {
        warn('MessageCallback function not defined');
      },
      resizedCallback: function() {},
      scrollCallback: function() {
        return true;
      }
    };

  function getMutationObserver() {
    return window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
  }

  function addEventListener(obj, evt, func) {
    /* istanbul ignore else */ // Not testable in PhantonJS
    if ('addEventListener' in window) {
      obj.addEventListener(evt, func, false);
    } else if ('attachEvent' in window) {
      //IE
      obj.attachEvent('on' + evt, func);
    }
  }

  function removeEventListener(el, evt, func) {
    /* istanbul ignore else */ // Not testable in phantonJS
    if ('removeEventListener' in window) {
      el.removeEventListener(evt, func, false);
    } else if ('detachEvent' in window) {
      //IE
      el.detachEvent('on' + evt, func);
    }
  }

  function setupRequestAnimationFrame() {
    var vendors = ['moz', 'webkit', 'o', 'ms'],
      x;

    // Remove vendor prefixing if prefixed and break early if not
    for (x = 0; x < vendors.length && !requestAnimationFrame; x += 1) {
      requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    }

    if (!requestAnimationFrame) {
      log('setup', 'RequestAnimationFrame not supported');
    }
  }

  function getMyID(iframeId) {
    var retStr = 'Host page: ' + iframeId;

    if (window.top !== window.self) {
      if (window.parentIFrame && window.parentIFrame.getId) {
        retStr = window.parentIFrame.getId() + ': ' + iframeId;
      } else {
        retStr = 'Nested host page: ' + iframeId;
      }
    }

    return retStr;
  }

  function formatLogHeader(iframeId) {
    return msgId + '[' + getMyID(iframeId) + ']';
  }

  function isLogEnabled(iframeId) {
    return settings[iframeId] ? settings[iframeId].log : logEnabled;
  }

  function log(iframeId, msg) {
    output('log', iframeId, msg, isLogEnabled(iframeId));
  }

  function info(iframeId, msg) {
    output('info', iframeId, msg, isLogEnabled(iframeId));
  }

  function warn(iframeId, msg) {
    output('warn', iframeId, msg, true);
  }

  function output(type, iframeId, msg, enabled) {
    if (true === enabled && 'object' === typeof window.console) {
      console[type](formatLogHeader(iframeId), msg);
    }
  }

  function iFrameListener(event) {
    function resizeIFrame() {
      function resize() {
        setSize(messageData);
        setPagePosition(iframeId);
        callback('resizedCallback', messageData);
      }

      ensureInRange('Height');
      ensureInRange('Width');

      syncResize(resize, messageData, 'init');
    }

    function processMsg() {
      var data = msg.substr(msgIdLen).split(':');

      return {
        iframe: settings[data[0]] && settings[data[0]].iframe,
        id: data[0],
        height: data[1],
        width: data[2],
        type: data[3]
      };
    }

    function ensureInRange(Dimension) {
      var max = Number(settings[iframeId]['max' + Dimension]),
        min = Number(settings[iframeId]['min' + Dimension]),
        dimension = Dimension.toLowerCase(),
        size = Number(messageData[dimension]);

      log(
        iframeId,
        'Checking ' + dimension + ' is in range ' + min + '-' + max
      );

      if (size < min) {
        size = min;
        log(iframeId, 'Set ' + dimension + ' to min value');
      }

      if (size > max) {
        size = max;
        log(iframeId, 'Set ' + dimension + ' to max value');
      }

      messageData[dimension] = '' + size;
    }

    function isMessageFromIFrame() {
      function checkAllowedOrigin() {
        function checkList() {
          var i = 0,
            retCode = false;

          log(
            iframeId,
            'Checking connection is from allowed list of origins: ' +
              checkOrigin
          );

          for (; i < checkOrigin.length; i++) {
            if (checkOrigin[i] === origin) {
              retCode = true;
              break;
            }
          }
          return retCode;
        }

        function checkSingle() {
          var remoteHost = settings[iframeId] && settings[iframeId].remoteHost;
          log(iframeId, 'Checking connection is from: ' + remoteHost);
          return origin === remoteHost;
        }

        return checkOrigin.constructor === Array ? checkList() : checkSingle();
      }

      var origin = event.origin,
        checkOrigin = settings[iframeId] && settings[iframeId].checkOrigin;

      if (checkOrigin && '' + origin !== 'null' && !checkAllowedOrigin()) {
        throw new Error(
          'Unexpected message received from: ' +
            origin +
            ' for ' +
            messageData.iframe.id +
            '. Message was: ' +
            event.data +
            '. This error can be disabled by setting the checkOrigin: false option or by providing of array of trusted domains.'
        );
      }

      return true;
    }

    function isMessageForUs() {
      return (
        msgId === ('' + msg).substr(0, msgIdLen) &&
        msg.substr(msgIdLen).split(':')[0] in settings
      ); //''+Protects against non-string msg
    }

    function isMessageFromMetaParent() {
      //Test if this message is from a parent above us. This is an ugly test, however, updating
      //the message format would break backwards compatibity.
      var retCode = messageData.type in { true: 1, false: 1, undefined: 1 };

      if (retCode) {
        log(iframeId, 'Ignoring init message from meta parent page');
      }

      return retCode;
    }

    function getMsgBody(offset) {
      return msg.substr(msg.indexOf(':') + msgHeaderLen + offset);
    }

    function forwardMsgFromIFrame(msgBody) {
      log(
        iframeId,
        'MessageCallback passed: {iframe: ' +
          messageData.iframe.id +
          ', message: ' +
          msgBody +
          '}'
      );
      callback('messageCallback', {
        iframe: messageData.iframe,
        message: JSON.parse(msgBody)
      });
      log(iframeId, '--');
    }

    function getPageInfo() {
      var bodyPosition = document.body.getBoundingClientRect(),
        iFramePosition = messageData.iframe.getBoundingClientRect();

      return JSON.stringify({
        iframeHeight: iFramePosition.height,
        iframeWidth: iFramePosition.width,
        clientHeight: Math.max(
          document.documentElement.clientHeight,
          window.innerHeight || 0
        ),
        clientWidth: Math.max(
          document.documentElement.clientWidth,
          window.innerWidth || 0
        ),
        offsetTop: parseInt(iFramePosition.top - bodyPosition.top, 10),
        offsetLeft: parseInt(iFramePosition.left - bodyPosition.left, 10),
        scrollTop: window.pageYOffset,
        scrollLeft: window.pageXOffset
      });
    }

    function sendPageInfoToIframe(iframe, iframeId) {
      function debouncedTrigger() {
        trigger(
          'Send Page Info',
          'pageInfo:' + getPageInfo(),
          iframe,
          iframeId
        );
      }
      debounceFrameEvents(debouncedTrigger, 32, iframeId);
    }

    function startPageInfoMonitor() {
      function setListener(type, func) {
        function sendPageInfo() {
          if (settings[id]) {
            sendPageInfoToIframe(settings[id].iframe, id);
          } else {
            stop();
          }
        }

        ['scroll', 'resize'].forEach(function(evt) {
          log(id, type + evt + ' listener for sendPageInfo');
          func(window, evt, sendPageInfo);
        });
      }

      function stop() {
        setListener('Remove ', removeEventListener);
      }

      function start() {
        setListener('Add ', addEventListener);
      }

      var id = iframeId; //Create locally scoped copy of iFrame ID

      start();

      if (settings[id]) {
        settings[id].stopPageInfo = stop;
      }
    }

    function stopPageInfoMonitor() {
      if (settings[iframeId] && settings[iframeId].stopPageInfo) {
        settings[iframeId].stopPageInfo();
        delete settings[iframeId].stopPageInfo;
      }
    }

    function checkIFrameExists() {
      var retBool = true;

      if (null === messageData.iframe) {
        warn(iframeId, 'IFrame (' + messageData.id + ') not found');
        retBool = false;
      }
      return retBool;
    }

    function getElementPosition(target) {
      var iFramePosition = target.getBoundingClientRect();

      getPagePosition(iframeId);

      return {
        x: Math.floor(Number(iFramePosition.left) + Number(pagePosition.x)),
        y: Math.floor(Number(iFramePosition.top) + Number(pagePosition.y))
      };
    }

    function scrollRequestFromChild(addOffset) {
      /* istanbul ignore next */ //Not testable in Karma
      function reposition() {
        pagePosition = newPosition;
        scrollTo();
        log(iframeId, '--');
      }

      function calcOffset() {
        return {
          x: Number(messageData.width) + offset.x,
          y: Number(messageData.height) + offset.y
        };
      }

      function scrollParent() {
        if (window.parentIFrame) {
          window.parentIFrame['scrollTo' + (addOffset ? 'Offset' : '')](
            newPosition.x,
            newPosition.y
          );
        } else {
          warn(
            iframeId,
            'Unable to scroll to requested position, window.parentIFrame not found'
          );
        }
      }

      var offset = addOffset
          ? getElementPosition(messageData.iframe)
          : { x: 0, y: 0 },
        newPosition = calcOffset();

      log(
        iframeId,
        'Reposition requested from iFrame (offset x:' +
          offset.x +
          ' y:' +
          offset.y +
          ')'
      );

      if (window.top !== window.self) {
        scrollParent();
      } else {
        reposition();
      }
    }

    function scrollTo() {
      if (false !== callback('scrollCallback', pagePosition)) {
        setPagePosition(iframeId);
      } else {
        unsetPagePosition();
      }
    }

    function findTarget(location) {
      function jumpToTarget() {
        var jumpPosition = getElementPosition(target);

        log(
          iframeId,
          'Moving to in page link (#' +
            hash +
            ') at x: ' +
            jumpPosition.x +
            ' y: ' +
            jumpPosition.y
        );
        pagePosition = {
          x: jumpPosition.x,
          y: jumpPosition.y
        };

        scrollTo();
        log(iframeId, '--');
      }

      function jumpToParent() {
        if (window.parentIFrame) {
          window.parentIFrame.moveToAnchor(hash);
        } else {
          log(
            iframeId,
            'In page link #' +
              hash +
              ' not found and window.parentIFrame not found'
          );
        }
      }

      var hash = location.split('#')[1] || '',
        hashData = decodeURIComponent(hash),
        target =
          document.getElementById(hashData) ||
          document.getElementsByName(hashData)[0];

      if (target) {
        jumpToTarget();
      } else if (window.top !== window.self) {
        jumpToParent();
      } else {
        log(iframeId, 'In page link #' + hash + ' not found');
      }
    }

    function callback(funcName, val) {
      return chkCallback(iframeId, funcName, val);
    }

    function actionMsg() {
      if (settings[iframeId] && settings[iframeId].firstRun) firstRun();

      switch (messageData.type) {
        case 'close':
          if (settings[iframeId].closeRequestCallback)
            chkCallback(
              iframeId,
              'closeRequestCallback',
              settings[iframeId].iframe
            );
          else closeIFrame(messageData.iframe);
          break;
        case 'message':
          forwardMsgFromIFrame(getMsgBody(6));
          break;
        case 'scrollTo':
          scrollRequestFromChild(false);
          break;
        case 'scrollToOffset':
          scrollRequestFromChild(true);
          break;
        case 'pageInfo':
          sendPageInfoToIframe(
            settings[iframeId] && settings[iframeId].iframe,
            iframeId
          );
          startPageInfoMonitor();
          break;
        case 'pageInfoStop':
          stopPageInfoMonitor();
          break;
        case 'inPageLink':
          findTarget(getMsgBody(9));
          break;
        case 'reset':
          resetIFrame(messageData);
          break;
        case 'init':
          resizeIFrame();
          callback('initCallback', messageData.iframe);
          break;
        default:
          resizeIFrame();
      }
    }

    function hasSettings(iframeId) {
      var retBool = true;

      if (!settings[iframeId]) {
        retBool = false;
        warn(
          messageData.type +
            ' No settings for ' +
            iframeId +
            '. Message was: ' +
            msg
        );
      }

      return retBool;
    }

    function iFrameReadyMsgReceived() {
      for (var iframeId in settings) {
        trigger(
          'iFrame requested init',
          createOutgoingMsg(iframeId),
          document.getElementById(iframeId),
          iframeId
        );
      }
    }

    function firstRun() {
      if (settings[iframeId]) {
        settings[iframeId].firstRun = false;
      }
    }

    function clearWarningTimeout() {
      if (settings[iframeId]) {
        clearTimeout(settings[iframeId].msgTimeout);
        settings[iframeId].warningTimeout = 0;
      }
    }

    var msg = event.data,
      messageData = {},
      iframeId = null;

    if ('[iFrameResizerChild]Ready' === msg) {
      iFrameReadyMsgReceived();
    } else if (isMessageForUs()) {
      messageData = processMsg();
      iframeId = logId = messageData.id;
      if (settings[iframeId]) {
        settings[iframeId].loaded = true;
      }

      if (!isMessageFromMetaParent() && hasSettings(iframeId)) {
        log(iframeId, 'Received: ' + msg);

        if (checkIFrameExists() && isMessageFromIFrame()) {
          actionMsg();
        }
      }
    } else {
      info(iframeId, 'Ignored: ' + msg);
    }
  }

  function chkCallback(iframeId, funcName, val) {
    var func = null,
      retVal = null;

    if (settings[iframeId]) {
      func = settings[iframeId][funcName];

      if ('function' === typeof func) {
        retVal = func(val);
      } else {
        throw new TypeError(
          funcName + ' on iFrame[' + iframeId + '] is not a function'
        );
      }
    }

    return retVal;
  }

  function removeIframeListeners(iframe) {
    var iframeId = iframe.id;
    delete settings[iframeId];
  }

  function closeIFrame(iframe) {
    var iframeId = iframe.id;
    log(iframeId, 'Removing iFrame: ' + iframeId);

    try {
      // Catch race condition error with React
      if (iframe.parentNode) {
        iframe.parentNode.removeChild(iframe);
      }
    } catch (e) {}

    chkCallback(iframeId, 'closedCallback', iframeId);
    log(iframeId, '--');
    removeIframeListeners(iframe);
  }

  function getPagePosition(iframeId) {
    if (null === pagePosition) {
      pagePosition = {
        x:
          window.pageXOffset !== undefined
            ? window.pageXOffset
            : document.documentElement.scrollLeft,
        y:
          window.pageYOffset !== undefined
            ? window.pageYOffset
            : document.documentElement.scrollTop
      };
      log(
        iframeId,
        'Get page position: ' + pagePosition.x + ',' + pagePosition.y
      );
    }
  }

  function setPagePosition(iframeId) {
    if (null !== pagePosition) {
      window.scrollTo(pagePosition.x, pagePosition.y);
      log(
        iframeId,
        'Set page position: ' + pagePosition.x + ',' + pagePosition.y
      );
      unsetPagePosition();
    }
  }

  function unsetPagePosition() {
    pagePosition = null;
  }

  function resetIFrame(messageData) {
    function reset() {
      setSize(messageData);
      trigger('reset', 'reset', messageData.iframe, messageData.id);
    }

    log(
      messageData.id,
      'Size reset requested by ' +
        ('init' === messageData.type ? 'host page' : 'iFrame')
    );
    getPagePosition(messageData.id);
    syncResize(reset, messageData, 'reset');
  }

  function setSize(messageData) {
    function setDimension(dimension) {
      if (!messageData.id) {
        log('undefined', 'messageData id not set');
        return;
      }
      messageData.iframe.style[dimension] = messageData[dimension] + 'px';
      log(
        messageData.id,
        'IFrame (' +
          iframeId +
          ') ' +
          dimension +
          ' set to ' +
          messageData[dimension] +
          'px'
      );
    }

    function chkZero(dimension) {
      //FireFox sets dimension of hidden iFrames to zero.
      //So if we detect that set up an event to check for
      //when iFrame becomes visible.

      /* istanbul ignore next */ //Not testable in PhantomJS
      if (!hiddenCheckEnabled && '0' === messageData[dimension]) {
        hiddenCheckEnabled = true;
        log(iframeId, 'Hidden iFrame detected, creating visibility listener');
        fixHiddenIFrames();
      }
    }

    function processDimension(dimension) {
      setDimension(dimension);
      chkZero(dimension);
    }

    var iframeId = messageData.iframe.id;

    if (settings[iframeId]) {
      if (settings[iframeId].sizeHeight) {
        processDimension('height');
      }
      if (settings[iframeId].sizeWidth) {
        processDimension('width');
      }
    }
  }

  function syncResize(func, messageData, doNotSync) {
    /* istanbul ignore if */ //Not testable in PhantomJS
    if (doNotSync !== messageData.type && requestAnimationFrame) {
      log(messageData.id, 'Requesting animation frame');
      requestAnimationFrame(func);
    } else {
      func();
    }
  }

  function trigger(calleeMsg, msg, iframe, id, noResponseWarning) {
    function postMessageToIFrame() {
      var target = settings[id] && settings[id].targetOrigin;
      log(
        id,
        '[' +
          calleeMsg +
          '] Sending msg to iframe[' +
          id +
          '] (' +
          msg +
          ') targetOrigin: ' +
          target
      );
      iframe.contentWindow.postMessage(msgId + msg, target);
    }

    function iFrameNotFound() {
      warn(id, '[' + calleeMsg + '] IFrame(' + id + ') not found');
    }

    function chkAndSend() {
      if (
        iframe &&
        'contentWindow' in iframe &&
        null !== iframe.contentWindow
      ) {
        //Null test for PhantomJS
        postMessageToIFrame();
      } else {
        iFrameNotFound();
      }
    }

    function warnOnNoResponse() {
      function warning() {
        if (settings[id] && !settings[id].loaded && !errorShown) {
          errorShown = true;
          warn(
            id,
            'IFrame has not responded within ' +
              settings[id].warningTimeout / 1000 +
              ' seconds. Check iFrameResizer.contentWindow.js has been loaded in iFrame. This message can be ignored if everything is working, or you can set the warningTimeout option to a higher value or zero to suppress this warning.'
          );
        }
      }

      if (
        !!noResponseWarning &&
        settings[id] &&
        !!settings[id].warningTimeout
      ) {
        settings[id].msgTimeout = setTimeout(
          warning,
          settings[id].warningTimeout
        );
      }
    }

    var errorShown = false;

    id = id || iframe.id;

    if (settings[id]) {
      chkAndSend();
      warnOnNoResponse();
    }
  }

  function createOutgoingMsg(iframeId) {
    return (
      iframeId +
      ':' +
      settings[iframeId].bodyMarginV1 +
      ':' +
      settings[iframeId].sizeWidth +
      ':' +
      settings[iframeId].log +
      ':' +
      settings[iframeId].interval +
      ':' +
      settings[iframeId].enablePublicMethods +
      ':' +
      settings[iframeId].autoResize +
      ':' +
      settings[iframeId].bodyMargin +
      ':' +
      settings[iframeId].heightCalculationMethod +
      ':' +
      settings[iframeId].bodyBackground +
      ':' +
      settings[iframeId].bodyPadding +
      ':' +
      settings[iframeId].tolerance +
      ':' +
      settings[iframeId].inPageLinks +
      ':' +
      settings[iframeId].resizeFrom +
      ':' +
      settings[iframeId].widthCalculationMethod
    );
  }

  function setupIFrame(iframe, options) {
    function setLimits() {
      function addStyle(style) {
        if (
          Infinity !== settings[iframeId][style] &&
          0 !== settings[iframeId][style]
        ) {
          iframe.style[style] = settings[iframeId][style] + 'px';
          log(
            iframeId,
            'Set ' + style + ' = ' + settings[iframeId][style] + 'px'
          );
        }
      }

      function chkMinMax(dimension) {
        if (
          settings[iframeId]['min' + dimension] >
          settings[iframeId]['max' + dimension]
        ) {
          throw new Error(
            'Value for min' +
              dimension +
              ' can not be greater than max' +
              dimension
          );
        }
      }

      chkMinMax('Height');
      chkMinMax('Width');

      addStyle('maxHeight');
      addStyle('minHeight');
      addStyle('maxWidth');
      addStyle('minWidth');
    }

    function newId() {
      var id = (options && options.id) || defaults.id + count++;
      if (null !== document.getElementById(id)) {
        id = id + count++;
      }
      return id;
    }

    function ensureHasId(iframeId) {
      logId = iframeId;
      if ('' === iframeId) {
        iframe.id = iframeId = newId();
        logEnabled = (options || {}).log;
        logId = iframeId;
        log(
          iframeId,
          'Added missing iframe ID: ' + iframeId + ' (' + iframe.src + ')'
        );
      }

      return iframeId;
    }

    function setScrolling() {
      log(
        iframeId,
        'IFrame scrolling ' +
          (settings[iframeId] && settings[iframeId].scrolling
            ? 'enabled'
            : 'disabled') +
          ' for ' +
          iframeId
      );
      iframe.style.overflow =
        false === (settings[iframeId] && settings[iframeId].scrolling)
          ? 'hidden'
          : 'auto';
      switch (settings[iframeId] && settings[iframeId].scrolling) {
        case 'omit':
          break;
        case true:
          iframe.scrolling = 'yes';
          break;
        case false:
          iframe.scrolling = 'no';
          break;
        default:
          iframe.scrolling = settings[iframeId]
            ? settings[iframeId].scrolling
            : 'no';
      }
    }

    //The V1 iFrame script expects an int, where as in V2 expects a CSS
    //string value such as '1px 3em', so if we have an int for V2, set V1=V2
    //and then convert V2 to a string PX value.
    function setupBodyMarginValues() {
      if (
        'number' ===
          typeof (settings[iframeId] && settings[iframeId].bodyMargin) ||
        '0' === (settings[iframeId] && settings[iframeId].bodyMargin)
      ) {
        settings[iframeId].bodyMarginV1 = settings[iframeId].bodyMargin;
        settings[iframeId].bodyMargin =
          '' + settings[iframeId].bodyMargin + 'px';
      }
    }

    function checkReset() {
      // Reduce scope of firstRun to function, because IE8's JS execution
      // context stack is borked and this value gets externally
      // changed midway through running this function!!!
      var firstRun = settings[iframeId] && settings[iframeId].firstRun,
        resetRequertMethod =
          settings[iframeId] &&
          settings[iframeId].heightCalculationMethod in resetRequiredMethods;

      if (!firstRun && resetRequertMethod) {
        resetIFrame({ iframe: iframe, height: 0, width: 0, type: 'init' });
      }
    }

    function setupIFrameObject() {
      if (Function.prototype.bind && settings[iframeId]) {
        //Ignore unpolyfilled IE8.
        settings[iframeId].iframe.iFrameResizer = {
          close: closeIFrame.bind(null, settings[iframeId].iframe),

          removeListeners: removeIframeListeners.bind(
            null,
            settings[iframeId].iframe
          ),

          resize: trigger.bind(
            null,
            'Window resize',
            'resize',
            settings[iframeId].iframe
          ),

          moveToAnchor: function(anchor) {
            trigger(
              'Move to anchor',
              'moveToAnchor:' + anchor,
              settings[iframeId].iframe,
              iframeId
            );
          },

          sendMessage: function(message) {
            message = JSON.stringify(message);
            trigger(
              'Send Message',
              'message:' + message,
              settings[iframeId].iframe,
              iframeId
            );
          }
        };
      }
    }

    //We have to call trigger twice, as we can not be sure if all
    //iframes have completed loading when this code runs. The
    //event listener also catches the page changing in the iFrame.
    function init(msg) {
      function iFrameLoaded() {
        trigger('iFrame.onload', msg, iframe, undefined, true);
        checkReset();
      }

      function createDestroyObserver(MutationObserver) {
        if (!iframe.parentNode) {
          return;
        }

        var destroyObserver = new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
            var removedNodes = Array.prototype.slice.call(mutation.removedNodes); // Transform NodeList into an Array
            removedNodes.forEach(function (removedNode) {
              if (removedNode === iframe) {
                closeIFrame(iframe);
              }
            });
          });
        });
        destroyObserver.observe(iframe.parentNode, {
          childList: true
        });
      }

      var MutationObserver = getMutationObserver();
      if (MutationObserver) {
        createDestroyObserver(MutationObserver);
      }

      addEventListener(iframe, 'load', iFrameLoaded);
      trigger('init', msg, iframe, undefined, true);
    }

    function checkOptions(options) {
      if ('object' !== typeof options) {
        throw new TypeError('Options is not an object');
      }
    }

    function copyOptions(options) {
      for (var option in defaults) {
        if (defaults.hasOwnProperty(option)) {
          settings[iframeId][option] = options.hasOwnProperty(option)
            ? options[option]
            : defaults[option];
        }
      }
    }

    function getTargetOrigin(remoteHost) {
      return '' === remoteHost || 'file://' === remoteHost ? '*' : remoteHost;
    }

    function processOptions(options) {
      options = options || {};
      settings[iframeId] = {
        firstRun: true,
        iframe: iframe,
        remoteHost: iframe.src
          .split('/')
          .slice(0, 3)
          .join('/')
      };

      checkOptions(options);
      copyOptions(options);

      if (settings[iframeId]) {
        settings[iframeId].targetOrigin =
          true === settings[iframeId].checkOrigin
            ? getTargetOrigin(settings[iframeId].remoteHost)
            : '*';
      }
    }

    function beenHere() {
      return iframeId in settings && 'iFrameResizer' in iframe;
    }

    var iframeId = ensureHasId(iframe.id);

    if (!beenHere()) {
      processOptions(options);
      setScrolling();
      setLimits();
      setupBodyMarginValues();
      init(createOutgoingMsg(iframeId));
      setupIFrameObject();
    } else {
      warn(iframeId, 'Ignored iFrame, already setup.');
    }
  }

  function debouce(fn, time) {
    if (null === timer) {
      timer = setTimeout(function() {
        timer = null;
        fn();
      }, time);
    }
  }

  var frameTimer = {};
  function debounceFrameEvents(fn, time, frameId) {
    if (!frameTimer[frameId]) {
      frameTimer[frameId] = setTimeout(function() {
        frameTimer[frameId] = null;
        fn();
      }, time);
    }
  } //Not testable in PhantomJS

  /* istanbul ignore next */ function fixHiddenIFrames() {
    function checkIFrames() {
      function checkIFrame(settingId) {
        function chkDimension(dimension) {
          return (
            '0px' ===
            (settings[settingId] && settings[settingId].iframe.style[dimension])
          );
        }

        function isVisible(el) {
          return null !== el.offsetParent;
        }

        if (
          settings[settingId] &&
          isVisible(settings[settingId].iframe) &&
          (chkDimension('height') || chkDimension('width'))
        ) {
          trigger(
            'Visibility change',
            'resize',
            settings[settingId].iframe,
            settingId
          );
        }
      }

      for (var settingId in settings) {
        checkIFrame(settingId);
      }
    }

    function mutationObserved(mutations) {
      log(
        'window',
        'Mutation observed: ' + mutations[0].target + ' ' + mutations[0].type
      );
      debouce(checkIFrames, 16);
    }

    function createMutationObserver() {
      var target = document.querySelector('body'),
        config = {
          attributes: true,
          attributeOldValue: false,
          characterData: true,
          characterDataOldValue: false,
          childList: true,
          subtree: true
        },
        observer = new MutationObserver(mutationObserved);

      observer.observe(target, config);
    }

    var MutationObserver = getMutationObserver();
    if (MutationObserver) {
      createMutationObserver();
    }
  }

  function resizeIFrames(event) {
    function resize() {
      sendTriggerMsg('Window ' + event, 'resize');
    }

    log('window', 'Trigger event: ' + event);
    debouce(resize, 16);
  } //Not testable in PhantomJS

  /* istanbul ignore next */ function tabVisible() {
    function resize() {
      sendTriggerMsg('Tab Visable', 'resize');
    }

    if ('hidden' !== document.visibilityState) {
      log('document', 'Trigger event: Visiblity change');
      debouce(resize, 16);
    }
  }

  function sendTriggerMsg(eventName, event) {
    function isIFrameResizeEnabled(iframeId) {
      return (
        settings[iframeId] &&
        'parent' === settings[iframeId].resizeFrom &&
        settings[iframeId].autoResize &&
        !settings[iframeId].firstRun
      );
    }

    for (var iframeId in settings) {
      if (isIFrameResizeEnabled(iframeId)) {
        trigger(eventName, event, document.getElementById(iframeId), iframeId);
      }
    }
  }

  function setupEventListeners() {
    addEventListener(window, 'message', iFrameListener);

    addEventListener(window, 'resize', function() {
      resizeIFrames('resize');
    });

    addEventListener(document, 'visibilitychange', tabVisible);
    addEventListener(document, '-webkit-visibilitychange', tabVisible); //Andriod 4.4
    addEventListener(window, 'focusin', function() {
      resizeIFrames('focus');
    }); //IE8-9
    addEventListener(window, 'focus', function() {
      resizeIFrames('focus');
    });
  }

  function factory() {
    function init(options, element) {
      function chkType() {
        if (!element.tagName) {
          throw new TypeError('Object is not a valid DOM element');
        } else if ('IFRAME' !== element.tagName.toUpperCase()) {
          throw new TypeError(
            'Expected <IFRAME> tag, found <' + element.tagName + '>'
          );
        }
      }

      if (element) {
        chkType();
        setupIFrame(element, options);
        iFrames.push(element);
      }
    }

    function warnDeprecatedOptions(options) {
      if (options && options.enablePublicMethods) {
        warn(
          'enablePublicMethods option has been removed, public methods are now always available in the iFrame'
        );
      }
    }

    var iFrames;

    setupRequestAnimationFrame();
    setupEventListeners();

    return function iFrameResizeF(options, target) {
      iFrames = []; //Only return iFrames past in on this call

      warnDeprecatedOptions(options);

      switch (typeof target) {
        case 'undefined':
        case 'string':
          Array.prototype.forEach.call(
            document.querySelectorAll(target || 'iframe'),
            init.bind(undefined, options)
          );
          break;
        case 'object':
          init(options, target);
          break;
        default:
          throw new TypeError('Unexpected data type (' + typeof target + ')');
      }

      return iFrames;
    };
  }

  function createJQueryPublicMethod($) {
    if (!$.fn) {
      info('', 'Unable to bind to jQuery, it is not fully loaded.');
    } else if (!$.fn.iFrameResize) {
      $.fn.iFrameResize = function $iFrameResizeF(options) {
        function init(index, element) {
          setupIFrame(element, options);
        }

        return this.filter('iframe')
          .each(init)
          .end();
      };
    }
  }

  if (__webpack_provided_window_dot_jQuery) {
    createJQueryPublicMethod(__webpack_provided_window_dot_jQuery);
  }

  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
  window.iFrameResize = window.iFrameResize || factory();
})();

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvaWZyYW1lLXJlc2l6ZXIvanMvaWZyYW1lUmVzaXplci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw0Q0FBNEM7O0FBRTVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQSxPQUFPO0FBQ1Asb0NBQW9DO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxlQUFlLDhDQUE4QztBQUM3RDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsZ0JBQWdCLHdCQUF3QjtBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7O0FBRUE7QUFDQTtBQUNBO0FBQ0EseUNBQXlDOztBQUV6QztBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSx3QkFBd0I7O0FBRXhCOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWEsYUFBYTtBQUMxQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHNCQUFzQjtBQUN0Qjs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EscUJBQXFCLG9EQUFvRDtBQUN6RTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVzs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUZBQWlGO0FBQ2pGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFdBQVc7QUFDWCxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQSx1RUFBdUU7QUFDdkU7QUFDQTtBQUNBLEtBQUssRUFBRTtBQUNQO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbUJBQW1COztBQUVuQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU0sb0NBQWE7QUFDbkIsNkJBQTZCLG9DQUFhO0FBQzFDOztBQUVBLE1BQU0sSUFBMEM7QUFDaEQsSUFBSSxpQ0FBTyxFQUFFLG9DQUFFLE9BQU87QUFBQTtBQUFBO0FBQUEsb0dBQUM7QUFDdkIsR0FBRyxNQUFNLEVBR047QUFDSDtBQUNBLENBQUMiLCJmaWxlIjoidmVuZG9yc35TVE8vRGlzcGxheS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBGaWxlOiBpZnJhbWVSZXNpemVyLmpzXG4gKiBEZXNjOiBGb3JjZSBpZnJhbWVzIHRvIHNpemUgdG8gY29udGVudC5cbiAqIFJlcXVpcmVzOiBpZnJhbWVSZXNpemVyLmNvbnRlbnRXaW5kb3cuanMgdG8gYmUgbG9hZGVkIGludG8gdGhlIHRhcmdldCBmcmFtZS5cbiAqIERvYzogaHR0cHM6Ly9naXRodWIuY29tL2RhdmlkamJyYWRzaGF3L2lmcmFtZS1yZXNpemVyXG4gKiBBdXRob3I6IERhdmlkIEouIEJyYWRzaGF3IC0gZGF2ZUBicmFkc2hhdy5uZXRcbiAqIENvbnRyaWJ1dG9yOiBKdXJlIE1hdiAtIGp1cmUubWF2QGdtYWlsLmNvbVxuICogQ29udHJpYnV0b3I6IFJlZWQgRGFkb3VuZSAtIHJlZWRAZGFkb3VuZS5jb21cbiAqL1xuXG4oZnVuY3Rpb24odW5kZWZpbmVkKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBpZiAodHlwZW9mIHdpbmRvdyA9PT0gJ3VuZGVmaW5lZCcpIHJldHVybjsgLy8gZG9uJ3QgcnVuIGZvciBzZXJ2ZXIgc2lkZSByZW5kZXJcblxuICB2YXIgY291bnQgPSAwLFxuICAgIGxvZ0VuYWJsZWQgPSBmYWxzZSxcbiAgICBoaWRkZW5DaGVja0VuYWJsZWQgPSBmYWxzZSxcbiAgICBtc2dIZWFkZXIgPSAnbWVzc2FnZScsXG4gICAgbXNnSGVhZGVyTGVuID0gbXNnSGVhZGVyLmxlbmd0aCxcbiAgICBtc2dJZCA9ICdbaUZyYW1lU2l6ZXJdJywgLy9NdXN0IG1hdGNoIGlmcmFtZSBtc2cgSURcbiAgICBtc2dJZExlbiA9IG1zZ0lkLmxlbmd0aCxcbiAgICBwYWdlUG9zaXRpb24gPSBudWxsLFxuICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZSA9IHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUsXG4gICAgcmVzZXRSZXF1aXJlZE1ldGhvZHMgPSB7XG4gICAgICBtYXg6IDEsXG4gICAgICBzY3JvbGw6IDEsXG4gICAgICBib2R5U2Nyb2xsOiAxLFxuICAgICAgZG9jdW1lbnRFbGVtZW50U2Nyb2xsOiAxXG4gICAgfSxcbiAgICBzZXR0aW5ncyA9IHt9LFxuICAgIHRpbWVyID0gbnVsbCxcbiAgICBsb2dJZCA9ICdIb3N0IFBhZ2UnLFxuICAgIGRlZmF1bHRzID0ge1xuICAgICAgYXV0b1Jlc2l6ZTogdHJ1ZSxcbiAgICAgIGJvZHlCYWNrZ3JvdW5kOiBudWxsLFxuICAgICAgYm9keU1hcmdpbjogbnVsbCxcbiAgICAgIGJvZHlNYXJnaW5WMTogOCxcbiAgICAgIGJvZHlQYWRkaW5nOiBudWxsLFxuICAgICAgY2hlY2tPcmlnaW46IHRydWUsXG4gICAgICBpblBhZ2VMaW5rczogZmFsc2UsXG4gICAgICBlbmFibGVQdWJsaWNNZXRob2RzOiB0cnVlLFxuICAgICAgaGVpZ2h0Q2FsY3VsYXRpb25NZXRob2Q6ICdib2R5T2Zmc2V0JyxcbiAgICAgIGlkOiAnaUZyYW1lUmVzaXplcicsXG4gICAgICBpbnRlcnZhbDogMzIsXG4gICAgICBsb2c6IGZhbHNlLFxuICAgICAgbWF4SGVpZ2h0OiBJbmZpbml0eSxcbiAgICAgIG1heFdpZHRoOiBJbmZpbml0eSxcbiAgICAgIG1pbkhlaWdodDogMCxcbiAgICAgIG1pbldpZHRoOiAwLFxuICAgICAgcmVzaXplRnJvbTogJ3BhcmVudCcsXG4gICAgICBzY3JvbGxpbmc6IGZhbHNlLFxuICAgICAgc2l6ZUhlaWdodDogdHJ1ZSxcbiAgICAgIHNpemVXaWR0aDogZmFsc2UsXG4gICAgICB3YXJuaW5nVGltZW91dDogNTAwMCxcbiAgICAgIHRvbGVyYW5jZTogMCxcbiAgICAgIHdpZHRoQ2FsY3VsYXRpb25NZXRob2Q6ICdzY3JvbGwnLFxuICAgICAgY2xvc2VkQ2FsbGJhY2s6IGZ1bmN0aW9uKCkge30sXG4gICAgICBpbml0Q2FsbGJhY2s6IGZ1bmN0aW9uKCkge30sXG4gICAgICBtZXNzYWdlQ2FsbGJhY2s6IGZ1bmN0aW9uKCkge1xuICAgICAgICB3YXJuKCdNZXNzYWdlQ2FsbGJhY2sgZnVuY3Rpb24gbm90IGRlZmluZWQnKTtcbiAgICAgIH0sXG4gICAgICByZXNpemVkQ2FsbGJhY2s6IGZ1bmN0aW9uKCkge30sXG4gICAgICBzY3JvbGxDYWxsYmFjazogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgIH07XG5cbiAgZnVuY3Rpb24gZ2V0TXV0YXRpb25PYnNlcnZlcigpIHtcbiAgICByZXR1cm4gd2luZG93Lk11dGF0aW9uT2JzZXJ2ZXIgfHwgd2luZG93LldlYktpdE11dGF0aW9uT2JzZXJ2ZXIgfHwgd2luZG93Lk1vek11dGF0aW9uT2JzZXJ2ZXI7XG4gIH1cblxuICBmdW5jdGlvbiBhZGRFdmVudExpc3RlbmVyKG9iaiwgZXZ0LCBmdW5jKSB7XG4gICAgLyogaXN0YW5idWwgaWdub3JlIGVsc2UgKi8gLy8gTm90IHRlc3RhYmxlIGluIFBoYW50b25KU1xuICAgIGlmICgnYWRkRXZlbnRMaXN0ZW5lcicgaW4gd2luZG93KSB7XG4gICAgICBvYmouYWRkRXZlbnRMaXN0ZW5lcihldnQsIGZ1bmMsIGZhbHNlKTtcbiAgICB9IGVsc2UgaWYgKCdhdHRhY2hFdmVudCcgaW4gd2luZG93KSB7XG4gICAgICAvL0lFXG4gICAgICBvYmouYXR0YWNoRXZlbnQoJ29uJyArIGV2dCwgZnVuYyk7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gcmVtb3ZlRXZlbnRMaXN0ZW5lcihlbCwgZXZ0LCBmdW5jKSB7XG4gICAgLyogaXN0YW5idWwgaWdub3JlIGVsc2UgKi8gLy8gTm90IHRlc3RhYmxlIGluIHBoYW50b25KU1xuICAgIGlmICgncmVtb3ZlRXZlbnRMaXN0ZW5lcicgaW4gd2luZG93KSB7XG4gICAgICBlbC5yZW1vdmVFdmVudExpc3RlbmVyKGV2dCwgZnVuYywgZmFsc2UpO1xuICAgIH0gZWxzZSBpZiAoJ2RldGFjaEV2ZW50JyBpbiB3aW5kb3cpIHtcbiAgICAgIC8vSUVcbiAgICAgIGVsLmRldGFjaEV2ZW50KCdvbicgKyBldnQsIGZ1bmMpO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHNldHVwUmVxdWVzdEFuaW1hdGlvbkZyYW1lKCkge1xuICAgIHZhciB2ZW5kb3JzID0gWydtb3onLCAnd2Via2l0JywgJ28nLCAnbXMnXSxcbiAgICAgIHg7XG5cbiAgICAvLyBSZW1vdmUgdmVuZG9yIHByZWZpeGluZyBpZiBwcmVmaXhlZCBhbmQgYnJlYWsgZWFybHkgaWYgbm90XG4gICAgZm9yICh4ID0gMDsgeCA8IHZlbmRvcnMubGVuZ3RoICYmICFyZXF1ZXN0QW5pbWF0aW9uRnJhbWU7IHggKz0gMSkge1xuICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93W3ZlbmRvcnNbeF0gKyAnUmVxdWVzdEFuaW1hdGlvbkZyYW1lJ107XG4gICAgfVxuXG4gICAgaWYgKCFyZXF1ZXN0QW5pbWF0aW9uRnJhbWUpIHtcbiAgICAgIGxvZygnc2V0dXAnLCAnUmVxdWVzdEFuaW1hdGlvbkZyYW1lIG5vdCBzdXBwb3J0ZWQnKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBnZXRNeUlEKGlmcmFtZUlkKSB7XG4gICAgdmFyIHJldFN0ciA9ICdIb3N0IHBhZ2U6ICcgKyBpZnJhbWVJZDtcblxuICAgIGlmICh3aW5kb3cudG9wICE9PSB3aW5kb3cuc2VsZikge1xuICAgICAgaWYgKHdpbmRvdy5wYXJlbnRJRnJhbWUgJiYgd2luZG93LnBhcmVudElGcmFtZS5nZXRJZCkge1xuICAgICAgICByZXRTdHIgPSB3aW5kb3cucGFyZW50SUZyYW1lLmdldElkKCkgKyAnOiAnICsgaWZyYW1lSWQ7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXRTdHIgPSAnTmVzdGVkIGhvc3QgcGFnZTogJyArIGlmcmFtZUlkO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiByZXRTdHI7XG4gIH1cblxuICBmdW5jdGlvbiBmb3JtYXRMb2dIZWFkZXIoaWZyYW1lSWQpIHtcbiAgICByZXR1cm4gbXNnSWQgKyAnWycgKyBnZXRNeUlEKGlmcmFtZUlkKSArICddJztcbiAgfVxuXG4gIGZ1bmN0aW9uIGlzTG9nRW5hYmxlZChpZnJhbWVJZCkge1xuICAgIHJldHVybiBzZXR0aW5nc1tpZnJhbWVJZF0gPyBzZXR0aW5nc1tpZnJhbWVJZF0ubG9nIDogbG9nRW5hYmxlZDtcbiAgfVxuXG4gIGZ1bmN0aW9uIGxvZyhpZnJhbWVJZCwgbXNnKSB7XG4gICAgb3V0cHV0KCdsb2cnLCBpZnJhbWVJZCwgbXNnLCBpc0xvZ0VuYWJsZWQoaWZyYW1lSWQpKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGluZm8oaWZyYW1lSWQsIG1zZykge1xuICAgIG91dHB1dCgnaW5mbycsIGlmcmFtZUlkLCBtc2csIGlzTG9nRW5hYmxlZChpZnJhbWVJZCkpO1xuICB9XG5cbiAgZnVuY3Rpb24gd2FybihpZnJhbWVJZCwgbXNnKSB7XG4gICAgb3V0cHV0KCd3YXJuJywgaWZyYW1lSWQsIG1zZywgdHJ1ZSk7XG4gIH1cblxuICBmdW5jdGlvbiBvdXRwdXQodHlwZSwgaWZyYW1lSWQsIG1zZywgZW5hYmxlZCkge1xuICAgIGlmICh0cnVlID09PSBlbmFibGVkICYmICdvYmplY3QnID09PSB0eXBlb2Ygd2luZG93LmNvbnNvbGUpIHtcbiAgICAgIGNvbnNvbGVbdHlwZV0oZm9ybWF0TG9nSGVhZGVyKGlmcmFtZUlkKSwgbXNnKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBpRnJhbWVMaXN0ZW5lcihldmVudCkge1xuICAgIGZ1bmN0aW9uIHJlc2l6ZUlGcmFtZSgpIHtcbiAgICAgIGZ1bmN0aW9uIHJlc2l6ZSgpIHtcbiAgICAgICAgc2V0U2l6ZShtZXNzYWdlRGF0YSk7XG4gICAgICAgIHNldFBhZ2VQb3NpdGlvbihpZnJhbWVJZCk7XG4gICAgICAgIGNhbGxiYWNrKCdyZXNpemVkQ2FsbGJhY2snLCBtZXNzYWdlRGF0YSk7XG4gICAgICB9XG5cbiAgICAgIGVuc3VyZUluUmFuZ2UoJ0hlaWdodCcpO1xuICAgICAgZW5zdXJlSW5SYW5nZSgnV2lkdGgnKTtcblxuICAgICAgc3luY1Jlc2l6ZShyZXNpemUsIG1lc3NhZ2VEYXRhLCAnaW5pdCcpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHByb2Nlc3NNc2coKSB7XG4gICAgICB2YXIgZGF0YSA9IG1zZy5zdWJzdHIobXNnSWRMZW4pLnNwbGl0KCc6Jyk7XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIGlmcmFtZTogc2V0dGluZ3NbZGF0YVswXV0gJiYgc2V0dGluZ3NbZGF0YVswXV0uaWZyYW1lLFxuICAgICAgICBpZDogZGF0YVswXSxcbiAgICAgICAgaGVpZ2h0OiBkYXRhWzFdLFxuICAgICAgICB3aWR0aDogZGF0YVsyXSxcbiAgICAgICAgdHlwZTogZGF0YVszXVxuICAgICAgfTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBlbnN1cmVJblJhbmdlKERpbWVuc2lvbikge1xuICAgICAgdmFyIG1heCA9IE51bWJlcihzZXR0aW5nc1tpZnJhbWVJZF1bJ21heCcgKyBEaW1lbnNpb25dKSxcbiAgICAgICAgbWluID0gTnVtYmVyKHNldHRpbmdzW2lmcmFtZUlkXVsnbWluJyArIERpbWVuc2lvbl0pLFxuICAgICAgICBkaW1lbnNpb24gPSBEaW1lbnNpb24udG9Mb3dlckNhc2UoKSxcbiAgICAgICAgc2l6ZSA9IE51bWJlcihtZXNzYWdlRGF0YVtkaW1lbnNpb25dKTtcblxuICAgICAgbG9nKFxuICAgICAgICBpZnJhbWVJZCxcbiAgICAgICAgJ0NoZWNraW5nICcgKyBkaW1lbnNpb24gKyAnIGlzIGluIHJhbmdlICcgKyBtaW4gKyAnLScgKyBtYXhcbiAgICAgICk7XG5cbiAgICAgIGlmIChzaXplIDwgbWluKSB7XG4gICAgICAgIHNpemUgPSBtaW47XG4gICAgICAgIGxvZyhpZnJhbWVJZCwgJ1NldCAnICsgZGltZW5zaW9uICsgJyB0byBtaW4gdmFsdWUnKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHNpemUgPiBtYXgpIHtcbiAgICAgICAgc2l6ZSA9IG1heDtcbiAgICAgICAgbG9nKGlmcmFtZUlkLCAnU2V0ICcgKyBkaW1lbnNpb24gKyAnIHRvIG1heCB2YWx1ZScpO1xuICAgICAgfVxuXG4gICAgICBtZXNzYWdlRGF0YVtkaW1lbnNpb25dID0gJycgKyBzaXplO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGlzTWVzc2FnZUZyb21JRnJhbWUoKSB7XG4gICAgICBmdW5jdGlvbiBjaGVja0FsbG93ZWRPcmlnaW4oKSB7XG4gICAgICAgIGZ1bmN0aW9uIGNoZWNrTGlzdCgpIHtcbiAgICAgICAgICB2YXIgaSA9IDAsXG4gICAgICAgICAgICByZXRDb2RlID0gZmFsc2U7XG5cbiAgICAgICAgICBsb2coXG4gICAgICAgICAgICBpZnJhbWVJZCxcbiAgICAgICAgICAgICdDaGVja2luZyBjb25uZWN0aW9uIGlzIGZyb20gYWxsb3dlZCBsaXN0IG9mIG9yaWdpbnM6ICcgK1xuICAgICAgICAgICAgICBjaGVja09yaWdpblxuICAgICAgICAgICk7XG5cbiAgICAgICAgICBmb3IgKDsgaSA8IGNoZWNrT3JpZ2luLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoY2hlY2tPcmlnaW5baV0gPT09IG9yaWdpbikge1xuICAgICAgICAgICAgICByZXRDb2RlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiByZXRDb2RlO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gY2hlY2tTaW5nbGUoKSB7XG4gICAgICAgICAgdmFyIHJlbW90ZUhvc3QgPSBzZXR0aW5nc1tpZnJhbWVJZF0gJiYgc2V0dGluZ3NbaWZyYW1lSWRdLnJlbW90ZUhvc3Q7XG4gICAgICAgICAgbG9nKGlmcmFtZUlkLCAnQ2hlY2tpbmcgY29ubmVjdGlvbiBpcyBmcm9tOiAnICsgcmVtb3RlSG9zdCk7XG4gICAgICAgICAgcmV0dXJuIG9yaWdpbiA9PT0gcmVtb3RlSG9zdDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBjaGVja09yaWdpbi5jb25zdHJ1Y3RvciA9PT0gQXJyYXkgPyBjaGVja0xpc3QoKSA6IGNoZWNrU2luZ2xlKCk7XG4gICAgICB9XG5cbiAgICAgIHZhciBvcmlnaW4gPSBldmVudC5vcmlnaW4sXG4gICAgICAgIGNoZWNrT3JpZ2luID0gc2V0dGluZ3NbaWZyYW1lSWRdICYmIHNldHRpbmdzW2lmcmFtZUlkXS5jaGVja09yaWdpbjtcblxuICAgICAgaWYgKGNoZWNrT3JpZ2luICYmICcnICsgb3JpZ2luICE9PSAnbnVsbCcgJiYgIWNoZWNrQWxsb3dlZE9yaWdpbigpKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAgICAgICAnVW5leHBlY3RlZCBtZXNzYWdlIHJlY2VpdmVkIGZyb206ICcgK1xuICAgICAgICAgICAgb3JpZ2luICtcbiAgICAgICAgICAgICcgZm9yICcgK1xuICAgICAgICAgICAgbWVzc2FnZURhdGEuaWZyYW1lLmlkICtcbiAgICAgICAgICAgICcuIE1lc3NhZ2Ugd2FzOiAnICtcbiAgICAgICAgICAgIGV2ZW50LmRhdGEgK1xuICAgICAgICAgICAgJy4gVGhpcyBlcnJvciBjYW4gYmUgZGlzYWJsZWQgYnkgc2V0dGluZyB0aGUgY2hlY2tPcmlnaW46IGZhbHNlIG9wdGlvbiBvciBieSBwcm92aWRpbmcgb2YgYXJyYXkgb2YgdHJ1c3RlZCBkb21haW5zLidcbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaXNNZXNzYWdlRm9yVXMoKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICBtc2dJZCA9PT0gKCcnICsgbXNnKS5zdWJzdHIoMCwgbXNnSWRMZW4pICYmXG4gICAgICAgIG1zZy5zdWJzdHIobXNnSWRMZW4pLnNwbGl0KCc6JylbMF0gaW4gc2V0dGluZ3NcbiAgICAgICk7IC8vJycrUHJvdGVjdHMgYWdhaW5zdCBub24tc3RyaW5nIG1zZ1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGlzTWVzc2FnZUZyb21NZXRhUGFyZW50KCkge1xuICAgICAgLy9UZXN0IGlmIHRoaXMgbWVzc2FnZSBpcyBmcm9tIGEgcGFyZW50IGFib3ZlIHVzLiBUaGlzIGlzIGFuIHVnbHkgdGVzdCwgaG93ZXZlciwgdXBkYXRpbmdcbiAgICAgIC8vdGhlIG1lc3NhZ2UgZm9ybWF0IHdvdWxkIGJyZWFrIGJhY2t3YXJkcyBjb21wYXRpYml0eS5cbiAgICAgIHZhciByZXRDb2RlID0gbWVzc2FnZURhdGEudHlwZSBpbiB7IHRydWU6IDEsIGZhbHNlOiAxLCB1bmRlZmluZWQ6IDEgfTtcblxuICAgICAgaWYgKHJldENvZGUpIHtcbiAgICAgICAgbG9nKGlmcmFtZUlkLCAnSWdub3JpbmcgaW5pdCBtZXNzYWdlIGZyb20gbWV0YSBwYXJlbnQgcGFnZScpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcmV0Q29kZTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRNc2dCb2R5KG9mZnNldCkge1xuICAgICAgcmV0dXJuIG1zZy5zdWJzdHIobXNnLmluZGV4T2YoJzonKSArIG1zZ0hlYWRlckxlbiArIG9mZnNldCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZm9yd2FyZE1zZ0Zyb21JRnJhbWUobXNnQm9keSkge1xuICAgICAgbG9nKFxuICAgICAgICBpZnJhbWVJZCxcbiAgICAgICAgJ01lc3NhZ2VDYWxsYmFjayBwYXNzZWQ6IHtpZnJhbWU6ICcgK1xuICAgICAgICAgIG1lc3NhZ2VEYXRhLmlmcmFtZS5pZCArXG4gICAgICAgICAgJywgbWVzc2FnZTogJyArXG4gICAgICAgICAgbXNnQm9keSArXG4gICAgICAgICAgJ30nXG4gICAgICApO1xuICAgICAgY2FsbGJhY2soJ21lc3NhZ2VDYWxsYmFjaycsIHtcbiAgICAgICAgaWZyYW1lOiBtZXNzYWdlRGF0YS5pZnJhbWUsXG4gICAgICAgIG1lc3NhZ2U6IEpTT04ucGFyc2UobXNnQm9keSlcbiAgICAgIH0pO1xuICAgICAgbG9nKGlmcmFtZUlkLCAnLS0nKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRQYWdlSW5mbygpIHtcbiAgICAgIHZhciBib2R5UG9zaXRpb24gPSBkb2N1bWVudC5ib2R5LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLFxuICAgICAgICBpRnJhbWVQb3NpdGlvbiA9IG1lc3NhZ2VEYXRhLmlmcmFtZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcblxuICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgaWZyYW1lSGVpZ2h0OiBpRnJhbWVQb3NpdGlvbi5oZWlnaHQsXG4gICAgICAgIGlmcmFtZVdpZHRoOiBpRnJhbWVQb3NpdGlvbi53aWR0aCxcbiAgICAgICAgY2xpZW50SGVpZ2h0OiBNYXRoLm1heChcbiAgICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0LFxuICAgICAgICAgIHdpbmRvdy5pbm5lckhlaWdodCB8fCAwXG4gICAgICAgICksXG4gICAgICAgIGNsaWVudFdpZHRoOiBNYXRoLm1heChcbiAgICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50V2lkdGgsXG4gICAgICAgICAgd2luZG93LmlubmVyV2lkdGggfHwgMFxuICAgICAgICApLFxuICAgICAgICBvZmZzZXRUb3A6IHBhcnNlSW50KGlGcmFtZVBvc2l0aW9uLnRvcCAtIGJvZHlQb3NpdGlvbi50b3AsIDEwKSxcbiAgICAgICAgb2Zmc2V0TGVmdDogcGFyc2VJbnQoaUZyYW1lUG9zaXRpb24ubGVmdCAtIGJvZHlQb3NpdGlvbi5sZWZ0LCAxMCksXG4gICAgICAgIHNjcm9sbFRvcDogd2luZG93LnBhZ2VZT2Zmc2V0LFxuICAgICAgICBzY3JvbGxMZWZ0OiB3aW5kb3cucGFnZVhPZmZzZXRcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNlbmRQYWdlSW5mb1RvSWZyYW1lKGlmcmFtZSwgaWZyYW1lSWQpIHtcbiAgICAgIGZ1bmN0aW9uIGRlYm91bmNlZFRyaWdnZXIoKSB7XG4gICAgICAgIHRyaWdnZXIoXG4gICAgICAgICAgJ1NlbmQgUGFnZSBJbmZvJyxcbiAgICAgICAgICAncGFnZUluZm86JyArIGdldFBhZ2VJbmZvKCksXG4gICAgICAgICAgaWZyYW1lLFxuICAgICAgICAgIGlmcmFtZUlkXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgICBkZWJvdW5jZUZyYW1lRXZlbnRzKGRlYm91bmNlZFRyaWdnZXIsIDMyLCBpZnJhbWVJZCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc3RhcnRQYWdlSW5mb01vbml0b3IoKSB7XG4gICAgICBmdW5jdGlvbiBzZXRMaXN0ZW5lcih0eXBlLCBmdW5jKSB7XG4gICAgICAgIGZ1bmN0aW9uIHNlbmRQYWdlSW5mbygpIHtcbiAgICAgICAgICBpZiAoc2V0dGluZ3NbaWRdKSB7XG4gICAgICAgICAgICBzZW5kUGFnZUluZm9Ub0lmcmFtZShzZXR0aW5nc1tpZF0uaWZyYW1lLCBpZCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHN0b3AoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBbJ3Njcm9sbCcsICdyZXNpemUnXS5mb3JFYWNoKGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIGxvZyhpZCwgdHlwZSArIGV2dCArICcgbGlzdGVuZXIgZm9yIHNlbmRQYWdlSW5mbycpO1xuICAgICAgICAgIGZ1bmMod2luZG93LCBldnQsIHNlbmRQYWdlSW5mbyk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBzdG9wKCkge1xuICAgICAgICBzZXRMaXN0ZW5lcignUmVtb3ZlICcsIHJlbW92ZUV2ZW50TGlzdGVuZXIpO1xuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBzdGFydCgpIHtcbiAgICAgICAgc2V0TGlzdGVuZXIoJ0FkZCAnLCBhZGRFdmVudExpc3RlbmVyKTtcbiAgICAgIH1cblxuICAgICAgdmFyIGlkID0gaWZyYW1lSWQ7IC8vQ3JlYXRlIGxvY2FsbHkgc2NvcGVkIGNvcHkgb2YgaUZyYW1lIElEXG5cbiAgICAgIHN0YXJ0KCk7XG5cbiAgICAgIGlmIChzZXR0aW5nc1tpZF0pIHtcbiAgICAgICAgc2V0dGluZ3NbaWRdLnN0b3BQYWdlSW5mbyA9IHN0b3A7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc3RvcFBhZ2VJbmZvTW9uaXRvcigpIHtcbiAgICAgIGlmIChzZXR0aW5nc1tpZnJhbWVJZF0gJiYgc2V0dGluZ3NbaWZyYW1lSWRdLnN0b3BQYWdlSW5mbykge1xuICAgICAgICBzZXR0aW5nc1tpZnJhbWVJZF0uc3RvcFBhZ2VJbmZvKCk7XG4gICAgICAgIGRlbGV0ZSBzZXR0aW5nc1tpZnJhbWVJZF0uc3RvcFBhZ2VJbmZvO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNoZWNrSUZyYW1lRXhpc3RzKCkge1xuICAgICAgdmFyIHJldEJvb2wgPSB0cnVlO1xuXG4gICAgICBpZiAobnVsbCA9PT0gbWVzc2FnZURhdGEuaWZyYW1lKSB7XG4gICAgICAgIHdhcm4oaWZyYW1lSWQsICdJRnJhbWUgKCcgKyBtZXNzYWdlRGF0YS5pZCArICcpIG5vdCBmb3VuZCcpO1xuICAgICAgICByZXRCb29sID0gZmFsc2U7XG4gICAgICB9XG4gICAgICByZXR1cm4gcmV0Qm9vbDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRFbGVtZW50UG9zaXRpb24odGFyZ2V0KSB7XG4gICAgICB2YXIgaUZyYW1lUG9zaXRpb24gPSB0YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG5cbiAgICAgIGdldFBhZ2VQb3NpdGlvbihpZnJhbWVJZCk7XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIHg6IE1hdGguZmxvb3IoTnVtYmVyKGlGcmFtZVBvc2l0aW9uLmxlZnQpICsgTnVtYmVyKHBhZ2VQb3NpdGlvbi54KSksXG4gICAgICAgIHk6IE1hdGguZmxvb3IoTnVtYmVyKGlGcmFtZVBvc2l0aW9uLnRvcCkgKyBOdW1iZXIocGFnZVBvc2l0aW9uLnkpKVxuICAgICAgfTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzY3JvbGxSZXF1ZXN0RnJvbUNoaWxkKGFkZE9mZnNldCkge1xuICAgICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi8gLy9Ob3QgdGVzdGFibGUgaW4gS2FybWFcbiAgICAgIGZ1bmN0aW9uIHJlcG9zaXRpb24oKSB7XG4gICAgICAgIHBhZ2VQb3NpdGlvbiA9IG5ld1Bvc2l0aW9uO1xuICAgICAgICBzY3JvbGxUbygpO1xuICAgICAgICBsb2coaWZyYW1lSWQsICctLScpO1xuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBjYWxjT2Zmc2V0KCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIHg6IE51bWJlcihtZXNzYWdlRGF0YS53aWR0aCkgKyBvZmZzZXQueCxcbiAgICAgICAgICB5OiBOdW1iZXIobWVzc2FnZURhdGEuaGVpZ2h0KSArIG9mZnNldC55XG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIHNjcm9sbFBhcmVudCgpIHtcbiAgICAgICAgaWYgKHdpbmRvdy5wYXJlbnRJRnJhbWUpIHtcbiAgICAgICAgICB3aW5kb3cucGFyZW50SUZyYW1lWydzY3JvbGxUbycgKyAoYWRkT2Zmc2V0ID8gJ09mZnNldCcgOiAnJyldKFxuICAgICAgICAgICAgbmV3UG9zaXRpb24ueCxcbiAgICAgICAgICAgIG5ld1Bvc2l0aW9uLnlcbiAgICAgICAgICApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHdhcm4oXG4gICAgICAgICAgICBpZnJhbWVJZCxcbiAgICAgICAgICAgICdVbmFibGUgdG8gc2Nyb2xsIHRvIHJlcXVlc3RlZCBwb3NpdGlvbiwgd2luZG93LnBhcmVudElGcmFtZSBub3QgZm91bmQnXG4gICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB2YXIgb2Zmc2V0ID0gYWRkT2Zmc2V0XG4gICAgICAgICAgPyBnZXRFbGVtZW50UG9zaXRpb24obWVzc2FnZURhdGEuaWZyYW1lKVxuICAgICAgICAgIDogeyB4OiAwLCB5OiAwIH0sXG4gICAgICAgIG5ld1Bvc2l0aW9uID0gY2FsY09mZnNldCgpO1xuXG4gICAgICBsb2coXG4gICAgICAgIGlmcmFtZUlkLFxuICAgICAgICAnUmVwb3NpdGlvbiByZXF1ZXN0ZWQgZnJvbSBpRnJhbWUgKG9mZnNldCB4OicgK1xuICAgICAgICAgIG9mZnNldC54ICtcbiAgICAgICAgICAnIHk6JyArXG4gICAgICAgICAgb2Zmc2V0LnkgK1xuICAgICAgICAgICcpJ1xuICAgICAgKTtcblxuICAgICAgaWYgKHdpbmRvdy50b3AgIT09IHdpbmRvdy5zZWxmKSB7XG4gICAgICAgIHNjcm9sbFBhcmVudCgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmVwb3NpdGlvbigpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHNjcm9sbFRvKCkge1xuICAgICAgaWYgKGZhbHNlICE9PSBjYWxsYmFjaygnc2Nyb2xsQ2FsbGJhY2snLCBwYWdlUG9zaXRpb24pKSB7XG4gICAgICAgIHNldFBhZ2VQb3NpdGlvbihpZnJhbWVJZCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB1bnNldFBhZ2VQb3NpdGlvbigpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGZpbmRUYXJnZXQobG9jYXRpb24pIHtcbiAgICAgIGZ1bmN0aW9uIGp1bXBUb1RhcmdldCgpIHtcbiAgICAgICAgdmFyIGp1bXBQb3NpdGlvbiA9IGdldEVsZW1lbnRQb3NpdGlvbih0YXJnZXQpO1xuXG4gICAgICAgIGxvZyhcbiAgICAgICAgICBpZnJhbWVJZCxcbiAgICAgICAgICAnTW92aW5nIHRvIGluIHBhZ2UgbGluayAoIycgK1xuICAgICAgICAgICAgaGFzaCArXG4gICAgICAgICAgICAnKSBhdCB4OiAnICtcbiAgICAgICAgICAgIGp1bXBQb3NpdGlvbi54ICtcbiAgICAgICAgICAgICcgeTogJyArXG4gICAgICAgICAgICBqdW1wUG9zaXRpb24ueVxuICAgICAgICApO1xuICAgICAgICBwYWdlUG9zaXRpb24gPSB7XG4gICAgICAgICAgeDoganVtcFBvc2l0aW9uLngsXG4gICAgICAgICAgeToganVtcFBvc2l0aW9uLnlcbiAgICAgICAgfTtcblxuICAgICAgICBzY3JvbGxUbygpO1xuICAgICAgICBsb2coaWZyYW1lSWQsICctLScpO1xuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBqdW1wVG9QYXJlbnQoKSB7XG4gICAgICAgIGlmICh3aW5kb3cucGFyZW50SUZyYW1lKSB7XG4gICAgICAgICAgd2luZG93LnBhcmVudElGcmFtZS5tb3ZlVG9BbmNob3IoaGFzaCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbG9nKFxuICAgICAgICAgICAgaWZyYW1lSWQsXG4gICAgICAgICAgICAnSW4gcGFnZSBsaW5rICMnICtcbiAgICAgICAgICAgICAgaGFzaCArXG4gICAgICAgICAgICAgICcgbm90IGZvdW5kIGFuZCB3aW5kb3cucGFyZW50SUZyYW1lIG5vdCBmb3VuZCdcbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHZhciBoYXNoID0gbG9jYXRpb24uc3BsaXQoJyMnKVsxXSB8fCAnJyxcbiAgICAgICAgaGFzaERhdGEgPSBkZWNvZGVVUklDb21wb25lbnQoaGFzaCksXG4gICAgICAgIHRhcmdldCA9XG4gICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaGFzaERhdGEpIHx8XG4gICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeU5hbWUoaGFzaERhdGEpWzBdO1xuXG4gICAgICBpZiAodGFyZ2V0KSB7XG4gICAgICAgIGp1bXBUb1RhcmdldCgpO1xuICAgICAgfSBlbHNlIGlmICh3aW5kb3cudG9wICE9PSB3aW5kb3cuc2VsZikge1xuICAgICAgICBqdW1wVG9QYXJlbnQoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGxvZyhpZnJhbWVJZCwgJ0luIHBhZ2UgbGluayAjJyArIGhhc2ggKyAnIG5vdCBmb3VuZCcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNhbGxiYWNrKGZ1bmNOYW1lLCB2YWwpIHtcbiAgICAgIHJldHVybiBjaGtDYWxsYmFjayhpZnJhbWVJZCwgZnVuY05hbWUsIHZhbCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gYWN0aW9uTXNnKCkge1xuICAgICAgaWYgKHNldHRpbmdzW2lmcmFtZUlkXSAmJiBzZXR0aW5nc1tpZnJhbWVJZF0uZmlyc3RSdW4pIGZpcnN0UnVuKCk7XG5cbiAgICAgIHN3aXRjaCAobWVzc2FnZURhdGEudHlwZSkge1xuICAgICAgICBjYXNlICdjbG9zZSc6XG4gICAgICAgICAgaWYgKHNldHRpbmdzW2lmcmFtZUlkXS5jbG9zZVJlcXVlc3RDYWxsYmFjaylcbiAgICAgICAgICAgIGNoa0NhbGxiYWNrKFxuICAgICAgICAgICAgICBpZnJhbWVJZCxcbiAgICAgICAgICAgICAgJ2Nsb3NlUmVxdWVzdENhbGxiYWNrJyxcbiAgICAgICAgICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmlmcmFtZVxuICAgICAgICAgICAgKTtcbiAgICAgICAgICBlbHNlIGNsb3NlSUZyYW1lKG1lc3NhZ2VEYXRhLmlmcmFtZSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ21lc3NhZ2UnOlxuICAgICAgICAgIGZvcndhcmRNc2dGcm9tSUZyYW1lKGdldE1zZ0JvZHkoNikpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdzY3JvbGxUbyc6XG4gICAgICAgICAgc2Nyb2xsUmVxdWVzdEZyb21DaGlsZChmYWxzZSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ3Njcm9sbFRvT2Zmc2V0JzpcbiAgICAgICAgICBzY3JvbGxSZXF1ZXN0RnJvbUNoaWxkKHRydWUpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdwYWdlSW5mbyc6XG4gICAgICAgICAgc2VuZFBhZ2VJbmZvVG9JZnJhbWUoXG4gICAgICAgICAgICBzZXR0aW5nc1tpZnJhbWVJZF0gJiYgc2V0dGluZ3NbaWZyYW1lSWRdLmlmcmFtZSxcbiAgICAgICAgICAgIGlmcmFtZUlkXG4gICAgICAgICAgKTtcbiAgICAgICAgICBzdGFydFBhZ2VJbmZvTW9uaXRvcigpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdwYWdlSW5mb1N0b3AnOlxuICAgICAgICAgIHN0b3BQYWdlSW5mb01vbml0b3IoKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnaW5QYWdlTGluayc6XG4gICAgICAgICAgZmluZFRhcmdldChnZXRNc2dCb2R5KDkpKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAncmVzZXQnOlxuICAgICAgICAgIHJlc2V0SUZyYW1lKG1lc3NhZ2VEYXRhKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnaW5pdCc6XG4gICAgICAgICAgcmVzaXplSUZyYW1lKCk7XG4gICAgICAgICAgY2FsbGJhY2soJ2luaXRDYWxsYmFjaycsIG1lc3NhZ2VEYXRhLmlmcmFtZSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgcmVzaXplSUZyYW1lKCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaGFzU2V0dGluZ3MoaWZyYW1lSWQpIHtcbiAgICAgIHZhciByZXRCb29sID0gdHJ1ZTtcblxuICAgICAgaWYgKCFzZXR0aW5nc1tpZnJhbWVJZF0pIHtcbiAgICAgICAgcmV0Qm9vbCA9IGZhbHNlO1xuICAgICAgICB3YXJuKFxuICAgICAgICAgIG1lc3NhZ2VEYXRhLnR5cGUgK1xuICAgICAgICAgICAgJyBObyBzZXR0aW5ncyBmb3IgJyArXG4gICAgICAgICAgICBpZnJhbWVJZCArXG4gICAgICAgICAgICAnLiBNZXNzYWdlIHdhczogJyArXG4gICAgICAgICAgICBtc2dcbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHJldEJvb2w7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaUZyYW1lUmVhZHlNc2dSZWNlaXZlZCgpIHtcbiAgICAgIGZvciAodmFyIGlmcmFtZUlkIGluIHNldHRpbmdzKSB7XG4gICAgICAgIHRyaWdnZXIoXG4gICAgICAgICAgJ2lGcmFtZSByZXF1ZXN0ZWQgaW5pdCcsXG4gICAgICAgICAgY3JlYXRlT3V0Z29pbmdNc2coaWZyYW1lSWQpLFxuICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlmcmFtZUlkKSxcbiAgICAgICAgICBpZnJhbWVJZFxuICAgICAgICApO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGZpcnN0UnVuKCkge1xuICAgICAgaWYgKHNldHRpbmdzW2lmcmFtZUlkXSkge1xuICAgICAgICBzZXR0aW5nc1tpZnJhbWVJZF0uZmlyc3RSdW4gPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjbGVhcldhcm5pbmdUaW1lb3V0KCkge1xuICAgICAgaWYgKHNldHRpbmdzW2lmcmFtZUlkXSkge1xuICAgICAgICBjbGVhclRpbWVvdXQoc2V0dGluZ3NbaWZyYW1lSWRdLm1zZ1RpbWVvdXQpO1xuICAgICAgICBzZXR0aW5nc1tpZnJhbWVJZF0ud2FybmluZ1RpbWVvdXQgPSAwO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciBtc2cgPSBldmVudC5kYXRhLFxuICAgICAgbWVzc2FnZURhdGEgPSB7fSxcbiAgICAgIGlmcmFtZUlkID0gbnVsbDtcblxuICAgIGlmICgnW2lGcmFtZVJlc2l6ZXJDaGlsZF1SZWFkeScgPT09IG1zZykge1xuICAgICAgaUZyYW1lUmVhZHlNc2dSZWNlaXZlZCgpO1xuICAgIH0gZWxzZSBpZiAoaXNNZXNzYWdlRm9yVXMoKSkge1xuICAgICAgbWVzc2FnZURhdGEgPSBwcm9jZXNzTXNnKCk7XG4gICAgICBpZnJhbWVJZCA9IGxvZ0lkID0gbWVzc2FnZURhdGEuaWQ7XG4gICAgICBpZiAoc2V0dGluZ3NbaWZyYW1lSWRdKSB7XG4gICAgICAgIHNldHRpbmdzW2lmcmFtZUlkXS5sb2FkZWQgPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICBpZiAoIWlzTWVzc2FnZUZyb21NZXRhUGFyZW50KCkgJiYgaGFzU2V0dGluZ3MoaWZyYW1lSWQpKSB7XG4gICAgICAgIGxvZyhpZnJhbWVJZCwgJ1JlY2VpdmVkOiAnICsgbXNnKTtcblxuICAgICAgICBpZiAoY2hlY2tJRnJhbWVFeGlzdHMoKSAmJiBpc01lc3NhZ2VGcm9tSUZyYW1lKCkpIHtcbiAgICAgICAgICBhY3Rpb25Nc2coKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBpbmZvKGlmcmFtZUlkLCAnSWdub3JlZDogJyArIG1zZyk7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gY2hrQ2FsbGJhY2soaWZyYW1lSWQsIGZ1bmNOYW1lLCB2YWwpIHtcbiAgICB2YXIgZnVuYyA9IG51bGwsXG4gICAgICByZXRWYWwgPSBudWxsO1xuXG4gICAgaWYgKHNldHRpbmdzW2lmcmFtZUlkXSkge1xuICAgICAgZnVuYyA9IHNldHRpbmdzW2lmcmFtZUlkXVtmdW5jTmFtZV07XG5cbiAgICAgIGlmICgnZnVuY3Rpb24nID09PSB0eXBlb2YgZnVuYykge1xuICAgICAgICByZXRWYWwgPSBmdW5jKHZhbCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFxuICAgICAgICAgIGZ1bmNOYW1lICsgJyBvbiBpRnJhbWVbJyArIGlmcmFtZUlkICsgJ10gaXMgbm90IGEgZnVuY3Rpb24nXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHJldFZhbDtcbiAgfVxuXG4gIGZ1bmN0aW9uIHJlbW92ZUlmcmFtZUxpc3RlbmVycyhpZnJhbWUpIHtcbiAgICB2YXIgaWZyYW1lSWQgPSBpZnJhbWUuaWQ7XG4gICAgZGVsZXRlIHNldHRpbmdzW2lmcmFtZUlkXTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGNsb3NlSUZyYW1lKGlmcmFtZSkge1xuICAgIHZhciBpZnJhbWVJZCA9IGlmcmFtZS5pZDtcbiAgICBsb2coaWZyYW1lSWQsICdSZW1vdmluZyBpRnJhbWU6ICcgKyBpZnJhbWVJZCk7XG5cbiAgICB0cnkge1xuICAgICAgLy8gQ2F0Y2ggcmFjZSBjb25kaXRpb24gZXJyb3Igd2l0aCBSZWFjdFxuICAgICAgaWYgKGlmcmFtZS5wYXJlbnROb2RlKSB7XG4gICAgICAgIGlmcmFtZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGlmcmFtZSk7XG4gICAgICB9XG4gICAgfSBjYXRjaCAoZSkge31cblxuICAgIGNoa0NhbGxiYWNrKGlmcmFtZUlkLCAnY2xvc2VkQ2FsbGJhY2snLCBpZnJhbWVJZCk7XG4gICAgbG9nKGlmcmFtZUlkLCAnLS0nKTtcbiAgICByZW1vdmVJZnJhbWVMaXN0ZW5lcnMoaWZyYW1lKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGdldFBhZ2VQb3NpdGlvbihpZnJhbWVJZCkge1xuICAgIGlmIChudWxsID09PSBwYWdlUG9zaXRpb24pIHtcbiAgICAgIHBhZ2VQb3NpdGlvbiA9IHtcbiAgICAgICAgeDpcbiAgICAgICAgICB3aW5kb3cucGFnZVhPZmZzZXQgIT09IHVuZGVmaW5lZFxuICAgICAgICAgICAgPyB3aW5kb3cucGFnZVhPZmZzZXRcbiAgICAgICAgICAgIDogZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbExlZnQsXG4gICAgICAgIHk6XG4gICAgICAgICAgd2luZG93LnBhZ2VZT2Zmc2V0ICE9PSB1bmRlZmluZWRcbiAgICAgICAgICAgID8gd2luZG93LnBhZ2VZT2Zmc2V0XG4gICAgICAgICAgICA6IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3BcbiAgICAgIH07XG4gICAgICBsb2coXG4gICAgICAgIGlmcmFtZUlkLFxuICAgICAgICAnR2V0IHBhZ2UgcG9zaXRpb246ICcgKyBwYWdlUG9zaXRpb24ueCArICcsJyArIHBhZ2VQb3NpdGlvbi55XG4gICAgICApO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHNldFBhZ2VQb3NpdGlvbihpZnJhbWVJZCkge1xuICAgIGlmIChudWxsICE9PSBwYWdlUG9zaXRpb24pIHtcbiAgICAgIHdpbmRvdy5zY3JvbGxUbyhwYWdlUG9zaXRpb24ueCwgcGFnZVBvc2l0aW9uLnkpO1xuICAgICAgbG9nKFxuICAgICAgICBpZnJhbWVJZCxcbiAgICAgICAgJ1NldCBwYWdlIHBvc2l0aW9uOiAnICsgcGFnZVBvc2l0aW9uLnggKyAnLCcgKyBwYWdlUG9zaXRpb24ueVxuICAgICAgKTtcbiAgICAgIHVuc2V0UGFnZVBvc2l0aW9uKCk7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gdW5zZXRQYWdlUG9zaXRpb24oKSB7XG4gICAgcGFnZVBvc2l0aW9uID0gbnVsbDtcbiAgfVxuXG4gIGZ1bmN0aW9uIHJlc2V0SUZyYW1lKG1lc3NhZ2VEYXRhKSB7XG4gICAgZnVuY3Rpb24gcmVzZXQoKSB7XG4gICAgICBzZXRTaXplKG1lc3NhZ2VEYXRhKTtcbiAgICAgIHRyaWdnZXIoJ3Jlc2V0JywgJ3Jlc2V0JywgbWVzc2FnZURhdGEuaWZyYW1lLCBtZXNzYWdlRGF0YS5pZCk7XG4gICAgfVxuXG4gICAgbG9nKFxuICAgICAgbWVzc2FnZURhdGEuaWQsXG4gICAgICAnU2l6ZSByZXNldCByZXF1ZXN0ZWQgYnkgJyArXG4gICAgICAgICgnaW5pdCcgPT09IG1lc3NhZ2VEYXRhLnR5cGUgPyAnaG9zdCBwYWdlJyA6ICdpRnJhbWUnKVxuICAgICk7XG4gICAgZ2V0UGFnZVBvc2l0aW9uKG1lc3NhZ2VEYXRhLmlkKTtcbiAgICBzeW5jUmVzaXplKHJlc2V0LCBtZXNzYWdlRGF0YSwgJ3Jlc2V0Jyk7XG4gIH1cblxuICBmdW5jdGlvbiBzZXRTaXplKG1lc3NhZ2VEYXRhKSB7XG4gICAgZnVuY3Rpb24gc2V0RGltZW5zaW9uKGRpbWVuc2lvbikge1xuICAgICAgaWYgKCFtZXNzYWdlRGF0YS5pZCkge1xuICAgICAgICBsb2coJ3VuZGVmaW5lZCcsICdtZXNzYWdlRGF0YSBpZCBub3Qgc2V0Jyk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIG1lc3NhZ2VEYXRhLmlmcmFtZS5zdHlsZVtkaW1lbnNpb25dID0gbWVzc2FnZURhdGFbZGltZW5zaW9uXSArICdweCc7XG4gICAgICBsb2coXG4gICAgICAgIG1lc3NhZ2VEYXRhLmlkLFxuICAgICAgICAnSUZyYW1lICgnICtcbiAgICAgICAgICBpZnJhbWVJZCArXG4gICAgICAgICAgJykgJyArXG4gICAgICAgICAgZGltZW5zaW9uICtcbiAgICAgICAgICAnIHNldCB0byAnICtcbiAgICAgICAgICBtZXNzYWdlRGF0YVtkaW1lbnNpb25dICtcbiAgICAgICAgICAncHgnXG4gICAgICApO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNoa1plcm8oZGltZW5zaW9uKSB7XG4gICAgICAvL0ZpcmVGb3ggc2V0cyBkaW1lbnNpb24gb2YgaGlkZGVuIGlGcmFtZXMgdG8gemVyby5cbiAgICAgIC8vU28gaWYgd2UgZGV0ZWN0IHRoYXQgc2V0IHVwIGFuIGV2ZW50IHRvIGNoZWNrIGZvclxuICAgICAgLy93aGVuIGlGcmFtZSBiZWNvbWVzIHZpc2libGUuXG5cbiAgICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovIC8vTm90IHRlc3RhYmxlIGluIFBoYW50b21KU1xuICAgICAgaWYgKCFoaWRkZW5DaGVja0VuYWJsZWQgJiYgJzAnID09PSBtZXNzYWdlRGF0YVtkaW1lbnNpb25dKSB7XG4gICAgICAgIGhpZGRlbkNoZWNrRW5hYmxlZCA9IHRydWU7XG4gICAgICAgIGxvZyhpZnJhbWVJZCwgJ0hpZGRlbiBpRnJhbWUgZGV0ZWN0ZWQsIGNyZWF0aW5nIHZpc2liaWxpdHkgbGlzdGVuZXInKTtcbiAgICAgICAgZml4SGlkZGVuSUZyYW1lcygpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHByb2Nlc3NEaW1lbnNpb24oZGltZW5zaW9uKSB7XG4gICAgICBzZXREaW1lbnNpb24oZGltZW5zaW9uKTtcbiAgICAgIGNoa1plcm8oZGltZW5zaW9uKTtcbiAgICB9XG5cbiAgICB2YXIgaWZyYW1lSWQgPSBtZXNzYWdlRGF0YS5pZnJhbWUuaWQ7XG5cbiAgICBpZiAoc2V0dGluZ3NbaWZyYW1lSWRdKSB7XG4gICAgICBpZiAoc2V0dGluZ3NbaWZyYW1lSWRdLnNpemVIZWlnaHQpIHtcbiAgICAgICAgcHJvY2Vzc0RpbWVuc2lvbignaGVpZ2h0Jyk7XG4gICAgICB9XG4gICAgICBpZiAoc2V0dGluZ3NbaWZyYW1lSWRdLnNpemVXaWR0aCkge1xuICAgICAgICBwcm9jZXNzRGltZW5zaW9uKCd3aWR0aCcpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHN5bmNSZXNpemUoZnVuYywgbWVzc2FnZURhdGEsIGRvTm90U3luYykge1xuICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBpZiAqLyAvL05vdCB0ZXN0YWJsZSBpbiBQaGFudG9tSlNcbiAgICBpZiAoZG9Ob3RTeW5jICE9PSBtZXNzYWdlRGF0YS50eXBlICYmIHJlcXVlc3RBbmltYXRpb25GcmFtZSkge1xuICAgICAgbG9nKG1lc3NhZ2VEYXRhLmlkLCAnUmVxdWVzdGluZyBhbmltYXRpb24gZnJhbWUnKTtcbiAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jKTtcbiAgICB9IGVsc2Uge1xuICAgICAgZnVuYygpO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHRyaWdnZXIoY2FsbGVlTXNnLCBtc2csIGlmcmFtZSwgaWQsIG5vUmVzcG9uc2VXYXJuaW5nKSB7XG4gICAgZnVuY3Rpb24gcG9zdE1lc3NhZ2VUb0lGcmFtZSgpIHtcbiAgICAgIHZhciB0YXJnZXQgPSBzZXR0aW5nc1tpZF0gJiYgc2V0dGluZ3NbaWRdLnRhcmdldE9yaWdpbjtcbiAgICAgIGxvZyhcbiAgICAgICAgaWQsXG4gICAgICAgICdbJyArXG4gICAgICAgICAgY2FsbGVlTXNnICtcbiAgICAgICAgICAnXSBTZW5kaW5nIG1zZyB0byBpZnJhbWVbJyArXG4gICAgICAgICAgaWQgK1xuICAgICAgICAgICddICgnICtcbiAgICAgICAgICBtc2cgK1xuICAgICAgICAgICcpIHRhcmdldE9yaWdpbjogJyArXG4gICAgICAgICAgdGFyZ2V0XG4gICAgICApO1xuICAgICAgaWZyYW1lLmNvbnRlbnRXaW5kb3cucG9zdE1lc3NhZ2UobXNnSWQgKyBtc2csIHRhcmdldCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaUZyYW1lTm90Rm91bmQoKSB7XG4gICAgICB3YXJuKGlkLCAnWycgKyBjYWxsZWVNc2cgKyAnXSBJRnJhbWUoJyArIGlkICsgJykgbm90IGZvdW5kJyk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2hrQW5kU2VuZCgpIHtcbiAgICAgIGlmIChcbiAgICAgICAgaWZyYW1lICYmXG4gICAgICAgICdjb250ZW50V2luZG93JyBpbiBpZnJhbWUgJiZcbiAgICAgICAgbnVsbCAhPT0gaWZyYW1lLmNvbnRlbnRXaW5kb3dcbiAgICAgICkge1xuICAgICAgICAvL051bGwgdGVzdCBmb3IgUGhhbnRvbUpTXG4gICAgICAgIHBvc3RNZXNzYWdlVG9JRnJhbWUoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlGcmFtZU5vdEZvdW5kKCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gd2Fybk9uTm9SZXNwb25zZSgpIHtcbiAgICAgIGZ1bmN0aW9uIHdhcm5pbmcoKSB7XG4gICAgICAgIGlmIChzZXR0aW5nc1tpZF0gJiYgIXNldHRpbmdzW2lkXS5sb2FkZWQgJiYgIWVycm9yU2hvd24pIHtcbiAgICAgICAgICBlcnJvclNob3duID0gdHJ1ZTtcbiAgICAgICAgICB3YXJuKFxuICAgICAgICAgICAgaWQsXG4gICAgICAgICAgICAnSUZyYW1lIGhhcyBub3QgcmVzcG9uZGVkIHdpdGhpbiAnICtcbiAgICAgICAgICAgICAgc2V0dGluZ3NbaWRdLndhcm5pbmdUaW1lb3V0IC8gMTAwMCArXG4gICAgICAgICAgICAgICcgc2Vjb25kcy4gQ2hlY2sgaUZyYW1lUmVzaXplci5jb250ZW50V2luZG93LmpzIGhhcyBiZWVuIGxvYWRlZCBpbiBpRnJhbWUuIFRoaXMgbWVzc2FnZSBjYW4gYmUgaWdub3JlZCBpZiBldmVyeXRoaW5nIGlzIHdvcmtpbmcsIG9yIHlvdSBjYW4gc2V0IHRoZSB3YXJuaW5nVGltZW91dCBvcHRpb24gdG8gYSBoaWdoZXIgdmFsdWUgb3IgemVybyB0byBzdXBwcmVzcyB0aGlzIHdhcm5pbmcuJ1xuICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKFxuICAgICAgICAhIW5vUmVzcG9uc2VXYXJuaW5nICYmXG4gICAgICAgIHNldHRpbmdzW2lkXSAmJlxuICAgICAgICAhIXNldHRpbmdzW2lkXS53YXJuaW5nVGltZW91dFxuICAgICAgKSB7XG4gICAgICAgIHNldHRpbmdzW2lkXS5tc2dUaW1lb3V0ID0gc2V0VGltZW91dChcbiAgICAgICAgICB3YXJuaW5nLFxuICAgICAgICAgIHNldHRpbmdzW2lkXS53YXJuaW5nVGltZW91dFxuICAgICAgICApO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciBlcnJvclNob3duID0gZmFsc2U7XG5cbiAgICBpZCA9IGlkIHx8IGlmcmFtZS5pZDtcblxuICAgIGlmIChzZXR0aW5nc1tpZF0pIHtcbiAgICAgIGNoa0FuZFNlbmQoKTtcbiAgICAgIHdhcm5Pbk5vUmVzcG9uc2UoKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBjcmVhdGVPdXRnb2luZ01zZyhpZnJhbWVJZCkge1xuICAgIHJldHVybiAoXG4gICAgICBpZnJhbWVJZCArXG4gICAgICAnOicgK1xuICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmJvZHlNYXJnaW5WMSArXG4gICAgICAnOicgK1xuICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLnNpemVXaWR0aCArXG4gICAgICAnOicgK1xuICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmxvZyArXG4gICAgICAnOicgK1xuICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmludGVydmFsICtcbiAgICAgICc6JyArXG4gICAgICBzZXR0aW5nc1tpZnJhbWVJZF0uZW5hYmxlUHVibGljTWV0aG9kcyArXG4gICAgICAnOicgK1xuICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmF1dG9SZXNpemUgK1xuICAgICAgJzonICtcbiAgICAgIHNldHRpbmdzW2lmcmFtZUlkXS5ib2R5TWFyZ2luICtcbiAgICAgICc6JyArXG4gICAgICBzZXR0aW5nc1tpZnJhbWVJZF0uaGVpZ2h0Q2FsY3VsYXRpb25NZXRob2QgK1xuICAgICAgJzonICtcbiAgICAgIHNldHRpbmdzW2lmcmFtZUlkXS5ib2R5QmFja2dyb3VuZCArXG4gICAgICAnOicgK1xuICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmJvZHlQYWRkaW5nICtcbiAgICAgICc6JyArXG4gICAgICBzZXR0aW5nc1tpZnJhbWVJZF0udG9sZXJhbmNlICtcbiAgICAgICc6JyArXG4gICAgICBzZXR0aW5nc1tpZnJhbWVJZF0uaW5QYWdlTGlua3MgK1xuICAgICAgJzonICtcbiAgICAgIHNldHRpbmdzW2lmcmFtZUlkXS5yZXNpemVGcm9tICtcbiAgICAgICc6JyArXG4gICAgICBzZXR0aW5nc1tpZnJhbWVJZF0ud2lkdGhDYWxjdWxhdGlvbk1ldGhvZFxuICAgICk7XG4gIH1cblxuICBmdW5jdGlvbiBzZXR1cElGcmFtZShpZnJhbWUsIG9wdGlvbnMpIHtcbiAgICBmdW5jdGlvbiBzZXRMaW1pdHMoKSB7XG4gICAgICBmdW5jdGlvbiBhZGRTdHlsZShzdHlsZSkge1xuICAgICAgICBpZiAoXG4gICAgICAgICAgSW5maW5pdHkgIT09IHNldHRpbmdzW2lmcmFtZUlkXVtzdHlsZV0gJiZcbiAgICAgICAgICAwICE9PSBzZXR0aW5nc1tpZnJhbWVJZF1bc3R5bGVdXG4gICAgICAgICkge1xuICAgICAgICAgIGlmcmFtZS5zdHlsZVtzdHlsZV0gPSBzZXR0aW5nc1tpZnJhbWVJZF1bc3R5bGVdICsgJ3B4JztcbiAgICAgICAgICBsb2coXG4gICAgICAgICAgICBpZnJhbWVJZCxcbiAgICAgICAgICAgICdTZXQgJyArIHN0eWxlICsgJyA9ICcgKyBzZXR0aW5nc1tpZnJhbWVJZF1bc3R5bGVdICsgJ3B4J1xuICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgZnVuY3Rpb24gY2hrTWluTWF4KGRpbWVuc2lvbikge1xuICAgICAgICBpZiAoXG4gICAgICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdWydtaW4nICsgZGltZW5zaW9uXSA+XG4gICAgICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdWydtYXgnICsgZGltZW5zaW9uXVxuICAgICAgICApIHtcbiAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAgICAgICAnVmFsdWUgZm9yIG1pbicgK1xuICAgICAgICAgICAgICBkaW1lbnNpb24gK1xuICAgICAgICAgICAgICAnIGNhbiBub3QgYmUgZ3JlYXRlciB0aGFuIG1heCcgK1xuICAgICAgICAgICAgICBkaW1lbnNpb25cbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGNoa01pbk1heCgnSGVpZ2h0Jyk7XG4gICAgICBjaGtNaW5NYXgoJ1dpZHRoJyk7XG5cbiAgICAgIGFkZFN0eWxlKCdtYXhIZWlnaHQnKTtcbiAgICAgIGFkZFN0eWxlKCdtaW5IZWlnaHQnKTtcbiAgICAgIGFkZFN0eWxlKCdtYXhXaWR0aCcpO1xuICAgICAgYWRkU3R5bGUoJ21pbldpZHRoJyk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gbmV3SWQoKSB7XG4gICAgICB2YXIgaWQgPSAob3B0aW9ucyAmJiBvcHRpb25zLmlkKSB8fCBkZWZhdWx0cy5pZCArIGNvdW50Kys7XG4gICAgICBpZiAobnVsbCAhPT0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaWQpKSB7XG4gICAgICAgIGlkID0gaWQgKyBjb3VudCsrO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGlkO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGVuc3VyZUhhc0lkKGlmcmFtZUlkKSB7XG4gICAgICBsb2dJZCA9IGlmcmFtZUlkO1xuICAgICAgaWYgKCcnID09PSBpZnJhbWVJZCkge1xuICAgICAgICBpZnJhbWUuaWQgPSBpZnJhbWVJZCA9IG5ld0lkKCk7XG4gICAgICAgIGxvZ0VuYWJsZWQgPSAob3B0aW9ucyB8fCB7fSkubG9nO1xuICAgICAgICBsb2dJZCA9IGlmcmFtZUlkO1xuICAgICAgICBsb2coXG4gICAgICAgICAgaWZyYW1lSWQsXG4gICAgICAgICAgJ0FkZGVkIG1pc3NpbmcgaWZyYW1lIElEOiAnICsgaWZyYW1lSWQgKyAnICgnICsgaWZyYW1lLnNyYyArICcpJ1xuICAgICAgICApO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gaWZyYW1lSWQ7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2V0U2Nyb2xsaW5nKCkge1xuICAgICAgbG9nKFxuICAgICAgICBpZnJhbWVJZCxcbiAgICAgICAgJ0lGcmFtZSBzY3JvbGxpbmcgJyArXG4gICAgICAgICAgKHNldHRpbmdzW2lmcmFtZUlkXSAmJiBzZXR0aW5nc1tpZnJhbWVJZF0uc2Nyb2xsaW5nXG4gICAgICAgICAgICA/ICdlbmFibGVkJ1xuICAgICAgICAgICAgOiAnZGlzYWJsZWQnKSArXG4gICAgICAgICAgJyBmb3IgJyArXG4gICAgICAgICAgaWZyYW1lSWRcbiAgICAgICk7XG4gICAgICBpZnJhbWUuc3R5bGUub3ZlcmZsb3cgPVxuICAgICAgICBmYWxzZSA9PT0gKHNldHRpbmdzW2lmcmFtZUlkXSAmJiBzZXR0aW5nc1tpZnJhbWVJZF0uc2Nyb2xsaW5nKVxuICAgICAgICAgID8gJ2hpZGRlbidcbiAgICAgICAgICA6ICdhdXRvJztcbiAgICAgIHN3aXRjaCAoc2V0dGluZ3NbaWZyYW1lSWRdICYmIHNldHRpbmdzW2lmcmFtZUlkXS5zY3JvbGxpbmcpIHtcbiAgICAgICAgY2FzZSAnb21pdCc6XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgdHJ1ZTpcbiAgICAgICAgICBpZnJhbWUuc2Nyb2xsaW5nID0gJ3llcyc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgZmFsc2U6XG4gICAgICAgICAgaWZyYW1lLnNjcm9sbGluZyA9ICdubyc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgaWZyYW1lLnNjcm9sbGluZyA9IHNldHRpbmdzW2lmcmFtZUlkXVxuICAgICAgICAgICAgPyBzZXR0aW5nc1tpZnJhbWVJZF0uc2Nyb2xsaW5nXG4gICAgICAgICAgICA6ICdubyc7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy9UaGUgVjEgaUZyYW1lIHNjcmlwdCBleHBlY3RzIGFuIGludCwgd2hlcmUgYXMgaW4gVjIgZXhwZWN0cyBhIENTU1xuICAgIC8vc3RyaW5nIHZhbHVlIHN1Y2ggYXMgJzFweCAzZW0nLCBzbyBpZiB3ZSBoYXZlIGFuIGludCBmb3IgVjIsIHNldCBWMT1WMlxuICAgIC8vYW5kIHRoZW4gY29udmVydCBWMiB0byBhIHN0cmluZyBQWCB2YWx1ZS5cbiAgICBmdW5jdGlvbiBzZXR1cEJvZHlNYXJnaW5WYWx1ZXMoKSB7XG4gICAgICBpZiAoXG4gICAgICAgICdudW1iZXInID09PVxuICAgICAgICAgIHR5cGVvZiAoc2V0dGluZ3NbaWZyYW1lSWRdICYmIHNldHRpbmdzW2lmcmFtZUlkXS5ib2R5TWFyZ2luKSB8fFxuICAgICAgICAnMCcgPT09IChzZXR0aW5nc1tpZnJhbWVJZF0gJiYgc2V0dGluZ3NbaWZyYW1lSWRdLmJvZHlNYXJnaW4pXG4gICAgICApIHtcbiAgICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmJvZHlNYXJnaW5WMSA9IHNldHRpbmdzW2lmcmFtZUlkXS5ib2R5TWFyZ2luO1xuICAgICAgICBzZXR0aW5nc1tpZnJhbWVJZF0uYm9keU1hcmdpbiA9XG4gICAgICAgICAgJycgKyBzZXR0aW5nc1tpZnJhbWVJZF0uYm9keU1hcmdpbiArICdweCc7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2hlY2tSZXNldCgpIHtcbiAgICAgIC8vIFJlZHVjZSBzY29wZSBvZiBmaXJzdFJ1biB0byBmdW5jdGlvbiwgYmVjYXVzZSBJRTgncyBKUyBleGVjdXRpb25cbiAgICAgIC8vIGNvbnRleHQgc3RhY2sgaXMgYm9ya2VkIGFuZCB0aGlzIHZhbHVlIGdldHMgZXh0ZXJuYWxseVxuICAgICAgLy8gY2hhbmdlZCBtaWR3YXkgdGhyb3VnaCBydW5uaW5nIHRoaXMgZnVuY3Rpb24hISFcbiAgICAgIHZhciBmaXJzdFJ1biA9IHNldHRpbmdzW2lmcmFtZUlkXSAmJiBzZXR0aW5nc1tpZnJhbWVJZF0uZmlyc3RSdW4sXG4gICAgICAgIHJlc2V0UmVxdWVydE1ldGhvZCA9XG4gICAgICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdICYmXG4gICAgICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmhlaWdodENhbGN1bGF0aW9uTWV0aG9kIGluIHJlc2V0UmVxdWlyZWRNZXRob2RzO1xuXG4gICAgICBpZiAoIWZpcnN0UnVuICYmIHJlc2V0UmVxdWVydE1ldGhvZCkge1xuICAgICAgICByZXNldElGcmFtZSh7IGlmcmFtZTogaWZyYW1lLCBoZWlnaHQ6IDAsIHdpZHRoOiAwLCB0eXBlOiAnaW5pdCcgfSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2V0dXBJRnJhbWVPYmplY3QoKSB7XG4gICAgICBpZiAoRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQgJiYgc2V0dGluZ3NbaWZyYW1lSWRdKSB7XG4gICAgICAgIC8vSWdub3JlIHVucG9seWZpbGxlZCBJRTguXG4gICAgICAgIHNldHRpbmdzW2lmcmFtZUlkXS5pZnJhbWUuaUZyYW1lUmVzaXplciA9IHtcbiAgICAgICAgICBjbG9zZTogY2xvc2VJRnJhbWUuYmluZChudWxsLCBzZXR0aW5nc1tpZnJhbWVJZF0uaWZyYW1lKSxcblxuICAgICAgICAgIHJlbW92ZUxpc3RlbmVyczogcmVtb3ZlSWZyYW1lTGlzdGVuZXJzLmJpbmQoXG4gICAgICAgICAgICBudWxsLFxuICAgICAgICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmlmcmFtZVxuICAgICAgICAgICksXG5cbiAgICAgICAgICByZXNpemU6IHRyaWdnZXIuYmluZChcbiAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAnV2luZG93IHJlc2l6ZScsXG4gICAgICAgICAgICAncmVzaXplJyxcbiAgICAgICAgICAgIHNldHRpbmdzW2lmcmFtZUlkXS5pZnJhbWVcbiAgICAgICAgICApLFxuXG4gICAgICAgICAgbW92ZVRvQW5jaG9yOiBmdW5jdGlvbihhbmNob3IpIHtcbiAgICAgICAgICAgIHRyaWdnZXIoXG4gICAgICAgICAgICAgICdNb3ZlIHRvIGFuY2hvcicsXG4gICAgICAgICAgICAgICdtb3ZlVG9BbmNob3I6JyArIGFuY2hvcixcbiAgICAgICAgICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdLmlmcmFtZSxcbiAgICAgICAgICAgICAgaWZyYW1lSWRcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIHNlbmRNZXNzYWdlOiBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgICAgICAgICBtZXNzYWdlID0gSlNPTi5zdHJpbmdpZnkobWVzc2FnZSk7XG4gICAgICAgICAgICB0cmlnZ2VyKFxuICAgICAgICAgICAgICAnU2VuZCBNZXNzYWdlJyxcbiAgICAgICAgICAgICAgJ21lc3NhZ2U6JyArIG1lc3NhZ2UsXG4gICAgICAgICAgICAgIHNldHRpbmdzW2lmcmFtZUlkXS5pZnJhbWUsXG4gICAgICAgICAgICAgIGlmcmFtZUlkXG4gICAgICAgICAgICApO1xuICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvL1dlIGhhdmUgdG8gY2FsbCB0cmlnZ2VyIHR3aWNlLCBhcyB3ZSBjYW4gbm90IGJlIHN1cmUgaWYgYWxsXG4gICAgLy9pZnJhbWVzIGhhdmUgY29tcGxldGVkIGxvYWRpbmcgd2hlbiB0aGlzIGNvZGUgcnVucy4gVGhlXG4gICAgLy9ldmVudCBsaXN0ZW5lciBhbHNvIGNhdGNoZXMgdGhlIHBhZ2UgY2hhbmdpbmcgaW4gdGhlIGlGcmFtZS5cbiAgICBmdW5jdGlvbiBpbml0KG1zZykge1xuICAgICAgZnVuY3Rpb24gaUZyYW1lTG9hZGVkKCkge1xuICAgICAgICB0cmlnZ2VyKCdpRnJhbWUub25sb2FkJywgbXNnLCBpZnJhbWUsIHVuZGVmaW5lZCwgdHJ1ZSk7XG4gICAgICAgIGNoZWNrUmVzZXQoKTtcbiAgICAgIH1cblxuICAgICAgZnVuY3Rpb24gY3JlYXRlRGVzdHJveU9ic2VydmVyKE11dGF0aW9uT2JzZXJ2ZXIpIHtcbiAgICAgICAgaWYgKCFpZnJhbWUucGFyZW50Tm9kZSkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBkZXN0cm95T2JzZXJ2ZXIgPSBuZXcgTXV0YXRpb25PYnNlcnZlcihmdW5jdGlvbiAobXV0YXRpb25zKSB7XG4gICAgICAgICAgbXV0YXRpb25zLmZvckVhY2goZnVuY3Rpb24gKG11dGF0aW9uKSB7XG4gICAgICAgICAgICB2YXIgcmVtb3ZlZE5vZGVzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwobXV0YXRpb24ucmVtb3ZlZE5vZGVzKTsgLy8gVHJhbnNmb3JtIE5vZGVMaXN0IGludG8gYW4gQXJyYXlcbiAgICAgICAgICAgIHJlbW92ZWROb2Rlcy5mb3JFYWNoKGZ1bmN0aW9uIChyZW1vdmVkTm9kZSkge1xuICAgICAgICAgICAgICBpZiAocmVtb3ZlZE5vZGUgPT09IGlmcmFtZSkge1xuICAgICAgICAgICAgICAgIGNsb3NlSUZyYW1lKGlmcmFtZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgICAgZGVzdHJveU9ic2VydmVyLm9ic2VydmUoaWZyYW1lLnBhcmVudE5vZGUsIHtcbiAgICAgICAgICBjaGlsZExpc3Q6IHRydWVcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIHZhciBNdXRhdGlvbk9ic2VydmVyID0gZ2V0TXV0YXRpb25PYnNlcnZlcigpO1xuICAgICAgaWYgKE11dGF0aW9uT2JzZXJ2ZXIpIHtcbiAgICAgICAgY3JlYXRlRGVzdHJveU9ic2VydmVyKE11dGF0aW9uT2JzZXJ2ZXIpO1xuICAgICAgfVxuXG4gICAgICBhZGRFdmVudExpc3RlbmVyKGlmcmFtZSwgJ2xvYWQnLCBpRnJhbWVMb2FkZWQpO1xuICAgICAgdHJpZ2dlcignaW5pdCcsIG1zZywgaWZyYW1lLCB1bmRlZmluZWQsIHRydWUpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNoZWNrT3B0aW9ucyhvcHRpb25zKSB7XG4gICAgICBpZiAoJ29iamVjdCcgIT09IHR5cGVvZiBvcHRpb25zKSB7XG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ09wdGlvbnMgaXMgbm90IGFuIG9iamVjdCcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNvcHlPcHRpb25zKG9wdGlvbnMpIHtcbiAgICAgIGZvciAodmFyIG9wdGlvbiBpbiBkZWZhdWx0cykge1xuICAgICAgICBpZiAoZGVmYXVsdHMuaGFzT3duUHJvcGVydHkob3B0aW9uKSkge1xuICAgICAgICAgIHNldHRpbmdzW2lmcmFtZUlkXVtvcHRpb25dID0gb3B0aW9ucy5oYXNPd25Qcm9wZXJ0eShvcHRpb24pXG4gICAgICAgICAgICA/IG9wdGlvbnNbb3B0aW9uXVxuICAgICAgICAgICAgOiBkZWZhdWx0c1tvcHRpb25dO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0VGFyZ2V0T3JpZ2luKHJlbW90ZUhvc3QpIHtcbiAgICAgIHJldHVybiAnJyA9PT0gcmVtb3RlSG9zdCB8fCAnZmlsZTovLycgPT09IHJlbW90ZUhvc3QgPyAnKicgOiByZW1vdGVIb3N0O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHByb2Nlc3NPcHRpb25zKG9wdGlvbnMpIHtcbiAgICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdID0ge1xuICAgICAgICBmaXJzdFJ1bjogdHJ1ZSxcbiAgICAgICAgaWZyYW1lOiBpZnJhbWUsXG4gICAgICAgIHJlbW90ZUhvc3Q6IGlmcmFtZS5zcmNcbiAgICAgICAgICAuc3BsaXQoJy8nKVxuICAgICAgICAgIC5zbGljZSgwLCAzKVxuICAgICAgICAgIC5qb2luKCcvJylcbiAgICAgIH07XG5cbiAgICAgIGNoZWNrT3B0aW9ucyhvcHRpb25zKTtcbiAgICAgIGNvcHlPcHRpb25zKG9wdGlvbnMpO1xuXG4gICAgICBpZiAoc2V0dGluZ3NbaWZyYW1lSWRdKSB7XG4gICAgICAgIHNldHRpbmdzW2lmcmFtZUlkXS50YXJnZXRPcmlnaW4gPVxuICAgICAgICAgIHRydWUgPT09IHNldHRpbmdzW2lmcmFtZUlkXS5jaGVja09yaWdpblxuICAgICAgICAgICAgPyBnZXRUYXJnZXRPcmlnaW4oc2V0dGluZ3NbaWZyYW1lSWRdLnJlbW90ZUhvc3QpXG4gICAgICAgICAgICA6ICcqJztcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBiZWVuSGVyZSgpIHtcbiAgICAgIHJldHVybiBpZnJhbWVJZCBpbiBzZXR0aW5ncyAmJiAnaUZyYW1lUmVzaXplcicgaW4gaWZyYW1lO1xuICAgIH1cblxuICAgIHZhciBpZnJhbWVJZCA9IGVuc3VyZUhhc0lkKGlmcmFtZS5pZCk7XG5cbiAgICBpZiAoIWJlZW5IZXJlKCkpIHtcbiAgICAgIHByb2Nlc3NPcHRpb25zKG9wdGlvbnMpO1xuICAgICAgc2V0U2Nyb2xsaW5nKCk7XG4gICAgICBzZXRMaW1pdHMoKTtcbiAgICAgIHNldHVwQm9keU1hcmdpblZhbHVlcygpO1xuICAgICAgaW5pdChjcmVhdGVPdXRnb2luZ01zZyhpZnJhbWVJZCkpO1xuICAgICAgc2V0dXBJRnJhbWVPYmplY3QoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgd2FybihpZnJhbWVJZCwgJ0lnbm9yZWQgaUZyYW1lLCBhbHJlYWR5IHNldHVwLicpO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIGRlYm91Y2UoZm4sIHRpbWUpIHtcbiAgICBpZiAobnVsbCA9PT0gdGltZXIpIHtcbiAgICAgIHRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgdGltZXIgPSBudWxsO1xuICAgICAgICBmbigpO1xuICAgICAgfSwgdGltZSk7XG4gICAgfVxuICB9XG5cbiAgdmFyIGZyYW1lVGltZXIgPSB7fTtcbiAgZnVuY3Rpb24gZGVib3VuY2VGcmFtZUV2ZW50cyhmbiwgdGltZSwgZnJhbWVJZCkge1xuICAgIGlmICghZnJhbWVUaW1lcltmcmFtZUlkXSkge1xuICAgICAgZnJhbWVUaW1lcltmcmFtZUlkXSA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgIGZyYW1lVGltZXJbZnJhbWVJZF0gPSBudWxsO1xuICAgICAgICBmbigpO1xuICAgICAgfSwgdGltZSk7XG4gICAgfVxuICB9IC8vTm90IHRlc3RhYmxlIGluIFBoYW50b21KU1xuXG4gIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovIGZ1bmN0aW9uIGZpeEhpZGRlbklGcmFtZXMoKSB7XG4gICAgZnVuY3Rpb24gY2hlY2tJRnJhbWVzKCkge1xuICAgICAgZnVuY3Rpb24gY2hlY2tJRnJhbWUoc2V0dGluZ0lkKSB7XG4gICAgICAgIGZ1bmN0aW9uIGNoa0RpbWVuc2lvbihkaW1lbnNpb24pIHtcbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgJzBweCcgPT09XG4gICAgICAgICAgICAoc2V0dGluZ3Nbc2V0dGluZ0lkXSAmJiBzZXR0aW5nc1tzZXR0aW5nSWRdLmlmcmFtZS5zdHlsZVtkaW1lbnNpb25dKVxuICAgICAgICAgICk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBpc1Zpc2libGUoZWwpIHtcbiAgICAgICAgICByZXR1cm4gbnVsbCAhPT0gZWwub2Zmc2V0UGFyZW50O1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKFxuICAgICAgICAgIHNldHRpbmdzW3NldHRpbmdJZF0gJiZcbiAgICAgICAgICBpc1Zpc2libGUoc2V0dGluZ3Nbc2V0dGluZ0lkXS5pZnJhbWUpICYmXG4gICAgICAgICAgKGNoa0RpbWVuc2lvbignaGVpZ2h0JykgfHwgY2hrRGltZW5zaW9uKCd3aWR0aCcpKVxuICAgICAgICApIHtcbiAgICAgICAgICB0cmlnZ2VyKFxuICAgICAgICAgICAgJ1Zpc2liaWxpdHkgY2hhbmdlJyxcbiAgICAgICAgICAgICdyZXNpemUnLFxuICAgICAgICAgICAgc2V0dGluZ3Nbc2V0dGluZ0lkXS5pZnJhbWUsXG4gICAgICAgICAgICBzZXR0aW5nSWRcbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGZvciAodmFyIHNldHRpbmdJZCBpbiBzZXR0aW5ncykge1xuICAgICAgICBjaGVja0lGcmFtZShzZXR0aW5nSWQpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIG11dGF0aW9uT2JzZXJ2ZWQobXV0YXRpb25zKSB7XG4gICAgICBsb2coXG4gICAgICAgICd3aW5kb3cnLFxuICAgICAgICAnTXV0YXRpb24gb2JzZXJ2ZWQ6ICcgKyBtdXRhdGlvbnNbMF0udGFyZ2V0ICsgJyAnICsgbXV0YXRpb25zWzBdLnR5cGVcbiAgICAgICk7XG4gICAgICBkZWJvdWNlKGNoZWNrSUZyYW1lcywgMTYpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNyZWF0ZU11dGF0aW9uT2JzZXJ2ZXIoKSB7XG4gICAgICB2YXIgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignYm9keScpLFxuICAgICAgICBjb25maWcgPSB7XG4gICAgICAgICAgYXR0cmlidXRlczogdHJ1ZSxcbiAgICAgICAgICBhdHRyaWJ1dGVPbGRWYWx1ZTogZmFsc2UsXG4gICAgICAgICAgY2hhcmFjdGVyRGF0YTogdHJ1ZSxcbiAgICAgICAgICBjaGFyYWN0ZXJEYXRhT2xkVmFsdWU6IGZhbHNlLFxuICAgICAgICAgIGNoaWxkTGlzdDogdHJ1ZSxcbiAgICAgICAgICBzdWJ0cmVlOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIG9ic2VydmVyID0gbmV3IE11dGF0aW9uT2JzZXJ2ZXIobXV0YXRpb25PYnNlcnZlZCk7XG5cbiAgICAgIG9ic2VydmVyLm9ic2VydmUodGFyZ2V0LCBjb25maWcpO1xuICAgIH1cblxuICAgIHZhciBNdXRhdGlvbk9ic2VydmVyID0gZ2V0TXV0YXRpb25PYnNlcnZlcigpO1xuICAgIGlmIChNdXRhdGlvbk9ic2VydmVyKSB7XG4gICAgICBjcmVhdGVNdXRhdGlvbk9ic2VydmVyKCk7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gcmVzaXplSUZyYW1lcyhldmVudCkge1xuICAgIGZ1bmN0aW9uIHJlc2l6ZSgpIHtcbiAgICAgIHNlbmRUcmlnZ2VyTXNnKCdXaW5kb3cgJyArIGV2ZW50LCAncmVzaXplJyk7XG4gICAgfVxuXG4gICAgbG9nKCd3aW5kb3cnLCAnVHJpZ2dlciBldmVudDogJyArIGV2ZW50KTtcbiAgICBkZWJvdWNlKHJlc2l6ZSwgMTYpO1xuICB9IC8vTm90IHRlc3RhYmxlIGluIFBoYW50b21KU1xuXG4gIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovIGZ1bmN0aW9uIHRhYlZpc2libGUoKSB7XG4gICAgZnVuY3Rpb24gcmVzaXplKCkge1xuICAgICAgc2VuZFRyaWdnZXJNc2coJ1RhYiBWaXNhYmxlJywgJ3Jlc2l6ZScpO1xuICAgIH1cblxuICAgIGlmICgnaGlkZGVuJyAhPT0gZG9jdW1lbnQudmlzaWJpbGl0eVN0YXRlKSB7XG4gICAgICBsb2coJ2RvY3VtZW50JywgJ1RyaWdnZXIgZXZlbnQ6IFZpc2libGl0eSBjaGFuZ2UnKTtcbiAgICAgIGRlYm91Y2UocmVzaXplLCAxNik7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gc2VuZFRyaWdnZXJNc2coZXZlbnROYW1lLCBldmVudCkge1xuICAgIGZ1bmN0aW9uIGlzSUZyYW1lUmVzaXplRW5hYmxlZChpZnJhbWVJZCkge1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgc2V0dGluZ3NbaWZyYW1lSWRdICYmXG4gICAgICAgICdwYXJlbnQnID09PSBzZXR0aW5nc1tpZnJhbWVJZF0ucmVzaXplRnJvbSAmJlxuICAgICAgICBzZXR0aW5nc1tpZnJhbWVJZF0uYXV0b1Jlc2l6ZSAmJlxuICAgICAgICAhc2V0dGluZ3NbaWZyYW1lSWRdLmZpcnN0UnVuXG4gICAgICApO1xuICAgIH1cblxuICAgIGZvciAodmFyIGlmcmFtZUlkIGluIHNldHRpbmdzKSB7XG4gICAgICBpZiAoaXNJRnJhbWVSZXNpemVFbmFibGVkKGlmcmFtZUlkKSkge1xuICAgICAgICB0cmlnZ2VyKGV2ZW50TmFtZSwgZXZlbnQsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlmcmFtZUlkKSwgaWZyYW1lSWQpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHNldHVwRXZlbnRMaXN0ZW5lcnMoKSB7XG4gICAgYWRkRXZlbnRMaXN0ZW5lcih3aW5kb3csICdtZXNzYWdlJywgaUZyYW1lTGlzdGVuZXIpO1xuXG4gICAgYWRkRXZlbnRMaXN0ZW5lcih3aW5kb3csICdyZXNpemUnLCBmdW5jdGlvbigpIHtcbiAgICAgIHJlc2l6ZUlGcmFtZXMoJ3Jlc2l6ZScpO1xuICAgIH0pO1xuXG4gICAgYWRkRXZlbnRMaXN0ZW5lcihkb2N1bWVudCwgJ3Zpc2liaWxpdHljaGFuZ2UnLCB0YWJWaXNpYmxlKTtcbiAgICBhZGRFdmVudExpc3RlbmVyKGRvY3VtZW50LCAnLXdlYmtpdC12aXNpYmlsaXR5Y2hhbmdlJywgdGFiVmlzaWJsZSk7IC8vQW5kcmlvZCA0LjRcbiAgICBhZGRFdmVudExpc3RlbmVyKHdpbmRvdywgJ2ZvY3VzaW4nLCBmdW5jdGlvbigpIHtcbiAgICAgIHJlc2l6ZUlGcmFtZXMoJ2ZvY3VzJyk7XG4gICAgfSk7IC8vSUU4LTlcbiAgICBhZGRFdmVudExpc3RlbmVyKHdpbmRvdywgJ2ZvY3VzJywgZnVuY3Rpb24oKSB7XG4gICAgICByZXNpemVJRnJhbWVzKCdmb2N1cycpO1xuICAgIH0pO1xuICB9XG5cbiAgZnVuY3Rpb24gZmFjdG9yeSgpIHtcbiAgICBmdW5jdGlvbiBpbml0KG9wdGlvbnMsIGVsZW1lbnQpIHtcbiAgICAgIGZ1bmN0aW9uIGNoa1R5cGUoKSB7XG4gICAgICAgIGlmICghZWxlbWVudC50YWdOYW1lKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignT2JqZWN0IGlzIG5vdCBhIHZhbGlkIERPTSBlbGVtZW50Jyk7XG4gICAgICAgIH0gZWxzZSBpZiAoJ0lGUkFNRScgIT09IGVsZW1lbnQudGFnTmFtZS50b1VwcGVyQ2FzZSgpKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcbiAgICAgICAgICAgICdFeHBlY3RlZCA8SUZSQU1FPiB0YWcsIGZvdW5kIDwnICsgZWxlbWVudC50YWdOYW1lICsgJz4nXG4gICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoZWxlbWVudCkge1xuICAgICAgICBjaGtUeXBlKCk7XG4gICAgICAgIHNldHVwSUZyYW1lKGVsZW1lbnQsIG9wdGlvbnMpO1xuICAgICAgICBpRnJhbWVzLnB1c2goZWxlbWVudCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gd2FybkRlcHJlY2F0ZWRPcHRpb25zKG9wdGlvbnMpIHtcbiAgICAgIGlmIChvcHRpb25zICYmIG9wdGlvbnMuZW5hYmxlUHVibGljTWV0aG9kcykge1xuICAgICAgICB3YXJuKFxuICAgICAgICAgICdlbmFibGVQdWJsaWNNZXRob2RzIG9wdGlvbiBoYXMgYmVlbiByZW1vdmVkLCBwdWJsaWMgbWV0aG9kcyBhcmUgbm93IGFsd2F5cyBhdmFpbGFibGUgaW4gdGhlIGlGcmFtZSdcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgaUZyYW1lcztcblxuICAgIHNldHVwUmVxdWVzdEFuaW1hdGlvbkZyYW1lKCk7XG4gICAgc2V0dXBFdmVudExpc3RlbmVycygpO1xuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIGlGcmFtZVJlc2l6ZUYob3B0aW9ucywgdGFyZ2V0KSB7XG4gICAgICBpRnJhbWVzID0gW107IC8vT25seSByZXR1cm4gaUZyYW1lcyBwYXN0IGluIG9uIHRoaXMgY2FsbFxuXG4gICAgICB3YXJuRGVwcmVjYXRlZE9wdGlvbnMob3B0aW9ucyk7XG5cbiAgICAgIHN3aXRjaCAodHlwZW9mIHRhcmdldCkge1xuICAgICAgICBjYXNlICd1bmRlZmluZWQnOlxuICAgICAgICBjYXNlICdzdHJpbmcnOlxuICAgICAgICAgIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHRhcmdldCB8fCAnaWZyYW1lJyksXG4gICAgICAgICAgICBpbml0LmJpbmQodW5kZWZpbmVkLCBvcHRpb25zKVxuICAgICAgICAgICk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ29iamVjdCc6XG4gICAgICAgICAgaW5pdChvcHRpb25zLCB0YXJnZXQpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1VuZXhwZWN0ZWQgZGF0YSB0eXBlICgnICsgdHlwZW9mIHRhcmdldCArICcpJyk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBpRnJhbWVzO1xuICAgIH07XG4gIH1cblxuICBmdW5jdGlvbiBjcmVhdGVKUXVlcnlQdWJsaWNNZXRob2QoJCkge1xuICAgIGlmICghJC5mbikge1xuICAgICAgaW5mbygnJywgJ1VuYWJsZSB0byBiaW5kIHRvIGpRdWVyeSwgaXQgaXMgbm90IGZ1bGx5IGxvYWRlZC4nKTtcbiAgICB9IGVsc2UgaWYgKCEkLmZuLmlGcmFtZVJlc2l6ZSkge1xuICAgICAgJC5mbi5pRnJhbWVSZXNpemUgPSBmdW5jdGlvbiAkaUZyYW1lUmVzaXplRihvcHRpb25zKSB7XG4gICAgICAgIGZ1bmN0aW9uIGluaXQoaW5kZXgsIGVsZW1lbnQpIHtcbiAgICAgICAgICBzZXR1cElGcmFtZShlbGVtZW50LCBvcHRpb25zKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLmZpbHRlcignaWZyYW1lJylcbiAgICAgICAgICAuZWFjaChpbml0KVxuICAgICAgICAgIC5lbmQoKTtcbiAgICAgIH07XG4gICAgfVxuICB9XG5cbiAgaWYgKHdpbmRvdy5qUXVlcnkpIHtcbiAgICBjcmVhdGVKUXVlcnlQdWJsaWNNZXRob2Qod2luZG93LmpRdWVyeSk7XG4gIH1cblxuICBpZiAodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSB7XG4gICAgZGVmaW5lKFtdLCBmYWN0b3J5KTtcbiAgfSBlbHNlIGlmICh0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlLmV4cG9ydHMgPT09ICdvYmplY3QnKSB7XG4gICAgLy9Ob2RlIGZvciBicm93c2VyZnlcbiAgICBtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcbiAgfVxuICB3aW5kb3cuaUZyYW1lUmVzaXplID0gd2luZG93LmlGcmFtZVJlc2l6ZSB8fCBmYWN0b3J5KCk7XG59KSgpO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==