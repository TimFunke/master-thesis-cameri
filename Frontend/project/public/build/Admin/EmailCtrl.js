(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Admin/EmailCtrl"],{

/***/ "./assets/js/Admin/EmailCtrl.js":
/*!**************************************!*\
  !*** ./assets/js/Admin/EmailCtrl.js ***!
  \**************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($, jQuery) {/* harmony import */ var _Library_Selectize__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Library/Selectize */ "./assets/js/Library/Selectize.js");
/* harmony import */ var _Common_Angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Common/Angular */ "./assets/js/Common/Angular.js");
/* harmony import */ var _Common_Common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Common/Common */ "./assets/js/Common/Common.js");
/* harmony import */ var _Library_TinyMCE__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Library/TinyMCE */ "./assets/js/Library/TinyMCE.js");
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }






var EmailController = /*#__PURE__*/function () {
  function EmailController(eid) {
    _classCallCheck(this, EmailController);

    this.eid = eid;
    this.data = {
      reset: 1,
      receiver: 'Everyone',
      custom: ''
    };
    this.enabled = true;
    this.tinyMCE();
  }

  _createClass(EmailController, [{
    key: "init",
    value: function init(data) {
      if (data) {
        this.data = $.parseJSON(data);
        this.data.reset = 0;
      }
    }
  }, {
    key: "send",
    value: function send() {
      var _this = this;

      this.enabled = false;
      this.eid.change(Routing.generate('admin_email_send'), this.data).then(function (response) {
        if (response.data.success) {
          var progressBar = $('#progress-bar');
          var percent = response.data.msg.percent;
          progressBar.parent().show();
          progressBar.css('width', percent + '%');

          if (percent < 100) {
            _this.data.reset = 0;

            _this.send();
          } else {
            _Common_Common__WEBPACK_IMPORTED_MODULE_2__["default"].showAlertDialog('success', 'All emails were sent');
          }
        } else {
          var result = response.data.msg;
          _Common_Common__WEBPACK_IMPORTED_MODULE_2__["default"].showAlertDialog('danger', result.msg);

          if (result.stop === false) {
            _this.enabled = true;
          }
        }
      });
    }
  }, {
    key: "tinyMCE",
    value: function (_tinyMCE) {
      function tinyMCE() {
        return _tinyMCE.apply(this, arguments);
      }

      tinyMCE.toString = function () {
        return _tinyMCE.toString();
      };

      return tinyMCE;
    }(function () {
      var _this2 = this;

      tinyMCE.baseURL = "/build/tinymce";
      _Library_TinyMCE__WEBPACK_IMPORTED_MODULE_3__["default"].init({
        selector: '.tinymce',
        height: 200,
        theme: 'modern',
        plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak', 'searchreplace wordcount visualblocks visualchars code fullscreen', 'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons template paste textcolor colorpicker textpattern imagetools'],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        setup: function setup(ed) {
          ed.on('change', function () {
            _this2.data.body = ed.getContent();
          });
        }
      });
    })
  }]);

  return EmailController;
}();

var selectizeFieldDirective = /*#__PURE__*/function () {
  function selectizeFieldDirective() {
    _classCallCheck(this, selectizeFieldDirective);

    this.require = "ngModel";
  }

  _createClass(selectizeFieldDirective, [{
    key: "link",
    value: function link($scope, element, attrs, ngModel) {
      element.selectize({
        valueField: 'title',
        labelField: 'title',
        searchField: 'title',
        delimiter: ',',
        plugins: ['remove_button'],
        options: [],
        create: false,
        load: function load(query, callback) {
          if (!query.length) return callback();
          jQuery.ajax({
            type: "POST",
            url: Routing.generate('admin_email_search'),
            data: {
              term: encodeURIComponent(query)
            },
            error: function error() {
              callback();
            },
            success: function success(data) {
              callback(data);
            }
          });
        }
      }).on('change', function () {
        $scope.$apply(function () {
          var newValue = element.selectize().val();
          ngModel.$setViewValue(newValue);
        });
      });
    }
  }]);

  return selectizeFieldDirective;
}();

_Common_Angular__WEBPACK_IMPORTED_MODULE_1__["default"].controller('EmailController', ['eid', EmailController]).directive('selectizeField', function () {
  return new selectizeFieldDirective();
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/Common/Angular.js":
/*!*************************************!*\
  !*** ./assets/js/Common/Angular.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! angular */ "./node_modules/angular/index.js");
/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(angular__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Common */ "./assets/js/Common/Common.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/



var Eid = /*#__PURE__*/function () {
  /**
   * Initiate
   *
   * @param $http
   */
  function Eid($http) {
    _classCallCheck(this, Eid);

    this.$http = $http;
    this.loading = false;
  }
  /**
   * send normal POST
   *
   * @param url
   * @param input
   * @param event
   * @returns {Promise<any> | Promise<T> | *}
   */


  _createClass(Eid, [{
    key: "change",
    value: function change(url, input, event) {
      this.loading = true;

      if (!input) {
        input = {};
      }

      var headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      return this.send('POST', url, $.param(input), headers, event);
    }
    /**
     * Send a GET request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "get",
    value: function get(url, event) {
      return this.send('GET', url, null, null, event);
    }
    /**
     * Send a POST request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "post",
    value: function post(url, input, event) {
      return this.send('POST', url, input, null, event);
    }
    /**
     * Send a DELETE request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "delete",
    value: function _delete(url, event) {
      return this.send('DELETE', url, null, null, event);
    }
    /**
     * Send a PATCH request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "patch",
    value: function patch(url, input, event) {
      var headers = {
        'Content-Type': 'application/merge-patch+json',
        'accept': 'application/json'
      };
      return this.send('PATCH', url, input, headers, event);
    }
    /**
     * Send a request
     *
     * @param method
     * @param url
     * @param input
     * @param headers
     * @param event
     * @returns {Promise<any> | Promise<T> | *}
     */

  }, {
    key: "send",
    value: function send(method, url, input, headers, event) {
      var btn = null;

      if (!input) {
        input = {};
      }

      if (!headers) {
        headers = {
          'Content-Type': 'application/json',
          'accept': 'application/ld+json'
        };
      }

      if (event) {
        btn = $(event.currentTarget);
        btn.addClass('running');
        btn.prop("disabled", true);
      }

      return this.$http({
        method: method,
        url: url,
        data: input,
        headers: headers
      })["catch"](function (error) {
        if (error.data.detail) {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', error.data.detail);
        } else {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', 'Action cannot be performed');
        }
      })["finally"](function () {
        $(document).trigger('loading.stop');

        if (btn) {
          var modal = btn.data('close');

          if (modal) {
            $('#' + modal).modal('hide');
          }

          btn.removeClass('running');
          btn.prop("disabled", false);
        }
      });
    }
  }]);

  return Eid;
}();

var app = angular__WEBPACK_IMPORTED_MODULE_0___default.a.module('Ecosystem', []) // change Angular default {{ to {[{ to avoid conflict with Twig
.config(['$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false,
    rewriteLinks: false
  });
}]).filter('unsafe', ['$sce', function ($sce) {
  return function (val) {
    return $sce.trustAsHtml(val);
  };
}]).filter('range', function () {
  return function (input, total) {
    total = parseInt(total);

    for (var i = 0; i < total; i++) {
      input.push(i);
    }

    return input;
  };
}).service('eid', ['$http', Eid]).directive('form', ['$location', function ($location) {
  return {
    restrict: 'E',
    priority: 999,
    compile: function compile() {
      return {
        pre: function pre(scope, element, attrs) {
          if (attrs.noaction === '') return;

          if (attrs.action === undefined || attrs.action === '') {
            attrs.action = $location.absUrl();
          }
        }
      };
    }
  };
}]).directive('initTooltip', function () {
  return {
    restrict: 'A',
    link: function link(scope, element, attrs) {
      $(element).tooltip({
        trigger: 'hover'
      });
    }
  };
});
/* harmony default export */ __webpack_exports__["default"] = (app);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/Library/TinyMCE.js":
/*!**************************************!*\
  !*** ./assets/js/Library/TinyMCE.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tinymce__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tinymce */ "./node_modules/tinymce/tinymce.js");
/* harmony import */ var tinymce__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(tinymce__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var tinymce_themes_modern_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tinymce/themes/modern/theme */ "./node_modules/tinymce/themes/modern/theme.js");
/* harmony import */ var tinymce_themes_modern_theme__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tinymce_themes_modern_theme__WEBPACK_IMPORTED_MODULE_1__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/




/* harmony default export */ __webpack_exports__["default"] = (tinymce__WEBPACK_IMPORTED_MODULE_0___default.a);

/***/ })

},[["./assets/js/Admin/EmailCtrl.js","runtime","vendors~Admin/EmailCtrl~Admin/FileAudit~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Recor~b3f38238","vendors~Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/SearchCtrl~BigData/Se~6c26450f","vendors~Admin/EmailCtrl~CMS/Page~Library/TinyMCE~STO/Authoring","Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Record~Archive/SearchCtrl~Aut~d45f3fc6","Admin/EmailCtrl~Common/PrototypeCtrl~Content/IdentifierCtrl~Content/PwDSickActReportCtrl~Content/PwD~d97d5f95"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQWRtaW4vRW1haWxDdHJsLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9Db21tb24vQW5ndWxhci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvTGlicmFyeS9UaW55TUNFLmpzIl0sIm5hbWVzIjpbIkVtYWlsQ29udHJvbGxlciIsImVpZCIsImRhdGEiLCJyZXNldCIsInJlY2VpdmVyIiwiY3VzdG9tIiwiZW5hYmxlZCIsInRpbnlNQ0UiLCIkIiwicGFyc2VKU09OIiwiY2hhbmdlIiwiUm91dGluZyIsImdlbmVyYXRlIiwidGhlbiIsInJlc3BvbnNlIiwic3VjY2VzcyIsInByb2dyZXNzQmFyIiwicGVyY2VudCIsIm1zZyIsInBhcmVudCIsInNob3ciLCJjc3MiLCJzZW5kIiwiQ29tbW9uIiwic2hvd0FsZXJ0RGlhbG9nIiwicmVzdWx0Iiwic3RvcCIsImJhc2VVUkwiLCJ0aW55bWNlIiwiaW5pdCIsInNlbGVjdG9yIiwiaGVpZ2h0IiwidGhlbWUiLCJwbHVnaW5zIiwidG9vbGJhcjEiLCJ0b29sYmFyMiIsInNldHVwIiwiZWQiLCJvbiIsImJvZHkiLCJnZXRDb250ZW50Iiwic2VsZWN0aXplRmllbGREaXJlY3RpdmUiLCJyZXF1aXJlIiwiJHNjb3BlIiwiZWxlbWVudCIsImF0dHJzIiwibmdNb2RlbCIsInNlbGVjdGl6ZSIsInZhbHVlRmllbGQiLCJsYWJlbEZpZWxkIiwic2VhcmNoRmllbGQiLCJkZWxpbWl0ZXIiLCJvcHRpb25zIiwiY3JlYXRlIiwibG9hZCIsInF1ZXJ5IiwiY2FsbGJhY2siLCJsZW5ndGgiLCJqUXVlcnkiLCJhamF4IiwidHlwZSIsInVybCIsInRlcm0iLCJlbmNvZGVVUklDb21wb25lbnQiLCJlcnJvciIsIiRhcHBseSIsIm5ld1ZhbHVlIiwidmFsIiwiJHNldFZpZXdWYWx1ZSIsImFwcCIsImNvbnRyb2xsZXIiLCJkaXJlY3RpdmUiLCJFaWQiLCIkaHR0cCIsImxvYWRpbmciLCJpbnB1dCIsImV2ZW50IiwiaGVhZGVycyIsInBhcmFtIiwibWV0aG9kIiwiYnRuIiwiY3VycmVudFRhcmdldCIsImFkZENsYXNzIiwicHJvcCIsImRldGFpbCIsImRvY3VtZW50IiwidHJpZ2dlciIsIm1vZGFsIiwicmVtb3ZlQ2xhc3MiLCJhbmd1bGFyIiwibW9kdWxlIiwiY29uZmlnIiwiJGludGVycG9sYXRlUHJvdmlkZXIiLCIkbG9jYXRpb25Qcm92aWRlciIsInN0YXJ0U3ltYm9sIiwiZW5kU3ltYm9sIiwiaHRtbDVNb2RlIiwicmVxdWlyZUJhc2UiLCJyZXdyaXRlTGlua3MiLCJmaWx0ZXIiLCIkc2NlIiwidHJ1c3RBc0h0bWwiLCJ0b3RhbCIsInBhcnNlSW50IiwiaSIsInB1c2giLCJzZXJ2aWNlIiwiJGxvY2F0aW9uIiwicmVzdHJpY3QiLCJwcmlvcml0eSIsImNvbXBpbGUiLCJwcmUiLCJzY29wZSIsIm5vYWN0aW9uIiwiYWN0aW9uIiwidW5kZWZpbmVkIiwiYWJzVXJsIiwibGluayIsInRvb2x0aXAiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUFjYTs7Ozs7Ozs7QUFFYjtBQUVBO0FBQ0E7QUFDQTs7SUFFTUEsZTtBQUNGLDJCQUFZQyxHQUFaLEVBQWlCO0FBQUE7O0FBQ2IsU0FBS0EsR0FBTCxHQUFXQSxHQUFYO0FBQ0EsU0FBS0MsSUFBTCxHQUFZO0FBQ1JDLFdBQUssRUFBRSxDQURDO0FBRVJDLGNBQVEsRUFBRSxVQUZGO0FBR1JDLFlBQU0sRUFBRTtBQUhBLEtBQVo7QUFLQSxTQUFLQyxPQUFMLEdBQWUsSUFBZjtBQUNBLFNBQUtDLE9BQUw7QUFDSDs7Ozt5QkFFSUwsSSxFQUFNO0FBQ1AsVUFBSUEsSUFBSixFQUFVO0FBQ04sYUFBS0EsSUFBTCxHQUFZTSxDQUFDLENBQUNDLFNBQUYsQ0FBWVAsSUFBWixDQUFaO0FBQ0EsYUFBS0EsSUFBTCxDQUFVQyxLQUFWLEdBQWtCLENBQWxCO0FBQ0g7QUFDSjs7OzJCQUVNO0FBQUE7O0FBQ0gsV0FBS0csT0FBTCxHQUFlLEtBQWY7QUFDQSxXQUFLTCxHQUFMLENBQVNTLE1BQVQsQ0FBZ0JDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixrQkFBakIsQ0FBaEIsRUFBc0QsS0FBS1YsSUFBM0QsRUFBaUVXLElBQWpFLENBQXNFLFVBQUNDLFFBQUQsRUFBYztBQUNoRixZQUFJQSxRQUFRLENBQUNaLElBQVQsQ0FBY2EsT0FBbEIsRUFBMkI7QUFDdkIsY0FBTUMsV0FBVyxHQUFHUixDQUFDLENBQUMsZUFBRCxDQUFyQjtBQUVBLGNBQU1TLE9BQU8sR0FBR0gsUUFBUSxDQUFDWixJQUFULENBQWNnQixHQUFkLENBQWtCRCxPQUFsQztBQUNBRCxxQkFBVyxDQUFDRyxNQUFaLEdBQXFCQyxJQUFyQjtBQUNBSixxQkFBVyxDQUFDSyxHQUFaLENBQWdCLE9BQWhCLEVBQXlCSixPQUFPLEdBQUcsR0FBbkM7O0FBRUEsY0FBSUEsT0FBTyxHQUFHLEdBQWQsRUFBbUI7QUFDZixpQkFBSSxDQUFDZixJQUFMLENBQVVDLEtBQVYsR0FBa0IsQ0FBbEI7O0FBQ0EsaUJBQUksQ0FBQ21CLElBQUw7QUFDSCxXQUhELE1BR087QUFDSEMsa0VBQU0sQ0FBQ0MsZUFBUCxDQUF1QixTQUF2QixFQUFrQyxzQkFBbEM7QUFDSDtBQUNKLFNBYkQsTUFhTztBQUNILGNBQU1DLE1BQU0sR0FBR1gsUUFBUSxDQUFDWixJQUFULENBQWNnQixHQUE3QjtBQUNBSyxnRUFBTSxDQUFDQyxlQUFQLENBQXVCLFFBQXZCLEVBQWlDQyxNQUFNLENBQUNQLEdBQXhDOztBQUNBLGNBQUlPLE1BQU0sQ0FBQ0MsSUFBUCxLQUFnQixLQUFwQixFQUEyQjtBQUN2QixpQkFBSSxDQUFDcEIsT0FBTCxHQUFlLElBQWY7QUFDSDtBQUNKO0FBQ0osT0FyQkQ7QUFzQkg7Ozs7Ozs7Ozs7Ozs7a0JBRVM7QUFBQTs7QUFDTkMsYUFBTyxDQUFDb0IsT0FBUixHQUFrQixnQkFBbEI7QUFDQUMsOERBQU8sQ0FBQ0MsSUFBUixDQUFhO0FBQ1RDLGdCQUFRLEVBQUcsVUFERjtBQUVUQyxjQUFNLEVBQUUsR0FGQztBQUdUQyxhQUFLLEVBQUUsUUFIRTtBQUlUQyxlQUFPLEVBQUUsQ0FDTCw2RUFESyxFQUVMLGtFQUZLLEVBR0wsd0VBSEssRUFJTCx1RUFKSyxDQUpBO0FBVVRDLGdCQUFRLEVBQUUsZ0pBVkQ7QUFXVEMsZ0JBQVEsRUFBRSxxREFYRDtBQVlUQyxhQUFLLEVBQUMsZUFBQ0MsRUFBRCxFQUFRO0FBQ1ZBLFlBQUUsQ0FBQ0MsRUFBSCxDQUFNLFFBQU4sRUFBZ0IsWUFBTTtBQUNsQixrQkFBSSxDQUFDcEMsSUFBTCxDQUFVcUMsSUFBVixHQUFpQkYsRUFBRSxDQUFDRyxVQUFILEVBQWpCO0FBQ0gsV0FGRDtBQUdIO0FBaEJRLE9BQWI7QUFrQkgsSzs7Ozs7O0lBR0NDLHVCO0FBQ0YscUNBQWM7QUFBQTs7QUFDVixTQUFLQyxPQUFMLEdBQWUsU0FBZjtBQUNIOzs7O3lCQUVJQyxNLEVBQVFDLE8sRUFBU0MsSyxFQUFPQyxPLEVBQVM7QUFDbENGLGFBQU8sQ0FBQ0csU0FBUixDQUFrQjtBQUNkQyxrQkFBVSxFQUFFLE9BREU7QUFFZEMsa0JBQVUsRUFBRSxPQUZFO0FBR2RDLG1CQUFXLEVBQUUsT0FIQztBQUlkQyxpQkFBUyxFQUFFLEdBSkc7QUFLZGxCLGVBQU8sRUFBRSxDQUFDLGVBQUQsQ0FMSztBQU1kbUIsZUFBTyxFQUFFLEVBTks7QUFPZEMsY0FBTSxFQUFFLEtBUE07QUFRZEMsWUFBSSxFQUFFLGNBQUNDLEtBQUQsRUFBUUMsUUFBUixFQUFxQjtBQUN2QixjQUFJLENBQUNELEtBQUssQ0FBQ0UsTUFBWCxFQUFtQixPQUFPRCxRQUFRLEVBQWY7QUFDbkJFLGdCQUFNLENBQUNDLElBQVAsQ0FBWTtBQUNSQyxnQkFBSSxFQUFFLE1BREU7QUFFUkMsZUFBRyxFQUFFbEQsT0FBTyxDQUFDQyxRQUFSLENBQWlCLG9CQUFqQixDQUZHO0FBR1JWLGdCQUFJLEVBQUU7QUFDRjRELGtCQUFJLEVBQUVDLGtCQUFrQixDQUFDUixLQUFEO0FBRHRCLGFBSEU7QUFNUlMsaUJBQUssRUFBRSxpQkFBTTtBQUNUUixzQkFBUTtBQUNYLGFBUk87QUFTUnpDLG1CQUFPLEVBQUUsaUJBQUNiLElBQUQsRUFBVTtBQUNmc0Qsc0JBQVEsQ0FBQ3RELElBQUQsQ0FBUjtBQUNIO0FBWE8sV0FBWjtBQWFIO0FBdkJhLE9BQWxCLEVBd0JHb0MsRUF4QkgsQ0F3Qk0sUUF4Qk4sRUF3QmdCLFlBQU07QUFDbEJLLGNBQU0sQ0FBQ3NCLE1BQVAsQ0FBYyxZQUFNO0FBQ2hCLGNBQU1DLFFBQVEsR0FBR3RCLE9BQU8sQ0FBQ0csU0FBUixHQUFvQm9CLEdBQXBCLEVBQWpCO0FBQ0FyQixpQkFBTyxDQUFDc0IsYUFBUixDQUFzQkYsUUFBdEI7QUFDSCxTQUhEO0FBSUgsT0E3QkQ7QUE4Qkg7Ozs7OztBQUdMRyx1REFBRyxDQUNFQyxVQURMLENBQ2dCLGlCQURoQixFQUNtQyxDQUFDLEtBQUQsRUFBUXRFLGVBQVIsQ0FEbkMsRUFFS3VFLFNBRkwsQ0FFZSxnQkFGZixFQUVpQztBQUFBLFNBQU0sSUFBSTlCLHVCQUFKLEVBQU47QUFBQSxDQUZqQyxFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pJQTs7Ozs7Ozs7Ozs7OztBQWNBO0FBQ0E7O0lBRU0rQixHO0FBQ0Y7Ozs7O0FBS0EsZUFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUNmLFNBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNBLFNBQUtDLE9BQUwsR0FBZSxLQUFmO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7OzJCQVFPYixHLEVBQUtjLEssRUFBT0MsSyxFQUFPO0FBQ3RCLFdBQUtGLE9BQUwsR0FBZSxJQUFmOztBQUVBLFVBQUksQ0FBQ0MsS0FBTCxFQUFZO0FBQ1JBLGFBQUssR0FBRyxFQUFSO0FBQ0g7O0FBRUQsVUFBSUUsT0FBTyxHQUFHO0FBQ1Ysd0JBQWdCO0FBRE4sT0FBZDtBQUlBLGFBQU8sS0FBS3ZELElBQUwsQ0FBVSxNQUFWLEVBQWtCdUMsR0FBbEIsRUFBdUJyRCxDQUFDLENBQUNzRSxLQUFGLENBQVFILEtBQVIsQ0FBdkIsRUFBdUNFLE9BQXZDLEVBQWdERCxLQUFoRCxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozt3QkFPSWYsRyxFQUFLZSxLLEVBQU87QUFDWixhQUFPLEtBQUt0RCxJQUFMLENBQVUsS0FBVixFQUFpQnVDLEdBQWpCLEVBQXNCLElBQXRCLEVBQTRCLElBQTVCLEVBQWtDZSxLQUFsQyxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7eUJBUUtmLEcsRUFBS2MsSyxFQUFPQyxLLEVBQU87QUFDcEIsYUFBTyxLQUFLdEQsSUFBTCxDQUFVLE1BQVYsRUFBa0J1QyxHQUFsQixFQUF1QmMsS0FBdkIsRUFBOEIsSUFBOUIsRUFBb0NDLEtBQXBDLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7OzRCQU9PZixHLEVBQUtlLEssRUFBTztBQUNmLGFBQU8sS0FBS3RELElBQUwsQ0FBVSxRQUFWLEVBQW9CdUMsR0FBcEIsRUFBeUIsSUFBekIsRUFBK0IsSUFBL0IsRUFBcUNlLEtBQXJDLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7OzswQkFRTWYsRyxFQUFLYyxLLEVBQU9DLEssRUFBTztBQUNyQixVQUFJQyxPQUFPLEdBQUc7QUFDVix3QkFBZ0IsOEJBRE47QUFFVixrQkFBVTtBQUZBLE9BQWQ7QUFLQSxhQUFPLEtBQUt2RCxJQUFMLENBQVUsT0FBVixFQUFtQnVDLEdBQW5CLEVBQXdCYyxLQUF4QixFQUErQkUsT0FBL0IsRUFBd0NELEtBQXhDLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7O3lCQVVLRyxNLEVBQVFsQixHLEVBQUtjLEssRUFBT0UsTyxFQUFTRCxLLEVBQU87QUFDckMsVUFBSUksR0FBRyxHQUFHLElBQVY7O0FBRUEsVUFBSSxDQUFDTCxLQUFMLEVBQVk7QUFDUkEsYUFBSyxHQUFHLEVBQVI7QUFDSDs7QUFFRCxVQUFJLENBQUNFLE9BQUwsRUFBYztBQUNWQSxlQUFPLEdBQUc7QUFDTiwwQkFBZ0Isa0JBRFY7QUFFTixvQkFBVTtBQUZKLFNBQVY7QUFJSDs7QUFFRCxVQUFJRCxLQUFKLEVBQVc7QUFDUEksV0FBRyxHQUFHeEUsQ0FBQyxDQUFDb0UsS0FBSyxDQUFDSyxhQUFQLENBQVA7QUFDQUQsV0FBRyxDQUFDRSxRQUFKLENBQWEsU0FBYjtBQUNBRixXQUFHLENBQUNHLElBQUosQ0FBUyxVQUFULEVBQXFCLElBQXJCO0FBQ0g7O0FBRUQsYUFBTyxLQUFLVixLQUFMLENBQVc7QUFDZE0sY0FBTSxFQUFFQSxNQURNO0FBRWRsQixXQUFHLEVBQUVBLEdBRlM7QUFHZDNELFlBQUksRUFBRXlFLEtBSFE7QUFJZEUsZUFBTyxFQUFFQTtBQUpLLE9BQVgsV0FLRSxVQUFDYixLQUFELEVBQVc7QUFDaEIsWUFBSUEsS0FBSyxDQUFDOUQsSUFBTixDQUFXa0YsTUFBZixFQUF1QjtBQUNuQjdELHlEQUFNLENBQUNDLGVBQVAsQ0FBdUIsUUFBdkIsRUFBaUN3QyxLQUFLLENBQUM5RCxJQUFOLENBQVdrRixNQUE1QztBQUNILFNBRkQsTUFFTztBQUNIN0QseURBQU0sQ0FBQ0MsZUFBUCxDQUF1QixRQUF2QixFQUFpQyw0QkFBakM7QUFDSDtBQUNKLE9BWE0sYUFXSSxZQUFNO0FBQ2JoQixTQUFDLENBQUM2RSxRQUFELENBQUQsQ0FBWUMsT0FBWixDQUFvQixjQUFwQjs7QUFDQSxZQUFJTixHQUFKLEVBQVM7QUFDTCxjQUFNTyxLQUFLLEdBQUdQLEdBQUcsQ0FBQzlFLElBQUosQ0FBUyxPQUFULENBQWQ7O0FBQ0EsY0FBSXFGLEtBQUosRUFBVztBQUNQL0UsYUFBQyxDQUFDLE1BQU0rRSxLQUFQLENBQUQsQ0FBZUEsS0FBZixDQUFxQixNQUFyQjtBQUNIOztBQUVEUCxhQUFHLENBQUNRLFdBQUosQ0FBZ0IsU0FBaEI7QUFDQVIsYUFBRyxDQUFDRyxJQUFKLENBQVMsVUFBVCxFQUFxQixLQUFyQjtBQUNIO0FBQ0osT0F0Qk0sQ0FBUDtBQXVCSDs7Ozs7O0FBR0wsSUFBTWQsR0FBRyxHQUFHb0IsOENBQU8sQ0FBQ0MsTUFBUixDQUFlLFdBQWYsRUFBNEIsRUFBNUIsRUFDUjtBQURRLENBRVBDLE1BRk8sQ0FFQSxDQUFDLHNCQUFELEVBQXlCLG1CQUF6QixFQUE4QyxVQUFDQyxvQkFBRCxFQUF1QkMsaUJBQXZCLEVBQTZDO0FBQy9GRCxzQkFBb0IsQ0FBQ0UsV0FBckIsQ0FBaUMsS0FBakMsRUFBd0NDLFNBQXhDLENBQWtELEtBQWxEO0FBQ0FGLG1CQUFpQixDQUFDRyxTQUFsQixDQUE0QjtBQUN4QjFGLFdBQU8sRUFBRSxJQURlO0FBRXhCMkYsZUFBVyxFQUFFLEtBRlc7QUFHeEJDLGdCQUFZLEVBQUU7QUFIVSxHQUE1QjtBQUtILENBUE8sQ0FGQSxFQVVQQyxNQVZPLENBVUEsUUFWQSxFQVVVLENBQUMsTUFBRCxFQUFTLFVBQUNDLElBQUQsRUFBVTtBQUNqQyxTQUFPLFVBQUNqQyxHQUFELEVBQVM7QUFDWixXQUFPaUMsSUFBSSxDQUFDQyxXQUFMLENBQWlCbEMsR0FBakIsQ0FBUDtBQUNILEdBRkQ7QUFHSCxDQUppQixDQVZWLEVBZVBnQyxNQWZPLENBZUEsT0FmQSxFQWVTLFlBQU07QUFDbkIsU0FBTyxVQUFDeEIsS0FBRCxFQUFRMkIsS0FBUixFQUFrQjtBQUNyQkEsU0FBSyxHQUFHQyxRQUFRLENBQUNELEtBQUQsQ0FBaEI7O0FBRUEsU0FBSyxJQUFJRSxDQUFDLEdBQUMsQ0FBWCxFQUFjQSxDQUFDLEdBQUNGLEtBQWhCLEVBQXVCRSxDQUFDLEVBQXhCLEVBQTRCO0FBQ3hCN0IsV0FBSyxDQUFDOEIsSUFBTixDQUFXRCxDQUFYO0FBQ0g7O0FBRUQsV0FBTzdCLEtBQVA7QUFDSCxHQVJEO0FBU0gsQ0F6Qk8sRUEwQlArQixPQTFCTyxDQTBCQyxLQTFCRCxFQTBCUSxDQUFDLE9BQUQsRUFBVWxDLEdBQVYsQ0ExQlIsRUEyQlBELFNBM0JPLENBMkJHLE1BM0JILEVBMkJXLENBQUMsV0FBRCxFQUFjLFVBQUNvQyxTQUFELEVBQWU7QUFDNUMsU0FBTztBQUNIQyxZQUFRLEVBQUMsR0FETjtBQUVIQyxZQUFRLEVBQUUsR0FGUDtBQUdIQyxXQUFPLEVBQUUsbUJBQU07QUFDWCxhQUFPO0FBQ0hDLFdBQUcsRUFBRSxhQUFDQyxLQUFELEVBQVFwRSxPQUFSLEVBQWlCQyxLQUFqQixFQUEyQjtBQUM1QixjQUFJQSxLQUFLLENBQUNvRSxRQUFOLEtBQW1CLEVBQXZCLEVBQTJCOztBQUMzQixjQUFJcEUsS0FBSyxDQUFDcUUsTUFBTixLQUFpQkMsU0FBakIsSUFBOEJ0RSxLQUFLLENBQUNxRSxNQUFOLEtBQWlCLEVBQW5ELEVBQXNEO0FBQ2xEckUsaUJBQUssQ0FBQ3FFLE1BQU4sR0FBZVAsU0FBUyxDQUFDUyxNQUFWLEVBQWY7QUFDSDtBQUNKO0FBTkUsT0FBUDtBQVFIO0FBWkUsR0FBUDtBQWNILENBZmtCLENBM0JYLEVBMkNQN0MsU0EzQ08sQ0EyQ0csYUEzQ0gsRUEyQ2tCLFlBQU07QUFDNUIsU0FBTztBQUNIcUMsWUFBUSxFQUFFLEdBRFA7QUFFSFMsUUFBSSxFQUFFLGNBQUNMLEtBQUQsRUFBUXBFLE9BQVIsRUFBaUJDLEtBQWpCLEVBQTJCO0FBQzdCckMsT0FBQyxDQUFDb0MsT0FBRCxDQUFELENBQVcwRSxPQUFYLENBQW1CO0FBQ2ZoQyxlQUFPLEVBQUc7QUFESyxPQUFuQjtBQUdIO0FBTkUsR0FBUDtBQVFILENBcERPLENBQVo7QUF1RGVqQixrRUFBZixFOzs7Ozs7Ozs7Ozs7O0FDcE5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQWNhOztBQUViO0FBQ0E7QUFFZXpDLDZHQUFmLEUiLCJmaWxlIjoiQWRtaW4vRW1haWxDdHJsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKiAgQ29weXJpZ2h0IG5vdGljZVxyXG4gKlxyXG4gKiAgKGMpIDIwMTggRlRLIGUuVi4gPGZ0ay5kZT5cclxuICogIEFsbCByaWdodHMgcmVzZXJ2ZWRcclxuICpcclxuICogIFRoaXMgc2NyaXB0IGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAqICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gKiAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gKiAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuICpcclxuICogIFRoaXMgY29weXJpZ2h0IG5vdGljZSBNVVNUIEFQUEVBUiBpbiBhbGwgY29waWVzIG9mIHRoZSBzY3JpcHQhXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG4ndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgJy4uL0xpYnJhcnkvU2VsZWN0aXplJztcclxuXHJcbmltcG9ydCBhcHAgZnJvbSAnLi4vQ29tbW9uL0FuZ3VsYXInO1xyXG5pbXBvcnQgQ29tbW9uIGZyb20gJy4uL0NvbW1vbi9Db21tb24nXHJcbmltcG9ydCB0aW55bWNlIGZyb20gJy4uL0xpYnJhcnkvVGlueU1DRSc7XHJcblxyXG5jbGFzcyBFbWFpbENvbnRyb2xsZXIge1xyXG4gICAgY29uc3RydWN0b3IoZWlkKSB7XHJcbiAgICAgICAgdGhpcy5laWQgPSBlaWQ7XHJcbiAgICAgICAgdGhpcy5kYXRhID0ge1xyXG4gICAgICAgICAgICByZXNldDogMSxcclxuICAgICAgICAgICAgcmVjZWl2ZXI6ICdFdmVyeW9uZScsXHJcbiAgICAgICAgICAgIGN1c3RvbTogJycsXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmVuYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMudGlueU1DRSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXQoZGF0YSkge1xyXG4gICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGF0YSA9ICQucGFyc2VKU09OKGRhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLmRhdGEucmVzZXQgPSAwO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZW5kKCkge1xyXG4gICAgICAgIHRoaXMuZW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZWlkLmNoYW5nZShSb3V0aW5nLmdlbmVyYXRlKCdhZG1pbl9lbWFpbF9zZW5kJyksIHRoaXMuZGF0YSkudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmRhdGEuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcHJvZ3Jlc3NCYXIgPSAkKCcjcHJvZ3Jlc3MtYmFyJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgcGVyY2VudCA9IHJlc3BvbnNlLmRhdGEubXNnLnBlcmNlbnQ7XHJcbiAgICAgICAgICAgICAgICBwcm9ncmVzc0Jhci5wYXJlbnQoKS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICBwcm9ncmVzc0Jhci5jc3MoJ3dpZHRoJywgcGVyY2VudCArICclJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHBlcmNlbnQgPCAxMDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGEucmVzZXQgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VuZCgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBDb21tb24uc2hvd0FsZXJ0RGlhbG9nKCdzdWNjZXNzJywgJ0FsbCBlbWFpbHMgd2VyZSBzZW50Jyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZXN1bHQgPSByZXNwb25zZS5kYXRhLm1zZztcclxuICAgICAgICAgICAgICAgIENvbW1vbi5zaG93QWxlcnREaWFsb2coJ2RhbmdlcicsIHJlc3VsdC5tc2cpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdC5zdG9wID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZW5hYmxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB0aW55TUNFKCkge1xyXG4gICAgICAgIHRpbnlNQ0UuYmFzZVVSTCA9IFwiL2J1aWxkL3RpbnltY2VcIjtcclxuICAgICAgICB0aW55bWNlLmluaXQoe1xyXG4gICAgICAgICAgICBzZWxlY3RvcjogICcudGlueW1jZScsXHJcbiAgICAgICAgICAgIGhlaWdodDogMjAwLFxyXG4gICAgICAgICAgICB0aGVtZTogJ21vZGVybicsXHJcbiAgICAgICAgICAgIHBsdWdpbnM6IFtcclxuICAgICAgICAgICAgICAgICdhZHZsaXN0IGF1dG9saW5rIGxpc3RzIGxpbmsgaW1hZ2UgY2hhcm1hcCBwcmludCBwcmV2aWV3IGhyIGFuY2hvciBwYWdlYnJlYWsnLFxyXG4gICAgICAgICAgICAgICAgJ3NlYXJjaHJlcGxhY2Ugd29yZGNvdW50IHZpc3VhbGJsb2NrcyB2aXN1YWxjaGFycyBjb2RlIGZ1bGxzY3JlZW4nLFxyXG4gICAgICAgICAgICAgICAgJ2luc2VydGRhdGV0aW1lIG1lZGlhIG5vbmJyZWFraW5nIHNhdmUgdGFibGUgY29udGV4dG1lbnUgZGlyZWN0aW9uYWxpdHknLFxyXG4gICAgICAgICAgICAgICAgJ2Vtb3RpY29ucyB0ZW1wbGF0ZSBwYXN0ZSB0ZXh0Y29sb3IgY29sb3JwaWNrZXIgdGV4dHBhdHRlcm4gaW1hZ2V0b29scydcclxuICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgdG9vbGJhcjE6ICdpbnNlcnRmaWxlIHVuZG8gcmVkbyB8IHN0eWxlc2VsZWN0IHwgYm9sZCBpdGFsaWMgfCBhbGlnbmxlZnQgYWxpZ25jZW50ZXIgYWxpZ25yaWdodCBhbGlnbmp1c3RpZnkgfCBidWxsaXN0IG51bWxpc3Qgb3V0ZGVudCBpbmRlbnQgfCBsaW5rIGltYWdlJyxcclxuICAgICAgICAgICAgdG9vbGJhcjI6ICdwcmludCBwcmV2aWV3IG1lZGlhIHwgZm9yZWNvbG9yIGJhY2tjb2xvciBlbW90aWNvbnMnLFxyXG4gICAgICAgICAgICBzZXR1cDooZWQpID0+IHtcclxuICAgICAgICAgICAgICAgIGVkLm9uKCdjaGFuZ2UnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRhLmJvZHkgPSBlZC5nZXRDb250ZW50KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG5jbGFzcyBzZWxlY3RpemVGaWVsZERpcmVjdGl2ZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLnJlcXVpcmUgPSBcIm5nTW9kZWxcIjtcclxuICAgIH1cclxuXHJcbiAgICBsaW5rKCRzY29wZSwgZWxlbWVudCwgYXR0cnMsIG5nTW9kZWwpIHtcclxuICAgICAgICBlbGVtZW50LnNlbGVjdGl6ZSh7XHJcbiAgICAgICAgICAgIHZhbHVlRmllbGQ6ICd0aXRsZScsXHJcbiAgICAgICAgICAgIGxhYmVsRmllbGQ6ICd0aXRsZScsXHJcbiAgICAgICAgICAgIHNlYXJjaEZpZWxkOiAndGl0bGUnLFxyXG4gICAgICAgICAgICBkZWxpbWl0ZXI6ICcsJyxcclxuICAgICAgICAgICAgcGx1Z2luczogWydyZW1vdmVfYnV0dG9uJ10sXHJcbiAgICAgICAgICAgIG9wdGlvbnM6IFtdLFxyXG4gICAgICAgICAgICBjcmVhdGU6IGZhbHNlLFxyXG4gICAgICAgICAgICBsb2FkOiAocXVlcnksIGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXF1ZXJ5Lmxlbmd0aCkgcmV0dXJuIGNhbGxiYWNrKCk7XHJcbiAgICAgICAgICAgICAgICBqUXVlcnkuYWpheCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJQT1NUXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsOiBSb3V0aW5nLmdlbmVyYXRlKCdhZG1pbl9lbWFpbF9zZWFyY2gnKSxcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlcm06IGVuY29kZVVSSUNvbXBvbmVudChxdWVyeSlcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLm9uKCdjaGFuZ2UnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICRzY29wZS4kYXBwbHkoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbmV3VmFsdWUgPSBlbGVtZW50LnNlbGVjdGl6ZSgpLnZhbCgpO1xyXG4gICAgICAgICAgICAgICAgbmdNb2RlbC4kc2V0Vmlld1ZhbHVlKG5ld1ZhbHVlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmFwcFxyXG4gICAgLmNvbnRyb2xsZXIoJ0VtYWlsQ29udHJvbGxlcicsIFsnZWlkJywgRW1haWxDb250cm9sbGVyXSlcclxuICAgIC5kaXJlY3RpdmUoJ3NlbGVjdGl6ZUZpZWxkJywgKCkgPT4gbmV3IHNlbGVjdGl6ZUZpZWxkRGlyZWN0aXZlKTsiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbmltcG9ydCBhbmd1bGFyIGZyb20gJ2FuZ3VsYXInO1xyXG5pbXBvcnQgQ29tbW9uIGZyb20gJy4vQ29tbW9uJztcclxuXHJcbmNsYXNzIEVpZCB7XHJcbiAgICAvKipcclxuICAgICAqIEluaXRpYXRlXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtICRodHRwXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKCRodHRwKSB7XHJcbiAgICAgICAgdGhpcy4kaHR0cCA9ICRodHRwO1xyXG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogc2VuZCBub3JtYWwgUE9TVFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB1cmxcclxuICAgICAqIEBwYXJhbSBpbnB1dFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+IHwgUHJvbWlzZTxUPiB8ICp9XHJcbiAgICAgKi9cclxuICAgIGNoYW5nZSh1cmwsIGlucHV0LCBldmVudCkge1xyXG4gICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XHJcblxyXG4gICAgICAgIGlmICghaW5wdXQpIHtcclxuICAgICAgICAgICAgaW5wdXQgPSB7fTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBoZWFkZXJzID0ge1xyXG4gICAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCdcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5zZW5kKCdQT1NUJywgdXJsLCAkLnBhcmFtKGlucHV0KSwgaGVhZGVycywgZXZlbnQpXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIGEgR0VUIHJlcXVlc3RcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT58UHJvbWlzZTxUPnwqfVxyXG4gICAgICovXHJcbiAgICBnZXQodXJsLCBldmVudCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbmQoJ0dFVCcsIHVybCwgbnVsbCwgbnVsbCwgZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBhIFBPU1QgcmVxdWVzdCB3aXRoIEpTT04gZGF0YVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB1cmxcclxuICAgICAqIEBwYXJhbSBpbnB1dFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fFByb21pc2U8VD58Kn1cclxuICAgICAqL1xyXG4gICAgcG9zdCh1cmwsIGlucHV0LCBldmVudCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbmQoJ1BPU1QnLCB1cmwsIGlucHV0LCBudWxsLCBldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIGEgREVMRVRFIHJlcXVlc3RcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT58UHJvbWlzZTxUPnwqfVxyXG4gICAgICovXHJcbiAgICBkZWxldGUodXJsLCBldmVudCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbmQoJ0RFTEVURScsIHVybCwgbnVsbCwgbnVsbCwgZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBhIFBBVENIIHJlcXVlc3Qgd2l0aCBKU09OIGRhdGFcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gaW5wdXRcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PnxQcm9taXNlPFQ+fCp9XHJcbiAgICAgKi9cclxuICAgIHBhdGNoKHVybCwgaW5wdXQsIGV2ZW50KSB7XHJcbiAgICAgICAgbGV0IGhlYWRlcnMgPSB7XHJcbiAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vbWVyZ2UtcGF0Y2granNvbicsXHJcbiAgICAgICAgICAgICdhY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VuZCgnUEFUQ0gnLCB1cmwsIGlucHV0LCBoZWFkZXJzLCBldmVudClcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbmQgYSByZXF1ZXN0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIG1ldGhvZFxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGlucHV0XHJcbiAgICAgKiBAcGFyYW0gaGVhZGVyc1xyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+IHwgUHJvbWlzZTxUPiB8ICp9XHJcbiAgICAgKi9cclxuICAgIHNlbmQobWV0aG9kLCB1cmwsIGlucHV0LCBoZWFkZXJzLCBldmVudCkge1xyXG4gICAgICAgIGxldCBidG4gPSBudWxsO1xyXG5cclxuICAgICAgICBpZiAoIWlucHV0KSB7XHJcbiAgICAgICAgICAgIGlucHV0ID0ge307XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWhlYWRlcnMpIHtcclxuICAgICAgICAgICAgaGVhZGVycyA9IHtcclxuICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXHJcbiAgICAgICAgICAgICAgICAnYWNjZXB0JzogJ2FwcGxpY2F0aW9uL2xkK2pzb24nLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIGJ0biA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XHJcbiAgICAgICAgICAgIGJ0bi5hZGRDbGFzcygncnVubmluZycpO1xyXG4gICAgICAgICAgICBidG4ucHJvcChcImRpc2FibGVkXCIsIHRydWUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuJGh0dHAoe1xyXG4gICAgICAgICAgICBtZXRob2Q6IG1ldGhvZCxcclxuICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgIGRhdGE6IGlucHV0LFxyXG4gICAgICAgICAgICBoZWFkZXJzOiBoZWFkZXJzXHJcbiAgICAgICAgfSkuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChlcnJvci5kYXRhLmRldGFpbCkge1xyXG4gICAgICAgICAgICAgICAgQ29tbW9uLnNob3dBbGVydERpYWxvZygnZGFuZ2VyJywgZXJyb3IuZGF0YS5kZXRhaWwpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgQ29tbW9uLnNob3dBbGVydERpYWxvZygnZGFuZ2VyJywgJ0FjdGlvbiBjYW5ub3QgYmUgcGVyZm9ybWVkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgJChkb2N1bWVudCkudHJpZ2dlcignbG9hZGluZy5zdG9wJyk7XHJcbiAgICAgICAgICAgIGlmIChidG4pIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1vZGFsID0gYnRuLmRhdGEoJ2Nsb3NlJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAobW9kYWwpIHtcclxuICAgICAgICAgICAgICAgICAgICAkKCcjJyArIG1vZGFsKS5tb2RhbCgnaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGJ0bi5yZW1vdmVDbGFzcygncnVubmluZycpO1xyXG4gICAgICAgICAgICAgICAgYnRuLnByb3AoXCJkaXNhYmxlZFwiLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG5cclxuY29uc3QgYXBwID0gYW5ndWxhci5tb2R1bGUoJ0Vjb3N5c3RlbScsIFtdKVxyXG4gICAgLy8gY2hhbmdlIEFuZ3VsYXIgZGVmYXVsdCB7eyB0byB7W3sgdG8gYXZvaWQgY29uZmxpY3Qgd2l0aCBUd2lnXHJcbiAgICAuY29uZmlnKFsnJGludGVycG9sYXRlUHJvdmlkZXInLCAnJGxvY2F0aW9uUHJvdmlkZXInLCAoJGludGVycG9sYXRlUHJvdmlkZXIsICRsb2NhdGlvblByb3ZpZGVyKSA9PiB7XHJcbiAgICAgICAgJGludGVycG9sYXRlUHJvdmlkZXIuc3RhcnRTeW1ib2woJ3tbeycpLmVuZFN5bWJvbCgnfV19Jyk7XHJcbiAgICAgICAgJGxvY2F0aW9uUHJvdmlkZXIuaHRtbDVNb2RlKHtcclxuICAgICAgICAgICAgZW5hYmxlZDogdHJ1ZSxcclxuICAgICAgICAgICAgcmVxdWlyZUJhc2U6IGZhbHNlLFxyXG4gICAgICAgICAgICByZXdyaXRlTGlua3M6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XSlcclxuICAgIC5maWx0ZXIoJ3Vuc2FmZScsIFsnJHNjZScsICgkc2NlKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuICh2YWwpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuICRzY2UudHJ1c3RBc0h0bWwodmFsKTtcclxuICAgICAgICB9O1xyXG4gICAgfV0pXHJcbiAgICAuZmlsdGVyKCdyYW5nZScsICgpID0+IHtcclxuICAgICAgICByZXR1cm4gKGlucHV0LCB0b3RhbCkgPT4ge1xyXG4gICAgICAgICAgICB0b3RhbCA9IHBhcnNlSW50KHRvdGFsKTtcclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGk9MDsgaTx0b3RhbDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBpbnB1dC5wdXNoKGkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gaW5wdXQ7XHJcbiAgICAgICAgfTtcclxuICAgIH0pXHJcbiAgICAuc2VydmljZSgnZWlkJywgWyckaHR0cCcsIEVpZF0pXHJcbiAgICAuZGlyZWN0aXZlKCdmb3JtJywgWyckbG9jYXRpb24nLCAoJGxvY2F0aW9uKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcmVzdHJpY3Q6J0UnLFxyXG4gICAgICAgICAgICBwcmlvcml0eTogOTk5LFxyXG4gICAgICAgICAgICBjb21waWxlOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHByZTogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXR0cnMubm9hY3Rpb24gPT09ICcnKSByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhdHRycy5hY3Rpb24gPT09IHVuZGVmaW5lZCB8fCBhdHRycy5hY3Rpb24gPT09ICcnKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzLmFjdGlvbiA9ICRsb2NhdGlvbi5hYnNVcmwoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1dKVxyXG4gICAgLmRpcmVjdGl2ZSgnaW5pdFRvb2x0aXAnLCAoKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcmVzdHJpY3Q6ICdBJyxcclxuICAgICAgICAgICAgbGluazogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgPT4ge1xyXG4gICAgICAgICAgICAgICAgJChlbGVtZW50KS50b29sdGlwKHtcclxuICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyIDogJ2hvdmVyJ1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfSlcclxuO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgYXBwOyIsIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuICogIENvcHlyaWdodCBub3RpY2VcclxuICpcclxuICogIChjKSAyMDE4IEZUSyBlLlYuIDxmdGsuZGU+XHJcbiAqICBBbGwgcmlnaHRzIHJlc2VydmVkXHJcbiAqXHJcbiAqICBUaGlzIHNjcmlwdCBpcyBkaXN0cmlidXRlZCBpbiB0aGUgaG9wZSB0aGF0IGl0IHdpbGwgYmUgdXNlZnVsLFxyXG4gKiAgYnV0IFdJVEhPVVQgQU5ZIFdBUlJBTlRZOyB3aXRob3V0IGV2ZW4gdGhlIGltcGxpZWQgd2FycmFudHkgb2ZcclxuICogIE1FUkNIQU5UQUJJTElUWSBvciBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRS4gIFNlZSB0aGVcclxuICogIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGZvciBtb3JlIGRldGFpbHMuXHJcbiAqXHJcbiAqICBUaGlzIGNvcHlyaWdodCBub3RpY2UgTVVTVCBBUFBFQVIgaW4gYWxsIGNvcGllcyBvZiB0aGUgc2NyaXB0IVxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxuaW1wb3J0IHRpbnltY2UgZnJvbSAndGlueW1jZSc7XHJcbmltcG9ydCAndGlueW1jZS90aGVtZXMvbW9kZXJuL3RoZW1lJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHRpbnltY2U7Il0sInNvdXJjZVJvb3QiOiIifQ==