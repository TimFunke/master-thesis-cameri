(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Admin/FileAuditCtrl"],{

/***/ "./assets/js/Admin/FileAuditCtrl.js":
/*!******************************************!*\
  !*** ./assets/js/Admin/FileAuditCtrl.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _Library_DataTables__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Library/DataTables */ "./assets/js/Library/DataTables.js");
/* harmony import */ var _Common_Angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Common/Angular */ "./assets/js/Common/Angular.js");
/* harmony import */ var _Common_Common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Common/Common */ "./assets/js/Common/Common.js");
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }





var FileAuditController = /*#__PURE__*/function () {
  function FileAuditController(eid) {
    _classCallCheck(this, FileAuditController);

    this.eid = eid;
    this.startDate = '';
    this.endDate = '';
    this.data = {};
  }

  _createClass(FileAuditController, [{
    key: "init",
    value: function init() {
      var _this = this;

      $(document).trigger('loading.start');
      this.eid.change(Routing.generate('admin_fileaudit_init')).then(function (response) {
        if (response.data.success) {
          ['startDate', 'endDate'].forEach(function (name) {
            _this[name] = response.data.msg[name];
          });

          _this.viewReport();
        } else {
          _Common_Common__WEBPACK_IMPORTED_MODULE_2__["default"].showAlertDialog('danger', response.data.msg);
        }
      });
    }
  }, {
    key: "viewReport",
    value: function viewReport() {
      var _this2 = this;

      if (!this.startDate || !this.endDate) {
        _Common_Common__WEBPACK_IMPORTED_MODULE_2__["default"].showAlertDialog('danger', 'Please input audit\'s date range');
        return;
      }

      var url = Routing.generate('admin_fileaudit_view', {
        begin: this.startDate,
        end: this.endDate
      });
      $(document).trigger('loading.start');
      this.eid.change(url).then(function (response) {
        if (response.data.success) {
          ['data'].forEach(function (name) {
            _this2[name] = response.data.msg[name];
          });
        } else {
          _Common_Common__WEBPACK_IMPORTED_MODULE_2__["default"].showAlertDialog('danger', response.data.msg);
        }
      });
    }
  }]);

  return FileAuditController;
}();

var LogTableDirective = /*#__PURE__*/function () {
  function LogTableDirective() {
    _classCallCheck(this, LogTableDirective);
  }

  _createClass(LogTableDirective, [{
    key: "link",
    value: function link($scope, element) {
      var dataTable = element.DataTable({
        "language": {
          search: "<span class='fas fa-search'></span> _INPUT_"
        },
        "order": [[0, "desc"]]
      });
      $scope.$watch('audit.data', function (value) {
        if (value) {
          dataTable.clear();
          dataTable.rows.add(value);
          dataTable.draw();
        }
      }, true);
    }
  }]);

  return LogTableDirective;
}();

_Common_Angular__WEBPACK_IMPORTED_MODULE_1__["default"].controller('FileAuditController', ['eid', FileAuditController]).directive('logTable', function () {
  return new LogTableDirective();
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/Common/Angular.js":
/*!*************************************!*\
  !*** ./assets/js/Common/Angular.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! angular */ "./node_modules/angular/index.js");
/* harmony import */ var angular__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(angular__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Common */ "./assets/js/Common/Common.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/



var Eid = /*#__PURE__*/function () {
  /**
   * Initiate
   *
   * @param $http
   */
  function Eid($http) {
    _classCallCheck(this, Eid);

    this.$http = $http;
    this.loading = false;
  }
  /**
   * send normal POST
   *
   * @param url
   * @param input
   * @param event
   * @returns {Promise<any> | Promise<T> | *}
   */


  _createClass(Eid, [{
    key: "change",
    value: function change(url, input, event) {
      this.loading = true;

      if (!input) {
        input = {};
      }

      var headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      return this.send('POST', url, $.param(input), headers, event);
    }
    /**
     * Send a GET request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "get",
    value: function get(url, event) {
      return this.send('GET', url, null, null, event);
    }
    /**
     * Send a POST request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "post",
    value: function post(url, input, event) {
      return this.send('POST', url, input, null, event);
    }
    /**
     * Send a DELETE request
     *
     * @param url
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "delete",
    value: function _delete(url, event) {
      return this.send('DELETE', url, null, null, event);
    }
    /**
     * Send a PATCH request with JSON data
     *
     * @param url
     * @param input
     * @param event
     * @returns {Promise<any>|Promise<T>|*}
     */

  }, {
    key: "patch",
    value: function patch(url, input, event) {
      var headers = {
        'Content-Type': 'application/merge-patch+json',
        'accept': 'application/json'
      };
      return this.send('PATCH', url, input, headers, event);
    }
    /**
     * Send a request
     *
     * @param method
     * @param url
     * @param input
     * @param headers
     * @param event
     * @returns {Promise<any> | Promise<T> | *}
     */

  }, {
    key: "send",
    value: function send(method, url, input, headers, event) {
      var btn = null;

      if (!input) {
        input = {};
      }

      if (!headers) {
        headers = {
          'Content-Type': 'application/json',
          'accept': 'application/ld+json'
        };
      }

      if (event) {
        btn = $(event.currentTarget);
        btn.addClass('running');
        btn.prop("disabled", true);
      }

      return this.$http({
        method: method,
        url: url,
        data: input,
        headers: headers
      })["catch"](function (error) {
        if (error.data.detail) {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', error.data.detail);
        } else {
          _Common__WEBPACK_IMPORTED_MODULE_1__["default"].showAlertDialog('danger', 'Action cannot be performed');
        }
      })["finally"](function () {
        $(document).trigger('loading.stop');

        if (btn) {
          var modal = btn.data('close');

          if (modal) {
            $('#' + modal).modal('hide');
          }

          btn.removeClass('running');
          btn.prop("disabled", false);
        }
      });
    }
  }]);

  return Eid;
}();

var app = angular__WEBPACK_IMPORTED_MODULE_0___default.a.module('Ecosystem', []) // change Angular default {{ to {[{ to avoid conflict with Twig
.config(['$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false,
    rewriteLinks: false
  });
}]).filter('unsafe', ['$sce', function ($sce) {
  return function (val) {
    return $sce.trustAsHtml(val);
  };
}]).filter('range', function () {
  return function (input, total) {
    total = parseInt(total);

    for (var i = 0; i < total; i++) {
      input.push(i);
    }

    return input;
  };
}).service('eid', ['$http', Eid]).directive('form', ['$location', function ($location) {
  return {
    restrict: 'E',
    priority: 999,
    compile: function compile() {
      return {
        pre: function pre(scope, element, attrs) {
          if (attrs.noaction === '') return;

          if (attrs.action === undefined || attrs.action === '') {
            attrs.action = $location.absUrl();
          }
        }
      };
    }
  };
}]).directive('initTooltip', function () {
  return {
    restrict: 'A',
    link: function link(scope, element, attrs) {
      $(element).tooltip({
        trigger: 'hover'
      });
    }
  };
});
/* harmony default export */ __webpack_exports__["default"] = (app);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/Library/DataTables.js":
/*!*****************************************!*\
  !*** ./assets/js/Library/DataTables.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var datatables_net__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! datatables.net */ "./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var datatables_net__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(datatables_net__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var datatables_net_bs4_css_dataTables_bootstrap4_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! datatables.net-bs4/css/dataTables.bootstrap4.min.css */ "./node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css");
/* harmony import */ var datatables_net_bs4_css_dataTables_bootstrap4_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(datatables_net_bs4_css_dataTables_bootstrap4_min_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var datatables_net_bs4_js_dataTables_bootstrap4_min_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! datatables.net-bs4/js/dataTables.bootstrap4.min.js */ "./node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js");
/* harmony import */ var datatables_net_bs4_js_dataTables_bootstrap4_min_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(datatables_net_bs4_js_dataTables_bootstrap4_min_js__WEBPACK_IMPORTED_MODULE_2__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/






/***/ })

},[["./assets/js/Admin/FileAuditCtrl.js","runtime","vendors~Admin/EmailCtrl~Admin/FileAudit~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Recor~b3f38238","vendors~Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/SearchCtrl~BigData/Se~6c26450f","vendors~Admin/FileAuditCtrl~Archive/Record~Authentication/User~Authentication/UserGroup~Content/Cont~c2893b9d","Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Record~Archive/SearchCtrl~Aut~d45f3fc6"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQWRtaW4vRmlsZUF1ZGl0Q3RybC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQ29tbW9uL0FuZ3VsYXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL0xpYnJhcnkvRGF0YVRhYmxlcy5qcyJdLCJuYW1lcyI6WyJGaWxlQXVkaXRDb250cm9sbGVyIiwiZWlkIiwic3RhcnREYXRlIiwiZW5kRGF0ZSIsImRhdGEiLCIkIiwiZG9jdW1lbnQiLCJ0cmlnZ2VyIiwiY2hhbmdlIiwiUm91dGluZyIsImdlbmVyYXRlIiwidGhlbiIsInJlc3BvbnNlIiwic3VjY2VzcyIsImZvckVhY2giLCJuYW1lIiwibXNnIiwidmlld1JlcG9ydCIsIkNvbW1vbiIsInNob3dBbGVydERpYWxvZyIsInVybCIsImJlZ2luIiwiZW5kIiwiTG9nVGFibGVEaXJlY3RpdmUiLCIkc2NvcGUiLCJlbGVtZW50IiwiZGF0YVRhYmxlIiwiRGF0YVRhYmxlIiwic2VhcmNoIiwiJHdhdGNoIiwidmFsdWUiLCJjbGVhciIsInJvd3MiLCJhZGQiLCJkcmF3IiwiYXBwIiwiY29udHJvbGxlciIsImRpcmVjdGl2ZSIsIkVpZCIsIiRodHRwIiwibG9hZGluZyIsImlucHV0IiwiZXZlbnQiLCJoZWFkZXJzIiwic2VuZCIsInBhcmFtIiwibWV0aG9kIiwiYnRuIiwiY3VycmVudFRhcmdldCIsImFkZENsYXNzIiwicHJvcCIsImVycm9yIiwiZGV0YWlsIiwibW9kYWwiLCJyZW1vdmVDbGFzcyIsImFuZ3VsYXIiLCJtb2R1bGUiLCJjb25maWciLCIkaW50ZXJwb2xhdGVQcm92aWRlciIsIiRsb2NhdGlvblByb3ZpZGVyIiwic3RhcnRTeW1ib2wiLCJlbmRTeW1ib2wiLCJodG1sNU1vZGUiLCJlbmFibGVkIiwicmVxdWlyZUJhc2UiLCJyZXdyaXRlTGlua3MiLCJmaWx0ZXIiLCIkc2NlIiwidmFsIiwidHJ1c3RBc0h0bWwiLCJ0b3RhbCIsInBhcnNlSW50IiwiaSIsInB1c2giLCJzZXJ2aWNlIiwiJGxvY2F0aW9uIiwicmVzdHJpY3QiLCJwcmlvcml0eSIsImNvbXBpbGUiLCJwcmUiLCJzY29wZSIsImF0dHJzIiwibm9hY3Rpb24iLCJhY3Rpb24iLCJ1bmRlZmluZWQiLCJhYnNVcmwiLCJsaW5rIiwidG9vbHRpcCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUFjYTs7Ozs7Ozs7QUFFYjtBQUVBO0FBQ0E7O0lBRU1BLG1CO0FBQ0YsK0JBQVlDLEdBQVosRUFBaUI7QUFBQTs7QUFDYixTQUFLQSxHQUFMLEdBQVdBLEdBQVg7QUFFQSxTQUFLQyxTQUFMLEdBQWlCLEVBQWpCO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLEVBQWY7QUFFQSxTQUFLQyxJQUFMLEdBQVksRUFBWjtBQUNIOzs7OzJCQUVNO0FBQUE7O0FBQ0hDLE9BQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlDLE9BQVosQ0FBb0IsZUFBcEI7QUFFQSxXQUFLTixHQUFMLENBQVNPLE1BQVQsQ0FBZ0JDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixzQkFBakIsQ0FBaEIsRUFBMERDLElBQTFELENBQStELFVBQUNDLFFBQUQsRUFBYztBQUN6RSxZQUFJQSxRQUFRLENBQUNSLElBQVQsQ0FBY1MsT0FBbEIsRUFBMkI7QUFDdkIsV0FBQyxXQUFELEVBQWMsU0FBZCxFQUF5QkMsT0FBekIsQ0FBaUMsVUFBQ0MsSUFBRCxFQUFVO0FBQ3ZDLGlCQUFJLENBQUNBLElBQUQsQ0FBSixHQUFhSCxRQUFRLENBQUNSLElBQVQsQ0FBY1ksR0FBZCxDQUFrQkQsSUFBbEIsQ0FBYjtBQUNILFdBRkQ7O0FBR0EsZUFBSSxDQUFDRSxVQUFMO0FBQ0gsU0FMRCxNQUtPO0FBQ0hDLGdFQUFNLENBQUNDLGVBQVAsQ0FBdUIsUUFBdkIsRUFBaUNQLFFBQVEsQ0FBQ1IsSUFBVCxDQUFjWSxHQUEvQztBQUNIO0FBQ0osT0FURDtBQVVIOzs7aUNBR1k7QUFBQTs7QUFDVCxVQUFJLENBQUMsS0FBS2QsU0FBTixJQUFtQixDQUFDLEtBQUtDLE9BQTdCLEVBQXNDO0FBQ2xDZSw4REFBTSxDQUFDQyxlQUFQLENBQXVCLFFBQXZCLEVBQWlDLGtDQUFqQztBQUNBO0FBQ0g7O0FBRUQsVUFBTUMsR0FBRyxHQUFHWCxPQUFPLENBQUNDLFFBQVIsQ0FBaUIsc0JBQWpCLEVBQXlDO0FBQ2pEVyxhQUFLLEVBQUUsS0FBS25CLFNBRHFDO0FBRWpEb0IsV0FBRyxFQUFFLEtBQUtuQjtBQUZ1QyxPQUF6QyxDQUFaO0FBSUFFLE9BQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlDLE9BQVosQ0FBb0IsZUFBcEI7QUFFQSxXQUFLTixHQUFMLENBQVNPLE1BQVQsQ0FBZ0JZLEdBQWhCLEVBQXFCVCxJQUFyQixDQUEwQixVQUFDQyxRQUFELEVBQWM7QUFDcEMsWUFBSUEsUUFBUSxDQUFDUixJQUFULENBQWNTLE9BQWxCLEVBQTJCO0FBQ3ZCLFdBQUMsTUFBRCxFQUFTQyxPQUFULENBQWlCLFVBQUNDLElBQUQsRUFBVTtBQUN2QixrQkFBSSxDQUFDQSxJQUFELENBQUosR0FBYUgsUUFBUSxDQUFDUixJQUFULENBQWNZLEdBQWQsQ0FBa0JELElBQWxCLENBQWI7QUFDSCxXQUZEO0FBR0gsU0FKRCxNQUlPO0FBQ0hHLGdFQUFNLENBQUNDLGVBQVAsQ0FBdUIsUUFBdkIsRUFBaUNQLFFBQVEsQ0FBQ1IsSUFBVCxDQUFjWSxHQUEvQztBQUNIO0FBQ0osT0FSRDtBQVNIOzs7Ozs7SUFHQ08saUI7Ozs7Ozs7eUJBQ0dDLE0sRUFBUUMsTyxFQUFTO0FBQ2xCLFVBQUlDLFNBQVMsR0FBR0QsT0FBTyxDQUFDRSxTQUFSLENBQWtCO0FBQzlCLG9CQUFZO0FBQ1JDLGdCQUFNLEVBQUU7QUFEQSxTQURrQjtBQUk5QixpQkFBUyxDQUFDLENBQUUsQ0FBRixFQUFLLE1BQUwsQ0FBRDtBQUpxQixPQUFsQixDQUFoQjtBQU9BSixZQUFNLENBQUNLLE1BQVAsQ0FBYyxZQUFkLEVBQTRCLFVBQUNDLEtBQUQsRUFBVztBQUNuQyxZQUFJQSxLQUFKLEVBQVc7QUFDUEosbUJBQVMsQ0FBQ0ssS0FBVjtBQUNBTCxtQkFBUyxDQUFDTSxJQUFWLENBQWVDLEdBQWYsQ0FBbUJILEtBQW5CO0FBQ0FKLG1CQUFTLENBQUNRLElBQVY7QUFDSDtBQUNKLE9BTkQsRUFNRSxJQU5GO0FBT0g7Ozs7OztBQUdMQyx1REFBRyxDQUNFQyxVQURMLENBQ2dCLHFCQURoQixFQUN1QyxDQUFDLEtBQUQsRUFBUXBDLG1CQUFSLENBRHZDLEVBRUtxQyxTQUZMLENBRWUsVUFGZixFQUUyQjtBQUFBLFNBQU0sSUFBSWQsaUJBQUosRUFBTjtBQUFBLENBRjNCLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUZBOzs7Ozs7Ozs7Ozs7O0FBY0E7QUFDQTs7SUFFTWUsRztBQUNGOzs7OztBQUtBLGVBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFDZixTQUFLQSxLQUFMLEdBQWFBLEtBQWI7QUFDQSxTQUFLQyxPQUFMLEdBQWUsS0FBZjtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7OzsyQkFRT3BCLEcsRUFBS3FCLEssRUFBT0MsSyxFQUFPO0FBQ3RCLFdBQUtGLE9BQUwsR0FBZSxJQUFmOztBQUVBLFVBQUksQ0FBQ0MsS0FBTCxFQUFZO0FBQ1JBLGFBQUssR0FBRyxFQUFSO0FBQ0g7O0FBRUQsVUFBSUUsT0FBTyxHQUFHO0FBQ1Ysd0JBQWdCO0FBRE4sT0FBZDtBQUlBLGFBQU8sS0FBS0MsSUFBTCxDQUFVLE1BQVYsRUFBa0J4QixHQUFsQixFQUF1QmYsQ0FBQyxDQUFDd0MsS0FBRixDQUFRSixLQUFSLENBQXZCLEVBQXVDRSxPQUF2QyxFQUFnREQsS0FBaEQsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7d0JBT0l0QixHLEVBQUtzQixLLEVBQU87QUFDWixhQUFPLEtBQUtFLElBQUwsQ0FBVSxLQUFWLEVBQWlCeEIsR0FBakIsRUFBc0IsSUFBdEIsRUFBNEIsSUFBNUIsRUFBa0NzQixLQUFsQyxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7eUJBUUt0QixHLEVBQUtxQixLLEVBQU9DLEssRUFBTztBQUNwQixhQUFPLEtBQUtFLElBQUwsQ0FBVSxNQUFWLEVBQWtCeEIsR0FBbEIsRUFBdUJxQixLQUF2QixFQUE4QixJQUE5QixFQUFvQ0MsS0FBcEMsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7NEJBT090QixHLEVBQUtzQixLLEVBQU87QUFDZixhQUFPLEtBQUtFLElBQUwsQ0FBVSxRQUFWLEVBQW9CeEIsR0FBcEIsRUFBeUIsSUFBekIsRUFBK0IsSUFBL0IsRUFBcUNzQixLQUFyQyxDQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7MEJBUU10QixHLEVBQUtxQixLLEVBQU9DLEssRUFBTztBQUNyQixVQUFJQyxPQUFPLEdBQUc7QUFDVix3QkFBZ0IsOEJBRE47QUFFVixrQkFBVTtBQUZBLE9BQWQ7QUFLQSxhQUFPLEtBQUtDLElBQUwsQ0FBVSxPQUFWLEVBQW1CeEIsR0FBbkIsRUFBd0JxQixLQUF4QixFQUErQkUsT0FBL0IsRUFBd0NELEtBQXhDLENBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7O3lCQVVLSSxNLEVBQVExQixHLEVBQUtxQixLLEVBQU9FLE8sRUFBU0QsSyxFQUFPO0FBQ3JDLFVBQUlLLEdBQUcsR0FBRyxJQUFWOztBQUVBLFVBQUksQ0FBQ04sS0FBTCxFQUFZO0FBQ1JBLGFBQUssR0FBRyxFQUFSO0FBQ0g7O0FBRUQsVUFBSSxDQUFDRSxPQUFMLEVBQWM7QUFDVkEsZUFBTyxHQUFHO0FBQ04sMEJBQWdCLGtCQURWO0FBRU4sb0JBQVU7QUFGSixTQUFWO0FBSUg7O0FBRUQsVUFBSUQsS0FBSixFQUFXO0FBQ1BLLFdBQUcsR0FBRzFDLENBQUMsQ0FBQ3FDLEtBQUssQ0FBQ00sYUFBUCxDQUFQO0FBQ0FELFdBQUcsQ0FBQ0UsUUFBSixDQUFhLFNBQWI7QUFDQUYsV0FBRyxDQUFDRyxJQUFKLENBQVMsVUFBVCxFQUFxQixJQUFyQjtBQUNIOztBQUVELGFBQU8sS0FBS1gsS0FBTCxDQUFXO0FBQ2RPLGNBQU0sRUFBRUEsTUFETTtBQUVkMUIsV0FBRyxFQUFFQSxHQUZTO0FBR2RoQixZQUFJLEVBQUVxQyxLQUhRO0FBSWRFLGVBQU8sRUFBRUE7QUFKSyxPQUFYLFdBS0UsVUFBQ1EsS0FBRCxFQUFXO0FBQ2hCLFlBQUlBLEtBQUssQ0FBQy9DLElBQU4sQ0FBV2dELE1BQWYsRUFBdUI7QUFDbkJsQyx5REFBTSxDQUFDQyxlQUFQLENBQXVCLFFBQXZCLEVBQWlDZ0MsS0FBSyxDQUFDL0MsSUFBTixDQUFXZ0QsTUFBNUM7QUFDSCxTQUZELE1BRU87QUFDSGxDLHlEQUFNLENBQUNDLGVBQVAsQ0FBdUIsUUFBdkIsRUFBaUMsNEJBQWpDO0FBQ0g7QUFDSixPQVhNLGFBV0ksWUFBTTtBQUNiZCxTQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxPQUFaLENBQW9CLGNBQXBCOztBQUNBLFlBQUl3QyxHQUFKLEVBQVM7QUFDTCxjQUFNTSxLQUFLLEdBQUdOLEdBQUcsQ0FBQzNDLElBQUosQ0FBUyxPQUFULENBQWQ7O0FBQ0EsY0FBSWlELEtBQUosRUFBVztBQUNQaEQsYUFBQyxDQUFDLE1BQU1nRCxLQUFQLENBQUQsQ0FBZUEsS0FBZixDQUFxQixNQUFyQjtBQUNIOztBQUVETixhQUFHLENBQUNPLFdBQUosQ0FBZ0IsU0FBaEI7QUFDQVAsYUFBRyxDQUFDRyxJQUFKLENBQVMsVUFBVCxFQUFxQixLQUFyQjtBQUNIO0FBQ0osT0F0Qk0sQ0FBUDtBQXVCSDs7Ozs7O0FBR0wsSUFBTWYsR0FBRyxHQUFHb0IsOENBQU8sQ0FBQ0MsTUFBUixDQUFlLFdBQWYsRUFBNEIsRUFBNUIsRUFDUjtBQURRLENBRVBDLE1BRk8sQ0FFQSxDQUFDLHNCQUFELEVBQXlCLG1CQUF6QixFQUE4QyxVQUFDQyxvQkFBRCxFQUF1QkMsaUJBQXZCLEVBQTZDO0FBQy9GRCxzQkFBb0IsQ0FBQ0UsV0FBckIsQ0FBaUMsS0FBakMsRUFBd0NDLFNBQXhDLENBQWtELEtBQWxEO0FBQ0FGLG1CQUFpQixDQUFDRyxTQUFsQixDQUE0QjtBQUN4QkMsV0FBTyxFQUFFLElBRGU7QUFFeEJDLGVBQVcsRUFBRSxLQUZXO0FBR3hCQyxnQkFBWSxFQUFFO0FBSFUsR0FBNUI7QUFLSCxDQVBPLENBRkEsRUFVUEMsTUFWTyxDQVVBLFFBVkEsRUFVVSxDQUFDLE1BQUQsRUFBUyxVQUFDQyxJQUFELEVBQVU7QUFDakMsU0FBTyxVQUFDQyxHQUFELEVBQVM7QUFDWixXQUFPRCxJQUFJLENBQUNFLFdBQUwsQ0FBaUJELEdBQWpCLENBQVA7QUFDSCxHQUZEO0FBR0gsQ0FKaUIsQ0FWVixFQWVQRixNQWZPLENBZUEsT0FmQSxFQWVTLFlBQU07QUFDbkIsU0FBTyxVQUFDekIsS0FBRCxFQUFRNkIsS0FBUixFQUFrQjtBQUNyQkEsU0FBSyxHQUFHQyxRQUFRLENBQUNELEtBQUQsQ0FBaEI7O0FBRUEsU0FBSyxJQUFJRSxDQUFDLEdBQUMsQ0FBWCxFQUFjQSxDQUFDLEdBQUNGLEtBQWhCLEVBQXVCRSxDQUFDLEVBQXhCLEVBQTRCO0FBQ3hCL0IsV0FBSyxDQUFDZ0MsSUFBTixDQUFXRCxDQUFYO0FBQ0g7O0FBRUQsV0FBTy9CLEtBQVA7QUFDSCxHQVJEO0FBU0gsQ0F6Qk8sRUEwQlBpQyxPQTFCTyxDQTBCQyxLQTFCRCxFQTBCUSxDQUFDLE9BQUQsRUFBVXBDLEdBQVYsQ0ExQlIsRUEyQlBELFNBM0JPLENBMkJHLE1BM0JILEVBMkJXLENBQUMsV0FBRCxFQUFjLFVBQUNzQyxTQUFELEVBQWU7QUFDNUMsU0FBTztBQUNIQyxZQUFRLEVBQUMsR0FETjtBQUVIQyxZQUFRLEVBQUUsR0FGUDtBQUdIQyxXQUFPLEVBQUUsbUJBQU07QUFDWCxhQUFPO0FBQ0hDLFdBQUcsRUFBRSxhQUFDQyxLQUFELEVBQVF2RCxPQUFSLEVBQWlCd0QsS0FBakIsRUFBMkI7QUFDNUIsY0FBSUEsS0FBSyxDQUFDQyxRQUFOLEtBQW1CLEVBQXZCLEVBQTJCOztBQUMzQixjQUFJRCxLQUFLLENBQUNFLE1BQU4sS0FBaUJDLFNBQWpCLElBQThCSCxLQUFLLENBQUNFLE1BQU4sS0FBaUIsRUFBbkQsRUFBc0Q7QUFDbERGLGlCQUFLLENBQUNFLE1BQU4sR0FBZVIsU0FBUyxDQUFDVSxNQUFWLEVBQWY7QUFDSDtBQUNKO0FBTkUsT0FBUDtBQVFIO0FBWkUsR0FBUDtBQWNILENBZmtCLENBM0JYLEVBMkNQaEQsU0EzQ08sQ0EyQ0csYUEzQ0gsRUEyQ2tCLFlBQU07QUFDNUIsU0FBTztBQUNIdUMsWUFBUSxFQUFFLEdBRFA7QUFFSFUsUUFBSSxFQUFFLGNBQUNOLEtBQUQsRUFBUXZELE9BQVIsRUFBaUJ3RCxLQUFqQixFQUEyQjtBQUM3QjVFLE9BQUMsQ0FBQ29CLE9BQUQsQ0FBRCxDQUFXOEQsT0FBWCxDQUFtQjtBQUNmaEYsZUFBTyxFQUFHO0FBREssT0FBbkI7QUFHSDtBQU5FLEdBQVA7QUFRSCxDQXBETyxDQUFaO0FBdURlNEIsa0VBQWYsRTs7Ozs7Ozs7Ozs7OztBQ3BOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FBY2E7O0FBRWI7QUFDQSIsImZpbGUiOiJBZG1pbi9GaWxlQXVkaXRDdHJsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKiAgQ29weXJpZ2h0IG5vdGljZVxyXG4gKlxyXG4gKiAgKGMpIDIwMjAgRlRLIGUuVi4gPGZ0ay5kZT5cclxuICogIEFsbCByaWdodHMgcmVzZXJ2ZWRcclxuICpcclxuICogIFRoaXMgc2NyaXB0IGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAqICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gKiAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gKiAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuICpcclxuICogIFRoaXMgY29weXJpZ2h0IG5vdGljZSBNVVNUIEFQUEVBUiBpbiBhbGwgY29waWVzIG9mIHRoZSBzY3JpcHQhXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG4ndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgJy4uL0xpYnJhcnkvRGF0YVRhYmxlcyc7XHJcblxyXG5pbXBvcnQgYXBwIGZyb20gJy4uL0NvbW1vbi9Bbmd1bGFyJztcclxuaW1wb3J0IENvbW1vbiBmcm9tICcuLi9Db21tb24vQ29tbW9uJ1xyXG5cclxuY2xhc3MgRmlsZUF1ZGl0Q29udHJvbGxlciB7XHJcbiAgICBjb25zdHJ1Y3RvcihlaWQpIHtcclxuICAgICAgICB0aGlzLmVpZCA9IGVpZDtcclxuXHJcbiAgICAgICAgdGhpcy5zdGFydERhdGUgPSAnJztcclxuICAgICAgICB0aGlzLmVuZERhdGUgPSAnJztcclxuXHJcbiAgICAgICAgdGhpcy5kYXRhID0ge307XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdCgpIHtcclxuICAgICAgICAkKGRvY3VtZW50KS50cmlnZ2VyKCdsb2FkaW5nLnN0YXJ0Jyk7XHJcblxyXG4gICAgICAgIHRoaXMuZWlkLmNoYW5nZShSb3V0aW5nLmdlbmVyYXRlKCdhZG1pbl9maWxlYXVkaXRfaW5pdCcpKS50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzcG9uc2UuZGF0YS5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICBbJ3N0YXJ0RGF0ZScsICdlbmREYXRlJ10uZm9yRWFjaCgobmFtZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXNbbmFtZV0gPSByZXNwb25zZS5kYXRhLm1zZ1tuYW1lXTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy52aWV3UmVwb3J0KCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBDb21tb24uc2hvd0FsZXJ0RGlhbG9nKCdkYW5nZXInLCByZXNwb25zZS5kYXRhLm1zZyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgdmlld1JlcG9ydCgpIHtcclxuICAgICAgICBpZiAoIXRoaXMuc3RhcnREYXRlIHx8ICF0aGlzLmVuZERhdGUpIHtcclxuICAgICAgICAgICAgQ29tbW9uLnNob3dBbGVydERpYWxvZygnZGFuZ2VyJywgJ1BsZWFzZSBpbnB1dCBhdWRpdFxcJ3MgZGF0ZSByYW5nZScpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB1cmwgPSBSb3V0aW5nLmdlbmVyYXRlKCdhZG1pbl9maWxlYXVkaXRfdmlldycsIHtcclxuICAgICAgICAgICAgYmVnaW46IHRoaXMuc3RhcnREYXRlLFxyXG4gICAgICAgICAgICBlbmQ6IHRoaXMuZW5kRGF0ZSxcclxuICAgICAgICB9KTtcclxuICAgICAgICAkKGRvY3VtZW50KS50cmlnZ2VyKCdsb2FkaW5nLnN0YXJ0Jyk7XHJcblxyXG4gICAgICAgIHRoaXMuZWlkLmNoYW5nZSh1cmwpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXNwb25zZS5kYXRhLnN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgICAgIFsnZGF0YSddLmZvckVhY2goKG5hbWUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzW25hbWVdID0gcmVzcG9uc2UuZGF0YS5tc2dbbmFtZV07XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIENvbW1vbi5zaG93QWxlcnREaWFsb2coJ2RhbmdlcicsIHJlc3BvbnNlLmRhdGEubXNnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG5jbGFzcyBMb2dUYWJsZURpcmVjdGl2ZSB7XHJcbiAgICBsaW5rKCRzY29wZSwgZWxlbWVudCkge1xyXG4gICAgICAgIGxldCBkYXRhVGFibGUgPSBlbGVtZW50LkRhdGFUYWJsZSh7XHJcbiAgICAgICAgICAgIFwibGFuZ3VhZ2VcIjoge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoOiBcIjxzcGFuIGNsYXNzPSdmYXMgZmEtc2VhcmNoJz48L3NwYW4+IF9JTlBVVF9cIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBcIm9yZGVyXCI6IFtbIDAsIFwiZGVzY1wiIF1dLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkc2NvcGUuJHdhdGNoKCdhdWRpdC5kYXRhJywgKHZhbHVlKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgZGF0YVRhYmxlLmNsZWFyKCk7XHJcbiAgICAgICAgICAgICAgICBkYXRhVGFibGUucm93cy5hZGQodmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgZGF0YVRhYmxlLmRyYXcoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sdHJ1ZSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmFwcFxyXG4gICAgLmNvbnRyb2xsZXIoJ0ZpbGVBdWRpdENvbnRyb2xsZXInLCBbJ2VpZCcgLEZpbGVBdWRpdENvbnRyb2xsZXJdKVxyXG4gICAgLmRpcmVjdGl2ZSgnbG9nVGFibGUnLCAoKSA9PiBuZXcgTG9nVGFibGVEaXJlY3RpdmUpXHJcbjsiLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbmltcG9ydCBhbmd1bGFyIGZyb20gJ2FuZ3VsYXInO1xyXG5pbXBvcnQgQ29tbW9uIGZyb20gJy4vQ29tbW9uJztcclxuXHJcbmNsYXNzIEVpZCB7XHJcbiAgICAvKipcclxuICAgICAqIEluaXRpYXRlXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtICRodHRwXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKCRodHRwKSB7XHJcbiAgICAgICAgdGhpcy4kaHR0cCA9ICRodHRwO1xyXG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogc2VuZCBub3JtYWwgUE9TVFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB1cmxcclxuICAgICAqIEBwYXJhbSBpbnB1dFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+IHwgUHJvbWlzZTxUPiB8ICp9XHJcbiAgICAgKi9cclxuICAgIGNoYW5nZSh1cmwsIGlucHV0LCBldmVudCkge1xyXG4gICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XHJcblxyXG4gICAgICAgIGlmICghaW5wdXQpIHtcclxuICAgICAgICAgICAgaW5wdXQgPSB7fTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBoZWFkZXJzID0ge1xyXG4gICAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCdcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5zZW5kKCdQT1NUJywgdXJsLCAkLnBhcmFtKGlucHV0KSwgaGVhZGVycywgZXZlbnQpXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIGEgR0VUIHJlcXVlc3RcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT58UHJvbWlzZTxUPnwqfVxyXG4gICAgICovXHJcbiAgICBnZXQodXJsLCBldmVudCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbmQoJ0dFVCcsIHVybCwgbnVsbCwgbnVsbCwgZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBhIFBPU1QgcmVxdWVzdCB3aXRoIEpTT04gZGF0YVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB1cmxcclxuICAgICAqIEBwYXJhbSBpbnB1dFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fFByb21pc2U8VD58Kn1cclxuICAgICAqL1xyXG4gICAgcG9zdCh1cmwsIGlucHV0LCBldmVudCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbmQoJ1BPU1QnLCB1cmwsIGlucHV0LCBudWxsLCBldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIGEgREVMRVRFIHJlcXVlc3RcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT58UHJvbWlzZTxUPnwqfVxyXG4gICAgICovXHJcbiAgICBkZWxldGUodXJsLCBldmVudCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbmQoJ0RFTEVURScsIHVybCwgbnVsbCwgbnVsbCwgZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBhIFBBVENIIHJlcXVlc3Qgd2l0aCBKU09OIGRhdGFcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdXJsXHJcbiAgICAgKiBAcGFyYW0gaW5wdXRcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHJldHVybnMge1Byb21pc2U8YW55PnxQcm9taXNlPFQ+fCp9XHJcbiAgICAgKi9cclxuICAgIHBhdGNoKHVybCwgaW5wdXQsIGV2ZW50KSB7XHJcbiAgICAgICAgbGV0IGhlYWRlcnMgPSB7XHJcbiAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vbWVyZ2UtcGF0Y2granNvbicsXHJcbiAgICAgICAgICAgICdhY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VuZCgnUEFUQ0gnLCB1cmwsIGlucHV0LCBoZWFkZXJzLCBldmVudClcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbmQgYSByZXF1ZXN0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIG1ldGhvZFxyXG4gICAgICogQHBhcmFtIHVybFxyXG4gICAgICogQHBhcmFtIGlucHV0XHJcbiAgICAgKiBAcGFyYW0gaGVhZGVyc1xyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+IHwgUHJvbWlzZTxUPiB8ICp9XHJcbiAgICAgKi9cclxuICAgIHNlbmQobWV0aG9kLCB1cmwsIGlucHV0LCBoZWFkZXJzLCBldmVudCkge1xyXG4gICAgICAgIGxldCBidG4gPSBudWxsO1xyXG5cclxuICAgICAgICBpZiAoIWlucHV0KSB7XHJcbiAgICAgICAgICAgIGlucHV0ID0ge307XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWhlYWRlcnMpIHtcclxuICAgICAgICAgICAgaGVhZGVycyA9IHtcclxuICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXHJcbiAgICAgICAgICAgICAgICAnYWNjZXB0JzogJ2FwcGxpY2F0aW9uL2xkK2pzb24nLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIGJ0biA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XHJcbiAgICAgICAgICAgIGJ0bi5hZGRDbGFzcygncnVubmluZycpO1xyXG4gICAgICAgICAgICBidG4ucHJvcChcImRpc2FibGVkXCIsIHRydWUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuJGh0dHAoe1xyXG4gICAgICAgICAgICBtZXRob2Q6IG1ldGhvZCxcclxuICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgIGRhdGE6IGlucHV0LFxyXG4gICAgICAgICAgICBoZWFkZXJzOiBoZWFkZXJzXHJcbiAgICAgICAgfSkuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChlcnJvci5kYXRhLmRldGFpbCkge1xyXG4gICAgICAgICAgICAgICAgQ29tbW9uLnNob3dBbGVydERpYWxvZygnZGFuZ2VyJywgZXJyb3IuZGF0YS5kZXRhaWwpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgQ29tbW9uLnNob3dBbGVydERpYWxvZygnZGFuZ2VyJywgJ0FjdGlvbiBjYW5ub3QgYmUgcGVyZm9ybWVkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICAgICAgJChkb2N1bWVudCkudHJpZ2dlcignbG9hZGluZy5zdG9wJyk7XHJcbiAgICAgICAgICAgIGlmIChidG4pIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1vZGFsID0gYnRuLmRhdGEoJ2Nsb3NlJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAobW9kYWwpIHtcclxuICAgICAgICAgICAgICAgICAgICAkKCcjJyArIG1vZGFsKS5tb2RhbCgnaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGJ0bi5yZW1vdmVDbGFzcygncnVubmluZycpO1xyXG4gICAgICAgICAgICAgICAgYnRuLnByb3AoXCJkaXNhYmxlZFwiLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG5cclxuY29uc3QgYXBwID0gYW5ndWxhci5tb2R1bGUoJ0Vjb3N5c3RlbScsIFtdKVxyXG4gICAgLy8gY2hhbmdlIEFuZ3VsYXIgZGVmYXVsdCB7eyB0byB7W3sgdG8gYXZvaWQgY29uZmxpY3Qgd2l0aCBUd2lnXHJcbiAgICAuY29uZmlnKFsnJGludGVycG9sYXRlUHJvdmlkZXInLCAnJGxvY2F0aW9uUHJvdmlkZXInLCAoJGludGVycG9sYXRlUHJvdmlkZXIsICRsb2NhdGlvblByb3ZpZGVyKSA9PiB7XHJcbiAgICAgICAgJGludGVycG9sYXRlUHJvdmlkZXIuc3RhcnRTeW1ib2woJ3tbeycpLmVuZFN5bWJvbCgnfV19Jyk7XHJcbiAgICAgICAgJGxvY2F0aW9uUHJvdmlkZXIuaHRtbDVNb2RlKHtcclxuICAgICAgICAgICAgZW5hYmxlZDogdHJ1ZSxcclxuICAgICAgICAgICAgcmVxdWlyZUJhc2U6IGZhbHNlLFxyXG4gICAgICAgICAgICByZXdyaXRlTGlua3M6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XSlcclxuICAgIC5maWx0ZXIoJ3Vuc2FmZScsIFsnJHNjZScsICgkc2NlKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuICh2YWwpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuICRzY2UudHJ1c3RBc0h0bWwodmFsKTtcclxuICAgICAgICB9O1xyXG4gICAgfV0pXHJcbiAgICAuZmlsdGVyKCdyYW5nZScsICgpID0+IHtcclxuICAgICAgICByZXR1cm4gKGlucHV0LCB0b3RhbCkgPT4ge1xyXG4gICAgICAgICAgICB0b3RhbCA9IHBhcnNlSW50KHRvdGFsKTtcclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGk9MDsgaTx0b3RhbDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBpbnB1dC5wdXNoKGkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gaW5wdXQ7XHJcbiAgICAgICAgfTtcclxuICAgIH0pXHJcbiAgICAuc2VydmljZSgnZWlkJywgWyckaHR0cCcsIEVpZF0pXHJcbiAgICAuZGlyZWN0aXZlKCdmb3JtJywgWyckbG9jYXRpb24nLCAoJGxvY2F0aW9uKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcmVzdHJpY3Q6J0UnLFxyXG4gICAgICAgICAgICBwcmlvcml0eTogOTk5LFxyXG4gICAgICAgICAgICBjb21waWxlOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHByZTogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXR0cnMubm9hY3Rpb24gPT09ICcnKSByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhdHRycy5hY3Rpb24gPT09IHVuZGVmaW5lZCB8fCBhdHRycy5hY3Rpb24gPT09ICcnKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzLmFjdGlvbiA9ICRsb2NhdGlvbi5hYnNVcmwoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1dKVxyXG4gICAgLmRpcmVjdGl2ZSgnaW5pdFRvb2x0aXAnLCAoKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcmVzdHJpY3Q6ICdBJyxcclxuICAgICAgICAgICAgbGluazogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgPT4ge1xyXG4gICAgICAgICAgICAgICAgJChlbGVtZW50KS50b29sdGlwKHtcclxuICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyIDogJ2hvdmVyJ1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfSlcclxuO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgYXBwOyIsIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuICogIENvcHlyaWdodCBub3RpY2VcclxuICpcclxuICogIChjKSAyMDE4IEZUSyBlLlYuIDxmdGsuZGU+XHJcbiAqICBBbGwgcmlnaHRzIHJlc2VydmVkXHJcbiAqXHJcbiAqICBUaGlzIHNjcmlwdCBpcyBkaXN0cmlidXRlZCBpbiB0aGUgaG9wZSB0aGF0IGl0IHdpbGwgYmUgdXNlZnVsLFxyXG4gKiAgYnV0IFdJVEhPVVQgQU5ZIFdBUlJBTlRZOyB3aXRob3V0IGV2ZW4gdGhlIGltcGxpZWQgd2FycmFudHkgb2ZcclxuICogIE1FUkNIQU5UQUJJTElUWSBvciBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRS4gIFNlZSB0aGVcclxuICogIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGZvciBtb3JlIGRldGFpbHMuXHJcbiAqXHJcbiAqICBUaGlzIGNvcHlyaWdodCBub3RpY2UgTVVTVCBBUFBFQVIgaW4gYWxsIGNvcGllcyBvZiB0aGUgc2NyaXB0IVxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxuaW1wb3J0ICdkYXRhdGFibGVzLm5ldCc7XHJcbmltcG9ydCAnZGF0YXRhYmxlcy5uZXQtYnM0L2Nzcy9kYXRhVGFibGVzLmJvb3RzdHJhcDQubWluLmNzcyc7XHJcbmltcG9ydCAnZGF0YXRhYmxlcy5uZXQtYnM0L2pzL2RhdGFUYWJsZXMuYm9vdHN0cmFwNC5taW4uanMnOyJdLCJzb3VyY2VSb290IjoiIn0=