(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Admin/EmailCtrl~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Record~Archive/SearchCtrl~Aut~d45f3fc6"],{

/***/ "./assets/js/Common/Common.js":
/*!************************************!*\
  !*** ./assets/js/Common/Common.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($, jQuery) {function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
var Common = /*#__PURE__*/function () {
  function Common() {
    _classCallCheck(this, Common);
  }

  _createClass(Common, null, [{
    key: "deleteBtn",
    value: function deleteBtn() {
      return $('#deleteConfirmation-btn');
    }
  }, {
    key: "deleteDiv",
    value: function deleteDiv() {
      return $('#deleteConfirmation-div');
    }
  }, {
    key: "alertDiv",
    value: function alertDiv() {
      return $('#alert-dialog');
    }
  }, {
    key: "COOKIE_DONOTTRACK",
    value: function COOKIE_DONOTTRACK() {
      return 'DoNotTrack';
    }
  }, {
    key: "showConfirmation",
    value: function showConfirmation(msg, btnTitle, callBack) {
      if (Common.deleteDiv().length > 0) {
        $('#deleteConfirmation-msg').html(msg);

        if (btnTitle) {
          Common.deleteBtn().html(btnTitle);
        }

        Common.deleteBtn().off('click').click(function (event) {
          $(event.currentTarget).prop('disabled', true);
          callBack();
          Common.deleteDiv().appendTo("body").modal('hide');
        });
        Common.deleteBtn().prop('disabled', false);
        Common.deleteDiv().appendTo("body").modal('show');
      }
    }
  }, {
    key: "hideConfirmation",
    value: function hideConfirmation() {
      Common.deleteDiv().modal('hide');
      Common.deleteBtn().prop('disabled', false);
    }
  }, {
    key: "prepareAlert",
    value: function prepareAlert(type, text) {
      var json = JSON.stringify({
        type: type,
        text: text
      });
      Common.setCookie('flash', json, 1 / 24 / 60);
    }
  }, {
    key: "showAlertDialog",
    value: function showAlertDialog(type, text) {
      var alertDiv = Common.alertDiv();
      alertDiv.css('bottom', '0px');
      alertDiv.removeClass().addClass('alert-bottom alert alert-' + type);
      alertDiv.find('.text').html(text);
      alertDiv.show().animate({
        bottom: '50px'
      }, 500).delay(5000).animate({
        bottom: '-50px'
      }, 500, 'swing', function () {
        alertDiv.hide();
      });
    }
  }, {
    key: "setCookie",
    value: function setCookie(name, value, days) {
      var expires;

      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        expires = "; expires=" + date.toGMTString();
      } else {
        expires = "";
      }

      document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
    }
  }, {
    key: "getCookie",
    value: function getCookie(name) {
      var nameEQ = encodeURIComponent(name) + "=";
      var ca = document.cookie.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) === ' ') {
          c = c.substring(1, c.length);
        }

        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
      }

      return null;
    }
  }, {
    key: "initMessage",
    value: function initMessage(infoDiv) {
      infoDiv.on('action.fail', function (event, msg, autoClose) {
        if (!msg) {
          msg = 'Unable to perform the action';
        }

        Common.prepareMessage(infoDiv, msg, false, autoClose);
      }).on('action.success', function (event, msg, autoClose) {
        if (!msg) {
          msg = 'The action was performed successfully';
        }

        Common.prepareMessage(infoDiv, msg, true, autoClose);
      }).find('.close').click(function () {
        infoDiv.hide('fast');
      });
    }
  }, {
    key: "prepareMessage",
    value: function prepareMessage(infoDiv, msg, success, autoClose) {
      infoDiv.removeClass('alert-danger alert-success');

      if (success) {
        infoDiv.addClass('alert-success');
      } else {
        infoDiv.addClass('alert-danger');
      }

      infoDiv.find('.msg-text').text(msg);

      if (autoClose) {
        infoDiv.show('fast').delay(5000).hide('slow');
      } else {
        infoDiv.show('slow');
      }
    }
  }, {
    key: "uuidv4",
    value: function uuidv4() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : r & 0x3 | 0x8;
        return v.toString(16);
      });
    }
  }, {
    key: "updateLog",
    value: function updateLog(uuid, action, note) {
      jQuery.ajax({
        type: "GET",
        async: action === 'visit',
        url: Routing.generate('core_log_update', {
          uuid: uuid,
          referrer: document.referrer,
          action: action,
          note: note
        })
      });
    }
  }, {
    key: "startLog",
    value: function startLog() {
      if (Common.getCookie(Common.COOKIE_DONOTTRACK()) == 1) {
        return;
      }

      var uuid = Common.uuidv4();
      Common.updateLog(uuid, 'visit', '');
      setInterval(function () {
        Common.updateLog(uuid, 'visit', '');
      }, 30000);
    }
  }, {
    key: "initPanelPosition",
    value: function initPanelPosition(panelDiv, isPinnedFunction, parentWindow) {
      var originalY = -1; // Space between element and top of screen (when scrolling)

      var topMargin = 50;

      if (typeof parentWindow == 'undefined') {
        parentWindow = $(window);
      } // Should probably be set in CSS; but here just for emphasis


      panelDiv.css('position', 'relative');
      parentWindow.on('scroll', function () {
        var scrollTop = parentWindow.scrollTop();

        if (originalY < 0 && panelDiv.css('display') == 'block') {
          originalY = panelDiv.offset().top;
        }

        if (originalY > 0 && !isPinnedFunction()) {
          panelDiv.stop(false, false).animate({
            top: scrollTop < originalY ? 0 : scrollTop - originalY + topMargin
          }, 0);
        }
      });
    }
  }, {
    key: "reRenderTable",
    value: function reRenderTable(prefix, center, buttonFunction) {
      //render row
      $('td.' + prefix + '-id').each(function (index, item) {
        var element = $(item);
        var id = element.text();
        var row = element.parent();
        $.each(center, function (index, field) {
          row.find('.' + prefix + '-' + field).css('text-align', 'center');
        });
        var buttons = row.find('.' + prefix + '-btn'); //render button

        if (buttons.length > 0) {
          buttons.css('text-align', 'right').html(buttonFunction(id, row));
        }
      });
    }
  }, {
    key: "createDataTable",
    value: function createDataTable(prefix, fields, tableRoute, parameters, orderCallBack, dataCallback, drawCallback) {
      var table = $('#' + prefix + '-tbl');

      if (table.length > 0) {
        var columns = [];
        $.each(fields, function (index, field) {
          var column = {
            "data": field,
            "class": prefix + '-' + field
          };

          if (orderCallBack) {
            column.orderable = orderCallBack(field);
          }

          columns.push(column);
        });
        columns.push({
          "orderable": false,
          "data": null,
          "class": prefix + '-' + 'btn',
          "defaultContent": ''
        });
        var config = {
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": Routing.generate(tableRoute, parameters),
            "type": "POST"
          },
          "drawCallback": drawCallback,
          "order": [[0, "desc"]],
          "columns": columns,
          "language": {
            search: "<span class='fas fa-search'></span> _INPUT_"
          }
        };

        if (dataCallback) {
          config.ajax.data = dataCallback;
        }

        return table.DataTable(config);
      }

      return null;
    }
  }, {
    key: "iconPath",
    value: function iconPath() {
      return '/icon/';
    }
  }, {
    key: "getTaxonomyIcon",
    value: function getTaxonomyIcon() {
      return {
        "taxonomy": {
          "icon": this.iconPath() + "text_align_justify.png"
        },
        "category": {
          "icon": this.iconPath() + "folder.png"
        },
        "default": {
          "icon": this.iconPath() + "page.png"
        }
      };
    }
  }, {
    key: "showInput",
    value: function showInput(className) {
      $('.' + className).each(function (index, input) {
        var element = $(input);
        element.parent().show();
        element.prop('required', element.attr('data-required') === 'true');
      });
    }
  }, {
    key: "hideInput",
    value: function hideInput(className) {
      $('.' + className).each(function (index, input) {
        var element = $(input);
        element.parent().hide();
        element.attr('data-required', element.prop('required'));
        element.removeAttr('required');
      });
    }
  }, {
    key: "slugify",
    value: function slugify(text, separator) {
      text = text.toString().toLowerCase().trim();
      var sets = [{
        to: 'a',
        from: '[ÀÁÂÃÄÅÆĀĂĄẠẢẤẦẨẪẬẮẰẲẴẶ]'
      }, {
        to: 'c',
        from: '[ÇĆĈČ]'
      }, {
        to: 'd',
        from: '[ÐĎĐÞ]'
      }, {
        to: 'e',
        from: '[ÈÉÊËĒĔĖĘĚẸẺẼẾỀỂỄỆ]'
      }, {
        to: 'g',
        from: '[ĜĞĢǴ]'
      }, {
        to: 'h',
        from: '[ĤḦ]'
      }, {
        to: 'i',
        from: '[ÌÍÎÏĨĪĮİỈỊ]'
      }, {
        to: 'j',
        from: '[Ĵ]'
      }, {
        to: 'ij',
        from: '[Ĳ]'
      }, {
        to: 'k',
        from: '[Ķ]'
      }, {
        to: 'l',
        from: '[ĹĻĽŁ]'
      }, {
        to: 'm',
        from: '[Ḿ]'
      }, {
        to: 'n',
        from: '[ÑŃŅŇ]'
      }, {
        to: 'o',
        from: '[ÒÓÔÕÖØŌŎŐỌỎỐỒỔỖỘỚỜỞỠỢǪǬƠ]'
      }, {
        to: 'oe',
        from: '[Œ]'
      }, {
        to: 'p',
        from: '[ṕ]'
      }, {
        to: 'r',
        from: '[ŔŖŘ]'
      }, {
        to: 's',
        from: '[ßŚŜŞŠ]'
      }, {
        to: 't',
        from: '[ŢŤ]'
      }, {
        to: 'u',
        from: '[ÙÚÛÜŨŪŬŮŰŲỤỦỨỪỬỮỰƯ]'
      }, {
        to: 'w',
        from: '[ẂŴẀẄ]'
      }, {
        to: 'x',
        from: '[ẍ]'
      }, {
        to: 'y',
        from: '[ÝŶŸỲỴỶỸ]'
      }, {
        to: 'z',
        from: '[ŹŻŽ]'
      }, {
        to: '-',
        from: '[·/_,:;\']'
      }];
      sets.forEach(function (set) {
        text = text.replace(new RegExp(set.from, 'gi'), set.to);
      });
      text = text.toString().toLowerCase().replace(/\s+/g, '-') // Replace spaces with -
      .replace(/&/g, '-and-') // Replace & with 'and'
      .replace(/[^\w\-]+/g, '') // Remove all non-word chars
      .replace(/\--+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, ''); // Trim - from end of text

      if (typeof separator !== 'undefined' && separator !== '-') {
        text = text.replace(/-/g, separator);
      }

      return text;
    }
  }, {
    key: "pad",
    value: function pad(num, size) {
      var s = '000000000' + num;
      return s.substr(s.length - size);
    }
  }, {
    key: "ucfirst",
    value: function ucfirst(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
  }]);

  return Common;
}();

/* harmony default export */ __webpack_exports__["default"] = (Common);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvQ29tbW9uL0NvbW1vbi5qcyJdLCJuYW1lcyI6WyJDb21tb24iLCIkIiwibXNnIiwiYnRuVGl0bGUiLCJjYWxsQmFjayIsImRlbGV0ZURpdiIsImxlbmd0aCIsImh0bWwiLCJkZWxldGVCdG4iLCJvZmYiLCJjbGljayIsImV2ZW50IiwiY3VycmVudFRhcmdldCIsInByb3AiLCJhcHBlbmRUbyIsIm1vZGFsIiwidHlwZSIsInRleHQiLCJqc29uIiwiSlNPTiIsInN0cmluZ2lmeSIsInNldENvb2tpZSIsImFsZXJ0RGl2IiwiY3NzIiwicmVtb3ZlQ2xhc3MiLCJhZGRDbGFzcyIsImZpbmQiLCJzaG93IiwiYW5pbWF0ZSIsImJvdHRvbSIsImRlbGF5IiwiaGlkZSIsIm5hbWUiLCJ2YWx1ZSIsImRheXMiLCJleHBpcmVzIiwiZGF0ZSIsIkRhdGUiLCJzZXRUaW1lIiwiZ2V0VGltZSIsInRvR01UU3RyaW5nIiwiZG9jdW1lbnQiLCJjb29raWUiLCJlbmNvZGVVUklDb21wb25lbnQiLCJuYW1lRVEiLCJjYSIsInNwbGl0IiwiaSIsImMiLCJjaGFyQXQiLCJzdWJzdHJpbmciLCJpbmRleE9mIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwiaW5mb0RpdiIsIm9uIiwiYXV0b0Nsb3NlIiwicHJlcGFyZU1lc3NhZ2UiLCJzdWNjZXNzIiwicmVwbGFjZSIsInIiLCJNYXRoIiwicmFuZG9tIiwidiIsInRvU3RyaW5nIiwidXVpZCIsImFjdGlvbiIsIm5vdGUiLCJqUXVlcnkiLCJhamF4IiwiYXN5bmMiLCJ1cmwiLCJSb3V0aW5nIiwiZ2VuZXJhdGUiLCJyZWZlcnJlciIsImdldENvb2tpZSIsIkNPT0tJRV9ET05PVFRSQUNLIiwidXVpZHY0IiwidXBkYXRlTG9nIiwic2V0SW50ZXJ2YWwiLCJwYW5lbERpdiIsImlzUGlubmVkRnVuY3Rpb24iLCJwYXJlbnRXaW5kb3ciLCJvcmlnaW5hbFkiLCJ0b3BNYXJnaW4iLCJ3aW5kb3ciLCJzY3JvbGxUb3AiLCJvZmZzZXQiLCJ0b3AiLCJzdG9wIiwicHJlZml4IiwiY2VudGVyIiwiYnV0dG9uRnVuY3Rpb24iLCJlYWNoIiwiaW5kZXgiLCJpdGVtIiwiZWxlbWVudCIsImlkIiwicm93IiwicGFyZW50IiwiZmllbGQiLCJidXR0b25zIiwiZmllbGRzIiwidGFibGVSb3V0ZSIsInBhcmFtZXRlcnMiLCJvcmRlckNhbGxCYWNrIiwiZGF0YUNhbGxiYWNrIiwiZHJhd0NhbGxiYWNrIiwidGFibGUiLCJjb2x1bW5zIiwiY29sdW1uIiwib3JkZXJhYmxlIiwicHVzaCIsImNvbmZpZyIsInNlYXJjaCIsImRhdGEiLCJEYXRhVGFibGUiLCJpY29uUGF0aCIsImNsYXNzTmFtZSIsImlucHV0IiwiYXR0ciIsInJlbW92ZUF0dHIiLCJzZXBhcmF0b3IiLCJ0b0xvd2VyQ2FzZSIsInRyaW0iLCJzZXRzIiwidG8iLCJmcm9tIiwiZm9yRWFjaCIsInNldCIsIlJlZ0V4cCIsIm51bSIsInNpemUiLCJzIiwic3Vic3RyIiwic3RyaW5nIiwidG9VcHBlckNhc2UiLCJzbGljZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7OztJQWNNQSxNOzs7Ozs7O2dDQUVpQjtBQUNmLGFBQU9DLENBQUMsQ0FBQyx5QkFBRCxDQUFSO0FBQ0g7OztnQ0FFa0I7QUFDZixhQUFPQSxDQUFDLENBQUMseUJBQUQsQ0FBUjtBQUNIOzs7K0JBRWlCO0FBQ2QsYUFBT0EsQ0FBQyxDQUFDLGVBQUQsQ0FBUjtBQUNIOzs7d0NBRTBCO0FBQ3ZCLGFBQU8sWUFBUDtBQUNIOzs7cUNBRXVCQyxHLEVBQUtDLFEsRUFBVUMsUSxFQUFVO0FBQzdDLFVBQUlKLE1BQU0sQ0FBQ0ssU0FBUCxHQUFtQkMsTUFBbkIsR0FBNEIsQ0FBaEMsRUFBbUM7QUFDL0JMLFNBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCTSxJQUE3QixDQUFrQ0wsR0FBbEM7O0FBRUEsWUFBSUMsUUFBSixFQUFjO0FBQ1ZILGdCQUFNLENBQUNRLFNBQVAsR0FBbUJELElBQW5CLENBQXdCSixRQUF4QjtBQUNIOztBQUVESCxjQUFNLENBQUNRLFNBQVAsR0FBbUJDLEdBQW5CLENBQXVCLE9BQXZCLEVBQWdDQyxLQUFoQyxDQUFzQyxVQUFDQyxLQUFELEVBQVc7QUFDN0NWLFdBQUMsQ0FBQ1UsS0FBSyxDQUFDQyxhQUFQLENBQUQsQ0FBdUJDLElBQXZCLENBQTRCLFVBQTVCLEVBQXdDLElBQXhDO0FBQ0FULGtCQUFRO0FBQ1JKLGdCQUFNLENBQUNLLFNBQVAsR0FBbUJTLFFBQW5CLENBQTRCLE1BQTVCLEVBQW9DQyxLQUFwQyxDQUEwQyxNQUExQztBQUNILFNBSkQ7QUFNQWYsY0FBTSxDQUFDUSxTQUFQLEdBQW1CSyxJQUFuQixDQUF3QixVQUF4QixFQUFvQyxLQUFwQztBQUNBYixjQUFNLENBQUNLLFNBQVAsR0FBbUJTLFFBQW5CLENBQTRCLE1BQTVCLEVBQW9DQyxLQUFwQyxDQUEwQyxNQUExQztBQUNIO0FBQ0o7Ozt1Q0FHeUI7QUFDdEJmLFlBQU0sQ0FBQ0ssU0FBUCxHQUFtQlUsS0FBbkIsQ0FBeUIsTUFBekI7QUFDQWYsWUFBTSxDQUFDUSxTQUFQLEdBQW1CSyxJQUFuQixDQUF3QixVQUF4QixFQUFvQyxLQUFwQztBQUNIOzs7aUNBR21CRyxJLEVBQU1DLEksRUFBTTtBQUM1QixVQUFNQyxJQUFJLEdBQUdDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQUNKLFlBQUksRUFBRUEsSUFBUDtBQUFhQyxZQUFJLEVBQUVBO0FBQW5CLE9BQWYsQ0FBYjtBQUNBakIsWUFBTSxDQUFDcUIsU0FBUCxDQUFpQixPQUFqQixFQUEwQkgsSUFBMUIsRUFBZ0MsSUFBRSxFQUFGLEdBQUssRUFBckM7QUFDSDs7O29DQUdzQkYsSSxFQUFNQyxJLEVBQU07QUFDL0IsVUFBSUssUUFBUSxHQUFHdEIsTUFBTSxDQUFDc0IsUUFBUCxFQUFmO0FBQ0FBLGNBQVEsQ0FBQ0MsR0FBVCxDQUFhLFFBQWIsRUFBdUIsS0FBdkI7QUFDQUQsY0FBUSxDQUFDRSxXQUFULEdBQXVCQyxRQUF2QixDQUFnQyw4QkFBOEJULElBQTlEO0FBQ0FNLGNBQVEsQ0FBQ0ksSUFBVCxDQUFjLE9BQWQsRUFBdUJuQixJQUF2QixDQUE0QlUsSUFBNUI7QUFDQUssY0FBUSxDQUNISyxJQURMLEdBRUtDLE9BRkwsQ0FFYTtBQUFDQyxjQUFNLEVBQUM7QUFBUixPQUZiLEVBRThCLEdBRjlCLEVBR0tDLEtBSEwsQ0FHVyxJQUhYLEVBSUtGLE9BSkwsQ0FJYTtBQUFDQyxjQUFNLEVBQUM7QUFBUixPQUpiLEVBSStCLEdBSi9CLEVBSW9DLE9BSnBDLEVBSTZDLFlBQU07QUFDM0NQLGdCQUFRLENBQUNTLElBQVQ7QUFDSCxPQU5MO0FBT0g7Ozs4QkFHZ0JDLEksRUFBTUMsSyxFQUFPQyxJLEVBQU07QUFDaEMsVUFBSUMsT0FBSjs7QUFFQSxVQUFJRCxJQUFKLEVBQVU7QUFDTixZQUFJRSxJQUFJLEdBQUcsSUFBSUMsSUFBSixFQUFYO0FBQ0FELFlBQUksQ0FBQ0UsT0FBTCxDQUFhRixJQUFJLENBQUNHLE9BQUwsS0FBa0JMLElBQUksR0FBRyxFQUFQLEdBQVksRUFBWixHQUFpQixFQUFqQixHQUFzQixJQUFyRDtBQUNBQyxlQUFPLEdBQUcsZUFBZUMsSUFBSSxDQUFDSSxXQUFMLEVBQXpCO0FBQ0gsT0FKRCxNQUlPO0FBQ0hMLGVBQU8sR0FBRyxFQUFWO0FBQ0g7O0FBQ0RNLGNBQVEsQ0FBQ0MsTUFBVCxHQUFrQkMsa0JBQWtCLENBQUNYLElBQUQsQ0FBbEIsR0FBMkIsR0FBM0IsR0FBaUNXLGtCQUFrQixDQUFDVixLQUFELENBQW5ELEdBQTZERSxPQUE3RCxHQUF1RSxVQUF6RjtBQUNIOzs7OEJBR2dCSCxJLEVBQU07QUFDbkIsVUFBTVksTUFBTSxHQUFHRCxrQkFBa0IsQ0FBQ1gsSUFBRCxDQUFsQixHQUEyQixHQUExQztBQUNBLFVBQU1hLEVBQUUsR0FBR0osUUFBUSxDQUFDQyxNQUFULENBQWdCSSxLQUFoQixDQUFzQixHQUF0QixDQUFYOztBQUNBLFdBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0YsRUFBRSxDQUFDdkMsTUFBdkIsRUFBK0J5QyxDQUFDLEVBQWhDLEVBQW9DO0FBQ2hDLFlBQUlDLENBQUMsR0FBR0gsRUFBRSxDQUFDRSxDQUFELENBQVY7O0FBQ0EsZUFBT0MsQ0FBQyxDQUFDQyxNQUFGLENBQVMsQ0FBVCxNQUFnQixHQUF2QjtBQUE0QkQsV0FBQyxHQUFHQSxDQUFDLENBQUNFLFNBQUYsQ0FBWSxDQUFaLEVBQWVGLENBQUMsQ0FBQzFDLE1BQWpCLENBQUo7QUFBNUI7O0FBQ0EsWUFBSTBDLENBQUMsQ0FBQ0csT0FBRixDQUFVUCxNQUFWLE1BQXNCLENBQTFCLEVBQTZCLE9BQU9RLGtCQUFrQixDQUFDSixDQUFDLENBQUNFLFNBQUYsQ0FBWU4sTUFBTSxDQUFDdEMsTUFBbkIsRUFBMkIwQyxDQUFDLENBQUMxQyxNQUE3QixDQUFELENBQXpCO0FBQ2hDOztBQUNELGFBQU8sSUFBUDtBQUNIOzs7Z0NBR2tCK0MsTyxFQUFTO0FBQ3hCQSxhQUFPLENBQUNDLEVBQVIsQ0FBVyxhQUFYLEVBQTBCLFVBQUMzQyxLQUFELEVBQVFULEdBQVIsRUFBYXFELFNBQWIsRUFBMkI7QUFDakQsWUFBSSxDQUFDckQsR0FBTCxFQUFVO0FBQ05BLGFBQUcsR0FBRyw4QkFBTjtBQUNIOztBQUNERixjQUFNLENBQUN3RCxjQUFQLENBQXNCSCxPQUF0QixFQUErQm5ELEdBQS9CLEVBQW9DLEtBQXBDLEVBQTJDcUQsU0FBM0M7QUFDSCxPQUxELEVBS0dELEVBTEgsQ0FLTSxnQkFMTixFQUt3QixVQUFDM0MsS0FBRCxFQUFRVCxHQUFSLEVBQWFxRCxTQUFiLEVBQTJCO0FBQy9DLFlBQUksQ0FBQ3JELEdBQUwsRUFBVTtBQUNOQSxhQUFHLEdBQUcsdUNBQU47QUFDSDs7QUFDREYsY0FBTSxDQUFDd0QsY0FBUCxDQUFzQkgsT0FBdEIsRUFBK0JuRCxHQUEvQixFQUFvQyxJQUFwQyxFQUEwQ3FELFNBQTFDO0FBQ0gsT0FWRCxFQVVHN0IsSUFWSCxDQVVRLFFBVlIsRUFVa0JoQixLQVZsQixDQVV3QixZQUFNO0FBQzFCMkMsZUFBTyxDQUFDdEIsSUFBUixDQUFhLE1BQWI7QUFDSCxPQVpEO0FBYUg7OzttQ0FHcUJzQixPLEVBQVNuRCxHLEVBQUt1RCxPLEVBQVNGLFMsRUFBVztBQUNwREYsYUFBTyxDQUFDN0IsV0FBUixDQUFvQiw0QkFBcEI7O0FBRUEsVUFBSWlDLE9BQUosRUFBYTtBQUNUSixlQUFPLENBQUM1QixRQUFSLENBQWlCLGVBQWpCO0FBQ0gsT0FGRCxNQUVPO0FBQ0g0QixlQUFPLENBQUM1QixRQUFSLENBQWlCLGNBQWpCO0FBQ0g7O0FBRUQ0QixhQUFPLENBQUMzQixJQUFSLENBQWEsV0FBYixFQUEwQlQsSUFBMUIsQ0FBK0JmLEdBQS9COztBQUVBLFVBQUlxRCxTQUFKLEVBQWU7QUFDWEYsZUFBTyxDQUFDMUIsSUFBUixDQUFhLE1BQWIsRUFBcUJHLEtBQXJCLENBQTJCLElBQTNCLEVBQWlDQyxJQUFqQyxDQUFzQyxNQUF0QztBQUNILE9BRkQsTUFFTztBQUNIc0IsZUFBTyxDQUFDMUIsSUFBUixDQUFhLE1BQWI7QUFDSDtBQUNKOzs7NkJBR2U7QUFDWixhQUFPLHVDQUF1QytCLE9BQXZDLENBQStDLE9BQS9DLEVBQXdELFVBQUNWLENBQUQsRUFBTztBQUNsRSxZQUFJVyxDQUFDLEdBQUdDLElBQUksQ0FBQ0MsTUFBTCxLQUFnQixFQUFoQixHQUFxQixDQUE3QjtBQUFBLFlBQWdDQyxDQUFDLEdBQUdkLENBQUMsSUFBSSxHQUFMLEdBQVdXLENBQVgsR0FBZ0JBLENBQUMsR0FBRyxHQUFKLEdBQVUsR0FBOUQ7QUFDQSxlQUFPRyxDQUFDLENBQUNDLFFBQUYsQ0FBVyxFQUFYLENBQVA7QUFDSCxPQUhNLENBQVA7QUFJSDs7OzhCQUdnQkMsSSxFQUFNQyxNLEVBQVFDLEksRUFBTTtBQUNqQ0MsWUFBTSxDQUFDQyxJQUFQLENBQVk7QUFDUnBELFlBQUksRUFBRSxLQURFO0FBRVJxRCxhQUFLLEVBQUVKLE1BQU0sS0FBSyxPQUZWO0FBR1JLLFdBQUcsRUFBRUMsT0FBTyxDQUFDQyxRQUFSLENBQWlCLGlCQUFqQixFQUFvQztBQUNyQ1IsY0FBSSxFQUFFQSxJQUQrQjtBQUVyQ1Msa0JBQVEsRUFBRWhDLFFBQVEsQ0FBQ2dDLFFBRmtCO0FBR3JDUixnQkFBTSxFQUFDQSxNQUg4QjtBQUlyQ0MsY0FBSSxFQUFFQTtBQUorQixTQUFwQztBQUhHLE9BQVo7QUFVSDs7OytCQUdpQjtBQUNkLFVBQUlsRSxNQUFNLENBQUMwRSxTQUFQLENBQWlCMUUsTUFBTSxDQUFDMkUsaUJBQVAsRUFBakIsS0FBZ0QsQ0FBcEQsRUFBdUQ7QUFDbkQ7QUFDSDs7QUFFRCxVQUFNWCxJQUFJLEdBQUdoRSxNQUFNLENBQUM0RSxNQUFQLEVBQWI7QUFFQTVFLFlBQU0sQ0FBQzZFLFNBQVAsQ0FBaUJiLElBQWpCLEVBQXVCLE9BQXZCLEVBQWdDLEVBQWhDO0FBQ0FjLGlCQUFXLENBQUMsWUFBTTtBQUNkOUUsY0FBTSxDQUFDNkUsU0FBUCxDQUFpQmIsSUFBakIsRUFBdUIsT0FBdkIsRUFBZ0MsRUFBaEM7QUFDSCxPQUZVLEVBRVIsS0FGUSxDQUFYO0FBR0g7OztzQ0FHd0JlLFEsRUFBVUMsZ0IsRUFBa0JDLFksRUFBYztBQUMvRCxVQUFJQyxTQUFTLEdBQUcsQ0FBQyxDQUFqQixDQUQrRCxDQUcvRDs7QUFDQyxVQUFNQyxTQUFTLEdBQUcsRUFBbEI7O0FBRUQsVUFBSSxPQUFPRixZQUFQLElBQXVCLFdBQTNCLEVBQXdDO0FBQ3BDQSxvQkFBWSxHQUFHaEYsQ0FBQyxDQUFDbUYsTUFBRCxDQUFoQjtBQUNILE9BUjhELENBVS9EOzs7QUFDQUwsY0FBUSxDQUFDeEQsR0FBVCxDQUFhLFVBQWIsRUFBeUIsVUFBekI7QUFFQTBELGtCQUFZLENBQUMzQixFQUFiLENBQWdCLFFBQWhCLEVBQTBCLFlBQU07QUFDNUIsWUFBTStCLFNBQVMsR0FBR0osWUFBWSxDQUFDSSxTQUFiLEVBQWxCOztBQUVBLFlBQUlILFNBQVMsR0FBRyxDQUFaLElBQWlCSCxRQUFRLENBQUN4RCxHQUFULENBQWEsU0FBYixLQUEyQixPQUFoRCxFQUF5RDtBQUNyRDJELG1CQUFTLEdBQUdILFFBQVEsQ0FBQ08sTUFBVCxHQUFrQkMsR0FBOUI7QUFDSDs7QUFFRCxZQUFJTCxTQUFTLEdBQUcsQ0FBWixJQUFpQixDQUFDRixnQkFBZ0IsRUFBdEMsRUFBMEM7QUFDdENELGtCQUFRLENBQUNTLElBQVQsQ0FBYyxLQUFkLEVBQXFCLEtBQXJCLEVBQTRCNUQsT0FBNUIsQ0FBb0M7QUFDaEMyRCxlQUFHLEVBQUVGLFNBQVMsR0FBR0gsU0FBWixHQUNDLENBREQsR0FFQ0csU0FBUyxHQUFHSCxTQUFaLEdBQXdCQztBQUhFLFdBQXBDLEVBSUcsQ0FKSDtBQUtIO0FBQ0osT0FkRDtBQWVIOzs7a0NBRW9CTSxNLEVBQVFDLE0sRUFBUUMsYyxFQUFnQjtBQUNqRDtBQUNBMUYsT0FBQyxDQUFDLFFBQVF3RixNQUFSLEdBQWlCLEtBQWxCLENBQUQsQ0FBMEJHLElBQTFCLENBQStCLFVBQUNDLEtBQUQsRUFBUUMsSUFBUixFQUFpQjtBQUM1QyxZQUFJQyxPQUFPLEdBQUc5RixDQUFDLENBQUM2RixJQUFELENBQWY7QUFDQSxZQUFNRSxFQUFFLEdBQUdELE9BQU8sQ0FBQzlFLElBQVIsRUFBWDtBQUNBLFlBQUlnRixHQUFHLEdBQUdGLE9BQU8sQ0FBQ0csTUFBUixFQUFWO0FBRUFqRyxTQUFDLENBQUMyRixJQUFGLENBQU9GLE1BQVAsRUFBZSxVQUFDRyxLQUFELEVBQVFNLEtBQVIsRUFBa0I7QUFDN0JGLGFBQUcsQ0FBQ3ZFLElBQUosQ0FBUyxNQUFNK0QsTUFBTixHQUFlLEdBQWYsR0FBcUJVLEtBQTlCLEVBQXFDNUUsR0FBckMsQ0FBeUMsWUFBekMsRUFBc0QsUUFBdEQ7QUFDSCxTQUZEO0FBSUEsWUFBSTZFLE9BQU8sR0FBR0gsR0FBRyxDQUFDdkUsSUFBSixDQUFTLE1BQU0rRCxNQUFOLEdBQWUsTUFBeEIsQ0FBZCxDQVQ0QyxDQVc1Qzs7QUFDQSxZQUFJVyxPQUFPLENBQUM5RixNQUFSLEdBQWlCLENBQXJCLEVBQXdCO0FBQ3BCOEYsaUJBQU8sQ0FBQzdFLEdBQVIsQ0FBWSxZQUFaLEVBQXlCLE9BQXpCLEVBQWtDaEIsSUFBbEMsQ0FBdUNvRixjQUFjLENBQUNLLEVBQUQsRUFBS0MsR0FBTCxDQUFyRDtBQUNIO0FBQ0osT0FmRDtBQWdCSDs7O29DQUdzQlIsTSxFQUFRWSxNLEVBQVFDLFUsRUFBWUMsVSxFQUFZQyxhLEVBQWVDLFksRUFBY0MsWSxFQUFjO0FBQ3RHLFVBQUlDLEtBQUssR0FBRzFHLENBQUMsQ0FBQyxNQUFNd0YsTUFBTixHQUFlLE1BQWhCLENBQWI7O0FBRUEsVUFBSWtCLEtBQUssQ0FBQ3JHLE1BQU4sR0FBZSxDQUFuQixFQUFzQjtBQUNsQixZQUFJc0csT0FBTyxHQUFHLEVBQWQ7QUFFQTNHLFNBQUMsQ0FBQzJGLElBQUYsQ0FBT1MsTUFBUCxFQUFlLFVBQVVSLEtBQVYsRUFBaUJNLEtBQWpCLEVBQXdCO0FBQ25DLGNBQUlVLE1BQU0sR0FBRztBQUNULG9CQUFRVixLQURDO0FBRVQscUJBQVNWLE1BQU0sR0FBRyxHQUFULEdBQWVVO0FBRmYsV0FBYjs7QUFLQSxjQUFJSyxhQUFKLEVBQW1CO0FBQ2ZLLGtCQUFNLENBQUNDLFNBQVAsR0FBbUJOLGFBQWEsQ0FBQ0wsS0FBRCxDQUFoQztBQUNIOztBQUVEUyxpQkFBTyxDQUFDRyxJQUFSLENBQWFGLE1BQWI7QUFDSCxTQVhEO0FBYUFELGVBQU8sQ0FBQ0csSUFBUixDQUFhO0FBQ1QsdUJBQWEsS0FESjtBQUVULGtCQUFRLElBRkM7QUFHVCxtQkFBU3RCLE1BQU0sR0FBRyxHQUFULEdBQWUsS0FIZjtBQUlULDRCQUFrQjtBQUpULFNBQWI7QUFPQSxZQUFJdUIsTUFBTSxHQUFHO0FBQ1Qsd0JBQWMsSUFETDtBQUVULHdCQUFjLElBRkw7QUFHVCxrQkFBUTtBQUNKLG1CQUFPekMsT0FBTyxDQUFDQyxRQUFSLENBQWlCOEIsVUFBakIsRUFBNkJDLFVBQTdCLENBREg7QUFFSixvQkFBUTtBQUZKLFdBSEM7QUFPVCwwQkFBZ0JHLFlBUFA7QUFRVCxtQkFBUyxDQUFDLENBQUMsQ0FBRCxFQUFJLE1BQUosQ0FBRCxDQVJBO0FBU1QscUJBQVdFLE9BVEY7QUFVVCxzQkFBWTtBQUNSSyxrQkFBTSxFQUFFO0FBREE7QUFWSCxTQUFiOztBQWVBLFlBQUlSLFlBQUosRUFBa0I7QUFDZE8sZ0JBQU0sQ0FBQzVDLElBQVAsQ0FBWThDLElBQVosR0FBbUJULFlBQW5CO0FBQ0g7O0FBRUQsZUFBT0UsS0FBSyxDQUFDUSxTQUFOLENBQWdCSCxNQUFoQixDQUFQO0FBQ0g7O0FBRUQsYUFBTyxJQUFQO0FBQ0g7OzsrQkFFaUI7QUFDZCxhQUFPLFFBQVA7QUFDSDs7O3NDQUV3QjtBQUNyQixhQUFRO0FBQ0osb0JBQVk7QUFDUixrQkFBUSxLQUFLSSxRQUFMLEtBQWtCO0FBRGxCLFNBRFI7QUFJSixvQkFBWTtBQUNSLGtCQUFRLEtBQUtBLFFBQUwsS0FBa0I7QUFEbEIsU0FKUjtBQU9KLG1CQUFXO0FBQ1Asa0JBQVEsS0FBS0EsUUFBTCxLQUFrQjtBQURuQjtBQVBQLE9BQVI7QUFXSDs7OzhCQUVnQkMsUyxFQUFXO0FBQ3hCcEgsT0FBQyxDQUFDLE1BQU1vSCxTQUFQLENBQUQsQ0FBbUJ6QixJQUFuQixDQUF3QixVQUFDQyxLQUFELEVBQVF5QixLQUFSLEVBQWtCO0FBQ3RDLFlBQU12QixPQUFPLEdBQUc5RixDQUFDLENBQUNxSCxLQUFELENBQWpCO0FBQ0F2QixlQUFPLENBQUNHLE1BQVIsR0FBaUJ2RSxJQUFqQjtBQUNBb0UsZUFBTyxDQUFDbEYsSUFBUixDQUFhLFVBQWIsRUFBeUJrRixPQUFPLENBQUN3QixJQUFSLENBQWEsZUFBYixNQUFrQyxNQUEzRDtBQUNILE9BSkQ7QUFLSDs7OzhCQUVnQkYsUyxFQUFXO0FBQ3hCcEgsT0FBQyxDQUFDLE1BQU1vSCxTQUFQLENBQUQsQ0FBbUJ6QixJQUFuQixDQUF3QixVQUFDQyxLQUFELEVBQVF5QixLQUFSLEVBQWtCO0FBQ3RDLFlBQU12QixPQUFPLEdBQUc5RixDQUFDLENBQUNxSCxLQUFELENBQWpCO0FBQ0F2QixlQUFPLENBQUNHLE1BQVIsR0FBaUJuRSxJQUFqQjtBQUNBZ0UsZUFBTyxDQUFDd0IsSUFBUixDQUFhLGVBQWIsRUFBOEJ4QixPQUFPLENBQUNsRixJQUFSLENBQWEsVUFBYixDQUE5QjtBQUNBa0YsZUFBTyxDQUFDeUIsVUFBUixDQUFtQixVQUFuQjtBQUNILE9BTEQ7QUFNSDs7OzRCQUVjdkcsSSxFQUFNd0csUyxFQUFXO0FBQzVCeEcsVUFBSSxHQUFHQSxJQUFJLENBQUM4QyxRQUFMLEdBQWdCMkQsV0FBaEIsR0FBOEJDLElBQTlCLEVBQVA7QUFFQSxVQUFNQyxJQUFJLEdBQUcsQ0FDVDtBQUFDQyxVQUFFLEVBQUUsR0FBTDtBQUFVQyxZQUFJLEVBQUU7QUFBaEIsT0FEUyxFQUVUO0FBQUNELFVBQUUsRUFBRSxHQUFMO0FBQVVDLFlBQUksRUFBRTtBQUFoQixPQUZTLEVBR1Q7QUFBQ0QsVUFBRSxFQUFFLEdBQUw7QUFBVUMsWUFBSSxFQUFFO0FBQWhCLE9BSFMsRUFJVDtBQUFDRCxVQUFFLEVBQUUsR0FBTDtBQUFVQyxZQUFJLEVBQUU7QUFBaEIsT0FKUyxFQUtUO0FBQUNELFVBQUUsRUFBRSxHQUFMO0FBQVVDLFlBQUksRUFBRTtBQUFoQixPQUxTLEVBTVQ7QUFBQ0QsVUFBRSxFQUFFLEdBQUw7QUFBVUMsWUFBSSxFQUFFO0FBQWhCLE9BTlMsRUFPVDtBQUFDRCxVQUFFLEVBQUUsR0FBTDtBQUFVQyxZQUFJLEVBQUU7QUFBaEIsT0FQUyxFQVFUO0FBQUNELFVBQUUsRUFBRSxHQUFMO0FBQVVDLFlBQUksRUFBRTtBQUFoQixPQVJTLEVBU1Q7QUFBQ0QsVUFBRSxFQUFFLElBQUw7QUFBV0MsWUFBSSxFQUFFO0FBQWpCLE9BVFMsRUFVVDtBQUFDRCxVQUFFLEVBQUUsR0FBTDtBQUFVQyxZQUFJLEVBQUU7QUFBaEIsT0FWUyxFQVdUO0FBQUNELFVBQUUsRUFBRSxHQUFMO0FBQVVDLFlBQUksRUFBRTtBQUFoQixPQVhTLEVBWVQ7QUFBQ0QsVUFBRSxFQUFFLEdBQUw7QUFBVUMsWUFBSSxFQUFFO0FBQWhCLE9BWlMsRUFhVDtBQUFDRCxVQUFFLEVBQUUsR0FBTDtBQUFVQyxZQUFJLEVBQUU7QUFBaEIsT0FiUyxFQWNUO0FBQUNELFVBQUUsRUFBRSxHQUFMO0FBQVVDLFlBQUksRUFBRTtBQUFoQixPQWRTLEVBZVQ7QUFBQ0QsVUFBRSxFQUFFLElBQUw7QUFBV0MsWUFBSSxFQUFFO0FBQWpCLE9BZlMsRUFnQlQ7QUFBQ0QsVUFBRSxFQUFFLEdBQUw7QUFBVUMsWUFBSSxFQUFFO0FBQWhCLE9BaEJTLEVBaUJUO0FBQUNELFVBQUUsRUFBRSxHQUFMO0FBQVVDLFlBQUksRUFBRTtBQUFoQixPQWpCUyxFQWtCVDtBQUFDRCxVQUFFLEVBQUUsR0FBTDtBQUFVQyxZQUFJLEVBQUU7QUFBaEIsT0FsQlMsRUFtQlQ7QUFBQ0QsVUFBRSxFQUFFLEdBQUw7QUFBVUMsWUFBSSxFQUFFO0FBQWhCLE9BbkJTLEVBb0JUO0FBQUNELFVBQUUsRUFBRSxHQUFMO0FBQVVDLFlBQUksRUFBRTtBQUFoQixPQXBCUyxFQXFCVDtBQUFDRCxVQUFFLEVBQUUsR0FBTDtBQUFVQyxZQUFJLEVBQUU7QUFBaEIsT0FyQlMsRUFzQlQ7QUFBQ0QsVUFBRSxFQUFFLEdBQUw7QUFBVUMsWUFBSSxFQUFFO0FBQWhCLE9BdEJTLEVBdUJUO0FBQUNELFVBQUUsRUFBRSxHQUFMO0FBQVVDLFlBQUksRUFBRTtBQUFoQixPQXZCUyxFQXdCVDtBQUFDRCxVQUFFLEVBQUUsR0FBTDtBQUFVQyxZQUFJLEVBQUU7QUFBaEIsT0F4QlMsRUF5QlQ7QUFBQ0QsVUFBRSxFQUFFLEdBQUw7QUFBVUMsWUFBSSxFQUFFO0FBQWhCLE9BekJTLENBQWI7QUE0QkFGLFVBQUksQ0FBQ0csT0FBTCxDQUFhLFVBQUFDLEdBQUcsRUFBSTtBQUNoQi9HLFlBQUksR0FBR0EsSUFBSSxDQUFDeUMsT0FBTCxDQUFhLElBQUl1RSxNQUFKLENBQVdELEdBQUcsQ0FBQ0YsSUFBZixFQUFvQixJQUFwQixDQUFiLEVBQXdDRSxHQUFHLENBQUNILEVBQTVDLENBQVA7QUFDSCxPQUZEO0FBSUE1RyxVQUFJLEdBQUdBLElBQUksQ0FBQzhDLFFBQUwsR0FBZ0IyRCxXQUFoQixHQUNGaEUsT0FERSxDQUNNLE1BRE4sRUFDYyxHQURkLEVBQzJCO0FBRDNCLE9BRUZBLE9BRkUsQ0FFTSxJQUZOLEVBRVksT0FGWixFQUUyQjtBQUYzQixPQUdGQSxPQUhFLENBR00sV0FITixFQUdtQixFQUhuQixFQUcyQjtBQUgzQixPQUlGQSxPQUpFLENBSU0sT0FKTixFQUllLEdBSmYsRUFJMkI7QUFKM0IsT0FLRkEsT0FMRSxDQUtNLEtBTE4sRUFLYSxFQUxiLEVBSzJCO0FBTDNCLE9BTUZBLE9BTkUsQ0FNTSxLQU5OLEVBTWEsRUFOYixDQUFQLENBbkM0QixDQXlDTTs7QUFFbEMsVUFBSyxPQUFPK0QsU0FBUCxLQUFxQixXQUF0QixJQUF1Q0EsU0FBUyxLQUFLLEdBQXpELEVBQStEO0FBQzNEeEcsWUFBSSxHQUFHQSxJQUFJLENBQUN5QyxPQUFMLENBQWEsSUFBYixFQUFtQitELFNBQW5CLENBQVA7QUFDSDs7QUFFRCxhQUFPeEcsSUFBUDtBQUNIOzs7d0JBRVVpSCxHLEVBQUtDLEksRUFBTTtBQUNsQixVQUFNQyxDQUFDLEdBQUcsY0FBY0YsR0FBeEI7QUFDQSxhQUFPRSxDQUFDLENBQUNDLE1BQUYsQ0FBU0QsQ0FBQyxDQUFDOUgsTUFBRixHQUFXNkgsSUFBcEIsQ0FBUDtBQUNIOzs7NEJBRWNHLE0sRUFBUTtBQUNuQixhQUFPQSxNQUFNLENBQUNyRixNQUFQLENBQWMsQ0FBZCxFQUFpQnNGLFdBQWpCLEtBQWlDRCxNQUFNLENBQUNFLEtBQVAsQ0FBYSxDQUFiLENBQXhDO0FBQ0g7Ozs7OztBQUdVeEkscUVBQWYsRSIsImZpbGUiOiJBZG1pbi9FbWFpbEN0cmx+QWRtaW4vRmlsZUF1ZGl0Q3RybH5BcmNoaXZlL0NhdGVnb3JpemF0aW9uQ3RybH5BcmNoaXZlL1JlY29yZH5BcmNoaXZlL1NlYXJjaEN0cmx+QXV0fmQ0NWYzZmM2LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKiAgQ29weXJpZ2h0IG5vdGljZVxyXG4gKlxyXG4gKiAgKGMpIDIwMTggRlRLIGUuVi4gPGZ0ay5kZT5cclxuICogIEFsbCByaWdodHMgcmVzZXJ2ZWRcclxuICpcclxuICogIFRoaXMgc2NyaXB0IGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAqICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gKiAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gKiAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuICpcclxuICogIFRoaXMgY29weXJpZ2h0IG5vdGljZSBNVVNUIEFQUEVBUiBpbiBhbGwgY29waWVzIG9mIHRoZSBzY3JpcHQhXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG5jbGFzcyBDb21tb24ge1xyXG5cclxuICAgIHN0YXRpYyBkZWxldGVCdG4oKSB7XHJcbiAgICAgICAgcmV0dXJuICQoJyNkZWxldGVDb25maXJtYXRpb24tYnRuJyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGRlbGV0ZURpdigpIHtcclxuICAgICAgICByZXR1cm4gJCgnI2RlbGV0ZUNvbmZpcm1hdGlvbi1kaXYnKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgYWxlcnREaXYoKSB7XHJcbiAgICAgICAgcmV0dXJuICQoJyNhbGVydC1kaWFsb2cnKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgQ09PS0lFX0RPTk9UVFJBQ0soKSB7XHJcbiAgICAgICAgcmV0dXJuICdEb05vdFRyYWNrJztcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgc2hvd0NvbmZpcm1hdGlvbihtc2csIGJ0blRpdGxlLCBjYWxsQmFjaykge1xyXG4gICAgICAgIGlmIChDb21tb24uZGVsZXRlRGl2KCkubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAkKCcjZGVsZXRlQ29uZmlybWF0aW9uLW1zZycpLmh0bWwobXNnKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChidG5UaXRsZSkge1xyXG4gICAgICAgICAgICAgICAgQ29tbW9uLmRlbGV0ZUJ0bigpLmh0bWwoYnRuVGl0bGUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBDb21tb24uZGVsZXRlQnRuKCkub2ZmKCdjbGljaycpLmNsaWNrKChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KS5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgY2FsbEJhY2soKTtcclxuICAgICAgICAgICAgICAgIENvbW1vbi5kZWxldGVEaXYoKS5hcHBlbmRUbyhcImJvZHlcIikubW9kYWwoJ2hpZGUnKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBDb21tb24uZGVsZXRlQnRuKCkucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIENvbW1vbi5kZWxldGVEaXYoKS5hcHBlbmRUbyhcImJvZHlcIikubW9kYWwoJ3Nob3cnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuICAgIHN0YXRpYyBoaWRlQ29uZmlybWF0aW9uKCkge1xyXG4gICAgICAgIENvbW1vbi5kZWxldGVEaXYoKS5tb2RhbCgnaGlkZScpO1xyXG4gICAgICAgIENvbW1vbi5kZWxldGVCdG4oKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgc3RhdGljIHByZXBhcmVBbGVydCh0eXBlLCB0ZXh0KSB7XHJcbiAgICAgICAgY29uc3QganNvbiA9IEpTT04uc3RyaW5naWZ5KHt0eXBlOiB0eXBlLCB0ZXh0OiB0ZXh0fSk7XHJcbiAgICAgICAgQ29tbW9uLnNldENvb2tpZSgnZmxhc2gnLCBqc29uLCAxLzI0LzYwKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgc3RhdGljIHNob3dBbGVydERpYWxvZyh0eXBlLCB0ZXh0KSB7XHJcbiAgICAgICAgbGV0IGFsZXJ0RGl2ID0gQ29tbW9uLmFsZXJ0RGl2KCk7XHJcbiAgICAgICAgYWxlcnREaXYuY3NzKCdib3R0b20nLCAnMHB4Jyk7XHJcbiAgICAgICAgYWxlcnREaXYucmVtb3ZlQ2xhc3MoKS5hZGRDbGFzcygnYWxlcnQtYm90dG9tIGFsZXJ0IGFsZXJ0LScgKyB0eXBlKTtcclxuICAgICAgICBhbGVydERpdi5maW5kKCcudGV4dCcpLmh0bWwodGV4dCk7XHJcbiAgICAgICAgYWxlcnREaXZcclxuICAgICAgICAgICAgLnNob3coKVxyXG4gICAgICAgICAgICAuYW5pbWF0ZSh7Ym90dG9tOic1MHB4J30sIDUwMClcclxuICAgICAgICAgICAgLmRlbGF5KDUwMDApXHJcbiAgICAgICAgICAgIC5hbmltYXRlKHtib3R0b206Jy01MHB4J30sIDUwMCwgJ3N3aW5nJywgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgYWxlcnREaXYuaGlkZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgc3RhdGljIHNldENvb2tpZShuYW1lLCB2YWx1ZSwgZGF5cykge1xyXG4gICAgICAgIGxldCBleHBpcmVzO1xyXG5cclxuICAgICAgICBpZiAoZGF5cykge1xyXG4gICAgICAgICAgICBsZXQgZGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgICAgIGRhdGUuc2V0VGltZShkYXRlLmdldFRpbWUoKSArIChkYXlzICogMjQgKiA2MCAqIDYwICogMTAwMCkpO1xyXG4gICAgICAgICAgICBleHBpcmVzID0gXCI7IGV4cGlyZXM9XCIgKyBkYXRlLnRvR01UU3RyaW5nKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZXhwaXJlcyA9IFwiXCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRvY3VtZW50LmNvb2tpZSA9IGVuY29kZVVSSUNvbXBvbmVudChuYW1lKSArIFwiPVwiICsgZW5jb2RlVVJJQ29tcG9uZW50KHZhbHVlKSArIGV4cGlyZXMgKyBcIjsgcGF0aD0vXCI7XHJcbiAgICB9O1xyXG5cclxuXHJcbiAgICBzdGF0aWMgZ2V0Q29va2llKG5hbWUpIHtcclxuICAgICAgICBjb25zdCBuYW1lRVEgPSBlbmNvZGVVUklDb21wb25lbnQobmFtZSkgKyBcIj1cIjtcclxuICAgICAgICBjb25zdCBjYSA9IGRvY3VtZW50LmNvb2tpZS5zcGxpdCgnOycpO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2EubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IGMgPSBjYVtpXTtcclxuICAgICAgICAgICAgd2hpbGUgKGMuY2hhckF0KDApID09PSAnICcpIGMgPSBjLnN1YnN0cmluZygxLCBjLmxlbmd0aCk7XHJcbiAgICAgICAgICAgIGlmIChjLmluZGV4T2YobmFtZUVRKSA9PT0gMCkgcmV0dXJuIGRlY29kZVVSSUNvbXBvbmVudChjLnN1YnN0cmluZyhuYW1lRVEubGVuZ3RoLCBjLmxlbmd0aCkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH07XHJcblxyXG5cclxuICAgIHN0YXRpYyBpbml0TWVzc2FnZShpbmZvRGl2KSB7XHJcbiAgICAgICAgaW5mb0Rpdi5vbignYWN0aW9uLmZhaWwnLCAoZXZlbnQsIG1zZywgYXV0b0Nsb3NlKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghbXNnKSB7XHJcbiAgICAgICAgICAgICAgICBtc2cgPSAnVW5hYmxlIHRvIHBlcmZvcm0gdGhlIGFjdGlvbic7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgQ29tbW9uLnByZXBhcmVNZXNzYWdlKGluZm9EaXYsIG1zZywgZmFsc2UsIGF1dG9DbG9zZSk7XHJcbiAgICAgICAgfSkub24oJ2FjdGlvbi5zdWNjZXNzJywgKGV2ZW50LCBtc2csIGF1dG9DbG9zZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIW1zZykge1xyXG4gICAgICAgICAgICAgICAgbXNnID0gJ1RoZSBhY3Rpb24gd2FzIHBlcmZvcm1lZCBzdWNjZXNzZnVsbHknO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIENvbW1vbi5wcmVwYXJlTWVzc2FnZShpbmZvRGl2LCBtc2csIHRydWUsIGF1dG9DbG9zZSk7XHJcbiAgICAgICAgfSkuZmluZCgnLmNsb3NlJykuY2xpY2soKCkgPT4ge1xyXG4gICAgICAgICAgICBpbmZvRGl2LmhpZGUoJ2Zhc3QnKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgc3RhdGljIHByZXBhcmVNZXNzYWdlKGluZm9EaXYsIG1zZywgc3VjY2VzcywgYXV0b0Nsb3NlKSB7XHJcbiAgICAgICAgaW5mb0Rpdi5yZW1vdmVDbGFzcygnYWxlcnQtZGFuZ2VyIGFsZXJ0LXN1Y2Nlc3MnKTtcclxuXHJcbiAgICAgICAgaWYgKHN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgaW5mb0Rpdi5hZGRDbGFzcygnYWxlcnQtc3VjY2VzcycpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGluZm9EaXYuYWRkQ2xhc3MoJ2FsZXJ0LWRhbmdlcicpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW5mb0Rpdi5maW5kKCcubXNnLXRleHQnKS50ZXh0KG1zZyk7XHJcblxyXG4gICAgICAgIGlmIChhdXRvQ2xvc2UpIHtcclxuICAgICAgICAgICAgaW5mb0Rpdi5zaG93KCdmYXN0JykuZGVsYXkoNTAwMCkuaGlkZSgnc2xvdycpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGluZm9EaXYuc2hvdygnc2xvdycpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG4gICAgc3RhdGljIHV1aWR2NCgpIHtcclxuICAgICAgICByZXR1cm4gJ3h4eHh4eHh4LXh4eHgtNHh4eC15eHh4LXh4eHh4eHh4eHh4eCcucmVwbGFjZSgvW3h5XS9nLCAoYykgPT4ge1xyXG4gICAgICAgICAgICBsZXQgciA9IE1hdGgucmFuZG9tKCkgKiAxNiB8IDAsIHYgPSBjID09ICd4JyA/IHIgOiAociAmIDB4MyB8IDB4OCk7XHJcbiAgICAgICAgICAgIHJldHVybiB2LnRvU3RyaW5nKDE2KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgc3RhdGljIHVwZGF0ZUxvZyh1dWlkLCBhY3Rpb24sIG5vdGUpIHtcclxuICAgICAgICBqUXVlcnkuYWpheCh7XHJcbiAgICAgICAgICAgIHR5cGU6IFwiR0VUXCIsXHJcbiAgICAgICAgICAgIGFzeW5jOiBhY3Rpb24gPT09ICd2aXNpdCcsXHJcbiAgICAgICAgICAgIHVybDogUm91dGluZy5nZW5lcmF0ZSgnY29yZV9sb2dfdXBkYXRlJywge1xyXG4gICAgICAgICAgICAgICAgdXVpZDogdXVpZCxcclxuICAgICAgICAgICAgICAgIHJlZmVycmVyOiBkb2N1bWVudC5yZWZlcnJlcixcclxuICAgICAgICAgICAgICAgIGFjdGlvbjphY3Rpb24sXHJcbiAgICAgICAgICAgICAgICBub3RlOiBub3RlXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIHN0YXRpYyBzdGFydExvZygpIHtcclxuICAgICAgICBpZiAoQ29tbW9uLmdldENvb2tpZShDb21tb24uQ09PS0lFX0RPTk9UVFJBQ0soKSkgPT0gMSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB1dWlkID0gQ29tbW9uLnV1aWR2NCgpO1xyXG5cclxuICAgICAgICBDb21tb24udXBkYXRlTG9nKHV1aWQsICd2aXNpdCcsICcnKTtcclxuICAgICAgICBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgICAgICAgIENvbW1vbi51cGRhdGVMb2codXVpZCwgJ3Zpc2l0JywgJycpO1xyXG4gICAgICAgIH0sIDMwMDAwKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgc3RhdGljIGluaXRQYW5lbFBvc2l0aW9uKHBhbmVsRGl2LCBpc1Bpbm5lZEZ1bmN0aW9uLCBwYXJlbnRXaW5kb3cpIHtcclxuICAgICAgICBsZXQgb3JpZ2luYWxZID0gLTE7XHJcblxyXG4gICAgICAgIC8vIFNwYWNlIGJldHdlZW4gZWxlbWVudCBhbmQgdG9wIG9mIHNjcmVlbiAod2hlbiBzY3JvbGxpbmcpXHJcbiAgICAgICAgIGNvbnN0IHRvcE1hcmdpbiA9IDUwO1xyXG5cclxuICAgICAgICBpZiAodHlwZW9mIHBhcmVudFdpbmRvdyA9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICBwYXJlbnRXaW5kb3cgPSAkKHdpbmRvdyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTaG91bGQgcHJvYmFibHkgYmUgc2V0IGluIENTUzsgYnV0IGhlcmUganVzdCBmb3IgZW1waGFzaXNcclxuICAgICAgICBwYW5lbERpdi5jc3MoJ3Bvc2l0aW9uJywgJ3JlbGF0aXZlJyk7XHJcblxyXG4gICAgICAgIHBhcmVudFdpbmRvdy5vbignc2Nyb2xsJywgKCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzY3JvbGxUb3AgPSBwYXJlbnRXaW5kb3cuc2Nyb2xsVG9wKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAob3JpZ2luYWxZIDwgMCAmJiBwYW5lbERpdi5jc3MoJ2Rpc3BsYXknKSA9PSAnYmxvY2snKSB7XHJcbiAgICAgICAgICAgICAgICBvcmlnaW5hbFkgPSBwYW5lbERpdi5vZmZzZXQoKS50b3A7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChvcmlnaW5hbFkgPiAwICYmICFpc1Bpbm5lZEZ1bmN0aW9uKCkpIHtcclxuICAgICAgICAgICAgICAgIHBhbmVsRGl2LnN0b3AoZmFsc2UsIGZhbHNlKS5hbmltYXRlKHtcclxuICAgICAgICAgICAgICAgICAgICB0b3A6IHNjcm9sbFRvcCA8IG9yaWdpbmFsWVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA/IDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgOiBzY3JvbGxUb3AgLSBvcmlnaW5hbFkgKyB0b3BNYXJnaW5cclxuICAgICAgICAgICAgICAgIH0sIDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIHJlUmVuZGVyVGFibGUocHJlZml4LCBjZW50ZXIsIGJ1dHRvbkZ1bmN0aW9uKSB7XHJcbiAgICAgICAgLy9yZW5kZXIgcm93XHJcbiAgICAgICAgJCgndGQuJyArIHByZWZpeCArICctaWQnKS5lYWNoKChpbmRleCwgaXRlbSkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgZWxlbWVudCA9ICQoaXRlbSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGlkID0gZWxlbWVudC50ZXh0KCk7XHJcbiAgICAgICAgICAgIGxldCByb3cgPSBlbGVtZW50LnBhcmVudCgpO1xyXG5cclxuICAgICAgICAgICAgJC5lYWNoKGNlbnRlciwgKGluZGV4LCBmaWVsZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcm93LmZpbmQoJy4nICsgcHJlZml4ICsgJy0nICsgZmllbGQpLmNzcygndGV4dC1hbGlnbicsJ2NlbnRlcicpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGxldCBidXR0b25zID0gcm93LmZpbmQoJy4nICsgcHJlZml4ICsgJy1idG4nKTtcclxuXHJcbiAgICAgICAgICAgIC8vcmVuZGVyIGJ1dHRvblxyXG4gICAgICAgICAgICBpZiAoYnV0dG9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBidXR0b25zLmNzcygndGV4dC1hbGlnbicsJ3JpZ2h0JykuaHRtbChidXR0b25GdW5jdGlvbihpZCwgcm93KSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgc3RhdGljIGNyZWF0ZURhdGFUYWJsZShwcmVmaXgsIGZpZWxkcywgdGFibGVSb3V0ZSwgcGFyYW1ldGVycywgb3JkZXJDYWxsQmFjaywgZGF0YUNhbGxiYWNrLCBkcmF3Q2FsbGJhY2spIHtcclxuICAgICAgICBsZXQgdGFibGUgPSAkKCcjJyArIHByZWZpeCArICctdGJsJyk7XHJcblxyXG4gICAgICAgIGlmICh0YWJsZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGxldCBjb2x1bW5zID0gW107XHJcblxyXG4gICAgICAgICAgICAkLmVhY2goZmllbGRzLCBmdW5jdGlvbiAoaW5kZXgsIGZpZWxkKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgY29sdW1uID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZGF0YVwiOiBmaWVsZCxcclxuICAgICAgICAgICAgICAgICAgICBcImNsYXNzXCI6IHByZWZpeCArICctJyArIGZpZWxkLFxyXG4gICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAob3JkZXJDYWxsQmFjaykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbHVtbi5vcmRlcmFibGUgPSBvcmRlckNhbGxCYWNrKGZpZWxkKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBjb2x1bW5zLnB1c2goY29sdW1uKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBjb2x1bW5zLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgXCJvcmRlcmFibGVcIjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBcImRhdGFcIjogbnVsbCxcclxuICAgICAgICAgICAgICAgIFwiY2xhc3NcIjogcHJlZml4ICsgJy0nICsgJ2J0bicsXHJcbiAgICAgICAgICAgICAgICBcImRlZmF1bHRDb250ZW50XCI6ICcnXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgbGV0IGNvbmZpZyA9IHtcclxuICAgICAgICAgICAgICAgIFwicHJvY2Vzc2luZ1wiOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgXCJzZXJ2ZXJTaWRlXCI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBcImFqYXhcIjoge1xyXG4gICAgICAgICAgICAgICAgICAgIFwidXJsXCI6IFJvdXRpbmcuZ2VuZXJhdGUodGFibGVSb3V0ZSwgcGFyYW1ldGVycyksXHJcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIFwiZHJhd0NhbGxiYWNrXCI6IGRyYXdDYWxsYmFjayxcclxuICAgICAgICAgICAgICAgIFwib3JkZXJcIjogW1swLCBcImRlc2NcIl1dLFxyXG4gICAgICAgICAgICAgICAgXCJjb2x1bW5zXCI6IGNvbHVtbnMsXHJcbiAgICAgICAgICAgICAgICBcImxhbmd1YWdlXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICBzZWFyY2g6IFwiPHNwYW4gY2xhc3M9J2ZhcyBmYS1zZWFyY2gnPjwvc3Bhbj4gX0lOUFVUX1wiXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBpZiAoZGF0YUNhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25maWcuYWpheC5kYXRhID0gZGF0YUNhbGxiYWNrO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdGFibGUuRGF0YVRhYmxlKGNvbmZpZyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaWNvblBhdGgoKSB7XHJcbiAgICAgICAgcmV0dXJuICcvaWNvbi8nO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBnZXRUYXhvbm9teUljb24oKSB7XHJcbiAgICAgICAgcmV0dXJuICB7XHJcbiAgICAgICAgICAgIFwidGF4b25vbXlcIjoge1xyXG4gICAgICAgICAgICAgICAgXCJpY29uXCI6IHRoaXMuaWNvblBhdGgoKSArIFwidGV4dF9hbGlnbl9qdXN0aWZ5LnBuZ1wiXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFwiY2F0ZWdvcnlcIjoge1xyXG4gICAgICAgICAgICAgICAgXCJpY29uXCI6IHRoaXMuaWNvblBhdGgoKSArIFwiZm9sZGVyLnBuZ1wiXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFwiZGVmYXVsdFwiOiB7XHJcbiAgICAgICAgICAgICAgICBcImljb25cIjogdGhpcy5pY29uUGF0aCgpICsgXCJwYWdlLnBuZ1wiXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgc2hvd0lucHV0KGNsYXNzTmFtZSkge1xyXG4gICAgICAgICQoJy4nICsgY2xhc3NOYW1lKS5lYWNoKChpbmRleCwgaW5wdXQpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgZWxlbWVudCA9ICQoaW5wdXQpO1xyXG4gICAgICAgICAgICBlbGVtZW50LnBhcmVudCgpLnNob3coKTtcclxuICAgICAgICAgICAgZWxlbWVudC5wcm9wKCdyZXF1aXJlZCcsIGVsZW1lbnQuYXR0cignZGF0YS1yZXF1aXJlZCcpID09PSAndHJ1ZScpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoaWRlSW5wdXQoY2xhc3NOYW1lKSB7XHJcbiAgICAgICAgJCgnLicgKyBjbGFzc05hbWUpLmVhY2goKGluZGV4LCBpbnB1dCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBlbGVtZW50ID0gJChpbnB1dCk7XHJcbiAgICAgICAgICAgIGVsZW1lbnQucGFyZW50KCkuaGlkZSgpO1xyXG4gICAgICAgICAgICBlbGVtZW50LmF0dHIoJ2RhdGEtcmVxdWlyZWQnLCBlbGVtZW50LnByb3AoJ3JlcXVpcmVkJykpO1xyXG4gICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHIoJ3JlcXVpcmVkJyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIHNsdWdpZnkodGV4dCwgc2VwYXJhdG9yKSB7XHJcbiAgICAgICAgdGV4dCA9IHRleHQudG9TdHJpbmcoKS50b0xvd2VyQ2FzZSgpLnRyaW0oKTtcclxuXHJcbiAgICAgICAgY29uc3Qgc2V0cyA9IFtcclxuICAgICAgICAgICAge3RvOiAnYScsIGZyb206ICdbw4DDgcOCw4PDhMOFw4bEgMSCxIThuqDhuqLhuqThuqbhuqjhuqrhuqzhuq7hurDhurLhurThurZdJ30sXHJcbiAgICAgICAgICAgIHt0bzogJ2MnLCBmcm9tOiAnW8OHxIbEiMSMXSd9LFxyXG4gICAgICAgICAgICB7dG86ICdkJywgZnJvbTogJ1vDkMSOxJDDnl0nfSxcclxuICAgICAgICAgICAge3RvOiAnZScsIGZyb206ICdbw4jDicOKw4vEksSUxJbEmMSa4bq44bq64bq84bq+4buA4buC4buE4buGXSd9LFxyXG4gICAgICAgICAgICB7dG86ICdnJywgZnJvbTogJ1vEnMSexKLHtF0nfSxcclxuICAgICAgICAgICAge3RvOiAnaCcsIGZyb206ICdbxKThuKZdJ30sXHJcbiAgICAgICAgICAgIHt0bzogJ2knLCBmcm9tOiAnW8OMw43DjsOPxKjEqsSuxLDhu4jhu4pdJ30sXHJcbiAgICAgICAgICAgIHt0bzogJ2onLCBmcm9tOiAnW8S0XSd9LFxyXG4gICAgICAgICAgICB7dG86ICdpaicsIGZyb206ICdbxLJdJ30sXHJcbiAgICAgICAgICAgIHt0bzogJ2snLCBmcm9tOiAnW8S2XSd9LFxyXG4gICAgICAgICAgICB7dG86ICdsJywgZnJvbTogJ1vEucS7xL3FgV0nfSxcclxuICAgICAgICAgICAge3RvOiAnbScsIGZyb206ICdb4bi+XSd9LFxyXG4gICAgICAgICAgICB7dG86ICduJywgZnJvbTogJ1vDkcWDxYXFh10nfSxcclxuICAgICAgICAgICAge3RvOiAnbycsIGZyb206ICdbw5LDk8OUw5XDlsOYxYzFjsWQ4buM4buO4buQ4buS4buU4buW4buY4bua4buc4bue4bug4buix6rHrMagXSd9LFxyXG4gICAgICAgICAgICB7dG86ICdvZScsIGZyb206ICdbxZJdJ30sXHJcbiAgICAgICAgICAgIHt0bzogJ3AnLCBmcm9tOiAnW+G5lV0nfSxcclxuICAgICAgICAgICAge3RvOiAncicsIGZyb206ICdbxZTFlsWYXSd9LFxyXG4gICAgICAgICAgICB7dG86ICdzJywgZnJvbTogJ1vDn8WaxZzFnsWgXSd9LFxyXG4gICAgICAgICAgICB7dG86ICd0JywgZnJvbTogJ1vFosWkXSd9LFxyXG4gICAgICAgICAgICB7dG86ICd1JywgZnJvbTogJ1vDmcOaw5vDnMWoxarFrMWuxbDFsuG7pOG7puG7qOG7quG7rOG7ruG7sMavXSd9LFxyXG4gICAgICAgICAgICB7dG86ICd3JywgZnJvbTogJ1vhuoLFtOG6gOG6hF0nfSxcclxuICAgICAgICAgICAge3RvOiAneCcsIGZyb206ICdb4bqNXSd9LFxyXG4gICAgICAgICAgICB7dG86ICd5JywgZnJvbTogJ1vDncW2xbjhu7Lhu7Thu7bhu7hdJ30sXHJcbiAgICAgICAgICAgIHt0bzogJ3onLCBmcm9tOiAnW8W5xbvFvV0nfSxcclxuICAgICAgICAgICAge3RvOiAnLScsIGZyb206ICdbwrcvXyw6O1xcJ10nfVxyXG4gICAgICAgIF07XHJcblxyXG4gICAgICAgIHNldHMuZm9yRWFjaChzZXQgPT4ge1xyXG4gICAgICAgICAgICB0ZXh0ID0gdGV4dC5yZXBsYWNlKG5ldyBSZWdFeHAoc2V0LmZyb20sJ2dpJyksIHNldC50byk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRleHQgPSB0ZXh0LnRvU3RyaW5nKCkudG9Mb3dlckNhc2UoKVxyXG4gICAgICAgICAgICAucmVwbGFjZSgvXFxzKy9nLCAnLScpICAgICAgICAgLy8gUmVwbGFjZSBzcGFjZXMgd2l0aCAtXHJcbiAgICAgICAgICAgIC5yZXBsYWNlKC8mL2csICctYW5kLScpICAgICAgIC8vIFJlcGxhY2UgJiB3aXRoICdhbmQnXHJcbiAgICAgICAgICAgIC5yZXBsYWNlKC9bXlxcd1xcLV0rL2csICcnKSAgICAgLy8gUmVtb3ZlIGFsbCBub24td29yZCBjaGFyc1xyXG4gICAgICAgICAgICAucmVwbGFjZSgvXFwtLSsvZywgJy0nKSAgICAgICAgLy8gUmVwbGFjZSBtdWx0aXBsZSAtIHdpdGggc2luZ2xlIC1cclxuICAgICAgICAgICAgLnJlcGxhY2UoL14tKy8sICcnKSAgICAgICAgICAgLy8gVHJpbSAtIGZyb20gc3RhcnQgb2YgdGV4dFxyXG4gICAgICAgICAgICAucmVwbGFjZSgvLSskLywgJycpOyAgICAgICAgICAvLyBUcmltIC0gZnJvbSBlbmQgb2YgdGV4dFxyXG5cclxuICAgICAgICBpZiAoKHR5cGVvZiBzZXBhcmF0b3IgIT09ICd1bmRlZmluZWQnKSAmJiAoc2VwYXJhdG9yICE9PSAnLScpKSB7XHJcbiAgICAgICAgICAgIHRleHQgPSB0ZXh0LnJlcGxhY2UoLy0vZywgc2VwYXJhdG9yKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0ZXh0O1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBwYWQobnVtLCBzaXplKSB7XHJcbiAgICAgICAgY29uc3QgcyA9ICcwMDAwMDAwMDAnICsgbnVtO1xyXG4gICAgICAgIHJldHVybiBzLnN1YnN0cihzLmxlbmd0aCAtIHNpemUpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyB1Y2ZpcnN0KHN0cmluZykge1xyXG4gICAgICAgIHJldHVybiBzdHJpbmcuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBzdHJpbmcuc2xpY2UoMSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IENvbW1vbjsiXSwic291cmNlUm9vdCI6IiJ9