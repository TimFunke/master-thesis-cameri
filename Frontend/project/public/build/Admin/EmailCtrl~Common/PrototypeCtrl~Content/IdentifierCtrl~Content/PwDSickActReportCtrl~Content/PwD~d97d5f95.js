(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Admin/EmailCtrl~Common/PrototypeCtrl~Content/IdentifierCtrl~Content/PwDSickActReportCtrl~Content/PwD~d97d5f95"],{

/***/ "./assets/js/Library/Selectize.js":
/*!****************************************!*\
  !*** ./assets/js/Library/Selectize.js ***!
  \****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _library_selectize_dist_js_standalone_selectize__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../library/selectize/dist/js/standalone/selectize */ "./assets/library/selectize/dist/js/standalone/selectize.js");
/* harmony import */ var _library_selectize_dist_js_standalone_selectize__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_library_selectize_dist_js_standalone_selectize__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _library_selectize_dist_css_selectize_bootstrap4_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../library/selectize/dist/css/selectize.bootstrap4.css */ "./assets/library/selectize/dist/css/selectize.bootstrap4.css");
/* harmony import */ var _library_selectize_dist_css_selectize_bootstrap4_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_library_selectize_dist_css_selectize_bootstrap4_css__WEBPACK_IMPORTED_MODULE_1__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/





/***/ }),

/***/ "./assets/library/selectize/dist/css/selectize.bootstrap4.css":
/*!********************************************************************!*\
  !*** ./assets/library/selectize/dist/css/selectize.bootstrap4.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/library/selectize/dist/js/standalone/selectize.js":
/*!******************************************************************!*\
  !*** ./assets/library/selectize/dist/js/standalone/selectize.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {var __WEBPACK_LOCAL_MODULE_0__, __WEBPACK_LOCAL_MODULE_0__factory, __WEBPACK_LOCAL_MODULE_0__module;var __WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_1__factory, __WEBPACK_LOCAL_MODULE_1__module;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * sifter.js
 * Copyright (c) 2013 Brian Reavis & contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 * @author Brian Reavis <brian@thirdroute.com>
 */
(function (root, factory) {
  if (true) {
    !(__WEBPACK_LOCAL_MODULE_0__factory = (factory), (__WEBPACK_LOCAL_MODULE_0__module = { id: "sifter", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_0__ = (typeof __WEBPACK_LOCAL_MODULE_0__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_0__factory.call(__WEBPACK_LOCAL_MODULE_0__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_0__module.exports, __WEBPACK_LOCAL_MODULE_0__module)) : __WEBPACK_LOCAL_MODULE_0__factory), (__WEBPACK_LOCAL_MODULE_0__module.loaded = true), __WEBPACK_LOCAL_MODULE_0__ === undefined && (__WEBPACK_LOCAL_MODULE_0__ = __WEBPACK_LOCAL_MODULE_0__module.exports));
  } else {}
})(this, function () {
  /**
   * Textually searches arrays and hashes of objects
   * by property (or multiple properties). Designed
   * specifically for autocomplete.
   *
   * @constructor
   * @param {array|object} items
   * @param {object} items
   */
  var Sifter = function Sifter(items, settings) {
    this.items = items;
    this.settings = settings || {
      diacritics: true
    };
  };
  /**
   * Splits a search string into an array of individual
   * regexps to be used to match results.
   *
   * @param {string} query
   * @returns {array}
   */


  Sifter.prototype.tokenize = function (query) {
    query = trim(String(query || '').toLowerCase());
    if (!query || !query.length) return [];
    var i, n, regex, letter;
    var tokens = [];
    var words = query.split(/ +/);

    for (i = 0, n = words.length; i < n; i++) {
      regex = escape_regex(words[i]);

      if (this.settings.diacritics) {
        for (letter in DIACRITICS) {
          if (DIACRITICS.hasOwnProperty(letter)) {
            regex = regex.replace(new RegExp(letter, 'g'), DIACRITICS[letter]);
          }
        }
      }

      tokens.push({
        string: words[i],
        regex: new RegExp(regex, 'i')
      });
    }

    return tokens;
  };
  /**
   * Iterates over arrays and hashes.
   *
   * ```
   * this.iterator(this.items, function(item, id) {
   *    // invoked for each item
   * });
   * ```
   *
   * @param {array|object} object
   */


  Sifter.prototype.iterator = function (object, callback) {
    var iterator;

    if (is_array(object)) {
      iterator = Array.prototype.forEach || function (callback) {
        for (var i = 0, n = this.length; i < n; i++) {
          callback(this[i], i, this);
        }
      };
    } else {
      iterator = function iterator(callback) {
        for (var key in this) {
          if (this.hasOwnProperty(key)) {
            callback(this[key], key, this);
          }
        }
      };
    }

    iterator.apply(object, [callback]);
  };
  /**
   * Returns a function to be used to score individual results.
   *
   * Good matches will have a higher score than poor matches.
   * If an item is not a match, 0 will be returned by the function.
   *
   * @param {object|string} search
   * @param {object} options (optional)
   * @returns {function}
   */


  Sifter.prototype.getScoreFunction = function (search, options) {
    var self, fields, tokens, token_count;
    self = this;
    search = self.prepareSearch(search, options);
    tokens = search.tokens;
    fields = search.options.fields;
    token_count = tokens.length;
    /**
     * Calculates how close of a match the
     * given value is against a search token.
     *
     * @param {mixed} value
     * @param {object} token
     * @return {number}
     */

    var scoreValue = function scoreValue(value, token) {
      var score, pos;
      if (!value) return 0;
      value = String(value || '');
      pos = value.search(token.regex);
      if (pos === -1) return 0;
      score = token.string.length / value.length;
      if (pos === 0) score += 0.5;
      return score;
    };
    /**
     * Calculates the score of an object
     * against the search query.
     *
     * @param {object} token
     * @param {object} data
     * @return {number}
     */


    var scoreObject = function () {
      var field_count = fields.length;

      if (!field_count) {
        return function () {
          return 0;
        };
      }

      if (field_count === 1) {
        return function (token, data) {
          return scoreValue(data[fields[0]], token);
        };
      }

      return function (token, data) {
        for (var i = 0, sum = 0; i < field_count; i++) {
          sum += scoreValue(data[fields[i]], token);
        }

        return sum / field_count;
      };
    }();

    if (!token_count) {
      return function () {
        return 0;
      };
    }

    if (token_count === 1) {
      return function (data) {
        return scoreObject(tokens[0], data);
      };
    }

    if (search.options.conjunction === 'and') {
      return function (data) {
        var score;

        for (var i = 0, sum = 0; i < token_count; i++) {
          score = scoreObject(tokens[i], data);
          if (score <= 0) return 0;
          sum += score;
        }

        return sum / token_count;
      };
    } else {
      return function (data) {
        for (var i = 0, sum = 0; i < token_count; i++) {
          sum += scoreObject(tokens[i], data);
        }

        return sum / token_count;
      };
    }
  };
  /**
   * Returns a function that can be used to compare two
   * results, for sorting purposes. If no sorting should
   * be performed, `null` will be returned.
   *
   * @param {string|object} search
   * @param {object} options
   * @return function(a,b)
   */


  Sifter.prototype.getSortFunction = function (search, options) {
    var i, n, self, field, fields, fields_count, multiplier, multipliers, get_field, implicit_score, sort;
    self = this;
    search = self.prepareSearch(search, options);
    sort = !search.query && options.sort_empty || options.sort;
    /**
     * Fetches the specified sort field value
     * from a search result item.
     *
     * @param  {string} name
     * @param  {object} result
     * @return {mixed}
     */

    get_field = function get_field(name, result) {
      if (name === '$score') return result.score;
      return self.items[result.id][name];
    }; // parse options


    fields = [];

    if (sort) {
      for (i = 0, n = sort.length; i < n; i++) {
        if (search.query || sort[i].field !== '$score') {
          fields.push(sort[i]);
        }
      }
    } // the "$score" field is implied to be the primary
    // sort field, unless it's manually specified


    if (search.query) {
      implicit_score = true;

      for (i = 0, n = fields.length; i < n; i++) {
        if (fields[i].field === '$score') {
          implicit_score = false;
          break;
        }
      }

      if (implicit_score) {
        fields.unshift({
          field: '$score',
          direction: 'desc'
        });
      }
    } else {
      for (i = 0, n = fields.length; i < n; i++) {
        if (fields[i].field === '$score') {
          fields.splice(i, 1);
          break;
        }
      }
    }

    multipliers = [];

    for (i = 0, n = fields.length; i < n; i++) {
      multipliers.push(fields[i].direction === 'desc' ? -1 : 1);
    } // build function


    fields_count = fields.length;

    if (!fields_count) {
      return null;
    } else if (fields_count === 1) {
      field = fields[0].field;
      multiplier = multipliers[0];
      return function (a, b) {
        return multiplier * cmp(get_field(field, a), get_field(field, b));
      };
    } else {
      return function (a, b) {
        var i, result, a_value, b_value, field;

        for (i = 0; i < fields_count; i++) {
          field = fields[i].field;
          result = multipliers[i] * cmp(get_field(field, a), get_field(field, b));
          if (result) return result;
        }

        return 0;
      };
    }
  };
  /**
   * Parses a search query and returns an object
   * with tokens and fields ready to be populated
   * with results.
   *
   * @param {string} query
   * @param {object} options
   * @returns {object}
   */


  Sifter.prototype.prepareSearch = function (query, options) {
    if (_typeof(query) === 'object') return query;
    options = extend({}, options);
    var option_fields = options.fields;
    var option_sort = options.sort;
    var option_sort_empty = options.sort_empty;
    if (option_fields && !is_array(option_fields)) options.fields = [option_fields];
    if (option_sort && !is_array(option_sort)) options.sort = [option_sort];
    if (option_sort_empty && !is_array(option_sort_empty)) options.sort_empty = [option_sort_empty];
    return {
      options: options,
      query: String(query || '').toLowerCase(),
      tokens: this.tokenize(query),
      total: 0,
      items: []
    };
  };
  /**
   * Searches through all items and returns a sorted array of matches.
   *
   * The `options` parameter can contain:
   *
   *   - fields {string|array}
   *   - sort {array}
   *   - score {function}
   *   - filter {bool}
   *   - limit {integer}
   *
   * Returns an object containing:
   *
   *   - options {object}
   *   - query {string}
   *   - tokens {array}
   *   - total {int}
   *   - items {array}
   *
   * @param {string} query
   * @param {object} options
   * @returns {object}
   */


  Sifter.prototype.search = function (query, options) {
    var self = this,
        value,
        score,
        search,
        calculateScore;
    var fn_sort;
    var fn_score;
    search = this.prepareSearch(query, options);
    options = search.options;
    query = search.query; // generate result scoring function

    fn_score = options.score || self.getScoreFunction(search); // perform search and sort

    if (query.length) {
      self.iterator(self.items, function (item, id) {
        score = fn_score(item);

        if (options.filter === false || score > 0) {
          search.items.push({
            'score': score,
            'id': id
          });
        }
      });
    } else {
      self.iterator(self.items, function (item, id) {
        search.items.push({
          'score': 1,
          'id': id
        });
      });
    }

    fn_sort = self.getSortFunction(search, options);
    if (fn_sort) search.items.sort(fn_sort); // apply limits

    search.total = search.items.length;

    if (typeof options.limit === 'number') {
      search.items = search.items.slice(0, options.limit);
    }

    return search;
  }; // utilities
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  var cmp = function cmp(a, b) {
    if (typeof a === 'number' && typeof b === 'number') {
      return a > b ? 1 : a < b ? -1 : 0;
    }

    a = asciifold(String(a || ''));
    b = asciifold(String(b || ''));
    if (a > b) return 1;
    if (b > a) return -1;
    return 0;
  };

  var extend = function extend(a, b) {
    var i, n, k, object;

    for (i = 1, n = arguments.length; i < n; i++) {
      object = arguments[i];
      if (!object) continue;

      for (k in object) {
        if (object.hasOwnProperty(k)) {
          a[k] = object[k];
        }
      }
    }

    return a;
  };

  var trim = function trim(str) {
    return (str + '').replace(/^\s+|\s+$|/g, '');
  };

  var escape_regex = function escape_regex(str) {
    return (str + '').replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
  };

  var is_array = Array.isArray || $ && $.isArray || function (object) {
    return Object.prototype.toString.call(object) === '[object Array]';
  };

  var DIACRITICS = {
    'a': '[aÀÁÂÃÄÅàáâãäåĀāąĄ]',
    'c': '[cÇçćĆčČ]',
    'd': '[dđĐďĎ]',
    'e': '[eÈÉÊËèéêëěĚĒēęĘ]',
    'i': '[iÌÍÎÏìíîïĪī]',
    'l': '[lłŁ]',
    'n': '[nÑñňŇńŃ]',
    'o': '[oÒÓÔÕÕÖØòóôõöøŌō]',
    'r': '[rřŘ]',
    's': '[sŠšśŚ]',
    't': '[tťŤ]',
    'u': '[uÙÚÛÜùúûüůŮŪū]',
    'y': '[yŸÿýÝ]',
    'z': '[zŽžżŻźŹ]'
  };

  var asciifold = function () {
    var i, n, k, chunk;
    var foreignletters = '';
    var lookup = {};

    for (k in DIACRITICS) {
      if (DIACRITICS.hasOwnProperty(k)) {
        chunk = DIACRITICS[k].substring(2, DIACRITICS[k].length - 1);
        foreignletters += chunk;

        for (i = 0, n = chunk.length; i < n; i++) {
          lookup[chunk.charAt(i)] = k;
        }
      }
    }

    var regexp = new RegExp('[' + foreignletters + ']', 'g');
    return function (str) {
      return str.replace(regexp, function (foreignletter) {
        return lookup[foreignletter];
      }).toLowerCase();
    };
  }(); // export
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  return Sifter;
});
/**
 * microplugin.js
 * Copyright (c) 2013 Brian Reavis & contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 * @author Brian Reavis <brian@thirdroute.com>
 */


(function (root, factory) {
  if (true) {
    !(__WEBPACK_LOCAL_MODULE_1__factory = (factory), (__WEBPACK_LOCAL_MODULE_1__module = { id: "microplugin", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_1__ = (typeof __WEBPACK_LOCAL_MODULE_1__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_1__factory.call(__WEBPACK_LOCAL_MODULE_1__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_1__module.exports, __WEBPACK_LOCAL_MODULE_1__module)) : __WEBPACK_LOCAL_MODULE_1__factory), (__WEBPACK_LOCAL_MODULE_1__module.loaded = true), __WEBPACK_LOCAL_MODULE_1__ === undefined && (__WEBPACK_LOCAL_MODULE_1__ = __WEBPACK_LOCAL_MODULE_1__module.exports));
  } else {}
})(this, function () {
  var MicroPlugin = {};

  MicroPlugin.mixin = function (Interface) {
    Interface.plugins = {};
    /**
     * Initializes the listed plugins (with options).
     * Acceptable formats:
     *
     * List (without options):
     *   ['a', 'b', 'c']
     *
     * List (with options):
     *   [{'name': 'a', options: {}}, {'name': 'b', options: {}}]
     *
     * Hash (with options):
     *   {'a': { ... }, 'b': { ... }, 'c': { ... }}
     *
     * @param {mixed} plugins
     */

    Interface.prototype.initializePlugins = function (plugins) {
      var i, n, key;
      var self = this;
      var queue = [];
      self.plugins = {
        names: [],
        settings: {},
        requested: {},
        loaded: {}
      };

      if (utils.isArray(plugins)) {
        for (i = 0, n = plugins.length; i < n; i++) {
          if (typeof plugins[i] === 'string') {
            queue.push(plugins[i]);
          } else {
            self.plugins.settings[plugins[i].name] = plugins[i].options;
            queue.push(plugins[i].name);
          }
        }
      } else if (plugins) {
        for (key in plugins) {
          if (plugins.hasOwnProperty(key)) {
            self.plugins.settings[key] = plugins[key];
            queue.push(key);
          }
        }
      }

      while (queue.length) {
        self.require(queue.shift());
      }
    };

    Interface.prototype.loadPlugin = function (name) {
      var self = this;
      var plugins = self.plugins;
      var plugin = Interface.plugins[name];

      if (!Interface.plugins.hasOwnProperty(name)) {
        throw new Error('Unable to find "' + name + '" plugin');
      }

      plugins.requested[name] = true;
      plugins.loaded[name] = plugin.fn.apply(self, [self.plugins.settings[name] || {}]);
      plugins.names.push(name);
    };
    /**
     * Initializes a plugin.
     *
     * @param {string} name
     */


    Interface.prototype.require = function (name) {
      var self = this;
      var plugins = self.plugins;

      if (!self.plugins.loaded.hasOwnProperty(name)) {
        if (plugins.requested[name]) {
          throw new Error('Plugin has circular dependency ("' + name + '")');
        }

        self.loadPlugin(name);
      }

      return plugins.loaded[name];
    };
    /**
     * Registers a plugin.
     *
     * @param {string} name
     * @param {function} fn
     */


    Interface.define = function (name, fn) {
      Interface.plugins[name] = {
        'name': name,
        'fn': fn
      };
    };
  };

  var utils = {
    isArray: Array.isArray || function (vArg) {
      return Object.prototype.toString.call(vArg) === '[object Array]';
    }
  };
  return MicroPlugin;
});
/**
 * selectize.js (v0.12.1)
 * Copyright (c) 2013–2015 Brian Reavis & contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 * @author Brian Reavis <brian@thirdroute.com>
 */

/*jshint curly:false */

/*jshint browser:true */


(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __WEBPACK_LOCAL_MODULE_0__, __WEBPACK_LOCAL_MODULE_1__], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
})(this, function ($, Sifter, MicroPlugin) {
  'use strict';

  var highlight = function highlight($element, pattern) {
    if (typeof pattern === 'string' && !pattern.length) return;
    var regex = typeof pattern === 'string' ? new RegExp(pattern, 'i') : pattern;

    var highlight = function highlight(node) {
      var skip = 0;

      if (node.nodeType === 3) {
        var pos = node.data.search(regex);

        if (pos >= 0 && node.data.length > 0) {
          var match = node.data.match(regex);
          var spannode = document.createElement('span');
          spannode.className = 'highlight';
          var middlebit = node.splitText(pos);
          var endbit = middlebit.splitText(match[0].length);
          var middleclone = middlebit.cloneNode(true);
          spannode.appendChild(middleclone);
          middlebit.parentNode.replaceChild(spannode, middlebit);
          skip = 1;
        }
      } else if (node.nodeType === 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
        for (var i = 0; i < node.childNodes.length; ++i) {
          i += highlight(node.childNodes[i]);
        }
      }

      return skip;
    };

    return $element.each(function () {
      highlight(this);
    });
  };

  var MicroEvent = function MicroEvent() {};

  MicroEvent.prototype = {
    on: function on(event, fct) {
      this._events = this._events || {};
      this._events[event] = this._events[event] || [];

      this._events[event].push(fct);
    },
    off: function off(event, fct) {
      var n = arguments.length;
      if (n === 0) return delete this._events;
      if (n === 1) return delete this._events[event];
      this._events = this._events || {};
      if (event in this._events === false) return;

      this._events[event].splice(this._events[event].indexOf(fct), 1);
    },
    trigger: function trigger(event
    /* , args... */
    ) {
      this._events = this._events || {};
      if (event in this._events === false) return;

      for (var i = 0; i < this._events[event].length; i++) {
        this._events[event][i].apply(this, Array.prototype.slice.call(arguments, 1));
      }
    }
  };
  /**
   * Mixin will delegate all MicroEvent.js function in the destination object.
   *
   * - MicroEvent.mixin(Foobar) will make Foobar able to use MicroEvent
   *
   * @param {object} the object which will support MicroEvent
   */

  MicroEvent.mixin = function (destObject) {
    var props = ['on', 'off', 'trigger'];

    for (var i = 0; i < props.length; i++) {
      destObject.prototype[props[i]] = MicroEvent.prototype[props[i]];
    }
  };

  var IS_MAC = /Mac/.test(navigator.userAgent);
  var KEY_A = 65;
  var KEY_COMMA = 188;
  var KEY_RETURN = 13;
  var KEY_ESC = 27;
  var KEY_LEFT = 37;
  var KEY_UP = 38;
  var KEY_P = 80;
  var KEY_RIGHT = 39;
  var KEY_DOWN = 40;
  var KEY_N = 78;
  var KEY_BACKSPACE = 8;
  var KEY_DELETE = 46;
  var KEY_SHIFT = 16;
  var KEY_CMD = IS_MAC ? 91 : 17;
  var KEY_CTRL = IS_MAC ? 18 : 17;
  var KEY_TAB = 9;
  var TAG_SELECT = 1;
  var TAG_INPUT = 2; // for now, android support in general is too spotty to support validity

  var SUPPORTS_VALIDITY_API = !/android/i.test(window.navigator.userAgent) && !!document.createElement('form').validity;

  var isset = function isset(object) {
    return typeof object !== 'undefined';
  };
  /**
   * Converts a scalar to its best string representation
   * for hash keys and HTML attribute values.
   *
   * Transformations:
   *   'str'     -> 'str'
   *   null      -> ''
   *   undefined -> ''
   *   true      -> '1'
   *   false     -> '0'
   *   0         -> '0'
   *   1         -> '1'
   *
   * @param {string} value
   * @returns {string|null}
   */


  var hash_key = function hash_key(value) {
    if (typeof value === 'undefined' || value === null) return null;
    if (typeof value === 'boolean') return value ? '1' : '0';
    return value + '';
  };
  /**
   * Escapes a string for use within HTML.
   *
   * @param {string} str
   * @returns {string}
   */


  var escape_html = function escape_html(str) {
    return (str + '').replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
  };
  /**
   * Escapes "$" characters in replacement strings.
   *
   * @param {string} str
   * @returns {string}
   */


  var escape_replace = function escape_replace(str) {
    return (str + '').replace(/\$/g, '$$$$');
  };

  var hook = {};
  /**
   * Wraps `method` on `self` so that `fn`
   * is invoked before the original method.
   *
   * @param {object} self
   * @param {string} method
   * @param {function} fn
   */

  hook.before = function (self, method, fn) {
    var original = self[method];

    self[method] = function () {
      fn.apply(self, arguments);
      return original.apply(self, arguments);
    };
  };
  /**
   * Wraps `method` on `self` so that `fn`
   * is invoked after the original method.
   *
   * @param {object} self
   * @param {string} method
   * @param {function} fn
   */


  hook.after = function (self, method, fn) {
    var original = self[method];

    self[method] = function () {
      var result = original.apply(self, arguments);
      fn.apply(self, arguments);
      return result;
    };
  };
  /**
   * Wraps `fn` so that it can only be invoked once.
   *
   * @param {function} fn
   * @returns {function}
   */


  var once = function once(fn) {
    var called = false;
    return function () {
      if (called) return;
      called = true;
      fn.apply(this, arguments);
    };
  };
  /**
   * Wraps `fn` so that it can only be called once
   * every `delay` milliseconds (invoked on the falling edge).
   *
   * @param {function} fn
   * @param {int} delay
   * @returns {function}
   */


  var debounce = function debounce(fn, delay) {
    var timeout;
    return function () {
      var self = this;
      var args = arguments;
      window.clearTimeout(timeout);
      timeout = window.setTimeout(function () {
        fn.apply(self, args);
      }, delay);
    };
  };
  /**
   * Debounce all fired events types listed in `types`
   * while executing the provided `fn`.
   *
   * @param {object} self
   * @param {array} types
   * @param {function} fn
   */


  var debounce_events = function debounce_events(self, types, fn) {
    var type;
    var trigger = self.trigger;
    var event_args = {}; // override trigger method

    self.trigger = function () {
      var type = arguments[0];

      if (types.indexOf(type) !== -1) {
        event_args[type] = arguments;
      } else {
        return trigger.apply(self, arguments);
      }
    }; // invoke provided function


    fn.apply(self, []);
    self.trigger = trigger; // trigger queued events

    for (type in event_args) {
      if (event_args.hasOwnProperty(type)) {
        trigger.apply(self, event_args[type]);
      }
    }
  };
  /**
   * A workaround for http://bugs.jquery.com/ticket/6696
   *
   * @param {object} $parent - Parent element to listen on.
   * @param {string} event - Event name.
   * @param {string} selector - Descendant selector to filter by.
   * @param {function} fn - Event handler.
   */


  var watchChildEvent = function watchChildEvent($parent, event, selector, fn) {
    $parent.on(event, selector, function (e) {
      var child = e.target;

      while (child && child.parentNode !== $parent[0]) {
        child = child.parentNode;
      }

      e.currentTarget = child;
      return fn.apply(this, [e]);
    });
  };
  /**
   * Determines the current selection within a text input control.
   * Returns an object containing:
   *   - start
   *   - length
   *
   * @param {object} input
   * @returns {object}
   */


  var getSelection = function getSelection(input) {
    var result = {};

    if ('selectionStart' in input) {
      result.start = input.selectionStart;
      result.length = input.selectionEnd - result.start;
    } else if (document.selection) {
      input.focus();
      var sel = document.selection.createRange();
      var selLen = document.selection.createRange().text.length;
      sel.moveStart('character', -input.value.length);
      result.start = sel.text.length - selLen;
      result.length = selLen;
    }

    return result;
  };
  /**
   * Copies CSS properties from one element to another.
   *
   * @param {object} $from
   * @param {object} $to
   * @param {array} properties
   */


  var transferStyles = function transferStyles($from, $to, properties) {
    var i,
        n,
        styles = {};

    if (properties) {
      for (i = 0, n = properties.length; i < n; i++) {
        styles[properties[i]] = $from.css(properties[i]);
      }
    } else {
      styles = $from.css();
    }

    $to.css(styles);
  };
  /**
   * Measures the width of a string within a
   * parent element (in pixels).
   *
   * @param {string} str
   * @param {object} $parent
   * @returns {int}
   */


  var measureString = function measureString(str, $parent) {
    if (!str) {
      return 0;
    }

    var $test = $('<test>').css({
      position: 'absolute',
      top: -99999,
      left: -99999,
      width: 'auto',
      padding: 0,
      whiteSpace: 'pre'
    }).text(str).appendTo('body');
    transferStyles($parent, $test, ['letterSpacing', 'fontSize', 'fontFamily', 'fontWeight', 'textTransform']);
    var width = $test.width();
    $test.remove();
    return width;
  };
  /**
   * Sets up an input to grow horizontally as the user
   * types. If the value is changed manually, you can
   * trigger the "update" handler to resize:
   *
   * $input.trigger('update');
   *
   * @param {object} $input
   */


  var autoGrow = function autoGrow($input) {
    var currentWidth = null;

    var update = function update(e, options) {
      var value, keyCode, printable, placeholder, width;
      var shift, character, selection;
      e = e || window.event || {};
      options = options || {};
      if (e.metaKey || e.altKey) return;
      if (!options.force && $input.data('grow') === false) return;
      value = $input.val();

      if (e.type && e.type.toLowerCase() === 'keydown') {
        keyCode = e.keyCode;
        printable = keyCode >= 97 && keyCode <= 122 || // a-z
        keyCode >= 65 && keyCode <= 90 || // A-Z
        keyCode >= 48 && keyCode <= 57 || // 0-9
        keyCode === 32 // space
        ;

        if (keyCode === KEY_DELETE || keyCode === KEY_BACKSPACE) {
          selection = getSelection($input[0]);

          if (selection.length) {
            value = value.substring(0, selection.start) + value.substring(selection.start + selection.length);
          } else if (keyCode === KEY_BACKSPACE && selection.start) {
            value = value.substring(0, selection.start - 1) + value.substring(selection.start + 1);
          } else if (keyCode === KEY_DELETE && typeof selection.start !== 'undefined') {
            value = value.substring(0, selection.start) + value.substring(selection.start + 1);
          }
        } else if (printable) {
          shift = e.shiftKey;
          character = String.fromCharCode(e.keyCode);
          if (shift) character = character.toUpperCase();else character = character.toLowerCase();
          value += character;
        }
      }

      placeholder = $input.attr('placeholder');

      if (!value && placeholder) {
        value = placeholder;
      }

      width = measureString(value, $input) + 4;

      if (width !== currentWidth) {
        currentWidth = width;
        $input.width(width);
        $input.triggerHandler('resize');
      }
    };

    $input.on('keydown keyup update blur', update);
    update();
  };

  var Selectize = function Selectize($input, settings) {
    var key,
        i,
        n,
        dir,
        input,
        self = this;
    input = $input[0];
    input.selectize = self; // detect rtl environment

    var computedStyle = window.getComputedStyle && window.getComputedStyle(input, null);
    dir = computedStyle ? computedStyle.getPropertyValue('direction') : input.currentStyle && input.currentStyle.direction;
    dir = dir || $input.parents('[dir]:first').attr('dir') || ''; // setup default state

    $.extend(self, {
      order: 0,
      settings: settings,
      $input: $input,
      tabIndex: $input.attr('tabindex') || '',
      tagType: input.tagName.toLowerCase() === 'select' ? TAG_SELECT : TAG_INPUT,
      rtl: /rtl/i.test(dir),
      eventNS: '.selectize' + ++Selectize.count,
      highlightedValue: null,
      isOpen: false,
      isDisabled: false,
      isRequired: $input.is('[required]'),
      isInvalid: false,
      isLocked: false,
      isFocused: false,
      isInputHidden: false,
      isSetup: false,
      isShiftDown: false,
      isCmdDown: false,
      isCtrlDown: false,
      ignoreFocus: false,
      ignoreBlur: false,
      ignoreHover: false,
      hasOptions: false,
      currentResults: null,
      lastValue: '',
      caretPos: 0,
      loading: 0,
      loadedSearches: {},
      $activeOption: null,
      $activeItems: [],
      optgroups: {},
      options: {},
      userOptions: {},
      items: [],
      renderCache: {},
      onSearchChange: settings.loadThrottle === null ? self.onSearchChange : debounce(self.onSearchChange, settings.loadThrottle)
    }); // search system

    self.sifter = new Sifter(this.options, {
      diacritics: settings.diacritics
    }); // build options table

    if (self.settings.options) {
      for (i = 0, n = self.settings.options.length; i < n; i++) {
        self.registerOption(self.settings.options[i]);
      }

      delete self.settings.options;
    } // build optgroup table


    if (self.settings.optgroups) {
      for (i = 0, n = self.settings.optgroups.length; i < n; i++) {
        self.registerOptionGroup(self.settings.optgroups[i]);
      }

      delete self.settings.optgroups;
    } // option-dependent defaults


    self.settings.mode = self.settings.mode || (self.settings.maxItems === 1 ? 'single' : 'multi');

    if (typeof self.settings.hideSelected !== 'boolean') {
      self.settings.hideSelected = self.settings.mode === 'multi';
    }

    self.initializePlugins(self.settings.plugins);
    self.setupCallbacks();
    self.setupTemplates();
    self.setup();
  }; // mixins
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  MicroEvent.mixin(Selectize);
  MicroPlugin.mixin(Selectize); // methods
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  $.extend(Selectize.prototype, {
    /**
     * Creates all elements and sets up event bindings.
     */
    setup: function setup() {
      var self = this;
      var settings = self.settings;
      var eventNS = self.eventNS;
      var $window = $(window);
      var $document = $(document);
      var $input = self.$input;
      var $wrapper;
      var $control;
      var $control_input;
      var $dropdown;
      var $dropdown_content;
      var $dropdown_parent;
      var inputMode;
      var timeout_blur;
      var timeout_focus;
      var classes;
      var classes_plugins;
      inputMode = self.settings.mode;
      classes = $input.attr('class') || '';
      $wrapper = $('<div>').addClass(settings.wrapperClass).addClass(classes).addClass(inputMode);
      $control = $('<div>').addClass(settings.inputClass).addClass('items').appendTo($wrapper);
      $control_input = $('<input type="text" autocomplete="off" />').appendTo($control).attr('tabindex', $input.is(':disabled') ? '-1' : self.tabIndex);
      $dropdown_parent = $(settings.dropdownParent || $wrapper);
      $dropdown = $('<div>').addClass(settings.dropdownClass).addClass(inputMode).hide().appendTo($dropdown_parent);
      $dropdown_content = $('<div>').addClass(settings.dropdownContentClass).appendTo($dropdown);

      if (self.settings.copyClassesToDropdown) {
        $dropdown.addClass(classes);
      }

      $wrapper.css({
        width: $input[0].style.width
      });

      if (self.plugins.names.length) {
        classes_plugins = 'plugin-' + self.plugins.names.join(' plugin-');
        $wrapper.addClass(classes_plugins);
        $dropdown.addClass(classes_plugins);
      }

      if ((settings.maxItems === null || settings.maxItems > 1) && self.tagType === TAG_SELECT) {
        $input.attr('multiple', 'multiple');
      }

      if (self.settings.placeholder) {
        $control_input.attr('placeholder', settings.placeholder);
      } // if splitOn was not passed in, construct it from the delimiter to allow pasting universally


      if (!self.settings.splitOn && self.settings.delimiter) {
        var delimiterEscaped = self.settings.delimiter.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        self.settings.splitOn = new RegExp('\\s*' + delimiterEscaped + '+\\s*');
      }

      if ($input.attr('autocorrect')) {
        $control_input.attr('autocorrect', $input.attr('autocorrect'));
      }

      if ($input.attr('autocapitalize')) {
        $control_input.attr('autocapitalize', $input.attr('autocapitalize'));
      }

      self.$wrapper = $wrapper;
      self.$control = $control;
      self.$control_input = $control_input;
      self.$dropdown = $dropdown;
      self.$dropdown_content = $dropdown_content;
      $dropdown.on('mouseenter', '[data-selectable]', function () {
        return self.onOptionHover.apply(self, arguments);
      });
      $dropdown.on('mousedown click', '[data-selectable]', function () {
        return self.onOptionSelect.apply(self, arguments);
      });
      watchChildEvent($control, 'mousedown', '*:not(input)', function () {
        return self.onItemSelect.apply(self, arguments);
      });
      autoGrow($control_input);
      $control.on({
        mousedown: function mousedown() {
          return self.onMouseDown.apply(self, arguments);
        },
        click: function click() {
          return self.onClick.apply(self, arguments);
        }
      });
      $control_input.on({
        mousedown: function mousedown(e) {
          e.stopPropagation();
        },
        keydown: function keydown() {
          return self.onKeyDown.apply(self, arguments);
        },
        keyup: function keyup() {
          return self.onKeyUp.apply(self, arguments);
        },
        keypress: function keypress() {
          return self.onKeyPress.apply(self, arguments);
        },
        resize: function resize() {
          self.positionDropdown.apply(self, []);
        },
        blur: function blur() {
          return self.onBlur.apply(self, arguments);
        },
        focus: function focus() {
          self.ignoreBlur = false;
          return self.onFocus.apply(self, arguments);
        },
        paste: function paste() {
          return self.onPaste.apply(self, arguments);
        }
      });
      $document.on('keydown' + eventNS, function (e) {
        self.isCmdDown = e[IS_MAC ? 'metaKey' : 'ctrlKey'];
        self.isCtrlDown = e[IS_MAC ? 'altKey' : 'ctrlKey'];
        self.isShiftDown = e.shiftKey;
      });
      $document.on('keyup' + eventNS, function (e) {
        if (e.keyCode === KEY_CTRL) self.isCtrlDown = false;
        if (e.keyCode === KEY_SHIFT) self.isShiftDown = false;
        if (e.keyCode === KEY_CMD) self.isCmdDown = false;
      });
      $document.on('mousedown' + eventNS, function (e) {
        if (self.isFocused) {
          // prevent events on the dropdown scrollbar from causing the control to blur
          if (e.target === self.$dropdown[0] || e.target.parentNode === self.$dropdown[0]) {
            return false;
          } // blur on click outside


          if (!self.$control.has(e.target).length && e.target !== self.$control[0]) {
            self.blur(e.target);
          }
        }
      });
      $window.on(['scroll' + eventNS, 'resize' + eventNS].join(' '), function () {
        if (self.isOpen) {
          self.positionDropdown.apply(self, arguments);
        }
      });
      $window.on('mousemove' + eventNS, function () {
        self.ignoreHover = false;
      }); // store original children and tab index so that they can be
      // restored when the destroy() method is called.

      this.revertSettings = {
        $children: $input.children().detach(),
        tabindex: $input.attr('tabindex')
      };
      $input.attr('tabindex', -1).hide().after(self.$wrapper);

      if ($.isArray(settings.items)) {
        self.setValue(settings.items);
        delete settings.items;
      } // feature detect for the validation API


      if (SUPPORTS_VALIDITY_API) {
        $input.on('invalid' + eventNS, function (e) {
          e.preventDefault();
          self.isInvalid = true;
          self.refreshState();
        });
      }

      self.updateOriginalInput();
      self.refreshItems();
      self.refreshState();
      self.updatePlaceholder();
      self.isSetup = true;

      if ($input.is(':disabled')) {
        self.disable();
      }

      self.on('change', this.onChange);
      $input.data('selectize', self);
      $input.addClass('selectized');
      self.trigger('initialize'); // preload options

      if (settings.preload === true) {
        self.onSearchChange('');
      }
    },

    /**
     * Sets up default rendering functions.
     */
    setupTemplates: function setupTemplates() {
      var self = this;
      var field_label = self.settings.labelField;
      var field_optgroup = self.settings.optgroupLabelField;
      var templates = {
        'optgroup': function optgroup(data) {
          return '<div class="optgroup">' + data.html + '</div>';
        },
        'optgroup_header': function optgroup_header(data, escape) {
          return '<div class="optgroup-header">' + escape(data[field_optgroup]) + '</div>';
        },
        'option': function option(data, escape) {
          return '<div class="option">' + escape(data[field_label]) + '</div>';
        },
        'item': function item(data, escape) {
          return '<div class="item">' + escape(data[field_label]) + '</div>';
        },
        'option_create': function option_create(data, escape) {
          return '<div class="create">Add <strong>' + escape(data.input) + '</strong>&hellip;</div>';
        }
      };
      self.settings.render = $.extend({}, templates, self.settings.render);
    },

    /**
     * Maps fired events to callbacks provided
     * in the settings used when creating the control.
     */
    setupCallbacks: function setupCallbacks() {
      var key,
          fn,
          callbacks = {
        'initialize': 'onInitialize',
        'change': 'onChange',
        'item_add': 'onItemAdd',
        'item_remove': 'onItemRemove',
        'clear': 'onClear',
        'option_add': 'onOptionAdd',
        'option_remove': 'onOptionRemove',
        'option_clear': 'onOptionClear',
        'optgroup_add': 'onOptionGroupAdd',
        'optgroup_remove': 'onOptionGroupRemove',
        'optgroup_clear': 'onOptionGroupClear',
        'dropdown_open': 'onDropdownOpen',
        'dropdown_close': 'onDropdownClose',
        'type': 'onType',
        'load': 'onLoad',
        'focus': 'onFocus',
        'blur': 'onBlur'
      };

      for (key in callbacks) {
        if (callbacks.hasOwnProperty(key)) {
          fn = this.settings[callbacks[key]];
          if (fn) this.on(key, fn);
        }
      }
    },

    /**
     * Triggered when the main control element
     * has a click event.
     *
     * @param {object} e
     * @return {boolean}
     */
    onClick: function onClick(e) {
      var self = this; // necessary for mobile webkit devices (manual focus triggering
      // is ignored unless invoked within a click event)

      if (!self.isFocused) {
        self.focus();
        e.preventDefault();
      }
    },

    /**
     * Triggered when the main control element
     * has a mouse down event.
     *
     * @param {object} e
     * @return {boolean}
     */
    onMouseDown: function onMouseDown(e) {
      var self = this;
      var defaultPrevented = e.isDefaultPrevented();
      var $target = $(e.target);

      if (self.isFocused) {
        // retain focus by preventing native handling. if the
        // event target is the input it should not be modified.
        // otherwise, text selection within the input won't work.
        if (e.target !== self.$control_input[0]) {
          if (self.settings.mode === 'single') {
            // toggle dropdown
            self.isOpen ? self.close() : self.open();
          } else if (!defaultPrevented) {
            self.setActiveItem(null);
          }

          return false;
        }
      } else {
        // give control focus
        if (!defaultPrevented) {
          window.setTimeout(function () {
            self.focus();
          }, 0);
        }
      }
    },

    /**
     * Triggered when the value of the control has been changed.
     * This should propagate the event to the original DOM
     * input / select element.
     */
    onChange: function onChange() {
      this.$input.trigger('change');
    },

    /**
     * Triggered on <input> paste.
     *
     * @param {object} e
     * @returns {boolean}
     */
    onPaste: function onPaste(e) {
      var self = this;

      if (self.isFull() || self.isInputHidden || self.isLocked) {
        e.preventDefault();
      } else {
        // If a regex or string is included, this will split the pasted
        // input and create Items for each separate value
        if (self.settings.splitOn) {
          setTimeout(function () {
            var splitInput = $.trim(self.$control_input.val() || '').split(self.settings.splitOn);

            for (var i = 0, n = splitInput.length; i < n; i++) {
              self.createItem(splitInput[i]);
            }
          }, 0);
        }
      }
    },

    /**
     * Triggered on <input> keypress.
     *
     * @param {object} e
     * @returns {boolean}
     */
    onKeyPress: function onKeyPress(e) {
      if (this.isLocked) return e && e.preventDefault();
      var character = String.fromCharCode(e.keyCode || e.which);

      if (this.settings.create && this.settings.mode === 'multi' && character === this.settings.delimiter) {
        this.createItem();
        e.preventDefault();
        return false;
      }
    },

    /**
     * Triggered on <input> keydown.
     *
     * @param {object} e
     * @returns {boolean}
     */
    onKeyDown: function onKeyDown(e) {
      var isInput = e.target === this.$control_input[0];
      var self = this;

      if (self.isLocked) {
        if (e.keyCode !== KEY_TAB) {
          e.preventDefault();
        }

        return;
      }

      switch (e.keyCode) {
        case KEY_A:
          if (self.isCmdDown) {
            self.selectAll();
            return;
          }

          break;

        case KEY_ESC:
          if (self.isOpen) {
            e.preventDefault();
            e.stopPropagation();
            self.close();
          }

          return;

        case KEY_N:
          if (!e.ctrlKey || e.altKey) break;

        case KEY_DOWN:
          if (!self.isOpen && self.hasOptions) {
            self.open();
          } else if (self.$activeOption) {
            self.ignoreHover = true;
            var $next = self.getAdjacentOption(self.$activeOption, 1);
            if ($next.length) self.setActiveOption($next, true, true);
          }

          e.preventDefault();
          return;

        case KEY_P:
          if (!e.ctrlKey || e.altKey) break;

        case KEY_UP:
          if (self.$activeOption) {
            self.ignoreHover = true;
            var $prev = self.getAdjacentOption(self.$activeOption, -1);
            if ($prev.length) self.setActiveOption($prev, true, true);
          }

          e.preventDefault();
          return;

        case KEY_RETURN:
          if (self.isOpen && self.$activeOption) {
            self.onOptionSelect({
              currentTarget: self.$activeOption
            });
            e.preventDefault();
          }

          return;

        case KEY_LEFT:
          self.advanceSelection(-1, e);
          return;

        case KEY_RIGHT:
          self.advanceSelection(1, e);
          return;

        case KEY_TAB:
          if (self.settings.selectOnTab && self.isOpen && self.$activeOption) {
            self.onOptionSelect({
              currentTarget: self.$activeOption
            }); // Default behaviour is to jump to the next field, we only want this
            // if the current field doesn't accept any more entries

            if (!self.isFull()) {
              e.preventDefault();
            }
          }

          if (self.settings.create && self.createItem()) {
            e.preventDefault();
          }

          return;

        case KEY_BACKSPACE:
        case KEY_DELETE:
          self.deleteSelection(e);
          return;
      }

      if ((self.isFull() || self.isInputHidden) && !(IS_MAC ? e.metaKey : e.ctrlKey)) {
        e.preventDefault();
        return;
      }
    },

    /**
     * Triggered on <input> keyup.
     *
     * @param {object} e
     * @returns {boolean}
     */
    onKeyUp: function onKeyUp(e) {
      var self = this;
      if (self.isLocked) return e && e.preventDefault();
      var value = self.$control_input.val() || '';

      if (self.lastValue !== value) {
        self.lastValue = value;
        self.onSearchChange(value);
        self.refreshOptions();
        self.trigger('type', value);
      }
    },

    /**
     * Invokes the user-provide option provider / loader.
     *
     * Note: this function is debounced in the Selectize
     * constructor (by `settings.loadDelay` milliseconds)
     *
     * @param {string} value
     */
    onSearchChange: function onSearchChange(value) {
      var self = this;
      var fn = self.settings.load;
      if (!fn) return;
      if (self.loadedSearches.hasOwnProperty(value)) return;
      self.loadedSearches[value] = true;
      self.load(function (callback) {
        fn.apply(self, [value, callback]);
      });
    },

    /**
     * Triggered on <input> focus.
     *
     * @param {object} e (optional)
     * @returns {boolean}
     */
    onFocus: function onFocus(e) {
      var self = this;
      var wasFocused = self.isFocused;

      if (self.isDisabled) {
        self.blur();
        e && e.preventDefault();
        return false;
      }

      if (self.ignoreFocus) return;
      self.isFocused = true;
      if (self.settings.preload === 'focus') self.onSearchChange('');
      if (!wasFocused) self.trigger('focus');

      if (!self.$activeItems.length) {
        self.showInput();
        self.setActiveItem(null);
        self.refreshOptions(!!self.settings.openOnFocus);
      }

      self.refreshState();
    },

    /**
     * Triggered on <input> blur.
     *
     * @param {object} e
     * @param {Element} dest
     */
    onBlur: function onBlur(e, dest) {
      var self = this;
      if (!self.isFocused) return;
      self.isFocused = false;

      if (self.ignoreFocus) {
        return;
      } else if (!self.ignoreBlur && document.activeElement === self.$dropdown_content[0]) {
        // necessary to prevent IE closing the dropdown when the scrollbar is clicked
        self.ignoreBlur = true;
        self.onFocus(e);
        return;
      }

      var deactivate = function deactivate() {
        self.close();
        self.setTextboxValue('');
        self.setActiveItem(null);
        self.setActiveOption(null);
        self.setCaret(self.items.length);
        self.refreshState(); // IE11 bug: element still marked as active

        (dest || document.body).focus();
        self.ignoreFocus = false;
        self.trigger('blur');
      };

      self.ignoreFocus = true;

      if (self.settings.create && self.settings.createOnBlur) {
        self.createItem(null, false, deactivate);
      } else {
        deactivate();
      }
    },

    /**
     * Triggered when the user rolls over
     * an option in the autocomplete dropdown menu.
     *
     * @param {object} e
     * @returns {boolean}
     */
    onOptionHover: function onOptionHover(e) {
      if (this.ignoreHover) return;
      this.setActiveOption(e.currentTarget, false);
    },

    /**
     * Triggered when the user clicks on an option
     * in the autocomplete dropdown menu.
     *
     * @param {object} e
     * @returns {boolean}
     */
    onOptionSelect: function onOptionSelect(e) {
      var value,
          $target,
          $option,
          self = this;

      if (e.preventDefault) {
        e.preventDefault();
        e.stopPropagation();
      }

      $target = $(e.currentTarget);

      if ($target.hasClass('create')) {
        self.createItem(null, function () {
          if (self.settings.closeAfterSelect) {
            self.close();
          }
        });
      } else {
        value = $target.attr('data-value');

        if (typeof value !== 'undefined') {
          self.lastQuery = null;
          self.setTextboxValue('');
          self.addItem(value);

          if (self.settings.closeAfterSelect) {
            self.close();
          } else if (!self.settings.hideSelected && e.type && /mouse/.test(e.type)) {
            self.setActiveOption(self.getOption(value));
          }
        }
      }
    },

    /**
     * Triggered when the user clicks on an item
     * that has been selected.
     *
     * @param {object} e
     * @returns {boolean}
     */
    onItemSelect: function onItemSelect(e) {
      var self = this;
      if (self.isLocked) return;

      if (self.settings.mode === 'multi') {
        e.preventDefault();
        self.setActiveItem(e.currentTarget, e);
      }
    },

    /**
     * Invokes the provided method that provides
     * results to a callback---which are then added
     * as options to the control.
     *
     * @param {function} fn
     */
    load: function load(fn) {
      var self = this;
      var $wrapper = self.$wrapper.addClass(self.settings.loadingClass);
      self.loading++;
      fn.apply(self, [function (results) {
        self.loading = Math.max(self.loading - 1, 0);

        if (results && results.length) {
          self.addOption(results);
          self.refreshOptions(self.isFocused && !self.isInputHidden);
        }

        if (!self.loading) {
          $wrapper.removeClass(self.settings.loadingClass);
        }

        self.trigger('load', results);
      }]);
    },

    /**
     * Sets the input field of the control to the specified value.
     *
     * @param {string} value
     */
    setTextboxValue: function setTextboxValue(value) {
      var $input = this.$control_input;
      var changed = $input.val() !== value;

      if (changed) {
        $input.val(value).triggerHandler('update');
        this.lastValue = value;
      }
    },

    /**
     * Returns the value of the control. If multiple items
     * can be selected (e.g. <select multiple>), this returns
     * an array. If only one item can be selected, this
     * returns a string.
     *
     * @returns {mixed}
     */
    getValue: function getValue() {
      if (this.tagType === TAG_SELECT && this.$input.attr('multiple')) {
        return this.items;
      } else {
        return this.items.join(this.settings.delimiter);
      }
    },

    /**
     * Resets the selected items to the given value.
     *
     * @param {mixed} value
     */
    setValue: function setValue(value, silent) {
      var events = silent ? [] : ['change'];
      debounce_events(this, events, function () {
        this.clear(silent);
        this.addItems(value, silent);
      });
    },

    /**
     * Sets the selected item.
     *
     * @param {object} $item
     * @param {object} e (optional)
     */
    setActiveItem: function setActiveItem($item, e) {
      var self = this;
      var eventName;
      var i, idx, begin, end, item, swap;
      var $last;
      if (self.settings.mode === 'single') return;
      $item = $($item); // clear the active selection

      if (!$item.length) {
        $(self.$activeItems).removeClass('active');
        self.$activeItems = [];

        if (self.isFocused) {
          self.showInput();
        }

        return;
      } // modify selection


      eventName = e && e.type.toLowerCase();

      if (eventName === 'mousedown' && self.isShiftDown && self.$activeItems.length) {
        $last = self.$control.children('.active:last');
        begin = Array.prototype.indexOf.apply(self.$control[0].childNodes, [$last[0]]);
        end = Array.prototype.indexOf.apply(self.$control[0].childNodes, [$item[0]]);

        if (begin > end) {
          swap = begin;
          begin = end;
          end = swap;
        }

        for (i = begin; i <= end; i++) {
          item = self.$control[0].childNodes[i];

          if (self.$activeItems.indexOf(item) === -1) {
            $(item).addClass('active');
            self.$activeItems.push(item);
          }
        }

        e.preventDefault();
      } else if (eventName === 'mousedown' && self.isCtrlDown || eventName === 'keydown' && this.isShiftDown) {
        if ($item.hasClass('active')) {
          idx = self.$activeItems.indexOf($item[0]);
          self.$activeItems.splice(idx, 1);
          $item.removeClass('active');
        } else {
          self.$activeItems.push($item.addClass('active')[0]);
        }
      } else {
        $(self.$activeItems).removeClass('active');
        self.$activeItems = [$item.addClass('active')[0]];
      } // ensure control has focus


      self.hideInput();

      if (!this.isFocused) {
        self.focus();
      }
    },

    /**
     * Sets the selected item in the dropdown menu
     * of available options.
     *
     * @param {object} $object
     * @param {boolean} scroll
     * @param {boolean} animate
     */
    setActiveOption: function setActiveOption($option, scroll, animate) {
      var height_menu, height_item, y;
      var scroll_top, scroll_bottom;
      var self = this;
      if (self.$activeOption) self.$activeOption.removeClass('active');
      self.$activeOption = null;
      $option = $($option);
      if (!$option.length) return;
      self.$activeOption = $option.addClass('active');

      if (scroll || !isset(scroll)) {
        height_menu = self.$dropdown_content.height();
        height_item = self.$activeOption.outerHeight(true);
        scroll = self.$dropdown_content.scrollTop() || 0;
        y = self.$activeOption.offset().top - self.$dropdown_content.offset().top + scroll;
        scroll_top = y;
        scroll_bottom = y - height_menu + height_item;

        if (y + height_item > height_menu + scroll) {
          self.$dropdown_content.stop().animate({
            scrollTop: scroll_bottom
          }, animate ? self.settings.scrollDuration : 0);
        } else if (y < scroll) {
          self.$dropdown_content.stop().animate({
            scrollTop: scroll_top
          }, animate ? self.settings.scrollDuration : 0);
        }
      }
    },

    /**
     * Selects all items (CTRL + A).
     */
    selectAll: function selectAll() {
      var self = this;
      if (self.settings.mode === 'single') return;
      self.$activeItems = Array.prototype.slice.apply(self.$control.children(':not(input)').addClass('active'));

      if (self.$activeItems.length) {
        self.hideInput();
        self.close();
      }

      self.focus();
    },

    /**
     * Hides the input element out of view, while
     * retaining its focus.
     */
    hideInput: function hideInput() {
      var self = this;
      self.setTextboxValue('');
      self.$control_input.css({
        opacity: 0,
        position: 'absolute',
        left: self.rtl ? 10000 : -10000
      });
      self.isInputHidden = true;
    },

    /**
     * Restores input visibility.
     */
    showInput: function showInput() {
      this.$control_input.css({
        opacity: 1,
        position: 'relative',
        left: 0
      });
      this.isInputHidden = false;
    },

    /**
     * Gives the control focus.
     */
    focus: function focus() {
      var self = this;
      if (self.isDisabled) return;
      self.ignoreFocus = true;
      self.$control_input[0].focus();
      window.setTimeout(function () {
        self.ignoreFocus = false;
        self.onFocus();
      }, 0);
    },

    /**
     * Forces the control out of focus.
     *
     * @param {Element} dest
     */
    blur: function blur(dest) {
      this.$control_input[0].blur();
      this.onBlur(null, dest);
    },

    /**
     * Returns a function that scores an object
     * to show how good of a match it is to the
     * provided query.
     *
     * @param {string} query
     * @param {object} options
     * @return {function}
     */
    getScoreFunction: function getScoreFunction(query) {
      return this.sifter.getScoreFunction(query, this.getSearchOptions());
    },

    /**
     * Returns search options for sifter (the system
     * for scoring and sorting results).
     *
     * @see https://github.com/brianreavis/sifter.js
     * @return {object}
     */
    getSearchOptions: function getSearchOptions() {
      var settings = this.settings;
      var sort = settings.sortField;

      if (typeof sort === 'string') {
        sort = [{
          field: sort
        }];
      }

      return {
        fields: settings.searchField,
        conjunction: settings.searchConjunction,
        sort: sort
      };
    },

    /**
     * Searches through available options and returns
     * a sorted array of matches.
     *
     * Returns an object containing:
     *
     *   - query {string}
     *   - tokens {array}
     *   - total {int}
     *   - items {array}
     *
     * @param {string} query
     * @returns {object}
     */
    search: function search(query) {
      var i, value, score, result, calculateScore;
      var self = this;
      var settings = self.settings;
      var options = this.getSearchOptions(); // validate user-provided result scoring function

      if (settings.score) {
        calculateScore = self.settings.score.apply(this, [query]);

        if (typeof calculateScore !== 'function') {
          throw new Error('Selectize "score" setting must be a function that returns a function');
        }
      } // perform search


      if (query !== self.lastQuery) {
        self.lastQuery = query;
        result = self.sifter.search(query, $.extend(options, {
          score: calculateScore
        }));
        self.currentResults = result;
      } else {
        result = $.extend(true, {}, self.currentResults);
      } // filter out selected items


      if (settings.hideSelected) {
        for (i = result.items.length - 1; i >= 0; i--) {
          if (self.items.indexOf(hash_key(result.items[i].id)) !== -1) {
            result.items.splice(i, 1);
          }
        }
      }

      return result;
    },

    /**
     * Refreshes the list of available options shown
     * in the autocomplete dropdown menu.
     *
     * @param {boolean} triggerDropdown
     */
    refreshOptions: function refreshOptions(triggerDropdown) {
      var i, j, k, n, groups, groups_order, option, option_html, optgroup, optgroups, html, html_children, has_create_option;
      var $active, $active_before, $create;

      if (typeof triggerDropdown === 'undefined') {
        triggerDropdown = true;
      }

      var self = this;
      var query = $.trim(self.$control_input.val());
      var results = self.search(query);
      var $dropdown_content = self.$dropdown_content;
      var active_before = self.$activeOption && hash_key(self.$activeOption.attr('data-value')); // build markup

      n = results.items.length;

      if (typeof self.settings.maxOptions === 'number') {
        n = Math.min(n, self.settings.maxOptions);
      } // render and group available options individually


      groups = {};
      groups_order = [];

      for (i = 0; i < n; i++) {
        option = self.options[results.items[i].id];
        option_html = self.render('option', option);
        optgroup = option[self.settings.optgroupField] || '';
        optgroups = $.isArray(optgroup) ? optgroup : [optgroup];

        for (j = 0, k = optgroups && optgroups.length; j < k; j++) {
          optgroup = optgroups[j];

          if (!self.optgroups.hasOwnProperty(optgroup)) {
            optgroup = '';
          }

          if (!groups.hasOwnProperty(optgroup)) {
            groups[optgroup] = [];
            groups_order.push(optgroup);
          }

          groups[optgroup].push(option_html);
        }
      } // sort optgroups


      if (this.settings.lockOptgroupOrder) {
        groups_order.sort(function (a, b) {
          var a_order = self.optgroups[a].$order || 0;
          var b_order = self.optgroups[b].$order || 0;
          return a_order - b_order;
        });
      } // render optgroup headers & join groups


      html = [];

      for (i = 0, n = groups_order.length; i < n; i++) {
        optgroup = groups_order[i];

        if (self.optgroups.hasOwnProperty(optgroup) && groups[optgroup].length) {
          // render the optgroup header and options within it,
          // then pass it to the wrapper template
          html_children = self.render('optgroup_header', self.optgroups[optgroup]) || '';
          html_children += groups[optgroup].join('');
          html.push(self.render('optgroup', $.extend({}, self.optgroups[optgroup], {
            html: html_children
          })));
        } else {
          html.push(groups[optgroup].join(''));
        }
      }

      $dropdown_content.html(html.join('')); // highlight matching terms inline

      if (self.settings.highlight && results.query.length && results.tokens.length) {
        for (i = 0, n = results.tokens.length; i < n; i++) {
          highlight($dropdown_content, results.tokens[i].regex);
        }
      } // add "selected" class to selected options


      if (!self.settings.hideSelected) {
        for (i = 0, n = self.items.length; i < n; i++) {
          self.getOption(self.items[i]).addClass('selected');
        }
      } // add create option


      has_create_option = self.canCreate(query);

      if (has_create_option) {
        $dropdown_content.prepend(self.render('option_create', {
          input: query
        }));
        $create = $($dropdown_content[0].childNodes[0]);
      } // activate


      self.hasOptions = results.items.length > 0 || has_create_option;

      if (self.hasOptions) {
        if (results.items.length > 0) {
          $active_before = active_before && self.getOption(active_before);

          if ($active_before && $active_before.length) {
            $active = $active_before;
          } else if (self.settings.mode === 'single' && self.items.length) {
            $active = self.getOption(self.items[0]);
          }

          if (!$active || !$active.length) {
            if ($create && !self.settings.addPrecedence) {
              $active = self.getAdjacentOption($create, 1);
            } else {
              $active = $dropdown_content.find('[data-selectable]:first');
            }
          }
        } else {
          $active = $create;
        }

        self.setActiveOption($active);

        if (triggerDropdown && !self.isOpen) {
          self.open();
        }
      } else {
        self.setActiveOption(null);

        if (triggerDropdown && self.isOpen) {
          self.close();
        }
      }
    },

    /**
     * Adds an available option. If it already exists,
     * nothing will happen. Note: this does not refresh
     * the options list dropdown (use `refreshOptions`
     * for that).
     *
     * Usage:
     *
     *   this.addOption(data)
     *
     * @param {object|array} data
     */
    addOption: function addOption(data) {
      var i,
          n,
          value,
          self = this;

      if ($.isArray(data)) {
        for (i = 0, n = data.length; i < n; i++) {
          self.addOption(data[i]);
        }

        return;
      }

      if (value = self.registerOption(data)) {
        self.userOptions[value] = true;
        self.lastQuery = null;
        self.trigger('option_add', value, data);
      }
    },

    /**
     * Registers an option to the pool of options.
     *
     * @param {object} data
     * @return {boolean|string}
     */
    registerOption: function registerOption(data) {
      var key = hash_key(data[this.settings.valueField]);
      if (!key || this.options.hasOwnProperty(key)) return false;
      data.$order = data.$order || ++this.order;
      this.options[key] = data;
      return key;
    },

    /**
     * Registers an option group to the pool of option groups.
     *
     * @param {object} data
     * @return {boolean|string}
     */
    registerOptionGroup: function registerOptionGroup(data) {
      var key = hash_key(data[this.settings.optgroupValueField]);
      if (!key) return false;
      data.$order = data.$order || ++this.order;
      this.optgroups[key] = data;
      return key;
    },

    /**
     * Registers a new optgroup for options
     * to be bucketed into.
     *
     * @param {string} id
     * @param {object} data
     */
    addOptionGroup: function addOptionGroup(id, data) {
      data[this.settings.optgroupValueField] = id;

      if (id = this.registerOptionGroup(data)) {
        this.trigger('optgroup_add', id, data);
      }
    },

    /**
     * Removes an existing option group.
     *
     * @param {string} id
     */
    removeOptionGroup: function removeOptionGroup(id) {
      if (this.optgroups.hasOwnProperty(id)) {
        delete this.optgroups[id];
        this.renderCache = {};
        this.trigger('optgroup_remove', id);
      }
    },

    /**
     * Clears all existing option groups.
     */
    clearOptionGroups: function clearOptionGroups() {
      this.optgroups = {};
      this.renderCache = {};
      this.trigger('optgroup_clear');
    },

    /**
     * Updates an option available for selection. If
     * it is visible in the selected items or options
     * dropdown, it will be re-rendered automatically.
     *
     * @param {string} value
     * @param {object} data
     */
    updateOption: function updateOption(value, data) {
      var self = this;
      var $item, $item_new;
      var value_new, index_item, cache_items, cache_options, order_old;
      value = hash_key(value);
      value_new = hash_key(data[self.settings.valueField]); // sanity checks

      if (value === null) return;
      if (!self.options.hasOwnProperty(value)) return;
      if (typeof value_new !== 'string') throw new Error('Value must be set in option data');
      order_old = self.options[value].$order; // update references

      if (value_new !== value) {
        delete self.options[value];
        index_item = self.items.indexOf(value);

        if (index_item !== -1) {
          self.items.splice(index_item, 1, value_new);
        }
      }

      data.$order = data.$order || order_old;
      self.options[value_new] = data; // invalidate render cache

      cache_items = self.renderCache['item'];
      cache_options = self.renderCache['option'];

      if (cache_items) {
        delete cache_items[value];
        delete cache_items[value_new];
      }

      if (cache_options) {
        delete cache_options[value];
        delete cache_options[value_new];
      } // update the item if it's selected


      if (self.items.indexOf(value_new) !== -1) {
        $item = self.getItem(value);
        $item_new = $(self.render('item', data));
        if ($item.hasClass('active')) $item_new.addClass('active');
        $item.replaceWith($item_new);
      } // invalidate last query because we might have updated the sortField


      self.lastQuery = null; // update dropdown contents

      if (self.isOpen) {
        self.refreshOptions(false);
      }
    },

    /**
     * Removes a single option.
     *
     * @param {string} value
     * @param {boolean} silent
     */
    removeOption: function removeOption(value, silent) {
      var self = this;
      value = hash_key(value);
      var cache_items = self.renderCache['item'];
      var cache_options = self.renderCache['option'];
      if (cache_items) delete cache_items[value];
      if (cache_options) delete cache_options[value];
      delete self.userOptions[value];
      delete self.options[value];
      self.lastQuery = null;
      self.trigger('option_remove', value);
      self.removeItem(value, silent);
    },

    /**
     * Clears all options.
     */
    clearOptions: function clearOptions() {
      var self = this;
      self.loadedSearches = {};
      self.userOptions = {};
      self.renderCache = {};
      self.options = self.sifter.items = {};
      self.lastQuery = null;
      self.trigger('option_clear');
      self.clear();
    },

    /**
     * Returns the jQuery element of the option
     * matching the given value.
     *
     * @param {string} value
     * @returns {object}
     */
    getOption: function getOption(value) {
      return this.getElementWithValue(value, this.$dropdown_content.find('[data-selectable]'));
    },

    /**
     * Returns the jQuery element of the next or
     * previous selectable option.
     *
     * @param {object} $option
     * @param {int} direction  can be 1 for next or -1 for previous
     * @return {object}
     */
    getAdjacentOption: function getAdjacentOption($option, direction) {
      var $options = this.$dropdown.find('[data-selectable]');
      var index = $options.index($option) + direction;
      return index >= 0 && index < $options.length ? $options.eq(index) : $();
    },

    /**
     * Finds the first element with a "data-value" attribute
     * that matches the given value.
     *
     * @param {mixed} value
     * @param {object} $els
     * @return {object}
     */
    getElementWithValue: function getElementWithValue(value, $els) {
      value = hash_key(value);

      if (typeof value !== 'undefined' && value !== null) {
        for (var i = 0, n = $els.length; i < n; i++) {
          if ($els[i].getAttribute('data-value') === value) {
            return $($els[i]);
          }
        }
      }

      return $();
    },

    /**
     * Returns the jQuery element of the item
     * matching the given value.
     *
     * @param {string} value
     * @returns {object}
     */
    getItem: function getItem(value) {
      return this.getElementWithValue(value, this.$control.children());
    },

    /**
     * "Selects" multiple items at once. Adds them to the list
     * at the current caret position.
     *
     * @param {string} value
     * @param {boolean} silent
     */
    addItems: function addItems(values, silent) {
      var items = $.isArray(values) ? values : [values];

      for (var i = 0, n = items.length; i < n; i++) {
        this.isPending = i < n - 1;
        this.addItem(items[i], silent);
      }
    },

    /**
     * "Selects" an item. Adds it to the list
     * at the current caret position.
     *
     * @param {string} value
     * @param {boolean} silent
     */
    addItem: function addItem(value, silent) {
      var events = silent ? [] : ['change'];
      debounce_events(this, events, function () {
        var $item, $option, $options;
        var self = this;
        var inputMode = self.settings.mode;
        var i, active, value_next, wasFull;
        value = hash_key(value);

        if (self.items.indexOf(value) !== -1) {
          if (inputMode === 'single') self.close();
          return;
        }

        if (!self.options.hasOwnProperty(value)) return;
        if (inputMode === 'single') self.clear(silent);
        if (inputMode === 'multi' && self.isFull()) return;
        $item = $(self.render('item', self.options[value]));
        wasFull = self.isFull();
        self.items.splice(self.caretPos, 0, value);
        self.insertAtCaret($item);

        if (!self.isPending || !wasFull && self.isFull()) {
          self.refreshState();
        }

        if (self.isSetup) {
          $options = self.$dropdown_content.find('[data-selectable]'); // update menu / remove the option (if this is not one item being added as part of series)

          if (!self.isPending) {
            $option = self.getOption(value);
            value_next = self.getAdjacentOption($option, 1).attr('data-value');
            self.refreshOptions(self.isFocused && inputMode !== 'single');

            if (value_next) {
              self.setActiveOption(self.getOption(value_next));
            }
          } // hide the menu if the maximum number of items have been selected or no options are left


          if (!$options.length || self.isFull()) {
            self.close();
          } else {
            self.positionDropdown();
          }

          self.updatePlaceholder();
          self.trigger('item_add', value, $item);
          self.updateOriginalInput({
            silent: silent
          });
        }
      });
    },

    /**
     * Removes the selected item matching
     * the provided value.
     *
     * @param {string} value
     */
    removeItem: function removeItem(value, silent) {
      var self = this;
      var $item, i, idx;
      $item = _typeof(value) === 'object' ? value : self.getItem(value);
      value = hash_key($item.attr('data-value'));
      i = self.items.indexOf(value);

      if (i !== -1) {
        $item.remove();

        if ($item.hasClass('active')) {
          idx = self.$activeItems.indexOf($item[0]);
          self.$activeItems.splice(idx, 1);
        }

        self.items.splice(i, 1);
        self.lastQuery = null;

        if (!self.settings.persist && self.userOptions.hasOwnProperty(value)) {
          self.removeOption(value, silent);
        }

        if (i < self.caretPos) {
          self.setCaret(self.caretPos - 1);
        }

        self.refreshState();
        self.updatePlaceholder();
        self.updateOriginalInput({
          silent: silent
        });
        self.positionDropdown();
        self.trigger('item_remove', value, $item);
      }
    },

    /**
     * Invokes the `create` method provided in the
     * selectize options that should provide the data
     * for the new item, given the user input.
     *
     * Once this completes, it will be added
     * to the item list.
     *
     * @param {string} value
     * @param {boolean} [triggerDropdown]
     * @param {function} [callback]
     * @return {boolean}
     */
    createItem: function createItem(input, triggerDropdown) {
      var self = this;
      var caret = self.caretPos;
      input = input || $.trim(self.$control_input.val() || '');
      var callback = arguments[arguments.length - 1];
      if (typeof callback !== 'function') callback = function callback() {};

      if (typeof triggerDropdown !== 'boolean') {
        triggerDropdown = true;
      }

      if (!self.canCreate(input)) {
        callback();
        return false;
      }

      self.lock();
      var setup = typeof self.settings.create === 'function' ? this.settings.create : function (input) {
        var data = {};
        data[self.settings.labelField] = input;
        data[self.settings.valueField] = input;
        return data;
      };
      var create = once(function (data) {
        self.unlock();
        if (!data || _typeof(data) !== 'object') return callback();
        var value = hash_key(data[self.settings.valueField]);
        if (typeof value !== 'string') return callback();
        self.setTextboxValue('');
        self.addOption(data);
        self.setCaret(caret);
        self.addItem(value);
        self.refreshOptions(triggerDropdown && self.settings.mode !== 'single');
        callback(data);
      });
      var output = setup.apply(this, [input, create]);

      if (typeof output !== 'undefined') {
        create(output);
      }

      return true;
    },

    /**
     * Re-renders the selected item lists.
     */
    refreshItems: function refreshItems() {
      this.lastQuery = null;

      if (this.isSetup) {
        this.addItem(this.items);
      }

      this.refreshState();
      this.updateOriginalInput();
    },

    /**
     * Updates all state-dependent attributes
     * and CSS classes.
     */
    refreshState: function refreshState() {
      var invalid,
          self = this;

      if (self.isRequired) {
        if (self.items.length) self.isInvalid = false;
        self.$control_input.prop('required', invalid);
      }

      self.refreshClasses();
    },

    /**
     * Updates all state-dependent CSS classes.
     */
    refreshClasses: function refreshClasses() {
      var self = this;
      var isFull = self.isFull();
      var isLocked = self.isLocked;
      self.$wrapper.toggleClass('rtl', self.rtl);
      self.$control.toggleClass('focus', self.isFocused).toggleClass('disabled', self.isDisabled).toggleClass('required', self.isRequired).toggleClass('invalid', self.isInvalid).toggleClass('locked', isLocked).toggleClass('full', isFull).toggleClass('not-full', !isFull).toggleClass('input-active', self.isFocused && !self.isInputHidden).toggleClass('dropdown-active', self.isOpen).toggleClass('has-options', !$.isEmptyObject(self.options)).toggleClass('has-items', self.items.length > 0);
      self.$control_input.data('grow', !isFull && !isLocked);
    },

    /**
     * Determines whether or not more items can be added
     * to the control without exceeding the user-defined maximum.
     *
     * @returns {boolean}
     */
    isFull: function isFull() {
      return this.settings.maxItems !== null && this.items.length >= this.settings.maxItems;
    },

    /**
     * Refreshes the original <select> or <input>
     * element to reflect the current state.
     */
    updateOriginalInput: function updateOriginalInput(opts) {
      var i,
          n,
          options,
          label,
          self = this;
      opts = opts || {};

      if (self.tagType === TAG_SELECT) {
        options = [];

        for (i = 0, n = self.items.length; i < n; i++) {
          label = self.options[self.items[i]][self.settings.labelField] || '';
          options.push('<option value="' + escape_html(self.items[i]) + '" selected="selected">' + escape_html(label) + '</option>');
        }

        if (!options.length && !this.$input.attr('multiple')) {
          options.push('<option value="" selected="selected"></option>');
        }

        self.$input.html(options.join(''));
      } else {
        self.$input.val(self.getValue());
        self.$input.attr('value', self.$input.val());
      }

      if (self.isSetup) {
        if (!opts.silent) {
          self.trigger('change', self.$input.val());
        }
      }
    },

    /**
     * Shows/hide the input placeholder depending
     * on if there items in the list already.
     */
    updatePlaceholder: function updatePlaceholder() {
      if (!this.settings.placeholder) return;
      var $input = this.$control_input;

      if (this.items.length) {
        $input.removeAttr('placeholder');
      } else {
        $input.attr('placeholder', this.settings.placeholder);
      }

      $input.triggerHandler('update', {
        force: true
      });
    },

    /**
     * Shows the autocomplete dropdown containing
     * the available options.
     */
    open: function open() {
      var self = this;
      if (self.isLocked || self.isOpen || self.settings.mode === 'multi' && self.isFull()) return;
      self.focus();
      self.isOpen = true;
      self.refreshState();
      self.$dropdown.css({
        visibility: 'hidden',
        display: 'block'
      });
      self.positionDropdown();
      self.$dropdown.css({
        visibility: 'visible'
      });
      self.trigger('dropdown_open', self.$dropdown);
    },

    /**
     * Closes the autocomplete dropdown menu.
     */
    close: function close() {
      var self = this;
      var trigger = self.isOpen;

      if (self.settings.mode === 'single' && self.items.length) {
        self.hideInput();
      }

      self.isOpen = false;
      self.$dropdown.hide();
      self.setActiveOption(null);
      self.refreshState();
      if (trigger) self.trigger('dropdown_close', self.$dropdown);
    },

    /**
     * Calculates and applies the appropriate
     * position of the dropdown.
     */
    positionDropdown: function positionDropdown() {
      var $control = this.$control;
      var offset = this.settings.dropdownParent === 'body' ? $control.offset() : $control.position();
      offset.top += $control.outerHeight(true);
      this.$dropdown.css({
        width: $control.outerWidth(),
        top: offset.top,
        left: offset.left
      });
    },

    /**
     * Resets / clears all selected items
     * from the control.
     *
     * @param {boolean} silent
     */
    clear: function clear(silent) {
      var self = this;
      if (!self.items.length) return;
      self.$control.children(':not(input)').remove();
      self.items = [];
      self.lastQuery = null;
      self.setCaret(0);
      self.setActiveItem(null);
      self.updatePlaceholder();
      self.updateOriginalInput({
        silent: silent
      });
      self.refreshState();
      self.showInput();
      self.trigger('clear');
    },

    /**
     * A helper method for inserting an element
     * at the current caret position.
     *
     * @param {object} $el
     */
    insertAtCaret: function insertAtCaret($el) {
      var caret = Math.min(this.caretPos, this.items.length);

      if (caret === 0) {
        this.$control.prepend($el);
      } else {
        $(this.$control[0].childNodes[caret]).before($el);
      }

      this.setCaret(caret + 1);
    },

    /**
     * Removes the current selected item(s).
     *
     * @param {object} e (optional)
     * @returns {boolean}
     */
    deleteSelection: function deleteSelection(e) {
      var i, n, direction, selection, values, caret, option_select, $option_select, $tail;
      var self = this;
      direction = e && e.keyCode === KEY_BACKSPACE ? -1 : 1;
      selection = getSelection(self.$control_input[0]);

      if (self.$activeOption && !self.settings.hideSelected) {
        option_select = self.getAdjacentOption(self.$activeOption, -1).attr('data-value');
      } // determine items that will be removed


      values = [];

      if (self.$activeItems.length) {
        $tail = self.$control.children('.active:' + (direction > 0 ? 'last' : 'first'));
        caret = self.$control.children(':not(input)').index($tail);

        if (direction > 0) {
          caret++;
        }

        for (i = 0, n = self.$activeItems.length; i < n; i++) {
          values.push($(self.$activeItems[i]).attr('data-value'));
        }

        if (e) {
          e.preventDefault();
          e.stopPropagation();
        }
      } else if ((self.isFocused || self.settings.mode === 'single') && self.items.length) {
        if (direction < 0 && selection.start === 0 && selection.length === 0) {
          values.push(self.items[self.caretPos - 1]);
        } else if (direction > 0 && selection.start === self.$control_input.val().length) {
          values.push(self.items[self.caretPos]);
        }
      } // allow the callback to abort


      if (!values.length || typeof self.settings.onDelete === 'function' && self.settings.onDelete.apply(self, [values]) === false) {
        return false;
      } // perform removal


      if (typeof caret !== 'undefined') {
        self.setCaret(caret);
      }

      while (values.length) {
        self.removeItem(values.pop());
      }

      self.showInput();
      self.positionDropdown();
      self.refreshOptions(true); // select previous option

      if (option_select) {
        $option_select = self.getOption(option_select);

        if ($option_select.length) {
          self.setActiveOption($option_select);
        }
      }

      return true;
    },

    /**
     * Selects the previous / next item (depending
     * on the `direction` argument).
     *
     * > 0 - right
     * < 0 - left
     *
     * @param {int} direction
     * @param {object} e (optional)
     */
    advanceSelection: function advanceSelection(direction, e) {
      var tail, selection, idx, valueLength, cursorAtEdge, $tail;
      var self = this;
      if (direction === 0) return;
      if (self.rtl) direction *= -1;
      tail = direction > 0 ? 'last' : 'first';
      selection = getSelection(self.$control_input[0]);

      if (self.isFocused && !self.isInputHidden) {
        valueLength = self.$control_input.val().length;
        cursorAtEdge = direction < 0 ? selection.start === 0 && selection.length === 0 : selection.start === valueLength;

        if (cursorAtEdge && !valueLength) {
          self.advanceCaret(direction, e);
        }
      } else {
        $tail = self.$control.children('.active:' + tail);

        if ($tail.length) {
          idx = self.$control.children(':not(input)').index($tail);
          self.setActiveItem(null);
          self.setCaret(direction > 0 ? idx + 1 : idx);
        }
      }
    },

    /**
     * Moves the caret left / right.
     *
     * @param {int} direction
     * @param {object} e (optional)
     */
    advanceCaret: function advanceCaret(direction, e) {
      var self = this,
          fn,
          $adj;
      if (direction === 0) return;
      fn = direction > 0 ? 'next' : 'prev';

      if (self.isShiftDown) {
        $adj = self.$control_input[fn]();

        if ($adj.length) {
          self.hideInput();
          self.setActiveItem($adj);
          e && e.preventDefault();
        }
      } else {
        self.setCaret(self.caretPos + direction);
      }
    },

    /**
     * Moves the caret to the specified index.
     *
     * @param {int} i
     */
    setCaret: function setCaret(i) {
      var self = this;

      if (self.settings.mode === 'single') {
        i = self.items.length;
      } else {
        i = Math.max(0, Math.min(self.items.length, i));
      }

      if (!self.isPending) {
        // the input must be moved by leaving it in place and moving the
        // siblings, due to the fact that focus cannot be restored once lost
        // on mobile webkit devices
        var j, n, fn, $children, $child;
        $children = self.$control.children(':not(input)');

        for (j = 0, n = $children.length; j < n; j++) {
          $child = $($children[j]).detach();

          if (j < i) {
            self.$control_input.before($child);
          } else {
            self.$control.append($child);
          }
        }
      }

      self.caretPos = i;
    },

    /**
     * Disables user input on the control. Used while
     * items are being asynchronously created.
     */
    lock: function lock() {
      this.close();
      this.isLocked = true;
      this.refreshState();
    },

    /**
     * Re-enables user input on the control.
     */
    unlock: function unlock() {
      this.isLocked = false;
      this.refreshState();
    },

    /**
     * Disables user input on the control completely.
     * While disabled, it cannot receive focus.
     */
    disable: function disable() {
      var self = this;
      self.$input.prop('disabled', true);
      self.$control_input.prop('disabled', true).prop('tabindex', -1);
      self.isDisabled = true;
      self.lock();
    },

    /**
     * Enables the control so that it can respond
     * to focus and user input.
     */
    enable: function enable() {
      var self = this;
      self.$input.prop('disabled', false);
      self.$control_input.prop('disabled', false).prop('tabindex', self.tabIndex);
      self.isDisabled = false;
      self.unlock();
    },

    /**
     * Completely destroys the control and
     * unbinds all event listeners so that it can
     * be garbage collected.
     */
    destroy: function destroy() {
      var self = this;
      var eventNS = self.eventNS;
      var revertSettings = self.revertSettings;
      self.trigger('destroy');
      self.off();
      self.$wrapper.remove();
      self.$dropdown.remove();
      self.$input.html('').append(revertSettings.$children).removeAttr('tabindex').removeClass('selectized').attr({
        tabindex: revertSettings.tabindex
      }).show();
      self.$control_input.removeData('grow');
      self.$input.removeData('selectize');
      $(window).off(eventNS);
      $(document).off(eventNS);
      $(document.body).off(eventNS);
      delete self.$input[0].selectize;
    },

    /**
     * A helper method for rendering "item" and
     * "option" templates, given the data.
     *
     * @param {string} templateName
     * @param {object} data
     * @returns {string}
     */
    render: function render(templateName, data) {
      var value, id, label;
      var html = '';
      var cache = false;
      var self = this;
      var regex_tag = /^[\t \r\n]*<([a-z][a-z0-9\-_]*(?:\:[a-z][a-z0-9\-_]*)?)/i;

      if (templateName === 'option' || templateName === 'item') {
        value = hash_key(data[self.settings.valueField]);
        cache = !!value;
      } // pull markup from cache if it exists


      if (cache) {
        if (!isset(self.renderCache[templateName])) {
          self.renderCache[templateName] = {};
        }

        if (self.renderCache[templateName].hasOwnProperty(value)) {
          return self.renderCache[templateName][value];
        }
      } // render markup


      html = self.settings.render[templateName].apply(this, [data, escape_html]); // add mandatory attributes

      if (templateName === 'option' || templateName === 'option_create') {
        html = html.replace(regex_tag, '<$1 data-selectable');
      }

      if (templateName === 'optgroup') {
        id = data[self.settings.optgroupValueField] || '';
        html = html.replace(regex_tag, '<$1 data-group="' + escape_replace(escape_html(id)) + '"');
      }

      if (templateName === 'option' || templateName === 'item') {
        html = html.replace(regex_tag, '<$1 data-value="' + escape_replace(escape_html(value || '')) + '"');
      } // update cache


      if (cache) {
        self.renderCache[templateName][value] = html;
      }

      return html;
    },

    /**
     * Clears the render cache for a template. If
     * no template is given, clears all render
     * caches.
     *
     * @param {string} templateName
     */
    clearCache: function clearCache(templateName) {
      var self = this;

      if (typeof templateName === 'undefined') {
        self.renderCache = {};
      } else {
        delete self.renderCache[templateName];
      }
    },

    /**
     * Determines whether or not to display the
     * create item prompt, given a user input.
     *
     * @param {string} input
     * @return {boolean}
     */
    canCreate: function canCreate(input) {
      var self = this;
      if (!self.settings.create) return false;
      var filter = self.settings.createFilter;
      return input.length && (typeof filter !== 'function' || filter.apply(self, [input])) && (typeof filter !== 'string' || new RegExp(filter).test(input)) && (!(filter instanceof RegExp) || filter.test(input));
    }
  });
  Selectize.count = 0;
  Selectize.defaults = {
    options: [],
    optgroups: [],
    plugins: [],
    delimiter: ',',
    splitOn: null,
    // regexp or string for splitting up values from a paste command
    persist: true,
    diacritics: true,
    create: false,
    createOnBlur: false,
    createFilter: null,
    highlight: true,
    openOnFocus: true,
    maxOptions: 1000,
    maxItems: null,
    hideSelected: null,
    addPrecedence: false,
    selectOnTab: false,
    preload: false,
    allowEmptyOption: false,
    closeAfterSelect: false,
    scrollDuration: 60,
    loadThrottle: 300,
    loadingClass: 'loading',
    dataAttr: 'data-data',
    optgroupField: 'optgroup',
    valueField: 'value',
    labelField: 'text',
    optgroupLabelField: 'label',
    optgroupValueField: 'value',
    lockOptgroupOrder: false,
    sortField: '$order',
    searchField: ['text'],
    searchConjunction: 'and',
    mode: null,
    wrapperClass: 'selectize-control',
    inputClass: 'selectize-input',
    dropdownClass: 'selectize-dropdown',
    dropdownContentClass: 'selectize-dropdown-content',
    dropdownParent: null,
    copyClassesToDropdown: true,

    /*
    load                 : null, // function(query, callback) { ... }
    score                : null, // function(search) { ... }
    onInitialize         : null, // function() { ... }
    onChange             : null, // function(value) { ... }
    onItemAdd            : null, // function(value, $item) { ... }
    onItemRemove         : null, // function(value) { ... }
    onClear              : null, // function() { ... }
    onOptionAdd          : null, // function(value, data) { ... }
    onOptionRemove       : null, // function(value) { ... }
    onOptionClear        : null, // function() { ... }
    onOptionGroupAdd     : null, // function(id, data) { ... }
    onOptionGroupRemove  : null, // function(id) { ... }
    onOptionGroupClear   : null, // function() { ... }
    onDropdownOpen       : null, // function($dropdown) { ... }
    onDropdownClose      : null, // function($dropdown) { ... }
    onType               : null, // function(str) { ... }
    onDelete             : null, // function(values) { ... }
    */
    render: {
      /*
      item: null,
      optgroup: null,
      optgroup_header: null,
      option: null,
      option_create: null
      */
    }
  };

  $.fn.selectize = function (settings_user) {
    var defaults = $.fn.selectize.defaults;
    var settings = $.extend({}, defaults, settings_user);
    var attr_data = settings.dataAttr;
    var field_label = settings.labelField;
    var field_value = settings.valueField;
    var field_optgroup = settings.optgroupField;
    var field_optgroup_label = settings.optgroupLabelField;
    var field_optgroup_value = settings.optgroupValueField;
    /**
     * Initializes selectize from a <input type="text"> element.
     *
     * @param {object} $input
     * @param {object} settings_element
     */

    var init_textbox = function init_textbox($input, settings_element) {
      var i, n, values, option;
      var data_raw = $input.attr(attr_data);

      if (!data_raw) {
        var value = $.trim($input.val() || '');
        if (!settings.allowEmptyOption && !value.length) return;
        values = value.split(settings.delimiter);

        for (i = 0, n = values.length; i < n; i++) {
          option = {};
          option[field_label] = values[i];
          option[field_value] = values[i];
          settings_element.options.push(option);
        }

        settings_element.items = values;
      } else {
        settings_element.options = JSON.parse(data_raw);

        for (i = 0, n = settings_element.options.length; i < n; i++) {
          settings_element.items.push(settings_element.options[i][field_value]);
        }
      }
    };
    /**
     * Initializes selectize from a <select> element.
     *
     * @param {object} $input
     * @param {object} settings_element
     */


    var init_select = function init_select($input, settings_element) {
      var i,
          n,
          tagName,
          $children,
          order = 0;
      var options = settings_element.options;
      var optionsMap = {};

      var readData = function readData($el) {
        var data = attr_data && $el.attr(attr_data);

        if (typeof data === 'string' && data.length) {
          return JSON.parse(data);
        }

        return null;
      };

      var addOption = function addOption($option, group) {
        $option = $($option);
        var value = hash_key($option.attr('value'));
        if (!value && !settings.allowEmptyOption) return; // if the option already exists, it's probably been
        // duplicated in another optgroup. in this case, push
        // the current group to the "optgroup" property on the
        // existing option so that it's rendered in both places.

        if (optionsMap.hasOwnProperty(value)) {
          if (group) {
            var arr = optionsMap[value][field_optgroup];

            if (!arr) {
              optionsMap[value][field_optgroup] = group;
            } else if (!$.isArray(arr)) {
              optionsMap[value][field_optgroup] = [arr, group];
            } else {
              arr.push(group);
            }
          }

          return;
        }

        var option = readData($option) || {};
        option[field_label] = option[field_label] || $option.text();
        option[field_value] = option[field_value] || value;
        option[field_optgroup] = option[field_optgroup] || group;
        optionsMap[value] = option;
        options.push(option);

        if ($option.is(':selected')) {
          settings_element.items.push(value);
        }
      };

      var addGroup = function addGroup($optgroup) {
        var i, n, id, optgroup, $options;
        $optgroup = $($optgroup);
        id = $optgroup.attr('label');

        if (id) {
          optgroup = readData($optgroup) || {};
          optgroup[field_optgroup_label] = id;
          optgroup[field_optgroup_value] = id;
          settings_element.optgroups.push(optgroup);
        }

        $options = $('option', $optgroup);

        for (i = 0, n = $options.length; i < n; i++) {
          addOption($options[i], id);
        }
      };

      settings_element.maxItems = $input.attr('multiple') ? null : 1;
      $children = $input.children();

      for (i = 0, n = $children.length; i < n; i++) {
        tagName = $children[i].tagName.toLowerCase();

        if (tagName === 'optgroup') {
          addGroup($children[i]);
        } else if (tagName === 'option') {
          addOption($children[i]);
        }
      }
    };

    return this.each(function () {
      if (this.selectize) return;
      var instance;
      var $input = $(this);
      var tag_name = this.tagName.toLowerCase();
      var placeholder = $input.attr('placeholder') || $input.attr('data-placeholder');

      if (!placeholder && !settings.allowEmptyOption) {
        placeholder = $input.children('option[value=""]').text();
      }

      var settings_element = {
        'placeholder': placeholder,
        'options': [],
        'optgroups': [],
        'items': []
      };

      if (tag_name === 'select') {
        init_select($input, settings_element);
      } else {
        init_textbox($input, settings_element);
      }

      instance = new Selectize($input, $.extend(true, {}, defaults, settings_element, settings_user));
    });
  };

  $.fn.selectize.defaults = Selectize.defaults;
  $.fn.selectize.support = {
    validity: SUPPORTS_VALIDITY_API
  };
  Selectize.define('drag_drop', function (options) {
    if (!$.fn.sortable) throw new Error('The "drag_drop" plugin requires jQuery UI "sortable".');
    if (this.settings.mode !== 'multi') return;
    var self = this;

    self.lock = function () {
      var original = self.lock;
      return function () {
        var sortable = self.$control.data('sortable');
        if (sortable) sortable.disable();
        return original.apply(self, arguments);
      };
    }();

    self.unlock = function () {
      var original = self.unlock;
      return function () {
        var sortable = self.$control.data('sortable');
        if (sortable) sortable.enable();
        return original.apply(self, arguments);
      };
    }();

    self.setup = function () {
      var original = self.setup;
      return function () {
        original.apply(this, arguments);
        var $control = self.$control.sortable({
          items: '[data-value]',
          forcePlaceholderSize: true,
          disabled: self.isLocked,
          start: function start(e, ui) {
            ui.placeholder.css('width', ui.helper.css('width'));
            $control.css({
              overflow: 'visible'
            });
          },
          stop: function stop() {
            $control.css({
              overflow: 'hidden'
            });
            var active = self.$activeItems ? self.$activeItems.slice() : null;
            var values = [];
            $control.children('[data-value]').each(function () {
              values.push($(this).attr('data-value'));
            });
            self.setValue(values);
            self.setActiveItem(active);
          }
        });
      };
    }();
  });
  Selectize.define('dropdown_header', function (options) {
    var self = this;
    options = $.extend({
      title: 'Untitled',
      headerClass: 'selectize-dropdown-header',
      titleRowClass: 'selectize-dropdown-header-title',
      labelClass: 'selectize-dropdown-header-label',
      closeClass: 'selectize-dropdown-header-close',
      html: function html(data) {
        return '<div class="' + data.headerClass + '">' + '<div class="' + data.titleRowClass + '">' + '<span class="' + data.labelClass + '">' + data.title + '</span>' + '<a href="javascript:void(0)" class="' + data.closeClass + '">&times;</a>' + '</div>' + '</div>';
      }
    }, options);

    self.setup = function () {
      var original = self.setup;
      return function () {
        original.apply(self, arguments);
        self.$dropdown_header = $(options.html(options));
        self.$dropdown.prepend(self.$dropdown_header);
      };
    }();
  });
  Selectize.define('optgroup_columns', function (options) {
    var self = this;
    options = $.extend({
      equalizeWidth: true,
      equalizeHeight: true
    }, options);

    this.getAdjacentOption = function ($option, direction) {
      var $options = $option.closest('[data-group]').find('[data-selectable]');
      var index = $options.index($option) + direction;
      return index >= 0 && index < $options.length ? $options.eq(index) : $();
    };

    this.onKeyDown = function () {
      var original = self.onKeyDown;
      return function (e) {
        var index, $option, $options, $optgroup;

        if (this.isOpen && (e.keyCode === KEY_LEFT || e.keyCode === KEY_RIGHT)) {
          self.ignoreHover = true;
          $optgroup = this.$activeOption.closest('[data-group]');
          index = $optgroup.find('[data-selectable]').index(this.$activeOption);

          if (e.keyCode === KEY_LEFT) {
            $optgroup = $optgroup.prev('[data-group]');
          } else {
            $optgroup = $optgroup.next('[data-group]');
          }

          $options = $optgroup.find('[data-selectable]');
          $option = $options.eq(Math.min($options.length - 1, index));

          if ($option.length) {
            this.setActiveOption($option);
          }

          return;
        }

        return original.apply(this, arguments);
      };
    }();

    var getScrollbarWidth = function getScrollbarWidth() {
      var div;
      var width = getScrollbarWidth.width;
      var doc = document;

      if (typeof width === 'undefined') {
        div = doc.createElement('div');
        div.innerHTML = '<div style="width:50px;height:50px;position:absolute;left:-50px;top:-50px;overflow:auto;"><div style="width:1px;height:100px;"></div></div>';
        div = div.firstChild;
        doc.body.appendChild(div);
        width = getScrollbarWidth.width = div.offsetWidth - div.clientWidth;
        doc.body.removeChild(div);
      }

      return width;
    };

    var equalizeSizes = function equalizeSizes() {
      var i, n, height_max, width, width_last, width_parent, $optgroups;
      $optgroups = $('[data-group]', self.$dropdown_content);
      n = $optgroups.length;
      if (!n || !self.$dropdown_content.width()) return;

      if (options.equalizeHeight) {
        height_max = 0;

        for (i = 0; i < n; i++) {
          height_max = Math.max(height_max, $optgroups.eq(i).height());
        }

        $optgroups.css({
          height: height_max
        });
      }

      if (options.equalizeWidth) {
        width_parent = self.$dropdown_content.innerWidth() - getScrollbarWidth();
        width = Math.round(width_parent / n);
        $optgroups.css({
          width: width
        });

        if (n > 1) {
          width_last = width_parent - width * (n - 1);
          $optgroups.eq(n - 1).css({
            width: width_last
          });
        }
      }
    };

    if (options.equalizeHeight || options.equalizeWidth) {
      hook.after(this, 'positionDropdown', equalizeSizes);
      hook.after(this, 'refreshOptions', equalizeSizes);
    }
  });
  Selectize.define('remove_button', function (options) {
    if (this.settings.mode === 'single') return;
    options = $.extend({
      label: '&times;',
      title: 'Remove',
      className: 'remove',
      append: true
    }, options);
    var self = this;
    var html = '<a href="javascript:void(0)" class="' + options.className + '" tabindex="-1" title="' + escape_html(options.title) + '">' + options.label + '</a>';
    /**
     * Appends an element as a child (with raw HTML).
     *
     * @param {string} html_container
     * @param {string} html_element
     * @return {string}
     */

    var append = function append(html_container, html_element) {
      var pos = html_container.search(/(<\/[^>]+>\s*)$/);
      return html_container.substring(0, pos) + html_element + html_container.substring(pos);
    };

    this.setup = function () {
      var original = self.setup;
      return function () {
        // override the item rendering method to add the button to each
        if (options.append) {
          var render_item = self.settings.render.item;

          self.settings.render.item = function (data) {
            return append(render_item.apply(this, arguments), html);
          };
        }

        original.apply(this, arguments); // add event listener

        this.$control.on('click', '.' + options.className, function (e) {
          e.preventDefault();
          if (self.isLocked) return;
          var $item = $(e.currentTarget).parent();
          self.setActiveItem($item);

          if (self.deleteSelection()) {
            self.setCaret(self.items.length);
          }
        });
      };
    }();
  });
  Selectize.define('restore_on_backspace', function (options) {
    var self = this;

    options.text = options.text || function (option) {
      return option[this.settings.labelField];
    };

    this.onKeyDown = function () {
      var original = self.onKeyDown;
      return function (e) {
        var index, option;

        if (e.keyCode === KEY_BACKSPACE && this.$control_input.val() === '' && !this.$activeItems.length) {
          index = this.caretPos - 1;

          if (index >= 0 && index < this.items.length) {
            option = this.options[this.items[index]];

            if (this.deleteSelection(e)) {
              this.setTextboxValue(options.text.apply(this, [option]));
              this.refreshOptions(true);
            }

            e.preventDefault();
            return;
          }
        }

        return original.apply(this, arguments);
      };
    }();
  });
  return Selectize;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvTGlicmFyeS9TZWxlY3RpemUuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2xpYnJhcnkvc2VsZWN0aXplL2Rpc3QvY3NzL3NlbGVjdGl6ZS5ib290c3RyYXA0LmNzcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvbGlicmFyeS9zZWxlY3RpemUvZGlzdC9qcy9zdGFuZGFsb25lL3NlbGVjdGl6ZS5qcyJdLCJuYW1lcyI6WyJyb290IiwiZmFjdG9yeSIsImRlZmluZSIsIlNpZnRlciIsIml0ZW1zIiwic2V0dGluZ3MiLCJkaWFjcml0aWNzIiwicHJvdG90eXBlIiwidG9rZW5pemUiLCJxdWVyeSIsInRyaW0iLCJTdHJpbmciLCJ0b0xvd2VyQ2FzZSIsImxlbmd0aCIsImkiLCJuIiwicmVnZXgiLCJsZXR0ZXIiLCJ0b2tlbnMiLCJ3b3JkcyIsInNwbGl0IiwiZXNjYXBlX3JlZ2V4IiwiRElBQ1JJVElDUyIsImhhc093blByb3BlcnR5IiwicmVwbGFjZSIsIlJlZ0V4cCIsInB1c2giLCJzdHJpbmciLCJpdGVyYXRvciIsIm9iamVjdCIsImNhbGxiYWNrIiwiaXNfYXJyYXkiLCJBcnJheSIsImZvckVhY2giLCJrZXkiLCJhcHBseSIsImdldFNjb3JlRnVuY3Rpb24iLCJzZWFyY2giLCJvcHRpb25zIiwic2VsZiIsImZpZWxkcyIsInRva2VuX2NvdW50IiwicHJlcGFyZVNlYXJjaCIsInNjb3JlVmFsdWUiLCJ2YWx1ZSIsInRva2VuIiwic2NvcmUiLCJwb3MiLCJzY29yZU9iamVjdCIsImZpZWxkX2NvdW50IiwiZGF0YSIsInN1bSIsImNvbmp1bmN0aW9uIiwiZ2V0U29ydEZ1bmN0aW9uIiwiZmllbGQiLCJmaWVsZHNfY291bnQiLCJtdWx0aXBsaWVyIiwibXVsdGlwbGllcnMiLCJnZXRfZmllbGQiLCJpbXBsaWNpdF9zY29yZSIsInNvcnQiLCJzb3J0X2VtcHR5IiwibmFtZSIsInJlc3VsdCIsImlkIiwidW5zaGlmdCIsImRpcmVjdGlvbiIsInNwbGljZSIsImEiLCJiIiwiY21wIiwiYV92YWx1ZSIsImJfdmFsdWUiLCJleHRlbmQiLCJvcHRpb25fZmllbGRzIiwib3B0aW9uX3NvcnQiLCJvcHRpb25fc29ydF9lbXB0eSIsInRvdGFsIiwiY2FsY3VsYXRlU2NvcmUiLCJmbl9zb3J0IiwiZm5fc2NvcmUiLCJpdGVtIiwiZmlsdGVyIiwibGltaXQiLCJzbGljZSIsImFzY2lpZm9sZCIsImsiLCJhcmd1bWVudHMiLCJzdHIiLCJpc0FycmF5IiwiJCIsIk9iamVjdCIsInRvU3RyaW5nIiwiY2FsbCIsImNodW5rIiwiZm9yZWlnbmxldHRlcnMiLCJsb29rdXAiLCJzdWJzdHJpbmciLCJjaGFyQXQiLCJyZWdleHAiLCJmb3JlaWdubGV0dGVyIiwiTWljcm9QbHVnaW4iLCJtaXhpbiIsIkludGVyZmFjZSIsInBsdWdpbnMiLCJpbml0aWFsaXplUGx1Z2lucyIsInF1ZXVlIiwibmFtZXMiLCJyZXF1ZXN0ZWQiLCJsb2FkZWQiLCJ1dGlscyIsInJlcXVpcmUiLCJzaGlmdCIsImxvYWRQbHVnaW4iLCJwbHVnaW4iLCJFcnJvciIsImZuIiwidkFyZyIsImhpZ2hsaWdodCIsIiRlbGVtZW50IiwicGF0dGVybiIsIm5vZGUiLCJza2lwIiwibm9kZVR5cGUiLCJtYXRjaCIsInNwYW5ub2RlIiwiZG9jdW1lbnQiLCJjcmVhdGVFbGVtZW50IiwiY2xhc3NOYW1lIiwibWlkZGxlYml0Iiwic3BsaXRUZXh0IiwiZW5kYml0IiwibWlkZGxlY2xvbmUiLCJjbG9uZU5vZGUiLCJhcHBlbmRDaGlsZCIsInBhcmVudE5vZGUiLCJyZXBsYWNlQ2hpbGQiLCJjaGlsZE5vZGVzIiwidGVzdCIsInRhZ05hbWUiLCJlYWNoIiwiTWljcm9FdmVudCIsIm9uIiwiZXZlbnQiLCJmY3QiLCJfZXZlbnRzIiwib2ZmIiwiaW5kZXhPZiIsInRyaWdnZXIiLCJkZXN0T2JqZWN0IiwicHJvcHMiLCJJU19NQUMiLCJuYXZpZ2F0b3IiLCJ1c2VyQWdlbnQiLCJLRVlfQSIsIktFWV9DT01NQSIsIktFWV9SRVRVUk4iLCJLRVlfRVNDIiwiS0VZX0xFRlQiLCJLRVlfVVAiLCJLRVlfUCIsIktFWV9SSUdIVCIsIktFWV9ET1dOIiwiS0VZX04iLCJLRVlfQkFDS1NQQUNFIiwiS0VZX0RFTEVURSIsIktFWV9TSElGVCIsIktFWV9DTUQiLCJLRVlfQ1RSTCIsIktFWV9UQUIiLCJUQUdfU0VMRUNUIiwiVEFHX0lOUFVUIiwiU1VQUE9SVFNfVkFMSURJVFlfQVBJIiwid2luZG93IiwidmFsaWRpdHkiLCJpc3NldCIsImhhc2hfa2V5IiwiZXNjYXBlX2h0bWwiLCJlc2NhcGVfcmVwbGFjZSIsImhvb2siLCJiZWZvcmUiLCJtZXRob2QiLCJvcmlnaW5hbCIsImFmdGVyIiwib25jZSIsImNhbGxlZCIsImRlYm91bmNlIiwiZGVsYXkiLCJ0aW1lb3V0IiwiYXJncyIsImNsZWFyVGltZW91dCIsInNldFRpbWVvdXQiLCJkZWJvdW5jZV9ldmVudHMiLCJ0eXBlcyIsInR5cGUiLCJldmVudF9hcmdzIiwid2F0Y2hDaGlsZEV2ZW50IiwiJHBhcmVudCIsInNlbGVjdG9yIiwiZSIsImNoaWxkIiwidGFyZ2V0IiwiY3VycmVudFRhcmdldCIsImdldFNlbGVjdGlvbiIsImlucHV0Iiwic3RhcnQiLCJzZWxlY3Rpb25TdGFydCIsInNlbGVjdGlvbkVuZCIsInNlbGVjdGlvbiIsImZvY3VzIiwic2VsIiwiY3JlYXRlUmFuZ2UiLCJzZWxMZW4iLCJ0ZXh0IiwibW92ZVN0YXJ0IiwidHJhbnNmZXJTdHlsZXMiLCIkZnJvbSIsIiR0byIsInByb3BlcnRpZXMiLCJzdHlsZXMiLCJjc3MiLCJtZWFzdXJlU3RyaW5nIiwiJHRlc3QiLCJwb3NpdGlvbiIsInRvcCIsImxlZnQiLCJ3aWR0aCIsInBhZGRpbmciLCJ3aGl0ZVNwYWNlIiwiYXBwZW5kVG8iLCJyZW1vdmUiLCJhdXRvR3JvdyIsIiRpbnB1dCIsImN1cnJlbnRXaWR0aCIsInVwZGF0ZSIsImtleUNvZGUiLCJwcmludGFibGUiLCJwbGFjZWhvbGRlciIsImNoYXJhY3RlciIsIm1ldGFLZXkiLCJhbHRLZXkiLCJmb3JjZSIsInZhbCIsInNoaWZ0S2V5IiwiZnJvbUNoYXJDb2RlIiwidG9VcHBlckNhc2UiLCJhdHRyIiwidHJpZ2dlckhhbmRsZXIiLCJTZWxlY3RpemUiLCJkaXIiLCJzZWxlY3RpemUiLCJjb21wdXRlZFN0eWxlIiwiZ2V0Q29tcHV0ZWRTdHlsZSIsImdldFByb3BlcnR5VmFsdWUiLCJjdXJyZW50U3R5bGUiLCJwYXJlbnRzIiwib3JkZXIiLCJ0YWJJbmRleCIsInRhZ1R5cGUiLCJydGwiLCJldmVudE5TIiwiY291bnQiLCJoaWdobGlnaHRlZFZhbHVlIiwiaXNPcGVuIiwiaXNEaXNhYmxlZCIsImlzUmVxdWlyZWQiLCJpcyIsImlzSW52YWxpZCIsImlzTG9ja2VkIiwiaXNGb2N1c2VkIiwiaXNJbnB1dEhpZGRlbiIsImlzU2V0dXAiLCJpc1NoaWZ0RG93biIsImlzQ21kRG93biIsImlzQ3RybERvd24iLCJpZ25vcmVGb2N1cyIsImlnbm9yZUJsdXIiLCJpZ25vcmVIb3ZlciIsImhhc09wdGlvbnMiLCJjdXJyZW50UmVzdWx0cyIsImxhc3RWYWx1ZSIsImNhcmV0UG9zIiwibG9hZGluZyIsImxvYWRlZFNlYXJjaGVzIiwiJGFjdGl2ZU9wdGlvbiIsIiRhY3RpdmVJdGVtcyIsIm9wdGdyb3VwcyIsInVzZXJPcHRpb25zIiwicmVuZGVyQ2FjaGUiLCJvblNlYXJjaENoYW5nZSIsImxvYWRUaHJvdHRsZSIsInNpZnRlciIsInJlZ2lzdGVyT3B0aW9uIiwicmVnaXN0ZXJPcHRpb25Hcm91cCIsIm1vZGUiLCJtYXhJdGVtcyIsImhpZGVTZWxlY3RlZCIsInNldHVwQ2FsbGJhY2tzIiwic2V0dXBUZW1wbGF0ZXMiLCJzZXR1cCIsIiR3aW5kb3ciLCIkZG9jdW1lbnQiLCIkd3JhcHBlciIsIiRjb250cm9sIiwiJGNvbnRyb2xfaW5wdXQiLCIkZHJvcGRvd24iLCIkZHJvcGRvd25fY29udGVudCIsIiRkcm9wZG93bl9wYXJlbnQiLCJpbnB1dE1vZGUiLCJ0aW1lb3V0X2JsdXIiLCJ0aW1lb3V0X2ZvY3VzIiwiY2xhc3NlcyIsImNsYXNzZXNfcGx1Z2lucyIsImFkZENsYXNzIiwid3JhcHBlckNsYXNzIiwiaW5wdXRDbGFzcyIsImRyb3Bkb3duUGFyZW50IiwiZHJvcGRvd25DbGFzcyIsImhpZGUiLCJkcm9wZG93bkNvbnRlbnRDbGFzcyIsImNvcHlDbGFzc2VzVG9Ecm9wZG93biIsInN0eWxlIiwiam9pbiIsInNwbGl0T24iLCJkZWxpbWl0ZXIiLCJkZWxpbWl0ZXJFc2NhcGVkIiwib25PcHRpb25Ib3ZlciIsIm9uT3B0aW9uU2VsZWN0Iiwib25JdGVtU2VsZWN0IiwibW91c2Vkb3duIiwib25Nb3VzZURvd24iLCJjbGljayIsIm9uQ2xpY2siLCJzdG9wUHJvcGFnYXRpb24iLCJrZXlkb3duIiwib25LZXlEb3duIiwia2V5dXAiLCJvbktleVVwIiwia2V5cHJlc3MiLCJvbktleVByZXNzIiwicmVzaXplIiwicG9zaXRpb25Ecm9wZG93biIsImJsdXIiLCJvbkJsdXIiLCJvbkZvY3VzIiwicGFzdGUiLCJvblBhc3RlIiwiaGFzIiwicmV2ZXJ0U2V0dGluZ3MiLCIkY2hpbGRyZW4iLCJjaGlsZHJlbiIsImRldGFjaCIsInRhYmluZGV4Iiwic2V0VmFsdWUiLCJwcmV2ZW50RGVmYXVsdCIsInJlZnJlc2hTdGF0ZSIsInVwZGF0ZU9yaWdpbmFsSW5wdXQiLCJyZWZyZXNoSXRlbXMiLCJ1cGRhdGVQbGFjZWhvbGRlciIsImRpc2FibGUiLCJvbkNoYW5nZSIsInByZWxvYWQiLCJmaWVsZF9sYWJlbCIsImxhYmVsRmllbGQiLCJmaWVsZF9vcHRncm91cCIsIm9wdGdyb3VwTGFiZWxGaWVsZCIsInRlbXBsYXRlcyIsImh0bWwiLCJlc2NhcGUiLCJyZW5kZXIiLCJjYWxsYmFja3MiLCJkZWZhdWx0UHJldmVudGVkIiwiaXNEZWZhdWx0UHJldmVudGVkIiwiJHRhcmdldCIsImNsb3NlIiwib3BlbiIsInNldEFjdGl2ZUl0ZW0iLCJpc0Z1bGwiLCJzcGxpdElucHV0IiwiY3JlYXRlSXRlbSIsIndoaWNoIiwiY3JlYXRlIiwiaXNJbnB1dCIsInNlbGVjdEFsbCIsImN0cmxLZXkiLCIkbmV4dCIsImdldEFkamFjZW50T3B0aW9uIiwic2V0QWN0aXZlT3B0aW9uIiwiJHByZXYiLCJhZHZhbmNlU2VsZWN0aW9uIiwic2VsZWN0T25UYWIiLCJkZWxldGVTZWxlY3Rpb24iLCJyZWZyZXNoT3B0aW9ucyIsImxvYWQiLCJ3YXNGb2N1c2VkIiwic2hvd0lucHV0Iiwib3Blbk9uRm9jdXMiLCJkZXN0IiwiYWN0aXZlRWxlbWVudCIsImRlYWN0aXZhdGUiLCJzZXRUZXh0Ym94VmFsdWUiLCJzZXRDYXJldCIsImJvZHkiLCJjcmVhdGVPbkJsdXIiLCIkb3B0aW9uIiwiaGFzQ2xhc3MiLCJjbG9zZUFmdGVyU2VsZWN0IiwibGFzdFF1ZXJ5IiwiYWRkSXRlbSIsImdldE9wdGlvbiIsImxvYWRpbmdDbGFzcyIsInJlc3VsdHMiLCJNYXRoIiwibWF4IiwiYWRkT3B0aW9uIiwicmVtb3ZlQ2xhc3MiLCJjaGFuZ2VkIiwiZ2V0VmFsdWUiLCJzaWxlbnQiLCJldmVudHMiLCJjbGVhciIsImFkZEl0ZW1zIiwiJGl0ZW0iLCJldmVudE5hbWUiLCJpZHgiLCJiZWdpbiIsImVuZCIsInN3YXAiLCIkbGFzdCIsImhpZGVJbnB1dCIsInNjcm9sbCIsImFuaW1hdGUiLCJoZWlnaHRfbWVudSIsImhlaWdodF9pdGVtIiwieSIsInNjcm9sbF90b3AiLCJzY3JvbGxfYm90dG9tIiwiaGVpZ2h0Iiwib3V0ZXJIZWlnaHQiLCJzY3JvbGxUb3AiLCJvZmZzZXQiLCJzdG9wIiwic2Nyb2xsRHVyYXRpb24iLCJvcGFjaXR5IiwiZ2V0U2VhcmNoT3B0aW9ucyIsInNvcnRGaWVsZCIsInNlYXJjaEZpZWxkIiwic2VhcmNoQ29uanVuY3Rpb24iLCJ0cmlnZ2VyRHJvcGRvd24iLCJqIiwiZ3JvdXBzIiwiZ3JvdXBzX29yZGVyIiwib3B0aW9uIiwib3B0aW9uX2h0bWwiLCJvcHRncm91cCIsImh0bWxfY2hpbGRyZW4iLCJoYXNfY3JlYXRlX29wdGlvbiIsIiRhY3RpdmUiLCIkYWN0aXZlX2JlZm9yZSIsIiRjcmVhdGUiLCJhY3RpdmVfYmVmb3JlIiwibWF4T3B0aW9ucyIsIm1pbiIsIm9wdGdyb3VwRmllbGQiLCJsb2NrT3B0Z3JvdXBPcmRlciIsImFfb3JkZXIiLCIkb3JkZXIiLCJiX29yZGVyIiwiY2FuQ3JlYXRlIiwicHJlcGVuZCIsImFkZFByZWNlZGVuY2UiLCJmaW5kIiwidmFsdWVGaWVsZCIsIm9wdGdyb3VwVmFsdWVGaWVsZCIsImFkZE9wdGlvbkdyb3VwIiwicmVtb3ZlT3B0aW9uR3JvdXAiLCJjbGVhck9wdGlvbkdyb3VwcyIsInVwZGF0ZU9wdGlvbiIsIiRpdGVtX25ldyIsInZhbHVlX25ldyIsImluZGV4X2l0ZW0iLCJjYWNoZV9pdGVtcyIsImNhY2hlX29wdGlvbnMiLCJvcmRlcl9vbGQiLCJnZXRJdGVtIiwicmVwbGFjZVdpdGgiLCJyZW1vdmVPcHRpb24iLCJyZW1vdmVJdGVtIiwiY2xlYXJPcHRpb25zIiwiZ2V0RWxlbWVudFdpdGhWYWx1ZSIsIiRvcHRpb25zIiwiaW5kZXgiLCJlcSIsIiRlbHMiLCJnZXRBdHRyaWJ1dGUiLCJ2YWx1ZXMiLCJpc1BlbmRpbmciLCJhY3RpdmUiLCJ2YWx1ZV9uZXh0Iiwid2FzRnVsbCIsImluc2VydEF0Q2FyZXQiLCJwZXJzaXN0IiwiY2FyZXQiLCJsb2NrIiwidW5sb2NrIiwib3V0cHV0IiwiaW52YWxpZCIsInByb3AiLCJyZWZyZXNoQ2xhc3NlcyIsInRvZ2dsZUNsYXNzIiwiaXNFbXB0eU9iamVjdCIsIm9wdHMiLCJsYWJlbCIsInJlbW92ZUF0dHIiLCJ2aXNpYmlsaXR5IiwiZGlzcGxheSIsIm91dGVyV2lkdGgiLCIkZWwiLCJvcHRpb25fc2VsZWN0IiwiJG9wdGlvbl9zZWxlY3QiLCIkdGFpbCIsIm9uRGVsZXRlIiwicG9wIiwidGFpbCIsInZhbHVlTGVuZ3RoIiwiY3Vyc29yQXRFZGdlIiwiYWR2YW5jZUNhcmV0IiwiJGFkaiIsIiRjaGlsZCIsImFwcGVuZCIsImVuYWJsZSIsImRlc3Ryb3kiLCJzaG93IiwicmVtb3ZlRGF0YSIsInRlbXBsYXRlTmFtZSIsImNhY2hlIiwicmVnZXhfdGFnIiwiY2xlYXJDYWNoZSIsImNyZWF0ZUZpbHRlciIsImRlZmF1bHRzIiwiYWxsb3dFbXB0eU9wdGlvbiIsImRhdGFBdHRyIiwic2V0dGluZ3NfdXNlciIsImF0dHJfZGF0YSIsImZpZWxkX3ZhbHVlIiwiZmllbGRfb3B0Z3JvdXBfbGFiZWwiLCJmaWVsZF9vcHRncm91cF92YWx1ZSIsImluaXRfdGV4dGJveCIsInNldHRpbmdzX2VsZW1lbnQiLCJkYXRhX3JhdyIsIkpTT04iLCJwYXJzZSIsImluaXRfc2VsZWN0Iiwib3B0aW9uc01hcCIsInJlYWREYXRhIiwiZ3JvdXAiLCJhcnIiLCJhZGRHcm91cCIsIiRvcHRncm91cCIsImluc3RhbmNlIiwidGFnX25hbWUiLCJzdXBwb3J0Iiwic29ydGFibGUiLCJmb3JjZVBsYWNlaG9sZGVyU2l6ZSIsImRpc2FibGVkIiwidWkiLCJoZWxwZXIiLCJvdmVyZmxvdyIsInRpdGxlIiwiaGVhZGVyQ2xhc3MiLCJ0aXRsZVJvd0NsYXNzIiwibGFiZWxDbGFzcyIsImNsb3NlQ2xhc3MiLCIkZHJvcGRvd25faGVhZGVyIiwiZXF1YWxpemVXaWR0aCIsImVxdWFsaXplSGVpZ2h0IiwiY2xvc2VzdCIsInByZXYiLCJuZXh0IiwiZ2V0U2Nyb2xsYmFyV2lkdGgiLCJkaXYiLCJkb2MiLCJpbm5lckhUTUwiLCJmaXJzdENoaWxkIiwib2Zmc2V0V2lkdGgiLCJjbGllbnRXaWR0aCIsInJlbW92ZUNoaWxkIiwiZXF1YWxpemVTaXplcyIsImhlaWdodF9tYXgiLCJ3aWR0aF9sYXN0Iiwid2lkdGhfcGFyZW50IiwiJG9wdGdyb3VwcyIsImlubmVyV2lkdGgiLCJyb3VuZCIsImh0bWxfY29udGFpbmVyIiwiaHRtbF9lbGVtZW50IiwicmVuZGVyX2l0ZW0iLCJwYXJlbnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUFjYTs7QUFFYjs7Ozs7Ozs7Ozs7O0FDaEJBLHVDOzs7Ozs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7Ozs7OztBQWdCQyxXQUFTQSxJQUFULEVBQWVDLE9BQWYsRUFBd0I7QUFDeEIsTUFBSSxJQUFKLEVBQWdEO0FBQy9DQywyQ0FBaUJELE9BQVgsOGlCQUFOO0FBQ0EsR0FGRCxNQUVPLEVBSU47QUFDRCxDQVJBLEVBUUMsSUFSRCxFQVFPLFlBQVc7QUFFbEI7Ozs7Ozs7OztBQVNBLE1BQUlFLE1BQU0sR0FBRyxTQUFUQSxNQUFTLENBQVNDLEtBQVQsRUFBZ0JDLFFBQWhCLEVBQTBCO0FBQ3RDLFNBQUtELEtBQUwsR0FBYUEsS0FBYjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0JBLFFBQVEsSUFBSTtBQUFDQyxnQkFBVSxFQUFFO0FBQWIsS0FBNUI7QUFDQSxHQUhEO0FBS0E7Ozs7Ozs7OztBQU9BSCxRQUFNLENBQUNJLFNBQVAsQ0FBaUJDLFFBQWpCLEdBQTRCLFVBQVNDLEtBQVQsRUFBZ0I7QUFDM0NBLFNBQUssR0FBR0MsSUFBSSxDQUFDQyxNQUFNLENBQUNGLEtBQUssSUFBSSxFQUFWLENBQU4sQ0FBb0JHLFdBQXBCLEVBQUQsQ0FBWjtBQUNBLFFBQUksQ0FBQ0gsS0FBRCxJQUFVLENBQUNBLEtBQUssQ0FBQ0ksTUFBckIsRUFBNkIsT0FBTyxFQUFQO0FBRTdCLFFBQUlDLENBQUosRUFBT0MsQ0FBUCxFQUFVQyxLQUFWLEVBQWlCQyxNQUFqQjtBQUNBLFFBQUlDLE1BQU0sR0FBRyxFQUFiO0FBQ0EsUUFBSUMsS0FBSyxHQUFHVixLQUFLLENBQUNXLEtBQU4sQ0FBWSxJQUFaLENBQVo7O0FBRUEsU0FBS04sQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxHQUFHSSxLQUFLLENBQUNOLE1BQXRCLEVBQThCQyxDQUFDLEdBQUdDLENBQWxDLEVBQXFDRCxDQUFDLEVBQXRDLEVBQTBDO0FBQ3pDRSxXQUFLLEdBQUdLLFlBQVksQ0FBQ0YsS0FBSyxDQUFDTCxDQUFELENBQU4sQ0FBcEI7O0FBQ0EsVUFBSSxLQUFLVCxRQUFMLENBQWNDLFVBQWxCLEVBQThCO0FBQzdCLGFBQUtXLE1BQUwsSUFBZUssVUFBZixFQUEyQjtBQUMxQixjQUFJQSxVQUFVLENBQUNDLGNBQVgsQ0FBMEJOLE1BQTFCLENBQUosRUFBdUM7QUFDdENELGlCQUFLLEdBQUdBLEtBQUssQ0FBQ1EsT0FBTixDQUFjLElBQUlDLE1BQUosQ0FBV1IsTUFBWCxFQUFtQixHQUFuQixDQUFkLEVBQXVDSyxVQUFVLENBQUNMLE1BQUQsQ0FBakQsQ0FBUjtBQUNBO0FBQ0Q7QUFDRDs7QUFDREMsWUFBTSxDQUFDUSxJQUFQLENBQVk7QUFDWEMsY0FBTSxFQUFHUixLQUFLLENBQUNMLENBQUQsQ0FESDtBQUVYRSxhQUFLLEVBQUksSUFBSVMsTUFBSixDQUFXVCxLQUFYLEVBQWtCLEdBQWxCO0FBRkUsT0FBWjtBQUlBOztBQUVELFdBQU9FLE1BQVA7QUFDQSxHQXhCRDtBQTBCQTs7Ozs7Ozs7Ozs7OztBQVdBZixRQUFNLENBQUNJLFNBQVAsQ0FBaUJxQixRQUFqQixHQUE0QixVQUFTQyxNQUFULEVBQWlCQyxRQUFqQixFQUEyQjtBQUN0RCxRQUFJRixRQUFKOztBQUNBLFFBQUlHLFFBQVEsQ0FBQ0YsTUFBRCxDQUFaLEVBQXNCO0FBQ3JCRCxjQUFRLEdBQUdJLEtBQUssQ0FBQ3pCLFNBQU4sQ0FBZ0IwQixPQUFoQixJQUEyQixVQUFTSCxRQUFULEVBQW1CO0FBQ3hELGFBQUssSUFBSWhCLENBQUMsR0FBRyxDQUFSLEVBQVdDLENBQUMsR0FBRyxLQUFLRixNQUF6QixFQUFpQ0MsQ0FBQyxHQUFHQyxDQUFyQyxFQUF3Q0QsQ0FBQyxFQUF6QyxFQUE2QztBQUM1Q2dCLGtCQUFRLENBQUMsS0FBS2hCLENBQUwsQ0FBRCxFQUFVQSxDQUFWLEVBQWEsSUFBYixDQUFSO0FBQ0E7QUFDRCxPQUpEO0FBS0EsS0FORCxNQU1PO0FBQ05jLGNBQVEsR0FBRyxrQkFBU0UsUUFBVCxFQUFtQjtBQUM3QixhQUFLLElBQUlJLEdBQVQsSUFBZ0IsSUFBaEIsRUFBc0I7QUFDckIsY0FBSSxLQUFLWCxjQUFMLENBQW9CVyxHQUFwQixDQUFKLEVBQThCO0FBQzdCSixvQkFBUSxDQUFDLEtBQUtJLEdBQUwsQ0FBRCxFQUFZQSxHQUFaLEVBQWlCLElBQWpCLENBQVI7QUFDQTtBQUNEO0FBQ0QsT0FORDtBQU9BOztBQUVETixZQUFRLENBQUNPLEtBQVQsQ0FBZU4sTUFBZixFQUF1QixDQUFDQyxRQUFELENBQXZCO0FBQ0EsR0FuQkQ7QUFxQkE7Ozs7Ozs7Ozs7OztBQVVBM0IsUUFBTSxDQUFDSSxTQUFQLENBQWlCNkIsZ0JBQWpCLEdBQW9DLFVBQVNDLE1BQVQsRUFBaUJDLE9BQWpCLEVBQTBCO0FBQzdELFFBQUlDLElBQUosRUFBVUMsTUFBVixFQUFrQnRCLE1BQWxCLEVBQTBCdUIsV0FBMUI7QUFFQUYsUUFBSSxHQUFVLElBQWQ7QUFDQUYsVUFBTSxHQUFRRSxJQUFJLENBQUNHLGFBQUwsQ0FBbUJMLE1BQW5CLEVBQTJCQyxPQUEzQixDQUFkO0FBQ0FwQixVQUFNLEdBQVFtQixNQUFNLENBQUNuQixNQUFyQjtBQUNBc0IsVUFBTSxHQUFRSCxNQUFNLENBQUNDLE9BQVAsQ0FBZUUsTUFBN0I7QUFDQUMsZUFBVyxHQUFHdkIsTUFBTSxDQUFDTCxNQUFyQjtBQUVBOzs7Ozs7Ozs7QUFRQSxRQUFJOEIsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBU0MsS0FBVCxFQUFnQkMsS0FBaEIsRUFBdUI7QUFDdkMsVUFBSUMsS0FBSixFQUFXQyxHQUFYO0FBRUEsVUFBSSxDQUFDSCxLQUFMLEVBQVksT0FBTyxDQUFQO0FBQ1pBLFdBQUssR0FBR2pDLE1BQU0sQ0FBQ2lDLEtBQUssSUFBSSxFQUFWLENBQWQ7QUFDQUcsU0FBRyxHQUFHSCxLQUFLLENBQUNQLE1BQU4sQ0FBYVEsS0FBSyxDQUFDN0IsS0FBbkIsQ0FBTjtBQUNBLFVBQUkrQixHQUFHLEtBQUssQ0FBQyxDQUFiLEVBQWdCLE9BQU8sQ0FBUDtBQUNoQkQsV0FBSyxHQUFHRCxLQUFLLENBQUNsQixNQUFOLENBQWFkLE1BQWIsR0FBc0IrQixLQUFLLENBQUMvQixNQUFwQztBQUNBLFVBQUlrQyxHQUFHLEtBQUssQ0FBWixFQUFlRCxLQUFLLElBQUksR0FBVDtBQUNmLGFBQU9BLEtBQVA7QUFDQSxLQVZEO0FBWUE7Ozs7Ozs7Ozs7QUFRQSxRQUFJRSxXQUFXLEdBQUksWUFBVztBQUM3QixVQUFJQyxXQUFXLEdBQUdULE1BQU0sQ0FBQzNCLE1BQXpCOztBQUNBLFVBQUksQ0FBQ29DLFdBQUwsRUFBa0I7QUFDakIsZUFBTyxZQUFXO0FBQUUsaUJBQU8sQ0FBUDtBQUFXLFNBQS9CO0FBQ0E7O0FBQ0QsVUFBSUEsV0FBVyxLQUFLLENBQXBCLEVBQXVCO0FBQ3RCLGVBQU8sVUFBU0osS0FBVCxFQUFnQkssSUFBaEIsRUFBc0I7QUFDNUIsaUJBQU9QLFVBQVUsQ0FBQ08sSUFBSSxDQUFDVixNQUFNLENBQUMsQ0FBRCxDQUFQLENBQUwsRUFBa0JLLEtBQWxCLENBQWpCO0FBQ0EsU0FGRDtBQUdBOztBQUNELGFBQU8sVUFBU0EsS0FBVCxFQUFnQkssSUFBaEIsRUFBc0I7QUFDNUIsYUFBSyxJQUFJcEMsQ0FBQyxHQUFHLENBQVIsRUFBV3FDLEdBQUcsR0FBRyxDQUF0QixFQUF5QnJDLENBQUMsR0FBR21DLFdBQTdCLEVBQTBDbkMsQ0FBQyxFQUEzQyxFQUErQztBQUM5Q3FDLGFBQUcsSUFBSVIsVUFBVSxDQUFDTyxJQUFJLENBQUNWLE1BQU0sQ0FBQzFCLENBQUQsQ0FBUCxDQUFMLEVBQWtCK0IsS0FBbEIsQ0FBakI7QUFDQTs7QUFDRCxlQUFPTSxHQUFHLEdBQUdGLFdBQWI7QUFDQSxPQUxEO0FBTUEsS0FoQmlCLEVBQWxCOztBQWtCQSxRQUFJLENBQUNSLFdBQUwsRUFBa0I7QUFDakIsYUFBTyxZQUFXO0FBQUUsZUFBTyxDQUFQO0FBQVcsT0FBL0I7QUFDQTs7QUFDRCxRQUFJQSxXQUFXLEtBQUssQ0FBcEIsRUFBdUI7QUFDdEIsYUFBTyxVQUFTUyxJQUFULEVBQWU7QUFDckIsZUFBT0YsV0FBVyxDQUFDOUIsTUFBTSxDQUFDLENBQUQsQ0FBUCxFQUFZZ0MsSUFBWixDQUFsQjtBQUNBLE9BRkQ7QUFHQTs7QUFFRCxRQUFJYixNQUFNLENBQUNDLE9BQVAsQ0FBZWMsV0FBZixLQUErQixLQUFuQyxFQUEwQztBQUN6QyxhQUFPLFVBQVNGLElBQVQsRUFBZTtBQUNyQixZQUFJSixLQUFKOztBQUNBLGFBQUssSUFBSWhDLENBQUMsR0FBRyxDQUFSLEVBQVdxQyxHQUFHLEdBQUcsQ0FBdEIsRUFBeUJyQyxDQUFDLEdBQUcyQixXQUE3QixFQUEwQzNCLENBQUMsRUFBM0MsRUFBK0M7QUFDOUNnQyxlQUFLLEdBQUdFLFdBQVcsQ0FBQzlCLE1BQU0sQ0FBQ0osQ0FBRCxDQUFQLEVBQVlvQyxJQUFaLENBQW5CO0FBQ0EsY0FBSUosS0FBSyxJQUFJLENBQWIsRUFBZ0IsT0FBTyxDQUFQO0FBQ2hCSyxhQUFHLElBQUlMLEtBQVA7QUFDQTs7QUFDRCxlQUFPSyxHQUFHLEdBQUdWLFdBQWI7QUFDQSxPQVJEO0FBU0EsS0FWRCxNQVVPO0FBQ04sYUFBTyxVQUFTUyxJQUFULEVBQWU7QUFDckIsYUFBSyxJQUFJcEMsQ0FBQyxHQUFHLENBQVIsRUFBV3FDLEdBQUcsR0FBRyxDQUF0QixFQUF5QnJDLENBQUMsR0FBRzJCLFdBQTdCLEVBQTBDM0IsQ0FBQyxFQUEzQyxFQUErQztBQUM5Q3FDLGFBQUcsSUFBSUgsV0FBVyxDQUFDOUIsTUFBTSxDQUFDSixDQUFELENBQVAsRUFBWW9DLElBQVosQ0FBbEI7QUFDQTs7QUFDRCxlQUFPQyxHQUFHLEdBQUdWLFdBQWI7QUFDQSxPQUxEO0FBTUE7QUFDRCxHQWxGRDtBQW9GQTs7Ozs7Ozs7Ozs7QUFTQXRDLFFBQU0sQ0FBQ0ksU0FBUCxDQUFpQjhDLGVBQWpCLEdBQW1DLFVBQVNoQixNQUFULEVBQWlCQyxPQUFqQixFQUEwQjtBQUM1RCxRQUFJeEIsQ0FBSixFQUFPQyxDQUFQLEVBQVV3QixJQUFWLEVBQWdCZSxLQUFoQixFQUF1QmQsTUFBdkIsRUFBK0JlLFlBQS9CLEVBQTZDQyxVQUE3QyxFQUF5REMsV0FBekQsRUFBc0VDLFNBQXRFLEVBQWlGQyxjQUFqRixFQUFpR0MsSUFBakc7QUFFQXJCLFFBQUksR0FBSyxJQUFUO0FBQ0FGLFVBQU0sR0FBR0UsSUFBSSxDQUFDRyxhQUFMLENBQW1CTCxNQUFuQixFQUEyQkMsT0FBM0IsQ0FBVDtBQUNBc0IsUUFBSSxHQUFNLENBQUN2QixNQUFNLENBQUM1QixLQUFSLElBQWlCNkIsT0FBTyxDQUFDdUIsVUFBMUIsSUFBeUN2QixPQUFPLENBQUNzQixJQUExRDtBQUVBOzs7Ozs7Ozs7QUFRQUYsYUFBUyxHQUFHLG1CQUFTSSxJQUFULEVBQWVDLE1BQWYsRUFBdUI7QUFDbEMsVUFBSUQsSUFBSSxLQUFLLFFBQWIsRUFBdUIsT0FBT0MsTUFBTSxDQUFDakIsS0FBZDtBQUN2QixhQUFPUCxJQUFJLENBQUNuQyxLQUFMLENBQVcyRCxNQUFNLENBQUNDLEVBQWxCLEVBQXNCRixJQUF0QixDQUFQO0FBQ0EsS0FIRCxDQWY0RCxDQW9CNUQ7OztBQUNBdEIsVUFBTSxHQUFHLEVBQVQ7O0FBQ0EsUUFBSW9CLElBQUosRUFBVTtBQUNULFdBQUs5QyxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUc2QyxJQUFJLENBQUMvQyxNQUFyQixFQUE2QkMsQ0FBQyxHQUFHQyxDQUFqQyxFQUFvQ0QsQ0FBQyxFQUFyQyxFQUF5QztBQUN4QyxZQUFJdUIsTUFBTSxDQUFDNUIsS0FBUCxJQUFnQm1ELElBQUksQ0FBQzlDLENBQUQsQ0FBSixDQUFRd0MsS0FBUixLQUFrQixRQUF0QyxFQUFnRDtBQUMvQ2QsZ0JBQU0sQ0FBQ2QsSUFBUCxDQUFZa0MsSUFBSSxDQUFDOUMsQ0FBRCxDQUFoQjtBQUNBO0FBQ0Q7QUFDRCxLQTVCMkQsQ0E4QjVEO0FBQ0E7OztBQUNBLFFBQUl1QixNQUFNLENBQUM1QixLQUFYLEVBQWtCO0FBQ2pCa0Qsb0JBQWMsR0FBRyxJQUFqQjs7QUFDQSxXQUFLN0MsQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxHQUFHeUIsTUFBTSxDQUFDM0IsTUFBdkIsRUFBK0JDLENBQUMsR0FBR0MsQ0FBbkMsRUFBc0NELENBQUMsRUFBdkMsRUFBMkM7QUFDMUMsWUFBSTBCLE1BQU0sQ0FBQzFCLENBQUQsQ0FBTixDQUFVd0MsS0FBVixLQUFvQixRQUF4QixFQUFrQztBQUNqQ0ssd0JBQWMsR0FBRyxLQUFqQjtBQUNBO0FBQ0E7QUFDRDs7QUFDRCxVQUFJQSxjQUFKLEVBQW9CO0FBQ25CbkIsY0FBTSxDQUFDeUIsT0FBUCxDQUFlO0FBQUNYLGVBQUssRUFBRSxRQUFSO0FBQWtCWSxtQkFBUyxFQUFFO0FBQTdCLFNBQWY7QUFDQTtBQUNELEtBWEQsTUFXTztBQUNOLFdBQUtwRCxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUd5QixNQUFNLENBQUMzQixNQUF2QixFQUErQkMsQ0FBQyxHQUFHQyxDQUFuQyxFQUFzQ0QsQ0FBQyxFQUF2QyxFQUEyQztBQUMxQyxZQUFJMEIsTUFBTSxDQUFDMUIsQ0FBRCxDQUFOLENBQVV3QyxLQUFWLEtBQW9CLFFBQXhCLEVBQWtDO0FBQ2pDZCxnQkFBTSxDQUFDMkIsTUFBUCxDQUFjckQsQ0FBZCxFQUFpQixDQUFqQjtBQUNBO0FBQ0E7QUFDRDtBQUNEOztBQUVEMkMsZUFBVyxHQUFHLEVBQWQ7O0FBQ0EsU0FBSzNDLENBQUMsR0FBRyxDQUFKLEVBQU9DLENBQUMsR0FBR3lCLE1BQU0sQ0FBQzNCLE1BQXZCLEVBQStCQyxDQUFDLEdBQUdDLENBQW5DLEVBQXNDRCxDQUFDLEVBQXZDLEVBQTJDO0FBQzFDMkMsaUJBQVcsQ0FBQy9CLElBQVosQ0FBaUJjLE1BQU0sQ0FBQzFCLENBQUQsQ0FBTixDQUFVb0QsU0FBVixLQUF3QixNQUF4QixHQUFpQyxDQUFDLENBQWxDLEdBQXNDLENBQXZEO0FBQ0EsS0F2RDJELENBeUQ1RDs7O0FBQ0FYLGdCQUFZLEdBQUdmLE1BQU0sQ0FBQzNCLE1BQXRCOztBQUNBLFFBQUksQ0FBQzBDLFlBQUwsRUFBbUI7QUFDbEIsYUFBTyxJQUFQO0FBQ0EsS0FGRCxNQUVPLElBQUlBLFlBQVksS0FBSyxDQUFyQixFQUF3QjtBQUM5QkQsV0FBSyxHQUFHZCxNQUFNLENBQUMsQ0FBRCxDQUFOLENBQVVjLEtBQWxCO0FBQ0FFLGdCQUFVLEdBQUdDLFdBQVcsQ0FBQyxDQUFELENBQXhCO0FBQ0EsYUFBTyxVQUFTVyxDQUFULEVBQVlDLENBQVosRUFBZTtBQUNyQixlQUFPYixVQUFVLEdBQUdjLEdBQUcsQ0FDdEJaLFNBQVMsQ0FBQ0osS0FBRCxFQUFRYyxDQUFSLENBRGEsRUFFdEJWLFNBQVMsQ0FBQ0osS0FBRCxFQUFRZSxDQUFSLENBRmEsQ0FBdkI7QUFJQSxPQUxEO0FBTUEsS0FUTSxNQVNBO0FBQ04sYUFBTyxVQUFTRCxDQUFULEVBQVlDLENBQVosRUFBZTtBQUNyQixZQUFJdkQsQ0FBSixFQUFPaUQsTUFBUCxFQUFlUSxPQUFmLEVBQXdCQyxPQUF4QixFQUFpQ2xCLEtBQWpDOztBQUNBLGFBQUt4QyxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUd5QyxZQUFoQixFQUE4QnpDLENBQUMsRUFBL0IsRUFBbUM7QUFDbEN3QyxlQUFLLEdBQUdkLE1BQU0sQ0FBQzFCLENBQUQsQ0FBTixDQUFVd0MsS0FBbEI7QUFDQVMsZ0JBQU0sR0FBR04sV0FBVyxDQUFDM0MsQ0FBRCxDQUFYLEdBQWlCd0QsR0FBRyxDQUM1QlosU0FBUyxDQUFDSixLQUFELEVBQVFjLENBQVIsQ0FEbUIsRUFFNUJWLFNBQVMsQ0FBQ0osS0FBRCxFQUFRZSxDQUFSLENBRm1CLENBQTdCO0FBSUEsY0FBSU4sTUFBSixFQUFZLE9BQU9BLE1BQVA7QUFDWjs7QUFDRCxlQUFPLENBQVA7QUFDQSxPQVhEO0FBWUE7QUFDRCxHQXBGRDtBQXNGQTs7Ozs7Ozs7Ozs7QUFTQTVELFFBQU0sQ0FBQ0ksU0FBUCxDQUFpQm1DLGFBQWpCLEdBQWlDLFVBQVNqQyxLQUFULEVBQWdCNkIsT0FBaEIsRUFBeUI7QUFDekQsUUFBSSxRQUFPN0IsS0FBUCxNQUFpQixRQUFyQixFQUErQixPQUFPQSxLQUFQO0FBRS9CNkIsV0FBTyxHQUFHbUMsTUFBTSxDQUFDLEVBQUQsRUFBS25DLE9BQUwsQ0FBaEI7QUFFQSxRQUFJb0MsYUFBYSxHQUFPcEMsT0FBTyxDQUFDRSxNQUFoQztBQUNBLFFBQUltQyxXQUFXLEdBQVNyQyxPQUFPLENBQUNzQixJQUFoQztBQUNBLFFBQUlnQixpQkFBaUIsR0FBR3RDLE9BQU8sQ0FBQ3VCLFVBQWhDO0FBRUEsUUFBSWEsYUFBYSxJQUFJLENBQUMzQyxRQUFRLENBQUMyQyxhQUFELENBQTlCLEVBQStDcEMsT0FBTyxDQUFDRSxNQUFSLEdBQWlCLENBQUNrQyxhQUFELENBQWpCO0FBQy9DLFFBQUlDLFdBQVcsSUFBSSxDQUFDNUMsUUFBUSxDQUFDNEMsV0FBRCxDQUE1QixFQUEyQ3JDLE9BQU8sQ0FBQ3NCLElBQVIsR0FBZSxDQUFDZSxXQUFELENBQWY7QUFDM0MsUUFBSUMsaUJBQWlCLElBQUksQ0FBQzdDLFFBQVEsQ0FBQzZDLGlCQUFELENBQWxDLEVBQXVEdEMsT0FBTyxDQUFDdUIsVUFBUixHQUFxQixDQUFDZSxpQkFBRCxDQUFyQjtBQUV2RCxXQUFPO0FBQ050QyxhQUFPLEVBQUdBLE9BREo7QUFFTjdCLFdBQUssRUFBS0UsTUFBTSxDQUFDRixLQUFLLElBQUksRUFBVixDQUFOLENBQW9CRyxXQUFwQixFQUZKO0FBR05NLFlBQU0sRUFBSSxLQUFLVixRQUFMLENBQWNDLEtBQWQsQ0FISjtBQUlOb0UsV0FBSyxFQUFLLENBSko7QUFLTnpFLFdBQUssRUFBSztBQUxKLEtBQVA7QUFPQSxHQXBCRDtBQXNCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXVCQUQsUUFBTSxDQUFDSSxTQUFQLENBQWlCOEIsTUFBakIsR0FBMEIsVUFBUzVCLEtBQVQsRUFBZ0I2QixPQUFoQixFQUF5QjtBQUNsRCxRQUFJQyxJQUFJLEdBQUcsSUFBWDtBQUFBLFFBQWlCSyxLQUFqQjtBQUFBLFFBQXdCRSxLQUF4QjtBQUFBLFFBQStCVCxNQUEvQjtBQUFBLFFBQXVDeUMsY0FBdkM7QUFDQSxRQUFJQyxPQUFKO0FBQ0EsUUFBSUMsUUFBSjtBQUVBM0MsVUFBTSxHQUFJLEtBQUtLLGFBQUwsQ0FBbUJqQyxLQUFuQixFQUEwQjZCLE9BQTFCLENBQVY7QUFDQUEsV0FBTyxHQUFHRCxNQUFNLENBQUNDLE9BQWpCO0FBQ0E3QixTQUFLLEdBQUs0QixNQUFNLENBQUM1QixLQUFqQixDQVBrRCxDQVNsRDs7QUFDQXVFLFlBQVEsR0FBRzFDLE9BQU8sQ0FBQ1EsS0FBUixJQUFpQlAsSUFBSSxDQUFDSCxnQkFBTCxDQUFzQkMsTUFBdEIsQ0FBNUIsQ0FWa0QsQ0FZbEQ7O0FBQ0EsUUFBSTVCLEtBQUssQ0FBQ0ksTUFBVixFQUFrQjtBQUNqQjBCLFVBQUksQ0FBQ1gsUUFBTCxDQUFjVyxJQUFJLENBQUNuQyxLQUFuQixFQUEwQixVQUFTNkUsSUFBVCxFQUFlakIsRUFBZixFQUFtQjtBQUM1Q2xCLGFBQUssR0FBR2tDLFFBQVEsQ0FBQ0MsSUFBRCxDQUFoQjs7QUFDQSxZQUFJM0MsT0FBTyxDQUFDNEMsTUFBUixLQUFtQixLQUFuQixJQUE0QnBDLEtBQUssR0FBRyxDQUF4QyxFQUEyQztBQUMxQ1QsZ0JBQU0sQ0FBQ2pDLEtBQVAsQ0FBYXNCLElBQWIsQ0FBa0I7QUFBQyxxQkFBU29CLEtBQVY7QUFBaUIsa0JBQU1rQjtBQUF2QixXQUFsQjtBQUNBO0FBQ0QsT0FMRDtBQU1BLEtBUEQsTUFPTztBQUNOekIsVUFBSSxDQUFDWCxRQUFMLENBQWNXLElBQUksQ0FBQ25DLEtBQW5CLEVBQTBCLFVBQVM2RSxJQUFULEVBQWVqQixFQUFmLEVBQW1CO0FBQzVDM0IsY0FBTSxDQUFDakMsS0FBUCxDQUFhc0IsSUFBYixDQUFrQjtBQUFDLG1CQUFTLENBQVY7QUFBYSxnQkFBTXNDO0FBQW5CLFNBQWxCO0FBQ0EsT0FGRDtBQUdBOztBQUVEZSxXQUFPLEdBQUd4QyxJQUFJLENBQUNjLGVBQUwsQ0FBcUJoQixNQUFyQixFQUE2QkMsT0FBN0IsQ0FBVjtBQUNBLFFBQUl5QyxPQUFKLEVBQWExQyxNQUFNLENBQUNqQyxLQUFQLENBQWF3RCxJQUFiLENBQWtCbUIsT0FBbEIsRUEzQnFDLENBNkJsRDs7QUFDQTFDLFVBQU0sQ0FBQ3dDLEtBQVAsR0FBZXhDLE1BQU0sQ0FBQ2pDLEtBQVAsQ0FBYVMsTUFBNUI7O0FBQ0EsUUFBSSxPQUFPeUIsT0FBTyxDQUFDNkMsS0FBZixLQUF5QixRQUE3QixFQUF1QztBQUN0QzlDLFlBQU0sQ0FBQ2pDLEtBQVAsR0FBZWlDLE1BQU0sQ0FBQ2pDLEtBQVAsQ0FBYWdGLEtBQWIsQ0FBbUIsQ0FBbkIsRUFBc0I5QyxPQUFPLENBQUM2QyxLQUE5QixDQUFmO0FBQ0E7O0FBRUQsV0FBTzlDLE1BQVA7QUFDQSxHQXBDRCxDQXBVa0IsQ0EwV2xCO0FBQ0E7OztBQUVBLE1BQUlpQyxHQUFHLEdBQUcsU0FBTkEsR0FBTSxDQUFTRixDQUFULEVBQVlDLENBQVosRUFBZTtBQUN4QixRQUFJLE9BQU9ELENBQVAsS0FBYSxRQUFiLElBQXlCLE9BQU9DLENBQVAsS0FBYSxRQUExQyxFQUFvRDtBQUNuRCxhQUFPRCxDQUFDLEdBQUdDLENBQUosR0FBUSxDQUFSLEdBQWFELENBQUMsR0FBR0MsQ0FBSixHQUFRLENBQUMsQ0FBVCxHQUFhLENBQWpDO0FBQ0E7O0FBQ0RELEtBQUMsR0FBR2lCLFNBQVMsQ0FBQzFFLE1BQU0sQ0FBQ3lELENBQUMsSUFBSSxFQUFOLENBQVAsQ0FBYjtBQUNBQyxLQUFDLEdBQUdnQixTQUFTLENBQUMxRSxNQUFNLENBQUMwRCxDQUFDLElBQUksRUFBTixDQUFQLENBQWI7QUFDQSxRQUFJRCxDQUFDLEdBQUdDLENBQVIsRUFBVyxPQUFPLENBQVA7QUFDWCxRQUFJQSxDQUFDLEdBQUdELENBQVIsRUFBVyxPQUFPLENBQUMsQ0FBUjtBQUNYLFdBQU8sQ0FBUDtBQUNBLEdBVEQ7O0FBV0EsTUFBSUssTUFBTSxHQUFHLFNBQVRBLE1BQVMsQ0FBU0wsQ0FBVCxFQUFZQyxDQUFaLEVBQWU7QUFDM0IsUUFBSXZELENBQUosRUFBT0MsQ0FBUCxFQUFVdUUsQ0FBVixFQUFhekQsTUFBYjs7QUFDQSxTQUFLZixDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUd3RSxTQUFTLENBQUMxRSxNQUExQixFQUFrQ0MsQ0FBQyxHQUFHQyxDQUF0QyxFQUF5Q0QsQ0FBQyxFQUExQyxFQUE4QztBQUM3Q2UsWUFBTSxHQUFHMEQsU0FBUyxDQUFDekUsQ0FBRCxDQUFsQjtBQUNBLFVBQUksQ0FBQ2UsTUFBTCxFQUFhOztBQUNiLFdBQUt5RCxDQUFMLElBQVV6RCxNQUFWLEVBQWtCO0FBQ2pCLFlBQUlBLE1BQU0sQ0FBQ04sY0FBUCxDQUFzQitELENBQXRCLENBQUosRUFBOEI7QUFDN0JsQixXQUFDLENBQUNrQixDQUFELENBQUQsR0FBT3pELE1BQU0sQ0FBQ3lELENBQUQsQ0FBYjtBQUNBO0FBQ0Q7QUFDRDs7QUFDRCxXQUFPbEIsQ0FBUDtBQUNBLEdBWkQ7O0FBY0EsTUFBSTFELElBQUksR0FBRyxTQUFQQSxJQUFPLENBQVM4RSxHQUFULEVBQWM7QUFDeEIsV0FBTyxDQUFDQSxHQUFHLEdBQUcsRUFBUCxFQUFXaEUsT0FBWCxDQUFtQixhQUFuQixFQUFrQyxFQUFsQyxDQUFQO0FBQ0EsR0FGRDs7QUFJQSxNQUFJSCxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFTbUUsR0FBVCxFQUFjO0FBQ2hDLFdBQU8sQ0FBQ0EsR0FBRyxHQUFHLEVBQVAsRUFBV2hFLE9BQVgsQ0FBbUIsd0JBQW5CLEVBQTZDLE1BQTdDLENBQVA7QUFDQSxHQUZEOztBQUlBLE1BQUlPLFFBQVEsR0FBR0MsS0FBSyxDQUFDeUQsT0FBTixJQUFrQkMsQ0FBQyxJQUFJQSxDQUFDLENBQUNELE9BQXpCLElBQXFDLFVBQVM1RCxNQUFULEVBQWlCO0FBQ3BFLFdBQU84RCxNQUFNLENBQUNwRixTQUFQLENBQWlCcUYsUUFBakIsQ0FBMEJDLElBQTFCLENBQStCaEUsTUFBL0IsTUFBMkMsZ0JBQWxEO0FBQ0EsR0FGRDs7QUFJQSxNQUFJUCxVQUFVLEdBQUc7QUFDaEIsU0FBSyxxQkFEVztBQUVoQixTQUFLLFdBRlc7QUFHaEIsU0FBSyxTQUhXO0FBSWhCLFNBQUssbUJBSlc7QUFLaEIsU0FBSyxlQUxXO0FBTWhCLFNBQUssT0FOVztBQU9oQixTQUFLLFdBUFc7QUFRaEIsU0FBSyxvQkFSVztBQVNoQixTQUFLLE9BVFc7QUFVaEIsU0FBSyxTQVZXO0FBV2hCLFNBQUssT0FYVztBQVloQixTQUFLLGlCQVpXO0FBYWhCLFNBQUssU0FiVztBQWNoQixTQUFLO0FBZFcsR0FBakI7O0FBaUJBLE1BQUkrRCxTQUFTLEdBQUksWUFBVztBQUMzQixRQUFJdkUsQ0FBSixFQUFPQyxDQUFQLEVBQVV1RSxDQUFWLEVBQWFRLEtBQWI7QUFDQSxRQUFJQyxjQUFjLEdBQUcsRUFBckI7QUFDQSxRQUFJQyxNQUFNLEdBQUcsRUFBYjs7QUFDQSxTQUFLVixDQUFMLElBQVVoRSxVQUFWLEVBQXNCO0FBQ3JCLFVBQUlBLFVBQVUsQ0FBQ0MsY0FBWCxDQUEwQitELENBQTFCLENBQUosRUFBa0M7QUFDakNRLGFBQUssR0FBR3hFLFVBQVUsQ0FBQ2dFLENBQUQsQ0FBVixDQUFjVyxTQUFkLENBQXdCLENBQXhCLEVBQTJCM0UsVUFBVSxDQUFDZ0UsQ0FBRCxDQUFWLENBQWN6RSxNQUFkLEdBQXVCLENBQWxELENBQVI7QUFDQWtGLHNCQUFjLElBQUlELEtBQWxCOztBQUNBLGFBQUtoRixDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUcrRSxLQUFLLENBQUNqRixNQUF0QixFQUE4QkMsQ0FBQyxHQUFHQyxDQUFsQyxFQUFxQ0QsQ0FBQyxFQUF0QyxFQUEwQztBQUN6Q2tGLGdCQUFNLENBQUNGLEtBQUssQ0FBQ0ksTUFBTixDQUFhcEYsQ0FBYixDQUFELENBQU4sR0FBMEJ3RSxDQUExQjtBQUNBO0FBQ0Q7QUFDRDs7QUFDRCxRQUFJYSxNQUFNLEdBQUcsSUFBSTFFLE1BQUosQ0FBVyxNQUFPc0UsY0FBUCxHQUF3QixHQUFuQyxFQUF3QyxHQUF4QyxDQUFiO0FBQ0EsV0FBTyxVQUFTUCxHQUFULEVBQWM7QUFDcEIsYUFBT0EsR0FBRyxDQUFDaEUsT0FBSixDQUFZMkUsTUFBWixFQUFvQixVQUFTQyxhQUFULEVBQXdCO0FBQ2xELGVBQU9KLE1BQU0sQ0FBQ0ksYUFBRCxDQUFiO0FBQ0EsT0FGTSxFQUVKeEYsV0FGSSxFQUFQO0FBR0EsS0FKRDtBQUtBLEdBbkJlLEVBQWhCLENBbmFrQixDQXlibEI7QUFDQTs7O0FBRUEsU0FBT1QsTUFBUDtBQUNBLENBcmNBLENBQUQ7QUF5Y0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JDLFdBQVNILElBQVQsRUFBZUMsT0FBZixFQUF3QjtBQUN4QixNQUFJLElBQUosRUFBZ0Q7QUFDL0NDLDJDQUFzQkQsT0FBaEIsbWpCQUFOO0FBQ0EsR0FGRCxNQUVPLEVBSU47QUFDRCxDQVJBLEVBUUMsSUFSRCxFQVFPLFlBQVc7QUFDbEIsTUFBSW9HLFdBQVcsR0FBRyxFQUFsQjs7QUFFQUEsYUFBVyxDQUFDQyxLQUFaLEdBQW9CLFVBQVNDLFNBQVQsRUFBb0I7QUFDdkNBLGFBQVMsQ0FBQ0MsT0FBVixHQUFvQixFQUFwQjtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZUFELGFBQVMsQ0FBQ2hHLFNBQVYsQ0FBb0JrRyxpQkFBcEIsR0FBd0MsVUFBU0QsT0FBVCxFQUFrQjtBQUN6RCxVQUFJMUYsQ0FBSixFQUFPQyxDQUFQLEVBQVVtQixHQUFWO0FBQ0EsVUFBSUssSUFBSSxHQUFJLElBQVo7QUFDQSxVQUFJbUUsS0FBSyxHQUFHLEVBQVo7QUFFQW5FLFVBQUksQ0FBQ2lFLE9BQUwsR0FBZTtBQUNkRyxhQUFLLEVBQU8sRUFERTtBQUVkdEcsZ0JBQVEsRUFBSSxFQUZFO0FBR2R1RyxpQkFBUyxFQUFHLEVBSEU7QUFJZEMsY0FBTSxFQUFNO0FBSkUsT0FBZjs7QUFPQSxVQUFJQyxLQUFLLENBQUNyQixPQUFOLENBQWNlLE9BQWQsQ0FBSixFQUE0QjtBQUMzQixhQUFLMUYsQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxHQUFHeUYsT0FBTyxDQUFDM0YsTUFBeEIsRUFBZ0NDLENBQUMsR0FBR0MsQ0FBcEMsRUFBdUNELENBQUMsRUFBeEMsRUFBNEM7QUFDM0MsY0FBSSxPQUFPMEYsT0FBTyxDQUFDMUYsQ0FBRCxDQUFkLEtBQXNCLFFBQTFCLEVBQW9DO0FBQ25DNEYsaUJBQUssQ0FBQ2hGLElBQU4sQ0FBVzhFLE9BQU8sQ0FBQzFGLENBQUQsQ0FBbEI7QUFDQSxXQUZELE1BRU87QUFDTnlCLGdCQUFJLENBQUNpRSxPQUFMLENBQWFuRyxRQUFiLENBQXNCbUcsT0FBTyxDQUFDMUYsQ0FBRCxDQUFQLENBQVdnRCxJQUFqQyxJQUF5QzBDLE9BQU8sQ0FBQzFGLENBQUQsQ0FBUCxDQUFXd0IsT0FBcEQ7QUFDQW9FLGlCQUFLLENBQUNoRixJQUFOLENBQVc4RSxPQUFPLENBQUMxRixDQUFELENBQVAsQ0FBV2dELElBQXRCO0FBQ0E7QUFDRDtBQUNELE9BVEQsTUFTTyxJQUFJMEMsT0FBSixFQUFhO0FBQ25CLGFBQUt0RSxHQUFMLElBQVlzRSxPQUFaLEVBQXFCO0FBQ3BCLGNBQUlBLE9BQU8sQ0FBQ2pGLGNBQVIsQ0FBdUJXLEdBQXZCLENBQUosRUFBaUM7QUFDaENLLGdCQUFJLENBQUNpRSxPQUFMLENBQWFuRyxRQUFiLENBQXNCNkIsR0FBdEIsSUFBNkJzRSxPQUFPLENBQUN0RSxHQUFELENBQXBDO0FBQ0F3RSxpQkFBSyxDQUFDaEYsSUFBTixDQUFXUSxHQUFYO0FBQ0E7QUFDRDtBQUNEOztBQUVELGFBQU93RSxLQUFLLENBQUM3RixNQUFiLEVBQXFCO0FBQ3BCMEIsWUFBSSxDQUFDd0UsT0FBTCxDQUFhTCxLQUFLLENBQUNNLEtBQU4sRUFBYjtBQUNBO0FBQ0QsS0FqQ0Q7O0FBbUNBVCxhQUFTLENBQUNoRyxTQUFWLENBQW9CMEcsVUFBcEIsR0FBaUMsVUFBU25ELElBQVQsRUFBZTtBQUMvQyxVQUFJdkIsSUFBSSxHQUFNLElBQWQ7QUFDQSxVQUFJaUUsT0FBTyxHQUFHakUsSUFBSSxDQUFDaUUsT0FBbkI7QUFDQSxVQUFJVSxNQUFNLEdBQUlYLFNBQVMsQ0FBQ0MsT0FBVixDQUFrQjFDLElBQWxCLENBQWQ7O0FBRUEsVUFBSSxDQUFDeUMsU0FBUyxDQUFDQyxPQUFWLENBQWtCakYsY0FBbEIsQ0FBaUN1QyxJQUFqQyxDQUFMLEVBQTZDO0FBQzVDLGNBQU0sSUFBSXFELEtBQUosQ0FBVSxxQkFBc0JyRCxJQUF0QixHQUE2QixVQUF2QyxDQUFOO0FBQ0E7O0FBRUQwQyxhQUFPLENBQUNJLFNBQVIsQ0FBa0I5QyxJQUFsQixJQUEwQixJQUExQjtBQUNBMEMsYUFBTyxDQUFDSyxNQUFSLENBQWUvQyxJQUFmLElBQXVCb0QsTUFBTSxDQUFDRSxFQUFQLENBQVVqRixLQUFWLENBQWdCSSxJQUFoQixFQUFzQixDQUFDQSxJQUFJLENBQUNpRSxPQUFMLENBQWFuRyxRQUFiLENBQXNCeUQsSUFBdEIsS0FBK0IsRUFBaEMsQ0FBdEIsQ0FBdkI7QUFDQTBDLGFBQU8sQ0FBQ0csS0FBUixDQUFjakYsSUFBZCxDQUFtQm9DLElBQW5CO0FBQ0EsS0FaRDtBQWNBOzs7Ozs7O0FBS0F5QyxhQUFTLENBQUNoRyxTQUFWLENBQW9Cd0csT0FBcEIsR0FBOEIsVUFBU2pELElBQVQsRUFBZTtBQUM1QyxVQUFJdkIsSUFBSSxHQUFHLElBQVg7QUFDQSxVQUFJaUUsT0FBTyxHQUFHakUsSUFBSSxDQUFDaUUsT0FBbkI7O0FBRUEsVUFBSSxDQUFDakUsSUFBSSxDQUFDaUUsT0FBTCxDQUFhSyxNQUFiLENBQW9CdEYsY0FBcEIsQ0FBbUN1QyxJQUFuQyxDQUFMLEVBQStDO0FBQzlDLFlBQUkwQyxPQUFPLENBQUNJLFNBQVIsQ0FBa0I5QyxJQUFsQixDQUFKLEVBQTZCO0FBQzVCLGdCQUFNLElBQUlxRCxLQUFKLENBQVUsc0NBQXNDckQsSUFBdEMsR0FBNkMsSUFBdkQsQ0FBTjtBQUNBOztBQUNEdkIsWUFBSSxDQUFDMEUsVUFBTCxDQUFnQm5ELElBQWhCO0FBQ0E7O0FBRUQsYUFBTzBDLE9BQU8sQ0FBQ0ssTUFBUixDQUFlL0MsSUFBZixDQUFQO0FBQ0EsS0FaRDtBQWNBOzs7Ozs7OztBQU1BeUMsYUFBUyxDQUFDckcsTUFBVixHQUFtQixVQUFTNEQsSUFBVCxFQUFlc0QsRUFBZixFQUFtQjtBQUNyQ2IsZUFBUyxDQUFDQyxPQUFWLENBQWtCMUMsSUFBbEIsSUFBMEI7QUFDekIsZ0JBQVNBLElBRGdCO0FBRXpCLGNBQVNzRDtBQUZnQixPQUExQjtBQUlBLEtBTEQ7QUFNQSxHQWxHRDs7QUFvR0EsTUFBSU4sS0FBSyxHQUFHO0FBQ1hyQixXQUFPLEVBQUV6RCxLQUFLLENBQUN5RCxPQUFOLElBQWlCLFVBQVM0QixJQUFULEVBQWU7QUFDeEMsYUFBTzFCLE1BQU0sQ0FBQ3BGLFNBQVAsQ0FBaUJxRixRQUFqQixDQUEwQkMsSUFBMUIsQ0FBK0J3QixJQUEvQixNQUF5QyxnQkFBaEQ7QUFDQTtBQUhVLEdBQVo7QUFNQSxTQUFPaEIsV0FBUDtBQUNBLENBdEhBLENBQUQ7QUF3SEE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7O0FBQ0E7OztBQUVDLFdBQVNyRyxJQUFULEVBQWVDLE9BQWYsRUFBd0I7QUFDeEIsTUFBSSxJQUFKLEVBQWdEO0FBQy9DQyxxQ0FBb0IsQ0FBQyx5RUFBRCxFQUFVLDBCQUFWLEVBQW1CLDBCQUFuQixDQUFkLG9DQUFpREQsT0FBakQ7QUFBQTtBQUFBO0FBQUEsb0dBQU47QUFDQSxHQUZELE1BRU8sRUFJTjtBQUNELENBUkEsRUFRQyxJQVJELEVBUU8sVUFBU3lGLENBQVQsRUFBWXZGLE1BQVosRUFBb0JrRyxXQUFwQixFQUFpQztBQUN4Qzs7QUFFQSxNQUFJaUIsU0FBUyxHQUFHLG1CQUFTQyxRQUFULEVBQW1CQyxPQUFuQixFQUE0QjtBQUMzQyxRQUFJLE9BQU9BLE9BQVAsS0FBbUIsUUFBbkIsSUFBK0IsQ0FBQ0EsT0FBTyxDQUFDM0csTUFBNUMsRUFBb0Q7QUFDcEQsUUFBSUcsS0FBSyxHQUFJLE9BQU93RyxPQUFQLEtBQW1CLFFBQXBCLEdBQWdDLElBQUkvRixNQUFKLENBQVcrRixPQUFYLEVBQW9CLEdBQXBCLENBQWhDLEdBQTJEQSxPQUF2RTs7QUFFQSxRQUFJRixTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFTRyxJQUFULEVBQWU7QUFDOUIsVUFBSUMsSUFBSSxHQUFHLENBQVg7O0FBQ0EsVUFBSUQsSUFBSSxDQUFDRSxRQUFMLEtBQWtCLENBQXRCLEVBQXlCO0FBQ3hCLFlBQUk1RSxHQUFHLEdBQUcwRSxJQUFJLENBQUN2RSxJQUFMLENBQVViLE1BQVYsQ0FBaUJyQixLQUFqQixDQUFWOztBQUNBLFlBQUkrQixHQUFHLElBQUksQ0FBUCxJQUFZMEUsSUFBSSxDQUFDdkUsSUFBTCxDQUFVckMsTUFBVixHQUFtQixDQUFuQyxFQUFzQztBQUNyQyxjQUFJK0csS0FBSyxHQUFHSCxJQUFJLENBQUN2RSxJQUFMLENBQVUwRSxLQUFWLENBQWdCNUcsS0FBaEIsQ0FBWjtBQUNBLGNBQUk2RyxRQUFRLEdBQUdDLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixNQUF2QixDQUFmO0FBQ0FGLGtCQUFRLENBQUNHLFNBQVQsR0FBcUIsV0FBckI7QUFDQSxjQUFJQyxTQUFTLEdBQUdSLElBQUksQ0FBQ1MsU0FBTCxDQUFlbkYsR0FBZixDQUFoQjtBQUNBLGNBQUlvRixNQUFNLEdBQUdGLFNBQVMsQ0FBQ0MsU0FBVixDQUFvQk4sS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTL0csTUFBN0IsQ0FBYjtBQUNBLGNBQUl1SCxXQUFXLEdBQUdILFNBQVMsQ0FBQ0ksU0FBVixDQUFvQixJQUFwQixDQUFsQjtBQUNBUixrQkFBUSxDQUFDUyxXQUFULENBQXFCRixXQUFyQjtBQUNBSCxtQkFBUyxDQUFDTSxVQUFWLENBQXFCQyxZQUFyQixDQUFrQ1gsUUFBbEMsRUFBNENJLFNBQTVDO0FBQ0FQLGNBQUksR0FBRyxDQUFQO0FBQ0E7QUFDRCxPQWJELE1BYU8sSUFBSUQsSUFBSSxDQUFDRSxRQUFMLEtBQWtCLENBQWxCLElBQXVCRixJQUFJLENBQUNnQixVQUE1QixJQUEwQyxDQUFDLGtCQUFrQkMsSUFBbEIsQ0FBdUJqQixJQUFJLENBQUNrQixPQUE1QixDQUEvQyxFQUFxRjtBQUMzRixhQUFLLElBQUk3SCxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHMkcsSUFBSSxDQUFDZ0IsVUFBTCxDQUFnQjVILE1BQXBDLEVBQTRDLEVBQUVDLENBQTlDLEVBQWlEO0FBQ2hEQSxXQUFDLElBQUl3RyxTQUFTLENBQUNHLElBQUksQ0FBQ2dCLFVBQUwsQ0FBZ0IzSCxDQUFoQixDQUFELENBQWQ7QUFDQTtBQUNEOztBQUNELGFBQU80RyxJQUFQO0FBQ0EsS0FyQkQ7O0FBdUJBLFdBQU9ILFFBQVEsQ0FBQ3FCLElBQVQsQ0FBYyxZQUFXO0FBQy9CdEIsZUFBUyxDQUFDLElBQUQsQ0FBVDtBQUNBLEtBRk0sQ0FBUDtBQUdBLEdBOUJEOztBQWdDQSxNQUFJdUIsVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FBVyxDQUFFLENBQTlCOztBQUNBQSxZQUFVLENBQUN0SSxTQUFYLEdBQXVCO0FBQ3RCdUksTUFBRSxFQUFFLFlBQVNDLEtBQVQsRUFBZ0JDLEdBQWhCLEVBQW9CO0FBQ3ZCLFdBQUtDLE9BQUwsR0FBZSxLQUFLQSxPQUFMLElBQWdCLEVBQS9CO0FBQ0EsV0FBS0EsT0FBTCxDQUFhRixLQUFiLElBQXNCLEtBQUtFLE9BQUwsQ0FBYUYsS0FBYixLQUF1QixFQUE3Qzs7QUFDQSxXQUFLRSxPQUFMLENBQWFGLEtBQWIsRUFBb0JySCxJQUFwQixDQUF5QnNILEdBQXpCO0FBQ0EsS0FMcUI7QUFNdEJFLE9BQUcsRUFBRSxhQUFTSCxLQUFULEVBQWdCQyxHQUFoQixFQUFvQjtBQUN4QixVQUFJakksQ0FBQyxHQUFHd0UsU0FBUyxDQUFDMUUsTUFBbEI7QUFDQSxVQUFJRSxDQUFDLEtBQUssQ0FBVixFQUFhLE9BQU8sT0FBTyxLQUFLa0ksT0FBbkI7QUFDYixVQUFJbEksQ0FBQyxLQUFLLENBQVYsRUFBYSxPQUFPLE9BQU8sS0FBS2tJLE9BQUwsQ0FBYUYsS0FBYixDQUFkO0FBRWIsV0FBS0UsT0FBTCxHQUFlLEtBQUtBLE9BQUwsSUFBZ0IsRUFBL0I7QUFDQSxVQUFJRixLQUFLLElBQUksS0FBS0UsT0FBZCxLQUEwQixLQUE5QixFQUFxQzs7QUFDckMsV0FBS0EsT0FBTCxDQUFhRixLQUFiLEVBQW9CNUUsTUFBcEIsQ0FBMkIsS0FBSzhFLE9BQUwsQ0FBYUYsS0FBYixFQUFvQkksT0FBcEIsQ0FBNEJILEdBQTVCLENBQTNCLEVBQTZELENBQTdEO0FBQ0EsS0FkcUI7QUFldEJJLFdBQU8sRUFBRSxpQkFBU0w7QUFBTTtBQUFmLE1BQStCO0FBQ3ZDLFdBQUtFLE9BQUwsR0FBZSxLQUFLQSxPQUFMLElBQWdCLEVBQS9CO0FBQ0EsVUFBSUYsS0FBSyxJQUFJLEtBQUtFLE9BQWQsS0FBMEIsS0FBOUIsRUFBcUM7O0FBQ3JDLFdBQUssSUFBSW5JLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS21JLE9BQUwsQ0FBYUYsS0FBYixFQUFvQmxJLE1BQXhDLEVBQWdEQyxDQUFDLEVBQWpELEVBQW9EO0FBQ25ELGFBQUttSSxPQUFMLENBQWFGLEtBQWIsRUFBb0JqSSxDQUFwQixFQUF1QnFCLEtBQXZCLENBQTZCLElBQTdCLEVBQW1DSCxLQUFLLENBQUN6QixTQUFOLENBQWdCNkUsS0FBaEIsQ0FBc0JTLElBQXRCLENBQTJCTixTQUEzQixFQUFzQyxDQUF0QyxDQUFuQztBQUNBO0FBQ0Q7QUFyQnFCLEdBQXZCO0FBd0JBOzs7Ozs7OztBQU9Bc0QsWUFBVSxDQUFDdkMsS0FBWCxHQUFtQixVQUFTK0MsVUFBVCxFQUFvQjtBQUN0QyxRQUFJQyxLQUFLLEdBQUcsQ0FBQyxJQUFELEVBQU8sS0FBUCxFQUFjLFNBQWQsQ0FBWjs7QUFDQSxTQUFLLElBQUl4SSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHd0ksS0FBSyxDQUFDekksTUFBMUIsRUFBa0NDLENBQUMsRUFBbkMsRUFBc0M7QUFDckN1SSxnQkFBVSxDQUFDOUksU0FBWCxDQUFxQitJLEtBQUssQ0FBQ3hJLENBQUQsQ0FBMUIsSUFBaUMrSCxVQUFVLENBQUN0SSxTQUFYLENBQXFCK0ksS0FBSyxDQUFDeEksQ0FBRCxDQUExQixDQUFqQztBQUNBO0FBQ0QsR0FMRDs7QUFPQSxNQUFJeUksTUFBTSxHQUFVLE1BQU1iLElBQU4sQ0FBV2MsU0FBUyxDQUFDQyxTQUFyQixDQUFwQjtBQUVBLE1BQUlDLEtBQUssR0FBVyxFQUFwQjtBQUNBLE1BQUlDLFNBQVMsR0FBTyxHQUFwQjtBQUNBLE1BQUlDLFVBQVUsR0FBTSxFQUFwQjtBQUNBLE1BQUlDLE9BQU8sR0FBUyxFQUFwQjtBQUNBLE1BQUlDLFFBQVEsR0FBUSxFQUFwQjtBQUNBLE1BQUlDLE1BQU0sR0FBVSxFQUFwQjtBQUNBLE1BQUlDLEtBQUssR0FBVyxFQUFwQjtBQUNBLE1BQUlDLFNBQVMsR0FBTyxFQUFwQjtBQUNBLE1BQUlDLFFBQVEsR0FBUSxFQUFwQjtBQUNBLE1BQUlDLEtBQUssR0FBVyxFQUFwQjtBQUNBLE1BQUlDLGFBQWEsR0FBRyxDQUFwQjtBQUNBLE1BQUlDLFVBQVUsR0FBTSxFQUFwQjtBQUNBLE1BQUlDLFNBQVMsR0FBTyxFQUFwQjtBQUNBLE1BQUlDLE9BQU8sR0FBU2hCLE1BQU0sR0FBRyxFQUFILEdBQVEsRUFBbEM7QUFDQSxNQUFJaUIsUUFBUSxHQUFRakIsTUFBTSxHQUFHLEVBQUgsR0FBUSxFQUFsQztBQUNBLE1BQUlrQixPQUFPLEdBQVMsQ0FBcEI7QUFFQSxNQUFJQyxVQUFVLEdBQU0sQ0FBcEI7QUFDQSxNQUFJQyxTQUFTLEdBQU8sQ0FBcEIsQ0E5RndDLENBZ0d4Qzs7QUFDQSxNQUFJQyxxQkFBcUIsR0FBRyxDQUFDLFdBQVdsQyxJQUFYLENBQWdCbUMsTUFBTSxDQUFDckIsU0FBUCxDQUFpQkMsU0FBakMsQ0FBRCxJQUFnRCxDQUFDLENBQUMzQixRQUFRLENBQUNDLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0IrQyxRQUE3Rzs7QUFFQSxNQUFJQyxLQUFLLEdBQUcsU0FBUkEsS0FBUSxDQUFTbEosTUFBVCxFQUFpQjtBQUM1QixXQUFPLE9BQU9BLE1BQVAsS0FBa0IsV0FBekI7QUFDQSxHQUZEO0FBSUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQSxNQUFJbUosUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBU3BJLEtBQVQsRUFBZ0I7QUFDOUIsUUFBSSxPQUFPQSxLQUFQLEtBQWlCLFdBQWpCLElBQWdDQSxLQUFLLEtBQUssSUFBOUMsRUFBb0QsT0FBTyxJQUFQO0FBQ3BELFFBQUksT0FBT0EsS0FBUCxLQUFpQixTQUFyQixFQUFnQyxPQUFPQSxLQUFLLEdBQUcsR0FBSCxHQUFTLEdBQXJCO0FBQ2hDLFdBQU9BLEtBQUssR0FBRyxFQUFmO0FBQ0EsR0FKRDtBQU1BOzs7Ozs7OztBQU1BLE1BQUlxSSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxDQUFTekYsR0FBVCxFQUFjO0FBQy9CLFdBQU8sQ0FBQ0EsR0FBRyxHQUFHLEVBQVAsRUFDTGhFLE9BREssQ0FDRyxJQURILEVBQ1MsT0FEVCxFQUVMQSxPQUZLLENBRUcsSUFGSCxFQUVTLE1BRlQsRUFHTEEsT0FISyxDQUdHLElBSEgsRUFHUyxNQUhULEVBSUxBLE9BSkssQ0FJRyxJQUpILEVBSVMsUUFKVCxDQUFQO0FBS0EsR0FORDtBQVFBOzs7Ozs7OztBQU1BLE1BQUkwSixjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQVMxRixHQUFULEVBQWM7QUFDbEMsV0FBTyxDQUFDQSxHQUFHLEdBQUcsRUFBUCxFQUFXaEUsT0FBWCxDQUFtQixLQUFuQixFQUEwQixNQUExQixDQUFQO0FBQ0EsR0FGRDs7QUFJQSxNQUFJMkosSUFBSSxHQUFHLEVBQVg7QUFFQTs7Ozs7Ozs7O0FBUUFBLE1BQUksQ0FBQ0MsTUFBTCxHQUFjLFVBQVM3SSxJQUFULEVBQWU4SSxNQUFmLEVBQXVCakUsRUFBdkIsRUFBMkI7QUFDeEMsUUFBSWtFLFFBQVEsR0FBRy9JLElBQUksQ0FBQzhJLE1BQUQsQ0FBbkI7O0FBQ0E5SSxRQUFJLENBQUM4SSxNQUFELENBQUosR0FBZSxZQUFXO0FBQ3pCakUsUUFBRSxDQUFDakYsS0FBSCxDQUFTSSxJQUFULEVBQWVnRCxTQUFmO0FBQ0EsYUFBTytGLFFBQVEsQ0FBQ25KLEtBQVQsQ0FBZUksSUFBZixFQUFxQmdELFNBQXJCLENBQVA7QUFDQSxLQUhEO0FBSUEsR0FORDtBQVFBOzs7Ozs7Ozs7O0FBUUE0RixNQUFJLENBQUNJLEtBQUwsR0FBYSxVQUFTaEosSUFBVCxFQUFlOEksTUFBZixFQUF1QmpFLEVBQXZCLEVBQTJCO0FBQ3ZDLFFBQUlrRSxRQUFRLEdBQUcvSSxJQUFJLENBQUM4SSxNQUFELENBQW5COztBQUNBOUksUUFBSSxDQUFDOEksTUFBRCxDQUFKLEdBQWUsWUFBVztBQUN6QixVQUFJdEgsTUFBTSxHQUFHdUgsUUFBUSxDQUFDbkosS0FBVCxDQUFlSSxJQUFmLEVBQXFCZ0QsU0FBckIsQ0FBYjtBQUNBNkIsUUFBRSxDQUFDakYsS0FBSCxDQUFTSSxJQUFULEVBQWVnRCxTQUFmO0FBQ0EsYUFBT3hCLE1BQVA7QUFDQSxLQUpEO0FBS0EsR0FQRDtBQVNBOzs7Ozs7OztBQU1BLE1BQUl5SCxJQUFJLEdBQUcsU0FBUEEsSUFBTyxDQUFTcEUsRUFBVCxFQUFhO0FBQ3ZCLFFBQUlxRSxNQUFNLEdBQUcsS0FBYjtBQUNBLFdBQU8sWUFBVztBQUNqQixVQUFJQSxNQUFKLEVBQVk7QUFDWkEsWUFBTSxHQUFHLElBQVQ7QUFDQXJFLFFBQUUsQ0FBQ2pGLEtBQUgsQ0FBUyxJQUFULEVBQWVvRCxTQUFmO0FBQ0EsS0FKRDtBQUtBLEdBUEQ7QUFTQTs7Ozs7Ozs7OztBQVFBLE1BQUltRyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFTdEUsRUFBVCxFQUFhdUUsS0FBYixFQUFvQjtBQUNsQyxRQUFJQyxPQUFKO0FBQ0EsV0FBTyxZQUFXO0FBQ2pCLFVBQUlySixJQUFJLEdBQUcsSUFBWDtBQUNBLFVBQUlzSixJQUFJLEdBQUd0RyxTQUFYO0FBQ0FzRixZQUFNLENBQUNpQixZQUFQLENBQW9CRixPQUFwQjtBQUNBQSxhQUFPLEdBQUdmLE1BQU0sQ0FBQ2tCLFVBQVAsQ0FBa0IsWUFBVztBQUN0QzNFLFVBQUUsQ0FBQ2pGLEtBQUgsQ0FBU0ksSUFBVCxFQUFlc0osSUFBZjtBQUNBLE9BRlMsRUFFUEYsS0FGTyxDQUFWO0FBR0EsS0FQRDtBQVFBLEdBVkQ7QUFZQTs7Ozs7Ozs7OztBQVFBLE1BQUlLLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBU3pKLElBQVQsRUFBZTBKLEtBQWYsRUFBc0I3RSxFQUF0QixFQUEwQjtBQUMvQyxRQUFJOEUsSUFBSjtBQUNBLFFBQUk5QyxPQUFPLEdBQUc3RyxJQUFJLENBQUM2RyxPQUFuQjtBQUNBLFFBQUkrQyxVQUFVLEdBQUcsRUFBakIsQ0FIK0MsQ0FLL0M7O0FBQ0E1SixRQUFJLENBQUM2RyxPQUFMLEdBQWUsWUFBVztBQUN6QixVQUFJOEMsSUFBSSxHQUFHM0csU0FBUyxDQUFDLENBQUQsQ0FBcEI7O0FBQ0EsVUFBSTBHLEtBQUssQ0FBQzlDLE9BQU4sQ0FBYytDLElBQWQsTUFBd0IsQ0FBQyxDQUE3QixFQUFnQztBQUMvQkMsa0JBQVUsQ0FBQ0QsSUFBRCxDQUFWLEdBQW1CM0csU0FBbkI7QUFDQSxPQUZELE1BRU87QUFDTixlQUFPNkQsT0FBTyxDQUFDakgsS0FBUixDQUFjSSxJQUFkLEVBQW9CZ0QsU0FBcEIsQ0FBUDtBQUNBO0FBQ0QsS0FQRCxDQU4rQyxDQWUvQzs7O0FBQ0E2QixNQUFFLENBQUNqRixLQUFILENBQVNJLElBQVQsRUFBZSxFQUFmO0FBQ0FBLFFBQUksQ0FBQzZHLE9BQUwsR0FBZUEsT0FBZixDQWpCK0MsQ0FtQi9DOztBQUNBLFNBQUs4QyxJQUFMLElBQWFDLFVBQWIsRUFBeUI7QUFDeEIsVUFBSUEsVUFBVSxDQUFDNUssY0FBWCxDQUEwQjJLLElBQTFCLENBQUosRUFBcUM7QUFDcEM5QyxlQUFPLENBQUNqSCxLQUFSLENBQWNJLElBQWQsRUFBb0I0SixVQUFVLENBQUNELElBQUQsQ0FBOUI7QUFDQTtBQUNEO0FBQ0QsR0F6QkQ7QUEyQkE7Ozs7Ozs7Ozs7QUFRQSxNQUFJRSxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQVNDLE9BQVQsRUFBa0J0RCxLQUFsQixFQUF5QnVELFFBQXpCLEVBQW1DbEYsRUFBbkMsRUFBdUM7QUFDNURpRixXQUFPLENBQUN2RCxFQUFSLENBQVdDLEtBQVgsRUFBa0J1RCxRQUFsQixFQUE0QixVQUFTQyxDQUFULEVBQVk7QUFDdkMsVUFBSUMsS0FBSyxHQUFHRCxDQUFDLENBQUNFLE1BQWQ7O0FBQ0EsYUFBT0QsS0FBSyxJQUFJQSxLQUFLLENBQUNqRSxVQUFOLEtBQXFCOEQsT0FBTyxDQUFDLENBQUQsQ0FBNUMsRUFBaUQ7QUFDaERHLGFBQUssR0FBR0EsS0FBSyxDQUFDakUsVUFBZDtBQUNBOztBQUNEZ0UsT0FBQyxDQUFDRyxhQUFGLEdBQWtCRixLQUFsQjtBQUNBLGFBQU9wRixFQUFFLENBQUNqRixLQUFILENBQVMsSUFBVCxFQUFlLENBQUNvSyxDQUFELENBQWYsQ0FBUDtBQUNBLEtBUEQ7QUFRQSxHQVREO0FBV0E7Ozs7Ozs7Ozs7O0FBU0EsTUFBSUksWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBU0MsS0FBVCxFQUFnQjtBQUNsQyxRQUFJN0ksTUFBTSxHQUFHLEVBQWI7O0FBQ0EsUUFBSSxvQkFBb0I2SSxLQUF4QixFQUErQjtBQUM5QjdJLFlBQU0sQ0FBQzhJLEtBQVAsR0FBZUQsS0FBSyxDQUFDRSxjQUFyQjtBQUNBL0ksWUFBTSxDQUFDbEQsTUFBUCxHQUFnQitMLEtBQUssQ0FBQ0csWUFBTixHQUFxQmhKLE1BQU0sQ0FBQzhJLEtBQTVDO0FBQ0EsS0FIRCxNQUdPLElBQUkvRSxRQUFRLENBQUNrRixTQUFiLEVBQXdCO0FBQzlCSixXQUFLLENBQUNLLEtBQU47QUFDQSxVQUFJQyxHQUFHLEdBQUdwRixRQUFRLENBQUNrRixTQUFULENBQW1CRyxXQUFuQixFQUFWO0FBQ0EsVUFBSUMsTUFBTSxHQUFHdEYsUUFBUSxDQUFDa0YsU0FBVCxDQUFtQkcsV0FBbkIsR0FBaUNFLElBQWpDLENBQXNDeE0sTUFBbkQ7QUFDQXFNLFNBQUcsQ0FBQ0ksU0FBSixDQUFjLFdBQWQsRUFBMkIsQ0FBQ1YsS0FBSyxDQUFDaEssS0FBTixDQUFZL0IsTUFBeEM7QUFDQWtELFlBQU0sQ0FBQzhJLEtBQVAsR0FBZUssR0FBRyxDQUFDRyxJQUFKLENBQVN4TSxNQUFULEdBQWtCdU0sTUFBakM7QUFDQXJKLFlBQU0sQ0FBQ2xELE1BQVAsR0FBZ0J1TSxNQUFoQjtBQUNBOztBQUNELFdBQU9ySixNQUFQO0FBQ0EsR0FkRDtBQWdCQTs7Ozs7Ozs7O0FBT0EsTUFBSXdKLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBU0MsS0FBVCxFQUFnQkMsR0FBaEIsRUFBcUJDLFVBQXJCLEVBQWlDO0FBQ3JELFFBQUk1TSxDQUFKO0FBQUEsUUFBT0MsQ0FBUDtBQUFBLFFBQVU0TSxNQUFNLEdBQUcsRUFBbkI7O0FBQ0EsUUFBSUQsVUFBSixFQUFnQjtBQUNmLFdBQUs1TSxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUcyTSxVQUFVLENBQUM3TSxNQUEzQixFQUFtQ0MsQ0FBQyxHQUFHQyxDQUF2QyxFQUEwQ0QsQ0FBQyxFQUEzQyxFQUErQztBQUM5QzZNLGNBQU0sQ0FBQ0QsVUFBVSxDQUFDNU0sQ0FBRCxDQUFYLENBQU4sR0FBd0IwTSxLQUFLLENBQUNJLEdBQU4sQ0FBVUYsVUFBVSxDQUFDNU0sQ0FBRCxDQUFwQixDQUF4QjtBQUNBO0FBQ0QsS0FKRCxNQUlPO0FBQ042TSxZQUFNLEdBQUdILEtBQUssQ0FBQ0ksR0FBTixFQUFUO0FBQ0E7O0FBQ0RILE9BQUcsQ0FBQ0csR0FBSixDQUFRRCxNQUFSO0FBQ0EsR0FWRDtBQVlBOzs7Ozs7Ozs7O0FBUUEsTUFBSUUsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFTckksR0FBVCxFQUFjNkcsT0FBZCxFQUF1QjtBQUMxQyxRQUFJLENBQUM3RyxHQUFMLEVBQVU7QUFDVCxhQUFPLENBQVA7QUFDQTs7QUFFRCxRQUFJc0ksS0FBSyxHQUFHcEksQ0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZa0ksR0FBWixDQUFnQjtBQUMzQkcsY0FBUSxFQUFFLFVBRGlCO0FBRTNCQyxTQUFHLEVBQUUsQ0FBQyxLQUZxQjtBQUczQkMsVUFBSSxFQUFFLENBQUMsS0FIb0I7QUFJM0JDLFdBQUssRUFBRSxNQUpvQjtBQUszQkMsYUFBTyxFQUFFLENBTGtCO0FBTTNCQyxnQkFBVSxFQUFFO0FBTmUsS0FBaEIsRUFPVGYsSUFQUyxDQU9KN0gsR0FQSSxFQU9DNkksUUFQRCxDQU9VLE1BUFYsQ0FBWjtBQVNBZCxrQkFBYyxDQUFDbEIsT0FBRCxFQUFVeUIsS0FBVixFQUFpQixDQUM5QixlQUQ4QixFQUU5QixVQUY4QixFQUc5QixZQUg4QixFQUk5QixZQUo4QixFQUs5QixlQUw4QixDQUFqQixDQUFkO0FBUUEsUUFBSUksS0FBSyxHQUFHSixLQUFLLENBQUNJLEtBQU4sRUFBWjtBQUNBSixTQUFLLENBQUNRLE1BQU47QUFFQSxXQUFPSixLQUFQO0FBQ0EsR0ExQkQ7QUE0QkE7Ozs7Ozs7Ozs7O0FBU0EsTUFBSUssUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBU0MsTUFBVCxFQUFpQjtBQUMvQixRQUFJQyxZQUFZLEdBQUcsSUFBbkI7O0FBRUEsUUFBSUMsTUFBTSxHQUFHLFNBQVRBLE1BQVMsQ0FBU25DLENBQVQsRUFBWWpLLE9BQVosRUFBcUI7QUFDakMsVUFBSU0sS0FBSixFQUFXK0wsT0FBWCxFQUFvQkMsU0FBcEIsRUFBK0JDLFdBQS9CLEVBQTRDWCxLQUE1QztBQUNBLFVBQUlsSCxLQUFKLEVBQVc4SCxTQUFYLEVBQXNCOUIsU0FBdEI7QUFDQVQsT0FBQyxHQUFHQSxDQUFDLElBQUkxQixNQUFNLENBQUM5QixLQUFaLElBQXFCLEVBQXpCO0FBQ0F6RyxhQUFPLEdBQUdBLE9BQU8sSUFBSSxFQUFyQjtBQUVBLFVBQUlpSyxDQUFDLENBQUN3QyxPQUFGLElBQWF4QyxDQUFDLENBQUN5QyxNQUFuQixFQUEyQjtBQUMzQixVQUFJLENBQUMxTSxPQUFPLENBQUMyTSxLQUFULElBQWtCVCxNQUFNLENBQUN0TCxJQUFQLENBQVksTUFBWixNQUF3QixLQUE5QyxFQUFxRDtBQUVyRE4sV0FBSyxHQUFHNEwsTUFBTSxDQUFDVSxHQUFQLEVBQVI7O0FBQ0EsVUFBSTNDLENBQUMsQ0FBQ0wsSUFBRixJQUFVSyxDQUFDLENBQUNMLElBQUYsQ0FBT3RMLFdBQVAsT0FBeUIsU0FBdkMsRUFBa0Q7QUFDakQrTixlQUFPLEdBQUdwQyxDQUFDLENBQUNvQyxPQUFaO0FBQ0FDLGlCQUFTLEdBQ1BELE9BQU8sSUFBSSxFQUFYLElBQWlCQSxPQUFPLElBQUksR0FBN0IsSUFBcUM7QUFDcENBLGVBQU8sSUFBSSxFQUFYLElBQWlCQSxPQUFPLElBQUksRUFEN0IsSUFDcUM7QUFDcENBLGVBQU8sSUFBSSxFQUFYLElBQWlCQSxPQUFPLElBQUksRUFGN0IsSUFFcUM7QUFDckNBLGVBQU8sS0FBSyxFQUpKLENBSU87QUFKaEI7O0FBT0EsWUFBSUEsT0FBTyxLQUFLdEUsVUFBWixJQUEwQnNFLE9BQU8sS0FBS3ZFLGFBQTFDLEVBQXlEO0FBQ3hENEMsbUJBQVMsR0FBR0wsWUFBWSxDQUFDNkIsTUFBTSxDQUFDLENBQUQsQ0FBUCxDQUF4Qjs7QUFDQSxjQUFJeEIsU0FBUyxDQUFDbk0sTUFBZCxFQUFzQjtBQUNyQitCLGlCQUFLLEdBQUdBLEtBQUssQ0FBQ3FELFNBQU4sQ0FBZ0IsQ0FBaEIsRUFBbUIrRyxTQUFTLENBQUNILEtBQTdCLElBQXNDakssS0FBSyxDQUFDcUQsU0FBTixDQUFnQitHLFNBQVMsQ0FBQ0gsS0FBVixHQUFrQkcsU0FBUyxDQUFDbk0sTUFBNUMsQ0FBOUM7QUFDQSxXQUZELE1BRU8sSUFBSThOLE9BQU8sS0FBS3ZFLGFBQVosSUFBNkI0QyxTQUFTLENBQUNILEtBQTNDLEVBQWtEO0FBQ3hEakssaUJBQUssR0FBR0EsS0FBSyxDQUFDcUQsU0FBTixDQUFnQixDQUFoQixFQUFtQitHLFNBQVMsQ0FBQ0gsS0FBVixHQUFrQixDQUFyQyxJQUEwQ2pLLEtBQUssQ0FBQ3FELFNBQU4sQ0FBZ0IrRyxTQUFTLENBQUNILEtBQVYsR0FBa0IsQ0FBbEMsQ0FBbEQ7QUFDQSxXQUZNLE1BRUEsSUFBSThCLE9BQU8sS0FBS3RFLFVBQVosSUFBMEIsT0FBTzJDLFNBQVMsQ0FBQ0gsS0FBakIsS0FBMkIsV0FBekQsRUFBc0U7QUFDNUVqSyxpQkFBSyxHQUFHQSxLQUFLLENBQUNxRCxTQUFOLENBQWdCLENBQWhCLEVBQW1CK0csU0FBUyxDQUFDSCxLQUE3QixJQUFzQ2pLLEtBQUssQ0FBQ3FELFNBQU4sQ0FBZ0IrRyxTQUFTLENBQUNILEtBQVYsR0FBa0IsQ0FBbEMsQ0FBOUM7QUFDQTtBQUNELFNBVEQsTUFTTyxJQUFJK0IsU0FBSixFQUFlO0FBQ3JCNUgsZUFBSyxHQUFHdUYsQ0FBQyxDQUFDNEMsUUFBVjtBQUNBTCxtQkFBUyxHQUFHbk8sTUFBTSxDQUFDeU8sWUFBUCxDQUFvQjdDLENBQUMsQ0FBQ29DLE9BQXRCLENBQVo7QUFDQSxjQUFJM0gsS0FBSixFQUFXOEgsU0FBUyxHQUFHQSxTQUFTLENBQUNPLFdBQVYsRUFBWixDQUFYLEtBQ0tQLFNBQVMsR0FBR0EsU0FBUyxDQUFDbE8sV0FBVixFQUFaO0FBQ0xnQyxlQUFLLElBQUlrTSxTQUFUO0FBQ0E7QUFDRDs7QUFFREQsaUJBQVcsR0FBR0wsTUFBTSxDQUFDYyxJQUFQLENBQVksYUFBWixDQUFkOztBQUNBLFVBQUksQ0FBQzFNLEtBQUQsSUFBVWlNLFdBQWQsRUFBMkI7QUFDMUJqTSxhQUFLLEdBQUdpTSxXQUFSO0FBQ0E7O0FBRURYLFdBQUssR0FBR0wsYUFBYSxDQUFDakwsS0FBRCxFQUFRNEwsTUFBUixDQUFiLEdBQStCLENBQXZDOztBQUNBLFVBQUlOLEtBQUssS0FBS08sWUFBZCxFQUE0QjtBQUMzQkEsb0JBQVksR0FBR1AsS0FBZjtBQUNBTSxjQUFNLENBQUNOLEtBQVAsQ0FBYUEsS0FBYjtBQUNBTSxjQUFNLENBQUNlLGNBQVAsQ0FBc0IsUUFBdEI7QUFDQTtBQUNELEtBaEREOztBQWtEQWYsVUFBTSxDQUFDMUYsRUFBUCxDQUFVLDJCQUFWLEVBQXVDNEYsTUFBdkM7QUFDQUEsVUFBTTtBQUNOLEdBdkREOztBQXlEQSxNQUFJYyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFTaEIsTUFBVCxFQUFpQm5PLFFBQWpCLEVBQTJCO0FBQzFDLFFBQUk2QixHQUFKO0FBQUEsUUFBU3BCLENBQVQ7QUFBQSxRQUFZQyxDQUFaO0FBQUEsUUFBZTBPLEdBQWY7QUFBQSxRQUFvQjdDLEtBQXBCO0FBQUEsUUFBMkJySyxJQUFJLEdBQUcsSUFBbEM7QUFDQXFLLFNBQUssR0FBRzRCLE1BQU0sQ0FBQyxDQUFELENBQWQ7QUFDQTVCLFNBQUssQ0FBQzhDLFNBQU4sR0FBa0JuTixJQUFsQixDQUgwQyxDQUsxQzs7QUFDQSxRQUFJb04sYUFBYSxHQUFHOUUsTUFBTSxDQUFDK0UsZ0JBQVAsSUFBMkIvRSxNQUFNLENBQUMrRSxnQkFBUCxDQUF3QmhELEtBQXhCLEVBQStCLElBQS9CLENBQS9DO0FBQ0E2QyxPQUFHLEdBQUdFLGFBQWEsR0FBR0EsYUFBYSxDQUFDRSxnQkFBZCxDQUErQixXQUEvQixDQUFILEdBQWlEakQsS0FBSyxDQUFDa0QsWUFBTixJQUFzQmxELEtBQUssQ0FBQ2tELFlBQU4sQ0FBbUI1TCxTQUE3RztBQUNBdUwsT0FBRyxHQUFHQSxHQUFHLElBQUlqQixNQUFNLENBQUN1QixPQUFQLENBQWUsYUFBZixFQUE4QlQsSUFBOUIsQ0FBbUMsS0FBbkMsQ0FBUCxJQUFvRCxFQUExRCxDQVIwQyxDQVUxQzs7QUFDQTVKLEtBQUMsQ0FBQ2pCLE1BQUYsQ0FBU2xDLElBQVQsRUFBZTtBQUNkeU4sV0FBSyxFQUFjLENBREw7QUFFZDNQLGNBQVEsRUFBV0EsUUFGTDtBQUdkbU8sWUFBTSxFQUFhQSxNQUhMO0FBSWR5QixjQUFRLEVBQVd6QixNQUFNLENBQUNjLElBQVAsQ0FBWSxVQUFaLEtBQTJCLEVBSmhDO0FBS2RZLGFBQU8sRUFBWXRELEtBQUssQ0FBQ2pFLE9BQU4sQ0FBYy9ILFdBQWQsT0FBZ0MsUUFBaEMsR0FBMkM4SixVQUEzQyxHQUF3REMsU0FMN0Q7QUFNZHdGLFNBQUcsRUFBZ0IsT0FBT3pILElBQVAsQ0FBWStHLEdBQVosQ0FOTDtBQVFkVyxhQUFPLEVBQVksZUFBZ0IsRUFBRVosU0FBUyxDQUFDYSxLQVJqQztBQVNkQyxzQkFBZ0IsRUFBRyxJQVRMO0FBVWRDLFlBQU0sRUFBYSxLQVZMO0FBV2RDLGdCQUFVLEVBQVMsS0FYTDtBQVlkQyxnQkFBVSxFQUFTakMsTUFBTSxDQUFDa0MsRUFBUCxDQUFVLFlBQVYsQ0FaTDtBQWFkQyxlQUFTLEVBQVUsS0FiTDtBQWNkQyxjQUFRLEVBQVcsS0FkTDtBQWVkQyxlQUFTLEVBQVUsS0FmTDtBQWdCZEMsbUJBQWEsRUFBTSxLQWhCTDtBQWlCZEMsYUFBTyxFQUFZLEtBakJMO0FBa0JkQyxpQkFBVyxFQUFRLEtBbEJMO0FBbUJkQyxlQUFTLEVBQVUsS0FuQkw7QUFvQmRDLGdCQUFVLEVBQVMsS0FwQkw7QUFxQmRDLGlCQUFXLEVBQVEsS0FyQkw7QUFzQmRDLGdCQUFVLEVBQVMsS0F0Qkw7QUF1QmRDLGlCQUFXLEVBQVEsS0F2Qkw7QUF3QmRDLGdCQUFVLEVBQVMsS0F4Qkw7QUF5QmRDLG9CQUFjLEVBQUssSUF6Qkw7QUEwQmRDLGVBQVMsRUFBVSxFQTFCTDtBQTJCZEMsY0FBUSxFQUFXLENBM0JMO0FBNEJkQyxhQUFPLEVBQVksQ0E1Qkw7QUE2QmRDLG9CQUFjLEVBQUssRUE3Qkw7QUErQmRDLG1CQUFhLEVBQU0sSUEvQkw7QUFnQ2RDLGtCQUFZLEVBQU8sRUFoQ0w7QUFrQ2RDLGVBQVMsRUFBVSxFQWxDTDtBQW1DZHhQLGFBQU8sRUFBWSxFQW5DTDtBQW9DZHlQLGlCQUFXLEVBQVEsRUFwQ0w7QUFxQ2QzUixXQUFLLEVBQWMsRUFyQ0w7QUFzQ2Q0UixpQkFBVyxFQUFRLEVBdENMO0FBdUNkQyxvQkFBYyxFQUFLNVIsUUFBUSxDQUFDNlIsWUFBVCxLQUEwQixJQUExQixHQUFpQzNQLElBQUksQ0FBQzBQLGNBQXRDLEdBQXVEdkcsUUFBUSxDQUFDbkosSUFBSSxDQUFDMFAsY0FBTixFQUFzQjVSLFFBQVEsQ0FBQzZSLFlBQS9CO0FBdkNwRSxLQUFmLEVBWDBDLENBcUQxQzs7QUFDQTNQLFFBQUksQ0FBQzRQLE1BQUwsR0FBYyxJQUFJaFMsTUFBSixDQUFXLEtBQUttQyxPQUFoQixFQUF5QjtBQUFDaEMsZ0JBQVUsRUFBRUQsUUFBUSxDQUFDQztBQUF0QixLQUF6QixDQUFkLENBdEQwQyxDQXdEMUM7O0FBQ0EsUUFBSWlDLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY2lDLE9BQWxCLEVBQTJCO0FBQzFCLFdBQUt4QixDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUd3QixJQUFJLENBQUNsQyxRQUFMLENBQWNpQyxPQUFkLENBQXNCekIsTUFBdEMsRUFBOENDLENBQUMsR0FBR0MsQ0FBbEQsRUFBcURELENBQUMsRUFBdEQsRUFBMEQ7QUFDekR5QixZQUFJLENBQUM2UCxjQUFMLENBQW9CN1AsSUFBSSxDQUFDbEMsUUFBTCxDQUFjaUMsT0FBZCxDQUFzQnhCLENBQXRCLENBQXBCO0FBQ0E7O0FBQ0QsYUFBT3lCLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY2lDLE9BQXJCO0FBQ0EsS0E5RHlDLENBZ0UxQzs7O0FBQ0EsUUFBSUMsSUFBSSxDQUFDbEMsUUFBTCxDQUFjeVIsU0FBbEIsRUFBNkI7QUFDNUIsV0FBS2hSLENBQUMsR0FBRyxDQUFKLEVBQU9DLENBQUMsR0FBR3dCLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY3lSLFNBQWQsQ0FBd0JqUixNQUF4QyxFQUFnREMsQ0FBQyxHQUFHQyxDQUFwRCxFQUF1REQsQ0FBQyxFQUF4RCxFQUE0RDtBQUMzRHlCLFlBQUksQ0FBQzhQLG1CQUFMLENBQXlCOVAsSUFBSSxDQUFDbEMsUUFBTCxDQUFjeVIsU0FBZCxDQUF3QmhSLENBQXhCLENBQXpCO0FBQ0E7O0FBQ0QsYUFBT3lCLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY3lSLFNBQXJCO0FBQ0EsS0F0RXlDLENBd0UxQzs7O0FBQ0F2UCxRQUFJLENBQUNsQyxRQUFMLENBQWNpUyxJQUFkLEdBQXFCL1AsSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVMsSUFBZCxLQUF1Qi9QLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY2tTLFFBQWQsS0FBMkIsQ0FBM0IsR0FBK0IsUUFBL0IsR0FBMEMsT0FBakUsQ0FBckI7O0FBQ0EsUUFBSSxPQUFPaFEsSUFBSSxDQUFDbEMsUUFBTCxDQUFjbVMsWUFBckIsS0FBc0MsU0FBMUMsRUFBcUQ7QUFDcERqUSxVQUFJLENBQUNsQyxRQUFMLENBQWNtUyxZQUFkLEdBQTZCalEsSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVMsSUFBZCxLQUF1QixPQUFwRDtBQUNBOztBQUVEL1AsUUFBSSxDQUFDa0UsaUJBQUwsQ0FBdUJsRSxJQUFJLENBQUNsQyxRQUFMLENBQWNtRyxPQUFyQztBQUNBakUsUUFBSSxDQUFDa1EsY0FBTDtBQUNBbFEsUUFBSSxDQUFDbVEsY0FBTDtBQUNBblEsUUFBSSxDQUFDb1EsS0FBTDtBQUNBLEdBbEZELENBbmF3QyxDQXVmeEM7QUFDQTs7O0FBRUE5SixZQUFVLENBQUN2QyxLQUFYLENBQWlCa0osU0FBakI7QUFDQW5KLGFBQVcsQ0FBQ0MsS0FBWixDQUFrQmtKLFNBQWxCLEVBM2Z3QyxDQTZmeEM7QUFDQTs7QUFFQTlKLEdBQUMsQ0FBQ2pCLE1BQUYsQ0FBUytLLFNBQVMsQ0FBQ2pQLFNBQW5CLEVBQThCO0FBRTdCOzs7QUFHQW9TLFNBQUssRUFBRSxpQkFBVztBQUNqQixVQUFJcFEsSUFBSSxHQUFRLElBQWhCO0FBQ0EsVUFBSWxDLFFBQVEsR0FBSWtDLElBQUksQ0FBQ2xDLFFBQXJCO0FBQ0EsVUFBSStQLE9BQU8sR0FBSzdOLElBQUksQ0FBQzZOLE9BQXJCO0FBQ0EsVUFBSXdDLE9BQU8sR0FBS2xOLENBQUMsQ0FBQ21GLE1BQUQsQ0FBakI7QUFDQSxVQUFJZ0ksU0FBUyxHQUFHbk4sQ0FBQyxDQUFDb0MsUUFBRCxDQUFqQjtBQUNBLFVBQUkwRyxNQUFNLEdBQU1qTSxJQUFJLENBQUNpTSxNQUFyQjtBQUVBLFVBQUlzRSxRQUFKO0FBQ0EsVUFBSUMsUUFBSjtBQUNBLFVBQUlDLGNBQUo7QUFDQSxVQUFJQyxTQUFKO0FBQ0EsVUFBSUMsaUJBQUo7QUFDQSxVQUFJQyxnQkFBSjtBQUNBLFVBQUlDLFNBQUo7QUFDQSxVQUFJQyxZQUFKO0FBQ0EsVUFBSUMsYUFBSjtBQUNBLFVBQUlDLE9BQUo7QUFDQSxVQUFJQyxlQUFKO0FBRUFKLGVBQVMsR0FBVzdRLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY2lTLElBQWxDO0FBQ0FpQixhQUFPLEdBQWEvRSxNQUFNLENBQUNjLElBQVAsQ0FBWSxPQUFaLEtBQXdCLEVBQTVDO0FBRUF3RCxjQUFRLEdBQVlwTixDQUFDLENBQUMsT0FBRCxDQUFELENBQVcrTixRQUFYLENBQW9CcFQsUUFBUSxDQUFDcVQsWUFBN0IsRUFBMkNELFFBQTNDLENBQW9ERixPQUFwRCxFQUE2REUsUUFBN0QsQ0FBc0VMLFNBQXRFLENBQXBCO0FBQ0FMLGNBQVEsR0FBWXJOLENBQUMsQ0FBQyxPQUFELENBQUQsQ0FBVytOLFFBQVgsQ0FBb0JwVCxRQUFRLENBQUNzVCxVQUE3QixFQUF5Q0YsUUFBekMsQ0FBa0QsT0FBbEQsRUFBMkRwRixRQUEzRCxDQUFvRXlFLFFBQXBFLENBQXBCO0FBQ0FFLG9CQUFjLEdBQU10TixDQUFDLENBQUMsMENBQUQsQ0FBRCxDQUE4QzJJLFFBQTlDLENBQXVEMEUsUUFBdkQsRUFBaUV6RCxJQUFqRSxDQUFzRSxVQUF0RSxFQUFrRmQsTUFBTSxDQUFDa0MsRUFBUCxDQUFVLFdBQVYsSUFBeUIsSUFBekIsR0FBZ0NuTyxJQUFJLENBQUMwTixRQUF2SCxDQUFwQjtBQUNBa0Qsc0JBQWdCLEdBQUl6TixDQUFDLENBQUNyRixRQUFRLENBQUN1VCxjQUFULElBQTJCZCxRQUE1QixDQUFyQjtBQUNBRyxlQUFTLEdBQVd2TixDQUFDLENBQUMsT0FBRCxDQUFELENBQVcrTixRQUFYLENBQW9CcFQsUUFBUSxDQUFDd1QsYUFBN0IsRUFBNENKLFFBQTVDLENBQXFETCxTQUFyRCxFQUFnRVUsSUFBaEUsR0FBdUV6RixRQUF2RSxDQUFnRjhFLGdCQUFoRixDQUFwQjtBQUNBRCx1QkFBaUIsR0FBR3hOLENBQUMsQ0FBQyxPQUFELENBQUQsQ0FBVytOLFFBQVgsQ0FBb0JwVCxRQUFRLENBQUMwVCxvQkFBN0IsRUFBbUQxRixRQUFuRCxDQUE0RDRFLFNBQTVELENBQXBCOztBQUVBLFVBQUcxUSxJQUFJLENBQUNsQyxRQUFMLENBQWMyVCxxQkFBakIsRUFBd0M7QUFDdkNmLGlCQUFTLENBQUNRLFFBQVYsQ0FBbUJGLE9BQW5CO0FBQ0E7O0FBRURULGNBQVEsQ0FBQ2xGLEdBQVQsQ0FBYTtBQUNaTSxhQUFLLEVBQUVNLE1BQU0sQ0FBQyxDQUFELENBQU4sQ0FBVXlGLEtBQVYsQ0FBZ0IvRjtBQURYLE9BQWI7O0FBSUEsVUFBSTNMLElBQUksQ0FBQ2lFLE9BQUwsQ0FBYUcsS0FBYixDQUFtQjlGLE1BQXZCLEVBQStCO0FBQzlCMlMsdUJBQWUsR0FBRyxZQUFZalIsSUFBSSxDQUFDaUUsT0FBTCxDQUFhRyxLQUFiLENBQW1CdU4sSUFBbkIsQ0FBd0IsVUFBeEIsQ0FBOUI7QUFDQXBCLGdCQUFRLENBQUNXLFFBQVQsQ0FBa0JELGVBQWxCO0FBQ0FQLGlCQUFTLENBQUNRLFFBQVYsQ0FBbUJELGVBQW5CO0FBQ0E7O0FBRUQsVUFBSSxDQUFDblQsUUFBUSxDQUFDa1MsUUFBVCxLQUFzQixJQUF0QixJQUE4QmxTLFFBQVEsQ0FBQ2tTLFFBQVQsR0FBb0IsQ0FBbkQsS0FBeURoUSxJQUFJLENBQUMyTixPQUFMLEtBQWlCeEYsVUFBOUUsRUFBMEY7QUFDekY4RCxjQUFNLENBQUNjLElBQVAsQ0FBWSxVQUFaLEVBQXdCLFVBQXhCO0FBQ0E7O0FBRUQsVUFBSS9NLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY3dPLFdBQWxCLEVBQStCO0FBQzlCbUUsc0JBQWMsQ0FBQzFELElBQWYsQ0FBb0IsYUFBcEIsRUFBbUNqUCxRQUFRLENBQUN3TyxXQUE1QztBQUNBLE9BbERnQixDQW9EakI7OztBQUNBLFVBQUksQ0FBQ3RNLElBQUksQ0FBQ2xDLFFBQUwsQ0FBYzhULE9BQWYsSUFBMEI1UixJQUFJLENBQUNsQyxRQUFMLENBQWMrVCxTQUE1QyxFQUF1RDtBQUN0RCxZQUFJQyxnQkFBZ0IsR0FBRzlSLElBQUksQ0FBQ2xDLFFBQUwsQ0FBYytULFNBQWQsQ0FBd0I1UyxPQUF4QixDQUFnQyx3QkFBaEMsRUFBMEQsTUFBMUQsQ0FBdkI7QUFDQWUsWUFBSSxDQUFDbEMsUUFBTCxDQUFjOFQsT0FBZCxHQUF3QixJQUFJMVMsTUFBSixDQUFXLFNBQVM0UyxnQkFBVCxHQUE0QixPQUF2QyxDQUF4QjtBQUNBOztBQUVELFVBQUk3RixNQUFNLENBQUNjLElBQVAsQ0FBWSxhQUFaLENBQUosRUFBZ0M7QUFDL0IwRCxzQkFBYyxDQUFDMUQsSUFBZixDQUFvQixhQUFwQixFQUFtQ2QsTUFBTSxDQUFDYyxJQUFQLENBQVksYUFBWixDQUFuQztBQUNBOztBQUVELFVBQUlkLE1BQU0sQ0FBQ2MsSUFBUCxDQUFZLGdCQUFaLENBQUosRUFBbUM7QUFDbEMwRCxzQkFBYyxDQUFDMUQsSUFBZixDQUFvQixnQkFBcEIsRUFBc0NkLE1BQU0sQ0FBQ2MsSUFBUCxDQUFZLGdCQUFaLENBQXRDO0FBQ0E7O0FBRUQvTSxVQUFJLENBQUN1USxRQUFMLEdBQXlCQSxRQUF6QjtBQUNBdlEsVUFBSSxDQUFDd1EsUUFBTCxHQUF5QkEsUUFBekI7QUFDQXhRLFVBQUksQ0FBQ3lRLGNBQUwsR0FBeUJBLGNBQXpCO0FBQ0F6USxVQUFJLENBQUMwUSxTQUFMLEdBQXlCQSxTQUF6QjtBQUNBMVEsVUFBSSxDQUFDMlEsaUJBQUwsR0FBeUJBLGlCQUF6QjtBQUVBRCxlQUFTLENBQUNuSyxFQUFWLENBQWEsWUFBYixFQUEyQixtQkFBM0IsRUFBZ0QsWUFBVztBQUFFLGVBQU92RyxJQUFJLENBQUMrUixhQUFMLENBQW1CblMsS0FBbkIsQ0FBeUJJLElBQXpCLEVBQStCZ0QsU0FBL0IsQ0FBUDtBQUFtRCxPQUFoSDtBQUNBME4sZUFBUyxDQUFDbkssRUFBVixDQUFhLGlCQUFiLEVBQWdDLG1CQUFoQyxFQUFxRCxZQUFXO0FBQUUsZUFBT3ZHLElBQUksQ0FBQ2dTLGNBQUwsQ0FBb0JwUyxLQUFwQixDQUEwQkksSUFBMUIsRUFBZ0NnRCxTQUFoQyxDQUFQO0FBQW9ELE9BQXRIO0FBQ0E2RyxxQkFBZSxDQUFDMkcsUUFBRCxFQUFXLFdBQVgsRUFBd0IsY0FBeEIsRUFBd0MsWUFBVztBQUFFLGVBQU94USxJQUFJLENBQUNpUyxZQUFMLENBQWtCclMsS0FBbEIsQ0FBd0JJLElBQXhCLEVBQThCZ0QsU0FBOUIsQ0FBUDtBQUFrRCxPQUF2RyxDQUFmO0FBQ0FnSixjQUFRLENBQUN5RSxjQUFELENBQVI7QUFFQUQsY0FBUSxDQUFDakssRUFBVCxDQUFZO0FBQ1gyTCxpQkFBUyxFQUFHLHFCQUFXO0FBQUUsaUJBQU9sUyxJQUFJLENBQUNtUyxXQUFMLENBQWlCdlMsS0FBakIsQ0FBdUJJLElBQXZCLEVBQTZCZ0QsU0FBN0IsQ0FBUDtBQUFpRCxTQUQvRDtBQUVYb1AsYUFBSyxFQUFPLGlCQUFXO0FBQUUsaUJBQU9wUyxJQUFJLENBQUNxUyxPQUFMLENBQWF6UyxLQUFiLENBQW1CSSxJQUFuQixFQUF5QmdELFNBQXpCLENBQVA7QUFBNkM7QUFGM0QsT0FBWjtBQUtBeU4sb0JBQWMsQ0FBQ2xLLEVBQWYsQ0FBa0I7QUFDakIyTCxpQkFBUyxFQUFHLG1CQUFTbEksQ0FBVCxFQUFZO0FBQUVBLFdBQUMsQ0FBQ3NJLGVBQUY7QUFBc0IsU0FEL0I7QUFFakJDLGVBQU8sRUFBSyxtQkFBVztBQUFFLGlCQUFPdlMsSUFBSSxDQUFDd1MsU0FBTCxDQUFlNVMsS0FBZixDQUFxQkksSUFBckIsRUFBMkJnRCxTQUEzQixDQUFQO0FBQStDLFNBRnZEO0FBR2pCeVAsYUFBSyxFQUFPLGlCQUFXO0FBQUUsaUJBQU96UyxJQUFJLENBQUMwUyxPQUFMLENBQWE5UyxLQUFiLENBQW1CSSxJQUFuQixFQUF5QmdELFNBQXpCLENBQVA7QUFBNkMsU0FIckQ7QUFJakIyUCxnQkFBUSxFQUFJLG9CQUFXO0FBQUUsaUJBQU8zUyxJQUFJLENBQUM0UyxVQUFMLENBQWdCaFQsS0FBaEIsQ0FBc0JJLElBQXRCLEVBQTRCZ0QsU0FBNUIsQ0FBUDtBQUFnRCxTQUp4RDtBQUtqQjZQLGNBQU0sRUFBTSxrQkFBVztBQUFFN1MsY0FBSSxDQUFDOFMsZ0JBQUwsQ0FBc0JsVCxLQUF0QixDQUE0QkksSUFBNUIsRUFBa0MsRUFBbEM7QUFBd0MsU0FMaEQ7QUFNakIrUyxZQUFJLEVBQVEsZ0JBQVc7QUFBRSxpQkFBTy9TLElBQUksQ0FBQ2dULE1BQUwsQ0FBWXBULEtBQVosQ0FBa0JJLElBQWxCLEVBQXdCZ0QsU0FBeEIsQ0FBUDtBQUE0QyxTQU5wRDtBQU9qQjBILGFBQUssRUFBTyxpQkFBVztBQUFFMUssY0FBSSxDQUFDNk8sVUFBTCxHQUFrQixLQUFsQjtBQUF5QixpQkFBTzdPLElBQUksQ0FBQ2lULE9BQUwsQ0FBYXJULEtBQWIsQ0FBbUJJLElBQW5CLEVBQXlCZ0QsU0FBekIsQ0FBUDtBQUE2QyxTQVA5RTtBQVFqQmtRLGFBQUssRUFBTyxpQkFBVztBQUFFLGlCQUFPbFQsSUFBSSxDQUFDbVQsT0FBTCxDQUFhdlQsS0FBYixDQUFtQkksSUFBbkIsRUFBeUJnRCxTQUF6QixDQUFQO0FBQTZDO0FBUnJELE9BQWxCO0FBV0FzTixlQUFTLENBQUMvSixFQUFWLENBQWEsWUFBWXNILE9BQXpCLEVBQWtDLFVBQVM3RCxDQUFULEVBQVk7QUFDN0NoSyxZQUFJLENBQUMwTyxTQUFMLEdBQWlCMUUsQ0FBQyxDQUFDaEQsTUFBTSxHQUFHLFNBQUgsR0FBZSxTQUF0QixDQUFsQjtBQUNBaEgsWUFBSSxDQUFDMk8sVUFBTCxHQUFrQjNFLENBQUMsQ0FBQ2hELE1BQU0sR0FBRyxRQUFILEdBQWMsU0FBckIsQ0FBbkI7QUFDQWhILFlBQUksQ0FBQ3lPLFdBQUwsR0FBbUJ6RSxDQUFDLENBQUM0QyxRQUFyQjtBQUNBLE9BSkQ7QUFNQTBELGVBQVMsQ0FBQy9KLEVBQVYsQ0FBYSxVQUFVc0gsT0FBdkIsRUFBZ0MsVUFBUzdELENBQVQsRUFBWTtBQUMzQyxZQUFJQSxDQUFDLENBQUNvQyxPQUFGLEtBQWNuRSxRQUFsQixFQUE0QmpJLElBQUksQ0FBQzJPLFVBQUwsR0FBa0IsS0FBbEI7QUFDNUIsWUFBSTNFLENBQUMsQ0FBQ29DLE9BQUYsS0FBY3JFLFNBQWxCLEVBQTZCL0gsSUFBSSxDQUFDeU8sV0FBTCxHQUFtQixLQUFuQjtBQUM3QixZQUFJekUsQ0FBQyxDQUFDb0MsT0FBRixLQUFjcEUsT0FBbEIsRUFBMkJoSSxJQUFJLENBQUMwTyxTQUFMLEdBQWlCLEtBQWpCO0FBQzNCLE9BSkQ7QUFNQTRCLGVBQVMsQ0FBQy9KLEVBQVYsQ0FBYSxjQUFjc0gsT0FBM0IsRUFBb0MsVUFBUzdELENBQVQsRUFBWTtBQUMvQyxZQUFJaEssSUFBSSxDQUFDc08sU0FBVCxFQUFvQjtBQUNuQjtBQUNBLGNBQUl0RSxDQUFDLENBQUNFLE1BQUYsS0FBYWxLLElBQUksQ0FBQzBRLFNBQUwsQ0FBZSxDQUFmLENBQWIsSUFBa0MxRyxDQUFDLENBQUNFLE1BQUYsQ0FBU2xFLFVBQVQsS0FBd0JoRyxJQUFJLENBQUMwUSxTQUFMLENBQWUsQ0FBZixDQUE5RCxFQUFpRjtBQUNoRixtQkFBTyxLQUFQO0FBQ0EsV0FKa0IsQ0FLbkI7OztBQUNBLGNBQUksQ0FBQzFRLElBQUksQ0FBQ3dRLFFBQUwsQ0FBYzRDLEdBQWQsQ0FBa0JwSixDQUFDLENBQUNFLE1BQXBCLEVBQTRCNUwsTUFBN0IsSUFBdUMwTCxDQUFDLENBQUNFLE1BQUYsS0FBYWxLLElBQUksQ0FBQ3dRLFFBQUwsQ0FBYyxDQUFkLENBQXhELEVBQTBFO0FBQ3pFeFEsZ0JBQUksQ0FBQytTLElBQUwsQ0FBVS9JLENBQUMsQ0FBQ0UsTUFBWjtBQUNBO0FBQ0Q7QUFDRCxPQVhEO0FBYUFtRyxhQUFPLENBQUM5SixFQUFSLENBQVcsQ0FBQyxXQUFXc0gsT0FBWixFQUFxQixXQUFXQSxPQUFoQyxFQUF5QzhELElBQXpDLENBQThDLEdBQTlDLENBQVgsRUFBK0QsWUFBVztBQUN6RSxZQUFJM1IsSUFBSSxDQUFDZ08sTUFBVCxFQUFpQjtBQUNoQmhPLGNBQUksQ0FBQzhTLGdCQUFMLENBQXNCbFQsS0FBdEIsQ0FBNEJJLElBQTVCLEVBQWtDZ0QsU0FBbEM7QUFDQTtBQUNELE9BSkQ7QUFLQXFOLGFBQU8sQ0FBQzlKLEVBQVIsQ0FBVyxjQUFjc0gsT0FBekIsRUFBa0MsWUFBVztBQUM1QzdOLFlBQUksQ0FBQzhPLFdBQUwsR0FBbUIsS0FBbkI7QUFDQSxPQUZELEVBM0hpQixDQStIakI7QUFDQTs7QUFDQSxXQUFLdUUsY0FBTCxHQUFzQjtBQUNyQkMsaUJBQVMsRUFBR3JILE1BQU0sQ0FBQ3NILFFBQVAsR0FBa0JDLE1BQWxCLEVBRFM7QUFFckJDLGdCQUFRLEVBQUl4SCxNQUFNLENBQUNjLElBQVAsQ0FBWSxVQUFaO0FBRlMsT0FBdEI7QUFLQWQsWUFBTSxDQUFDYyxJQUFQLENBQVksVUFBWixFQUF3QixDQUFDLENBQXpCLEVBQTRCd0UsSUFBNUIsR0FBbUN2SSxLQUFuQyxDQUF5Q2hKLElBQUksQ0FBQ3VRLFFBQTlDOztBQUVBLFVBQUlwTixDQUFDLENBQUNELE9BQUYsQ0FBVXBGLFFBQVEsQ0FBQ0QsS0FBbkIsQ0FBSixFQUErQjtBQUM5Qm1DLFlBQUksQ0FBQzBULFFBQUwsQ0FBYzVWLFFBQVEsQ0FBQ0QsS0FBdkI7QUFDQSxlQUFPQyxRQUFRLENBQUNELEtBQWhCO0FBQ0EsT0EzSWdCLENBNklqQjs7O0FBQ0EsVUFBSXdLLHFCQUFKLEVBQTJCO0FBQzFCNEQsY0FBTSxDQUFDMUYsRUFBUCxDQUFVLFlBQVlzSCxPQUF0QixFQUErQixVQUFTN0QsQ0FBVCxFQUFZO0FBQzFDQSxXQUFDLENBQUMySixjQUFGO0FBQ0EzVCxjQUFJLENBQUNvTyxTQUFMLEdBQWlCLElBQWpCO0FBQ0FwTyxjQUFJLENBQUM0VCxZQUFMO0FBQ0EsU0FKRDtBQUtBOztBQUVENVQsVUFBSSxDQUFDNlQsbUJBQUw7QUFDQTdULFVBQUksQ0FBQzhULFlBQUw7QUFDQTlULFVBQUksQ0FBQzRULFlBQUw7QUFDQTVULFVBQUksQ0FBQytULGlCQUFMO0FBQ0EvVCxVQUFJLENBQUN3TyxPQUFMLEdBQWUsSUFBZjs7QUFFQSxVQUFJdkMsTUFBTSxDQUFDa0MsRUFBUCxDQUFVLFdBQVYsQ0FBSixFQUE0QjtBQUMzQm5PLFlBQUksQ0FBQ2dVLE9BQUw7QUFDQTs7QUFFRGhVLFVBQUksQ0FBQ3VHLEVBQUwsQ0FBUSxRQUFSLEVBQWtCLEtBQUswTixRQUF2QjtBQUVBaEksWUFBTSxDQUFDdEwsSUFBUCxDQUFZLFdBQVosRUFBeUJYLElBQXpCO0FBQ0FpTSxZQUFNLENBQUNpRixRQUFQLENBQWdCLFlBQWhCO0FBQ0FsUixVQUFJLENBQUM2RyxPQUFMLENBQWEsWUFBYixFQXBLaUIsQ0FzS2pCOztBQUNBLFVBQUkvSSxRQUFRLENBQUNvVyxPQUFULEtBQXFCLElBQXpCLEVBQStCO0FBQzlCbFUsWUFBSSxDQUFDMFAsY0FBTCxDQUFvQixFQUFwQjtBQUNBO0FBRUQsS0FoTDRCOztBQWtMN0I7OztBQUdBUyxrQkFBYyxFQUFFLDBCQUFXO0FBQzFCLFVBQUluUSxJQUFJLEdBQUcsSUFBWDtBQUNBLFVBQUltVSxXQUFXLEdBQUduVSxJQUFJLENBQUNsQyxRQUFMLENBQWNzVyxVQUFoQztBQUNBLFVBQUlDLGNBQWMsR0FBR3JVLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY3dXLGtCQUFuQztBQUVBLFVBQUlDLFNBQVMsR0FBRztBQUNmLG9CQUFZLGtCQUFTNVQsSUFBVCxFQUFlO0FBQzFCLGlCQUFPLDJCQUEyQkEsSUFBSSxDQUFDNlQsSUFBaEMsR0FBdUMsUUFBOUM7QUFDQSxTQUhjO0FBSWYsMkJBQW1CLHlCQUFTN1QsSUFBVCxFQUFlOFQsTUFBZixFQUF1QjtBQUN6QyxpQkFBTyxrQ0FBa0NBLE1BQU0sQ0FBQzlULElBQUksQ0FBQzBULGNBQUQsQ0FBTCxDQUF4QyxHQUFpRSxRQUF4RTtBQUNBLFNBTmM7QUFPZixrQkFBVSxnQkFBUzFULElBQVQsRUFBZThULE1BQWYsRUFBdUI7QUFDaEMsaUJBQU8seUJBQXlCQSxNQUFNLENBQUM5VCxJQUFJLENBQUN3VCxXQUFELENBQUwsQ0FBL0IsR0FBcUQsUUFBNUQ7QUFDQSxTQVRjO0FBVWYsZ0JBQVEsY0FBU3hULElBQVQsRUFBZThULE1BQWYsRUFBdUI7QUFDOUIsaUJBQU8sdUJBQXVCQSxNQUFNLENBQUM5VCxJQUFJLENBQUN3VCxXQUFELENBQUwsQ0FBN0IsR0FBbUQsUUFBMUQ7QUFDQSxTQVpjO0FBYWYseUJBQWlCLHVCQUFTeFQsSUFBVCxFQUFlOFQsTUFBZixFQUF1QjtBQUN2QyxpQkFBTyxxQ0FBcUNBLE1BQU0sQ0FBQzlULElBQUksQ0FBQzBKLEtBQU4sQ0FBM0MsR0FBMEQseUJBQWpFO0FBQ0E7QUFmYyxPQUFoQjtBQWtCQXJLLFVBQUksQ0FBQ2xDLFFBQUwsQ0FBYzRXLE1BQWQsR0FBdUJ2UixDQUFDLENBQUNqQixNQUFGLENBQVMsRUFBVCxFQUFhcVMsU0FBYixFQUF3QnZVLElBQUksQ0FBQ2xDLFFBQUwsQ0FBYzRXLE1BQXRDLENBQXZCO0FBQ0EsS0E3TTRCOztBQStNN0I7Ozs7QUFJQXhFLGtCQUFjLEVBQUUsMEJBQVc7QUFDMUIsVUFBSXZRLEdBQUo7QUFBQSxVQUFTa0YsRUFBVDtBQUFBLFVBQWE4UCxTQUFTLEdBQUc7QUFDeEIsc0JBQW9CLGNBREk7QUFFeEIsa0JBQW9CLFVBRkk7QUFHeEIsb0JBQW9CLFdBSEk7QUFJeEIsdUJBQW9CLGNBSkk7QUFLeEIsaUJBQW9CLFNBTEk7QUFNeEIsc0JBQW9CLGFBTkk7QUFPeEIseUJBQW9CLGdCQVBJO0FBUXhCLHdCQUFvQixlQVJJO0FBU3hCLHdCQUFvQixrQkFUSTtBQVV4QiwyQkFBb0IscUJBVkk7QUFXeEIsMEJBQW9CLG9CQVhJO0FBWXhCLHlCQUFvQixnQkFaSTtBQWF4QiwwQkFBb0IsaUJBYkk7QUFjeEIsZ0JBQW9CLFFBZEk7QUFleEIsZ0JBQW9CLFFBZkk7QUFnQnhCLGlCQUFvQixTQWhCSTtBQWlCeEIsZ0JBQW9CO0FBakJJLE9BQXpCOztBQW9CQSxXQUFLaFYsR0FBTCxJQUFZZ1YsU0FBWixFQUF1QjtBQUN0QixZQUFJQSxTQUFTLENBQUMzVixjQUFWLENBQXlCVyxHQUF6QixDQUFKLEVBQW1DO0FBQ2xDa0YsWUFBRSxHQUFHLEtBQUsvRyxRQUFMLENBQWM2VyxTQUFTLENBQUNoVixHQUFELENBQXZCLENBQUw7QUFDQSxjQUFJa0YsRUFBSixFQUFRLEtBQUswQixFQUFMLENBQVE1RyxHQUFSLEVBQWFrRixFQUFiO0FBQ1I7QUFDRDtBQUNELEtBOU80Qjs7QUFnUDdCOzs7Ozs7O0FBT0F3TixXQUFPLEVBQUUsaUJBQVNySSxDQUFULEVBQVk7QUFDcEIsVUFBSWhLLElBQUksR0FBRyxJQUFYLENBRG9CLENBR3BCO0FBQ0E7O0FBQ0EsVUFBSSxDQUFDQSxJQUFJLENBQUNzTyxTQUFWLEVBQXFCO0FBQ3BCdE8sWUFBSSxDQUFDMEssS0FBTDtBQUNBVixTQUFDLENBQUMySixjQUFGO0FBQ0E7QUFDRCxLQWhRNEI7O0FBa1E3Qjs7Ozs7OztBQU9BeEIsZUFBVyxFQUFFLHFCQUFTbkksQ0FBVCxFQUFZO0FBQ3hCLFVBQUloSyxJQUFJLEdBQUcsSUFBWDtBQUNBLFVBQUk0VSxnQkFBZ0IsR0FBRzVLLENBQUMsQ0FBQzZLLGtCQUFGLEVBQXZCO0FBQ0EsVUFBSUMsT0FBTyxHQUFHM1IsQ0FBQyxDQUFDNkcsQ0FBQyxDQUFDRSxNQUFILENBQWY7O0FBRUEsVUFBSWxLLElBQUksQ0FBQ3NPLFNBQVQsRUFBb0I7QUFDbkI7QUFDQTtBQUNBO0FBQ0EsWUFBSXRFLENBQUMsQ0FBQ0UsTUFBRixLQUFhbEssSUFBSSxDQUFDeVEsY0FBTCxDQUFvQixDQUFwQixDQUFqQixFQUF5QztBQUN4QyxjQUFJelEsSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVMsSUFBZCxLQUF1QixRQUEzQixFQUFxQztBQUNwQztBQUNBL1AsZ0JBQUksQ0FBQ2dPLE1BQUwsR0FBY2hPLElBQUksQ0FBQytVLEtBQUwsRUFBZCxHQUE2Qi9VLElBQUksQ0FBQ2dWLElBQUwsRUFBN0I7QUFDQSxXQUhELE1BR08sSUFBSSxDQUFDSixnQkFBTCxFQUF1QjtBQUM3QjVVLGdCQUFJLENBQUNpVixhQUFMLENBQW1CLElBQW5CO0FBQ0E7O0FBQ0QsaUJBQU8sS0FBUDtBQUNBO0FBQ0QsT0FiRCxNQWFPO0FBQ047QUFDQSxZQUFJLENBQUNMLGdCQUFMLEVBQXVCO0FBQ3RCdE0sZ0JBQU0sQ0FBQ2tCLFVBQVAsQ0FBa0IsWUFBVztBQUM1QnhKLGdCQUFJLENBQUMwSyxLQUFMO0FBQ0EsV0FGRCxFQUVHLENBRkg7QUFHQTtBQUNEO0FBQ0QsS0FuUzRCOztBQXFTN0I7Ozs7O0FBS0F1SixZQUFRLEVBQUUsb0JBQVc7QUFDcEIsV0FBS2hJLE1BQUwsQ0FBWXBGLE9BQVosQ0FBb0IsUUFBcEI7QUFDQSxLQTVTNEI7O0FBOFM3Qjs7Ozs7O0FBTUFzTSxXQUFPLEVBQUUsaUJBQVNuSixDQUFULEVBQVk7QUFDcEIsVUFBSWhLLElBQUksR0FBRyxJQUFYOztBQUNBLFVBQUlBLElBQUksQ0FBQ2tWLE1BQUwsTUFBaUJsVixJQUFJLENBQUN1TyxhQUF0QixJQUF1Q3ZPLElBQUksQ0FBQ3FPLFFBQWhELEVBQTBEO0FBQ3pEckUsU0FBQyxDQUFDMkosY0FBRjtBQUNBLE9BRkQsTUFFTztBQUNOO0FBQ0E7QUFDQSxZQUFJM1QsSUFBSSxDQUFDbEMsUUFBTCxDQUFjOFQsT0FBbEIsRUFBMkI7QUFDMUJwSSxvQkFBVSxDQUFDLFlBQVc7QUFDckIsZ0JBQUkyTCxVQUFVLEdBQUdoUyxDQUFDLENBQUNoRixJQUFGLENBQU82QixJQUFJLENBQUN5USxjQUFMLENBQW9COUQsR0FBcEIsTUFBNkIsRUFBcEMsRUFBd0M5TixLQUF4QyxDQUE4Q21CLElBQUksQ0FBQ2xDLFFBQUwsQ0FBYzhULE9BQTVELENBQWpCOztBQUNBLGlCQUFLLElBQUlyVCxDQUFDLEdBQUcsQ0FBUixFQUFXQyxDQUFDLEdBQUcyVyxVQUFVLENBQUM3VyxNQUEvQixFQUF1Q0MsQ0FBQyxHQUFHQyxDQUEzQyxFQUE4Q0QsQ0FBQyxFQUEvQyxFQUFtRDtBQUNsRHlCLGtCQUFJLENBQUNvVixVQUFMLENBQWdCRCxVQUFVLENBQUM1VyxDQUFELENBQTFCO0FBQ0E7QUFDRCxXQUxTLEVBS1AsQ0FMTyxDQUFWO0FBTUE7QUFDRDtBQUNELEtBcFU0Qjs7QUFzVTdCOzs7Ozs7QUFNQXFVLGNBQVUsRUFBRSxvQkFBUzVJLENBQVQsRUFBWTtBQUN2QixVQUFJLEtBQUtxRSxRQUFULEVBQW1CLE9BQU9yRSxDQUFDLElBQUlBLENBQUMsQ0FBQzJKLGNBQUYsRUFBWjtBQUNuQixVQUFJcEgsU0FBUyxHQUFHbk8sTUFBTSxDQUFDeU8sWUFBUCxDQUFvQjdDLENBQUMsQ0FBQ29DLE9BQUYsSUFBYXBDLENBQUMsQ0FBQ3FMLEtBQW5DLENBQWhCOztBQUNBLFVBQUksS0FBS3ZYLFFBQUwsQ0FBY3dYLE1BQWQsSUFBd0IsS0FBS3hYLFFBQUwsQ0FBY2lTLElBQWQsS0FBdUIsT0FBL0MsSUFBMER4RCxTQUFTLEtBQUssS0FBS3pPLFFBQUwsQ0FBYytULFNBQTFGLEVBQXFHO0FBQ3BHLGFBQUt1RCxVQUFMO0FBQ0FwTCxTQUFDLENBQUMySixjQUFGO0FBQ0EsZUFBTyxLQUFQO0FBQ0E7QUFDRCxLQXBWNEI7O0FBc1Y3Qjs7Ozs7O0FBTUFuQixhQUFTLEVBQUUsbUJBQVN4SSxDQUFULEVBQVk7QUFDdEIsVUFBSXVMLE9BQU8sR0FBR3ZMLENBQUMsQ0FBQ0UsTUFBRixLQUFhLEtBQUt1RyxjQUFMLENBQW9CLENBQXBCLENBQTNCO0FBQ0EsVUFBSXpRLElBQUksR0FBRyxJQUFYOztBQUVBLFVBQUlBLElBQUksQ0FBQ3FPLFFBQVQsRUFBbUI7QUFDbEIsWUFBSXJFLENBQUMsQ0FBQ29DLE9BQUYsS0FBY2xFLE9BQWxCLEVBQTJCO0FBQzFCOEIsV0FBQyxDQUFDMkosY0FBRjtBQUNBOztBQUNEO0FBQ0E7O0FBRUQsY0FBUTNKLENBQUMsQ0FBQ29DLE9BQVY7QUFDQyxhQUFLakYsS0FBTDtBQUNDLGNBQUluSCxJQUFJLENBQUMwTyxTQUFULEVBQW9CO0FBQ25CMU8sZ0JBQUksQ0FBQ3dWLFNBQUw7QUFDQTtBQUNBOztBQUNEOztBQUNELGFBQUtsTyxPQUFMO0FBQ0MsY0FBSXRILElBQUksQ0FBQ2dPLE1BQVQsRUFBaUI7QUFDaEJoRSxhQUFDLENBQUMySixjQUFGO0FBQ0EzSixhQUFDLENBQUNzSSxlQUFGO0FBQ0F0UyxnQkFBSSxDQUFDK1UsS0FBTDtBQUNBOztBQUNEOztBQUNELGFBQUtuTixLQUFMO0FBQ0MsY0FBSSxDQUFDb0MsQ0FBQyxDQUFDeUwsT0FBSCxJQUFjekwsQ0FBQyxDQUFDeUMsTUFBcEIsRUFBNEI7O0FBQzdCLGFBQUs5RSxRQUFMO0FBQ0MsY0FBSSxDQUFDM0gsSUFBSSxDQUFDZ08sTUFBTixJQUFnQmhPLElBQUksQ0FBQytPLFVBQXpCLEVBQXFDO0FBQ3BDL08sZ0JBQUksQ0FBQ2dWLElBQUw7QUFDQSxXQUZELE1BRU8sSUFBSWhWLElBQUksQ0FBQ3FQLGFBQVQsRUFBd0I7QUFDOUJyUCxnQkFBSSxDQUFDOE8sV0FBTCxHQUFtQixJQUFuQjtBQUNBLGdCQUFJNEcsS0FBSyxHQUFHMVYsSUFBSSxDQUFDMlYsaUJBQUwsQ0FBdUIzVixJQUFJLENBQUNxUCxhQUE1QixFQUEyQyxDQUEzQyxDQUFaO0FBQ0EsZ0JBQUlxRyxLQUFLLENBQUNwWCxNQUFWLEVBQWtCMEIsSUFBSSxDQUFDNFYsZUFBTCxDQUFxQkYsS0FBckIsRUFBNEIsSUFBNUIsRUFBa0MsSUFBbEM7QUFDbEI7O0FBQ0QxTCxXQUFDLENBQUMySixjQUFGO0FBQ0E7O0FBQ0QsYUFBS2xNLEtBQUw7QUFDQyxjQUFJLENBQUN1QyxDQUFDLENBQUN5TCxPQUFILElBQWN6TCxDQUFDLENBQUN5QyxNQUFwQixFQUE0Qjs7QUFDN0IsYUFBS2pGLE1BQUw7QUFDQyxjQUFJeEgsSUFBSSxDQUFDcVAsYUFBVCxFQUF3QjtBQUN2QnJQLGdCQUFJLENBQUM4TyxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsZ0JBQUkrRyxLQUFLLEdBQUc3VixJQUFJLENBQUMyVixpQkFBTCxDQUF1QjNWLElBQUksQ0FBQ3FQLGFBQTVCLEVBQTJDLENBQUMsQ0FBNUMsQ0FBWjtBQUNBLGdCQUFJd0csS0FBSyxDQUFDdlgsTUFBVixFQUFrQjBCLElBQUksQ0FBQzRWLGVBQUwsQ0FBcUJDLEtBQXJCLEVBQTRCLElBQTVCLEVBQWtDLElBQWxDO0FBQ2xCOztBQUNEN0wsV0FBQyxDQUFDMkosY0FBRjtBQUNBOztBQUNELGFBQUt0TSxVQUFMO0FBQ0MsY0FBSXJILElBQUksQ0FBQ2dPLE1BQUwsSUFBZWhPLElBQUksQ0FBQ3FQLGFBQXhCLEVBQXVDO0FBQ3RDclAsZ0JBQUksQ0FBQ2dTLGNBQUwsQ0FBb0I7QUFBQzdILDJCQUFhLEVBQUVuSyxJQUFJLENBQUNxUDtBQUFyQixhQUFwQjtBQUNBckYsYUFBQyxDQUFDMkosY0FBRjtBQUNBOztBQUNEOztBQUNELGFBQUtwTSxRQUFMO0FBQ0N2SCxjQUFJLENBQUM4VixnQkFBTCxDQUFzQixDQUFDLENBQXZCLEVBQTBCOUwsQ0FBMUI7QUFDQTs7QUFDRCxhQUFLdEMsU0FBTDtBQUNDMUgsY0FBSSxDQUFDOFYsZ0JBQUwsQ0FBc0IsQ0FBdEIsRUFBeUI5TCxDQUF6QjtBQUNBOztBQUNELGFBQUs5QixPQUFMO0FBQ0MsY0FBSWxJLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY2lZLFdBQWQsSUFBNkIvVixJQUFJLENBQUNnTyxNQUFsQyxJQUE0Q2hPLElBQUksQ0FBQ3FQLGFBQXJELEVBQW9FO0FBQ25FclAsZ0JBQUksQ0FBQ2dTLGNBQUwsQ0FBb0I7QUFBQzdILDJCQUFhLEVBQUVuSyxJQUFJLENBQUNxUDtBQUFyQixhQUFwQixFQURtRSxDQUduRTtBQUNBOztBQUNBLGdCQUFJLENBQUNyUCxJQUFJLENBQUNrVixNQUFMLEVBQUwsRUFBb0I7QUFDbkJsTCxlQUFDLENBQUMySixjQUFGO0FBQ0E7QUFDRDs7QUFDRCxjQUFJM1QsSUFBSSxDQUFDbEMsUUFBTCxDQUFjd1gsTUFBZCxJQUF3QnRWLElBQUksQ0FBQ29WLFVBQUwsRUFBNUIsRUFBK0M7QUFDOUNwTCxhQUFDLENBQUMySixjQUFGO0FBQ0E7O0FBQ0Q7O0FBQ0QsYUFBSzlMLGFBQUw7QUFDQSxhQUFLQyxVQUFMO0FBQ0M5SCxjQUFJLENBQUNnVyxlQUFMLENBQXFCaE0sQ0FBckI7QUFDQTtBQWpFRjs7QUFvRUEsVUFBSSxDQUFDaEssSUFBSSxDQUFDa1YsTUFBTCxNQUFpQmxWLElBQUksQ0FBQ3VPLGFBQXZCLEtBQXlDLEVBQUV2SCxNQUFNLEdBQUdnRCxDQUFDLENBQUN3QyxPQUFMLEdBQWV4QyxDQUFDLENBQUN5TCxPQUF6QixDQUE3QyxFQUFnRjtBQUMvRXpMLFNBQUMsQ0FBQzJKLGNBQUY7QUFDQTtBQUNBO0FBQ0QsS0EvYTRCOztBQWliN0I7Ozs7OztBQU1BakIsV0FBTyxFQUFFLGlCQUFTMUksQ0FBVCxFQUFZO0FBQ3BCLFVBQUloSyxJQUFJLEdBQUcsSUFBWDtBQUVBLFVBQUlBLElBQUksQ0FBQ3FPLFFBQVQsRUFBbUIsT0FBT3JFLENBQUMsSUFBSUEsQ0FBQyxDQUFDMkosY0FBRixFQUFaO0FBQ25CLFVBQUl0VCxLQUFLLEdBQUdMLElBQUksQ0FBQ3lRLGNBQUwsQ0FBb0I5RCxHQUFwQixNQUE2QixFQUF6Qzs7QUFDQSxVQUFJM00sSUFBSSxDQUFDaVAsU0FBTCxLQUFtQjVPLEtBQXZCLEVBQThCO0FBQzdCTCxZQUFJLENBQUNpUCxTQUFMLEdBQWlCNU8sS0FBakI7QUFDQUwsWUFBSSxDQUFDMFAsY0FBTCxDQUFvQnJQLEtBQXBCO0FBQ0FMLFlBQUksQ0FBQ2lXLGNBQUw7QUFDQWpXLFlBQUksQ0FBQzZHLE9BQUwsQ0FBYSxNQUFiLEVBQXFCeEcsS0FBckI7QUFDQTtBQUNELEtBbGM0Qjs7QUFvYzdCOzs7Ozs7OztBQVFBcVAsa0JBQWMsRUFBRSx3QkFBU3JQLEtBQVQsRUFBZ0I7QUFDL0IsVUFBSUwsSUFBSSxHQUFHLElBQVg7QUFDQSxVQUFJNkUsRUFBRSxHQUFHN0UsSUFBSSxDQUFDbEMsUUFBTCxDQUFjb1ksSUFBdkI7QUFDQSxVQUFJLENBQUNyUixFQUFMLEVBQVM7QUFDVCxVQUFJN0UsSUFBSSxDQUFDb1AsY0FBTCxDQUFvQnBRLGNBQXBCLENBQW1DcUIsS0FBbkMsQ0FBSixFQUErQztBQUMvQ0wsVUFBSSxDQUFDb1AsY0FBTCxDQUFvQi9PLEtBQXBCLElBQTZCLElBQTdCO0FBQ0FMLFVBQUksQ0FBQ2tXLElBQUwsQ0FBVSxVQUFTM1csUUFBVCxFQUFtQjtBQUM1QnNGLFVBQUUsQ0FBQ2pGLEtBQUgsQ0FBU0ksSUFBVCxFQUFlLENBQUNLLEtBQUQsRUFBUWQsUUFBUixDQUFmO0FBQ0EsT0FGRDtBQUdBLEtBcmQ0Qjs7QUF1ZDdCOzs7Ozs7QUFNQTBULFdBQU8sRUFBRSxpQkFBU2pKLENBQVQsRUFBWTtBQUNwQixVQUFJaEssSUFBSSxHQUFHLElBQVg7QUFDQSxVQUFJbVcsVUFBVSxHQUFHblcsSUFBSSxDQUFDc08sU0FBdEI7O0FBRUEsVUFBSXRPLElBQUksQ0FBQ2lPLFVBQVQsRUFBcUI7QUFDcEJqTyxZQUFJLENBQUMrUyxJQUFMO0FBQ0EvSSxTQUFDLElBQUlBLENBQUMsQ0FBQzJKLGNBQUYsRUFBTDtBQUNBLGVBQU8sS0FBUDtBQUNBOztBQUVELFVBQUkzVCxJQUFJLENBQUM0TyxXQUFULEVBQXNCO0FBQ3RCNU8sVUFBSSxDQUFDc08sU0FBTCxHQUFpQixJQUFqQjtBQUNBLFVBQUl0TyxJQUFJLENBQUNsQyxRQUFMLENBQWNvVyxPQUFkLEtBQTBCLE9BQTlCLEVBQXVDbFUsSUFBSSxDQUFDMFAsY0FBTCxDQUFvQixFQUFwQjtBQUV2QyxVQUFJLENBQUN5RyxVQUFMLEVBQWlCblcsSUFBSSxDQUFDNkcsT0FBTCxDQUFhLE9BQWI7O0FBRWpCLFVBQUksQ0FBQzdHLElBQUksQ0FBQ3NQLFlBQUwsQ0FBa0JoUixNQUF2QixFQUErQjtBQUM5QjBCLFlBQUksQ0FBQ29XLFNBQUw7QUFDQXBXLFlBQUksQ0FBQ2lWLGFBQUwsQ0FBbUIsSUFBbkI7QUFDQWpWLFlBQUksQ0FBQ2lXLGNBQUwsQ0FBb0IsQ0FBQyxDQUFDalcsSUFBSSxDQUFDbEMsUUFBTCxDQUFjdVksV0FBcEM7QUFDQTs7QUFFRHJXLFVBQUksQ0FBQzRULFlBQUw7QUFDQSxLQXBmNEI7O0FBc2Y3Qjs7Ozs7O0FBTUFaLFVBQU0sRUFBRSxnQkFBU2hKLENBQVQsRUFBWXNNLElBQVosRUFBa0I7QUFDekIsVUFBSXRXLElBQUksR0FBRyxJQUFYO0FBQ0EsVUFBSSxDQUFDQSxJQUFJLENBQUNzTyxTQUFWLEVBQXFCO0FBQ3JCdE8sVUFBSSxDQUFDc08sU0FBTCxHQUFpQixLQUFqQjs7QUFFQSxVQUFJdE8sSUFBSSxDQUFDNE8sV0FBVCxFQUFzQjtBQUNyQjtBQUNBLE9BRkQsTUFFTyxJQUFJLENBQUM1TyxJQUFJLENBQUM2TyxVQUFOLElBQW9CdEosUUFBUSxDQUFDZ1IsYUFBVCxLQUEyQnZXLElBQUksQ0FBQzJRLGlCQUFMLENBQXVCLENBQXZCLENBQW5ELEVBQThFO0FBQ3BGO0FBQ0EzUSxZQUFJLENBQUM2TyxVQUFMLEdBQWtCLElBQWxCO0FBQ0E3TyxZQUFJLENBQUNpVCxPQUFMLENBQWFqSixDQUFiO0FBQ0E7QUFDQTs7QUFFRCxVQUFJd00sVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FBVztBQUMzQnhXLFlBQUksQ0FBQytVLEtBQUw7QUFDQS9VLFlBQUksQ0FBQ3lXLGVBQUwsQ0FBcUIsRUFBckI7QUFDQXpXLFlBQUksQ0FBQ2lWLGFBQUwsQ0FBbUIsSUFBbkI7QUFDQWpWLFlBQUksQ0FBQzRWLGVBQUwsQ0FBcUIsSUFBckI7QUFDQTVWLFlBQUksQ0FBQzBXLFFBQUwsQ0FBYzFXLElBQUksQ0FBQ25DLEtBQUwsQ0FBV1MsTUFBekI7QUFDQTBCLFlBQUksQ0FBQzRULFlBQUwsR0FOMkIsQ0FRM0I7O0FBQ0EsU0FBQzBDLElBQUksSUFBSS9RLFFBQVEsQ0FBQ29SLElBQWxCLEVBQXdCak0sS0FBeEI7QUFFQTFLLFlBQUksQ0FBQzRPLFdBQUwsR0FBbUIsS0FBbkI7QUFDQTVPLFlBQUksQ0FBQzZHLE9BQUwsQ0FBYSxNQUFiO0FBQ0EsT0FiRDs7QUFlQTdHLFVBQUksQ0FBQzRPLFdBQUwsR0FBbUIsSUFBbkI7O0FBQ0EsVUFBSTVPLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY3dYLE1BQWQsSUFBd0J0VixJQUFJLENBQUNsQyxRQUFMLENBQWM4WSxZQUExQyxFQUF3RDtBQUN2RDVXLFlBQUksQ0FBQ29WLFVBQUwsQ0FBZ0IsSUFBaEIsRUFBc0IsS0FBdEIsRUFBNkJvQixVQUE3QjtBQUNBLE9BRkQsTUFFTztBQUNOQSxrQkFBVTtBQUNWO0FBQ0QsS0EvaEI0Qjs7QUFpaUI3Qjs7Ozs7OztBQU9BekUsaUJBQWEsRUFBRSx1QkFBUy9ILENBQVQsRUFBWTtBQUMxQixVQUFJLEtBQUs4RSxXQUFULEVBQXNCO0FBQ3RCLFdBQUs4RyxlQUFMLENBQXFCNUwsQ0FBQyxDQUFDRyxhQUF2QixFQUFzQyxLQUF0QztBQUNBLEtBM2lCNEI7O0FBNmlCN0I7Ozs7Ozs7QUFPQTZILGtCQUFjLEVBQUUsd0JBQVNoSSxDQUFULEVBQVk7QUFDM0IsVUFBSTNKLEtBQUo7QUFBQSxVQUFXeVUsT0FBWDtBQUFBLFVBQW9CK0IsT0FBcEI7QUFBQSxVQUE2QjdXLElBQUksR0FBRyxJQUFwQzs7QUFFQSxVQUFJZ0ssQ0FBQyxDQUFDMkosY0FBTixFQUFzQjtBQUNyQjNKLFNBQUMsQ0FBQzJKLGNBQUY7QUFDQTNKLFNBQUMsQ0FBQ3NJLGVBQUY7QUFDQTs7QUFFRHdDLGFBQU8sR0FBRzNSLENBQUMsQ0FBQzZHLENBQUMsQ0FBQ0csYUFBSCxDQUFYOztBQUNBLFVBQUkySyxPQUFPLENBQUNnQyxRQUFSLENBQWlCLFFBQWpCLENBQUosRUFBZ0M7QUFDL0I5VyxZQUFJLENBQUNvVixVQUFMLENBQWdCLElBQWhCLEVBQXNCLFlBQVc7QUFDaEMsY0FBSXBWLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY2laLGdCQUFsQixFQUFvQztBQUNuQy9XLGdCQUFJLENBQUMrVSxLQUFMO0FBQ0E7QUFDRCxTQUpEO0FBS0EsT0FORCxNQU1PO0FBQ04xVSxhQUFLLEdBQUd5VSxPQUFPLENBQUMvSCxJQUFSLENBQWEsWUFBYixDQUFSOztBQUNBLFlBQUksT0FBTzFNLEtBQVAsS0FBaUIsV0FBckIsRUFBa0M7QUFDakNMLGNBQUksQ0FBQ2dYLFNBQUwsR0FBaUIsSUFBakI7QUFDQWhYLGNBQUksQ0FBQ3lXLGVBQUwsQ0FBcUIsRUFBckI7QUFDQXpXLGNBQUksQ0FBQ2lYLE9BQUwsQ0FBYTVXLEtBQWI7O0FBQ0EsY0FBSUwsSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVosZ0JBQWxCLEVBQW9DO0FBQ25DL1csZ0JBQUksQ0FBQytVLEtBQUw7QUFDQSxXQUZELE1BRU8sSUFBSSxDQUFDL1UsSUFBSSxDQUFDbEMsUUFBTCxDQUFjbVMsWUFBZixJQUErQmpHLENBQUMsQ0FBQ0wsSUFBakMsSUFBeUMsUUFBUXhELElBQVIsQ0FBYTZELENBQUMsQ0FBQ0wsSUFBZixDQUE3QyxFQUFtRTtBQUN6RTNKLGdCQUFJLENBQUM0VixlQUFMLENBQXFCNVYsSUFBSSxDQUFDa1gsU0FBTCxDQUFlN1csS0FBZixDQUFyQjtBQUNBO0FBQ0Q7QUFDRDtBQUNELEtBaGxCNEI7O0FBa2xCN0I7Ozs7Ozs7QUFPQTRSLGdCQUFZLEVBQUUsc0JBQVNqSSxDQUFULEVBQVk7QUFDekIsVUFBSWhLLElBQUksR0FBRyxJQUFYO0FBRUEsVUFBSUEsSUFBSSxDQUFDcU8sUUFBVCxFQUFtQjs7QUFDbkIsVUFBSXJPLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY2lTLElBQWQsS0FBdUIsT0FBM0IsRUFBb0M7QUFDbkMvRixTQUFDLENBQUMySixjQUFGO0FBQ0EzVCxZQUFJLENBQUNpVixhQUFMLENBQW1CakwsQ0FBQyxDQUFDRyxhQUFyQixFQUFvQ0gsQ0FBcEM7QUFDQTtBQUNELEtBam1CNEI7O0FBbW1CN0I7Ozs7Ozs7QUFPQWtNLFFBQUksRUFBRSxjQUFTclIsRUFBVCxFQUFhO0FBQ2xCLFVBQUk3RSxJQUFJLEdBQUcsSUFBWDtBQUNBLFVBQUl1USxRQUFRLEdBQUd2USxJQUFJLENBQUN1USxRQUFMLENBQWNXLFFBQWQsQ0FBdUJsUixJQUFJLENBQUNsQyxRQUFMLENBQWNxWixZQUFyQyxDQUFmO0FBRUFuWCxVQUFJLENBQUNtUCxPQUFMO0FBQ0F0SyxRQUFFLENBQUNqRixLQUFILENBQVNJLElBQVQsRUFBZSxDQUFDLFVBQVNvWCxPQUFULEVBQWtCO0FBQ2pDcFgsWUFBSSxDQUFDbVAsT0FBTCxHQUFla0ksSUFBSSxDQUFDQyxHQUFMLENBQVN0WCxJQUFJLENBQUNtUCxPQUFMLEdBQWUsQ0FBeEIsRUFBMkIsQ0FBM0IsQ0FBZjs7QUFDQSxZQUFJaUksT0FBTyxJQUFJQSxPQUFPLENBQUM5WSxNQUF2QixFQUErQjtBQUM5QjBCLGNBQUksQ0FBQ3VYLFNBQUwsQ0FBZUgsT0FBZjtBQUNBcFgsY0FBSSxDQUFDaVcsY0FBTCxDQUFvQmpXLElBQUksQ0FBQ3NPLFNBQUwsSUFBa0IsQ0FBQ3RPLElBQUksQ0FBQ3VPLGFBQTVDO0FBQ0E7O0FBQ0QsWUFBSSxDQUFDdk8sSUFBSSxDQUFDbVAsT0FBVixFQUFtQjtBQUNsQm9CLGtCQUFRLENBQUNpSCxXQUFULENBQXFCeFgsSUFBSSxDQUFDbEMsUUFBTCxDQUFjcVosWUFBbkM7QUFDQTs7QUFDRG5YLFlBQUksQ0FBQzZHLE9BQUwsQ0FBYSxNQUFiLEVBQXFCdVEsT0FBckI7QUFDQSxPQVZjLENBQWY7QUFXQSxLQTFuQjRCOztBQTRuQjdCOzs7OztBQUtBWCxtQkFBZSxFQUFFLHlCQUFTcFcsS0FBVCxFQUFnQjtBQUNoQyxVQUFJNEwsTUFBTSxHQUFHLEtBQUt3RSxjQUFsQjtBQUNBLFVBQUlnSCxPQUFPLEdBQUd4TCxNQUFNLENBQUNVLEdBQVAsT0FBaUJ0TSxLQUEvQjs7QUFDQSxVQUFJb1gsT0FBSixFQUFhO0FBQ1p4TCxjQUFNLENBQUNVLEdBQVAsQ0FBV3RNLEtBQVgsRUFBa0IyTSxjQUFsQixDQUFpQyxRQUFqQztBQUNBLGFBQUtpQyxTQUFMLEdBQWlCNU8sS0FBakI7QUFDQTtBQUNELEtBeG9CNEI7O0FBMG9CN0I7Ozs7Ozs7O0FBUUFxWCxZQUFRLEVBQUUsb0JBQVc7QUFDcEIsVUFBSSxLQUFLL0osT0FBTCxLQUFpQnhGLFVBQWpCLElBQStCLEtBQUs4RCxNQUFMLENBQVljLElBQVosQ0FBaUIsVUFBakIsQ0FBbkMsRUFBaUU7QUFDaEUsZUFBTyxLQUFLbFAsS0FBWjtBQUNBLE9BRkQsTUFFTztBQUNOLGVBQU8sS0FBS0EsS0FBTCxDQUFXOFQsSUFBWCxDQUFnQixLQUFLN1QsUUFBTCxDQUFjK1QsU0FBOUIsQ0FBUDtBQUNBO0FBQ0QsS0F4cEI0Qjs7QUEwcEI3Qjs7Ozs7QUFLQTZCLFlBQVEsRUFBRSxrQkFBU3JULEtBQVQsRUFBZ0JzWCxNQUFoQixFQUF3QjtBQUNqQyxVQUFJQyxNQUFNLEdBQUdELE1BQU0sR0FBRyxFQUFILEdBQVEsQ0FBQyxRQUFELENBQTNCO0FBRUFsTyxxQkFBZSxDQUFDLElBQUQsRUFBT21PLE1BQVAsRUFBZSxZQUFXO0FBQ3hDLGFBQUtDLEtBQUwsQ0FBV0YsTUFBWDtBQUNBLGFBQUtHLFFBQUwsQ0FBY3pYLEtBQWQsRUFBcUJzWCxNQUFyQjtBQUNBLE9BSGMsQ0FBZjtBQUlBLEtBdHFCNEI7O0FBd3FCN0I7Ozs7OztBQU1BMUMsaUJBQWEsRUFBRSx1QkFBUzhDLEtBQVQsRUFBZ0IvTixDQUFoQixFQUFtQjtBQUNqQyxVQUFJaEssSUFBSSxHQUFHLElBQVg7QUFDQSxVQUFJZ1ksU0FBSjtBQUNBLFVBQUl6WixDQUFKLEVBQU8wWixHQUFQLEVBQVlDLEtBQVosRUFBbUJDLEdBQW5CLEVBQXdCelYsSUFBeEIsRUFBOEIwVixJQUE5QjtBQUNBLFVBQUlDLEtBQUo7QUFFQSxVQUFJclksSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVMsSUFBZCxLQUF1QixRQUEzQixFQUFxQztBQUNyQ2dJLFdBQUssR0FBRzVVLENBQUMsQ0FBQzRVLEtBQUQsQ0FBVCxDQVBpQyxDQVNqQzs7QUFDQSxVQUFJLENBQUNBLEtBQUssQ0FBQ3paLE1BQVgsRUFBbUI7QUFDbEI2RSxTQUFDLENBQUNuRCxJQUFJLENBQUNzUCxZQUFOLENBQUQsQ0FBcUJrSSxXQUFyQixDQUFpQyxRQUFqQztBQUNBeFgsWUFBSSxDQUFDc1AsWUFBTCxHQUFvQixFQUFwQjs7QUFDQSxZQUFJdFAsSUFBSSxDQUFDc08sU0FBVCxFQUFvQjtBQUNuQnRPLGNBQUksQ0FBQ29XLFNBQUw7QUFDQTs7QUFDRDtBQUNBLE9BakJnQyxDQW1CakM7OztBQUNBNEIsZUFBUyxHQUFHaE8sQ0FBQyxJQUFJQSxDQUFDLENBQUNMLElBQUYsQ0FBT3RMLFdBQVAsRUFBakI7O0FBRUEsVUFBSTJaLFNBQVMsS0FBSyxXQUFkLElBQTZCaFksSUFBSSxDQUFDeU8sV0FBbEMsSUFBaUR6TyxJQUFJLENBQUNzUCxZQUFMLENBQWtCaFIsTUFBdkUsRUFBK0U7QUFDOUUrWixhQUFLLEdBQUdyWSxJQUFJLENBQUN3USxRQUFMLENBQWMrQyxRQUFkLENBQXVCLGNBQXZCLENBQVI7QUFDQTJFLGFBQUssR0FBR3pZLEtBQUssQ0FBQ3pCLFNBQU4sQ0FBZ0I0SSxPQUFoQixDQUF3QmhILEtBQXhCLENBQThCSSxJQUFJLENBQUN3USxRQUFMLENBQWMsQ0FBZCxFQUFpQnRLLFVBQS9DLEVBQTJELENBQUNtUyxLQUFLLENBQUMsQ0FBRCxDQUFOLENBQTNELENBQVI7QUFDQUYsV0FBRyxHQUFLMVksS0FBSyxDQUFDekIsU0FBTixDQUFnQjRJLE9BQWhCLENBQXdCaEgsS0FBeEIsQ0FBOEJJLElBQUksQ0FBQ3dRLFFBQUwsQ0FBYyxDQUFkLEVBQWlCdEssVUFBL0MsRUFBMkQsQ0FBQzZSLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FBM0QsQ0FBUjs7QUFDQSxZQUFJRyxLQUFLLEdBQUdDLEdBQVosRUFBaUI7QUFDaEJDLGNBQUksR0FBSUYsS0FBUjtBQUNBQSxlQUFLLEdBQUdDLEdBQVI7QUFDQUEsYUFBRyxHQUFLQyxJQUFSO0FBQ0E7O0FBQ0QsYUFBSzdaLENBQUMsR0FBRzJaLEtBQVQsRUFBZ0IzWixDQUFDLElBQUk0WixHQUFyQixFQUEwQjVaLENBQUMsRUFBM0IsRUFBK0I7QUFDOUJtRSxjQUFJLEdBQUcxQyxJQUFJLENBQUN3USxRQUFMLENBQWMsQ0FBZCxFQUFpQnRLLFVBQWpCLENBQTRCM0gsQ0FBNUIsQ0FBUDs7QUFDQSxjQUFJeUIsSUFBSSxDQUFDc1AsWUFBTCxDQUFrQjFJLE9BQWxCLENBQTBCbEUsSUFBMUIsTUFBb0MsQ0FBQyxDQUF6QyxFQUE0QztBQUMzQ1MsYUFBQyxDQUFDVCxJQUFELENBQUQsQ0FBUXdPLFFBQVIsQ0FBaUIsUUFBakI7QUFDQWxSLGdCQUFJLENBQUNzUCxZQUFMLENBQWtCblEsSUFBbEIsQ0FBdUJ1RCxJQUF2QjtBQUNBO0FBQ0Q7O0FBQ0RzSCxTQUFDLENBQUMySixjQUFGO0FBQ0EsT0FqQkQsTUFpQk8sSUFBS3FFLFNBQVMsS0FBSyxXQUFkLElBQTZCaFksSUFBSSxDQUFDMk8sVUFBbkMsSUFBbURxSixTQUFTLEtBQUssU0FBZCxJQUEyQixLQUFLdkosV0FBdkYsRUFBcUc7QUFDM0csWUFBSXNKLEtBQUssQ0FBQ2pCLFFBQU4sQ0FBZSxRQUFmLENBQUosRUFBOEI7QUFDN0JtQixhQUFHLEdBQUdqWSxJQUFJLENBQUNzUCxZQUFMLENBQWtCMUksT0FBbEIsQ0FBMEJtUixLQUFLLENBQUMsQ0FBRCxDQUEvQixDQUFOO0FBQ0EvWCxjQUFJLENBQUNzUCxZQUFMLENBQWtCMU4sTUFBbEIsQ0FBeUJxVyxHQUF6QixFQUE4QixDQUE5QjtBQUNBRixlQUFLLENBQUNQLFdBQU4sQ0FBa0IsUUFBbEI7QUFDQSxTQUpELE1BSU87QUFDTnhYLGNBQUksQ0FBQ3NQLFlBQUwsQ0FBa0JuUSxJQUFsQixDQUF1QjRZLEtBQUssQ0FBQzdHLFFBQU4sQ0FBZSxRQUFmLEVBQXlCLENBQXpCLENBQXZCO0FBQ0E7QUFDRCxPQVJNLE1BUUE7QUFDTi9OLFNBQUMsQ0FBQ25ELElBQUksQ0FBQ3NQLFlBQU4sQ0FBRCxDQUFxQmtJLFdBQXJCLENBQWlDLFFBQWpDO0FBQ0F4WCxZQUFJLENBQUNzUCxZQUFMLEdBQW9CLENBQUN5SSxLQUFLLENBQUM3RyxRQUFOLENBQWUsUUFBZixFQUF5QixDQUF6QixDQUFELENBQXBCO0FBQ0EsT0FsRGdDLENBb0RqQzs7O0FBQ0FsUixVQUFJLENBQUNzWSxTQUFMOztBQUNBLFVBQUksQ0FBQyxLQUFLaEssU0FBVixFQUFxQjtBQUNwQnRPLFlBQUksQ0FBQzBLLEtBQUw7QUFDQTtBQUNELEtBdnVCNEI7O0FBeXVCN0I7Ozs7Ozs7O0FBUUFrTCxtQkFBZSxFQUFFLHlCQUFTaUIsT0FBVCxFQUFrQjBCLE1BQWxCLEVBQTBCQyxPQUExQixFQUFtQztBQUNuRCxVQUFJQyxXQUFKLEVBQWlCQyxXQUFqQixFQUE4QkMsQ0FBOUI7QUFDQSxVQUFJQyxVQUFKLEVBQWdCQyxhQUFoQjtBQUNBLFVBQUk3WSxJQUFJLEdBQUcsSUFBWDtBQUVBLFVBQUlBLElBQUksQ0FBQ3FQLGFBQVQsRUFBd0JyUCxJQUFJLENBQUNxUCxhQUFMLENBQW1CbUksV0FBbkIsQ0FBK0IsUUFBL0I7QUFDeEJ4WCxVQUFJLENBQUNxUCxhQUFMLEdBQXFCLElBQXJCO0FBRUF3SCxhQUFPLEdBQUcxVCxDQUFDLENBQUMwVCxPQUFELENBQVg7QUFDQSxVQUFJLENBQUNBLE9BQU8sQ0FBQ3ZZLE1BQWIsRUFBcUI7QUFFckIwQixVQUFJLENBQUNxUCxhQUFMLEdBQXFCd0gsT0FBTyxDQUFDM0YsUUFBUixDQUFpQixRQUFqQixDQUFyQjs7QUFFQSxVQUFJcUgsTUFBTSxJQUFJLENBQUMvUCxLQUFLLENBQUMrUCxNQUFELENBQXBCLEVBQThCO0FBRTdCRSxtQkFBVyxHQUFLelksSUFBSSxDQUFDMlEsaUJBQUwsQ0FBdUJtSSxNQUF2QixFQUFoQjtBQUNBSixtQkFBVyxHQUFLMVksSUFBSSxDQUFDcVAsYUFBTCxDQUFtQjBKLFdBQW5CLENBQStCLElBQS9CLENBQWhCO0FBQ0FSLGNBQU0sR0FBVXZZLElBQUksQ0FBQzJRLGlCQUFMLENBQXVCcUksU0FBdkIsTUFBc0MsQ0FBdEQ7QUFDQUwsU0FBQyxHQUFlM1ksSUFBSSxDQUFDcVAsYUFBTCxDQUFtQjRKLE1BQW5CLEdBQTRCeE4sR0FBNUIsR0FBa0N6TCxJQUFJLENBQUMyUSxpQkFBTCxDQUF1QnNJLE1BQXZCLEdBQWdDeE4sR0FBbEUsR0FBd0U4TSxNQUF4RjtBQUNBSyxrQkFBVSxHQUFNRCxDQUFoQjtBQUNBRSxxQkFBYSxHQUFHRixDQUFDLEdBQUdGLFdBQUosR0FBa0JDLFdBQWxDOztBQUVBLFlBQUlDLENBQUMsR0FBR0QsV0FBSixHQUFrQkQsV0FBVyxHQUFHRixNQUFwQyxFQUE0QztBQUMzQ3ZZLGNBQUksQ0FBQzJRLGlCQUFMLENBQXVCdUksSUFBdkIsR0FBOEJWLE9BQTlCLENBQXNDO0FBQUNRLHFCQUFTLEVBQUVIO0FBQVosV0FBdEMsRUFBa0VMLE9BQU8sR0FBR3hZLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY3FiLGNBQWpCLEdBQWtDLENBQTNHO0FBQ0EsU0FGRCxNQUVPLElBQUlSLENBQUMsR0FBR0osTUFBUixFQUFnQjtBQUN0QnZZLGNBQUksQ0FBQzJRLGlCQUFMLENBQXVCdUksSUFBdkIsR0FBOEJWLE9BQTlCLENBQXNDO0FBQUNRLHFCQUFTLEVBQUVKO0FBQVosV0FBdEMsRUFBK0RKLE9BQU8sR0FBR3hZLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY3FiLGNBQWpCLEdBQWtDLENBQXhHO0FBQ0E7QUFFRDtBQUNELEtBOXdCNEI7O0FBZ3hCN0I7OztBQUdBM0QsYUFBUyxFQUFFLHFCQUFXO0FBQ3JCLFVBQUl4VixJQUFJLEdBQUcsSUFBWDtBQUNBLFVBQUlBLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY2lTLElBQWQsS0FBdUIsUUFBM0IsRUFBcUM7QUFFckMvUCxVQUFJLENBQUNzUCxZQUFMLEdBQW9CN1AsS0FBSyxDQUFDekIsU0FBTixDQUFnQjZFLEtBQWhCLENBQXNCakQsS0FBdEIsQ0FBNEJJLElBQUksQ0FBQ3dRLFFBQUwsQ0FBYytDLFFBQWQsQ0FBdUIsYUFBdkIsRUFBc0NyQyxRQUF0QyxDQUErQyxRQUEvQyxDQUE1QixDQUFwQjs7QUFDQSxVQUFJbFIsSUFBSSxDQUFDc1AsWUFBTCxDQUFrQmhSLE1BQXRCLEVBQThCO0FBQzdCMEIsWUFBSSxDQUFDc1ksU0FBTDtBQUNBdFksWUFBSSxDQUFDK1UsS0FBTDtBQUNBOztBQUNEL1UsVUFBSSxDQUFDMEssS0FBTDtBQUNBLEtBN3hCNEI7O0FBK3hCN0I7Ozs7QUFJQTROLGFBQVMsRUFBRSxxQkFBVztBQUNyQixVQUFJdFksSUFBSSxHQUFHLElBQVg7QUFFQUEsVUFBSSxDQUFDeVcsZUFBTCxDQUFxQixFQUFyQjtBQUNBelcsVUFBSSxDQUFDeVEsY0FBTCxDQUFvQnBGLEdBQXBCLENBQXdCO0FBQUMrTixlQUFPLEVBQUUsQ0FBVjtBQUFhNU4sZ0JBQVEsRUFBRSxVQUF2QjtBQUFtQ0UsWUFBSSxFQUFFMUwsSUFBSSxDQUFDNE4sR0FBTCxHQUFXLEtBQVgsR0FBbUIsQ0FBQztBQUE3RCxPQUF4QjtBQUNBNU4sVUFBSSxDQUFDdU8sYUFBTCxHQUFxQixJQUFyQjtBQUNBLEtBenlCNEI7O0FBMnlCN0I7OztBQUdBNkgsYUFBUyxFQUFFLHFCQUFXO0FBQ3JCLFdBQUszRixjQUFMLENBQW9CcEYsR0FBcEIsQ0FBd0I7QUFBQytOLGVBQU8sRUFBRSxDQUFWO0FBQWE1TixnQkFBUSxFQUFFLFVBQXZCO0FBQW1DRSxZQUFJLEVBQUU7QUFBekMsT0FBeEI7QUFDQSxXQUFLNkMsYUFBTCxHQUFxQixLQUFyQjtBQUNBLEtBanpCNEI7O0FBbXpCN0I7OztBQUdBN0QsU0FBSyxFQUFFLGlCQUFXO0FBQ2pCLFVBQUkxSyxJQUFJLEdBQUcsSUFBWDtBQUNBLFVBQUlBLElBQUksQ0FBQ2lPLFVBQVQsRUFBcUI7QUFFckJqTyxVQUFJLENBQUM0TyxXQUFMLEdBQW1CLElBQW5CO0FBQ0E1TyxVQUFJLENBQUN5USxjQUFMLENBQW9CLENBQXBCLEVBQXVCL0YsS0FBdkI7QUFDQXBDLFlBQU0sQ0FBQ2tCLFVBQVAsQ0FBa0IsWUFBVztBQUM1QnhKLFlBQUksQ0FBQzRPLFdBQUwsR0FBbUIsS0FBbkI7QUFDQTVPLFlBQUksQ0FBQ2lULE9BQUw7QUFDQSxPQUhELEVBR0csQ0FISDtBQUlBLEtBaDBCNEI7O0FBazBCN0I7Ozs7O0FBS0FGLFFBQUksRUFBRSxjQUFTdUQsSUFBVCxFQUFlO0FBQ3BCLFdBQUs3RixjQUFMLENBQW9CLENBQXBCLEVBQXVCc0MsSUFBdkI7QUFDQSxXQUFLQyxNQUFMLENBQVksSUFBWixFQUFrQnNELElBQWxCO0FBQ0EsS0ExMEI0Qjs7QUE0MEI3Qjs7Ozs7Ozs7O0FBU0F6VyxvQkFBZ0IsRUFBRSwwQkFBUzNCLEtBQVQsRUFBZ0I7QUFDakMsYUFBTyxLQUFLMFIsTUFBTCxDQUFZL1AsZ0JBQVosQ0FBNkIzQixLQUE3QixFQUFvQyxLQUFLbWIsZ0JBQUwsRUFBcEMsQ0FBUDtBQUNBLEtBdjFCNEI7O0FBeTFCN0I7Ozs7Ozs7QUFPQUEsb0JBQWdCLEVBQUUsNEJBQVc7QUFDNUIsVUFBSXZiLFFBQVEsR0FBRyxLQUFLQSxRQUFwQjtBQUNBLFVBQUl1RCxJQUFJLEdBQUd2RCxRQUFRLENBQUN3YixTQUFwQjs7QUFDQSxVQUFJLE9BQU9qWSxJQUFQLEtBQWdCLFFBQXBCLEVBQThCO0FBQzdCQSxZQUFJLEdBQUcsQ0FBQztBQUFDTixlQUFLLEVBQUVNO0FBQVIsU0FBRCxDQUFQO0FBQ0E7O0FBRUQsYUFBTztBQUNOcEIsY0FBTSxFQUFRbkMsUUFBUSxDQUFDeWIsV0FEakI7QUFFTjFZLG1CQUFXLEVBQUcvQyxRQUFRLENBQUMwYixpQkFGakI7QUFHTm5ZLFlBQUksRUFBVUE7QUFIUixPQUFQO0FBS0EsS0E1MkI0Qjs7QUE4MkI3Qjs7Ozs7Ozs7Ozs7Ozs7QUFjQXZCLFVBQU0sRUFBRSxnQkFBUzVCLEtBQVQsRUFBZ0I7QUFDdkIsVUFBSUssQ0FBSixFQUFPOEIsS0FBUCxFQUFjRSxLQUFkLEVBQXFCaUIsTUFBckIsRUFBNkJlLGNBQTdCO0FBQ0EsVUFBSXZDLElBQUksR0FBTyxJQUFmO0FBQ0EsVUFBSWxDLFFBQVEsR0FBR2tDLElBQUksQ0FBQ2xDLFFBQXBCO0FBQ0EsVUFBSWlDLE9BQU8sR0FBSSxLQUFLc1osZ0JBQUwsRUFBZixDQUp1QixDQU12Qjs7QUFDQSxVQUFJdmIsUUFBUSxDQUFDeUMsS0FBYixFQUFvQjtBQUNuQmdDLHNCQUFjLEdBQUd2QyxJQUFJLENBQUNsQyxRQUFMLENBQWN5QyxLQUFkLENBQW9CWCxLQUFwQixDQUEwQixJQUExQixFQUFnQyxDQUFDMUIsS0FBRCxDQUFoQyxDQUFqQjs7QUFDQSxZQUFJLE9BQU9xRSxjQUFQLEtBQTBCLFVBQTlCLEVBQTBDO0FBQ3pDLGdCQUFNLElBQUlxQyxLQUFKLENBQVUsc0VBQVYsQ0FBTjtBQUNBO0FBQ0QsT0Fac0IsQ0FjdkI7OztBQUNBLFVBQUkxRyxLQUFLLEtBQUs4QixJQUFJLENBQUNnWCxTQUFuQixFQUE4QjtBQUM3QmhYLFlBQUksQ0FBQ2dYLFNBQUwsR0FBaUI5WSxLQUFqQjtBQUNBc0QsY0FBTSxHQUFHeEIsSUFBSSxDQUFDNFAsTUFBTCxDQUFZOVAsTUFBWixDQUFtQjVCLEtBQW5CLEVBQTBCaUYsQ0FBQyxDQUFDakIsTUFBRixDQUFTbkMsT0FBVCxFQUFrQjtBQUFDUSxlQUFLLEVBQUVnQztBQUFSLFNBQWxCLENBQTFCLENBQVQ7QUFDQXZDLFlBQUksQ0FBQ2dQLGNBQUwsR0FBc0J4TixNQUF0QjtBQUNBLE9BSkQsTUFJTztBQUNOQSxjQUFNLEdBQUcyQixDQUFDLENBQUNqQixNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUJsQyxJQUFJLENBQUNnUCxjQUF4QixDQUFUO0FBQ0EsT0FyQnNCLENBdUJ2Qjs7O0FBQ0EsVUFBSWxSLFFBQVEsQ0FBQ21TLFlBQWIsRUFBMkI7QUFDMUIsYUFBSzFSLENBQUMsR0FBR2lELE1BQU0sQ0FBQzNELEtBQVAsQ0FBYVMsTUFBYixHQUFzQixDQUEvQixFQUFrQ0MsQ0FBQyxJQUFJLENBQXZDLEVBQTBDQSxDQUFDLEVBQTNDLEVBQStDO0FBQzlDLGNBQUl5QixJQUFJLENBQUNuQyxLQUFMLENBQVcrSSxPQUFYLENBQW1CNkIsUUFBUSxDQUFDakgsTUFBTSxDQUFDM0QsS0FBUCxDQUFhVSxDQUFiLEVBQWdCa0QsRUFBakIsQ0FBM0IsTUFBcUQsQ0FBQyxDQUExRCxFQUE2RDtBQUM1REQsa0JBQU0sQ0FBQzNELEtBQVAsQ0FBYStELE1BQWIsQ0FBb0JyRCxDQUFwQixFQUF1QixDQUF2QjtBQUNBO0FBQ0Q7QUFDRDs7QUFFRCxhQUFPaUQsTUFBUDtBQUNBLEtBNzVCNEI7O0FBKzVCN0I7Ozs7OztBQU1BeVUsa0JBQWMsRUFBRSx3QkFBU3dELGVBQVQsRUFBMEI7QUFDekMsVUFBSWxiLENBQUosRUFBT21iLENBQVAsRUFBVTNXLENBQVYsRUFBYXZFLENBQWIsRUFBZ0JtYixNQUFoQixFQUF3QkMsWUFBeEIsRUFBc0NDLE1BQXRDLEVBQThDQyxXQUE5QyxFQUEyREMsUUFBM0QsRUFBcUV4SyxTQUFyRSxFQUFnRmlGLElBQWhGLEVBQXNGd0YsYUFBdEYsRUFBcUdDLGlCQUFyRztBQUNBLFVBQUlDLE9BQUosRUFBYUMsY0FBYixFQUE2QkMsT0FBN0I7O0FBRUEsVUFBSSxPQUFPWCxlQUFQLEtBQTJCLFdBQS9CLEVBQTRDO0FBQzNDQSx1QkFBZSxHQUFHLElBQWxCO0FBQ0E7O0FBRUQsVUFBSXpaLElBQUksR0FBZ0IsSUFBeEI7QUFDQSxVQUFJOUIsS0FBSyxHQUFlaUYsQ0FBQyxDQUFDaEYsSUFBRixDQUFPNkIsSUFBSSxDQUFDeVEsY0FBTCxDQUFvQjlELEdBQXBCLEVBQVAsQ0FBeEI7QUFDQSxVQUFJeUssT0FBTyxHQUFhcFgsSUFBSSxDQUFDRixNQUFMLENBQVk1QixLQUFaLENBQXhCO0FBQ0EsVUFBSXlTLGlCQUFpQixHQUFHM1EsSUFBSSxDQUFDMlEsaUJBQTdCO0FBQ0EsVUFBSTBKLGFBQWEsR0FBT3JhLElBQUksQ0FBQ3FQLGFBQUwsSUFBc0I1RyxRQUFRLENBQUN6SSxJQUFJLENBQUNxUCxhQUFMLENBQW1CdEMsSUFBbkIsQ0FBd0IsWUFBeEIsQ0FBRCxDQUF0RCxDQVp5QyxDQWN6Qzs7QUFDQXZPLE9BQUMsR0FBRzRZLE9BQU8sQ0FBQ3ZaLEtBQVIsQ0FBY1MsTUFBbEI7O0FBQ0EsVUFBSSxPQUFPMEIsSUFBSSxDQUFDbEMsUUFBTCxDQUFjd2MsVUFBckIsS0FBb0MsUUFBeEMsRUFBa0Q7QUFDakQ5YixTQUFDLEdBQUc2WSxJQUFJLENBQUNrRCxHQUFMLENBQVMvYixDQUFULEVBQVl3QixJQUFJLENBQUNsQyxRQUFMLENBQWN3YyxVQUExQixDQUFKO0FBQ0EsT0FsQndDLENBb0J6Qzs7O0FBQ0FYLFlBQU0sR0FBRyxFQUFUO0FBQ0FDLGtCQUFZLEdBQUcsRUFBZjs7QUFFQSxXQUFLcmIsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHQyxDQUFoQixFQUFtQkQsQ0FBQyxFQUFwQixFQUF3QjtBQUN2QnNiLGNBQU0sR0FBUTdaLElBQUksQ0FBQ0QsT0FBTCxDQUFhcVgsT0FBTyxDQUFDdlosS0FBUixDQUFjVSxDQUFkLEVBQWlCa0QsRUFBOUIsQ0FBZDtBQUNBcVksbUJBQVcsR0FBRzlaLElBQUksQ0FBQzBVLE1BQUwsQ0FBWSxRQUFaLEVBQXNCbUYsTUFBdEIsQ0FBZDtBQUNBRSxnQkFBUSxHQUFNRixNQUFNLENBQUM3WixJQUFJLENBQUNsQyxRQUFMLENBQWMwYyxhQUFmLENBQU4sSUFBdUMsRUFBckQ7QUFDQWpMLGlCQUFTLEdBQUtwTSxDQUFDLENBQUNELE9BQUYsQ0FBVTZXLFFBQVYsSUFBc0JBLFFBQXRCLEdBQWlDLENBQUNBLFFBQUQsQ0FBL0M7O0FBRUEsYUFBS0wsQ0FBQyxHQUFHLENBQUosRUFBTzNXLENBQUMsR0FBR3dNLFNBQVMsSUFBSUEsU0FBUyxDQUFDalIsTUFBdkMsRUFBK0NvYixDQUFDLEdBQUczVyxDQUFuRCxFQUFzRDJXLENBQUMsRUFBdkQsRUFBMkQ7QUFDMURLLGtCQUFRLEdBQUd4SyxTQUFTLENBQUNtSyxDQUFELENBQXBCOztBQUNBLGNBQUksQ0FBQzFaLElBQUksQ0FBQ3VQLFNBQUwsQ0FBZXZRLGNBQWYsQ0FBOEIrYSxRQUE5QixDQUFMLEVBQThDO0FBQzdDQSxvQkFBUSxHQUFHLEVBQVg7QUFDQTs7QUFDRCxjQUFJLENBQUNKLE1BQU0sQ0FBQzNhLGNBQVAsQ0FBc0IrYSxRQUF0QixDQUFMLEVBQXNDO0FBQ3JDSixrQkFBTSxDQUFDSSxRQUFELENBQU4sR0FBbUIsRUFBbkI7QUFDQUgsd0JBQVksQ0FBQ3phLElBQWIsQ0FBa0I0YSxRQUFsQjtBQUNBOztBQUNESixnQkFBTSxDQUFDSSxRQUFELENBQU4sQ0FBaUI1YSxJQUFqQixDQUFzQjJhLFdBQXRCO0FBQ0E7QUFDRCxPQXpDd0MsQ0EyQ3pDOzs7QUFDQSxVQUFJLEtBQUtoYyxRQUFMLENBQWMyYyxpQkFBbEIsRUFBcUM7QUFDcENiLG9CQUFZLENBQUN2WSxJQUFiLENBQWtCLFVBQVNRLENBQVQsRUFBWUMsQ0FBWixFQUFlO0FBQ2hDLGNBQUk0WSxPQUFPLEdBQUcxYSxJQUFJLENBQUN1UCxTQUFMLENBQWUxTixDQUFmLEVBQWtCOFksTUFBbEIsSUFBNEIsQ0FBMUM7QUFDQSxjQUFJQyxPQUFPLEdBQUc1YSxJQUFJLENBQUN1UCxTQUFMLENBQWV6TixDQUFmLEVBQWtCNlksTUFBbEIsSUFBNEIsQ0FBMUM7QUFDQSxpQkFBT0QsT0FBTyxHQUFHRSxPQUFqQjtBQUNBLFNBSkQ7QUFLQSxPQWxEd0MsQ0FvRHpDOzs7QUFDQXBHLFVBQUksR0FBRyxFQUFQOztBQUNBLFdBQUtqVyxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUdvYixZQUFZLENBQUN0YixNQUE3QixFQUFxQ0MsQ0FBQyxHQUFHQyxDQUF6QyxFQUE0Q0QsQ0FBQyxFQUE3QyxFQUFpRDtBQUNoRHdiLGdCQUFRLEdBQUdILFlBQVksQ0FBQ3JiLENBQUQsQ0FBdkI7O0FBQ0EsWUFBSXlCLElBQUksQ0FBQ3VQLFNBQUwsQ0FBZXZRLGNBQWYsQ0FBOEIrYSxRQUE5QixLQUEyQ0osTUFBTSxDQUFDSSxRQUFELENBQU4sQ0FBaUJ6YixNQUFoRSxFQUF3RTtBQUN2RTtBQUNBO0FBQ0EwYix1QkFBYSxHQUFHaGEsSUFBSSxDQUFDMFUsTUFBTCxDQUFZLGlCQUFaLEVBQStCMVUsSUFBSSxDQUFDdVAsU0FBTCxDQUFld0ssUUFBZixDQUEvQixLQUE0RCxFQUE1RTtBQUNBQyx1QkFBYSxJQUFJTCxNQUFNLENBQUNJLFFBQUQsQ0FBTixDQUFpQnBJLElBQWpCLENBQXNCLEVBQXRCLENBQWpCO0FBQ0E2QyxjQUFJLENBQUNyVixJQUFMLENBQVVhLElBQUksQ0FBQzBVLE1BQUwsQ0FBWSxVQUFaLEVBQXdCdlIsQ0FBQyxDQUFDakIsTUFBRixDQUFTLEVBQVQsRUFBYWxDLElBQUksQ0FBQ3VQLFNBQUwsQ0FBZXdLLFFBQWYsQ0FBYixFQUF1QztBQUN4RXZGLGdCQUFJLEVBQUV3RjtBQURrRSxXQUF2QyxDQUF4QixDQUFWO0FBR0EsU0FSRCxNQVFPO0FBQ054RixjQUFJLENBQUNyVixJQUFMLENBQVV3YSxNQUFNLENBQUNJLFFBQUQsQ0FBTixDQUFpQnBJLElBQWpCLENBQXNCLEVBQXRCLENBQVY7QUFDQTtBQUNEOztBQUVEaEIsdUJBQWlCLENBQUM2RCxJQUFsQixDQUF1QkEsSUFBSSxDQUFDN0MsSUFBTCxDQUFVLEVBQVYsQ0FBdkIsRUFyRXlDLENBdUV6Qzs7QUFDQSxVQUFJM1IsSUFBSSxDQUFDbEMsUUFBTCxDQUFjaUgsU0FBZCxJQUEyQnFTLE9BQU8sQ0FBQ2xaLEtBQVIsQ0FBY0ksTUFBekMsSUFBbUQ4WSxPQUFPLENBQUN6WSxNQUFSLENBQWVMLE1BQXRFLEVBQThFO0FBQzdFLGFBQUtDLENBQUMsR0FBRyxDQUFKLEVBQU9DLENBQUMsR0FBRzRZLE9BQU8sQ0FBQ3pZLE1BQVIsQ0FBZUwsTUFBL0IsRUFBdUNDLENBQUMsR0FBR0MsQ0FBM0MsRUFBOENELENBQUMsRUFBL0MsRUFBbUQ7QUFDbER3RyxtQkFBUyxDQUFDNEwsaUJBQUQsRUFBb0J5RyxPQUFPLENBQUN6WSxNQUFSLENBQWVKLENBQWYsRUFBa0JFLEtBQXRDLENBQVQ7QUFDQTtBQUNELE9BNUV3QyxDQThFekM7OztBQUNBLFVBQUksQ0FBQ3VCLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY21TLFlBQW5CLEVBQWlDO0FBQ2hDLGFBQUsxUixDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUd3QixJQUFJLENBQUNuQyxLQUFMLENBQVdTLE1BQTNCLEVBQW1DQyxDQUFDLEdBQUdDLENBQXZDLEVBQTBDRCxDQUFDLEVBQTNDLEVBQStDO0FBQzlDeUIsY0FBSSxDQUFDa1gsU0FBTCxDQUFlbFgsSUFBSSxDQUFDbkMsS0FBTCxDQUFXVSxDQUFYLENBQWYsRUFBOEIyUyxRQUE5QixDQUF1QyxVQUF2QztBQUNBO0FBQ0QsT0FuRndDLENBcUZ6Qzs7O0FBQ0ErSSx1QkFBaUIsR0FBR2phLElBQUksQ0FBQzZhLFNBQUwsQ0FBZTNjLEtBQWYsQ0FBcEI7O0FBQ0EsVUFBSStiLGlCQUFKLEVBQXVCO0FBQ3RCdEoseUJBQWlCLENBQUNtSyxPQUFsQixDQUEwQjlhLElBQUksQ0FBQzBVLE1BQUwsQ0FBWSxlQUFaLEVBQTZCO0FBQUNySyxlQUFLLEVBQUVuTTtBQUFSLFNBQTdCLENBQTFCO0FBQ0FrYyxlQUFPLEdBQUdqWCxDQUFDLENBQUN3TixpQkFBaUIsQ0FBQyxDQUFELENBQWpCLENBQXFCekssVUFBckIsQ0FBZ0MsQ0FBaEMsQ0FBRCxDQUFYO0FBQ0EsT0ExRndDLENBNEZ6Qzs7O0FBQ0FsRyxVQUFJLENBQUMrTyxVQUFMLEdBQWtCcUksT0FBTyxDQUFDdlosS0FBUixDQUFjUyxNQUFkLEdBQXVCLENBQXZCLElBQTRCMmIsaUJBQTlDOztBQUNBLFVBQUlqYSxJQUFJLENBQUMrTyxVQUFULEVBQXFCO0FBQ3BCLFlBQUlxSSxPQUFPLENBQUN2WixLQUFSLENBQWNTLE1BQWQsR0FBdUIsQ0FBM0IsRUFBOEI7QUFDN0I2Yix3QkFBYyxHQUFHRSxhQUFhLElBQUlyYSxJQUFJLENBQUNrWCxTQUFMLENBQWVtRCxhQUFmLENBQWxDOztBQUNBLGNBQUlGLGNBQWMsSUFBSUEsY0FBYyxDQUFDN2IsTUFBckMsRUFBNkM7QUFDNUM0YixtQkFBTyxHQUFHQyxjQUFWO0FBQ0EsV0FGRCxNQUVPLElBQUluYSxJQUFJLENBQUNsQyxRQUFMLENBQWNpUyxJQUFkLEtBQXVCLFFBQXZCLElBQW1DL1AsSUFBSSxDQUFDbkMsS0FBTCxDQUFXUyxNQUFsRCxFQUEwRDtBQUNoRTRiLG1CQUFPLEdBQUdsYSxJQUFJLENBQUNrWCxTQUFMLENBQWVsWCxJQUFJLENBQUNuQyxLQUFMLENBQVcsQ0FBWCxDQUFmLENBQVY7QUFDQTs7QUFDRCxjQUFJLENBQUNxYyxPQUFELElBQVksQ0FBQ0EsT0FBTyxDQUFDNWIsTUFBekIsRUFBaUM7QUFDaEMsZ0JBQUk4YixPQUFPLElBQUksQ0FBQ3BhLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY2lkLGFBQTlCLEVBQTZDO0FBQzVDYixxQkFBTyxHQUFHbGEsSUFBSSxDQUFDMlYsaUJBQUwsQ0FBdUJ5RSxPQUF2QixFQUFnQyxDQUFoQyxDQUFWO0FBQ0EsYUFGRCxNQUVPO0FBQ05GLHFCQUFPLEdBQUd2SixpQkFBaUIsQ0FBQ3FLLElBQWxCLENBQXVCLHlCQUF2QixDQUFWO0FBQ0E7QUFDRDtBQUNELFNBZEQsTUFjTztBQUNOZCxpQkFBTyxHQUFHRSxPQUFWO0FBQ0E7O0FBQ0RwYSxZQUFJLENBQUM0VixlQUFMLENBQXFCc0UsT0FBckI7O0FBQ0EsWUFBSVQsZUFBZSxJQUFJLENBQUN6WixJQUFJLENBQUNnTyxNQUE3QixFQUFxQztBQUFFaE8sY0FBSSxDQUFDZ1YsSUFBTDtBQUFjO0FBQ3JELE9BcEJELE1Bb0JPO0FBQ05oVixZQUFJLENBQUM0VixlQUFMLENBQXFCLElBQXJCOztBQUNBLFlBQUk2RCxlQUFlLElBQUl6WixJQUFJLENBQUNnTyxNQUE1QixFQUFvQztBQUFFaE8sY0FBSSxDQUFDK1UsS0FBTDtBQUFlO0FBQ3JEO0FBQ0QsS0EzaEM0Qjs7QUE2aEM3Qjs7Ozs7Ozs7Ozs7O0FBWUF3QyxhQUFTLEVBQUUsbUJBQVM1VyxJQUFULEVBQWU7QUFDekIsVUFBSXBDLENBQUo7QUFBQSxVQUFPQyxDQUFQO0FBQUEsVUFBVTZCLEtBQVY7QUFBQSxVQUFpQkwsSUFBSSxHQUFHLElBQXhCOztBQUVBLFVBQUltRCxDQUFDLENBQUNELE9BQUYsQ0FBVXZDLElBQVYsQ0FBSixFQUFxQjtBQUNwQixhQUFLcEMsQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxHQUFHbUMsSUFBSSxDQUFDckMsTUFBckIsRUFBNkJDLENBQUMsR0FBR0MsQ0FBakMsRUFBb0NELENBQUMsRUFBckMsRUFBeUM7QUFDeEN5QixjQUFJLENBQUN1WCxTQUFMLENBQWU1VyxJQUFJLENBQUNwQyxDQUFELENBQW5CO0FBQ0E7O0FBQ0Q7QUFDQTs7QUFFRCxVQUFJOEIsS0FBSyxHQUFHTCxJQUFJLENBQUM2UCxjQUFMLENBQW9CbFAsSUFBcEIsQ0FBWixFQUF1QztBQUN0Q1gsWUFBSSxDQUFDd1AsV0FBTCxDQUFpQm5QLEtBQWpCLElBQTBCLElBQTFCO0FBQ0FMLFlBQUksQ0FBQ2dYLFNBQUwsR0FBaUIsSUFBakI7QUFDQWhYLFlBQUksQ0FBQzZHLE9BQUwsQ0FBYSxZQUFiLEVBQTJCeEcsS0FBM0IsRUFBa0NNLElBQWxDO0FBQ0E7QUFDRCxLQXhqQzRCOztBQTBqQzdCOzs7Ozs7QUFNQWtQLGtCQUFjLEVBQUUsd0JBQVNsUCxJQUFULEVBQWU7QUFDOUIsVUFBSWhCLEdBQUcsR0FBRzhJLFFBQVEsQ0FBQzlILElBQUksQ0FBQyxLQUFLN0MsUUFBTCxDQUFjbWQsVUFBZixDQUFMLENBQWxCO0FBQ0EsVUFBSSxDQUFDdGIsR0FBRCxJQUFRLEtBQUtJLE9BQUwsQ0FBYWYsY0FBYixDQUE0QlcsR0FBNUIsQ0FBWixFQUE4QyxPQUFPLEtBQVA7QUFDOUNnQixVQUFJLENBQUNnYSxNQUFMLEdBQWNoYSxJQUFJLENBQUNnYSxNQUFMLElBQWUsRUFBRSxLQUFLbE4sS0FBcEM7QUFDQSxXQUFLMU4sT0FBTCxDQUFhSixHQUFiLElBQW9CZ0IsSUFBcEI7QUFDQSxhQUFPaEIsR0FBUDtBQUNBLEtBdGtDNEI7O0FBd2tDN0I7Ozs7OztBQU1BbVEsdUJBQW1CLEVBQUUsNkJBQVNuUCxJQUFULEVBQWU7QUFDbkMsVUFBSWhCLEdBQUcsR0FBRzhJLFFBQVEsQ0FBQzlILElBQUksQ0FBQyxLQUFLN0MsUUFBTCxDQUFjb2Qsa0JBQWYsQ0FBTCxDQUFsQjtBQUNBLFVBQUksQ0FBQ3ZiLEdBQUwsRUFBVSxPQUFPLEtBQVA7QUFFVmdCLFVBQUksQ0FBQ2dhLE1BQUwsR0FBY2hhLElBQUksQ0FBQ2dhLE1BQUwsSUFBZSxFQUFFLEtBQUtsTixLQUFwQztBQUNBLFdBQUs4QixTQUFMLENBQWU1UCxHQUFmLElBQXNCZ0IsSUFBdEI7QUFDQSxhQUFPaEIsR0FBUDtBQUNBLEtBcmxDNEI7O0FBdWxDN0I7Ozs7Ozs7QUFPQXdiLGtCQUFjLEVBQUUsd0JBQVMxWixFQUFULEVBQWFkLElBQWIsRUFBbUI7QUFDbENBLFVBQUksQ0FBQyxLQUFLN0MsUUFBTCxDQUFjb2Qsa0JBQWYsQ0FBSixHQUF5Q3paLEVBQXpDOztBQUNBLFVBQUlBLEVBQUUsR0FBRyxLQUFLcU8sbUJBQUwsQ0FBeUJuUCxJQUF6QixDQUFULEVBQXlDO0FBQ3hDLGFBQUtrRyxPQUFMLENBQWEsY0FBYixFQUE2QnBGLEVBQTdCLEVBQWlDZCxJQUFqQztBQUNBO0FBQ0QsS0FubUM0Qjs7QUFxbUM3Qjs7Ozs7QUFLQXlhLHFCQUFpQixFQUFFLDJCQUFTM1osRUFBVCxFQUFhO0FBQy9CLFVBQUksS0FBSzhOLFNBQUwsQ0FBZXZRLGNBQWYsQ0FBOEJ5QyxFQUE5QixDQUFKLEVBQXVDO0FBQ3RDLGVBQU8sS0FBSzhOLFNBQUwsQ0FBZTlOLEVBQWYsQ0FBUDtBQUNBLGFBQUtnTyxXQUFMLEdBQW1CLEVBQW5CO0FBQ0EsYUFBSzVJLE9BQUwsQ0FBYSxpQkFBYixFQUFnQ3BGLEVBQWhDO0FBQ0E7QUFDRCxLQWhuQzRCOztBQWtuQzdCOzs7QUFHQTRaLHFCQUFpQixFQUFFLDZCQUFXO0FBQzdCLFdBQUs5TCxTQUFMLEdBQWlCLEVBQWpCO0FBQ0EsV0FBS0UsV0FBTCxHQUFtQixFQUFuQjtBQUNBLFdBQUs1SSxPQUFMLENBQWEsZ0JBQWI7QUFDQSxLQXpuQzRCOztBQTJuQzdCOzs7Ozs7OztBQVFBeVUsZ0JBQVksRUFBRSxzQkFBU2piLEtBQVQsRUFBZ0JNLElBQWhCLEVBQXNCO0FBQ25DLFVBQUlYLElBQUksR0FBRyxJQUFYO0FBQ0EsVUFBSStYLEtBQUosRUFBV3dELFNBQVg7QUFDQSxVQUFJQyxTQUFKLEVBQWVDLFVBQWYsRUFBMkJDLFdBQTNCLEVBQXdDQyxhQUF4QyxFQUF1REMsU0FBdkQ7QUFFQXZiLFdBQUssR0FBT29JLFFBQVEsQ0FBQ3BJLEtBQUQsQ0FBcEI7QUFDQW1iLGVBQVMsR0FBRy9TLFFBQVEsQ0FBQzlILElBQUksQ0FBQ1gsSUFBSSxDQUFDbEMsUUFBTCxDQUFjbWQsVUFBZixDQUFMLENBQXBCLENBTm1DLENBUW5DOztBQUNBLFVBQUk1YSxLQUFLLEtBQUssSUFBZCxFQUFvQjtBQUNwQixVQUFJLENBQUNMLElBQUksQ0FBQ0QsT0FBTCxDQUFhZixjQUFiLENBQTRCcUIsS0FBNUIsQ0FBTCxFQUF5QztBQUN6QyxVQUFJLE9BQU9tYixTQUFQLEtBQXFCLFFBQXpCLEVBQW1DLE1BQU0sSUFBSTVXLEtBQUosQ0FBVSxrQ0FBVixDQUFOO0FBRW5DZ1gsZUFBUyxHQUFHNWIsSUFBSSxDQUFDRCxPQUFMLENBQWFNLEtBQWIsRUFBb0JzYSxNQUFoQyxDQWJtQyxDQWVuQzs7QUFDQSxVQUFJYSxTQUFTLEtBQUtuYixLQUFsQixFQUF5QjtBQUN4QixlQUFPTCxJQUFJLENBQUNELE9BQUwsQ0FBYU0sS0FBYixDQUFQO0FBQ0FvYixrQkFBVSxHQUFHemIsSUFBSSxDQUFDbkMsS0FBTCxDQUFXK0ksT0FBWCxDQUFtQnZHLEtBQW5CLENBQWI7O0FBQ0EsWUFBSW9iLFVBQVUsS0FBSyxDQUFDLENBQXBCLEVBQXVCO0FBQ3RCemIsY0FBSSxDQUFDbkMsS0FBTCxDQUFXK0QsTUFBWCxDQUFrQjZaLFVBQWxCLEVBQThCLENBQTlCLEVBQWlDRCxTQUFqQztBQUNBO0FBQ0Q7O0FBQ0Q3YSxVQUFJLENBQUNnYSxNQUFMLEdBQWNoYSxJQUFJLENBQUNnYSxNQUFMLElBQWVpQixTQUE3QjtBQUNBNWIsVUFBSSxDQUFDRCxPQUFMLENBQWF5YixTQUFiLElBQTBCN2EsSUFBMUIsQ0F4Qm1DLENBMEJuQzs7QUFDQSthLGlCQUFXLEdBQUcxYixJQUFJLENBQUN5UCxXQUFMLENBQWlCLE1BQWpCLENBQWQ7QUFDQWtNLG1CQUFhLEdBQUczYixJQUFJLENBQUN5UCxXQUFMLENBQWlCLFFBQWpCLENBQWhCOztBQUVBLFVBQUlpTSxXQUFKLEVBQWlCO0FBQ2hCLGVBQU9BLFdBQVcsQ0FBQ3JiLEtBQUQsQ0FBbEI7QUFDQSxlQUFPcWIsV0FBVyxDQUFDRixTQUFELENBQWxCO0FBQ0E7O0FBQ0QsVUFBSUcsYUFBSixFQUFtQjtBQUNsQixlQUFPQSxhQUFhLENBQUN0YixLQUFELENBQXBCO0FBQ0EsZUFBT3NiLGFBQWEsQ0FBQ0gsU0FBRCxDQUFwQjtBQUNBLE9BckNrQyxDQXVDbkM7OztBQUNBLFVBQUl4YixJQUFJLENBQUNuQyxLQUFMLENBQVcrSSxPQUFYLENBQW1CNFUsU0FBbkIsTUFBa0MsQ0FBQyxDQUF2QyxFQUEwQztBQUN6Q3pELGFBQUssR0FBRy9YLElBQUksQ0FBQzZiLE9BQUwsQ0FBYXhiLEtBQWIsQ0FBUjtBQUNBa2IsaUJBQVMsR0FBR3BZLENBQUMsQ0FBQ25ELElBQUksQ0FBQzBVLE1BQUwsQ0FBWSxNQUFaLEVBQW9CL1QsSUFBcEIsQ0FBRCxDQUFiO0FBQ0EsWUFBSW9YLEtBQUssQ0FBQ2pCLFFBQU4sQ0FBZSxRQUFmLENBQUosRUFBOEJ5RSxTQUFTLENBQUNySyxRQUFWLENBQW1CLFFBQW5CO0FBQzlCNkcsYUFBSyxDQUFDK0QsV0FBTixDQUFrQlAsU0FBbEI7QUFDQSxPQTdDa0MsQ0ErQ25DOzs7QUFDQXZiLFVBQUksQ0FBQ2dYLFNBQUwsR0FBaUIsSUFBakIsQ0FoRG1DLENBa0RuQzs7QUFDQSxVQUFJaFgsSUFBSSxDQUFDZ08sTUFBVCxFQUFpQjtBQUNoQmhPLFlBQUksQ0FBQ2lXLGNBQUwsQ0FBb0IsS0FBcEI7QUFDQTtBQUNELEtBenJDNEI7O0FBMnJDN0I7Ozs7OztBQU1BOEYsZ0JBQVksRUFBRSxzQkFBUzFiLEtBQVQsRUFBZ0JzWCxNQUFoQixFQUF3QjtBQUNyQyxVQUFJM1gsSUFBSSxHQUFHLElBQVg7QUFDQUssV0FBSyxHQUFHb0ksUUFBUSxDQUFDcEksS0FBRCxDQUFoQjtBQUVBLFVBQUlxYixXQUFXLEdBQUcxYixJQUFJLENBQUN5UCxXQUFMLENBQWlCLE1BQWpCLENBQWxCO0FBQ0EsVUFBSWtNLGFBQWEsR0FBRzNiLElBQUksQ0FBQ3lQLFdBQUwsQ0FBaUIsUUFBakIsQ0FBcEI7QUFDQSxVQUFJaU0sV0FBSixFQUFpQixPQUFPQSxXQUFXLENBQUNyYixLQUFELENBQWxCO0FBQ2pCLFVBQUlzYixhQUFKLEVBQW1CLE9BQU9BLGFBQWEsQ0FBQ3RiLEtBQUQsQ0FBcEI7QUFFbkIsYUFBT0wsSUFBSSxDQUFDd1AsV0FBTCxDQUFpQm5QLEtBQWpCLENBQVA7QUFDQSxhQUFPTCxJQUFJLENBQUNELE9BQUwsQ0FBYU0sS0FBYixDQUFQO0FBQ0FMLFVBQUksQ0FBQ2dYLFNBQUwsR0FBaUIsSUFBakI7QUFDQWhYLFVBQUksQ0FBQzZHLE9BQUwsQ0FBYSxlQUFiLEVBQThCeEcsS0FBOUI7QUFDQUwsVUFBSSxDQUFDZ2MsVUFBTCxDQUFnQjNiLEtBQWhCLEVBQXVCc1gsTUFBdkI7QUFDQSxLQS9zQzRCOztBQWl0QzdCOzs7QUFHQXNFLGdCQUFZLEVBQUUsd0JBQVc7QUFDeEIsVUFBSWpjLElBQUksR0FBRyxJQUFYO0FBRUFBLFVBQUksQ0FBQ29QLGNBQUwsR0FBc0IsRUFBdEI7QUFDQXBQLFVBQUksQ0FBQ3dQLFdBQUwsR0FBbUIsRUFBbkI7QUFDQXhQLFVBQUksQ0FBQ3lQLFdBQUwsR0FBbUIsRUFBbkI7QUFDQXpQLFVBQUksQ0FBQ0QsT0FBTCxHQUFlQyxJQUFJLENBQUM0UCxNQUFMLENBQVkvUixLQUFaLEdBQW9CLEVBQW5DO0FBQ0FtQyxVQUFJLENBQUNnWCxTQUFMLEdBQWlCLElBQWpCO0FBQ0FoWCxVQUFJLENBQUM2RyxPQUFMLENBQWEsY0FBYjtBQUNBN0csVUFBSSxDQUFDNlgsS0FBTDtBQUNBLEtBOXRDNEI7O0FBZ3VDN0I7Ozs7Ozs7QUFPQVgsYUFBUyxFQUFFLG1CQUFTN1csS0FBVCxFQUFnQjtBQUMxQixhQUFPLEtBQUs2YixtQkFBTCxDQUF5QjdiLEtBQXpCLEVBQWdDLEtBQUtzUSxpQkFBTCxDQUF1QnFLLElBQXZCLENBQTRCLG1CQUE1QixDQUFoQyxDQUFQO0FBQ0EsS0F6dUM0Qjs7QUEydUM3Qjs7Ozs7Ozs7QUFRQXJGLHFCQUFpQixFQUFFLDJCQUFTa0IsT0FBVCxFQUFrQmxWLFNBQWxCLEVBQTZCO0FBQy9DLFVBQUl3YSxRQUFRLEdBQUcsS0FBS3pMLFNBQUwsQ0FBZXNLLElBQWYsQ0FBb0IsbUJBQXBCLENBQWY7QUFDQSxVQUFJb0IsS0FBSyxHQUFNRCxRQUFRLENBQUNDLEtBQVQsQ0FBZXZGLE9BQWYsSUFBMEJsVixTQUF6QztBQUVBLGFBQU95YSxLQUFLLElBQUksQ0FBVCxJQUFjQSxLQUFLLEdBQUdELFFBQVEsQ0FBQzdkLE1BQS9CLEdBQXdDNmQsUUFBUSxDQUFDRSxFQUFULENBQVlELEtBQVosQ0FBeEMsR0FBNkRqWixDQUFDLEVBQXJFO0FBQ0EsS0F4dkM0Qjs7QUEwdkM3Qjs7Ozs7Ozs7QUFRQStZLHVCQUFtQixFQUFFLDZCQUFTN2IsS0FBVCxFQUFnQmljLElBQWhCLEVBQXNCO0FBQzFDamMsV0FBSyxHQUFHb0ksUUFBUSxDQUFDcEksS0FBRCxDQUFoQjs7QUFFQSxVQUFJLE9BQU9BLEtBQVAsS0FBaUIsV0FBakIsSUFBZ0NBLEtBQUssS0FBSyxJQUE5QyxFQUFvRDtBQUNuRCxhQUFLLElBQUk5QixDQUFDLEdBQUcsQ0FBUixFQUFXQyxDQUFDLEdBQUc4ZCxJQUFJLENBQUNoZSxNQUF6QixFQUFpQ0MsQ0FBQyxHQUFHQyxDQUFyQyxFQUF3Q0QsQ0FBQyxFQUF6QyxFQUE2QztBQUM1QyxjQUFJK2QsSUFBSSxDQUFDL2QsQ0FBRCxDQUFKLENBQVFnZSxZQUFSLENBQXFCLFlBQXJCLE1BQXVDbGMsS0FBM0MsRUFBa0Q7QUFDakQsbUJBQU84QyxDQUFDLENBQUNtWixJQUFJLENBQUMvZCxDQUFELENBQUwsQ0FBUjtBQUNBO0FBQ0Q7QUFDRDs7QUFFRCxhQUFPNEUsQ0FBQyxFQUFSO0FBQ0EsS0E5d0M0Qjs7QUFneEM3Qjs7Ozs7OztBQU9BMFksV0FBTyxFQUFFLGlCQUFTeGIsS0FBVCxFQUFnQjtBQUN4QixhQUFPLEtBQUs2YixtQkFBTCxDQUF5QjdiLEtBQXpCLEVBQWdDLEtBQUttUSxRQUFMLENBQWMrQyxRQUFkLEVBQWhDLENBQVA7QUFDQSxLQXp4QzRCOztBQTJ4QzdCOzs7Ozs7O0FBT0F1RSxZQUFRLEVBQUUsa0JBQVMwRSxNQUFULEVBQWlCN0UsTUFBakIsRUFBeUI7QUFDbEMsVUFBSTlaLEtBQUssR0FBR3NGLENBQUMsQ0FBQ0QsT0FBRixDQUFVc1osTUFBVixJQUFvQkEsTUFBcEIsR0FBNkIsQ0FBQ0EsTUFBRCxDQUF6Qzs7QUFDQSxXQUFLLElBQUlqZSxDQUFDLEdBQUcsQ0FBUixFQUFXQyxDQUFDLEdBQUdYLEtBQUssQ0FBQ1MsTUFBMUIsRUFBa0NDLENBQUMsR0FBR0MsQ0FBdEMsRUFBeUNELENBQUMsRUFBMUMsRUFBOEM7QUFDN0MsYUFBS2tlLFNBQUwsR0FBa0JsZSxDQUFDLEdBQUdDLENBQUMsR0FBRyxDQUExQjtBQUNBLGFBQUt5WSxPQUFMLENBQWFwWixLQUFLLENBQUNVLENBQUQsQ0FBbEIsRUFBdUJvWixNQUF2QjtBQUNBO0FBQ0QsS0F4eUM0Qjs7QUEweUM3Qjs7Ozs7OztBQU9BVixXQUFPLEVBQUUsaUJBQVM1VyxLQUFULEVBQWdCc1gsTUFBaEIsRUFBd0I7QUFDaEMsVUFBSUMsTUFBTSxHQUFHRCxNQUFNLEdBQUcsRUFBSCxHQUFRLENBQUMsUUFBRCxDQUEzQjtBQUVBbE8scUJBQWUsQ0FBQyxJQUFELEVBQU9tTyxNQUFQLEVBQWUsWUFBVztBQUN4QyxZQUFJRyxLQUFKLEVBQVdsQixPQUFYLEVBQW9Cc0YsUUFBcEI7QUFDQSxZQUFJbmMsSUFBSSxHQUFHLElBQVg7QUFDQSxZQUFJNlEsU0FBUyxHQUFHN1EsSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVMsSUFBOUI7QUFDQSxZQUFJeFIsQ0FBSixFQUFPbWUsTUFBUCxFQUFlQyxVQUFmLEVBQTJCQyxPQUEzQjtBQUNBdmMsYUFBSyxHQUFHb0ksUUFBUSxDQUFDcEksS0FBRCxDQUFoQjs7QUFFQSxZQUFJTCxJQUFJLENBQUNuQyxLQUFMLENBQVcrSSxPQUFYLENBQW1CdkcsS0FBbkIsTUFBOEIsQ0FBQyxDQUFuQyxFQUFzQztBQUNyQyxjQUFJd1EsU0FBUyxLQUFLLFFBQWxCLEVBQTRCN1EsSUFBSSxDQUFDK1UsS0FBTDtBQUM1QjtBQUNBOztBQUVELFlBQUksQ0FBQy9VLElBQUksQ0FBQ0QsT0FBTCxDQUFhZixjQUFiLENBQTRCcUIsS0FBNUIsQ0FBTCxFQUF5QztBQUN6QyxZQUFJd1EsU0FBUyxLQUFLLFFBQWxCLEVBQTRCN1EsSUFBSSxDQUFDNlgsS0FBTCxDQUFXRixNQUFYO0FBQzVCLFlBQUk5RyxTQUFTLEtBQUssT0FBZCxJQUF5QjdRLElBQUksQ0FBQ2tWLE1BQUwsRUFBN0IsRUFBNEM7QUFFNUM2QyxhQUFLLEdBQUc1VSxDQUFDLENBQUNuRCxJQUFJLENBQUMwVSxNQUFMLENBQVksTUFBWixFQUFvQjFVLElBQUksQ0FBQ0QsT0FBTCxDQUFhTSxLQUFiLENBQXBCLENBQUQsQ0FBVDtBQUNBdWMsZUFBTyxHQUFHNWMsSUFBSSxDQUFDa1YsTUFBTCxFQUFWO0FBQ0FsVixZQUFJLENBQUNuQyxLQUFMLENBQVcrRCxNQUFYLENBQWtCNUIsSUFBSSxDQUFDa1AsUUFBdkIsRUFBaUMsQ0FBakMsRUFBb0M3TyxLQUFwQztBQUNBTCxZQUFJLENBQUM2YyxhQUFMLENBQW1COUUsS0FBbkI7O0FBQ0EsWUFBSSxDQUFDL1gsSUFBSSxDQUFDeWMsU0FBTixJQUFvQixDQUFDRyxPQUFELElBQVk1YyxJQUFJLENBQUNrVixNQUFMLEVBQXBDLEVBQW9EO0FBQ25EbFYsY0FBSSxDQUFDNFQsWUFBTDtBQUNBOztBQUVELFlBQUk1VCxJQUFJLENBQUN3TyxPQUFULEVBQWtCO0FBQ2pCMk4sa0JBQVEsR0FBR25jLElBQUksQ0FBQzJRLGlCQUFMLENBQXVCcUssSUFBdkIsQ0FBNEIsbUJBQTVCLENBQVgsQ0FEaUIsQ0FHakI7O0FBQ0EsY0FBSSxDQUFDaGIsSUFBSSxDQUFDeWMsU0FBVixFQUFxQjtBQUNwQjVGLG1CQUFPLEdBQUc3VyxJQUFJLENBQUNrWCxTQUFMLENBQWU3VyxLQUFmLENBQVY7QUFDQXNjLHNCQUFVLEdBQUczYyxJQUFJLENBQUMyVixpQkFBTCxDQUF1QmtCLE9BQXZCLEVBQWdDLENBQWhDLEVBQW1DOUosSUFBbkMsQ0FBd0MsWUFBeEMsQ0FBYjtBQUNBL00sZ0JBQUksQ0FBQ2lXLGNBQUwsQ0FBb0JqVyxJQUFJLENBQUNzTyxTQUFMLElBQWtCdUMsU0FBUyxLQUFLLFFBQXBEOztBQUNBLGdCQUFJOEwsVUFBSixFQUFnQjtBQUNmM2Msa0JBQUksQ0FBQzRWLGVBQUwsQ0FBcUI1VixJQUFJLENBQUNrWCxTQUFMLENBQWV5RixVQUFmLENBQXJCO0FBQ0E7QUFDRCxXQVhnQixDQWFqQjs7O0FBQ0EsY0FBSSxDQUFDUixRQUFRLENBQUM3ZCxNQUFWLElBQW9CMEIsSUFBSSxDQUFDa1YsTUFBTCxFQUF4QixFQUF1QztBQUN0Q2xWLGdCQUFJLENBQUMrVSxLQUFMO0FBQ0EsV0FGRCxNQUVPO0FBQ04vVSxnQkFBSSxDQUFDOFMsZ0JBQUw7QUFDQTs7QUFFRDlTLGNBQUksQ0FBQytULGlCQUFMO0FBQ0EvVCxjQUFJLENBQUM2RyxPQUFMLENBQWEsVUFBYixFQUF5QnhHLEtBQXpCLEVBQWdDMFgsS0FBaEM7QUFDQS9YLGNBQUksQ0FBQzZULG1CQUFMLENBQXlCO0FBQUM4RCxrQkFBTSxFQUFFQTtBQUFULFdBQXpCO0FBQ0E7QUFDRCxPQWhEYyxDQUFmO0FBaURBLEtBcjJDNEI7O0FBdTJDN0I7Ozs7OztBQU1BcUUsY0FBVSxFQUFFLG9CQUFTM2IsS0FBVCxFQUFnQnNYLE1BQWhCLEVBQXdCO0FBQ25DLFVBQUkzWCxJQUFJLEdBQUcsSUFBWDtBQUNBLFVBQUkrWCxLQUFKLEVBQVd4WixDQUFYLEVBQWMwWixHQUFkO0FBRUFGLFdBQUssR0FBSSxRQUFPMVgsS0FBUCxNQUFpQixRQUFsQixHQUE4QkEsS0FBOUIsR0FBc0NMLElBQUksQ0FBQzZiLE9BQUwsQ0FBYXhiLEtBQWIsQ0FBOUM7QUFDQUEsV0FBSyxHQUFHb0ksUUFBUSxDQUFDc1AsS0FBSyxDQUFDaEwsSUFBTixDQUFXLFlBQVgsQ0FBRCxDQUFoQjtBQUNBeE8sT0FBQyxHQUFHeUIsSUFBSSxDQUFDbkMsS0FBTCxDQUFXK0ksT0FBWCxDQUFtQnZHLEtBQW5CLENBQUo7O0FBRUEsVUFBSTlCLENBQUMsS0FBSyxDQUFDLENBQVgsRUFBYztBQUNid1osYUFBSyxDQUFDaE0sTUFBTjs7QUFDQSxZQUFJZ00sS0FBSyxDQUFDakIsUUFBTixDQUFlLFFBQWYsQ0FBSixFQUE4QjtBQUM3Qm1CLGFBQUcsR0FBR2pZLElBQUksQ0FBQ3NQLFlBQUwsQ0FBa0IxSSxPQUFsQixDQUEwQm1SLEtBQUssQ0FBQyxDQUFELENBQS9CLENBQU47QUFDQS9YLGNBQUksQ0FBQ3NQLFlBQUwsQ0FBa0IxTixNQUFsQixDQUF5QnFXLEdBQXpCLEVBQThCLENBQTlCO0FBQ0E7O0FBRURqWSxZQUFJLENBQUNuQyxLQUFMLENBQVcrRCxNQUFYLENBQWtCckQsQ0FBbEIsRUFBcUIsQ0FBckI7QUFDQXlCLFlBQUksQ0FBQ2dYLFNBQUwsR0FBaUIsSUFBakI7O0FBQ0EsWUFBSSxDQUFDaFgsSUFBSSxDQUFDbEMsUUFBTCxDQUFjZ2YsT0FBZixJQUEwQjljLElBQUksQ0FBQ3dQLFdBQUwsQ0FBaUJ4USxjQUFqQixDQUFnQ3FCLEtBQWhDLENBQTlCLEVBQXNFO0FBQ3JFTCxjQUFJLENBQUMrYixZQUFMLENBQWtCMWIsS0FBbEIsRUFBeUJzWCxNQUF6QjtBQUNBOztBQUVELFlBQUlwWixDQUFDLEdBQUd5QixJQUFJLENBQUNrUCxRQUFiLEVBQXVCO0FBQ3RCbFAsY0FBSSxDQUFDMFcsUUFBTCxDQUFjMVcsSUFBSSxDQUFDa1AsUUFBTCxHQUFnQixDQUE5QjtBQUNBOztBQUVEbFAsWUFBSSxDQUFDNFQsWUFBTDtBQUNBNVQsWUFBSSxDQUFDK1QsaUJBQUw7QUFDQS9ULFlBQUksQ0FBQzZULG1CQUFMLENBQXlCO0FBQUM4RCxnQkFBTSxFQUFFQTtBQUFULFNBQXpCO0FBQ0EzWCxZQUFJLENBQUM4UyxnQkFBTDtBQUNBOVMsWUFBSSxDQUFDNkcsT0FBTCxDQUFhLGFBQWIsRUFBNEJ4RyxLQUE1QixFQUFtQzBYLEtBQW5DO0FBQ0E7QUFDRCxLQTU0QzRCOztBQTg0QzdCOzs7Ozs7Ozs7Ozs7O0FBYUEzQyxjQUFVLEVBQUUsb0JBQVMvSyxLQUFULEVBQWdCb1AsZUFBaEIsRUFBaUM7QUFDNUMsVUFBSXpaLElBQUksR0FBSSxJQUFaO0FBQ0EsVUFBSStjLEtBQUssR0FBRy9jLElBQUksQ0FBQ2tQLFFBQWpCO0FBQ0E3RSxXQUFLLEdBQUdBLEtBQUssSUFBSWxILENBQUMsQ0FBQ2hGLElBQUYsQ0FBTzZCLElBQUksQ0FBQ3lRLGNBQUwsQ0FBb0I5RCxHQUFwQixNQUE2QixFQUFwQyxDQUFqQjtBQUVBLFVBQUlwTixRQUFRLEdBQUd5RCxTQUFTLENBQUNBLFNBQVMsQ0FBQzFFLE1BQVYsR0FBbUIsQ0FBcEIsQ0FBeEI7QUFDQSxVQUFJLE9BQU9pQixRQUFQLEtBQW9CLFVBQXhCLEVBQW9DQSxRQUFRLEdBQUcsb0JBQVcsQ0FBRSxDQUF4Qjs7QUFFcEMsVUFBSSxPQUFPa2EsZUFBUCxLQUEyQixTQUEvQixFQUEwQztBQUN6Q0EsdUJBQWUsR0FBRyxJQUFsQjtBQUNBOztBQUVELFVBQUksQ0FBQ3paLElBQUksQ0FBQzZhLFNBQUwsQ0FBZXhRLEtBQWYsQ0FBTCxFQUE0QjtBQUMzQjlLLGdCQUFRO0FBQ1IsZUFBTyxLQUFQO0FBQ0E7O0FBRURTLFVBQUksQ0FBQ2dkLElBQUw7QUFFQSxVQUFJNU0sS0FBSyxHQUFJLE9BQU9wUSxJQUFJLENBQUNsQyxRQUFMLENBQWN3WCxNQUFyQixLQUFnQyxVQUFqQyxHQUErQyxLQUFLeFgsUUFBTCxDQUFjd1gsTUFBN0QsR0FBc0UsVUFBU2pMLEtBQVQsRUFBZ0I7QUFDakcsWUFBSTFKLElBQUksR0FBRyxFQUFYO0FBQ0FBLFlBQUksQ0FBQ1gsSUFBSSxDQUFDbEMsUUFBTCxDQUFjc1csVUFBZixDQUFKLEdBQWlDL0osS0FBakM7QUFDQTFKLFlBQUksQ0FBQ1gsSUFBSSxDQUFDbEMsUUFBTCxDQUFjbWQsVUFBZixDQUFKLEdBQWlDNVEsS0FBakM7QUFDQSxlQUFPMUosSUFBUDtBQUNBLE9BTEQ7QUFPQSxVQUFJMlUsTUFBTSxHQUFHck0sSUFBSSxDQUFDLFVBQVN0SSxJQUFULEVBQWU7QUFDaENYLFlBQUksQ0FBQ2lkLE1BQUw7QUFFQSxZQUFJLENBQUN0YyxJQUFELElBQVMsUUFBT0EsSUFBUCxNQUFnQixRQUE3QixFQUF1QyxPQUFPcEIsUUFBUSxFQUFmO0FBQ3ZDLFlBQUljLEtBQUssR0FBR29JLFFBQVEsQ0FBQzlILElBQUksQ0FBQ1gsSUFBSSxDQUFDbEMsUUFBTCxDQUFjbWQsVUFBZixDQUFMLENBQXBCO0FBQ0EsWUFBSSxPQUFPNWEsS0FBUCxLQUFpQixRQUFyQixFQUErQixPQUFPZCxRQUFRLEVBQWY7QUFFL0JTLFlBQUksQ0FBQ3lXLGVBQUwsQ0FBcUIsRUFBckI7QUFDQXpXLFlBQUksQ0FBQ3VYLFNBQUwsQ0FBZTVXLElBQWY7QUFDQVgsWUFBSSxDQUFDMFcsUUFBTCxDQUFjcUcsS0FBZDtBQUNBL2MsWUFBSSxDQUFDaVgsT0FBTCxDQUFhNVcsS0FBYjtBQUNBTCxZQUFJLENBQUNpVyxjQUFMLENBQW9Cd0QsZUFBZSxJQUFJelosSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVMsSUFBZCxLQUF1QixRQUE5RDtBQUNBeFEsZ0JBQVEsQ0FBQ29CLElBQUQsQ0FBUjtBQUNBLE9BYmdCLENBQWpCO0FBZUEsVUFBSXVjLE1BQU0sR0FBRzlNLEtBQUssQ0FBQ3hRLEtBQU4sQ0FBWSxJQUFaLEVBQWtCLENBQUN5SyxLQUFELEVBQVFpTCxNQUFSLENBQWxCLENBQWI7O0FBQ0EsVUFBSSxPQUFPNEgsTUFBUCxLQUFrQixXQUF0QixFQUFtQztBQUNsQzVILGNBQU0sQ0FBQzRILE1BQUQsQ0FBTjtBQUNBOztBQUVELGFBQU8sSUFBUDtBQUNBLEtBMThDNEI7O0FBNDhDN0I7OztBQUdBcEosZ0JBQVksRUFBRSx3QkFBVztBQUN4QixXQUFLa0QsU0FBTCxHQUFpQixJQUFqQjs7QUFFQSxVQUFJLEtBQUt4SSxPQUFULEVBQWtCO0FBQ2pCLGFBQUt5SSxPQUFMLENBQWEsS0FBS3BaLEtBQWxCO0FBQ0E7O0FBRUQsV0FBSytWLFlBQUw7QUFDQSxXQUFLQyxtQkFBTDtBQUNBLEtBeDlDNEI7O0FBMDlDN0I7Ozs7QUFJQUQsZ0JBQVksRUFBRSx3QkFBVztBQUN4QixVQUFJdUosT0FBSjtBQUFBLFVBQWFuZCxJQUFJLEdBQUcsSUFBcEI7O0FBQ0EsVUFBSUEsSUFBSSxDQUFDa08sVUFBVCxFQUFxQjtBQUNwQixZQUFJbE8sSUFBSSxDQUFDbkMsS0FBTCxDQUFXUyxNQUFmLEVBQXVCMEIsSUFBSSxDQUFDb08sU0FBTCxHQUFpQixLQUFqQjtBQUN2QnBPLFlBQUksQ0FBQ3lRLGNBQUwsQ0FBb0IyTSxJQUFwQixDQUF5QixVQUF6QixFQUFxQ0QsT0FBckM7QUFDQTs7QUFDRG5kLFVBQUksQ0FBQ3FkLGNBQUw7QUFDQSxLQXIrQzRCOztBQXUrQzdCOzs7QUFHQUEsa0JBQWMsRUFBRSwwQkFBVztBQUMxQixVQUFJcmQsSUFBSSxHQUFPLElBQWY7QUFDQSxVQUFJa1YsTUFBTSxHQUFLbFYsSUFBSSxDQUFDa1YsTUFBTCxFQUFmO0FBQ0EsVUFBSTdHLFFBQVEsR0FBR3JPLElBQUksQ0FBQ3FPLFFBQXBCO0FBRUFyTyxVQUFJLENBQUN1USxRQUFMLENBQ0UrTSxXQURGLENBQ2MsS0FEZCxFQUNxQnRkLElBQUksQ0FBQzROLEdBRDFCO0FBR0E1TixVQUFJLENBQUN3USxRQUFMLENBQ0U4TSxXQURGLENBQ2MsT0FEZCxFQUN1QnRkLElBQUksQ0FBQ3NPLFNBRDVCLEVBRUVnUCxXQUZGLENBRWMsVUFGZCxFQUUwQnRkLElBQUksQ0FBQ2lPLFVBRi9CLEVBR0VxUCxXQUhGLENBR2MsVUFIZCxFQUcwQnRkLElBQUksQ0FBQ2tPLFVBSC9CLEVBSUVvUCxXQUpGLENBSWMsU0FKZCxFQUl5QnRkLElBQUksQ0FBQ29PLFNBSjlCLEVBS0VrUCxXQUxGLENBS2MsUUFMZCxFQUt3QmpQLFFBTHhCLEVBTUVpUCxXQU5GLENBTWMsTUFOZCxFQU1zQnBJLE1BTnRCLEVBTThCb0ksV0FOOUIsQ0FNMEMsVUFOMUMsRUFNc0QsQ0FBQ3BJLE1BTnZELEVBT0VvSSxXQVBGLENBT2MsY0FQZCxFQU84QnRkLElBQUksQ0FBQ3NPLFNBQUwsSUFBa0IsQ0FBQ3RPLElBQUksQ0FBQ3VPLGFBUHRELEVBUUUrTyxXQVJGLENBUWMsaUJBUmQsRUFRaUN0ZCxJQUFJLENBQUNnTyxNQVJ0QyxFQVNFc1AsV0FURixDQVNjLGFBVGQsRUFTNkIsQ0FBQ25hLENBQUMsQ0FBQ29hLGFBQUYsQ0FBZ0J2ZCxJQUFJLENBQUNELE9BQXJCLENBVDlCLEVBVUV1ZCxXQVZGLENBVWMsV0FWZCxFQVUyQnRkLElBQUksQ0FBQ25DLEtBQUwsQ0FBV1MsTUFBWCxHQUFvQixDQVYvQztBQVlBMEIsVUFBSSxDQUFDeVEsY0FBTCxDQUFvQjlQLElBQXBCLENBQXlCLE1BQXpCLEVBQWlDLENBQUN1VSxNQUFELElBQVcsQ0FBQzdHLFFBQTdDO0FBQ0EsS0EvL0M0Qjs7QUFpZ0Q3Qjs7Ozs7O0FBTUE2RyxVQUFNLEVBQUUsa0JBQVc7QUFDbEIsYUFBTyxLQUFLcFgsUUFBTCxDQUFja1MsUUFBZCxLQUEyQixJQUEzQixJQUFtQyxLQUFLblMsS0FBTCxDQUFXUyxNQUFYLElBQXFCLEtBQUtSLFFBQUwsQ0FBY2tTLFFBQTdFO0FBQ0EsS0F6Z0Q0Qjs7QUEyZ0Q3Qjs7OztBQUlBNkQsdUJBQW1CLEVBQUUsNkJBQVMySixJQUFULEVBQWU7QUFDbkMsVUFBSWpmLENBQUo7QUFBQSxVQUFPQyxDQUFQO0FBQUEsVUFBVXVCLE9BQVY7QUFBQSxVQUFtQjBkLEtBQW5CO0FBQUEsVUFBMEJ6ZCxJQUFJLEdBQUcsSUFBakM7QUFDQXdkLFVBQUksR0FBR0EsSUFBSSxJQUFJLEVBQWY7O0FBRUEsVUFBSXhkLElBQUksQ0FBQzJOLE9BQUwsS0FBaUJ4RixVQUFyQixFQUFpQztBQUNoQ3BJLGVBQU8sR0FBRyxFQUFWOztBQUNBLGFBQUt4QixDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUd3QixJQUFJLENBQUNuQyxLQUFMLENBQVdTLE1BQTNCLEVBQW1DQyxDQUFDLEdBQUdDLENBQXZDLEVBQTBDRCxDQUFDLEVBQTNDLEVBQStDO0FBQzlDa2YsZUFBSyxHQUFHemQsSUFBSSxDQUFDRCxPQUFMLENBQWFDLElBQUksQ0FBQ25DLEtBQUwsQ0FBV1UsQ0FBWCxDQUFiLEVBQTRCeUIsSUFBSSxDQUFDbEMsUUFBTCxDQUFjc1csVUFBMUMsS0FBeUQsRUFBakU7QUFDQXJVLGlCQUFPLENBQUNaLElBQVIsQ0FBYSxvQkFBb0J1SixXQUFXLENBQUMxSSxJQUFJLENBQUNuQyxLQUFMLENBQVdVLENBQVgsQ0FBRCxDQUEvQixHQUFpRCx3QkFBakQsR0FBNEVtSyxXQUFXLENBQUMrVSxLQUFELENBQXZGLEdBQWlHLFdBQTlHO0FBQ0E7O0FBQ0QsWUFBSSxDQUFDMWQsT0FBTyxDQUFDekIsTUFBVCxJQUFtQixDQUFDLEtBQUsyTixNQUFMLENBQVljLElBQVosQ0FBaUIsVUFBakIsQ0FBeEIsRUFBc0Q7QUFDckRoTixpQkFBTyxDQUFDWixJQUFSLENBQWEsZ0RBQWI7QUFDQTs7QUFDRGEsWUFBSSxDQUFDaU0sTUFBTCxDQUFZdUksSUFBWixDQUFpQnpVLE9BQU8sQ0FBQzRSLElBQVIsQ0FBYSxFQUFiLENBQWpCO0FBQ0EsT0FWRCxNQVVPO0FBQ04zUixZQUFJLENBQUNpTSxNQUFMLENBQVlVLEdBQVosQ0FBZ0IzTSxJQUFJLENBQUMwWCxRQUFMLEVBQWhCO0FBQ0ExWCxZQUFJLENBQUNpTSxNQUFMLENBQVljLElBQVosQ0FBaUIsT0FBakIsRUFBeUIvTSxJQUFJLENBQUNpTSxNQUFMLENBQVlVLEdBQVosRUFBekI7QUFDQTs7QUFFRCxVQUFJM00sSUFBSSxDQUFDd08sT0FBVCxFQUFrQjtBQUNqQixZQUFJLENBQUNnUCxJQUFJLENBQUM3RixNQUFWLEVBQWtCO0FBQ2pCM1gsY0FBSSxDQUFDNkcsT0FBTCxDQUFhLFFBQWIsRUFBdUI3RyxJQUFJLENBQUNpTSxNQUFMLENBQVlVLEdBQVosRUFBdkI7QUFDQTtBQUNEO0FBQ0QsS0F2aUQ0Qjs7QUF5aUQ3Qjs7OztBQUlBb0gscUJBQWlCLEVBQUUsNkJBQVc7QUFDN0IsVUFBSSxDQUFDLEtBQUtqVyxRQUFMLENBQWN3TyxXQUFuQixFQUFnQztBQUNoQyxVQUFJTCxNQUFNLEdBQUcsS0FBS3dFLGNBQWxCOztBQUVBLFVBQUksS0FBSzVTLEtBQUwsQ0FBV1MsTUFBZixFQUF1QjtBQUN0QjJOLGNBQU0sQ0FBQ3lSLFVBQVAsQ0FBa0IsYUFBbEI7QUFDQSxPQUZELE1BRU87QUFDTnpSLGNBQU0sQ0FBQ2MsSUFBUCxDQUFZLGFBQVosRUFBMkIsS0FBS2pQLFFBQUwsQ0FBY3dPLFdBQXpDO0FBQ0E7O0FBQ0RMLFlBQU0sQ0FBQ2UsY0FBUCxDQUFzQixRQUF0QixFQUFnQztBQUFDTixhQUFLLEVBQUU7QUFBUixPQUFoQztBQUNBLEtBdmpENEI7O0FBeWpEN0I7Ozs7QUFJQXNJLFFBQUksRUFBRSxnQkFBVztBQUNoQixVQUFJaFYsSUFBSSxHQUFHLElBQVg7QUFFQSxVQUFJQSxJQUFJLENBQUNxTyxRQUFMLElBQWlCck8sSUFBSSxDQUFDZ08sTUFBdEIsSUFBaUNoTyxJQUFJLENBQUNsQyxRQUFMLENBQWNpUyxJQUFkLEtBQXVCLE9BQXZCLElBQWtDL1AsSUFBSSxDQUFDa1YsTUFBTCxFQUF2RSxFQUF1RjtBQUN2RmxWLFVBQUksQ0FBQzBLLEtBQUw7QUFDQTFLLFVBQUksQ0FBQ2dPLE1BQUwsR0FBYyxJQUFkO0FBQ0FoTyxVQUFJLENBQUM0VCxZQUFMO0FBQ0E1VCxVQUFJLENBQUMwUSxTQUFMLENBQWVyRixHQUFmLENBQW1CO0FBQUNzUyxrQkFBVSxFQUFFLFFBQWI7QUFBdUJDLGVBQU8sRUFBRTtBQUFoQyxPQUFuQjtBQUNBNWQsVUFBSSxDQUFDOFMsZ0JBQUw7QUFDQTlTLFVBQUksQ0FBQzBRLFNBQUwsQ0FBZXJGLEdBQWYsQ0FBbUI7QUFBQ3NTLGtCQUFVLEVBQUU7QUFBYixPQUFuQjtBQUNBM2QsVUFBSSxDQUFDNkcsT0FBTCxDQUFhLGVBQWIsRUFBOEI3RyxJQUFJLENBQUMwUSxTQUFuQztBQUNBLEtBeGtENEI7O0FBMGtEN0I7OztBQUdBcUUsU0FBSyxFQUFFLGlCQUFXO0FBQ2pCLFVBQUkvVSxJQUFJLEdBQUcsSUFBWDtBQUNBLFVBQUk2RyxPQUFPLEdBQUc3RyxJQUFJLENBQUNnTyxNQUFuQjs7QUFFQSxVQUFJaE8sSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVMsSUFBZCxLQUF1QixRQUF2QixJQUFtQy9QLElBQUksQ0FBQ25DLEtBQUwsQ0FBV1MsTUFBbEQsRUFBMEQ7QUFDekQwQixZQUFJLENBQUNzWSxTQUFMO0FBQ0E7O0FBRUR0WSxVQUFJLENBQUNnTyxNQUFMLEdBQWMsS0FBZDtBQUNBaE8sVUFBSSxDQUFDMFEsU0FBTCxDQUFlYSxJQUFmO0FBQ0F2UixVQUFJLENBQUM0VixlQUFMLENBQXFCLElBQXJCO0FBQ0E1VixVQUFJLENBQUM0VCxZQUFMO0FBRUEsVUFBSS9NLE9BQUosRUFBYTdHLElBQUksQ0FBQzZHLE9BQUwsQ0FBYSxnQkFBYixFQUErQjdHLElBQUksQ0FBQzBRLFNBQXBDO0FBQ2IsS0EzbEQ0Qjs7QUE2bEQ3Qjs7OztBQUlBb0Msb0JBQWdCLEVBQUUsNEJBQVc7QUFDNUIsVUFBSXRDLFFBQVEsR0FBRyxLQUFLQSxRQUFwQjtBQUNBLFVBQUl5SSxNQUFNLEdBQUcsS0FBS25iLFFBQUwsQ0FBY3VULGNBQWQsS0FBaUMsTUFBakMsR0FBMENiLFFBQVEsQ0FBQ3lJLE1BQVQsRUFBMUMsR0FBOER6SSxRQUFRLENBQUNoRixRQUFULEVBQTNFO0FBQ0F5TixZQUFNLENBQUN4TixHQUFQLElBQWMrRSxRQUFRLENBQUN1SSxXQUFULENBQXFCLElBQXJCLENBQWQ7QUFFQSxXQUFLckksU0FBTCxDQUFlckYsR0FBZixDQUFtQjtBQUNsQk0sYUFBSyxFQUFHNkUsUUFBUSxDQUFDcU4sVUFBVCxFQURVO0FBRWxCcFMsV0FBRyxFQUFLd04sTUFBTSxDQUFDeE4sR0FGRztBQUdsQkMsWUFBSSxFQUFJdU4sTUFBTSxDQUFDdk47QUFIRyxPQUFuQjtBQUtBLEtBM21ENEI7O0FBNm1EN0I7Ozs7OztBQU1BbU0sU0FBSyxFQUFFLGVBQVNGLE1BQVQsRUFBaUI7QUFDdkIsVUFBSTNYLElBQUksR0FBRyxJQUFYO0FBRUEsVUFBSSxDQUFDQSxJQUFJLENBQUNuQyxLQUFMLENBQVdTLE1BQWhCLEVBQXdCO0FBQ3hCMEIsVUFBSSxDQUFDd1EsUUFBTCxDQUFjK0MsUUFBZCxDQUF1QixhQUF2QixFQUFzQ3hILE1BQXRDO0FBQ0EvTCxVQUFJLENBQUNuQyxLQUFMLEdBQWEsRUFBYjtBQUNBbUMsVUFBSSxDQUFDZ1gsU0FBTCxHQUFpQixJQUFqQjtBQUNBaFgsVUFBSSxDQUFDMFcsUUFBTCxDQUFjLENBQWQ7QUFDQTFXLFVBQUksQ0FBQ2lWLGFBQUwsQ0FBbUIsSUFBbkI7QUFDQWpWLFVBQUksQ0FBQytULGlCQUFMO0FBQ0EvVCxVQUFJLENBQUM2VCxtQkFBTCxDQUF5QjtBQUFDOEQsY0FBTSxFQUFFQTtBQUFULE9BQXpCO0FBQ0EzWCxVQUFJLENBQUM0VCxZQUFMO0FBQ0E1VCxVQUFJLENBQUNvVyxTQUFMO0FBQ0FwVyxVQUFJLENBQUM2RyxPQUFMLENBQWEsT0FBYjtBQUNBLEtBam9ENEI7O0FBbW9EN0I7Ozs7OztBQU1BZ1csaUJBQWEsRUFBRSx1QkFBU2lCLEdBQVQsRUFBYztBQUM1QixVQUFJZixLQUFLLEdBQUcxRixJQUFJLENBQUNrRCxHQUFMLENBQVMsS0FBS3JMLFFBQWQsRUFBd0IsS0FBS3JSLEtBQUwsQ0FBV1MsTUFBbkMsQ0FBWjs7QUFDQSxVQUFJeWUsS0FBSyxLQUFLLENBQWQsRUFBaUI7QUFDaEIsYUFBS3ZNLFFBQUwsQ0FBY3NLLE9BQWQsQ0FBc0JnRCxHQUF0QjtBQUNBLE9BRkQsTUFFTztBQUNOM2EsU0FBQyxDQUFDLEtBQUtxTixRQUFMLENBQWMsQ0FBZCxFQUFpQnRLLFVBQWpCLENBQTRCNlcsS0FBNUIsQ0FBRCxDQUFELENBQXNDbFUsTUFBdEMsQ0FBNkNpVixHQUE3QztBQUNBOztBQUNELFdBQUtwSCxRQUFMLENBQWNxRyxLQUFLLEdBQUcsQ0FBdEI7QUFDQSxLQWpwRDRCOztBQW1wRDdCOzs7Ozs7QUFNQS9HLG1CQUFlLEVBQUUseUJBQVNoTSxDQUFULEVBQVk7QUFDNUIsVUFBSXpMLENBQUosRUFBT0MsQ0FBUCxFQUFVbUQsU0FBVixFQUFxQjhJLFNBQXJCLEVBQWdDK1IsTUFBaEMsRUFBd0NPLEtBQXhDLEVBQStDZ0IsYUFBL0MsRUFBOERDLGNBQTlELEVBQThFQyxLQUE5RTtBQUNBLFVBQUlqZSxJQUFJLEdBQUcsSUFBWDtBQUVBMkIsZUFBUyxHQUFJcUksQ0FBQyxJQUFJQSxDQUFDLENBQUNvQyxPQUFGLEtBQWN2RSxhQUFwQixHQUFxQyxDQUFDLENBQXRDLEdBQTBDLENBQXREO0FBQ0E0QyxlQUFTLEdBQUdMLFlBQVksQ0FBQ3BLLElBQUksQ0FBQ3lRLGNBQUwsQ0FBb0IsQ0FBcEIsQ0FBRCxDQUF4Qjs7QUFFQSxVQUFJelEsSUFBSSxDQUFDcVAsYUFBTCxJQUFzQixDQUFDclAsSUFBSSxDQUFDbEMsUUFBTCxDQUFjbVMsWUFBekMsRUFBdUQ7QUFDdEQ4TixxQkFBYSxHQUFHL2QsSUFBSSxDQUFDMlYsaUJBQUwsQ0FBdUIzVixJQUFJLENBQUNxUCxhQUE1QixFQUEyQyxDQUFDLENBQTVDLEVBQStDdEMsSUFBL0MsQ0FBb0QsWUFBcEQsQ0FBaEI7QUFDQSxPQVQyQixDQVc1Qjs7O0FBQ0F5UCxZQUFNLEdBQUcsRUFBVDs7QUFFQSxVQUFJeGMsSUFBSSxDQUFDc1AsWUFBTCxDQUFrQmhSLE1BQXRCLEVBQThCO0FBQzdCMmYsYUFBSyxHQUFHamUsSUFBSSxDQUFDd1EsUUFBTCxDQUFjK0MsUUFBZCxDQUF1QixjQUFjNVIsU0FBUyxHQUFHLENBQVosR0FBZ0IsTUFBaEIsR0FBeUIsT0FBdkMsQ0FBdkIsQ0FBUjtBQUNBb2IsYUFBSyxHQUFHL2MsSUFBSSxDQUFDd1EsUUFBTCxDQUFjK0MsUUFBZCxDQUF1QixhQUF2QixFQUFzQzZJLEtBQXRDLENBQTRDNkIsS0FBNUMsQ0FBUjs7QUFDQSxZQUFJdGMsU0FBUyxHQUFHLENBQWhCLEVBQW1CO0FBQUVvYixlQUFLO0FBQUs7O0FBRS9CLGFBQUt4ZSxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUd3QixJQUFJLENBQUNzUCxZQUFMLENBQWtCaFIsTUFBbEMsRUFBMENDLENBQUMsR0FBR0MsQ0FBOUMsRUFBaURELENBQUMsRUFBbEQsRUFBc0Q7QUFDckRpZSxnQkFBTSxDQUFDcmQsSUFBUCxDQUFZZ0UsQ0FBQyxDQUFDbkQsSUFBSSxDQUFDc1AsWUFBTCxDQUFrQi9RLENBQWxCLENBQUQsQ0FBRCxDQUF3QndPLElBQXhCLENBQTZCLFlBQTdCLENBQVo7QUFDQTs7QUFDRCxZQUFJL0MsQ0FBSixFQUFPO0FBQ05BLFdBQUMsQ0FBQzJKLGNBQUY7QUFDQTNKLFdBQUMsQ0FBQ3NJLGVBQUY7QUFDQTtBQUNELE9BWkQsTUFZTyxJQUFJLENBQUN0UyxJQUFJLENBQUNzTyxTQUFMLElBQWtCdE8sSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVMsSUFBZCxLQUF1QixRQUExQyxLQUF1RC9QLElBQUksQ0FBQ25DLEtBQUwsQ0FBV1MsTUFBdEUsRUFBOEU7QUFDcEYsWUFBSXFELFNBQVMsR0FBRyxDQUFaLElBQWlCOEksU0FBUyxDQUFDSCxLQUFWLEtBQW9CLENBQXJDLElBQTBDRyxTQUFTLENBQUNuTSxNQUFWLEtBQXFCLENBQW5FLEVBQXNFO0FBQ3JFa2UsZ0JBQU0sQ0FBQ3JkLElBQVAsQ0FBWWEsSUFBSSxDQUFDbkMsS0FBTCxDQUFXbUMsSUFBSSxDQUFDa1AsUUFBTCxHQUFnQixDQUEzQixDQUFaO0FBQ0EsU0FGRCxNQUVPLElBQUl2TixTQUFTLEdBQUcsQ0FBWixJQUFpQjhJLFNBQVMsQ0FBQ0gsS0FBVixLQUFvQnRLLElBQUksQ0FBQ3lRLGNBQUwsQ0FBb0I5RCxHQUFwQixHQUEwQnJPLE1BQW5FLEVBQTJFO0FBQ2pGa2UsZ0JBQU0sQ0FBQ3JkLElBQVAsQ0FBWWEsSUFBSSxDQUFDbkMsS0FBTCxDQUFXbUMsSUFBSSxDQUFDa1AsUUFBaEIsQ0FBWjtBQUNBO0FBQ0QsT0FoQzJCLENBa0M1Qjs7O0FBQ0EsVUFBSSxDQUFDc04sTUFBTSxDQUFDbGUsTUFBUixJQUFtQixPQUFPMEIsSUFBSSxDQUFDbEMsUUFBTCxDQUFjb2dCLFFBQXJCLEtBQWtDLFVBQWxDLElBQWdEbGUsSUFBSSxDQUFDbEMsUUFBTCxDQUFjb2dCLFFBQWQsQ0FBdUJ0ZSxLQUF2QixDQUE2QkksSUFBN0IsRUFBbUMsQ0FBQ3djLE1BQUQsQ0FBbkMsTUFBaUQsS0FBeEgsRUFBZ0k7QUFDL0gsZUFBTyxLQUFQO0FBQ0EsT0FyQzJCLENBdUM1Qjs7O0FBQ0EsVUFBSSxPQUFPTyxLQUFQLEtBQWlCLFdBQXJCLEVBQWtDO0FBQ2pDL2MsWUFBSSxDQUFDMFcsUUFBTCxDQUFjcUcsS0FBZDtBQUNBOztBQUNELGFBQU9QLE1BQU0sQ0FBQ2xlLE1BQWQsRUFBc0I7QUFDckIwQixZQUFJLENBQUNnYyxVQUFMLENBQWdCUSxNQUFNLENBQUMyQixHQUFQLEVBQWhCO0FBQ0E7O0FBRURuZSxVQUFJLENBQUNvVyxTQUFMO0FBQ0FwVyxVQUFJLENBQUM4UyxnQkFBTDtBQUNBOVMsVUFBSSxDQUFDaVcsY0FBTCxDQUFvQixJQUFwQixFQWpENEIsQ0FtRDVCOztBQUNBLFVBQUk4SCxhQUFKLEVBQW1CO0FBQ2xCQyxzQkFBYyxHQUFHaGUsSUFBSSxDQUFDa1gsU0FBTCxDQUFlNkcsYUFBZixDQUFqQjs7QUFDQSxZQUFJQyxjQUFjLENBQUMxZixNQUFuQixFQUEyQjtBQUMxQjBCLGNBQUksQ0FBQzRWLGVBQUwsQ0FBcUJvSSxjQUFyQjtBQUNBO0FBQ0Q7O0FBRUQsYUFBTyxJQUFQO0FBQ0EsS0FydEQ0Qjs7QUF1dEQ3Qjs7Ozs7Ozs7OztBQVVBbEksb0JBQWdCLEVBQUUsMEJBQVNuVSxTQUFULEVBQW9CcUksQ0FBcEIsRUFBdUI7QUFDeEMsVUFBSW9VLElBQUosRUFBVTNULFNBQVYsRUFBcUJ3TixHQUFyQixFQUEwQm9HLFdBQTFCLEVBQXVDQyxZQUF2QyxFQUFxREwsS0FBckQ7QUFDQSxVQUFJamUsSUFBSSxHQUFHLElBQVg7QUFFQSxVQUFJMkIsU0FBUyxLQUFLLENBQWxCLEVBQXFCO0FBQ3JCLFVBQUkzQixJQUFJLENBQUM0TixHQUFULEVBQWNqTSxTQUFTLElBQUksQ0FBQyxDQUFkO0FBRWR5YyxVQUFJLEdBQUd6YyxTQUFTLEdBQUcsQ0FBWixHQUFnQixNQUFoQixHQUF5QixPQUFoQztBQUNBOEksZUFBUyxHQUFHTCxZQUFZLENBQUNwSyxJQUFJLENBQUN5USxjQUFMLENBQW9CLENBQXBCLENBQUQsQ0FBeEI7O0FBRUEsVUFBSXpRLElBQUksQ0FBQ3NPLFNBQUwsSUFBa0IsQ0FBQ3RPLElBQUksQ0FBQ3VPLGFBQTVCLEVBQTJDO0FBQzFDOFAsbUJBQVcsR0FBR3JlLElBQUksQ0FBQ3lRLGNBQUwsQ0FBb0I5RCxHQUFwQixHQUEwQnJPLE1BQXhDO0FBQ0FnZ0Isb0JBQVksR0FBRzNjLFNBQVMsR0FBRyxDQUFaLEdBQ1o4SSxTQUFTLENBQUNILEtBQVYsS0FBb0IsQ0FBcEIsSUFBeUJHLFNBQVMsQ0FBQ25NLE1BQVYsS0FBcUIsQ0FEbEMsR0FFWm1NLFNBQVMsQ0FBQ0gsS0FBVixLQUFvQitULFdBRnZCOztBQUlBLFlBQUlDLFlBQVksSUFBSSxDQUFDRCxXQUFyQixFQUFrQztBQUNqQ3JlLGNBQUksQ0FBQ3VlLFlBQUwsQ0FBa0I1YyxTQUFsQixFQUE2QnFJLENBQTdCO0FBQ0E7QUFDRCxPQVRELE1BU087QUFDTmlVLGFBQUssR0FBR2plLElBQUksQ0FBQ3dRLFFBQUwsQ0FBYytDLFFBQWQsQ0FBdUIsYUFBYTZLLElBQXBDLENBQVI7O0FBQ0EsWUFBSUgsS0FBSyxDQUFDM2YsTUFBVixFQUFrQjtBQUNqQjJaLGFBQUcsR0FBR2pZLElBQUksQ0FBQ3dRLFFBQUwsQ0FBYytDLFFBQWQsQ0FBdUIsYUFBdkIsRUFBc0M2SSxLQUF0QyxDQUE0QzZCLEtBQTVDLENBQU47QUFDQWplLGNBQUksQ0FBQ2lWLGFBQUwsQ0FBbUIsSUFBbkI7QUFDQWpWLGNBQUksQ0FBQzBXLFFBQUwsQ0FBYy9VLFNBQVMsR0FBRyxDQUFaLEdBQWdCc1csR0FBRyxHQUFHLENBQXRCLEdBQTBCQSxHQUF4QztBQUNBO0FBQ0Q7QUFDRCxLQTV2RDRCOztBQTh2RDdCOzs7Ozs7QUFNQXNHLGdCQUFZLEVBQUUsc0JBQVM1YyxTQUFULEVBQW9CcUksQ0FBcEIsRUFBdUI7QUFDcEMsVUFBSWhLLElBQUksR0FBRyxJQUFYO0FBQUEsVUFBaUI2RSxFQUFqQjtBQUFBLFVBQXFCMlosSUFBckI7QUFFQSxVQUFJN2MsU0FBUyxLQUFLLENBQWxCLEVBQXFCO0FBRXJCa0QsUUFBRSxHQUFHbEQsU0FBUyxHQUFHLENBQVosR0FBZ0IsTUFBaEIsR0FBeUIsTUFBOUI7O0FBQ0EsVUFBSTNCLElBQUksQ0FBQ3lPLFdBQVQsRUFBc0I7QUFDckIrUCxZQUFJLEdBQUd4ZSxJQUFJLENBQUN5USxjQUFMLENBQW9CNUwsRUFBcEIsR0FBUDs7QUFDQSxZQUFJMlosSUFBSSxDQUFDbGdCLE1BQVQsRUFBaUI7QUFDaEIwQixjQUFJLENBQUNzWSxTQUFMO0FBQ0F0WSxjQUFJLENBQUNpVixhQUFMLENBQW1CdUosSUFBbkI7QUFDQXhVLFdBQUMsSUFBSUEsQ0FBQyxDQUFDMkosY0FBRixFQUFMO0FBQ0E7QUFDRCxPQVBELE1BT087QUFDTjNULFlBQUksQ0FBQzBXLFFBQUwsQ0FBYzFXLElBQUksQ0FBQ2tQLFFBQUwsR0FBZ0J2TixTQUE5QjtBQUNBO0FBQ0QsS0FweEQ0Qjs7QUFzeEQ3Qjs7Ozs7QUFLQStVLFlBQVEsRUFBRSxrQkFBU25ZLENBQVQsRUFBWTtBQUNyQixVQUFJeUIsSUFBSSxHQUFHLElBQVg7O0FBRUEsVUFBSUEsSUFBSSxDQUFDbEMsUUFBTCxDQUFjaVMsSUFBZCxLQUF1QixRQUEzQixFQUFxQztBQUNwQ3hSLFNBQUMsR0FBR3lCLElBQUksQ0FBQ25DLEtBQUwsQ0FBV1MsTUFBZjtBQUNBLE9BRkQsTUFFTztBQUNOQyxTQUFDLEdBQUc4WSxJQUFJLENBQUNDLEdBQUwsQ0FBUyxDQUFULEVBQVlELElBQUksQ0FBQ2tELEdBQUwsQ0FBU3ZhLElBQUksQ0FBQ25DLEtBQUwsQ0FBV1MsTUFBcEIsRUFBNEJDLENBQTVCLENBQVosQ0FBSjtBQUNBOztBQUVELFVBQUcsQ0FBQ3lCLElBQUksQ0FBQ3ljLFNBQVQsRUFBb0I7QUFDbkI7QUFDQTtBQUNBO0FBQ0EsWUFBSS9DLENBQUosRUFBT2xiLENBQVAsRUFBVXFHLEVBQVYsRUFBY3lPLFNBQWQsRUFBeUJtTCxNQUF6QjtBQUNBbkwsaUJBQVMsR0FBR3RULElBQUksQ0FBQ3dRLFFBQUwsQ0FBYytDLFFBQWQsQ0FBdUIsYUFBdkIsQ0FBWjs7QUFDQSxhQUFLbUcsQ0FBQyxHQUFHLENBQUosRUFBT2xiLENBQUMsR0FBRzhVLFNBQVMsQ0FBQ2hWLE1BQTFCLEVBQWtDb2IsQ0FBQyxHQUFHbGIsQ0FBdEMsRUFBeUNrYixDQUFDLEVBQTFDLEVBQThDO0FBQzdDK0UsZ0JBQU0sR0FBR3RiLENBQUMsQ0FBQ21RLFNBQVMsQ0FBQ29HLENBQUQsQ0FBVixDQUFELENBQWdCbEcsTUFBaEIsRUFBVDs7QUFDQSxjQUFJa0csQ0FBQyxHQUFJbmIsQ0FBVCxFQUFZO0FBQ1h5QixnQkFBSSxDQUFDeVEsY0FBTCxDQUFvQjVILE1BQXBCLENBQTJCNFYsTUFBM0I7QUFDQSxXQUZELE1BRU87QUFDTnplLGdCQUFJLENBQUN3USxRQUFMLENBQWNrTyxNQUFkLENBQXFCRCxNQUFyQjtBQUNBO0FBQ0Q7QUFDRDs7QUFFRHplLFVBQUksQ0FBQ2tQLFFBQUwsR0FBZ0IzUSxDQUFoQjtBQUNBLEtBcnpENEI7O0FBdXpEN0I7Ozs7QUFJQXllLFFBQUksRUFBRSxnQkFBVztBQUNoQixXQUFLakksS0FBTDtBQUNBLFdBQUsxRyxRQUFMLEdBQWdCLElBQWhCO0FBQ0EsV0FBS3VGLFlBQUw7QUFDQSxLQS96RDRCOztBQWkwRDdCOzs7QUFHQXFKLFVBQU0sRUFBRSxrQkFBVztBQUNsQixXQUFLNU8sUUFBTCxHQUFnQixLQUFoQjtBQUNBLFdBQUt1RixZQUFMO0FBQ0EsS0F2MEQ0Qjs7QUF5MEQ3Qjs7OztBQUlBSSxXQUFPLEVBQUUsbUJBQVc7QUFDbkIsVUFBSWhVLElBQUksR0FBRyxJQUFYO0FBQ0FBLFVBQUksQ0FBQ2lNLE1BQUwsQ0FBWW1SLElBQVosQ0FBaUIsVUFBakIsRUFBNkIsSUFBN0I7QUFDQXBkLFVBQUksQ0FBQ3lRLGNBQUwsQ0FBb0IyTSxJQUFwQixDQUF5QixVQUF6QixFQUFxQyxJQUFyQyxFQUEyQ0EsSUFBM0MsQ0FBZ0QsVUFBaEQsRUFBNEQsQ0FBQyxDQUE3RDtBQUNBcGQsVUFBSSxDQUFDaU8sVUFBTCxHQUFrQixJQUFsQjtBQUNBak8sVUFBSSxDQUFDZ2QsSUFBTDtBQUNBLEtBbjFENEI7O0FBcTFEN0I7Ozs7QUFJQTJCLFVBQU0sRUFBRSxrQkFBVztBQUNsQixVQUFJM2UsSUFBSSxHQUFHLElBQVg7QUFDQUEsVUFBSSxDQUFDaU0sTUFBTCxDQUFZbVIsSUFBWixDQUFpQixVQUFqQixFQUE2QixLQUE3QjtBQUNBcGQsVUFBSSxDQUFDeVEsY0FBTCxDQUFvQjJNLElBQXBCLENBQXlCLFVBQXpCLEVBQXFDLEtBQXJDLEVBQTRDQSxJQUE1QyxDQUFpRCxVQUFqRCxFQUE2RHBkLElBQUksQ0FBQzBOLFFBQWxFO0FBQ0ExTixVQUFJLENBQUNpTyxVQUFMLEdBQWtCLEtBQWxCO0FBQ0FqTyxVQUFJLENBQUNpZCxNQUFMO0FBQ0EsS0EvMUQ0Qjs7QUFpMkQ3Qjs7Ozs7QUFLQTJCLFdBQU8sRUFBRSxtQkFBVztBQUNuQixVQUFJNWUsSUFBSSxHQUFHLElBQVg7QUFDQSxVQUFJNk4sT0FBTyxHQUFHN04sSUFBSSxDQUFDNk4sT0FBbkI7QUFDQSxVQUFJd0YsY0FBYyxHQUFHclQsSUFBSSxDQUFDcVQsY0FBMUI7QUFFQXJULFVBQUksQ0FBQzZHLE9BQUwsQ0FBYSxTQUFiO0FBQ0E3RyxVQUFJLENBQUMyRyxHQUFMO0FBQ0EzRyxVQUFJLENBQUN1USxRQUFMLENBQWN4RSxNQUFkO0FBQ0EvTCxVQUFJLENBQUMwUSxTQUFMLENBQWUzRSxNQUFmO0FBRUEvTCxVQUFJLENBQUNpTSxNQUFMLENBQ0V1SSxJQURGLENBQ08sRUFEUCxFQUVFa0ssTUFGRixDQUVTckwsY0FBYyxDQUFDQyxTQUZ4QixFQUdFb0ssVUFIRixDQUdhLFVBSGIsRUFJRWxHLFdBSkYsQ0FJYyxZQUpkLEVBS0V6SyxJQUxGLENBS087QUFBQzBHLGdCQUFRLEVBQUVKLGNBQWMsQ0FBQ0k7QUFBMUIsT0FMUCxFQU1Fb0wsSUFORjtBQVFBN2UsVUFBSSxDQUFDeVEsY0FBTCxDQUFvQnFPLFVBQXBCLENBQStCLE1BQS9CO0FBQ0E5ZSxVQUFJLENBQUNpTSxNQUFMLENBQVk2UyxVQUFaLENBQXVCLFdBQXZCO0FBRUEzYixPQUFDLENBQUNtRixNQUFELENBQUQsQ0FBVTNCLEdBQVYsQ0FBY2tILE9BQWQ7QUFDQTFLLE9BQUMsQ0FBQ29DLFFBQUQsQ0FBRCxDQUFZb0IsR0FBWixDQUFnQmtILE9BQWhCO0FBQ0ExSyxPQUFDLENBQUNvQyxRQUFRLENBQUNvUixJQUFWLENBQUQsQ0FBaUJoUSxHQUFqQixDQUFxQmtILE9BQXJCO0FBRUEsYUFBTzdOLElBQUksQ0FBQ2lNLE1BQUwsQ0FBWSxDQUFaLEVBQWVrQixTQUF0QjtBQUNBLEtBaDRENEI7O0FBazREN0I7Ozs7Ozs7O0FBUUF1SCxVQUFNLEVBQUUsZ0JBQVNxSyxZQUFULEVBQXVCcGUsSUFBdkIsRUFBNkI7QUFDcEMsVUFBSU4sS0FBSixFQUFXb0IsRUFBWCxFQUFlZ2MsS0FBZjtBQUNBLFVBQUlqSixJQUFJLEdBQUcsRUFBWDtBQUNBLFVBQUl3SyxLQUFLLEdBQUcsS0FBWjtBQUNBLFVBQUloZixJQUFJLEdBQUcsSUFBWDtBQUNBLFVBQUlpZixTQUFTLEdBQUcsMERBQWhCOztBQUVBLFVBQUlGLFlBQVksS0FBSyxRQUFqQixJQUE2QkEsWUFBWSxLQUFLLE1BQWxELEVBQTBEO0FBQ3pEMWUsYUFBSyxHQUFHb0ksUUFBUSxDQUFDOUgsSUFBSSxDQUFDWCxJQUFJLENBQUNsQyxRQUFMLENBQWNtZCxVQUFmLENBQUwsQ0FBaEI7QUFDQStELGFBQUssR0FBRyxDQUFDLENBQUMzZSxLQUFWO0FBQ0EsT0FWbUMsQ0FZcEM7OztBQUNBLFVBQUkyZSxLQUFKLEVBQVc7QUFDVixZQUFJLENBQUN4VyxLQUFLLENBQUN4SSxJQUFJLENBQUN5UCxXQUFMLENBQWlCc1AsWUFBakIsQ0FBRCxDQUFWLEVBQTRDO0FBQzNDL2UsY0FBSSxDQUFDeVAsV0FBTCxDQUFpQnNQLFlBQWpCLElBQWlDLEVBQWpDO0FBQ0E7O0FBQ0QsWUFBSS9lLElBQUksQ0FBQ3lQLFdBQUwsQ0FBaUJzUCxZQUFqQixFQUErQi9mLGNBQS9CLENBQThDcUIsS0FBOUMsQ0FBSixFQUEwRDtBQUN6RCxpQkFBT0wsSUFBSSxDQUFDeVAsV0FBTCxDQUFpQnNQLFlBQWpCLEVBQStCMWUsS0FBL0IsQ0FBUDtBQUNBO0FBQ0QsT0FwQm1DLENBc0JwQzs7O0FBQ0FtVSxVQUFJLEdBQUd4VSxJQUFJLENBQUNsQyxRQUFMLENBQWM0VyxNQUFkLENBQXFCcUssWUFBckIsRUFBbUNuZixLQUFuQyxDQUF5QyxJQUF6QyxFQUErQyxDQUFDZSxJQUFELEVBQU8rSCxXQUFQLENBQS9DLENBQVAsQ0F2Qm9DLENBeUJwQzs7QUFDQSxVQUFJcVcsWUFBWSxLQUFLLFFBQWpCLElBQTZCQSxZQUFZLEtBQUssZUFBbEQsRUFBbUU7QUFDbEV2SyxZQUFJLEdBQUdBLElBQUksQ0FBQ3ZWLE9BQUwsQ0FBYWdnQixTQUFiLEVBQXdCLHFCQUF4QixDQUFQO0FBQ0E7O0FBQ0QsVUFBSUYsWUFBWSxLQUFLLFVBQXJCLEVBQWlDO0FBQ2hDdGQsVUFBRSxHQUFHZCxJQUFJLENBQUNYLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY29kLGtCQUFmLENBQUosSUFBMEMsRUFBL0M7QUFDQTFHLFlBQUksR0FBR0EsSUFBSSxDQUFDdlYsT0FBTCxDQUFhZ2dCLFNBQWIsRUFBd0IscUJBQXFCdFcsY0FBYyxDQUFDRCxXQUFXLENBQUNqSCxFQUFELENBQVosQ0FBbkMsR0FBdUQsR0FBL0UsQ0FBUDtBQUNBOztBQUNELFVBQUlzZCxZQUFZLEtBQUssUUFBakIsSUFBNkJBLFlBQVksS0FBSyxNQUFsRCxFQUEwRDtBQUN6RHZLLFlBQUksR0FBR0EsSUFBSSxDQUFDdlYsT0FBTCxDQUFhZ2dCLFNBQWIsRUFBd0IscUJBQXFCdFcsY0FBYyxDQUFDRCxXQUFXLENBQUNySSxLQUFLLElBQUksRUFBVixDQUFaLENBQW5DLEdBQWdFLEdBQXhGLENBQVA7QUFDQSxPQW5DbUMsQ0FxQ3BDOzs7QUFDQSxVQUFJMmUsS0FBSixFQUFXO0FBQ1ZoZixZQUFJLENBQUN5UCxXQUFMLENBQWlCc1AsWUFBakIsRUFBK0IxZSxLQUEvQixJQUF3Q21VLElBQXhDO0FBQ0E7O0FBRUQsYUFBT0EsSUFBUDtBQUNBLEtBcjdENEI7O0FBdTdEN0I7Ozs7Ozs7QUFPQTBLLGNBQVUsRUFBRSxvQkFBU0gsWUFBVCxFQUF1QjtBQUNsQyxVQUFJL2UsSUFBSSxHQUFHLElBQVg7O0FBQ0EsVUFBSSxPQUFPK2UsWUFBUCxLQUF3QixXQUE1QixFQUF5QztBQUN4Qy9lLFlBQUksQ0FBQ3lQLFdBQUwsR0FBbUIsRUFBbkI7QUFDQSxPQUZELE1BRU87QUFDTixlQUFPelAsSUFBSSxDQUFDeVAsV0FBTCxDQUFpQnNQLFlBQWpCLENBQVA7QUFDQTtBQUNELEtBcjhENEI7O0FBdThEN0I7Ozs7Ozs7QUFPQWxFLGFBQVMsRUFBRSxtQkFBU3hRLEtBQVQsRUFBZ0I7QUFDMUIsVUFBSXJLLElBQUksR0FBRyxJQUFYO0FBQ0EsVUFBSSxDQUFDQSxJQUFJLENBQUNsQyxRQUFMLENBQWN3WCxNQUFuQixFQUEyQixPQUFPLEtBQVA7QUFDM0IsVUFBSTNTLE1BQU0sR0FBRzNDLElBQUksQ0FBQ2xDLFFBQUwsQ0FBY3FoQixZQUEzQjtBQUNBLGFBQU85VSxLQUFLLENBQUMvTCxNQUFOLEtBQ0YsT0FBT3FFLE1BQVAsS0FBa0IsVUFBbEIsSUFBZ0NBLE1BQU0sQ0FBQy9DLEtBQVAsQ0FBYUksSUFBYixFQUFtQixDQUFDcUssS0FBRCxDQUFuQixDQUQ5QixNQUVGLE9BQU8xSCxNQUFQLEtBQWtCLFFBQWxCLElBQThCLElBQUl6RCxNQUFKLENBQVd5RCxNQUFYLEVBQW1Cd0QsSUFBbkIsQ0FBd0JrRSxLQUF4QixDQUY1QixNQUdGLEVBQUUxSCxNQUFNLFlBQVl6RCxNQUFwQixLQUErQnlELE1BQU0sQ0FBQ3dELElBQVAsQ0FBWWtFLEtBQVosQ0FIN0IsQ0FBUDtBQUlBO0FBdDlENEIsR0FBOUI7QUEyOURBNEMsV0FBUyxDQUFDYSxLQUFWLEdBQWtCLENBQWxCO0FBQ0FiLFdBQVMsQ0FBQ21TLFFBQVYsR0FBcUI7QUFDcEJyZixXQUFPLEVBQUUsRUFEVztBQUVwQndQLGFBQVMsRUFBRSxFQUZTO0FBSXBCdEwsV0FBTyxFQUFFLEVBSlc7QUFLcEI0TixhQUFTLEVBQUUsR0FMUztBQU1wQkQsV0FBTyxFQUFFLElBTlc7QUFNTDtBQUNma0wsV0FBTyxFQUFFLElBUFc7QUFRcEIvZSxjQUFVLEVBQUUsSUFSUTtBQVNwQnVYLFVBQU0sRUFBRSxLQVRZO0FBVXBCc0IsZ0JBQVksRUFBRSxLQVZNO0FBV3BCdUksZ0JBQVksRUFBRSxJQVhNO0FBWXBCcGEsYUFBUyxFQUFFLElBWlM7QUFhcEJzUixlQUFXLEVBQUUsSUFiTztBQWNwQmlFLGNBQVUsRUFBRSxJQWRRO0FBZXBCdEssWUFBUSxFQUFFLElBZlU7QUFnQnBCQyxnQkFBWSxFQUFFLElBaEJNO0FBaUJwQjhLLGlCQUFhLEVBQUUsS0FqQks7QUFrQnBCaEYsZUFBVyxFQUFFLEtBbEJPO0FBbUJwQjdCLFdBQU8sRUFBRSxLQW5CVztBQW9CcEJtTCxvQkFBZ0IsRUFBRSxLQXBCRTtBQXFCcEJ0SSxvQkFBZ0IsRUFBRSxLQXJCRTtBQXVCcEJvQyxrQkFBYyxFQUFFLEVBdkJJO0FBd0JwQnhKLGdCQUFZLEVBQUUsR0F4Qk07QUF5QnBCd0gsZ0JBQVksRUFBRSxTQXpCTTtBQTJCcEJtSSxZQUFRLEVBQUUsV0EzQlU7QUE0QnBCOUUsaUJBQWEsRUFBRSxVQTVCSztBQTZCcEJTLGNBQVUsRUFBRSxPQTdCUTtBQThCcEI3RyxjQUFVLEVBQUUsTUE5QlE7QUErQnBCRSxzQkFBa0IsRUFBRSxPQS9CQTtBQWdDcEI0RyxzQkFBa0IsRUFBRSxPQWhDQTtBQWlDcEJULHFCQUFpQixFQUFFLEtBakNDO0FBbUNwQm5CLGFBQVMsRUFBRSxRQW5DUztBQW9DcEJDLGVBQVcsRUFBRSxDQUFDLE1BQUQsQ0FwQ087QUFxQ3BCQyxxQkFBaUIsRUFBRSxLQXJDQztBQXVDcEJ6SixRQUFJLEVBQUUsSUF2Q2M7QUF3Q3BCb0IsZ0JBQVksRUFBRSxtQkF4Q007QUF5Q3BCQyxjQUFVLEVBQUUsaUJBekNRO0FBMENwQkUsaUJBQWEsRUFBRSxvQkExQ0s7QUEyQ3BCRSx3QkFBb0IsRUFBRSw0QkEzQ0Y7QUE2Q3BCSCxrQkFBYyxFQUFFLElBN0NJO0FBK0NwQkkseUJBQXFCLEVBQUUsSUEvQ0g7O0FBaURwQjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9CQWlELFVBQU0sRUFBRTtBQUNQOzs7Ozs7O0FBRE87QUFyRVksR0FBckI7O0FBaUZBdlIsR0FBQyxDQUFDMEIsRUFBRixDQUFLc0ksU0FBTCxHQUFpQixVQUFTb1MsYUFBVCxFQUF3QjtBQUN4QyxRQUFJSCxRQUFRLEdBQWVqYyxDQUFDLENBQUMwQixFQUFGLENBQUtzSSxTQUFMLENBQWVpUyxRQUExQztBQUNBLFFBQUl0aEIsUUFBUSxHQUFlcUYsQ0FBQyxDQUFDakIsTUFBRixDQUFTLEVBQVQsRUFBYWtkLFFBQWIsRUFBdUJHLGFBQXZCLENBQTNCO0FBQ0EsUUFBSUMsU0FBUyxHQUFjMWhCLFFBQVEsQ0FBQ3doQixRQUFwQztBQUNBLFFBQUluTCxXQUFXLEdBQVlyVyxRQUFRLENBQUNzVyxVQUFwQztBQUNBLFFBQUlxTCxXQUFXLEdBQVkzaEIsUUFBUSxDQUFDbWQsVUFBcEM7QUFDQSxRQUFJNUcsY0FBYyxHQUFTdlcsUUFBUSxDQUFDMGMsYUFBcEM7QUFDQSxRQUFJa0Ysb0JBQW9CLEdBQUc1aEIsUUFBUSxDQUFDd1csa0JBQXBDO0FBQ0EsUUFBSXFMLG9CQUFvQixHQUFHN2hCLFFBQVEsQ0FBQ29kLGtCQUFwQztBQUVBOzs7Ozs7O0FBTUEsUUFBSTBFLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQVMzVCxNQUFULEVBQWlCNFQsZ0JBQWpCLEVBQW1DO0FBQ3JELFVBQUl0aEIsQ0FBSixFQUFPQyxDQUFQLEVBQVVnZSxNQUFWLEVBQWtCM0MsTUFBbEI7QUFFQSxVQUFJaUcsUUFBUSxHQUFHN1QsTUFBTSxDQUFDYyxJQUFQLENBQVl5UyxTQUFaLENBQWY7O0FBRUEsVUFBSSxDQUFDTSxRQUFMLEVBQWU7QUFDZCxZQUFJemYsS0FBSyxHQUFHOEMsQ0FBQyxDQUFDaEYsSUFBRixDQUFPOE4sTUFBTSxDQUFDVSxHQUFQLE1BQWdCLEVBQXZCLENBQVo7QUFDQSxZQUFJLENBQUM3TyxRQUFRLENBQUN1aEIsZ0JBQVYsSUFBOEIsQ0FBQ2hmLEtBQUssQ0FBQy9CLE1BQXpDLEVBQWlEO0FBQ2pEa2UsY0FBTSxHQUFHbmMsS0FBSyxDQUFDeEIsS0FBTixDQUFZZixRQUFRLENBQUMrVCxTQUFyQixDQUFUOztBQUNBLGFBQUt0VCxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEdBQUdnZSxNQUFNLENBQUNsZSxNQUF2QixFQUErQkMsQ0FBQyxHQUFHQyxDQUFuQyxFQUFzQ0QsQ0FBQyxFQUF2QyxFQUEyQztBQUMxQ3NiLGdCQUFNLEdBQUcsRUFBVDtBQUNBQSxnQkFBTSxDQUFDMUYsV0FBRCxDQUFOLEdBQXNCcUksTUFBTSxDQUFDamUsQ0FBRCxDQUE1QjtBQUNBc2IsZ0JBQU0sQ0FBQzRGLFdBQUQsQ0FBTixHQUFzQmpELE1BQU0sQ0FBQ2plLENBQUQsQ0FBNUI7QUFDQXNoQiwwQkFBZ0IsQ0FBQzlmLE9BQWpCLENBQXlCWixJQUF6QixDQUE4QjBhLE1BQTlCO0FBQ0E7O0FBQ0RnRyx3QkFBZ0IsQ0FBQ2hpQixLQUFqQixHQUF5QjJlLE1BQXpCO0FBQ0EsT0FYRCxNQVdPO0FBQ05xRCx3QkFBZ0IsQ0FBQzlmLE9BQWpCLEdBQTJCZ2dCLElBQUksQ0FBQ0MsS0FBTCxDQUFXRixRQUFYLENBQTNCOztBQUNBLGFBQUt2aEIsQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxHQUFHcWhCLGdCQUFnQixDQUFDOWYsT0FBakIsQ0FBeUJ6QixNQUF6QyxFQUFpREMsQ0FBQyxHQUFHQyxDQUFyRCxFQUF3REQsQ0FBQyxFQUF6RCxFQUE2RDtBQUM1RHNoQiwwQkFBZ0IsQ0FBQ2hpQixLQUFqQixDQUF1QnNCLElBQXZCLENBQTRCMGdCLGdCQUFnQixDQUFDOWYsT0FBakIsQ0FBeUJ4QixDQUF6QixFQUE0QmtoQixXQUE1QixDQUE1QjtBQUNBO0FBQ0Q7QUFDRCxLQXRCRDtBQXdCQTs7Ozs7Ozs7QUFNQSxRQUFJUSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxDQUFTaFUsTUFBVCxFQUFpQjRULGdCQUFqQixFQUFtQztBQUNwRCxVQUFJdGhCLENBQUo7QUFBQSxVQUFPQyxDQUFQO0FBQUEsVUFBVTRILE9BQVY7QUFBQSxVQUFtQmtOLFNBQW5CO0FBQUEsVUFBOEI3RixLQUFLLEdBQUcsQ0FBdEM7QUFDQSxVQUFJMU4sT0FBTyxHQUFHOGYsZ0JBQWdCLENBQUM5ZixPQUEvQjtBQUNBLFVBQUltZ0IsVUFBVSxHQUFHLEVBQWpCOztBQUVBLFVBQUlDLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQVNyQyxHQUFULEVBQWM7QUFDNUIsWUFBSW5kLElBQUksR0FBRzZlLFNBQVMsSUFBSTFCLEdBQUcsQ0FBQy9RLElBQUosQ0FBU3lTLFNBQVQsQ0FBeEI7O0FBQ0EsWUFBSSxPQUFPN2UsSUFBUCxLQUFnQixRQUFoQixJQUE0QkEsSUFBSSxDQUFDckMsTUFBckMsRUFBNkM7QUFDNUMsaUJBQU95aEIsSUFBSSxDQUFDQyxLQUFMLENBQVdyZixJQUFYLENBQVA7QUFDQTs7QUFDRCxlQUFPLElBQVA7QUFDQSxPQU5EOztBQVFBLFVBQUk0VyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFTVixPQUFULEVBQWtCdUosS0FBbEIsRUFBeUI7QUFDeEN2SixlQUFPLEdBQUcxVCxDQUFDLENBQUMwVCxPQUFELENBQVg7QUFFQSxZQUFJeFcsS0FBSyxHQUFHb0ksUUFBUSxDQUFDb08sT0FBTyxDQUFDOUosSUFBUixDQUFhLE9BQWIsQ0FBRCxDQUFwQjtBQUNBLFlBQUksQ0FBQzFNLEtBQUQsSUFBVSxDQUFDdkMsUUFBUSxDQUFDdWhCLGdCQUF4QixFQUEwQyxPQUpGLENBTXhDO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFlBQUlhLFVBQVUsQ0FBQ2xoQixjQUFYLENBQTBCcUIsS0FBMUIsQ0FBSixFQUFzQztBQUNyQyxjQUFJK2YsS0FBSixFQUFXO0FBQ1YsZ0JBQUlDLEdBQUcsR0FBR0gsVUFBVSxDQUFDN2YsS0FBRCxDQUFWLENBQWtCZ1UsY0FBbEIsQ0FBVjs7QUFDQSxnQkFBSSxDQUFDZ00sR0FBTCxFQUFVO0FBQ1RILHdCQUFVLENBQUM3ZixLQUFELENBQVYsQ0FBa0JnVSxjQUFsQixJQUFvQytMLEtBQXBDO0FBQ0EsYUFGRCxNQUVPLElBQUksQ0FBQ2pkLENBQUMsQ0FBQ0QsT0FBRixDQUFVbWQsR0FBVixDQUFMLEVBQXFCO0FBQzNCSCx3QkFBVSxDQUFDN2YsS0FBRCxDQUFWLENBQWtCZ1UsY0FBbEIsSUFBb0MsQ0FBQ2dNLEdBQUQsRUFBTUQsS0FBTixDQUFwQztBQUNBLGFBRk0sTUFFQTtBQUNOQyxpQkFBRyxDQUFDbGhCLElBQUosQ0FBU2loQixLQUFUO0FBQ0E7QUFDRDs7QUFDRDtBQUNBOztBQUVELFlBQUl2RyxNQUFNLEdBQWVzRyxRQUFRLENBQUN0SixPQUFELENBQVIsSUFBcUIsRUFBOUM7QUFDQWdELGNBQU0sQ0FBQzFGLFdBQUQsQ0FBTixHQUF5QjBGLE1BQU0sQ0FBQzFGLFdBQUQsQ0FBTixJQUF1QjBDLE9BQU8sQ0FBQy9MLElBQVIsRUFBaEQ7QUFDQStPLGNBQU0sQ0FBQzRGLFdBQUQsQ0FBTixHQUF5QjVGLE1BQU0sQ0FBQzRGLFdBQUQsQ0FBTixJQUF1QnBmLEtBQWhEO0FBQ0F3WixjQUFNLENBQUN4RixjQUFELENBQU4sR0FBeUJ3RixNQUFNLENBQUN4RixjQUFELENBQU4sSUFBMEIrTCxLQUFuRDtBQUVBRixrQkFBVSxDQUFDN2YsS0FBRCxDQUFWLEdBQW9Cd1osTUFBcEI7QUFDQTlaLGVBQU8sQ0FBQ1osSUFBUixDQUFhMGEsTUFBYjs7QUFFQSxZQUFJaEQsT0FBTyxDQUFDMUksRUFBUixDQUFXLFdBQVgsQ0FBSixFQUE2QjtBQUM1QjBSLDBCQUFnQixDQUFDaGlCLEtBQWpCLENBQXVCc0IsSUFBdkIsQ0FBNEJrQixLQUE1QjtBQUNBO0FBQ0QsT0FuQ0Q7O0FBcUNBLFVBQUlpZ0IsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBU0MsU0FBVCxFQUFvQjtBQUNsQyxZQUFJaGlCLENBQUosRUFBT0MsQ0FBUCxFQUFVaUQsRUFBVixFQUFjc1ksUUFBZCxFQUF3Qm9DLFFBQXhCO0FBRUFvRSxpQkFBUyxHQUFHcGQsQ0FBQyxDQUFDb2QsU0FBRCxDQUFiO0FBQ0E5ZSxVQUFFLEdBQUc4ZSxTQUFTLENBQUN4VCxJQUFWLENBQWUsT0FBZixDQUFMOztBQUVBLFlBQUl0TCxFQUFKLEVBQVE7QUFDUHNZLGtCQUFRLEdBQUdvRyxRQUFRLENBQUNJLFNBQUQsQ0FBUixJQUF1QixFQUFsQztBQUNBeEcsa0JBQVEsQ0FBQzJGLG9CQUFELENBQVIsR0FBaUNqZSxFQUFqQztBQUNBc1ksa0JBQVEsQ0FBQzRGLG9CQUFELENBQVIsR0FBaUNsZSxFQUFqQztBQUNBb2UsMEJBQWdCLENBQUN0USxTQUFqQixDQUEyQnBRLElBQTNCLENBQWdDNGEsUUFBaEM7QUFDQTs7QUFFRG9DLGdCQUFRLEdBQUdoWixDQUFDLENBQUMsUUFBRCxFQUFXb2QsU0FBWCxDQUFaOztBQUNBLGFBQUtoaUIsQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxHQUFHMmQsUUFBUSxDQUFDN2QsTUFBekIsRUFBaUNDLENBQUMsR0FBR0MsQ0FBckMsRUFBd0NELENBQUMsRUFBekMsRUFBNkM7QUFDNUNnWixtQkFBUyxDQUFDNEUsUUFBUSxDQUFDNWQsQ0FBRCxDQUFULEVBQWNrRCxFQUFkLENBQVQ7QUFDQTtBQUNELE9BakJEOztBQW1CQW9lLHNCQUFnQixDQUFDN1AsUUFBakIsR0FBNEIvRCxNQUFNLENBQUNjLElBQVAsQ0FBWSxVQUFaLElBQTBCLElBQTFCLEdBQWlDLENBQTdEO0FBRUF1RyxlQUFTLEdBQUdySCxNQUFNLENBQUNzSCxRQUFQLEVBQVo7O0FBQ0EsV0FBS2hWLENBQUMsR0FBRyxDQUFKLEVBQU9DLENBQUMsR0FBRzhVLFNBQVMsQ0FBQ2hWLE1BQTFCLEVBQWtDQyxDQUFDLEdBQUdDLENBQXRDLEVBQXlDRCxDQUFDLEVBQTFDLEVBQThDO0FBQzdDNkgsZUFBTyxHQUFHa04sU0FBUyxDQUFDL1UsQ0FBRCxDQUFULENBQWE2SCxPQUFiLENBQXFCL0gsV0FBckIsRUFBVjs7QUFDQSxZQUFJK0gsT0FBTyxLQUFLLFVBQWhCLEVBQTRCO0FBQzNCa2Esa0JBQVEsQ0FBQ2hOLFNBQVMsQ0FBQy9VLENBQUQsQ0FBVixDQUFSO0FBQ0EsU0FGRCxNQUVPLElBQUk2SCxPQUFPLEtBQUssUUFBaEIsRUFBMEI7QUFDaENtUixtQkFBUyxDQUFDakUsU0FBUyxDQUFDL1UsQ0FBRCxDQUFWLENBQVQ7QUFDQTtBQUNEO0FBQ0QsS0FoRkQ7O0FBa0ZBLFdBQU8sS0FBSzhILElBQUwsQ0FBVSxZQUFXO0FBQzNCLFVBQUksS0FBSzhHLFNBQVQsRUFBb0I7QUFFcEIsVUFBSXFULFFBQUo7QUFDQSxVQUFJdlUsTUFBTSxHQUFHOUksQ0FBQyxDQUFDLElBQUQsQ0FBZDtBQUNBLFVBQUlzZCxRQUFRLEdBQUcsS0FBS3JhLE9BQUwsQ0FBYS9ILFdBQWIsRUFBZjtBQUNBLFVBQUlpTyxXQUFXLEdBQUdMLE1BQU0sQ0FBQ2MsSUFBUCxDQUFZLGFBQVosS0FBOEJkLE1BQU0sQ0FBQ2MsSUFBUCxDQUFZLGtCQUFaLENBQWhEOztBQUNBLFVBQUksQ0FBQ1QsV0FBRCxJQUFnQixDQUFDeE8sUUFBUSxDQUFDdWhCLGdCQUE5QixFQUFnRDtBQUMvQy9TLG1CQUFXLEdBQUdMLE1BQU0sQ0FBQ3NILFFBQVAsQ0FBZ0Isa0JBQWhCLEVBQW9DekksSUFBcEMsRUFBZDtBQUNBOztBQUVELFVBQUkrVSxnQkFBZ0IsR0FBRztBQUN0Qix1QkFBZ0J2VCxXQURNO0FBRXRCLG1CQUFnQixFQUZNO0FBR3RCLHFCQUFnQixFQUhNO0FBSXRCLGlCQUFnQjtBQUpNLE9BQXZCOztBQU9BLFVBQUltVSxRQUFRLEtBQUssUUFBakIsRUFBMkI7QUFDMUJSLG1CQUFXLENBQUNoVSxNQUFELEVBQVM0VCxnQkFBVCxDQUFYO0FBQ0EsT0FGRCxNQUVPO0FBQ05ELG9CQUFZLENBQUMzVCxNQUFELEVBQVM0VCxnQkFBVCxDQUFaO0FBQ0E7O0FBRURXLGNBQVEsR0FBRyxJQUFJdlQsU0FBSixDQUFjaEIsTUFBZCxFQUFzQjlJLENBQUMsQ0FBQ2pCLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQmtkLFFBQW5CLEVBQTZCUyxnQkFBN0IsRUFBK0NOLGFBQS9DLENBQXRCLENBQVg7QUFDQSxLQXpCTSxDQUFQO0FBMEJBLEdBMUpEOztBQTRKQXBjLEdBQUMsQ0FBQzBCLEVBQUYsQ0FBS3NJLFNBQUwsQ0FBZWlTLFFBQWYsR0FBMEJuUyxTQUFTLENBQUNtUyxRQUFwQztBQUNBamMsR0FBQyxDQUFDMEIsRUFBRixDQUFLc0ksU0FBTCxDQUFldVQsT0FBZixHQUF5QjtBQUN4Qm5ZLFlBQVEsRUFBRUY7QUFEYyxHQUF6QjtBQUtBNEUsV0FBUyxDQUFDdFAsTUFBVixDQUFpQixXQUFqQixFQUE4QixVQUFTb0MsT0FBVCxFQUFrQjtBQUMvQyxRQUFJLENBQUNvRCxDQUFDLENBQUMwQixFQUFGLENBQUs4YixRQUFWLEVBQW9CLE1BQU0sSUFBSS9iLEtBQUosQ0FBVSx1REFBVixDQUFOO0FBQ3BCLFFBQUksS0FBSzlHLFFBQUwsQ0FBY2lTLElBQWQsS0FBdUIsT0FBM0IsRUFBb0M7QUFDcEMsUUFBSS9QLElBQUksR0FBRyxJQUFYOztBQUVBQSxRQUFJLENBQUNnZCxJQUFMLEdBQWEsWUFBVztBQUN2QixVQUFJalUsUUFBUSxHQUFHL0ksSUFBSSxDQUFDZ2QsSUFBcEI7QUFDQSxhQUFPLFlBQVc7QUFDakIsWUFBSTJELFFBQVEsR0FBRzNnQixJQUFJLENBQUN3USxRQUFMLENBQWM3UCxJQUFkLENBQW1CLFVBQW5CLENBQWY7QUFDQSxZQUFJZ2dCLFFBQUosRUFBY0EsUUFBUSxDQUFDM00sT0FBVDtBQUNkLGVBQU9qTCxRQUFRLENBQUNuSixLQUFULENBQWVJLElBQWYsRUFBcUJnRCxTQUFyQixDQUFQO0FBQ0EsT0FKRDtBQUtBLEtBUFcsRUFBWjs7QUFTQWhELFFBQUksQ0FBQ2lkLE1BQUwsR0FBZSxZQUFXO0FBQ3pCLFVBQUlsVSxRQUFRLEdBQUcvSSxJQUFJLENBQUNpZCxNQUFwQjtBQUNBLGFBQU8sWUFBVztBQUNqQixZQUFJMEQsUUFBUSxHQUFHM2dCLElBQUksQ0FBQ3dRLFFBQUwsQ0FBYzdQLElBQWQsQ0FBbUIsVUFBbkIsQ0FBZjtBQUNBLFlBQUlnZ0IsUUFBSixFQUFjQSxRQUFRLENBQUNoQyxNQUFUO0FBQ2QsZUFBTzVWLFFBQVEsQ0FBQ25KLEtBQVQsQ0FBZUksSUFBZixFQUFxQmdELFNBQXJCLENBQVA7QUFDQSxPQUpEO0FBS0EsS0FQYSxFQUFkOztBQVNBaEQsUUFBSSxDQUFDb1EsS0FBTCxHQUFjLFlBQVc7QUFDeEIsVUFBSXJILFFBQVEsR0FBRy9JLElBQUksQ0FBQ29RLEtBQXBCO0FBQ0EsYUFBTyxZQUFXO0FBQ2pCckgsZ0JBQVEsQ0FBQ25KLEtBQVQsQ0FBZSxJQUFmLEVBQXFCb0QsU0FBckI7QUFFQSxZQUFJd04sUUFBUSxHQUFHeFEsSUFBSSxDQUFDd1EsUUFBTCxDQUFjbVEsUUFBZCxDQUF1QjtBQUNyQzlpQixlQUFLLEVBQUUsY0FEOEI7QUFFckMraUIsOEJBQW9CLEVBQUUsSUFGZTtBQUdyQ0Msa0JBQVEsRUFBRTdnQixJQUFJLENBQUNxTyxRQUhzQjtBQUlyQy9ELGVBQUssRUFBRSxlQUFTTixDQUFULEVBQVk4VyxFQUFaLEVBQWdCO0FBQ3RCQSxjQUFFLENBQUN4VSxXQUFILENBQWVqQixHQUFmLENBQW1CLE9BQW5CLEVBQTRCeVYsRUFBRSxDQUFDQyxNQUFILENBQVUxVixHQUFWLENBQWMsT0FBZCxDQUE1QjtBQUNBbUYsb0JBQVEsQ0FBQ25GLEdBQVQsQ0FBYTtBQUFDMlYsc0JBQVEsRUFBRTtBQUFYLGFBQWI7QUFDQSxXQVBvQztBQVFyQzlILGNBQUksRUFBRSxnQkFBVztBQUNoQjFJLG9CQUFRLENBQUNuRixHQUFULENBQWE7QUFBQzJWLHNCQUFRLEVBQUU7QUFBWCxhQUFiO0FBQ0EsZ0JBQUl0RSxNQUFNLEdBQUcxYyxJQUFJLENBQUNzUCxZQUFMLEdBQW9CdFAsSUFBSSxDQUFDc1AsWUFBTCxDQUFrQnpNLEtBQWxCLEVBQXBCLEdBQWdELElBQTdEO0FBQ0EsZ0JBQUkyWixNQUFNLEdBQUcsRUFBYjtBQUNBaE0sb0JBQVEsQ0FBQytDLFFBQVQsQ0FBa0IsY0FBbEIsRUFBa0NsTixJQUFsQyxDQUF1QyxZQUFXO0FBQ2pEbVcsb0JBQU0sQ0FBQ3JkLElBQVAsQ0FBWWdFLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTRKLElBQVIsQ0FBYSxZQUFiLENBQVo7QUFDQSxhQUZEO0FBR0EvTSxnQkFBSSxDQUFDMFQsUUFBTCxDQUFjOEksTUFBZDtBQUNBeGMsZ0JBQUksQ0FBQ2lWLGFBQUwsQ0FBbUJ5SCxNQUFuQjtBQUNBO0FBakJvQyxTQUF2QixDQUFmO0FBbUJBLE9BdEJEO0FBdUJBLEtBekJZLEVBQWI7QUEyQkEsR0FsREQ7QUFvREF6UCxXQUFTLENBQUN0UCxNQUFWLENBQWlCLGlCQUFqQixFQUFvQyxVQUFTb0MsT0FBVCxFQUFrQjtBQUNyRCxRQUFJQyxJQUFJLEdBQUcsSUFBWDtBQUVBRCxXQUFPLEdBQUdvRCxDQUFDLENBQUNqQixNQUFGLENBQVM7QUFDbEIrZSxXQUFLLEVBQVcsVUFERTtBQUVsQkMsaUJBQVcsRUFBSywyQkFGRTtBQUdsQkMsbUJBQWEsRUFBRyxpQ0FIRTtBQUlsQkMsZ0JBQVUsRUFBTSxpQ0FKRTtBQUtsQkMsZ0JBQVUsRUFBTSxpQ0FMRTtBQU9sQjdNLFVBQUksRUFBRSxjQUFTN1QsSUFBVCxFQUFlO0FBQ3BCLGVBQ0MsaUJBQWlCQSxJQUFJLENBQUN1Z0IsV0FBdEIsR0FBb0MsSUFBcEMsR0FDQyxjQURELEdBQ2tCdmdCLElBQUksQ0FBQ3dnQixhQUR2QixHQUN1QyxJQUR2QyxHQUVFLGVBRkYsR0FFb0J4Z0IsSUFBSSxDQUFDeWdCLFVBRnpCLEdBRXNDLElBRnRDLEdBRTZDemdCLElBQUksQ0FBQ3NnQixLQUZsRCxHQUUwRCxTQUYxRCxHQUdFLHNDQUhGLEdBRzJDdGdCLElBQUksQ0FBQzBnQixVQUhoRCxHQUc2RCxlQUg3RCxHQUlDLFFBSkQsR0FLQSxRQU5EO0FBUUE7QUFoQmlCLEtBQVQsRUFpQlB0aEIsT0FqQk8sQ0FBVjs7QUFtQkFDLFFBQUksQ0FBQ29RLEtBQUwsR0FBYyxZQUFXO0FBQ3hCLFVBQUlySCxRQUFRLEdBQUcvSSxJQUFJLENBQUNvUSxLQUFwQjtBQUNBLGFBQU8sWUFBVztBQUNqQnJILGdCQUFRLENBQUNuSixLQUFULENBQWVJLElBQWYsRUFBcUJnRCxTQUFyQjtBQUNBaEQsWUFBSSxDQUFDc2hCLGdCQUFMLEdBQXdCbmUsQ0FBQyxDQUFDcEQsT0FBTyxDQUFDeVUsSUFBUixDQUFhelUsT0FBYixDQUFELENBQXpCO0FBQ0FDLFlBQUksQ0FBQzBRLFNBQUwsQ0FBZW9LLE9BQWYsQ0FBdUI5YSxJQUFJLENBQUNzaEIsZ0JBQTVCO0FBQ0EsT0FKRDtBQUtBLEtBUFksRUFBYjtBQVNBLEdBL0JEO0FBaUNBclUsV0FBUyxDQUFDdFAsTUFBVixDQUFpQixrQkFBakIsRUFBcUMsVUFBU29DLE9BQVQsRUFBa0I7QUFDdEQsUUFBSUMsSUFBSSxHQUFHLElBQVg7QUFFQUQsV0FBTyxHQUFHb0QsQ0FBQyxDQUFDakIsTUFBRixDQUFTO0FBQ2xCcWYsbUJBQWEsRUFBSSxJQURDO0FBRWxCQyxvQkFBYyxFQUFHO0FBRkMsS0FBVCxFQUdQemhCLE9BSE8sQ0FBVjs7QUFLQSxTQUFLNFYsaUJBQUwsR0FBeUIsVUFBU2tCLE9BQVQsRUFBa0JsVixTQUFsQixFQUE2QjtBQUNyRCxVQUFJd2EsUUFBUSxHQUFHdEYsT0FBTyxDQUFDNEssT0FBUixDQUFnQixjQUFoQixFQUFnQ3pHLElBQWhDLENBQXFDLG1CQUFyQyxDQUFmO0FBQ0EsVUFBSW9CLEtBQUssR0FBTUQsUUFBUSxDQUFDQyxLQUFULENBQWV2RixPQUFmLElBQTBCbFYsU0FBekM7QUFFQSxhQUFPeWEsS0FBSyxJQUFJLENBQVQsSUFBY0EsS0FBSyxHQUFHRCxRQUFRLENBQUM3ZCxNQUEvQixHQUF3QzZkLFFBQVEsQ0FBQ0UsRUFBVCxDQUFZRCxLQUFaLENBQXhDLEdBQTZEalosQ0FBQyxFQUFyRTtBQUNBLEtBTEQ7O0FBT0EsU0FBS3FQLFNBQUwsR0FBa0IsWUFBVztBQUM1QixVQUFJekosUUFBUSxHQUFHL0ksSUFBSSxDQUFDd1MsU0FBcEI7QUFDQSxhQUFPLFVBQVN4SSxDQUFULEVBQVk7QUFDbEIsWUFBSW9TLEtBQUosRUFBV3ZGLE9BQVgsRUFBb0JzRixRQUFwQixFQUE4Qm9FLFNBQTlCOztBQUVBLFlBQUksS0FBS3ZTLE1BQUwsS0FBZ0JoRSxDQUFDLENBQUNvQyxPQUFGLEtBQWM3RSxRQUFkLElBQTBCeUMsQ0FBQyxDQUFDb0MsT0FBRixLQUFjMUUsU0FBeEQsQ0FBSixFQUF3RTtBQUN2RTFILGNBQUksQ0FBQzhPLFdBQUwsR0FBbUIsSUFBbkI7QUFDQXlSLG1CQUFTLEdBQUcsS0FBS2xSLGFBQUwsQ0FBbUJvUyxPQUFuQixDQUEyQixjQUEzQixDQUFaO0FBQ0FyRixlQUFLLEdBQUdtRSxTQUFTLENBQUN2RixJQUFWLENBQWUsbUJBQWYsRUFBb0NvQixLQUFwQyxDQUEwQyxLQUFLL00sYUFBL0MsQ0FBUjs7QUFFQSxjQUFHckYsQ0FBQyxDQUFDb0MsT0FBRixLQUFjN0UsUUFBakIsRUFBMkI7QUFDMUJnWixxQkFBUyxHQUFHQSxTQUFTLENBQUNtQixJQUFWLENBQWUsY0FBZixDQUFaO0FBQ0EsV0FGRCxNQUVPO0FBQ05uQixxQkFBUyxHQUFHQSxTQUFTLENBQUNvQixJQUFWLENBQWUsY0FBZixDQUFaO0FBQ0E7O0FBRUR4RixrQkFBUSxHQUFHb0UsU0FBUyxDQUFDdkYsSUFBVixDQUFlLG1CQUFmLENBQVg7QUFDQW5FLGlCQUFPLEdBQUlzRixRQUFRLENBQUNFLEVBQVQsQ0FBWWhGLElBQUksQ0FBQ2tELEdBQUwsQ0FBUzRCLFFBQVEsQ0FBQzdkLE1BQVQsR0FBa0IsQ0FBM0IsRUFBOEI4ZCxLQUE5QixDQUFaLENBQVg7O0FBQ0EsY0FBSXZGLE9BQU8sQ0FBQ3ZZLE1BQVosRUFBb0I7QUFDbkIsaUJBQUtzWCxlQUFMLENBQXFCaUIsT0FBckI7QUFDQTs7QUFDRDtBQUNBOztBQUVELGVBQU85TixRQUFRLENBQUNuSixLQUFULENBQWUsSUFBZixFQUFxQm9ELFNBQXJCLENBQVA7QUFDQSxPQXZCRDtBQXdCQSxLQTFCZ0IsRUFBakI7O0FBNEJBLFFBQUk0ZSxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLEdBQVc7QUFDbEMsVUFBSUMsR0FBSjtBQUNBLFVBQUlsVyxLQUFLLEdBQUdpVyxpQkFBaUIsQ0FBQ2pXLEtBQTlCO0FBQ0EsVUFBSW1XLEdBQUcsR0FBR3ZjLFFBQVY7O0FBRUEsVUFBSSxPQUFPb0csS0FBUCxLQUFpQixXQUFyQixFQUFrQztBQUNqQ2tXLFdBQUcsR0FBR0MsR0FBRyxDQUFDdGMsYUFBSixDQUFrQixLQUFsQixDQUFOO0FBQ0FxYyxXQUFHLENBQUNFLFNBQUosR0FBZ0IsNklBQWhCO0FBQ0FGLFdBQUcsR0FBR0EsR0FBRyxDQUFDRyxVQUFWO0FBQ0FGLFdBQUcsQ0FBQ25MLElBQUosQ0FBUzVRLFdBQVQsQ0FBcUI4YixHQUFyQjtBQUNBbFcsYUFBSyxHQUFHaVcsaUJBQWlCLENBQUNqVyxLQUFsQixHQUEwQmtXLEdBQUcsQ0FBQ0ksV0FBSixHQUFrQkosR0FBRyxDQUFDSyxXQUF4RDtBQUNBSixXQUFHLENBQUNuTCxJQUFKLENBQVN3TCxXQUFULENBQXFCTixHQUFyQjtBQUNBOztBQUNELGFBQU9sVyxLQUFQO0FBQ0EsS0FkRDs7QUFnQkEsUUFBSXlXLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsR0FBVztBQUM5QixVQUFJN2pCLENBQUosRUFBT0MsQ0FBUCxFQUFVNmpCLFVBQVYsRUFBc0IxVyxLQUF0QixFQUE2QjJXLFVBQTdCLEVBQXlDQyxZQUF6QyxFQUF1REMsVUFBdkQ7QUFFQUEsZ0JBQVUsR0FBR3JmLENBQUMsQ0FBQyxjQUFELEVBQWlCbkQsSUFBSSxDQUFDMlEsaUJBQXRCLENBQWQ7QUFDQW5TLE9BQUMsR0FBR2drQixVQUFVLENBQUNsa0IsTUFBZjtBQUNBLFVBQUksQ0FBQ0UsQ0FBRCxJQUFNLENBQUN3QixJQUFJLENBQUMyUSxpQkFBTCxDQUF1QmhGLEtBQXZCLEVBQVgsRUFBMkM7O0FBRTNDLFVBQUk1TCxPQUFPLENBQUN5aEIsY0FBWixFQUE0QjtBQUMzQmEsa0JBQVUsR0FBRyxDQUFiOztBQUNBLGFBQUs5akIsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHQyxDQUFoQixFQUFtQkQsQ0FBQyxFQUFwQixFQUF3QjtBQUN2QjhqQixvQkFBVSxHQUFHaEwsSUFBSSxDQUFDQyxHQUFMLENBQVMrSyxVQUFULEVBQXFCRyxVQUFVLENBQUNuRyxFQUFYLENBQWM5ZCxDQUFkLEVBQWlCdWEsTUFBakIsRUFBckIsQ0FBYjtBQUNBOztBQUNEMEosa0JBQVUsQ0FBQ25YLEdBQVgsQ0FBZTtBQUFDeU4sZ0JBQU0sRUFBRXVKO0FBQVQsU0FBZjtBQUNBOztBQUVELFVBQUl0aUIsT0FBTyxDQUFDd2hCLGFBQVosRUFBMkI7QUFDMUJnQixvQkFBWSxHQUFHdmlCLElBQUksQ0FBQzJRLGlCQUFMLENBQXVCOFIsVUFBdkIsS0FBc0NiLGlCQUFpQixFQUF0RTtBQUNBalcsYUFBSyxHQUFHMEwsSUFBSSxDQUFDcUwsS0FBTCxDQUFXSCxZQUFZLEdBQUcvakIsQ0FBMUIsQ0FBUjtBQUNBZ2tCLGtCQUFVLENBQUNuWCxHQUFYLENBQWU7QUFBQ00sZUFBSyxFQUFFQTtBQUFSLFNBQWY7O0FBQ0EsWUFBSW5OLENBQUMsR0FBRyxDQUFSLEVBQVc7QUFDVjhqQixvQkFBVSxHQUFHQyxZQUFZLEdBQUc1VyxLQUFLLElBQUluTixDQUFDLEdBQUcsQ0FBUixDQUFqQztBQUNBZ2tCLG9CQUFVLENBQUNuRyxFQUFYLENBQWM3ZCxDQUFDLEdBQUcsQ0FBbEIsRUFBcUI2TSxHQUFyQixDQUF5QjtBQUFDTSxpQkFBSyxFQUFFMlc7QUFBUixXQUF6QjtBQUNBO0FBQ0Q7QUFDRCxLQXhCRDs7QUEwQkEsUUFBSXZpQixPQUFPLENBQUN5aEIsY0FBUixJQUEwQnpoQixPQUFPLENBQUN3aEIsYUFBdEMsRUFBcUQ7QUFDcEQzWSxVQUFJLENBQUNJLEtBQUwsQ0FBVyxJQUFYLEVBQWlCLGtCQUFqQixFQUFxQ29aLGFBQXJDO0FBQ0F4WixVQUFJLENBQUNJLEtBQUwsQ0FBVyxJQUFYLEVBQWlCLGdCQUFqQixFQUFtQ29aLGFBQW5DO0FBQ0E7QUFHRCxHQTNGRDtBQTZGQW5WLFdBQVMsQ0FBQ3RQLE1BQVYsQ0FBaUIsZUFBakIsRUFBa0MsVUFBU29DLE9BQVQsRUFBa0I7QUFDbkQsUUFBSSxLQUFLakMsUUFBTCxDQUFjaVMsSUFBZCxLQUF1QixRQUEzQixFQUFxQztBQUVyQ2hRLFdBQU8sR0FBR29ELENBQUMsQ0FBQ2pCLE1BQUYsQ0FBUztBQUNsQnViLFdBQUssRUFBTyxTQURNO0FBRWxCd0QsV0FBSyxFQUFPLFFBRk07QUFHbEJ4YixlQUFTLEVBQUcsUUFITTtBQUlsQmlaLFlBQU0sRUFBTTtBQUpNLEtBQVQsRUFLUDNlLE9BTE8sQ0FBVjtBQU9BLFFBQUlDLElBQUksR0FBRyxJQUFYO0FBQ0EsUUFBSXdVLElBQUksR0FBRyx5Q0FBeUN6VSxPQUFPLENBQUMwRixTQUFqRCxHQUE2RCx5QkFBN0QsR0FBeUZpRCxXQUFXLENBQUMzSSxPQUFPLENBQUNraEIsS0FBVCxDQUFwRyxHQUFzSCxJQUF0SCxHQUE2SGxoQixPQUFPLENBQUMwZCxLQUFySSxHQUE2SSxNQUF4SjtBQUVBOzs7Ozs7OztBQU9BLFFBQUlpQixNQUFNLEdBQUcsU0FBVEEsTUFBUyxDQUFTaUUsY0FBVCxFQUF5QkMsWUFBekIsRUFBdUM7QUFDbkQsVUFBSXBpQixHQUFHLEdBQUdtaUIsY0FBYyxDQUFDN2lCLE1BQWYsQ0FBc0IsaUJBQXRCLENBQVY7QUFDQSxhQUFPNmlCLGNBQWMsQ0FBQ2pmLFNBQWYsQ0FBeUIsQ0FBekIsRUFBNEJsRCxHQUE1QixJQUFtQ29pQixZQUFuQyxHQUFrREQsY0FBYyxDQUFDamYsU0FBZixDQUF5QmxELEdBQXpCLENBQXpEO0FBQ0EsS0FIRDs7QUFLQSxTQUFLNFAsS0FBTCxHQUFjLFlBQVc7QUFDeEIsVUFBSXJILFFBQVEsR0FBRy9JLElBQUksQ0FBQ29RLEtBQXBCO0FBQ0EsYUFBTyxZQUFXO0FBQ2pCO0FBQ0EsWUFBSXJRLE9BQU8sQ0FBQzJlLE1BQVosRUFBb0I7QUFDbkIsY0FBSW1FLFdBQVcsR0FBRzdpQixJQUFJLENBQUNsQyxRQUFMLENBQWM0VyxNQUFkLENBQXFCaFMsSUFBdkM7O0FBQ0ExQyxjQUFJLENBQUNsQyxRQUFMLENBQWM0VyxNQUFkLENBQXFCaFMsSUFBckIsR0FBNEIsVUFBUy9CLElBQVQsRUFBZTtBQUMxQyxtQkFBTytkLE1BQU0sQ0FBQ21FLFdBQVcsQ0FBQ2pqQixLQUFaLENBQWtCLElBQWxCLEVBQXdCb0QsU0FBeEIsQ0FBRCxFQUFxQ3dSLElBQXJDLENBQWI7QUFDQSxXQUZEO0FBR0E7O0FBRUR6TCxnQkFBUSxDQUFDbkosS0FBVCxDQUFlLElBQWYsRUFBcUJvRCxTQUFyQixFQVRpQixDQVdqQjs7QUFDQSxhQUFLd04sUUFBTCxDQUFjakssRUFBZCxDQUFpQixPQUFqQixFQUEwQixNQUFNeEcsT0FBTyxDQUFDMEYsU0FBeEMsRUFBbUQsVUFBU3VFLENBQVQsRUFBWTtBQUM5REEsV0FBQyxDQUFDMkosY0FBRjtBQUNBLGNBQUkzVCxJQUFJLENBQUNxTyxRQUFULEVBQW1CO0FBRW5CLGNBQUkwSixLQUFLLEdBQUc1VSxDQUFDLENBQUM2RyxDQUFDLENBQUNHLGFBQUgsQ0FBRCxDQUFtQjJZLE1BQW5CLEVBQVo7QUFDQTlpQixjQUFJLENBQUNpVixhQUFMLENBQW1COEMsS0FBbkI7O0FBQ0EsY0FBSS9YLElBQUksQ0FBQ2dXLGVBQUwsRUFBSixFQUE0QjtBQUMzQmhXLGdCQUFJLENBQUMwVyxRQUFMLENBQWMxVyxJQUFJLENBQUNuQyxLQUFMLENBQVdTLE1BQXpCO0FBQ0E7QUFDRCxTQVREO0FBV0EsT0F2QkQ7QUF3QkEsS0ExQlksRUFBYjtBQTRCQSxHQXJERDtBQXVEQTJPLFdBQVMsQ0FBQ3RQLE1BQVYsQ0FBaUIsc0JBQWpCLEVBQXlDLFVBQVNvQyxPQUFULEVBQWtCO0FBQzFELFFBQUlDLElBQUksR0FBRyxJQUFYOztBQUVBRCxXQUFPLENBQUMrSyxJQUFSLEdBQWUvSyxPQUFPLENBQUMrSyxJQUFSLElBQWdCLFVBQVMrTyxNQUFULEVBQWlCO0FBQy9DLGFBQU9BLE1BQU0sQ0FBQyxLQUFLL2IsUUFBTCxDQUFjc1csVUFBZixDQUFiO0FBQ0EsS0FGRDs7QUFJQSxTQUFLNUIsU0FBTCxHQUFrQixZQUFXO0FBQzVCLFVBQUl6SixRQUFRLEdBQUcvSSxJQUFJLENBQUN3UyxTQUFwQjtBQUNBLGFBQU8sVUFBU3hJLENBQVQsRUFBWTtBQUNsQixZQUFJb1MsS0FBSixFQUFXdkMsTUFBWDs7QUFDQSxZQUFJN1AsQ0FBQyxDQUFDb0MsT0FBRixLQUFjdkUsYUFBZCxJQUErQixLQUFLNEksY0FBTCxDQUFvQjlELEdBQXBCLE9BQThCLEVBQTdELElBQW1FLENBQUMsS0FBSzJDLFlBQUwsQ0FBa0JoUixNQUExRixFQUFrRztBQUNqRzhkLGVBQUssR0FBRyxLQUFLbE4sUUFBTCxHQUFnQixDQUF4Qjs7QUFDQSxjQUFJa04sS0FBSyxJQUFJLENBQVQsSUFBY0EsS0FBSyxHQUFHLEtBQUt2ZSxLQUFMLENBQVdTLE1BQXJDLEVBQTZDO0FBQzVDdWIsa0JBQU0sR0FBRyxLQUFLOVosT0FBTCxDQUFhLEtBQUtsQyxLQUFMLENBQVd1ZSxLQUFYLENBQWIsQ0FBVDs7QUFDQSxnQkFBSSxLQUFLcEcsZUFBTCxDQUFxQmhNLENBQXJCLENBQUosRUFBNkI7QUFDNUIsbUJBQUt5TSxlQUFMLENBQXFCMVcsT0FBTyxDQUFDK0ssSUFBUixDQUFhbEwsS0FBYixDQUFtQixJQUFuQixFQUF5QixDQUFDaWEsTUFBRCxDQUF6QixDQUFyQjtBQUNBLG1CQUFLNUQsY0FBTCxDQUFvQixJQUFwQjtBQUNBOztBQUNEak0sYUFBQyxDQUFDMkosY0FBRjtBQUNBO0FBQ0E7QUFDRDs7QUFDRCxlQUFPNUssUUFBUSxDQUFDbkosS0FBVCxDQUFlLElBQWYsRUFBcUJvRCxTQUFyQixDQUFQO0FBQ0EsT0FmRDtBQWdCQSxLQWxCZ0IsRUFBakI7QUFtQkEsR0ExQkQ7QUE2QkEsU0FBT2lLLFNBQVA7QUFDQSxDQTk5RkEsQ0FBRCxDIiwiZmlsZSI6IkFkbWluL0VtYWlsQ3RybH5Db21tb24vUHJvdG90eXBlQ3RybH5Db250ZW50L0lkZW50aWZpZXJDdHJsfkNvbnRlbnQvUHdEU2lja0FjdFJlcG9ydEN0cmx+Q29udGVudC9Qd0R+ZDk3ZDVmOTUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqICBDb3B5cmlnaHQgbm90aWNlXHJcbiAqXHJcbiAqICAoYykgMjAxOCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnLi4vLi4vbGlicmFyeS9zZWxlY3RpemUvZGlzdC9qcy9zdGFuZGFsb25lL3NlbGVjdGl6ZSc7XHJcbmltcG9ydCAnLi4vLi4vbGlicmFyeS9zZWxlY3RpemUvZGlzdC9jc3Mvc2VsZWN0aXplLmJvb3RzdHJhcDQuY3NzJzsiLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCIvKipcclxuICogc2lmdGVyLmpzXHJcbiAqIENvcHlyaWdodCAoYykgMjAxMyBCcmlhbiBSZWF2aXMgJiBjb250cmlidXRvcnNcclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXNcclxuICogZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXQ6XHJcbiAqIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyXHJcbiAqIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0ZcclxuICogQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlXHJcbiAqIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqXHJcbiAqIEBhdXRob3IgQnJpYW4gUmVhdmlzIDxicmlhbkB0aGlyZHJvdXRlLmNvbT5cclxuICovXHJcblxyXG4oZnVuY3Rpb24ocm9vdCwgZmFjdG9yeSkge1xyXG5cdGlmICh0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpIHtcclxuXHRcdGRlZmluZSgnc2lmdGVyJywgZmFjdG9yeSk7XHJcblx0fSBlbHNlIGlmICh0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpIHtcclxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRyb290LlNpZnRlciA9IGZhY3RvcnkoKTtcclxuXHR9XHJcbn0odGhpcywgZnVuY3Rpb24oKSB7XHJcblxyXG5cdC8qKlxyXG5cdCAqIFRleHR1YWxseSBzZWFyY2hlcyBhcnJheXMgYW5kIGhhc2hlcyBvZiBvYmplY3RzXHJcblx0ICogYnkgcHJvcGVydHkgKG9yIG11bHRpcGxlIHByb3BlcnRpZXMpLiBEZXNpZ25lZFxyXG5cdCAqIHNwZWNpZmljYWxseSBmb3IgYXV0b2NvbXBsZXRlLlxyXG5cdCAqXHJcblx0ICogQGNvbnN0cnVjdG9yXHJcblx0ICogQHBhcmFtIHthcnJheXxvYmplY3R9IGl0ZW1zXHJcblx0ICogQHBhcmFtIHtvYmplY3R9IGl0ZW1zXHJcblx0ICovXHJcblx0dmFyIFNpZnRlciA9IGZ1bmN0aW9uKGl0ZW1zLCBzZXR0aW5ncykge1xyXG5cdFx0dGhpcy5pdGVtcyA9IGl0ZW1zO1xyXG5cdFx0dGhpcy5zZXR0aW5ncyA9IHNldHRpbmdzIHx8IHtkaWFjcml0aWNzOiB0cnVlfTtcclxuXHR9O1xyXG5cclxuXHQvKipcclxuXHQgKiBTcGxpdHMgYSBzZWFyY2ggc3RyaW5nIGludG8gYW4gYXJyYXkgb2YgaW5kaXZpZHVhbFxyXG5cdCAqIHJlZ2V4cHMgdG8gYmUgdXNlZCB0byBtYXRjaCByZXN1bHRzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHF1ZXJ5XHJcblx0ICogQHJldHVybnMge2FycmF5fVxyXG5cdCAqL1xyXG5cdFNpZnRlci5wcm90b3R5cGUudG9rZW5pemUgPSBmdW5jdGlvbihxdWVyeSkge1xyXG5cdFx0cXVlcnkgPSB0cmltKFN0cmluZyhxdWVyeSB8fCAnJykudG9Mb3dlckNhc2UoKSk7XHJcblx0XHRpZiAoIXF1ZXJ5IHx8ICFxdWVyeS5sZW5ndGgpIHJldHVybiBbXTtcclxuXHJcblx0XHR2YXIgaSwgbiwgcmVnZXgsIGxldHRlcjtcclxuXHRcdHZhciB0b2tlbnMgPSBbXTtcclxuXHRcdHZhciB3b3JkcyA9IHF1ZXJ5LnNwbGl0KC8gKy8pO1xyXG5cclxuXHRcdGZvciAoaSA9IDAsIG4gPSB3b3Jkcy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuXHRcdFx0cmVnZXggPSBlc2NhcGVfcmVnZXgod29yZHNbaV0pO1xyXG5cdFx0XHRpZiAodGhpcy5zZXR0aW5ncy5kaWFjcml0aWNzKSB7XHJcblx0XHRcdFx0Zm9yIChsZXR0ZXIgaW4gRElBQ1JJVElDUykge1xyXG5cdFx0XHRcdFx0aWYgKERJQUNSSVRJQ1MuaGFzT3duUHJvcGVydHkobGV0dGVyKSkge1xyXG5cdFx0XHRcdFx0XHRyZWdleCA9IHJlZ2V4LnJlcGxhY2UobmV3IFJlZ0V4cChsZXR0ZXIsICdnJyksIERJQUNSSVRJQ1NbbGV0dGVyXSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHRva2Vucy5wdXNoKHtcclxuXHRcdFx0XHRzdHJpbmcgOiB3b3Jkc1tpXSxcclxuXHRcdFx0XHRyZWdleCAgOiBuZXcgUmVnRXhwKHJlZ2V4LCAnaScpXHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiB0b2tlbnM7XHJcblx0fTtcclxuXHJcblx0LyoqXHJcblx0ICogSXRlcmF0ZXMgb3ZlciBhcnJheXMgYW5kIGhhc2hlcy5cclxuXHQgKlxyXG5cdCAqIGBgYFxyXG5cdCAqIHRoaXMuaXRlcmF0b3IodGhpcy5pdGVtcywgZnVuY3Rpb24oaXRlbSwgaWQpIHtcclxuXHQgKiAgICAvLyBpbnZva2VkIGZvciBlYWNoIGl0ZW1cclxuXHQgKiB9KTtcclxuXHQgKiBgYGBcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7YXJyYXl8b2JqZWN0fSBvYmplY3RcclxuXHQgKi9cclxuXHRTaWZ0ZXIucHJvdG90eXBlLml0ZXJhdG9yID0gZnVuY3Rpb24ob2JqZWN0LCBjYWxsYmFjaykge1xyXG5cdFx0dmFyIGl0ZXJhdG9yO1xyXG5cdFx0aWYgKGlzX2FycmF5KG9iamVjdCkpIHtcclxuXHRcdFx0aXRlcmF0b3IgPSBBcnJheS5wcm90b3R5cGUuZm9yRWFjaCB8fCBmdW5jdGlvbihjYWxsYmFjaykge1xyXG5cdFx0XHRcdGZvciAodmFyIGkgPSAwLCBuID0gdGhpcy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuXHRcdFx0XHRcdGNhbGxiYWNrKHRoaXNbaV0sIGksIHRoaXMpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGl0ZXJhdG9yID0gZnVuY3Rpb24oY2FsbGJhY2spIHtcclxuXHRcdFx0XHRmb3IgKHZhciBrZXkgaW4gdGhpcykge1xyXG5cdFx0XHRcdFx0aWYgKHRoaXMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG5cdFx0XHRcdFx0XHRjYWxsYmFjayh0aGlzW2tleV0sIGtleSwgdGhpcyk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cdFx0fVxyXG5cclxuXHRcdGl0ZXJhdG9yLmFwcGx5KG9iamVjdCwgW2NhbGxiYWNrXSk7XHJcblx0fTtcclxuXHJcblx0LyoqXHJcblx0ICogUmV0dXJucyBhIGZ1bmN0aW9uIHRvIGJlIHVzZWQgdG8gc2NvcmUgaW5kaXZpZHVhbCByZXN1bHRzLlxyXG5cdCAqXHJcblx0ICogR29vZCBtYXRjaGVzIHdpbGwgaGF2ZSBhIGhpZ2hlciBzY29yZSB0aGFuIHBvb3IgbWF0Y2hlcy5cclxuXHQgKiBJZiBhbiBpdGVtIGlzIG5vdCBhIG1hdGNoLCAwIHdpbGwgYmUgcmV0dXJuZWQgYnkgdGhlIGZ1bmN0aW9uLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtvYmplY3R8c3RyaW5nfSBzZWFyY2hcclxuXHQgKiBAcGFyYW0ge29iamVjdH0gb3B0aW9ucyAob3B0aW9uYWwpXHJcblx0ICogQHJldHVybnMge2Z1bmN0aW9ufVxyXG5cdCAqL1xyXG5cdFNpZnRlci5wcm90b3R5cGUuZ2V0U2NvcmVGdW5jdGlvbiA9IGZ1bmN0aW9uKHNlYXJjaCwgb3B0aW9ucykge1xyXG5cdFx0dmFyIHNlbGYsIGZpZWxkcywgdG9rZW5zLCB0b2tlbl9jb3VudDtcclxuXHJcblx0XHRzZWxmICAgICAgICA9IHRoaXM7XHJcblx0XHRzZWFyY2ggICAgICA9IHNlbGYucHJlcGFyZVNlYXJjaChzZWFyY2gsIG9wdGlvbnMpO1xyXG5cdFx0dG9rZW5zICAgICAgPSBzZWFyY2gudG9rZW5zO1xyXG5cdFx0ZmllbGRzICAgICAgPSBzZWFyY2gub3B0aW9ucy5maWVsZHM7XHJcblx0XHR0b2tlbl9jb3VudCA9IHRva2Vucy5sZW5ndGg7XHJcblxyXG5cdFx0LyoqXHJcblx0XHQgKiBDYWxjdWxhdGVzIGhvdyBjbG9zZSBvZiBhIG1hdGNoIHRoZVxyXG5cdFx0ICogZ2l2ZW4gdmFsdWUgaXMgYWdhaW5zdCBhIHNlYXJjaCB0b2tlbi5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge21peGVkfSB2YWx1ZVxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IHRva2VuXHJcblx0XHQgKiBAcmV0dXJuIHtudW1iZXJ9XHJcblx0XHQgKi9cclxuXHRcdHZhciBzY29yZVZhbHVlID0gZnVuY3Rpb24odmFsdWUsIHRva2VuKSB7XHJcblx0XHRcdHZhciBzY29yZSwgcG9zO1xyXG5cclxuXHRcdFx0aWYgKCF2YWx1ZSkgcmV0dXJuIDA7XHJcblx0XHRcdHZhbHVlID0gU3RyaW5nKHZhbHVlIHx8ICcnKTtcclxuXHRcdFx0cG9zID0gdmFsdWUuc2VhcmNoKHRva2VuLnJlZ2V4KTtcclxuXHRcdFx0aWYgKHBvcyA9PT0gLTEpIHJldHVybiAwO1xyXG5cdFx0XHRzY29yZSA9IHRva2VuLnN0cmluZy5sZW5ndGggLyB2YWx1ZS5sZW5ndGg7XHJcblx0XHRcdGlmIChwb3MgPT09IDApIHNjb3JlICs9IDAuNTtcclxuXHRcdFx0cmV0dXJuIHNjb3JlO1xyXG5cdFx0fTtcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIENhbGN1bGF0ZXMgdGhlIHNjb3JlIG9mIGFuIG9iamVjdFxyXG5cdFx0ICogYWdhaW5zdCB0aGUgc2VhcmNoIHF1ZXJ5LlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSB0b2tlblxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGRhdGFcclxuXHRcdCAqIEByZXR1cm4ge251bWJlcn1cclxuXHRcdCAqL1xyXG5cdFx0dmFyIHNjb3JlT2JqZWN0ID0gKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgZmllbGRfY291bnQgPSBmaWVsZHMubGVuZ3RoO1xyXG5cdFx0XHRpZiAoIWZpZWxkX2NvdW50KSB7XHJcblx0XHRcdFx0cmV0dXJuIGZ1bmN0aW9uKCkgeyByZXR1cm4gMDsgfTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAoZmllbGRfY291bnQgPT09IDEpIHtcclxuXHRcdFx0XHRyZXR1cm4gZnVuY3Rpb24odG9rZW4sIGRhdGEpIHtcclxuXHRcdFx0XHRcdHJldHVybiBzY29yZVZhbHVlKGRhdGFbZmllbGRzWzBdXSwgdG9rZW4pO1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIGZ1bmN0aW9uKHRva2VuLCBkYXRhKSB7XHJcblx0XHRcdFx0Zm9yICh2YXIgaSA9IDAsIHN1bSA9IDA7IGkgPCBmaWVsZF9jb3VudDsgaSsrKSB7XHJcblx0XHRcdFx0XHRzdW0gKz0gc2NvcmVWYWx1ZShkYXRhW2ZpZWxkc1tpXV0sIHRva2VuKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuIHN1bSAvIGZpZWxkX2NvdW50O1xyXG5cdFx0XHR9O1xyXG5cdFx0fSkoKTtcclxuXHJcblx0XHRpZiAoIXRva2VuX2NvdW50KSB7XHJcblx0XHRcdHJldHVybiBmdW5jdGlvbigpIHsgcmV0dXJuIDA7IH07XHJcblx0XHR9XHJcblx0XHRpZiAodG9rZW5fY291bnQgPT09IDEpIHtcclxuXHRcdFx0cmV0dXJuIGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFx0XHRyZXR1cm4gc2NvcmVPYmplY3QodG9rZW5zWzBdLCBkYXRhKTtcclxuXHRcdFx0fTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAoc2VhcmNoLm9wdGlvbnMuY29uanVuY3Rpb24gPT09ICdhbmQnKSB7XHJcblx0XHRcdHJldHVybiBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcdFx0dmFyIHNjb3JlO1xyXG5cdFx0XHRcdGZvciAodmFyIGkgPSAwLCBzdW0gPSAwOyBpIDwgdG9rZW5fY291bnQ7IGkrKykge1xyXG5cdFx0XHRcdFx0c2NvcmUgPSBzY29yZU9iamVjdCh0b2tlbnNbaV0sIGRhdGEpO1xyXG5cdFx0XHRcdFx0aWYgKHNjb3JlIDw9IDApIHJldHVybiAwO1xyXG5cdFx0XHRcdFx0c3VtICs9IHNjb3JlO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRyZXR1cm4gc3VtIC8gdG9rZW5fY291bnQ7XHJcblx0XHRcdH07XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRyZXR1cm4gZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0XHRcdGZvciAodmFyIGkgPSAwLCBzdW0gPSAwOyBpIDwgdG9rZW5fY291bnQ7IGkrKykge1xyXG5cdFx0XHRcdFx0c3VtICs9IHNjb3JlT2JqZWN0KHRva2Vuc1tpXSwgZGF0YSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHJldHVybiBzdW0gLyB0b2tlbl9jb3VudDtcclxuXHRcdFx0fTtcclxuXHRcdH1cclxuXHR9O1xyXG5cclxuXHQvKipcclxuXHQgKiBSZXR1cm5zIGEgZnVuY3Rpb24gdGhhdCBjYW4gYmUgdXNlZCB0byBjb21wYXJlIHR3b1xyXG5cdCAqIHJlc3VsdHMsIGZvciBzb3J0aW5nIHB1cnBvc2VzLiBJZiBubyBzb3J0aW5nIHNob3VsZFxyXG5cdCAqIGJlIHBlcmZvcm1lZCwgYG51bGxgIHdpbGwgYmUgcmV0dXJuZWQuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge3N0cmluZ3xvYmplY3R9IHNlYXJjaFxyXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zXHJcblx0ICogQHJldHVybiBmdW5jdGlvbihhLGIpXHJcblx0ICovXHJcblx0U2lmdGVyLnByb3RvdHlwZS5nZXRTb3J0RnVuY3Rpb24gPSBmdW5jdGlvbihzZWFyY2gsIG9wdGlvbnMpIHtcclxuXHRcdHZhciBpLCBuLCBzZWxmLCBmaWVsZCwgZmllbGRzLCBmaWVsZHNfY291bnQsIG11bHRpcGxpZXIsIG11bHRpcGxpZXJzLCBnZXRfZmllbGQsIGltcGxpY2l0X3Njb3JlLCBzb3J0O1xyXG5cclxuXHRcdHNlbGYgICA9IHRoaXM7XHJcblx0XHRzZWFyY2ggPSBzZWxmLnByZXBhcmVTZWFyY2goc2VhcmNoLCBvcHRpb25zKTtcclxuXHRcdHNvcnQgICA9ICghc2VhcmNoLnF1ZXJ5ICYmIG9wdGlvbnMuc29ydF9lbXB0eSkgfHwgb3B0aW9ucy5zb3J0O1xyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogRmV0Y2hlcyB0aGUgc3BlY2lmaWVkIHNvcnQgZmllbGQgdmFsdWVcclxuXHRcdCAqIGZyb20gYSBzZWFyY2ggcmVzdWx0IGl0ZW0uXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtICB7c3RyaW5nfSBuYW1lXHJcblx0XHQgKiBAcGFyYW0gIHtvYmplY3R9IHJlc3VsdFxyXG5cdFx0ICogQHJldHVybiB7bWl4ZWR9XHJcblx0XHQgKi9cclxuXHRcdGdldF9maWVsZCA9IGZ1bmN0aW9uKG5hbWUsIHJlc3VsdCkge1xyXG5cdFx0XHRpZiAobmFtZSA9PT0gJyRzY29yZScpIHJldHVybiByZXN1bHQuc2NvcmU7XHJcblx0XHRcdHJldHVybiBzZWxmLml0ZW1zW3Jlc3VsdC5pZF1bbmFtZV07XHJcblx0XHR9O1xyXG5cclxuXHRcdC8vIHBhcnNlIG9wdGlvbnNcclxuXHRcdGZpZWxkcyA9IFtdO1xyXG5cdFx0aWYgKHNvcnQpIHtcclxuXHRcdFx0Zm9yIChpID0gMCwgbiA9IHNvcnQubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdFx0aWYgKHNlYXJjaC5xdWVyeSB8fCBzb3J0W2ldLmZpZWxkICE9PSAnJHNjb3JlJykge1xyXG5cdFx0XHRcdFx0ZmllbGRzLnB1c2goc29ydFtpXSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0Ly8gdGhlIFwiJHNjb3JlXCIgZmllbGQgaXMgaW1wbGllZCB0byBiZSB0aGUgcHJpbWFyeVxyXG5cdFx0Ly8gc29ydCBmaWVsZCwgdW5sZXNzIGl0J3MgbWFudWFsbHkgc3BlY2lmaWVkXHJcblx0XHRpZiAoc2VhcmNoLnF1ZXJ5KSB7XHJcblx0XHRcdGltcGxpY2l0X3Njb3JlID0gdHJ1ZTtcclxuXHRcdFx0Zm9yIChpID0gMCwgbiA9IGZpZWxkcy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuXHRcdFx0XHRpZiAoZmllbGRzW2ldLmZpZWxkID09PSAnJHNjb3JlJykge1xyXG5cdFx0XHRcdFx0aW1wbGljaXRfc2NvcmUgPSBmYWxzZTtcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAoaW1wbGljaXRfc2NvcmUpIHtcclxuXHRcdFx0XHRmaWVsZHMudW5zaGlmdCh7ZmllbGQ6ICckc2NvcmUnLCBkaXJlY3Rpb246ICdkZXNjJ30pO1xyXG5cdFx0XHR9XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRmb3IgKGkgPSAwLCBuID0gZmllbGRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG5cdFx0XHRcdGlmIChmaWVsZHNbaV0uZmllbGQgPT09ICckc2NvcmUnKSB7XHJcblx0XHRcdFx0XHRmaWVsZHMuc3BsaWNlKGksIDEpO1xyXG5cdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0bXVsdGlwbGllcnMgPSBbXTtcclxuXHRcdGZvciAoaSA9IDAsIG4gPSBmaWVsZHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdG11bHRpcGxpZXJzLnB1c2goZmllbGRzW2ldLmRpcmVjdGlvbiA9PT0gJ2Rlc2MnID8gLTEgOiAxKTtcclxuXHRcdH1cclxuXHJcblx0XHQvLyBidWlsZCBmdW5jdGlvblxyXG5cdFx0ZmllbGRzX2NvdW50ID0gZmllbGRzLmxlbmd0aDtcclxuXHRcdGlmICghZmllbGRzX2NvdW50KSB7XHJcblx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0fSBlbHNlIGlmIChmaWVsZHNfY291bnQgPT09IDEpIHtcclxuXHRcdFx0ZmllbGQgPSBmaWVsZHNbMF0uZmllbGQ7XHJcblx0XHRcdG11bHRpcGxpZXIgPSBtdWx0aXBsaWVyc1swXTtcclxuXHRcdFx0cmV0dXJuIGZ1bmN0aW9uKGEsIGIpIHtcclxuXHRcdFx0XHRyZXR1cm4gbXVsdGlwbGllciAqIGNtcChcclxuXHRcdFx0XHRcdGdldF9maWVsZChmaWVsZCwgYSksXHJcblx0XHRcdFx0XHRnZXRfZmllbGQoZmllbGQsIGIpXHJcblx0XHRcdFx0KTtcclxuXHRcdFx0fTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHJldHVybiBmdW5jdGlvbihhLCBiKSB7XHJcblx0XHRcdFx0dmFyIGksIHJlc3VsdCwgYV92YWx1ZSwgYl92YWx1ZSwgZmllbGQ7XHJcblx0XHRcdFx0Zm9yIChpID0gMDsgaSA8IGZpZWxkc19jb3VudDsgaSsrKSB7XHJcblx0XHRcdFx0XHRmaWVsZCA9IGZpZWxkc1tpXS5maWVsZDtcclxuXHRcdFx0XHRcdHJlc3VsdCA9IG11bHRpcGxpZXJzW2ldICogY21wKFxyXG5cdFx0XHRcdFx0XHRnZXRfZmllbGQoZmllbGQsIGEpLFxyXG5cdFx0XHRcdFx0XHRnZXRfZmllbGQoZmllbGQsIGIpXHJcblx0XHRcdFx0XHQpO1xyXG5cdFx0XHRcdFx0aWYgKHJlc3VsdCkgcmV0dXJuIHJlc3VsdDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuIDA7XHJcblx0XHRcdH07XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0LyoqXHJcblx0ICogUGFyc2VzIGEgc2VhcmNoIHF1ZXJ5IGFuZCByZXR1cm5zIGFuIG9iamVjdFxyXG5cdCAqIHdpdGggdG9rZW5zIGFuZCBmaWVsZHMgcmVhZHkgdG8gYmUgcG9wdWxhdGVkXHJcblx0ICogd2l0aCByZXN1bHRzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHF1ZXJ5XHJcblx0ICogQHBhcmFtIHtvYmplY3R9IG9wdGlvbnNcclxuXHQgKiBAcmV0dXJucyB7b2JqZWN0fVxyXG5cdCAqL1xyXG5cdFNpZnRlci5wcm90b3R5cGUucHJlcGFyZVNlYXJjaCA9IGZ1bmN0aW9uKHF1ZXJ5LCBvcHRpb25zKSB7XHJcblx0XHRpZiAodHlwZW9mIHF1ZXJ5ID09PSAnb2JqZWN0JykgcmV0dXJuIHF1ZXJ5O1xyXG5cclxuXHRcdG9wdGlvbnMgPSBleHRlbmQoe30sIG9wdGlvbnMpO1xyXG5cclxuXHRcdHZhciBvcHRpb25fZmllbGRzICAgICA9IG9wdGlvbnMuZmllbGRzO1xyXG5cdFx0dmFyIG9wdGlvbl9zb3J0ICAgICAgID0gb3B0aW9ucy5zb3J0O1xyXG5cdFx0dmFyIG9wdGlvbl9zb3J0X2VtcHR5ID0gb3B0aW9ucy5zb3J0X2VtcHR5O1xyXG5cclxuXHRcdGlmIChvcHRpb25fZmllbGRzICYmICFpc19hcnJheShvcHRpb25fZmllbGRzKSkgb3B0aW9ucy5maWVsZHMgPSBbb3B0aW9uX2ZpZWxkc107XHJcblx0XHRpZiAob3B0aW9uX3NvcnQgJiYgIWlzX2FycmF5KG9wdGlvbl9zb3J0KSkgb3B0aW9ucy5zb3J0ID0gW29wdGlvbl9zb3J0XTtcclxuXHRcdGlmIChvcHRpb25fc29ydF9lbXB0eSAmJiAhaXNfYXJyYXkob3B0aW9uX3NvcnRfZW1wdHkpKSBvcHRpb25zLnNvcnRfZW1wdHkgPSBbb3B0aW9uX3NvcnRfZW1wdHldO1xyXG5cclxuXHRcdHJldHVybiB7XHJcblx0XHRcdG9wdGlvbnMgOiBvcHRpb25zLFxyXG5cdFx0XHRxdWVyeSAgIDogU3RyaW5nKHF1ZXJ5IHx8ICcnKS50b0xvd2VyQ2FzZSgpLFxyXG5cdFx0XHR0b2tlbnMgIDogdGhpcy50b2tlbml6ZShxdWVyeSksXHJcblx0XHRcdHRvdGFsICAgOiAwLFxyXG5cdFx0XHRpdGVtcyAgIDogW11cclxuXHRcdH07XHJcblx0fTtcclxuXHJcblx0LyoqXHJcblx0ICogU2VhcmNoZXMgdGhyb3VnaCBhbGwgaXRlbXMgYW5kIHJldHVybnMgYSBzb3J0ZWQgYXJyYXkgb2YgbWF0Y2hlcy5cclxuXHQgKlxyXG5cdCAqIFRoZSBgb3B0aW9uc2AgcGFyYW1ldGVyIGNhbiBjb250YWluOlxyXG5cdCAqXHJcblx0ICogICAtIGZpZWxkcyB7c3RyaW5nfGFycmF5fVxyXG5cdCAqICAgLSBzb3J0IHthcnJheX1cclxuXHQgKiAgIC0gc2NvcmUge2Z1bmN0aW9ufVxyXG5cdCAqICAgLSBmaWx0ZXIge2Jvb2x9XHJcblx0ICogICAtIGxpbWl0IHtpbnRlZ2VyfVxyXG5cdCAqXHJcblx0ICogUmV0dXJucyBhbiBvYmplY3QgY29udGFpbmluZzpcclxuXHQgKlxyXG5cdCAqICAgLSBvcHRpb25zIHtvYmplY3R9XHJcblx0ICogICAtIHF1ZXJ5IHtzdHJpbmd9XHJcblx0ICogICAtIHRva2VucyB7YXJyYXl9XHJcblx0ICogICAtIHRvdGFsIHtpbnR9XHJcblx0ICogICAtIGl0ZW1zIHthcnJheX1cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBxdWVyeVxyXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zXHJcblx0ICogQHJldHVybnMge29iamVjdH1cclxuXHQgKi9cclxuXHRTaWZ0ZXIucHJvdG90eXBlLnNlYXJjaCA9IGZ1bmN0aW9uKHF1ZXJ5LCBvcHRpb25zKSB7XHJcblx0XHR2YXIgc2VsZiA9IHRoaXMsIHZhbHVlLCBzY29yZSwgc2VhcmNoLCBjYWxjdWxhdGVTY29yZTtcclxuXHRcdHZhciBmbl9zb3J0O1xyXG5cdFx0dmFyIGZuX3Njb3JlO1xyXG5cclxuXHRcdHNlYXJjaCAgPSB0aGlzLnByZXBhcmVTZWFyY2gocXVlcnksIG9wdGlvbnMpO1xyXG5cdFx0b3B0aW9ucyA9IHNlYXJjaC5vcHRpb25zO1xyXG5cdFx0cXVlcnkgICA9IHNlYXJjaC5xdWVyeTtcclxuXHJcblx0XHQvLyBnZW5lcmF0ZSByZXN1bHQgc2NvcmluZyBmdW5jdGlvblxyXG5cdFx0Zm5fc2NvcmUgPSBvcHRpb25zLnNjb3JlIHx8IHNlbGYuZ2V0U2NvcmVGdW5jdGlvbihzZWFyY2gpO1xyXG5cclxuXHRcdC8vIHBlcmZvcm0gc2VhcmNoIGFuZCBzb3J0XHJcblx0XHRpZiAocXVlcnkubGVuZ3RoKSB7XHJcblx0XHRcdHNlbGYuaXRlcmF0b3Ioc2VsZi5pdGVtcywgZnVuY3Rpb24oaXRlbSwgaWQpIHtcclxuXHRcdFx0XHRzY29yZSA9IGZuX3Njb3JlKGl0ZW0pO1xyXG5cdFx0XHRcdGlmIChvcHRpb25zLmZpbHRlciA9PT0gZmFsc2UgfHwgc2NvcmUgPiAwKSB7XHJcblx0XHRcdFx0XHRzZWFyY2guaXRlbXMucHVzaCh7J3Njb3JlJzogc2NvcmUsICdpZCc6IGlkfSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHNlbGYuaXRlcmF0b3Ioc2VsZi5pdGVtcywgZnVuY3Rpb24oaXRlbSwgaWQpIHtcclxuXHRcdFx0XHRzZWFyY2guaXRlbXMucHVzaCh7J3Njb3JlJzogMSwgJ2lkJzogaWR9KTtcclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblxyXG5cdFx0Zm5fc29ydCA9IHNlbGYuZ2V0U29ydEZ1bmN0aW9uKHNlYXJjaCwgb3B0aW9ucyk7XHJcblx0XHRpZiAoZm5fc29ydCkgc2VhcmNoLml0ZW1zLnNvcnQoZm5fc29ydCk7XHJcblxyXG5cdFx0Ly8gYXBwbHkgbGltaXRzXHJcblx0XHRzZWFyY2gudG90YWwgPSBzZWFyY2guaXRlbXMubGVuZ3RoO1xyXG5cdFx0aWYgKHR5cGVvZiBvcHRpb25zLmxpbWl0ID09PSAnbnVtYmVyJykge1xyXG5cdFx0XHRzZWFyY2guaXRlbXMgPSBzZWFyY2guaXRlbXMuc2xpY2UoMCwgb3B0aW9ucy5saW1pdCk7XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHNlYXJjaDtcclxuXHR9O1xyXG5cclxuXHQvLyB1dGlsaXRpZXNcclxuXHQvLyAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtXHJcblxyXG5cdHZhciBjbXAgPSBmdW5jdGlvbihhLCBiKSB7XHJcblx0XHRpZiAodHlwZW9mIGEgPT09ICdudW1iZXInICYmIHR5cGVvZiBiID09PSAnbnVtYmVyJykge1xyXG5cdFx0XHRyZXR1cm4gYSA+IGIgPyAxIDogKGEgPCBiID8gLTEgOiAwKTtcclxuXHRcdH1cclxuXHRcdGEgPSBhc2NpaWZvbGQoU3RyaW5nKGEgfHwgJycpKTtcclxuXHRcdGIgPSBhc2NpaWZvbGQoU3RyaW5nKGIgfHwgJycpKTtcclxuXHRcdGlmIChhID4gYikgcmV0dXJuIDE7XHJcblx0XHRpZiAoYiA+IGEpIHJldHVybiAtMTtcclxuXHRcdHJldHVybiAwO1xyXG5cdH07XHJcblxyXG5cdHZhciBleHRlbmQgPSBmdW5jdGlvbihhLCBiKSB7XHJcblx0XHR2YXIgaSwgbiwgaywgb2JqZWN0O1xyXG5cdFx0Zm9yIChpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuXHRcdFx0b2JqZWN0ID0gYXJndW1lbnRzW2ldO1xyXG5cdFx0XHRpZiAoIW9iamVjdCkgY29udGludWU7XHJcblx0XHRcdGZvciAoayBpbiBvYmplY3QpIHtcclxuXHRcdFx0XHRpZiAob2JqZWN0Lmhhc093blByb3BlcnR5KGspKSB7XHJcblx0XHRcdFx0XHRhW2tdID0gb2JqZWN0W2tdO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIGE7XHJcblx0fTtcclxuXHJcblx0dmFyIHRyaW0gPSBmdW5jdGlvbihzdHIpIHtcclxuXHRcdHJldHVybiAoc3RyICsgJycpLnJlcGxhY2UoL15cXHMrfFxccyskfC9nLCAnJyk7XHJcblx0fTtcclxuXHJcblx0dmFyIGVzY2FwZV9yZWdleCA9IGZ1bmN0aW9uKHN0cikge1xyXG5cdFx0cmV0dXJuIChzdHIgKyAnJykucmVwbGFjZSgvKFsuPyorXiRbXFxdXFxcXCgpe318LV0pL2csICdcXFxcJDEnKTtcclxuXHR9O1xyXG5cclxuXHR2YXIgaXNfYXJyYXkgPSBBcnJheS5pc0FycmF5IHx8ICgkICYmICQuaXNBcnJheSkgfHwgZnVuY3Rpb24ob2JqZWN0KSB7XHJcblx0XHRyZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG9iamVjdCkgPT09ICdbb2JqZWN0IEFycmF5XSc7XHJcblx0fTtcclxuXHJcblx0dmFyIERJQUNSSVRJQ1MgPSB7XHJcblx0XHQnYSc6ICdbYcOAw4HDgsODw4TDhcOgw6HDosOjw6TDpcSAxIHEhcSEXScsXHJcblx0XHQnYyc6ICdbY8OHw6fEh8SGxI3EjF0nLFxyXG5cdFx0J2QnOiAnW2TEkcSQxI/Ejl0nLFxyXG5cdFx0J2UnOiAnW2XDiMOJw4rDi8Oow6nDqsOrxJvEmsSSxJPEmcSYXScsXHJcblx0XHQnaSc6ICdbacOMw43DjsOPw6zDrcOuw6/EqsSrXScsXHJcblx0XHQnbCc6ICdbbMWCxYFdJyxcclxuXHRcdCduJzogJ1tuw5HDscWIxYfFhMWDXScsXHJcblx0XHQnbyc6ICdbb8OSw5PDlMOVw5XDlsOYw7LDs8O0w7XDtsO4xYzFjV0nLFxyXG5cdFx0J3InOiAnW3LFmcWYXScsXHJcblx0XHQncyc6ICdbc8WgxaHFm8WaXScsXHJcblx0XHQndCc6ICdbdMWlxaRdJyxcclxuXHRcdCd1JzogJ1t1w5nDmsObw5zDucO6w7vDvMWvxa7FqsWrXScsXHJcblx0XHQneSc6ICdbecW4w7/DvcOdXScsXHJcblx0XHQneic6ICdbesW9xb7FvMW7xbrFuV0nXHJcblx0fTtcclxuXHJcblx0dmFyIGFzY2lpZm9sZCA9IChmdW5jdGlvbigpIHtcclxuXHRcdHZhciBpLCBuLCBrLCBjaHVuaztcclxuXHRcdHZhciBmb3JlaWdubGV0dGVycyA9ICcnO1xyXG5cdFx0dmFyIGxvb2t1cCA9IHt9O1xyXG5cdFx0Zm9yIChrIGluIERJQUNSSVRJQ1MpIHtcclxuXHRcdFx0aWYgKERJQUNSSVRJQ1MuaGFzT3duUHJvcGVydHkoaykpIHtcclxuXHRcdFx0XHRjaHVuayA9IERJQUNSSVRJQ1Nba10uc3Vic3RyaW5nKDIsIERJQUNSSVRJQ1Nba10ubGVuZ3RoIC0gMSk7XHJcblx0XHRcdFx0Zm9yZWlnbmxldHRlcnMgKz0gY2h1bms7XHJcblx0XHRcdFx0Zm9yIChpID0gMCwgbiA9IGNodW5rLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG5cdFx0XHRcdFx0bG9va3VwW2NodW5rLmNoYXJBdChpKV0gPSBrO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0dmFyIHJlZ2V4cCA9IG5ldyBSZWdFeHAoJ1snICsgIGZvcmVpZ25sZXR0ZXJzICsgJ10nLCAnZycpO1xyXG5cdFx0cmV0dXJuIGZ1bmN0aW9uKHN0cikge1xyXG5cdFx0XHRyZXR1cm4gc3RyLnJlcGxhY2UocmVnZXhwLCBmdW5jdGlvbihmb3JlaWdubGV0dGVyKSB7XHJcblx0XHRcdFx0cmV0dXJuIGxvb2t1cFtmb3JlaWdubGV0dGVyXTtcclxuXHRcdFx0fSkudG9Mb3dlckNhc2UoKTtcclxuXHRcdH07XHJcblx0fSkoKTtcclxuXHJcblxyXG5cdC8vIGV4cG9ydFxyXG5cdC8vIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC1cclxuXHJcblx0cmV0dXJuIFNpZnRlcjtcclxufSkpO1xyXG5cclxuXHJcblxyXG4vKipcclxuICogbWljcm9wbHVnaW4uanNcclxuICogQ29weXJpZ2h0IChjKSAyMDEzIEJyaWFuIFJlYXZpcyAmIGNvbnRyaWJ1dG9yc1xyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpc1xyXG4gKiBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdDpcclxuICogaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXJcclxuICogdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRlxyXG4gKiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2VcclxuICogZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICpcclxuICogQGF1dGhvciBCcmlhbiBSZWF2aXMgPGJyaWFuQHRoaXJkcm91dGUuY29tPlxyXG4gKi9cclxuXHJcbihmdW5jdGlvbihyb290LCBmYWN0b3J5KSB7XHJcblx0aWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xyXG5cdFx0ZGVmaW5lKCdtaWNyb3BsdWdpbicsIGZhY3RvcnkpO1xyXG5cdH0gZWxzZSBpZiAodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKSB7XHJcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcclxuXHR9IGVsc2Uge1xyXG5cdFx0cm9vdC5NaWNyb1BsdWdpbiA9IGZhY3RvcnkoKTtcclxuXHR9XHJcbn0odGhpcywgZnVuY3Rpb24oKSB7XHJcblx0dmFyIE1pY3JvUGx1Z2luID0ge307XHJcblxyXG5cdE1pY3JvUGx1Z2luLm1peGluID0gZnVuY3Rpb24oSW50ZXJmYWNlKSB7XHJcblx0XHRJbnRlcmZhY2UucGx1Z2lucyA9IHt9O1xyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogSW5pdGlhbGl6ZXMgdGhlIGxpc3RlZCBwbHVnaW5zICh3aXRoIG9wdGlvbnMpLlxyXG5cdFx0ICogQWNjZXB0YWJsZSBmb3JtYXRzOlxyXG5cdFx0ICpcclxuXHRcdCAqIExpc3QgKHdpdGhvdXQgb3B0aW9ucyk6XHJcblx0XHQgKiAgIFsnYScsICdiJywgJ2MnXVxyXG5cdFx0ICpcclxuXHRcdCAqIExpc3QgKHdpdGggb3B0aW9ucyk6XHJcblx0XHQgKiAgIFt7J25hbWUnOiAnYScsIG9wdGlvbnM6IHt9fSwgeyduYW1lJzogJ2InLCBvcHRpb25zOiB7fX1dXHJcblx0XHQgKlxyXG5cdFx0ICogSGFzaCAod2l0aCBvcHRpb25zKTpcclxuXHRcdCAqICAgeydhJzogeyAuLi4gfSwgJ2InOiB7IC4uLiB9LCAnYyc6IHsgLi4uIH19XHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHttaXhlZH0gcGx1Z2luc1xyXG5cdFx0ICovXHJcblx0XHRJbnRlcmZhY2UucHJvdG90eXBlLmluaXRpYWxpemVQbHVnaW5zID0gZnVuY3Rpb24ocGx1Z2lucykge1xyXG5cdFx0XHR2YXIgaSwgbiwga2V5O1xyXG5cdFx0XHR2YXIgc2VsZiAgPSB0aGlzO1xyXG5cdFx0XHR2YXIgcXVldWUgPSBbXTtcclxuXHJcblx0XHRcdHNlbGYucGx1Z2lucyA9IHtcclxuXHRcdFx0XHRuYW1lcyAgICAgOiBbXSxcclxuXHRcdFx0XHRzZXR0aW5ncyAgOiB7fSxcclxuXHRcdFx0XHRyZXF1ZXN0ZWQgOiB7fSxcclxuXHRcdFx0XHRsb2FkZWQgICAgOiB7fVxyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0aWYgKHV0aWxzLmlzQXJyYXkocGx1Z2lucykpIHtcclxuXHRcdFx0XHRmb3IgKGkgPSAwLCBuID0gcGx1Z2lucy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuXHRcdFx0XHRcdGlmICh0eXBlb2YgcGx1Z2luc1tpXSA9PT0gJ3N0cmluZycpIHtcclxuXHRcdFx0XHRcdFx0cXVldWUucHVzaChwbHVnaW5zW2ldKTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdHNlbGYucGx1Z2lucy5zZXR0aW5nc1twbHVnaW5zW2ldLm5hbWVdID0gcGx1Z2luc1tpXS5vcHRpb25zO1xyXG5cdFx0XHRcdFx0XHRxdWV1ZS5wdXNoKHBsdWdpbnNbaV0ubmFtZSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2UgaWYgKHBsdWdpbnMpIHtcclxuXHRcdFx0XHRmb3IgKGtleSBpbiBwbHVnaW5zKSB7XHJcblx0XHRcdFx0XHRpZiAocGx1Z2lucy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XHJcblx0XHRcdFx0XHRcdHNlbGYucGx1Z2lucy5zZXR0aW5nc1trZXldID0gcGx1Z2luc1trZXldO1xyXG5cdFx0XHRcdFx0XHRxdWV1ZS5wdXNoKGtleSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR3aGlsZSAocXVldWUubGVuZ3RoKSB7XHJcblx0XHRcdFx0c2VsZi5yZXF1aXJlKHF1ZXVlLnNoaWZ0KCkpO1xyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cclxuXHRcdEludGVyZmFjZS5wcm90b3R5cGUubG9hZFBsdWdpbiA9IGZ1bmN0aW9uKG5hbWUpIHtcclxuXHRcdFx0dmFyIHNlbGYgICAgPSB0aGlzO1xyXG5cdFx0XHR2YXIgcGx1Z2lucyA9IHNlbGYucGx1Z2lucztcclxuXHRcdFx0dmFyIHBsdWdpbiAgPSBJbnRlcmZhY2UucGx1Z2luc1tuYW1lXTtcclxuXHJcblx0XHRcdGlmICghSW50ZXJmYWNlLnBsdWdpbnMuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcclxuXHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1VuYWJsZSB0byBmaW5kIFwiJyArICBuYW1lICsgJ1wiIHBsdWdpbicpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRwbHVnaW5zLnJlcXVlc3RlZFtuYW1lXSA9IHRydWU7XHJcblx0XHRcdHBsdWdpbnMubG9hZGVkW25hbWVdID0gcGx1Z2luLmZuLmFwcGx5KHNlbGYsIFtzZWxmLnBsdWdpbnMuc2V0dGluZ3NbbmFtZV0gfHwge31dKTtcclxuXHRcdFx0cGx1Z2lucy5uYW1lcy5wdXNoKG5hbWUpO1xyXG5cdFx0fTtcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIEluaXRpYWxpemVzIGEgcGx1Z2luLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXHJcblx0XHQgKi9cclxuXHRcdEludGVyZmFjZS5wcm90b3R5cGUucmVxdWlyZSA9IGZ1bmN0aW9uKG5hbWUpIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHR2YXIgcGx1Z2lucyA9IHNlbGYucGx1Z2lucztcclxuXHJcblx0XHRcdGlmICghc2VsZi5wbHVnaW5zLmxvYWRlZC5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xyXG5cdFx0XHRcdGlmIChwbHVnaW5zLnJlcXVlc3RlZFtuYW1lXSkge1xyXG5cdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCdQbHVnaW4gaGFzIGNpcmN1bGFyIGRlcGVuZGVuY3kgKFwiJyArIG5hbWUgKyAnXCIpJyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHNlbGYubG9hZFBsdWdpbihuYW1lKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHBsdWdpbnMubG9hZGVkW25hbWVdO1xyXG5cdFx0fTtcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIFJlZ2lzdGVycyBhIHBsdWdpbi5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gbmFtZVxyXG5cdFx0ICogQHBhcmFtIHtmdW5jdGlvbn0gZm5cclxuXHRcdCAqL1xyXG5cdFx0SW50ZXJmYWNlLmRlZmluZSA9IGZ1bmN0aW9uKG5hbWUsIGZuKSB7XHJcblx0XHRcdEludGVyZmFjZS5wbHVnaW5zW25hbWVdID0ge1xyXG5cdFx0XHRcdCduYW1lJyA6IG5hbWUsXHJcblx0XHRcdFx0J2ZuJyAgIDogZm5cclxuXHRcdFx0fTtcclxuXHRcdH07XHJcblx0fTtcclxuXHJcblx0dmFyIHV0aWxzID0ge1xyXG5cdFx0aXNBcnJheTogQXJyYXkuaXNBcnJheSB8fCBmdW5jdGlvbih2QXJnKSB7XHJcblx0XHRcdHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodkFyZykgPT09ICdbb2JqZWN0IEFycmF5XSc7XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0cmV0dXJuIE1pY3JvUGx1Z2luO1xyXG59KSk7XHJcblxyXG4vKipcclxuICogc2VsZWN0aXplLmpzICh2MC4xMi4xKVxyXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTPigJMyMDE1IEJyaWFuIFJlYXZpcyAmIGNvbnRyaWJ1dG9yc1xyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpc1xyXG4gKiBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdDpcclxuICogaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXJcclxuICogdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRlxyXG4gKiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2VcclxuICogZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICpcclxuICogQGF1dGhvciBCcmlhbiBSZWF2aXMgPGJyaWFuQHRoaXJkcm91dGUuY29tPlxyXG4gKi9cclxuXHJcbi8qanNoaW50IGN1cmx5OmZhbHNlICovXHJcbi8qanNoaW50IGJyb3dzZXI6dHJ1ZSAqL1xyXG5cclxuKGZ1bmN0aW9uKHJvb3QsIGZhY3RvcnkpIHtcclxuXHRpZiAodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSB7XHJcblx0XHRkZWZpbmUoJ3NlbGVjdGl6ZScsIFsnanF1ZXJ5Jywnc2lmdGVyJywnbWljcm9wbHVnaW4nXSwgZmFjdG9yeSk7XHJcblx0fSBlbHNlIGlmICh0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpIHtcclxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeShyZXF1aXJlKCdqcXVlcnknKSwgcmVxdWlyZSgnc2lmdGVyJyksIHJlcXVpcmUoJ21pY3JvcGx1Z2luJykpO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRyb290LlNlbGVjdGl6ZSA9IGZhY3Rvcnkocm9vdC5qUXVlcnksIHJvb3QuU2lmdGVyLCByb290Lk1pY3JvUGx1Z2luKTtcclxuXHR9XHJcbn0odGhpcywgZnVuY3Rpb24oJCwgU2lmdGVyLCBNaWNyb1BsdWdpbikge1xyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHJcblx0dmFyIGhpZ2hsaWdodCA9IGZ1bmN0aW9uKCRlbGVtZW50LCBwYXR0ZXJuKSB7XHJcblx0XHRpZiAodHlwZW9mIHBhdHRlcm4gPT09ICdzdHJpbmcnICYmICFwYXR0ZXJuLmxlbmd0aCkgcmV0dXJuO1xyXG5cdFx0dmFyIHJlZ2V4ID0gKHR5cGVvZiBwYXR0ZXJuID09PSAnc3RyaW5nJykgPyBuZXcgUmVnRXhwKHBhdHRlcm4sICdpJykgOiBwYXR0ZXJuO1xyXG5cdFxyXG5cdFx0dmFyIGhpZ2hsaWdodCA9IGZ1bmN0aW9uKG5vZGUpIHtcclxuXHRcdFx0dmFyIHNraXAgPSAwO1xyXG5cdFx0XHRpZiAobm9kZS5ub2RlVHlwZSA9PT0gMykge1xyXG5cdFx0XHRcdHZhciBwb3MgPSBub2RlLmRhdGEuc2VhcmNoKHJlZ2V4KTtcclxuXHRcdFx0XHRpZiAocG9zID49IDAgJiYgbm9kZS5kYXRhLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0XHRcdHZhciBtYXRjaCA9IG5vZGUuZGF0YS5tYXRjaChyZWdleCk7XHJcblx0XHRcdFx0XHR2YXIgc3Bhbm5vZGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzcGFuJyk7XHJcblx0XHRcdFx0XHRzcGFubm9kZS5jbGFzc05hbWUgPSAnaGlnaGxpZ2h0JztcclxuXHRcdFx0XHRcdHZhciBtaWRkbGViaXQgPSBub2RlLnNwbGl0VGV4dChwb3MpO1xyXG5cdFx0XHRcdFx0dmFyIGVuZGJpdCA9IG1pZGRsZWJpdC5zcGxpdFRleHQobWF0Y2hbMF0ubGVuZ3RoKTtcclxuXHRcdFx0XHRcdHZhciBtaWRkbGVjbG9uZSA9IG1pZGRsZWJpdC5jbG9uZU5vZGUodHJ1ZSk7XHJcblx0XHRcdFx0XHRzcGFubm9kZS5hcHBlbmRDaGlsZChtaWRkbGVjbG9uZSk7XHJcblx0XHRcdFx0XHRtaWRkbGViaXQucGFyZW50Tm9kZS5yZXBsYWNlQ2hpbGQoc3Bhbm5vZGUsIG1pZGRsZWJpdCk7XHJcblx0XHRcdFx0XHRza2lwID0gMTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSBpZiAobm9kZS5ub2RlVHlwZSA9PT0gMSAmJiBub2RlLmNoaWxkTm9kZXMgJiYgIS8oc2NyaXB0fHN0eWxlKS9pLnRlc3Qobm9kZS50YWdOYW1lKSkge1xyXG5cdFx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgbm9kZS5jaGlsZE5vZGVzLmxlbmd0aDsgKytpKSB7XHJcblx0XHRcdFx0XHRpICs9IGhpZ2hsaWdodChub2RlLmNoaWxkTm9kZXNbaV0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gc2tpcDtcclxuXHRcdH07XHJcblx0XHJcblx0XHRyZXR1cm4gJGVsZW1lbnQuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0aGlnaGxpZ2h0KHRoaXMpO1xyXG5cdFx0fSk7XHJcblx0fTtcclxuXHRcclxuXHR2YXIgTWljcm9FdmVudCA9IGZ1bmN0aW9uKCkge307XHJcblx0TWljcm9FdmVudC5wcm90b3R5cGUgPSB7XHJcblx0XHRvbjogZnVuY3Rpb24oZXZlbnQsIGZjdCl7XHJcblx0XHRcdHRoaXMuX2V2ZW50cyA9IHRoaXMuX2V2ZW50cyB8fCB7fTtcclxuXHRcdFx0dGhpcy5fZXZlbnRzW2V2ZW50XSA9IHRoaXMuX2V2ZW50c1tldmVudF0gfHwgW107XHJcblx0XHRcdHRoaXMuX2V2ZW50c1tldmVudF0ucHVzaChmY3QpO1xyXG5cdFx0fSxcclxuXHRcdG9mZjogZnVuY3Rpb24oZXZlbnQsIGZjdCl7XHJcblx0XHRcdHZhciBuID0gYXJndW1lbnRzLmxlbmd0aDtcclxuXHRcdFx0aWYgKG4gPT09IDApIHJldHVybiBkZWxldGUgdGhpcy5fZXZlbnRzO1xyXG5cdFx0XHRpZiAobiA9PT0gMSkgcmV0dXJuIGRlbGV0ZSB0aGlzLl9ldmVudHNbZXZlbnRdO1xyXG5cdFxyXG5cdFx0XHR0aGlzLl9ldmVudHMgPSB0aGlzLl9ldmVudHMgfHwge307XHJcblx0XHRcdGlmIChldmVudCBpbiB0aGlzLl9ldmVudHMgPT09IGZhbHNlKSByZXR1cm47XHJcblx0XHRcdHRoaXMuX2V2ZW50c1tldmVudF0uc3BsaWNlKHRoaXMuX2V2ZW50c1tldmVudF0uaW5kZXhPZihmY3QpLCAxKTtcclxuXHRcdH0sXHJcblx0XHR0cmlnZ2VyOiBmdW5jdGlvbihldmVudCAvKiAsIGFyZ3MuLi4gKi8pe1xyXG5cdFx0XHR0aGlzLl9ldmVudHMgPSB0aGlzLl9ldmVudHMgfHwge307XHJcblx0XHRcdGlmIChldmVudCBpbiB0aGlzLl9ldmVudHMgPT09IGZhbHNlKSByZXR1cm47XHJcblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fZXZlbnRzW2V2ZW50XS5sZW5ndGg7IGkrKyl7XHJcblx0XHRcdFx0dGhpcy5fZXZlbnRzW2V2ZW50XVtpXS5hcHBseSh0aGlzLCBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogTWl4aW4gd2lsbCBkZWxlZ2F0ZSBhbGwgTWljcm9FdmVudC5qcyBmdW5jdGlvbiBpbiB0aGUgZGVzdGluYXRpb24gb2JqZWN0LlxyXG5cdCAqXHJcblx0ICogLSBNaWNyb0V2ZW50Lm1peGluKEZvb2Jhcikgd2lsbCBtYWtlIEZvb2JhciBhYmxlIHRvIHVzZSBNaWNyb0V2ZW50XHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge29iamVjdH0gdGhlIG9iamVjdCB3aGljaCB3aWxsIHN1cHBvcnQgTWljcm9FdmVudFxyXG5cdCAqL1xyXG5cdE1pY3JvRXZlbnQubWl4aW4gPSBmdW5jdGlvbihkZXN0T2JqZWN0KXtcclxuXHRcdHZhciBwcm9wcyA9IFsnb24nLCAnb2ZmJywgJ3RyaWdnZXInXTtcclxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspe1xyXG5cdFx0XHRkZXN0T2JqZWN0LnByb3RvdHlwZVtwcm9wc1tpXV0gPSBNaWNyb0V2ZW50LnByb3RvdHlwZVtwcm9wc1tpXV07XHJcblx0XHR9XHJcblx0fTtcclxuXHRcclxuXHR2YXIgSVNfTUFDICAgICAgICA9IC9NYWMvLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCk7XHJcblx0XHJcblx0dmFyIEtFWV9BICAgICAgICAgPSA2NTtcclxuXHR2YXIgS0VZX0NPTU1BICAgICA9IDE4ODtcclxuXHR2YXIgS0VZX1JFVFVSTiAgICA9IDEzO1xyXG5cdHZhciBLRVlfRVNDICAgICAgID0gMjc7XHJcblx0dmFyIEtFWV9MRUZUICAgICAgPSAzNztcclxuXHR2YXIgS0VZX1VQICAgICAgICA9IDM4O1xyXG5cdHZhciBLRVlfUCAgICAgICAgID0gODA7XHJcblx0dmFyIEtFWV9SSUdIVCAgICAgPSAzOTtcclxuXHR2YXIgS0VZX0RPV04gICAgICA9IDQwO1xyXG5cdHZhciBLRVlfTiAgICAgICAgID0gNzg7XHJcblx0dmFyIEtFWV9CQUNLU1BBQ0UgPSA4O1xyXG5cdHZhciBLRVlfREVMRVRFICAgID0gNDY7XHJcblx0dmFyIEtFWV9TSElGVCAgICAgPSAxNjtcclxuXHR2YXIgS0VZX0NNRCAgICAgICA9IElTX01BQyA/IDkxIDogMTc7XHJcblx0dmFyIEtFWV9DVFJMICAgICAgPSBJU19NQUMgPyAxOCA6IDE3O1xyXG5cdHZhciBLRVlfVEFCICAgICAgID0gOTtcclxuXHRcclxuXHR2YXIgVEFHX1NFTEVDVCAgICA9IDE7XHJcblx0dmFyIFRBR19JTlBVVCAgICAgPSAyO1xyXG5cdFxyXG5cdC8vIGZvciBub3csIGFuZHJvaWQgc3VwcG9ydCBpbiBnZW5lcmFsIGlzIHRvbyBzcG90dHkgdG8gc3VwcG9ydCB2YWxpZGl0eVxyXG5cdHZhciBTVVBQT1JUU19WQUxJRElUWV9BUEkgPSAhL2FuZHJvaWQvaS50ZXN0KHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50KSAmJiAhIWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2Zvcm0nKS52YWxpZGl0eTtcclxuXHRcclxuXHR2YXIgaXNzZXQgPSBmdW5jdGlvbihvYmplY3QpIHtcclxuXHRcdHJldHVybiB0eXBlb2Ygb2JqZWN0ICE9PSAndW5kZWZpbmVkJztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENvbnZlcnRzIGEgc2NhbGFyIHRvIGl0cyBiZXN0IHN0cmluZyByZXByZXNlbnRhdGlvblxyXG5cdCAqIGZvciBoYXNoIGtleXMgYW5kIEhUTUwgYXR0cmlidXRlIHZhbHVlcy5cclxuXHQgKlxyXG5cdCAqIFRyYW5zZm9ybWF0aW9uczpcclxuXHQgKiAgICdzdHInICAgICAtPiAnc3RyJ1xyXG5cdCAqICAgbnVsbCAgICAgIC0+ICcnXHJcblx0ICogICB1bmRlZmluZWQgLT4gJydcclxuXHQgKiAgIHRydWUgICAgICAtPiAnMSdcclxuXHQgKiAgIGZhbHNlICAgICAtPiAnMCdcclxuXHQgKiAgIDAgICAgICAgICAtPiAnMCdcclxuXHQgKiAgIDEgICAgICAgICAtPiAnMSdcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG5cdCAqIEByZXR1cm5zIHtzdHJpbmd8bnVsbH1cclxuXHQgKi9cclxuXHR2YXIgaGFzaF9rZXkgPSBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0aWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3VuZGVmaW5lZCcgfHwgdmFsdWUgPT09IG51bGwpIHJldHVybiBudWxsO1xyXG5cdFx0aWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ2Jvb2xlYW4nKSByZXR1cm4gdmFsdWUgPyAnMScgOiAnMCc7XHJcblx0XHRyZXR1cm4gdmFsdWUgKyAnJztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEVzY2FwZXMgYSBzdHJpbmcgZm9yIHVzZSB3aXRoaW4gSFRNTC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBzdHJcclxuXHQgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG5cdCAqL1xyXG5cdHZhciBlc2NhcGVfaHRtbCA9IGZ1bmN0aW9uKHN0cikge1xyXG5cdFx0cmV0dXJuIChzdHIgKyAnJylcclxuXHRcdFx0LnJlcGxhY2UoLyYvZywgJyZhbXA7JylcclxuXHRcdFx0LnJlcGxhY2UoLzwvZywgJyZsdDsnKVxyXG5cdFx0XHQucmVwbGFjZSgvPi9nLCAnJmd0OycpXHJcblx0XHRcdC5yZXBsYWNlKC9cIi9nLCAnJnF1b3Q7Jyk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBFc2NhcGVzIFwiJFwiIGNoYXJhY3RlcnMgaW4gcmVwbGFjZW1lbnQgc3RyaW5ncy5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBzdHJcclxuXHQgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG5cdCAqL1xyXG5cdHZhciBlc2NhcGVfcmVwbGFjZSA9IGZ1bmN0aW9uKHN0cikge1xyXG5cdFx0cmV0dXJuIChzdHIgKyAnJykucmVwbGFjZSgvXFwkL2csICckJCQkJyk7XHJcblx0fTtcclxuXHRcclxuXHR2YXIgaG9vayA9IHt9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFdyYXBzIGBtZXRob2RgIG9uIGBzZWxmYCBzbyB0aGF0IGBmbmBcclxuXHQgKiBpcyBpbnZva2VkIGJlZm9yZSB0aGUgb3JpZ2luYWwgbWV0aG9kLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtvYmplY3R9IHNlbGZcclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gbWV0aG9kXHJcblx0ICogQHBhcmFtIHtmdW5jdGlvbn0gZm5cclxuXHQgKi9cclxuXHRob29rLmJlZm9yZSA9IGZ1bmN0aW9uKHNlbGYsIG1ldGhvZCwgZm4pIHtcclxuXHRcdHZhciBvcmlnaW5hbCA9IHNlbGZbbWV0aG9kXTtcclxuXHRcdHNlbGZbbWV0aG9kXSA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRmbi5hcHBseShzZWxmLCBhcmd1bWVudHMpO1xyXG5cdFx0XHRyZXR1cm4gb3JpZ2luYWwuYXBwbHkoc2VsZiwgYXJndW1lbnRzKTtcclxuXHRcdH07XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBXcmFwcyBgbWV0aG9kYCBvbiBgc2VsZmAgc28gdGhhdCBgZm5gXHJcblx0ICogaXMgaW52b2tlZCBhZnRlciB0aGUgb3JpZ2luYWwgbWV0aG9kLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtvYmplY3R9IHNlbGZcclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gbWV0aG9kXHJcblx0ICogQHBhcmFtIHtmdW5jdGlvbn0gZm5cclxuXHQgKi9cclxuXHRob29rLmFmdGVyID0gZnVuY3Rpb24oc2VsZiwgbWV0aG9kLCBmbikge1xyXG5cdFx0dmFyIG9yaWdpbmFsID0gc2VsZlttZXRob2RdO1xyXG5cdFx0c2VsZlttZXRob2RdID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciByZXN1bHQgPSBvcmlnaW5hbC5hcHBseShzZWxmLCBhcmd1bWVudHMpO1xyXG5cdFx0XHRmbi5hcHBseShzZWxmLCBhcmd1bWVudHMpO1xyXG5cdFx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdFx0fTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFdyYXBzIGBmbmAgc28gdGhhdCBpdCBjYW4gb25seSBiZSBpbnZva2VkIG9uY2UuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge2Z1bmN0aW9ufSBmblxyXG5cdCAqIEByZXR1cm5zIHtmdW5jdGlvbn1cclxuXHQgKi9cclxuXHR2YXIgb25jZSA9IGZ1bmN0aW9uKGZuKSB7XHJcblx0XHR2YXIgY2FsbGVkID0gZmFsc2U7XHJcblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XHJcblx0XHRcdGlmIChjYWxsZWQpIHJldHVybjtcclxuXHRcdFx0Y2FsbGVkID0gdHJ1ZTtcclxuXHRcdFx0Zm4uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcclxuXHRcdH07XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBXcmFwcyBgZm5gIHNvIHRoYXQgaXQgY2FuIG9ubHkgYmUgY2FsbGVkIG9uY2VcclxuXHQgKiBldmVyeSBgZGVsYXlgIG1pbGxpc2Vjb25kcyAoaW52b2tlZCBvbiB0aGUgZmFsbGluZyBlZGdlKS5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7ZnVuY3Rpb259IGZuXHJcblx0ICogQHBhcmFtIHtpbnR9IGRlbGF5XHJcblx0ICogQHJldHVybnMge2Z1bmN0aW9ufVxyXG5cdCAqL1xyXG5cdHZhciBkZWJvdW5jZSA9IGZ1bmN0aW9uKGZuLCBkZWxheSkge1xyXG5cdFx0dmFyIHRpbWVvdXQ7XHJcblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0dmFyIGFyZ3MgPSBhcmd1bWVudHM7XHJcblx0XHRcdHdpbmRvdy5jbGVhclRpbWVvdXQodGltZW91dCk7XHJcblx0XHRcdHRpbWVvdXQgPSB3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRmbi5hcHBseShzZWxmLCBhcmdzKTtcclxuXHRcdFx0fSwgZGVsYXkpO1xyXG5cdFx0fTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIERlYm91bmNlIGFsbCBmaXJlZCBldmVudHMgdHlwZXMgbGlzdGVkIGluIGB0eXBlc2BcclxuXHQgKiB3aGlsZSBleGVjdXRpbmcgdGhlIHByb3ZpZGVkIGBmbmAuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge29iamVjdH0gc2VsZlxyXG5cdCAqIEBwYXJhbSB7YXJyYXl9IHR5cGVzXHJcblx0ICogQHBhcmFtIHtmdW5jdGlvbn0gZm5cclxuXHQgKi9cclxuXHR2YXIgZGVib3VuY2VfZXZlbnRzID0gZnVuY3Rpb24oc2VsZiwgdHlwZXMsIGZuKSB7XHJcblx0XHR2YXIgdHlwZTtcclxuXHRcdHZhciB0cmlnZ2VyID0gc2VsZi50cmlnZ2VyO1xyXG5cdFx0dmFyIGV2ZW50X2FyZ3MgPSB7fTtcclxuXHRcclxuXHRcdC8vIG92ZXJyaWRlIHRyaWdnZXIgbWV0aG9kXHJcblx0XHRzZWxmLnRyaWdnZXIgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIHR5cGUgPSBhcmd1bWVudHNbMF07XHJcblx0XHRcdGlmICh0eXBlcy5pbmRleE9mKHR5cGUpICE9PSAtMSkge1xyXG5cdFx0XHRcdGV2ZW50X2FyZ3NbdHlwZV0gPSBhcmd1bWVudHM7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0cmV0dXJuIHRyaWdnZXIuYXBwbHkoc2VsZiwgYXJndW1lbnRzKTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRcdC8vIGludm9rZSBwcm92aWRlZCBmdW5jdGlvblxyXG5cdFx0Zm4uYXBwbHkoc2VsZiwgW10pO1xyXG5cdFx0c2VsZi50cmlnZ2VyID0gdHJpZ2dlcjtcclxuXHRcclxuXHRcdC8vIHRyaWdnZXIgcXVldWVkIGV2ZW50c1xyXG5cdFx0Zm9yICh0eXBlIGluIGV2ZW50X2FyZ3MpIHtcclxuXHRcdFx0aWYgKGV2ZW50X2FyZ3MuaGFzT3duUHJvcGVydHkodHlwZSkpIHtcclxuXHRcdFx0XHR0cmlnZ2VyLmFwcGx5KHNlbGYsIGV2ZW50X2FyZ3NbdHlwZV0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBBIHdvcmthcm91bmQgZm9yIGh0dHA6Ly9idWdzLmpxdWVyeS5jb20vdGlja2V0LzY2OTZcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7b2JqZWN0fSAkcGFyZW50IC0gUGFyZW50IGVsZW1lbnQgdG8gbGlzdGVuIG9uLlxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBldmVudCAtIEV2ZW50IG5hbWUuXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHNlbGVjdG9yIC0gRGVzY2VuZGFudCBzZWxlY3RvciB0byBmaWx0ZXIgYnkuXHJcblx0ICogQHBhcmFtIHtmdW5jdGlvbn0gZm4gLSBFdmVudCBoYW5kbGVyLlxyXG5cdCAqL1xyXG5cdHZhciB3YXRjaENoaWxkRXZlbnQgPSBmdW5jdGlvbigkcGFyZW50LCBldmVudCwgc2VsZWN0b3IsIGZuKSB7XHJcblx0XHQkcGFyZW50Lm9uKGV2ZW50LCBzZWxlY3RvciwgZnVuY3Rpb24oZSkge1xyXG5cdFx0XHR2YXIgY2hpbGQgPSBlLnRhcmdldDtcclxuXHRcdFx0d2hpbGUgKGNoaWxkICYmIGNoaWxkLnBhcmVudE5vZGUgIT09ICRwYXJlbnRbMF0pIHtcclxuXHRcdFx0XHRjaGlsZCA9IGNoaWxkLnBhcmVudE5vZGU7XHJcblx0XHRcdH1cclxuXHRcdFx0ZS5jdXJyZW50VGFyZ2V0ID0gY2hpbGQ7XHJcblx0XHRcdHJldHVybiBmbi5hcHBseSh0aGlzLCBbZV0pO1xyXG5cdFx0fSk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBEZXRlcm1pbmVzIHRoZSBjdXJyZW50IHNlbGVjdGlvbiB3aXRoaW4gYSB0ZXh0IGlucHV0IGNvbnRyb2wuXHJcblx0ICogUmV0dXJucyBhbiBvYmplY3QgY29udGFpbmluZzpcclxuXHQgKiAgIC0gc3RhcnRcclxuXHQgKiAgIC0gbGVuZ3RoXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge29iamVjdH0gaW5wdXRcclxuXHQgKiBAcmV0dXJucyB7b2JqZWN0fVxyXG5cdCAqL1xyXG5cdHZhciBnZXRTZWxlY3Rpb24gPSBmdW5jdGlvbihpbnB1dCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IHt9O1xyXG5cdFx0aWYgKCdzZWxlY3Rpb25TdGFydCcgaW4gaW5wdXQpIHtcclxuXHRcdFx0cmVzdWx0LnN0YXJ0ID0gaW5wdXQuc2VsZWN0aW9uU3RhcnQ7XHJcblx0XHRcdHJlc3VsdC5sZW5ndGggPSBpbnB1dC5zZWxlY3Rpb25FbmQgLSByZXN1bHQuc3RhcnQ7XHJcblx0XHR9IGVsc2UgaWYgKGRvY3VtZW50LnNlbGVjdGlvbikge1xyXG5cdFx0XHRpbnB1dC5mb2N1cygpO1xyXG5cdFx0XHR2YXIgc2VsID0gZG9jdW1lbnQuc2VsZWN0aW9uLmNyZWF0ZVJhbmdlKCk7XHJcblx0XHRcdHZhciBzZWxMZW4gPSBkb2N1bWVudC5zZWxlY3Rpb24uY3JlYXRlUmFuZ2UoKS50ZXh0Lmxlbmd0aDtcclxuXHRcdFx0c2VsLm1vdmVTdGFydCgnY2hhcmFjdGVyJywgLWlucHV0LnZhbHVlLmxlbmd0aCk7XHJcblx0XHRcdHJlc3VsdC5zdGFydCA9IHNlbC50ZXh0Lmxlbmd0aCAtIHNlbExlbjtcclxuXHRcdFx0cmVzdWx0Lmxlbmd0aCA9IHNlbExlbjtcclxuXHRcdH1cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDb3BpZXMgQ1NTIHByb3BlcnRpZXMgZnJvbSBvbmUgZWxlbWVudCB0byBhbm90aGVyLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtvYmplY3R9ICRmcm9tXHJcblx0ICogQHBhcmFtIHtvYmplY3R9ICR0b1xyXG5cdCAqIEBwYXJhbSB7YXJyYXl9IHByb3BlcnRpZXNcclxuXHQgKi9cclxuXHR2YXIgdHJhbnNmZXJTdHlsZXMgPSBmdW5jdGlvbigkZnJvbSwgJHRvLCBwcm9wZXJ0aWVzKSB7XHJcblx0XHR2YXIgaSwgbiwgc3R5bGVzID0ge307XHJcblx0XHRpZiAocHJvcGVydGllcykge1xyXG5cdFx0XHRmb3IgKGkgPSAwLCBuID0gcHJvcGVydGllcy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuXHRcdFx0XHRzdHlsZXNbcHJvcGVydGllc1tpXV0gPSAkZnJvbS5jc3MocHJvcGVydGllc1tpXSk7XHJcblx0XHRcdH1cclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHN0eWxlcyA9ICRmcm9tLmNzcygpO1xyXG5cdFx0fVxyXG5cdFx0JHRvLmNzcyhzdHlsZXMpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogTWVhc3VyZXMgdGhlIHdpZHRoIG9mIGEgc3RyaW5nIHdpdGhpbiBhXHJcblx0ICogcGFyZW50IGVsZW1lbnQgKGluIHBpeGVscykuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gc3RyXHJcblx0ICogQHBhcmFtIHtvYmplY3R9ICRwYXJlbnRcclxuXHQgKiBAcmV0dXJucyB7aW50fVxyXG5cdCAqL1xyXG5cdHZhciBtZWFzdXJlU3RyaW5nID0gZnVuY3Rpb24oc3RyLCAkcGFyZW50KSB7XHJcblx0XHRpZiAoIXN0cikge1xyXG5cdFx0XHRyZXR1cm4gMDtcclxuXHRcdH1cclxuXHRcclxuXHRcdHZhciAkdGVzdCA9ICQoJzx0ZXN0PicpLmNzcyh7XHJcblx0XHRcdHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxyXG5cdFx0XHR0b3A6IC05OTk5OSxcclxuXHRcdFx0bGVmdDogLTk5OTk5LFxyXG5cdFx0XHR3aWR0aDogJ2F1dG8nLFxyXG5cdFx0XHRwYWRkaW5nOiAwLFxyXG5cdFx0XHR3aGl0ZVNwYWNlOiAncHJlJ1xyXG5cdFx0fSkudGV4dChzdHIpLmFwcGVuZFRvKCdib2R5Jyk7XHJcblx0XHJcblx0XHR0cmFuc2ZlclN0eWxlcygkcGFyZW50LCAkdGVzdCwgW1xyXG5cdFx0XHQnbGV0dGVyU3BhY2luZycsXHJcblx0XHRcdCdmb250U2l6ZScsXHJcblx0XHRcdCdmb250RmFtaWx5JyxcclxuXHRcdFx0J2ZvbnRXZWlnaHQnLFxyXG5cdFx0XHQndGV4dFRyYW5zZm9ybSdcclxuXHRcdF0pO1xyXG5cdFxyXG5cdFx0dmFyIHdpZHRoID0gJHRlc3Qud2lkdGgoKTtcclxuXHRcdCR0ZXN0LnJlbW92ZSgpO1xyXG5cdFxyXG5cdFx0cmV0dXJuIHdpZHRoO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2V0cyB1cCBhbiBpbnB1dCB0byBncm93IGhvcml6b250YWxseSBhcyB0aGUgdXNlclxyXG5cdCAqIHR5cGVzLiBJZiB0aGUgdmFsdWUgaXMgY2hhbmdlZCBtYW51YWxseSwgeW91IGNhblxyXG5cdCAqIHRyaWdnZXIgdGhlIFwidXBkYXRlXCIgaGFuZGxlciB0byByZXNpemU6XHJcblx0ICpcclxuXHQgKiAkaW5wdXQudHJpZ2dlcigndXBkYXRlJyk7XHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge29iamVjdH0gJGlucHV0XHJcblx0ICovXHJcblx0dmFyIGF1dG9Hcm93ID0gZnVuY3Rpb24oJGlucHV0KSB7XHJcblx0XHR2YXIgY3VycmVudFdpZHRoID0gbnVsbDtcclxuXHRcclxuXHRcdHZhciB1cGRhdGUgPSBmdW5jdGlvbihlLCBvcHRpb25zKSB7XHJcblx0XHRcdHZhciB2YWx1ZSwga2V5Q29kZSwgcHJpbnRhYmxlLCBwbGFjZWhvbGRlciwgd2lkdGg7XHJcblx0XHRcdHZhciBzaGlmdCwgY2hhcmFjdGVyLCBzZWxlY3Rpb247XHJcblx0XHRcdGUgPSBlIHx8IHdpbmRvdy5ldmVudCB8fCB7fTtcclxuXHRcdFx0b3B0aW9ucyA9IG9wdGlvbnMgfHwge307XHJcblx0XHJcblx0XHRcdGlmIChlLm1ldGFLZXkgfHwgZS5hbHRLZXkpIHJldHVybjtcclxuXHRcdFx0aWYgKCFvcHRpb25zLmZvcmNlICYmICRpbnB1dC5kYXRhKCdncm93JykgPT09IGZhbHNlKSByZXR1cm47XHJcblx0XHJcblx0XHRcdHZhbHVlID0gJGlucHV0LnZhbCgpO1xyXG5cdFx0XHRpZiAoZS50eXBlICYmIGUudHlwZS50b0xvd2VyQ2FzZSgpID09PSAna2V5ZG93bicpIHtcclxuXHRcdFx0XHRrZXlDb2RlID0gZS5rZXlDb2RlO1xyXG5cdFx0XHRcdHByaW50YWJsZSA9IChcclxuXHRcdFx0XHRcdChrZXlDb2RlID49IDk3ICYmIGtleUNvZGUgPD0gMTIyKSB8fCAvLyBhLXpcclxuXHRcdFx0XHRcdChrZXlDb2RlID49IDY1ICYmIGtleUNvZGUgPD0gOTApICB8fCAvLyBBLVpcclxuXHRcdFx0XHRcdChrZXlDb2RlID49IDQ4ICYmIGtleUNvZGUgPD0gNTcpICB8fCAvLyAwLTlcclxuXHRcdFx0XHRcdGtleUNvZGUgPT09IDMyIC8vIHNwYWNlXHJcblx0XHRcdFx0KTtcclxuXHRcclxuXHRcdFx0XHRpZiAoa2V5Q29kZSA9PT0gS0VZX0RFTEVURSB8fCBrZXlDb2RlID09PSBLRVlfQkFDS1NQQUNFKSB7XHJcblx0XHRcdFx0XHRzZWxlY3Rpb24gPSBnZXRTZWxlY3Rpb24oJGlucHV0WzBdKTtcclxuXHRcdFx0XHRcdGlmIChzZWxlY3Rpb24ubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRcdHZhbHVlID0gdmFsdWUuc3Vic3RyaW5nKDAsIHNlbGVjdGlvbi5zdGFydCkgKyB2YWx1ZS5zdWJzdHJpbmcoc2VsZWN0aW9uLnN0YXJ0ICsgc2VsZWN0aW9uLmxlbmd0aCk7XHJcblx0XHRcdFx0XHR9IGVsc2UgaWYgKGtleUNvZGUgPT09IEtFWV9CQUNLU1BBQ0UgJiYgc2VsZWN0aW9uLnN0YXJ0KSB7XHJcblx0XHRcdFx0XHRcdHZhbHVlID0gdmFsdWUuc3Vic3RyaW5nKDAsIHNlbGVjdGlvbi5zdGFydCAtIDEpICsgdmFsdWUuc3Vic3RyaW5nKHNlbGVjdGlvbi5zdGFydCArIDEpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIGlmIChrZXlDb2RlID09PSBLRVlfREVMRVRFICYmIHR5cGVvZiBzZWxlY3Rpb24uc3RhcnQgIT09ICd1bmRlZmluZWQnKSB7XHJcblx0XHRcdFx0XHRcdHZhbHVlID0gdmFsdWUuc3Vic3RyaW5nKDAsIHNlbGVjdGlvbi5zdGFydCkgKyB2YWx1ZS5zdWJzdHJpbmcoc2VsZWN0aW9uLnN0YXJ0ICsgMSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSBlbHNlIGlmIChwcmludGFibGUpIHtcclxuXHRcdFx0XHRcdHNoaWZ0ID0gZS5zaGlmdEtleTtcclxuXHRcdFx0XHRcdGNoYXJhY3RlciA9IFN0cmluZy5mcm9tQ2hhckNvZGUoZS5rZXlDb2RlKTtcclxuXHRcdFx0XHRcdGlmIChzaGlmdCkgY2hhcmFjdGVyID0gY2hhcmFjdGVyLnRvVXBwZXJDYXNlKCk7XHJcblx0XHRcdFx0XHRlbHNlIGNoYXJhY3RlciA9IGNoYXJhY3Rlci50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0XHRcdFx0dmFsdWUgKz0gY2hhcmFjdGVyO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRwbGFjZWhvbGRlciA9ICRpbnB1dC5hdHRyKCdwbGFjZWhvbGRlcicpO1xyXG5cdFx0XHRpZiAoIXZhbHVlICYmIHBsYWNlaG9sZGVyKSB7XHJcblx0XHRcdFx0dmFsdWUgPSBwbGFjZWhvbGRlcjtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHR3aWR0aCA9IG1lYXN1cmVTdHJpbmcodmFsdWUsICRpbnB1dCkgKyA0O1xyXG5cdFx0XHRpZiAod2lkdGggIT09IGN1cnJlbnRXaWR0aCkge1xyXG5cdFx0XHRcdGN1cnJlbnRXaWR0aCA9IHdpZHRoO1xyXG5cdFx0XHRcdCRpbnB1dC53aWR0aCh3aWR0aCk7XHJcblx0XHRcdFx0JGlucHV0LnRyaWdnZXJIYW5kbGVyKCdyZXNpemUnKTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRcdCRpbnB1dC5vbigna2V5ZG93biBrZXl1cCB1cGRhdGUgYmx1cicsIHVwZGF0ZSk7XHJcblx0XHR1cGRhdGUoKTtcclxuXHR9O1xyXG5cdFxyXG5cdHZhciBTZWxlY3RpemUgPSBmdW5jdGlvbigkaW5wdXQsIHNldHRpbmdzKSB7XHJcblx0XHR2YXIga2V5LCBpLCBuLCBkaXIsIGlucHV0LCBzZWxmID0gdGhpcztcclxuXHRcdGlucHV0ID0gJGlucHV0WzBdO1xyXG5cdFx0aW5wdXQuc2VsZWN0aXplID0gc2VsZjtcclxuXHRcclxuXHRcdC8vIGRldGVjdCBydGwgZW52aXJvbm1lbnRcclxuXHRcdHZhciBjb21wdXRlZFN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUgJiYgd2luZG93LmdldENvbXB1dGVkU3R5bGUoaW5wdXQsIG51bGwpO1xyXG5cdFx0ZGlyID0gY29tcHV0ZWRTdHlsZSA/IGNvbXB1dGVkU3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnZGlyZWN0aW9uJykgOiBpbnB1dC5jdXJyZW50U3R5bGUgJiYgaW5wdXQuY3VycmVudFN0eWxlLmRpcmVjdGlvbjtcclxuXHRcdGRpciA9IGRpciB8fCAkaW5wdXQucGFyZW50cygnW2Rpcl06Zmlyc3QnKS5hdHRyKCdkaXInKSB8fCAnJztcclxuXHRcclxuXHRcdC8vIHNldHVwIGRlZmF1bHQgc3RhdGVcclxuXHRcdCQuZXh0ZW5kKHNlbGYsIHtcclxuXHRcdFx0b3JkZXIgICAgICAgICAgICA6IDAsXHJcblx0XHRcdHNldHRpbmdzICAgICAgICAgOiBzZXR0aW5ncyxcclxuXHRcdFx0JGlucHV0ICAgICAgICAgICA6ICRpbnB1dCxcclxuXHRcdFx0dGFiSW5kZXggICAgICAgICA6ICRpbnB1dC5hdHRyKCd0YWJpbmRleCcpIHx8ICcnLFxyXG5cdFx0XHR0YWdUeXBlICAgICAgICAgIDogaW5wdXQudGFnTmFtZS50b0xvd2VyQ2FzZSgpID09PSAnc2VsZWN0JyA/IFRBR19TRUxFQ1QgOiBUQUdfSU5QVVQsXHJcblx0XHRcdHJ0bCAgICAgICAgICAgICAgOiAvcnRsL2kudGVzdChkaXIpLFxyXG5cdFxyXG5cdFx0XHRldmVudE5TICAgICAgICAgIDogJy5zZWxlY3RpemUnICsgKCsrU2VsZWN0aXplLmNvdW50KSxcclxuXHRcdFx0aGlnaGxpZ2h0ZWRWYWx1ZSA6IG51bGwsXHJcblx0XHRcdGlzT3BlbiAgICAgICAgICAgOiBmYWxzZSxcclxuXHRcdFx0aXNEaXNhYmxlZCAgICAgICA6IGZhbHNlLFxyXG5cdFx0XHRpc1JlcXVpcmVkICAgICAgIDogJGlucHV0LmlzKCdbcmVxdWlyZWRdJyksXHJcblx0XHRcdGlzSW52YWxpZCAgICAgICAgOiBmYWxzZSxcclxuXHRcdFx0aXNMb2NrZWQgICAgICAgICA6IGZhbHNlLFxyXG5cdFx0XHRpc0ZvY3VzZWQgICAgICAgIDogZmFsc2UsXHJcblx0XHRcdGlzSW5wdXRIaWRkZW4gICAgOiBmYWxzZSxcclxuXHRcdFx0aXNTZXR1cCAgICAgICAgICA6IGZhbHNlLFxyXG5cdFx0XHRpc1NoaWZ0RG93biAgICAgIDogZmFsc2UsXHJcblx0XHRcdGlzQ21kRG93biAgICAgICAgOiBmYWxzZSxcclxuXHRcdFx0aXNDdHJsRG93biAgICAgICA6IGZhbHNlLFxyXG5cdFx0XHRpZ25vcmVGb2N1cyAgICAgIDogZmFsc2UsXHJcblx0XHRcdGlnbm9yZUJsdXIgICAgICAgOiBmYWxzZSxcclxuXHRcdFx0aWdub3JlSG92ZXIgICAgICA6IGZhbHNlLFxyXG5cdFx0XHRoYXNPcHRpb25zICAgICAgIDogZmFsc2UsXHJcblx0XHRcdGN1cnJlbnRSZXN1bHRzICAgOiBudWxsLFxyXG5cdFx0XHRsYXN0VmFsdWUgICAgICAgIDogJycsXHJcblx0XHRcdGNhcmV0UG9zICAgICAgICAgOiAwLFxyXG5cdFx0XHRsb2FkaW5nICAgICAgICAgIDogMCxcclxuXHRcdFx0bG9hZGVkU2VhcmNoZXMgICA6IHt9LFxyXG5cdFxyXG5cdFx0XHQkYWN0aXZlT3B0aW9uICAgIDogbnVsbCxcclxuXHRcdFx0JGFjdGl2ZUl0ZW1zICAgICA6IFtdLFxyXG5cdFxyXG5cdFx0XHRvcHRncm91cHMgICAgICAgIDoge30sXHJcblx0XHRcdG9wdGlvbnMgICAgICAgICAgOiB7fSxcclxuXHRcdFx0dXNlck9wdGlvbnMgICAgICA6IHt9LFxyXG5cdFx0XHRpdGVtcyAgICAgICAgICAgIDogW10sXHJcblx0XHRcdHJlbmRlckNhY2hlICAgICAgOiB7fSxcclxuXHRcdFx0b25TZWFyY2hDaGFuZ2UgICA6IHNldHRpbmdzLmxvYWRUaHJvdHRsZSA9PT0gbnVsbCA/IHNlbGYub25TZWFyY2hDaGFuZ2UgOiBkZWJvdW5jZShzZWxmLm9uU2VhcmNoQ2hhbmdlLCBzZXR0aW5ncy5sb2FkVGhyb3R0bGUpXHJcblx0XHR9KTtcclxuXHRcclxuXHRcdC8vIHNlYXJjaCBzeXN0ZW1cclxuXHRcdHNlbGYuc2lmdGVyID0gbmV3IFNpZnRlcih0aGlzLm9wdGlvbnMsIHtkaWFjcml0aWNzOiBzZXR0aW5ncy5kaWFjcml0aWNzfSk7XHJcblx0XHJcblx0XHQvLyBidWlsZCBvcHRpb25zIHRhYmxlXHJcblx0XHRpZiAoc2VsZi5zZXR0aW5ncy5vcHRpb25zKSB7XHJcblx0XHRcdGZvciAoaSA9IDAsIG4gPSBzZWxmLnNldHRpbmdzLm9wdGlvbnMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdFx0c2VsZi5yZWdpc3Rlck9wdGlvbihzZWxmLnNldHRpbmdzLm9wdGlvbnNbaV0pO1xyXG5cdFx0XHR9XHJcblx0XHRcdGRlbGV0ZSBzZWxmLnNldHRpbmdzLm9wdGlvbnM7XHJcblx0XHR9XHJcblx0XHJcblx0XHQvLyBidWlsZCBvcHRncm91cCB0YWJsZVxyXG5cdFx0aWYgKHNlbGYuc2V0dGluZ3Mub3B0Z3JvdXBzKSB7XHJcblx0XHRcdGZvciAoaSA9IDAsIG4gPSBzZWxmLnNldHRpbmdzLm9wdGdyb3Vwcy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuXHRcdFx0XHRzZWxmLnJlZ2lzdGVyT3B0aW9uR3JvdXAoc2VsZi5zZXR0aW5ncy5vcHRncm91cHNbaV0pO1xyXG5cdFx0XHR9XHJcblx0XHRcdGRlbGV0ZSBzZWxmLnNldHRpbmdzLm9wdGdyb3VwcztcclxuXHRcdH1cclxuXHRcclxuXHRcdC8vIG9wdGlvbi1kZXBlbmRlbnQgZGVmYXVsdHNcclxuXHRcdHNlbGYuc2V0dGluZ3MubW9kZSA9IHNlbGYuc2V0dGluZ3MubW9kZSB8fCAoc2VsZi5zZXR0aW5ncy5tYXhJdGVtcyA9PT0gMSA/ICdzaW5nbGUnIDogJ211bHRpJyk7XHJcblx0XHRpZiAodHlwZW9mIHNlbGYuc2V0dGluZ3MuaGlkZVNlbGVjdGVkICE9PSAnYm9vbGVhbicpIHtcclxuXHRcdFx0c2VsZi5zZXR0aW5ncy5oaWRlU2VsZWN0ZWQgPSBzZWxmLnNldHRpbmdzLm1vZGUgPT09ICdtdWx0aSc7XHJcblx0XHR9XHJcblx0XHJcblx0XHRzZWxmLmluaXRpYWxpemVQbHVnaW5zKHNlbGYuc2V0dGluZ3MucGx1Z2lucyk7XHJcblx0XHRzZWxmLnNldHVwQ2FsbGJhY2tzKCk7XHJcblx0XHRzZWxmLnNldHVwVGVtcGxhdGVzKCk7XHJcblx0XHRzZWxmLnNldHVwKCk7XHJcblx0fTtcclxuXHRcclxuXHQvLyBtaXhpbnNcclxuXHQvLyAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLVxyXG5cdFxyXG5cdE1pY3JvRXZlbnQubWl4aW4oU2VsZWN0aXplKTtcclxuXHRNaWNyb1BsdWdpbi5taXhpbihTZWxlY3RpemUpO1xyXG5cdFxyXG5cdC8vIG1ldGhvZHNcclxuXHQvLyAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLSAtIC0gLVxyXG5cdFxyXG5cdCQuZXh0ZW5kKFNlbGVjdGl6ZS5wcm90b3R5cGUsIHtcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogQ3JlYXRlcyBhbGwgZWxlbWVudHMgYW5kIHNldHMgdXAgZXZlbnQgYmluZGluZ3MuXHJcblx0XHQgKi9cclxuXHRcdHNldHVwOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIHNlbGYgICAgICA9IHRoaXM7XHJcblx0XHRcdHZhciBzZXR0aW5ncyAgPSBzZWxmLnNldHRpbmdzO1xyXG5cdFx0XHR2YXIgZXZlbnROUyAgID0gc2VsZi5ldmVudE5TO1xyXG5cdFx0XHR2YXIgJHdpbmRvdyAgID0gJCh3aW5kb3cpO1xyXG5cdFx0XHR2YXIgJGRvY3VtZW50ID0gJChkb2N1bWVudCk7XHJcblx0XHRcdHZhciAkaW5wdXQgICAgPSBzZWxmLiRpbnB1dDtcclxuXHRcclxuXHRcdFx0dmFyICR3cmFwcGVyO1xyXG5cdFx0XHR2YXIgJGNvbnRyb2w7XHJcblx0XHRcdHZhciAkY29udHJvbF9pbnB1dDtcclxuXHRcdFx0dmFyICRkcm9wZG93bjtcclxuXHRcdFx0dmFyICRkcm9wZG93bl9jb250ZW50O1xyXG5cdFx0XHR2YXIgJGRyb3Bkb3duX3BhcmVudDtcclxuXHRcdFx0dmFyIGlucHV0TW9kZTtcclxuXHRcdFx0dmFyIHRpbWVvdXRfYmx1cjtcclxuXHRcdFx0dmFyIHRpbWVvdXRfZm9jdXM7XHJcblx0XHRcdHZhciBjbGFzc2VzO1xyXG5cdFx0XHR2YXIgY2xhc3Nlc19wbHVnaW5zO1xyXG5cdFxyXG5cdFx0XHRpbnB1dE1vZGUgICAgICAgICA9IHNlbGYuc2V0dGluZ3MubW9kZTtcclxuXHRcdFx0Y2xhc3NlcyAgICAgICAgICAgPSAkaW5wdXQuYXR0cignY2xhc3MnKSB8fCAnJztcclxuXHRcclxuXHRcdFx0JHdyYXBwZXIgICAgICAgICAgPSAkKCc8ZGl2PicpLmFkZENsYXNzKHNldHRpbmdzLndyYXBwZXJDbGFzcykuYWRkQ2xhc3MoY2xhc3NlcykuYWRkQ2xhc3MoaW5wdXRNb2RlKTtcclxuXHRcdFx0JGNvbnRyb2wgICAgICAgICAgPSAkKCc8ZGl2PicpLmFkZENsYXNzKHNldHRpbmdzLmlucHV0Q2xhc3MpLmFkZENsYXNzKCdpdGVtcycpLmFwcGVuZFRvKCR3cmFwcGVyKTtcclxuXHRcdFx0JGNvbnRyb2xfaW5wdXQgICAgPSAkKCc8aW5wdXQgdHlwZT1cInRleHRcIiBhdXRvY29tcGxldGU9XCJvZmZcIiAvPicpLmFwcGVuZFRvKCRjb250cm9sKS5hdHRyKCd0YWJpbmRleCcsICRpbnB1dC5pcygnOmRpc2FibGVkJykgPyAnLTEnIDogc2VsZi50YWJJbmRleCk7XHJcblx0XHRcdCRkcm9wZG93bl9wYXJlbnQgID0gJChzZXR0aW5ncy5kcm9wZG93blBhcmVudCB8fCAkd3JhcHBlcik7XHJcblx0XHRcdCRkcm9wZG93biAgICAgICAgID0gJCgnPGRpdj4nKS5hZGRDbGFzcyhzZXR0aW5ncy5kcm9wZG93bkNsYXNzKS5hZGRDbGFzcyhpbnB1dE1vZGUpLmhpZGUoKS5hcHBlbmRUbygkZHJvcGRvd25fcGFyZW50KTtcclxuXHRcdFx0JGRyb3Bkb3duX2NvbnRlbnQgPSAkKCc8ZGl2PicpLmFkZENsYXNzKHNldHRpbmdzLmRyb3Bkb3duQ29udGVudENsYXNzKS5hcHBlbmRUbygkZHJvcGRvd24pO1xyXG5cdFxyXG5cdFx0XHRpZihzZWxmLnNldHRpbmdzLmNvcHlDbGFzc2VzVG9Ecm9wZG93bikge1xyXG5cdFx0XHRcdCRkcm9wZG93bi5hZGRDbGFzcyhjbGFzc2VzKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQkd3JhcHBlci5jc3Moe1xyXG5cdFx0XHRcdHdpZHRoOiAkaW5wdXRbMF0uc3R5bGUud2lkdGhcclxuXHRcdFx0fSk7XHJcblx0XHJcblx0XHRcdGlmIChzZWxmLnBsdWdpbnMubmFtZXMubGVuZ3RoKSB7XHJcblx0XHRcdFx0Y2xhc3Nlc19wbHVnaW5zID0gJ3BsdWdpbi0nICsgc2VsZi5wbHVnaW5zLm5hbWVzLmpvaW4oJyBwbHVnaW4tJyk7XHJcblx0XHRcdFx0JHdyYXBwZXIuYWRkQ2xhc3MoY2xhc3Nlc19wbHVnaW5zKTtcclxuXHRcdFx0XHQkZHJvcGRvd24uYWRkQ2xhc3MoY2xhc3Nlc19wbHVnaW5zKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRpZiAoKHNldHRpbmdzLm1heEl0ZW1zID09PSBudWxsIHx8IHNldHRpbmdzLm1heEl0ZW1zID4gMSkgJiYgc2VsZi50YWdUeXBlID09PSBUQUdfU0VMRUNUKSB7XHJcblx0XHRcdFx0JGlucHV0LmF0dHIoJ211bHRpcGxlJywgJ211bHRpcGxlJyk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0aWYgKHNlbGYuc2V0dGluZ3MucGxhY2Vob2xkZXIpIHtcclxuXHRcdFx0XHQkY29udHJvbF9pbnB1dC5hdHRyKCdwbGFjZWhvbGRlcicsIHNldHRpbmdzLnBsYWNlaG9sZGVyKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQvLyBpZiBzcGxpdE9uIHdhcyBub3QgcGFzc2VkIGluLCBjb25zdHJ1Y3QgaXQgZnJvbSB0aGUgZGVsaW1pdGVyIHRvIGFsbG93IHBhc3RpbmcgdW5pdmVyc2FsbHlcclxuXHRcdFx0aWYgKCFzZWxmLnNldHRpbmdzLnNwbGl0T24gJiYgc2VsZi5zZXR0aW5ncy5kZWxpbWl0ZXIpIHtcclxuXHRcdFx0XHR2YXIgZGVsaW1pdGVyRXNjYXBlZCA9IHNlbGYuc2V0dGluZ3MuZGVsaW1pdGVyLnJlcGxhY2UoL1stXFwvXFxcXF4kKis/LigpfFtcXF17fV0vZywgJ1xcXFwkJicpO1xyXG5cdFx0XHRcdHNlbGYuc2V0dGluZ3Muc3BsaXRPbiA9IG5ldyBSZWdFeHAoJ1xcXFxzKicgKyBkZWxpbWl0ZXJFc2NhcGVkICsgJytcXFxccyonKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRpZiAoJGlucHV0LmF0dHIoJ2F1dG9jb3JyZWN0JykpIHtcclxuXHRcdFx0XHQkY29udHJvbF9pbnB1dC5hdHRyKCdhdXRvY29ycmVjdCcsICRpbnB1dC5hdHRyKCdhdXRvY29ycmVjdCcpKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRpZiAoJGlucHV0LmF0dHIoJ2F1dG9jYXBpdGFsaXplJykpIHtcclxuXHRcdFx0XHQkY29udHJvbF9pbnB1dC5hdHRyKCdhdXRvY2FwaXRhbGl6ZScsICRpbnB1dC5hdHRyKCdhdXRvY2FwaXRhbGl6ZScpKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRzZWxmLiR3cmFwcGVyICAgICAgICAgID0gJHdyYXBwZXI7XHJcblx0XHRcdHNlbGYuJGNvbnRyb2wgICAgICAgICAgPSAkY29udHJvbDtcclxuXHRcdFx0c2VsZi4kY29udHJvbF9pbnB1dCAgICA9ICRjb250cm9sX2lucHV0O1xyXG5cdFx0XHRzZWxmLiRkcm9wZG93biAgICAgICAgID0gJGRyb3Bkb3duO1xyXG5cdFx0XHRzZWxmLiRkcm9wZG93bl9jb250ZW50ID0gJGRyb3Bkb3duX2NvbnRlbnQ7XHJcblx0XHJcblx0XHRcdCRkcm9wZG93bi5vbignbW91c2VlbnRlcicsICdbZGF0YS1zZWxlY3RhYmxlXScsIGZ1bmN0aW9uKCkgeyByZXR1cm4gc2VsZi5vbk9wdGlvbkhvdmVyLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7IH0pO1xyXG5cdFx0XHQkZHJvcGRvd24ub24oJ21vdXNlZG93biBjbGljaycsICdbZGF0YS1zZWxlY3RhYmxlXScsIGZ1bmN0aW9uKCkgeyByZXR1cm4gc2VsZi5vbk9wdGlvblNlbGVjdC5hcHBseShzZWxmLCBhcmd1bWVudHMpOyB9KTtcclxuXHRcdFx0d2F0Y2hDaGlsZEV2ZW50KCRjb250cm9sLCAnbW91c2Vkb3duJywgJyo6bm90KGlucHV0KScsIGZ1bmN0aW9uKCkgeyByZXR1cm4gc2VsZi5vbkl0ZW1TZWxlY3QuYXBwbHkoc2VsZiwgYXJndW1lbnRzKTsgfSk7XHJcblx0XHRcdGF1dG9Hcm93KCRjb250cm9sX2lucHV0KTtcclxuXHRcclxuXHRcdFx0JGNvbnRyb2wub24oe1xyXG5cdFx0XHRcdG1vdXNlZG93biA6IGZ1bmN0aW9uKCkgeyByZXR1cm4gc2VsZi5vbk1vdXNlRG93bi5hcHBseShzZWxmLCBhcmd1bWVudHMpOyB9LFxyXG5cdFx0XHRcdGNsaWNrICAgICA6IGZ1bmN0aW9uKCkgeyByZXR1cm4gc2VsZi5vbkNsaWNrLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7IH1cclxuXHRcdFx0fSk7XHJcblx0XHJcblx0XHRcdCRjb250cm9sX2lucHV0Lm9uKHtcclxuXHRcdFx0XHRtb3VzZWRvd24gOiBmdW5jdGlvbihlKSB7IGUuc3RvcFByb3BhZ2F0aW9uKCk7IH0sXHJcblx0XHRcdFx0a2V5ZG93biAgIDogZnVuY3Rpb24oKSB7IHJldHVybiBzZWxmLm9uS2V5RG93bi5hcHBseShzZWxmLCBhcmd1bWVudHMpOyB9LFxyXG5cdFx0XHRcdGtleXVwICAgICA6IGZ1bmN0aW9uKCkgeyByZXR1cm4gc2VsZi5vbktleVVwLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7IH0sXHJcblx0XHRcdFx0a2V5cHJlc3MgIDogZnVuY3Rpb24oKSB7IHJldHVybiBzZWxmLm9uS2V5UHJlc3MuYXBwbHkoc2VsZiwgYXJndW1lbnRzKTsgfSxcclxuXHRcdFx0XHRyZXNpemUgICAgOiBmdW5jdGlvbigpIHsgc2VsZi5wb3NpdGlvbkRyb3Bkb3duLmFwcGx5KHNlbGYsIFtdKTsgfSxcclxuXHRcdFx0XHRibHVyICAgICAgOiBmdW5jdGlvbigpIHsgcmV0dXJuIHNlbGYub25CbHVyLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7IH0sXHJcblx0XHRcdFx0Zm9jdXMgICAgIDogZnVuY3Rpb24oKSB7IHNlbGYuaWdub3JlQmx1ciA9IGZhbHNlOyByZXR1cm4gc2VsZi5vbkZvY3VzLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7IH0sXHJcblx0XHRcdFx0cGFzdGUgICAgIDogZnVuY3Rpb24oKSB7IHJldHVybiBzZWxmLm9uUGFzdGUuYXBwbHkoc2VsZiwgYXJndW1lbnRzKTsgfVxyXG5cdFx0XHR9KTtcclxuXHRcclxuXHRcdFx0JGRvY3VtZW50Lm9uKCdrZXlkb3duJyArIGV2ZW50TlMsIGZ1bmN0aW9uKGUpIHtcclxuXHRcdFx0XHRzZWxmLmlzQ21kRG93biA9IGVbSVNfTUFDID8gJ21ldGFLZXknIDogJ2N0cmxLZXknXTtcclxuXHRcdFx0XHRzZWxmLmlzQ3RybERvd24gPSBlW0lTX01BQyA/ICdhbHRLZXknIDogJ2N0cmxLZXknXTtcclxuXHRcdFx0XHRzZWxmLmlzU2hpZnREb3duID0gZS5zaGlmdEtleTtcclxuXHRcdFx0fSk7XHJcblx0XHJcblx0XHRcdCRkb2N1bWVudC5vbigna2V5dXAnICsgZXZlbnROUywgZnVuY3Rpb24oZSkge1xyXG5cdFx0XHRcdGlmIChlLmtleUNvZGUgPT09IEtFWV9DVFJMKSBzZWxmLmlzQ3RybERvd24gPSBmYWxzZTtcclxuXHRcdFx0XHRpZiAoZS5rZXlDb2RlID09PSBLRVlfU0hJRlQpIHNlbGYuaXNTaGlmdERvd24gPSBmYWxzZTtcclxuXHRcdFx0XHRpZiAoZS5rZXlDb2RlID09PSBLRVlfQ01EKSBzZWxmLmlzQ21kRG93biA9IGZhbHNlO1xyXG5cdFx0XHR9KTtcclxuXHRcclxuXHRcdFx0JGRvY3VtZW50Lm9uKCdtb3VzZWRvd24nICsgZXZlbnROUywgZnVuY3Rpb24oZSkge1xyXG5cdFx0XHRcdGlmIChzZWxmLmlzRm9jdXNlZCkge1xyXG5cdFx0XHRcdFx0Ly8gcHJldmVudCBldmVudHMgb24gdGhlIGRyb3Bkb3duIHNjcm9sbGJhciBmcm9tIGNhdXNpbmcgdGhlIGNvbnRyb2wgdG8gYmx1clxyXG5cdFx0XHRcdFx0aWYgKGUudGFyZ2V0ID09PSBzZWxmLiRkcm9wZG93blswXSB8fCBlLnRhcmdldC5wYXJlbnROb2RlID09PSBzZWxmLiRkcm9wZG93blswXSkge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHQvLyBibHVyIG9uIGNsaWNrIG91dHNpZGVcclxuXHRcdFx0XHRcdGlmICghc2VsZi4kY29udHJvbC5oYXMoZS50YXJnZXQpLmxlbmd0aCAmJiBlLnRhcmdldCAhPT0gc2VsZi4kY29udHJvbFswXSkge1xyXG5cdFx0XHRcdFx0XHRzZWxmLmJsdXIoZS50YXJnZXQpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHJcblx0XHRcdCR3aW5kb3cub24oWydzY3JvbGwnICsgZXZlbnROUywgJ3Jlc2l6ZScgKyBldmVudE5TXS5qb2luKCcgJyksIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGlmIChzZWxmLmlzT3Blbikge1xyXG5cdFx0XHRcdFx0c2VsZi5wb3NpdGlvbkRyb3Bkb3duLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0JHdpbmRvdy5vbignbW91c2Vtb3ZlJyArIGV2ZW50TlMsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdHNlbGYuaWdub3JlSG92ZXIgPSBmYWxzZTtcclxuXHRcdFx0fSk7XHJcblx0XHJcblx0XHRcdC8vIHN0b3JlIG9yaWdpbmFsIGNoaWxkcmVuIGFuZCB0YWIgaW5kZXggc28gdGhhdCB0aGV5IGNhbiBiZVxyXG5cdFx0XHQvLyByZXN0b3JlZCB3aGVuIHRoZSBkZXN0cm95KCkgbWV0aG9kIGlzIGNhbGxlZC5cclxuXHRcdFx0dGhpcy5yZXZlcnRTZXR0aW5ncyA9IHtcclxuXHRcdFx0XHQkY2hpbGRyZW4gOiAkaW5wdXQuY2hpbGRyZW4oKS5kZXRhY2goKSxcclxuXHRcdFx0XHR0YWJpbmRleCAgOiAkaW5wdXQuYXR0cigndGFiaW5kZXgnKVxyXG5cdFx0XHR9O1xyXG5cdFxyXG5cdFx0XHQkaW5wdXQuYXR0cigndGFiaW5kZXgnLCAtMSkuaGlkZSgpLmFmdGVyKHNlbGYuJHdyYXBwZXIpO1xyXG5cdFxyXG5cdFx0XHRpZiAoJC5pc0FycmF5KHNldHRpbmdzLml0ZW1zKSkge1xyXG5cdFx0XHRcdHNlbGYuc2V0VmFsdWUoc2V0dGluZ3MuaXRlbXMpO1xyXG5cdFx0XHRcdGRlbGV0ZSBzZXR0aW5ncy5pdGVtcztcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQvLyBmZWF0dXJlIGRldGVjdCBmb3IgdGhlIHZhbGlkYXRpb24gQVBJXHJcblx0XHRcdGlmIChTVVBQT1JUU19WQUxJRElUWV9BUEkpIHtcclxuXHRcdFx0XHQkaW5wdXQub24oJ2ludmFsaWQnICsgZXZlbnROUywgZnVuY3Rpb24oZSkge1xyXG5cdFx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdFx0c2VsZi5pc0ludmFsaWQgPSB0cnVlO1xyXG5cdFx0XHRcdFx0c2VsZi5yZWZyZXNoU3RhdGUoKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRzZWxmLnVwZGF0ZU9yaWdpbmFsSW5wdXQoKTtcclxuXHRcdFx0c2VsZi5yZWZyZXNoSXRlbXMoKTtcclxuXHRcdFx0c2VsZi5yZWZyZXNoU3RhdGUoKTtcclxuXHRcdFx0c2VsZi51cGRhdGVQbGFjZWhvbGRlcigpO1xyXG5cdFx0XHRzZWxmLmlzU2V0dXAgPSB0cnVlO1xyXG5cdFxyXG5cdFx0XHRpZiAoJGlucHV0LmlzKCc6ZGlzYWJsZWQnKSkge1xyXG5cdFx0XHRcdHNlbGYuZGlzYWJsZSgpO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdHNlbGYub24oJ2NoYW5nZScsIHRoaXMub25DaGFuZ2UpO1xyXG5cdFxyXG5cdFx0XHQkaW5wdXQuZGF0YSgnc2VsZWN0aXplJywgc2VsZik7XHJcblx0XHRcdCRpbnB1dC5hZGRDbGFzcygnc2VsZWN0aXplZCcpO1xyXG5cdFx0XHRzZWxmLnRyaWdnZXIoJ2luaXRpYWxpemUnKTtcclxuXHRcclxuXHRcdFx0Ly8gcHJlbG9hZCBvcHRpb25zXHJcblx0XHRcdGlmIChzZXR0aW5ncy5wcmVsb2FkID09PSB0cnVlKSB7XHJcblx0XHRcdFx0c2VsZi5vblNlYXJjaENoYW5nZSgnJyk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFNldHMgdXAgZGVmYXVsdCByZW5kZXJpbmcgZnVuY3Rpb25zLlxyXG5cdFx0ICovXHJcblx0XHRzZXR1cFRlbXBsYXRlczogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0dmFyIGZpZWxkX2xhYmVsID0gc2VsZi5zZXR0aW5ncy5sYWJlbEZpZWxkO1xyXG5cdFx0XHR2YXIgZmllbGRfb3B0Z3JvdXAgPSBzZWxmLnNldHRpbmdzLm9wdGdyb3VwTGFiZWxGaWVsZDtcclxuXHRcclxuXHRcdFx0dmFyIHRlbXBsYXRlcyA9IHtcclxuXHRcdFx0XHQnb3B0Z3JvdXAnOiBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gJzxkaXYgY2xhc3M9XCJvcHRncm91cFwiPicgKyBkYXRhLmh0bWwgKyAnPC9kaXY+JztcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdCdvcHRncm91cF9oZWFkZXInOiBmdW5jdGlvbihkYXRhLCBlc2NhcGUpIHtcclxuXHRcdFx0XHRcdHJldHVybiAnPGRpdiBjbGFzcz1cIm9wdGdyb3VwLWhlYWRlclwiPicgKyBlc2NhcGUoZGF0YVtmaWVsZF9vcHRncm91cF0pICsgJzwvZGl2Pic7XHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHQnb3B0aW9uJzogZnVuY3Rpb24oZGF0YSwgZXNjYXBlKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gJzxkaXYgY2xhc3M9XCJvcHRpb25cIj4nICsgZXNjYXBlKGRhdGFbZmllbGRfbGFiZWxdKSArICc8L2Rpdj4nO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0J2l0ZW0nOiBmdW5jdGlvbihkYXRhLCBlc2NhcGUpIHtcclxuXHRcdFx0XHRcdHJldHVybiAnPGRpdiBjbGFzcz1cIml0ZW1cIj4nICsgZXNjYXBlKGRhdGFbZmllbGRfbGFiZWxdKSArICc8L2Rpdj4nO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0J29wdGlvbl9jcmVhdGUnOiBmdW5jdGlvbihkYXRhLCBlc2NhcGUpIHtcclxuXHRcdFx0XHRcdHJldHVybiAnPGRpdiBjbGFzcz1cImNyZWF0ZVwiPkFkZCA8c3Ryb25nPicgKyBlc2NhcGUoZGF0YS5pbnB1dCkgKyAnPC9zdHJvbmc+JmhlbGxpcDs8L2Rpdj4nO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHRcclxuXHRcdFx0c2VsZi5zZXR0aW5ncy5yZW5kZXIgPSAkLmV4dGVuZCh7fSwgdGVtcGxhdGVzLCBzZWxmLnNldHRpbmdzLnJlbmRlcik7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBNYXBzIGZpcmVkIGV2ZW50cyB0byBjYWxsYmFja3MgcHJvdmlkZWRcclxuXHRcdCAqIGluIHRoZSBzZXR0aW5ncyB1c2VkIHdoZW4gY3JlYXRpbmcgdGhlIGNvbnRyb2wuXHJcblx0XHQgKi9cclxuXHRcdHNldHVwQ2FsbGJhY2tzOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIGtleSwgZm4sIGNhbGxiYWNrcyA9IHtcclxuXHRcdFx0XHQnaW5pdGlhbGl6ZScgICAgICA6ICdvbkluaXRpYWxpemUnLFxyXG5cdFx0XHRcdCdjaGFuZ2UnICAgICAgICAgIDogJ29uQ2hhbmdlJyxcclxuXHRcdFx0XHQnaXRlbV9hZGQnICAgICAgICA6ICdvbkl0ZW1BZGQnLFxyXG5cdFx0XHRcdCdpdGVtX3JlbW92ZScgICAgIDogJ29uSXRlbVJlbW92ZScsXHJcblx0XHRcdFx0J2NsZWFyJyAgICAgICAgICAgOiAnb25DbGVhcicsXHJcblx0XHRcdFx0J29wdGlvbl9hZGQnICAgICAgOiAnb25PcHRpb25BZGQnLFxyXG5cdFx0XHRcdCdvcHRpb25fcmVtb3ZlJyAgIDogJ29uT3B0aW9uUmVtb3ZlJyxcclxuXHRcdFx0XHQnb3B0aW9uX2NsZWFyJyAgICA6ICdvbk9wdGlvbkNsZWFyJyxcclxuXHRcdFx0XHQnb3B0Z3JvdXBfYWRkJyAgICA6ICdvbk9wdGlvbkdyb3VwQWRkJyxcclxuXHRcdFx0XHQnb3B0Z3JvdXBfcmVtb3ZlJyA6ICdvbk9wdGlvbkdyb3VwUmVtb3ZlJyxcclxuXHRcdFx0XHQnb3B0Z3JvdXBfY2xlYXInICA6ICdvbk9wdGlvbkdyb3VwQ2xlYXInLFxyXG5cdFx0XHRcdCdkcm9wZG93bl9vcGVuJyAgIDogJ29uRHJvcGRvd25PcGVuJyxcclxuXHRcdFx0XHQnZHJvcGRvd25fY2xvc2UnICA6ICdvbkRyb3Bkb3duQ2xvc2UnLFxyXG5cdFx0XHRcdCd0eXBlJyAgICAgICAgICAgIDogJ29uVHlwZScsXHJcblx0XHRcdFx0J2xvYWQnICAgICAgICAgICAgOiAnb25Mb2FkJyxcclxuXHRcdFx0XHQnZm9jdXMnICAgICAgICAgICA6ICdvbkZvY3VzJyxcclxuXHRcdFx0XHQnYmx1cicgICAgICAgICAgICA6ICdvbkJsdXInXHJcblx0XHRcdH07XHJcblx0XHJcblx0XHRcdGZvciAoa2V5IGluIGNhbGxiYWNrcykge1xyXG5cdFx0XHRcdGlmIChjYWxsYmFja3MuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG5cdFx0XHRcdFx0Zm4gPSB0aGlzLnNldHRpbmdzW2NhbGxiYWNrc1trZXldXTtcclxuXHRcdFx0XHRcdGlmIChmbikgdGhpcy5vbihrZXksIGZuKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFRyaWdnZXJlZCB3aGVuIHRoZSBtYWluIGNvbnRyb2wgZWxlbWVudFxyXG5cdFx0ICogaGFzIGEgY2xpY2sgZXZlbnQuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGVcclxuXHRcdCAqIEByZXR1cm4ge2Jvb2xlYW59XHJcblx0XHQgKi9cclxuXHRcdG9uQ2xpY2s6IGZ1bmN0aW9uKGUpIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdFx0XHQvLyBuZWNlc3NhcnkgZm9yIG1vYmlsZSB3ZWJraXQgZGV2aWNlcyAobWFudWFsIGZvY3VzIHRyaWdnZXJpbmdcclxuXHRcdFx0Ly8gaXMgaWdub3JlZCB1bmxlc3MgaW52b2tlZCB3aXRoaW4gYSBjbGljayBldmVudClcclxuXHRcdFx0aWYgKCFzZWxmLmlzRm9jdXNlZCkge1xyXG5cdFx0XHRcdHNlbGYuZm9jdXMoKTtcclxuXHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFRyaWdnZXJlZCB3aGVuIHRoZSBtYWluIGNvbnRyb2wgZWxlbWVudFxyXG5cdFx0ICogaGFzIGEgbW91c2UgZG93biBldmVudC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZVxyXG5cdFx0ICogQHJldHVybiB7Ym9vbGVhbn1cclxuXHRcdCAqL1xyXG5cdFx0b25Nb3VzZURvd246IGZ1bmN0aW9uKGUpIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHR2YXIgZGVmYXVsdFByZXZlbnRlZCA9IGUuaXNEZWZhdWx0UHJldmVudGVkKCk7XHJcblx0XHRcdHZhciAkdGFyZ2V0ID0gJChlLnRhcmdldCk7XHJcblx0XHJcblx0XHRcdGlmIChzZWxmLmlzRm9jdXNlZCkge1xyXG5cdFx0XHRcdC8vIHJldGFpbiBmb2N1cyBieSBwcmV2ZW50aW5nIG5hdGl2ZSBoYW5kbGluZy4gaWYgdGhlXHJcblx0XHRcdFx0Ly8gZXZlbnQgdGFyZ2V0IGlzIHRoZSBpbnB1dCBpdCBzaG91bGQgbm90IGJlIG1vZGlmaWVkLlxyXG5cdFx0XHRcdC8vIG90aGVyd2lzZSwgdGV4dCBzZWxlY3Rpb24gd2l0aGluIHRoZSBpbnB1dCB3b24ndCB3b3JrLlxyXG5cdFx0XHRcdGlmIChlLnRhcmdldCAhPT0gc2VsZi4kY29udHJvbF9pbnB1dFswXSkge1xyXG5cdFx0XHRcdFx0aWYgKHNlbGYuc2V0dGluZ3MubW9kZSA9PT0gJ3NpbmdsZScpIHtcclxuXHRcdFx0XHRcdFx0Ly8gdG9nZ2xlIGRyb3Bkb3duXHJcblx0XHRcdFx0XHRcdHNlbGYuaXNPcGVuID8gc2VsZi5jbG9zZSgpIDogc2VsZi5vcGVuKCk7XHJcblx0XHRcdFx0XHR9IGVsc2UgaWYgKCFkZWZhdWx0UHJldmVudGVkKSB7XHJcblx0XHRcdFx0XHRcdHNlbGYuc2V0QWN0aXZlSXRlbShudWxsKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0Ly8gZ2l2ZSBjb250cm9sIGZvY3VzXHJcblx0XHRcdFx0aWYgKCFkZWZhdWx0UHJldmVudGVkKSB7XHJcblx0XHRcdFx0XHR3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdFx0c2VsZi5mb2N1cygpO1xyXG5cdFx0XHRcdFx0fSwgMCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBUcmlnZ2VyZWQgd2hlbiB0aGUgdmFsdWUgb2YgdGhlIGNvbnRyb2wgaGFzIGJlZW4gY2hhbmdlZC5cclxuXHRcdCAqIFRoaXMgc2hvdWxkIHByb3BhZ2F0ZSB0aGUgZXZlbnQgdG8gdGhlIG9yaWdpbmFsIERPTVxyXG5cdFx0ICogaW5wdXQgLyBzZWxlY3QgZWxlbWVudC5cclxuXHRcdCAqL1xyXG5cdFx0b25DaGFuZ2U6IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR0aGlzLiRpbnB1dC50cmlnZ2VyKCdjaGFuZ2UnKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFRyaWdnZXJlZCBvbiA8aW5wdXQ+IHBhc3RlLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBlXHJcblx0XHQgKiBAcmV0dXJucyB7Ym9vbGVhbn1cclxuXHRcdCAqL1xyXG5cdFx0b25QYXN0ZTogZnVuY3Rpb24oZSkge1xyXG5cdFx0XHR2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHRcdGlmIChzZWxmLmlzRnVsbCgpIHx8IHNlbGYuaXNJbnB1dEhpZGRlbiB8fCBzZWxmLmlzTG9ja2VkKSB7XHJcblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdC8vIElmIGEgcmVnZXggb3Igc3RyaW5nIGlzIGluY2x1ZGVkLCB0aGlzIHdpbGwgc3BsaXQgdGhlIHBhc3RlZFxyXG5cdFx0XHRcdC8vIGlucHV0IGFuZCBjcmVhdGUgSXRlbXMgZm9yIGVhY2ggc2VwYXJhdGUgdmFsdWVcclxuXHRcdFx0XHRpZiAoc2VsZi5zZXR0aW5ncy5zcGxpdE9uKSB7XHJcblx0XHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHR2YXIgc3BsaXRJbnB1dCA9ICQudHJpbShzZWxmLiRjb250cm9sX2lucHV0LnZhbCgpIHx8ICcnKS5zcGxpdChzZWxmLnNldHRpbmdzLnNwbGl0T24pO1xyXG5cdFx0XHRcdFx0XHRmb3IgKHZhciBpID0gMCwgbiA9IHNwbGl0SW5wdXQubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdFx0XHRcdFx0c2VsZi5jcmVhdGVJdGVtKHNwbGl0SW5wdXRbaV0pO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9LCAwKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFRyaWdnZXJlZCBvbiA8aW5wdXQ+IGtleXByZXNzLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBlXHJcblx0XHQgKiBAcmV0dXJucyB7Ym9vbGVhbn1cclxuXHRcdCAqL1xyXG5cdFx0b25LZXlQcmVzczogZnVuY3Rpb24oZSkge1xyXG5cdFx0XHRpZiAodGhpcy5pc0xvY2tlZCkgcmV0dXJuIGUgJiYgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHR2YXIgY2hhcmFjdGVyID0gU3RyaW5nLmZyb21DaGFyQ29kZShlLmtleUNvZGUgfHwgZS53aGljaCk7XHJcblx0XHRcdGlmICh0aGlzLnNldHRpbmdzLmNyZWF0ZSAmJiB0aGlzLnNldHRpbmdzLm1vZGUgPT09ICdtdWx0aScgJiYgY2hhcmFjdGVyID09PSB0aGlzLnNldHRpbmdzLmRlbGltaXRlcikge1xyXG5cdFx0XHRcdHRoaXMuY3JlYXRlSXRlbSgpO1xyXG5cdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFRyaWdnZXJlZCBvbiA8aW5wdXQ+IGtleWRvd24uXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGVcclxuXHRcdCAqIEByZXR1cm5zIHtib29sZWFufVxyXG5cdFx0ICovXHJcblx0XHRvbktleURvd246IGZ1bmN0aW9uKGUpIHtcclxuXHRcdFx0dmFyIGlzSW5wdXQgPSBlLnRhcmdldCA9PT0gdGhpcy4kY29udHJvbF9pbnB1dFswXTtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdFx0XHRpZiAoc2VsZi5pc0xvY2tlZCkge1xyXG5cdFx0XHRcdGlmIChlLmtleUNvZGUgIT09IEtFWV9UQUIpIHtcclxuXHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdHN3aXRjaCAoZS5rZXlDb2RlKSB7XHJcblx0XHRcdFx0Y2FzZSBLRVlfQTpcclxuXHRcdFx0XHRcdGlmIChzZWxmLmlzQ21kRG93bikge1xyXG5cdFx0XHRcdFx0XHRzZWxmLnNlbGVjdEFsbCgpO1xyXG5cdFx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlIEtFWV9FU0M6XHJcblx0XHRcdFx0XHRpZiAoc2VsZi5pc09wZW4pIHtcclxuXHRcdFx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cdFx0XHRcdFx0XHRzZWxmLmNsb3NlKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0Y2FzZSBLRVlfTjpcclxuXHRcdFx0XHRcdGlmICghZS5jdHJsS2V5IHx8IGUuYWx0S2V5KSBicmVhaztcclxuXHRcdFx0XHRjYXNlIEtFWV9ET1dOOlxyXG5cdFx0XHRcdFx0aWYgKCFzZWxmLmlzT3BlbiAmJiBzZWxmLmhhc09wdGlvbnMpIHtcclxuXHRcdFx0XHRcdFx0c2VsZi5vcGVuKCk7XHJcblx0XHRcdFx0XHR9IGVsc2UgaWYgKHNlbGYuJGFjdGl2ZU9wdGlvbikge1xyXG5cdFx0XHRcdFx0XHRzZWxmLmlnbm9yZUhvdmVyID0gdHJ1ZTtcclxuXHRcdFx0XHRcdFx0dmFyICRuZXh0ID0gc2VsZi5nZXRBZGphY2VudE9wdGlvbihzZWxmLiRhY3RpdmVPcHRpb24sIDEpO1xyXG5cdFx0XHRcdFx0XHRpZiAoJG5leHQubGVuZ3RoKSBzZWxmLnNldEFjdGl2ZU9wdGlvbigkbmV4dCwgdHJ1ZSwgdHJ1ZSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0Y2FzZSBLRVlfUDpcclxuXHRcdFx0XHRcdGlmICghZS5jdHJsS2V5IHx8IGUuYWx0S2V5KSBicmVhaztcclxuXHRcdFx0XHRjYXNlIEtFWV9VUDpcclxuXHRcdFx0XHRcdGlmIChzZWxmLiRhY3RpdmVPcHRpb24pIHtcclxuXHRcdFx0XHRcdFx0c2VsZi5pZ25vcmVIb3ZlciA9IHRydWU7XHJcblx0XHRcdFx0XHRcdHZhciAkcHJldiA9IHNlbGYuZ2V0QWRqYWNlbnRPcHRpb24oc2VsZi4kYWN0aXZlT3B0aW9uLCAtMSk7XHJcblx0XHRcdFx0XHRcdGlmICgkcHJldi5sZW5ndGgpIHNlbGYuc2V0QWN0aXZlT3B0aW9uKCRwcmV2LCB0cnVlLCB0cnVlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHRjYXNlIEtFWV9SRVRVUk46XHJcblx0XHRcdFx0XHRpZiAoc2VsZi5pc09wZW4gJiYgc2VsZi4kYWN0aXZlT3B0aW9uKSB7XHJcblx0XHRcdFx0XHRcdHNlbGYub25PcHRpb25TZWxlY3Qoe2N1cnJlbnRUYXJnZXQ6IHNlbGYuJGFjdGl2ZU9wdGlvbn0pO1xyXG5cdFx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0Y2FzZSBLRVlfTEVGVDpcclxuXHRcdFx0XHRcdHNlbGYuYWR2YW5jZVNlbGVjdGlvbigtMSwgZSk7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0Y2FzZSBLRVlfUklHSFQ6XHJcblx0XHRcdFx0XHRzZWxmLmFkdmFuY2VTZWxlY3Rpb24oMSwgZSk7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0Y2FzZSBLRVlfVEFCOlxyXG5cdFx0XHRcdFx0aWYgKHNlbGYuc2V0dGluZ3Muc2VsZWN0T25UYWIgJiYgc2VsZi5pc09wZW4gJiYgc2VsZi4kYWN0aXZlT3B0aW9uKSB7XHJcblx0XHRcdFx0XHRcdHNlbGYub25PcHRpb25TZWxlY3Qoe2N1cnJlbnRUYXJnZXQ6IHNlbGYuJGFjdGl2ZU9wdGlvbn0pO1xyXG5cdFxyXG5cdFx0XHRcdFx0XHQvLyBEZWZhdWx0IGJlaGF2aW91ciBpcyB0byBqdW1wIHRvIHRoZSBuZXh0IGZpZWxkLCB3ZSBvbmx5IHdhbnQgdGhpc1xyXG5cdFx0XHRcdFx0XHQvLyBpZiB0aGUgY3VycmVudCBmaWVsZCBkb2Vzbid0IGFjY2VwdCBhbnkgbW9yZSBlbnRyaWVzXHJcblx0XHRcdFx0XHRcdGlmICghc2VsZi5pc0Z1bGwoKSkge1xyXG5cdFx0XHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKHNlbGYuc2V0dGluZ3MuY3JlYXRlICYmIHNlbGYuY3JlYXRlSXRlbSgpKSB7XHJcblx0XHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHRjYXNlIEtFWV9CQUNLU1BBQ0U6XHJcblx0XHRcdFx0Y2FzZSBLRVlfREVMRVRFOlxyXG5cdFx0XHRcdFx0c2VsZi5kZWxldGVTZWxlY3Rpb24oZSk7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0aWYgKChzZWxmLmlzRnVsbCgpIHx8IHNlbGYuaXNJbnB1dEhpZGRlbikgJiYgIShJU19NQUMgPyBlLm1ldGFLZXkgOiBlLmN0cmxLZXkpKSB7XHJcblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogVHJpZ2dlcmVkIG9uIDxpbnB1dD4ga2V5dXAuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGVcclxuXHRcdCAqIEByZXR1cm5zIHtib29sZWFufVxyXG5cdFx0ICovXHJcblx0XHRvbktleVVwOiBmdW5jdGlvbihlKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcclxuXHRcdFx0aWYgKHNlbGYuaXNMb2NrZWQpIHJldHVybiBlICYmIGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0dmFyIHZhbHVlID0gc2VsZi4kY29udHJvbF9pbnB1dC52YWwoKSB8fCAnJztcclxuXHRcdFx0aWYgKHNlbGYubGFzdFZhbHVlICE9PSB2YWx1ZSkge1xyXG5cdFx0XHRcdHNlbGYubGFzdFZhbHVlID0gdmFsdWU7XHJcblx0XHRcdFx0c2VsZi5vblNlYXJjaENoYW5nZSh2YWx1ZSk7XHJcblx0XHRcdFx0c2VsZi5yZWZyZXNoT3B0aW9ucygpO1xyXG5cdFx0XHRcdHNlbGYudHJpZ2dlcigndHlwZScsIHZhbHVlKTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogSW52b2tlcyB0aGUgdXNlci1wcm92aWRlIG9wdGlvbiBwcm92aWRlciAvIGxvYWRlci5cclxuXHRcdCAqXHJcblx0XHQgKiBOb3RlOiB0aGlzIGZ1bmN0aW9uIGlzIGRlYm91bmNlZCBpbiB0aGUgU2VsZWN0aXplXHJcblx0XHQgKiBjb25zdHJ1Y3RvciAoYnkgYHNldHRpbmdzLmxvYWREZWxheWAgbWlsbGlzZWNvbmRzKVxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG5cdFx0ICovXHJcblx0XHRvblNlYXJjaENoYW5nZTogZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHR2YXIgZm4gPSBzZWxmLnNldHRpbmdzLmxvYWQ7XHJcblx0XHRcdGlmICghZm4pIHJldHVybjtcclxuXHRcdFx0aWYgKHNlbGYubG9hZGVkU2VhcmNoZXMuaGFzT3duUHJvcGVydHkodmFsdWUpKSByZXR1cm47XHJcblx0XHRcdHNlbGYubG9hZGVkU2VhcmNoZXNbdmFsdWVdID0gdHJ1ZTtcclxuXHRcdFx0c2VsZi5sb2FkKGZ1bmN0aW9uKGNhbGxiYWNrKSB7XHJcblx0XHRcdFx0Zm4uYXBwbHkoc2VsZiwgW3ZhbHVlLCBjYWxsYmFja10pO1xyXG5cdFx0XHR9KTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFRyaWdnZXJlZCBvbiA8aW5wdXQ+IGZvY3VzLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBlIChvcHRpb25hbClcclxuXHRcdCAqIEByZXR1cm5zIHtib29sZWFufVxyXG5cdFx0ICovXHJcblx0XHRvbkZvY3VzOiBmdW5jdGlvbihlKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0dmFyIHdhc0ZvY3VzZWQgPSBzZWxmLmlzRm9jdXNlZDtcclxuXHRcclxuXHRcdFx0aWYgKHNlbGYuaXNEaXNhYmxlZCkge1xyXG5cdFx0XHRcdHNlbGYuYmx1cigpO1xyXG5cdFx0XHRcdGUgJiYgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRpZiAoc2VsZi5pZ25vcmVGb2N1cykgcmV0dXJuO1xyXG5cdFx0XHRzZWxmLmlzRm9jdXNlZCA9IHRydWU7XHJcblx0XHRcdGlmIChzZWxmLnNldHRpbmdzLnByZWxvYWQgPT09ICdmb2N1cycpIHNlbGYub25TZWFyY2hDaGFuZ2UoJycpO1xyXG5cdFxyXG5cdFx0XHRpZiAoIXdhc0ZvY3VzZWQpIHNlbGYudHJpZ2dlcignZm9jdXMnKTtcclxuXHRcclxuXHRcdFx0aWYgKCFzZWxmLiRhY3RpdmVJdGVtcy5sZW5ndGgpIHtcclxuXHRcdFx0XHRzZWxmLnNob3dJbnB1dCgpO1xyXG5cdFx0XHRcdHNlbGYuc2V0QWN0aXZlSXRlbShudWxsKTtcclxuXHRcdFx0XHRzZWxmLnJlZnJlc2hPcHRpb25zKCEhc2VsZi5zZXR0aW5ncy5vcGVuT25Gb2N1cyk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0c2VsZi5yZWZyZXNoU3RhdGUoKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFRyaWdnZXJlZCBvbiA8aW5wdXQ+IGJsdXIuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGVcclxuXHRcdCAqIEBwYXJhbSB7RWxlbWVudH0gZGVzdFxyXG5cdFx0ICovXHJcblx0XHRvbkJsdXI6IGZ1bmN0aW9uKGUsIGRlc3QpIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHRpZiAoIXNlbGYuaXNGb2N1c2VkKSByZXR1cm47XHJcblx0XHRcdHNlbGYuaXNGb2N1c2VkID0gZmFsc2U7XHJcblx0XHJcblx0XHRcdGlmIChzZWxmLmlnbm9yZUZvY3VzKSB7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9IGVsc2UgaWYgKCFzZWxmLmlnbm9yZUJsdXIgJiYgZG9jdW1lbnQuYWN0aXZlRWxlbWVudCA9PT0gc2VsZi4kZHJvcGRvd25fY29udGVudFswXSkge1xyXG5cdFx0XHRcdC8vIG5lY2Vzc2FyeSB0byBwcmV2ZW50IElFIGNsb3NpbmcgdGhlIGRyb3Bkb3duIHdoZW4gdGhlIHNjcm9sbGJhciBpcyBjbGlja2VkXHJcblx0XHRcdFx0c2VsZi5pZ25vcmVCbHVyID0gdHJ1ZTtcclxuXHRcdFx0XHRzZWxmLm9uRm9jdXMoZSk7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdHZhciBkZWFjdGl2YXRlID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0c2VsZi5jbG9zZSgpO1xyXG5cdFx0XHRcdHNlbGYuc2V0VGV4dGJveFZhbHVlKCcnKTtcclxuXHRcdFx0XHRzZWxmLnNldEFjdGl2ZUl0ZW0obnVsbCk7XHJcblx0XHRcdFx0c2VsZi5zZXRBY3RpdmVPcHRpb24obnVsbCk7XHJcblx0XHRcdFx0c2VsZi5zZXRDYXJldChzZWxmLml0ZW1zLmxlbmd0aCk7XHJcblx0XHRcdFx0c2VsZi5yZWZyZXNoU3RhdGUoKTtcclxuXHRcclxuXHRcdFx0XHQvLyBJRTExIGJ1ZzogZWxlbWVudCBzdGlsbCBtYXJrZWQgYXMgYWN0aXZlXHJcblx0XHRcdFx0KGRlc3QgfHwgZG9jdW1lbnQuYm9keSkuZm9jdXMoKTtcclxuXHRcclxuXHRcdFx0XHRzZWxmLmlnbm9yZUZvY3VzID0gZmFsc2U7XHJcblx0XHRcdFx0c2VsZi50cmlnZ2VyKCdibHVyJyk7XHJcblx0XHRcdH07XHJcblx0XHJcblx0XHRcdHNlbGYuaWdub3JlRm9jdXMgPSB0cnVlO1xyXG5cdFx0XHRpZiAoc2VsZi5zZXR0aW5ncy5jcmVhdGUgJiYgc2VsZi5zZXR0aW5ncy5jcmVhdGVPbkJsdXIpIHtcclxuXHRcdFx0XHRzZWxmLmNyZWF0ZUl0ZW0obnVsbCwgZmFsc2UsIGRlYWN0aXZhdGUpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGRlYWN0aXZhdGUoKTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogVHJpZ2dlcmVkIHdoZW4gdGhlIHVzZXIgcm9sbHMgb3ZlclxyXG5cdFx0ICogYW4gb3B0aW9uIGluIHRoZSBhdXRvY29tcGxldGUgZHJvcGRvd24gbWVudS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZVxyXG5cdFx0ICogQHJldHVybnMge2Jvb2xlYW59XHJcblx0XHQgKi9cclxuXHRcdG9uT3B0aW9uSG92ZXI6IGZ1bmN0aW9uKGUpIHtcclxuXHRcdFx0aWYgKHRoaXMuaWdub3JlSG92ZXIpIHJldHVybjtcclxuXHRcdFx0dGhpcy5zZXRBY3RpdmVPcHRpb24oZS5jdXJyZW50VGFyZ2V0LCBmYWxzZSk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBUcmlnZ2VyZWQgd2hlbiB0aGUgdXNlciBjbGlja3Mgb24gYW4gb3B0aW9uXHJcblx0XHQgKiBpbiB0aGUgYXV0b2NvbXBsZXRlIGRyb3Bkb3duIG1lbnUuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGVcclxuXHRcdCAqIEByZXR1cm5zIHtib29sZWFufVxyXG5cdFx0ICovXHJcblx0XHRvbk9wdGlvblNlbGVjdDogZnVuY3Rpb24oZSkge1xyXG5cdFx0XHR2YXIgdmFsdWUsICR0YXJnZXQsICRvcHRpb24sIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdFx0XHRpZiAoZS5wcmV2ZW50RGVmYXVsdCkge1xyXG5cdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdCR0YXJnZXQgPSAkKGUuY3VycmVudFRhcmdldCk7XHJcblx0XHRcdGlmICgkdGFyZ2V0Lmhhc0NsYXNzKCdjcmVhdGUnKSkge1xyXG5cdFx0XHRcdHNlbGYuY3JlYXRlSXRlbShudWxsLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdGlmIChzZWxmLnNldHRpbmdzLmNsb3NlQWZ0ZXJTZWxlY3QpIHtcclxuXHRcdFx0XHRcdFx0c2VsZi5jbG9zZSgpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHZhbHVlID0gJHRhcmdldC5hdHRyKCdkYXRhLXZhbHVlJyk7XHJcblx0XHRcdFx0aWYgKHR5cGVvZiB2YWx1ZSAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuXHRcdFx0XHRcdHNlbGYubGFzdFF1ZXJ5ID0gbnVsbDtcclxuXHRcdFx0XHRcdHNlbGYuc2V0VGV4dGJveFZhbHVlKCcnKTtcclxuXHRcdFx0XHRcdHNlbGYuYWRkSXRlbSh2YWx1ZSk7XHJcblx0XHRcdFx0XHRpZiAoc2VsZi5zZXR0aW5ncy5jbG9zZUFmdGVyU2VsZWN0KSB7XHJcblx0XHRcdFx0XHRcdHNlbGYuY2xvc2UoKTtcclxuXHRcdFx0XHRcdH0gZWxzZSBpZiAoIXNlbGYuc2V0dGluZ3MuaGlkZVNlbGVjdGVkICYmIGUudHlwZSAmJiAvbW91c2UvLnRlc3QoZS50eXBlKSkge1xyXG5cdFx0XHRcdFx0XHRzZWxmLnNldEFjdGl2ZU9wdGlvbihzZWxmLmdldE9wdGlvbih2YWx1ZSkpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogVHJpZ2dlcmVkIHdoZW4gdGhlIHVzZXIgY2xpY2tzIG9uIGFuIGl0ZW1cclxuXHRcdCAqIHRoYXQgaGFzIGJlZW4gc2VsZWN0ZWQuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGVcclxuXHRcdCAqIEByZXR1cm5zIHtib29sZWFufVxyXG5cdFx0ICovXHJcblx0XHRvbkl0ZW1TZWxlY3Q6IGZ1bmN0aW9uKGUpIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdFx0XHRpZiAoc2VsZi5pc0xvY2tlZCkgcmV0dXJuO1xyXG5cdFx0XHRpZiAoc2VsZi5zZXR0aW5ncy5tb2RlID09PSAnbXVsdGknKSB7XHJcblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdHNlbGYuc2V0QWN0aXZlSXRlbShlLmN1cnJlbnRUYXJnZXQsIGUpO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBJbnZva2VzIHRoZSBwcm92aWRlZCBtZXRob2QgdGhhdCBwcm92aWRlc1xyXG5cdFx0ICogcmVzdWx0cyB0byBhIGNhbGxiYWNrLS0td2hpY2ggYXJlIHRoZW4gYWRkZWRcclxuXHRcdCAqIGFzIG9wdGlvbnMgdG8gdGhlIGNvbnRyb2wuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtmdW5jdGlvbn0gZm5cclxuXHRcdCAqL1xyXG5cdFx0bG9hZDogZnVuY3Rpb24oZm4pIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHR2YXIgJHdyYXBwZXIgPSBzZWxmLiR3cmFwcGVyLmFkZENsYXNzKHNlbGYuc2V0dGluZ3MubG9hZGluZ0NsYXNzKTtcclxuXHRcclxuXHRcdFx0c2VsZi5sb2FkaW5nKys7XHJcblx0XHRcdGZuLmFwcGx5KHNlbGYsIFtmdW5jdGlvbihyZXN1bHRzKSB7XHJcblx0XHRcdFx0c2VsZi5sb2FkaW5nID0gTWF0aC5tYXgoc2VsZi5sb2FkaW5nIC0gMSwgMCk7XHJcblx0XHRcdFx0aWYgKHJlc3VsdHMgJiYgcmVzdWx0cy5sZW5ndGgpIHtcclxuXHRcdFx0XHRcdHNlbGYuYWRkT3B0aW9uKHJlc3VsdHMpO1xyXG5cdFx0XHRcdFx0c2VsZi5yZWZyZXNoT3B0aW9ucyhzZWxmLmlzRm9jdXNlZCAmJiAhc2VsZi5pc0lucHV0SGlkZGVuKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKCFzZWxmLmxvYWRpbmcpIHtcclxuXHRcdFx0XHRcdCR3cmFwcGVyLnJlbW92ZUNsYXNzKHNlbGYuc2V0dGluZ3MubG9hZGluZ0NsYXNzKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0c2VsZi50cmlnZ2VyKCdsb2FkJywgcmVzdWx0cyk7XHJcblx0XHRcdH1dKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFNldHMgdGhlIGlucHV0IGZpZWxkIG9mIHRoZSBjb250cm9sIHRvIHRoZSBzcGVjaWZpZWQgdmFsdWUuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlXHJcblx0XHQgKi9cclxuXHRcdHNldFRleHRib3hWYWx1ZTogZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0dmFyICRpbnB1dCA9IHRoaXMuJGNvbnRyb2xfaW5wdXQ7XHJcblx0XHRcdHZhciBjaGFuZ2VkID0gJGlucHV0LnZhbCgpICE9PSB2YWx1ZTtcclxuXHRcdFx0aWYgKGNoYW5nZWQpIHtcclxuXHRcdFx0XHQkaW5wdXQudmFsKHZhbHVlKS50cmlnZ2VySGFuZGxlcigndXBkYXRlJyk7XHJcblx0XHRcdFx0dGhpcy5sYXN0VmFsdWUgPSB2YWx1ZTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogUmV0dXJucyB0aGUgdmFsdWUgb2YgdGhlIGNvbnRyb2wuIElmIG11bHRpcGxlIGl0ZW1zXHJcblx0XHQgKiBjYW4gYmUgc2VsZWN0ZWQgKGUuZy4gPHNlbGVjdCBtdWx0aXBsZT4pLCB0aGlzIHJldHVybnNcclxuXHRcdCAqIGFuIGFycmF5LiBJZiBvbmx5IG9uZSBpdGVtIGNhbiBiZSBzZWxlY3RlZCwgdGhpc1xyXG5cdFx0ICogcmV0dXJucyBhIHN0cmluZy5cclxuXHRcdCAqXHJcblx0XHQgKiBAcmV0dXJucyB7bWl4ZWR9XHJcblx0XHQgKi9cclxuXHRcdGdldFZhbHVlOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKHRoaXMudGFnVHlwZSA9PT0gVEFHX1NFTEVDVCAmJiB0aGlzLiRpbnB1dC5hdHRyKCdtdWx0aXBsZScpKSB7XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMuaXRlbXM7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMuaXRlbXMuam9pbih0aGlzLnNldHRpbmdzLmRlbGltaXRlcik7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFJlc2V0cyB0aGUgc2VsZWN0ZWQgaXRlbXMgdG8gdGhlIGdpdmVuIHZhbHVlLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7bWl4ZWR9IHZhbHVlXHJcblx0XHQgKi9cclxuXHRcdHNldFZhbHVlOiBmdW5jdGlvbih2YWx1ZSwgc2lsZW50KSB7XHJcblx0XHRcdHZhciBldmVudHMgPSBzaWxlbnQgPyBbXSA6IFsnY2hhbmdlJ107XHJcblx0XHJcblx0XHRcdGRlYm91bmNlX2V2ZW50cyh0aGlzLCBldmVudHMsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdHRoaXMuY2xlYXIoc2lsZW50KTtcclxuXHRcdFx0XHR0aGlzLmFkZEl0ZW1zKHZhbHVlLCBzaWxlbnQpO1xyXG5cdFx0XHR9KTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFNldHMgdGhlIHNlbGVjdGVkIGl0ZW0uXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9ICRpdGVtXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZSAob3B0aW9uYWwpXHJcblx0XHQgKi9cclxuXHRcdHNldEFjdGl2ZUl0ZW06IGZ1bmN0aW9uKCRpdGVtLCBlKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0dmFyIGV2ZW50TmFtZTtcclxuXHRcdFx0dmFyIGksIGlkeCwgYmVnaW4sIGVuZCwgaXRlbSwgc3dhcDtcclxuXHRcdFx0dmFyICRsYXN0O1xyXG5cdFxyXG5cdFx0XHRpZiAoc2VsZi5zZXR0aW5ncy5tb2RlID09PSAnc2luZ2xlJykgcmV0dXJuO1xyXG5cdFx0XHQkaXRlbSA9ICQoJGl0ZW0pO1xyXG5cdFxyXG5cdFx0XHQvLyBjbGVhciB0aGUgYWN0aXZlIHNlbGVjdGlvblxyXG5cdFx0XHRpZiAoISRpdGVtLmxlbmd0aCkge1xyXG5cdFx0XHRcdCQoc2VsZi4kYWN0aXZlSXRlbXMpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRzZWxmLiRhY3RpdmVJdGVtcyA9IFtdO1xyXG5cdFx0XHRcdGlmIChzZWxmLmlzRm9jdXNlZCkge1xyXG5cdFx0XHRcdFx0c2VsZi5zaG93SW5wdXQoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIG1vZGlmeSBzZWxlY3Rpb25cclxuXHRcdFx0ZXZlbnROYW1lID0gZSAmJiBlLnR5cGUudG9Mb3dlckNhc2UoKTtcclxuXHRcclxuXHRcdFx0aWYgKGV2ZW50TmFtZSA9PT0gJ21vdXNlZG93bicgJiYgc2VsZi5pc1NoaWZ0RG93biAmJiBzZWxmLiRhY3RpdmVJdGVtcy5sZW5ndGgpIHtcclxuXHRcdFx0XHQkbGFzdCA9IHNlbGYuJGNvbnRyb2wuY2hpbGRyZW4oJy5hY3RpdmU6bGFzdCcpO1xyXG5cdFx0XHRcdGJlZ2luID0gQXJyYXkucHJvdG90eXBlLmluZGV4T2YuYXBwbHkoc2VsZi4kY29udHJvbFswXS5jaGlsZE5vZGVzLCBbJGxhc3RbMF1dKTtcclxuXHRcdFx0XHRlbmQgICA9IEFycmF5LnByb3RvdHlwZS5pbmRleE9mLmFwcGx5KHNlbGYuJGNvbnRyb2xbMF0uY2hpbGROb2RlcywgWyRpdGVtWzBdXSk7XHJcblx0XHRcdFx0aWYgKGJlZ2luID4gZW5kKSB7XHJcblx0XHRcdFx0XHRzd2FwICA9IGJlZ2luO1xyXG5cdFx0XHRcdFx0YmVnaW4gPSBlbmQ7XHJcblx0XHRcdFx0XHRlbmQgICA9IHN3YXA7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGZvciAoaSA9IGJlZ2luOyBpIDw9IGVuZDsgaSsrKSB7XHJcblx0XHRcdFx0XHRpdGVtID0gc2VsZi4kY29udHJvbFswXS5jaGlsZE5vZGVzW2ldO1xyXG5cdFx0XHRcdFx0aWYgKHNlbGYuJGFjdGl2ZUl0ZW1zLmluZGV4T2YoaXRlbSkgPT09IC0xKSB7XHJcblx0XHRcdFx0XHRcdCQoaXRlbSkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0XHRzZWxmLiRhY3RpdmVJdGVtcy5wdXNoKGl0ZW0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdH0gZWxzZSBpZiAoKGV2ZW50TmFtZSA9PT0gJ21vdXNlZG93bicgJiYgc2VsZi5pc0N0cmxEb3duKSB8fCAoZXZlbnROYW1lID09PSAna2V5ZG93bicgJiYgdGhpcy5pc1NoaWZ0RG93bikpIHtcclxuXHRcdFx0XHRpZiAoJGl0ZW0uaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XHJcblx0XHRcdFx0XHRpZHggPSBzZWxmLiRhY3RpdmVJdGVtcy5pbmRleE9mKCRpdGVtWzBdKTtcclxuXHRcdFx0XHRcdHNlbGYuJGFjdGl2ZUl0ZW1zLnNwbGljZShpZHgsIDEpO1xyXG5cdFx0XHRcdFx0JGl0ZW0ucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRzZWxmLiRhY3RpdmVJdGVtcy5wdXNoKCRpdGVtLmFkZENsYXNzKCdhY3RpdmUnKVswXSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCQoc2VsZi4kYWN0aXZlSXRlbXMpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRzZWxmLiRhY3RpdmVJdGVtcyA9IFskaXRlbS5hZGRDbGFzcygnYWN0aXZlJylbMF1dO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIGVuc3VyZSBjb250cm9sIGhhcyBmb2N1c1xyXG5cdFx0XHRzZWxmLmhpZGVJbnB1dCgpO1xyXG5cdFx0XHRpZiAoIXRoaXMuaXNGb2N1c2VkKSB7XHJcblx0XHRcdFx0c2VsZi5mb2N1cygpO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBTZXRzIHRoZSBzZWxlY3RlZCBpdGVtIGluIHRoZSBkcm9wZG93biBtZW51XHJcblx0XHQgKiBvZiBhdmFpbGFibGUgb3B0aW9ucy5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gJG9iamVjdFxyXG5cdFx0ICogQHBhcmFtIHtib29sZWFufSBzY3JvbGxcclxuXHRcdCAqIEBwYXJhbSB7Ym9vbGVhbn0gYW5pbWF0ZVxyXG5cdFx0ICovXHJcblx0XHRzZXRBY3RpdmVPcHRpb246IGZ1bmN0aW9uKCRvcHRpb24sIHNjcm9sbCwgYW5pbWF0ZSkge1xyXG5cdFx0XHR2YXIgaGVpZ2h0X21lbnUsIGhlaWdodF9pdGVtLCB5O1xyXG5cdFx0XHR2YXIgc2Nyb2xsX3RvcCwgc2Nyb2xsX2JvdHRvbTtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdFx0XHRpZiAoc2VsZi4kYWN0aXZlT3B0aW9uKSBzZWxmLiRhY3RpdmVPcHRpb24ucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRzZWxmLiRhY3RpdmVPcHRpb24gPSBudWxsO1xyXG5cdFxyXG5cdFx0XHQkb3B0aW9uID0gJCgkb3B0aW9uKTtcclxuXHRcdFx0aWYgKCEkb3B0aW9uLmxlbmd0aCkgcmV0dXJuO1xyXG5cdFxyXG5cdFx0XHRzZWxmLiRhY3RpdmVPcHRpb24gPSAkb3B0aW9uLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuXHRcclxuXHRcdFx0aWYgKHNjcm9sbCB8fCAhaXNzZXQoc2Nyb2xsKSkge1xyXG5cdFxyXG5cdFx0XHRcdGhlaWdodF9tZW51ICAgPSBzZWxmLiRkcm9wZG93bl9jb250ZW50LmhlaWdodCgpO1xyXG5cdFx0XHRcdGhlaWdodF9pdGVtICAgPSBzZWxmLiRhY3RpdmVPcHRpb24ub3V0ZXJIZWlnaHQodHJ1ZSk7XHJcblx0XHRcdFx0c2Nyb2xsICAgICAgICA9IHNlbGYuJGRyb3Bkb3duX2NvbnRlbnQuc2Nyb2xsVG9wKCkgfHwgMDtcclxuXHRcdFx0XHR5ICAgICAgICAgICAgID0gc2VsZi4kYWN0aXZlT3B0aW9uLm9mZnNldCgpLnRvcCAtIHNlbGYuJGRyb3Bkb3duX2NvbnRlbnQub2Zmc2V0KCkudG9wICsgc2Nyb2xsO1xyXG5cdFx0XHRcdHNjcm9sbF90b3AgICAgPSB5O1xyXG5cdFx0XHRcdHNjcm9sbF9ib3R0b20gPSB5IC0gaGVpZ2h0X21lbnUgKyBoZWlnaHRfaXRlbTtcclxuXHRcclxuXHRcdFx0XHRpZiAoeSArIGhlaWdodF9pdGVtID4gaGVpZ2h0X21lbnUgKyBzY3JvbGwpIHtcclxuXHRcdFx0XHRcdHNlbGYuJGRyb3Bkb3duX2NvbnRlbnQuc3RvcCgpLmFuaW1hdGUoe3Njcm9sbFRvcDogc2Nyb2xsX2JvdHRvbX0sIGFuaW1hdGUgPyBzZWxmLnNldHRpbmdzLnNjcm9sbER1cmF0aW9uIDogMCk7XHJcblx0XHRcdFx0fSBlbHNlIGlmICh5IDwgc2Nyb2xsKSB7XHJcblx0XHRcdFx0XHRzZWxmLiRkcm9wZG93bl9jb250ZW50LnN0b3AoKS5hbmltYXRlKHtzY3JvbGxUb3A6IHNjcm9sbF90b3B9LCBhbmltYXRlID8gc2VsZi5zZXR0aW5ncy5zY3JvbGxEdXJhdGlvbiA6IDApO1xyXG5cdFx0XHRcdH1cclxuXHRcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogU2VsZWN0cyBhbGwgaXRlbXMgKENUUkwgKyBBKS5cclxuXHRcdCAqL1xyXG5cdFx0c2VsZWN0QWxsOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHRpZiAoc2VsZi5zZXR0aW5ncy5tb2RlID09PSAnc2luZ2xlJykgcmV0dXJuO1xyXG5cdFxyXG5cdFx0XHRzZWxmLiRhY3RpdmVJdGVtcyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5hcHBseShzZWxmLiRjb250cm9sLmNoaWxkcmVuKCc6bm90KGlucHV0KScpLmFkZENsYXNzKCdhY3RpdmUnKSk7XHJcblx0XHRcdGlmIChzZWxmLiRhY3RpdmVJdGVtcy5sZW5ndGgpIHtcclxuXHRcdFx0XHRzZWxmLmhpZGVJbnB1dCgpO1xyXG5cdFx0XHRcdHNlbGYuY2xvc2UoKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRzZWxmLmZvY3VzKCk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBIaWRlcyB0aGUgaW5wdXQgZWxlbWVudCBvdXQgb2Ygdmlldywgd2hpbGVcclxuXHRcdCAqIHJldGFpbmluZyBpdHMgZm9jdXMuXHJcblx0XHQgKi9cclxuXHRcdGhpZGVJbnB1dDogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcclxuXHRcdFx0c2VsZi5zZXRUZXh0Ym94VmFsdWUoJycpO1xyXG5cdFx0XHRzZWxmLiRjb250cm9sX2lucHV0LmNzcyh7b3BhY2l0eTogMCwgcG9zaXRpb246ICdhYnNvbHV0ZScsIGxlZnQ6IHNlbGYucnRsID8gMTAwMDAgOiAtMTAwMDB9KTtcclxuXHRcdFx0c2VsZi5pc0lucHV0SGlkZGVuID0gdHJ1ZTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFJlc3RvcmVzIGlucHV0IHZpc2liaWxpdHkuXHJcblx0XHQgKi9cclxuXHRcdHNob3dJbnB1dDogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHRoaXMuJGNvbnRyb2xfaW5wdXQuY3NzKHtvcGFjaXR5OiAxLCBwb3NpdGlvbjogJ3JlbGF0aXZlJywgbGVmdDogMH0pO1xyXG5cdFx0XHR0aGlzLmlzSW5wdXRIaWRkZW4gPSBmYWxzZTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIEdpdmVzIHRoZSBjb250cm9sIGZvY3VzLlxyXG5cdFx0ICovXHJcblx0XHRmb2N1czogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0aWYgKHNlbGYuaXNEaXNhYmxlZCkgcmV0dXJuO1xyXG5cdFxyXG5cdFx0XHRzZWxmLmlnbm9yZUZvY3VzID0gdHJ1ZTtcclxuXHRcdFx0c2VsZi4kY29udHJvbF9pbnB1dFswXS5mb2N1cygpO1xyXG5cdFx0XHR3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRzZWxmLmlnbm9yZUZvY3VzID0gZmFsc2U7XHJcblx0XHRcdFx0c2VsZi5vbkZvY3VzKCk7XHJcblx0XHRcdH0sIDApO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRm9yY2VzIHRoZSBjb250cm9sIG91dCBvZiBmb2N1cy5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge0VsZW1lbnR9IGRlc3RcclxuXHRcdCAqL1xyXG5cdFx0Ymx1cjogZnVuY3Rpb24oZGVzdCkge1xyXG5cdFx0XHR0aGlzLiRjb250cm9sX2lucHV0WzBdLmJsdXIoKTtcclxuXHRcdFx0dGhpcy5vbkJsdXIobnVsbCwgZGVzdCk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBSZXR1cm5zIGEgZnVuY3Rpb24gdGhhdCBzY29yZXMgYW4gb2JqZWN0XHJcblx0XHQgKiB0byBzaG93IGhvdyBnb29kIG9mIGEgbWF0Y2ggaXQgaXMgdG8gdGhlXHJcblx0XHQgKiBwcm92aWRlZCBxdWVyeS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gcXVlcnlcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBvcHRpb25zXHJcblx0XHQgKiBAcmV0dXJuIHtmdW5jdGlvbn1cclxuXHRcdCAqL1xyXG5cdFx0Z2V0U2NvcmVGdW5jdGlvbjogZnVuY3Rpb24ocXVlcnkpIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuc2lmdGVyLmdldFNjb3JlRnVuY3Rpb24ocXVlcnksIHRoaXMuZ2V0U2VhcmNoT3B0aW9ucygpKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFJldHVybnMgc2VhcmNoIG9wdGlvbnMgZm9yIHNpZnRlciAodGhlIHN5c3RlbVxyXG5cdFx0ICogZm9yIHNjb3JpbmcgYW5kIHNvcnRpbmcgcmVzdWx0cykuXHJcblx0XHQgKlxyXG5cdFx0ICogQHNlZSBodHRwczovL2dpdGh1Yi5jb20vYnJpYW5yZWF2aXMvc2lmdGVyLmpzXHJcblx0XHQgKiBAcmV0dXJuIHtvYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGdldFNlYXJjaE9wdGlvbnM6IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgc2V0dGluZ3MgPSB0aGlzLnNldHRpbmdzO1xyXG5cdFx0XHR2YXIgc29ydCA9IHNldHRpbmdzLnNvcnRGaWVsZDtcclxuXHRcdFx0aWYgKHR5cGVvZiBzb3J0ID09PSAnc3RyaW5nJykge1xyXG5cdFx0XHRcdHNvcnQgPSBbe2ZpZWxkOiBzb3J0fV07XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0cmV0dXJuIHtcclxuXHRcdFx0XHRmaWVsZHMgICAgICA6IHNldHRpbmdzLnNlYXJjaEZpZWxkLFxyXG5cdFx0XHRcdGNvbmp1bmN0aW9uIDogc2V0dGluZ3Muc2VhcmNoQ29uanVuY3Rpb24sXHJcblx0XHRcdFx0c29ydCAgICAgICAgOiBzb3J0XHJcblx0XHRcdH07XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBTZWFyY2hlcyB0aHJvdWdoIGF2YWlsYWJsZSBvcHRpb25zIGFuZCByZXR1cm5zXHJcblx0XHQgKiBhIHNvcnRlZCBhcnJheSBvZiBtYXRjaGVzLlxyXG5cdFx0ICpcclxuXHRcdCAqIFJldHVybnMgYW4gb2JqZWN0IGNvbnRhaW5pbmc6XHJcblx0XHQgKlxyXG5cdFx0ICogICAtIHF1ZXJ5IHtzdHJpbmd9XHJcblx0XHQgKiAgIC0gdG9rZW5zIHthcnJheX1cclxuXHRcdCAqICAgLSB0b3RhbCB7aW50fVxyXG5cdFx0ICogICAtIGl0ZW1zIHthcnJheX1cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gcXVlcnlcclxuXHRcdCAqIEByZXR1cm5zIHtvYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdHNlYXJjaDogZnVuY3Rpb24ocXVlcnkpIHtcclxuXHRcdFx0dmFyIGksIHZhbHVlLCBzY29yZSwgcmVzdWx0LCBjYWxjdWxhdGVTY29yZTtcclxuXHRcdFx0dmFyIHNlbGYgICAgID0gdGhpcztcclxuXHRcdFx0dmFyIHNldHRpbmdzID0gc2VsZi5zZXR0aW5ncztcclxuXHRcdFx0dmFyIG9wdGlvbnMgID0gdGhpcy5nZXRTZWFyY2hPcHRpb25zKCk7XHJcblx0XHJcblx0XHRcdC8vIHZhbGlkYXRlIHVzZXItcHJvdmlkZWQgcmVzdWx0IHNjb3JpbmcgZnVuY3Rpb25cclxuXHRcdFx0aWYgKHNldHRpbmdzLnNjb3JlKSB7XHJcblx0XHRcdFx0Y2FsY3VsYXRlU2NvcmUgPSBzZWxmLnNldHRpbmdzLnNjb3JlLmFwcGx5KHRoaXMsIFtxdWVyeV0pO1xyXG5cdFx0XHRcdGlmICh0eXBlb2YgY2FsY3VsYXRlU2NvcmUgIT09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0XHRcdHRocm93IG5ldyBFcnJvcignU2VsZWN0aXplIFwic2NvcmVcIiBzZXR0aW5nIG11c3QgYmUgYSBmdW5jdGlvbiB0aGF0IHJldHVybnMgYSBmdW5jdGlvbicpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQvLyBwZXJmb3JtIHNlYXJjaFxyXG5cdFx0XHRpZiAocXVlcnkgIT09IHNlbGYubGFzdFF1ZXJ5KSB7XHJcblx0XHRcdFx0c2VsZi5sYXN0UXVlcnkgPSBxdWVyeTtcclxuXHRcdFx0XHRyZXN1bHQgPSBzZWxmLnNpZnRlci5zZWFyY2gocXVlcnksICQuZXh0ZW5kKG9wdGlvbnMsIHtzY29yZTogY2FsY3VsYXRlU2NvcmV9KSk7XHJcblx0XHRcdFx0c2VsZi5jdXJyZW50UmVzdWx0cyA9IHJlc3VsdDtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXN1bHQgPSAkLmV4dGVuZCh0cnVlLCB7fSwgc2VsZi5jdXJyZW50UmVzdWx0cyk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0Ly8gZmlsdGVyIG91dCBzZWxlY3RlZCBpdGVtc1xyXG5cdFx0XHRpZiAoc2V0dGluZ3MuaGlkZVNlbGVjdGVkKSB7XHJcblx0XHRcdFx0Zm9yIChpID0gcmVzdWx0Lml0ZW1zLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XHJcblx0XHRcdFx0XHRpZiAoc2VsZi5pdGVtcy5pbmRleE9mKGhhc2hfa2V5KHJlc3VsdC5pdGVtc1tpXS5pZCkpICE9PSAtMSkge1xyXG5cdFx0XHRcdFx0XHRyZXN1bHQuaXRlbXMuc3BsaWNlKGksIDEpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogUmVmcmVzaGVzIHRoZSBsaXN0IG9mIGF2YWlsYWJsZSBvcHRpb25zIHNob3duXHJcblx0XHQgKiBpbiB0aGUgYXV0b2NvbXBsZXRlIGRyb3Bkb3duIG1lbnUuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtib29sZWFufSB0cmlnZ2VyRHJvcGRvd25cclxuXHRcdCAqL1xyXG5cdFx0cmVmcmVzaE9wdGlvbnM6IGZ1bmN0aW9uKHRyaWdnZXJEcm9wZG93bikge1xyXG5cdFx0XHR2YXIgaSwgaiwgaywgbiwgZ3JvdXBzLCBncm91cHNfb3JkZXIsIG9wdGlvbiwgb3B0aW9uX2h0bWwsIG9wdGdyb3VwLCBvcHRncm91cHMsIGh0bWwsIGh0bWxfY2hpbGRyZW4sIGhhc19jcmVhdGVfb3B0aW9uO1xyXG5cdFx0XHR2YXIgJGFjdGl2ZSwgJGFjdGl2ZV9iZWZvcmUsICRjcmVhdGU7XHJcblx0XHJcblx0XHRcdGlmICh0eXBlb2YgdHJpZ2dlckRyb3Bkb3duID09PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHRcdHRyaWdnZXJEcm9wZG93biA9IHRydWU7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0dmFyIHNlbGYgICAgICAgICAgICAgID0gdGhpcztcclxuXHRcdFx0dmFyIHF1ZXJ5ICAgICAgICAgICAgID0gJC50cmltKHNlbGYuJGNvbnRyb2xfaW5wdXQudmFsKCkpO1xyXG5cdFx0XHR2YXIgcmVzdWx0cyAgICAgICAgICAgPSBzZWxmLnNlYXJjaChxdWVyeSk7XHJcblx0XHRcdHZhciAkZHJvcGRvd25fY29udGVudCA9IHNlbGYuJGRyb3Bkb3duX2NvbnRlbnQ7XHJcblx0XHRcdHZhciBhY3RpdmVfYmVmb3JlICAgICA9IHNlbGYuJGFjdGl2ZU9wdGlvbiAmJiBoYXNoX2tleShzZWxmLiRhY3RpdmVPcHRpb24uYXR0cignZGF0YS12YWx1ZScpKTtcclxuXHRcclxuXHRcdFx0Ly8gYnVpbGQgbWFya3VwXHJcblx0XHRcdG4gPSByZXN1bHRzLml0ZW1zLmxlbmd0aDtcclxuXHRcdFx0aWYgKHR5cGVvZiBzZWxmLnNldHRpbmdzLm1heE9wdGlvbnMgPT09ICdudW1iZXInKSB7XHJcblx0XHRcdFx0biA9IE1hdGgubWluKG4sIHNlbGYuc2V0dGluZ3MubWF4T3B0aW9ucyk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0Ly8gcmVuZGVyIGFuZCBncm91cCBhdmFpbGFibGUgb3B0aW9ucyBpbmRpdmlkdWFsbHlcclxuXHRcdFx0Z3JvdXBzID0ge307XHJcblx0XHRcdGdyb3Vwc19vcmRlciA9IFtdO1xyXG5cdFxyXG5cdFx0XHRmb3IgKGkgPSAwOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdFx0b3B0aW9uICAgICAgPSBzZWxmLm9wdGlvbnNbcmVzdWx0cy5pdGVtc1tpXS5pZF07XHJcblx0XHRcdFx0b3B0aW9uX2h0bWwgPSBzZWxmLnJlbmRlcignb3B0aW9uJywgb3B0aW9uKTtcclxuXHRcdFx0XHRvcHRncm91cCAgICA9IG9wdGlvbltzZWxmLnNldHRpbmdzLm9wdGdyb3VwRmllbGRdIHx8ICcnO1xyXG5cdFx0XHRcdG9wdGdyb3VwcyAgID0gJC5pc0FycmF5KG9wdGdyb3VwKSA/IG9wdGdyb3VwIDogW29wdGdyb3VwXTtcclxuXHRcclxuXHRcdFx0XHRmb3IgKGogPSAwLCBrID0gb3B0Z3JvdXBzICYmIG9wdGdyb3Vwcy5sZW5ndGg7IGogPCBrOyBqKyspIHtcclxuXHRcdFx0XHRcdG9wdGdyb3VwID0gb3B0Z3JvdXBzW2pdO1xyXG5cdFx0XHRcdFx0aWYgKCFzZWxmLm9wdGdyb3Vwcy5oYXNPd25Qcm9wZXJ0eShvcHRncm91cCkpIHtcclxuXHRcdFx0XHRcdFx0b3B0Z3JvdXAgPSAnJztcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmICghZ3JvdXBzLmhhc093blByb3BlcnR5KG9wdGdyb3VwKSkge1xyXG5cdFx0XHRcdFx0XHRncm91cHNbb3B0Z3JvdXBdID0gW107XHJcblx0XHRcdFx0XHRcdGdyb3Vwc19vcmRlci5wdXNoKG9wdGdyb3VwKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGdyb3Vwc1tvcHRncm91cF0ucHVzaChvcHRpb25faHRtbCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIHNvcnQgb3B0Z3JvdXBzXHJcblx0XHRcdGlmICh0aGlzLnNldHRpbmdzLmxvY2tPcHRncm91cE9yZGVyKSB7XHJcblx0XHRcdFx0Z3JvdXBzX29yZGVyLnNvcnQoZnVuY3Rpb24oYSwgYikge1xyXG5cdFx0XHRcdFx0dmFyIGFfb3JkZXIgPSBzZWxmLm9wdGdyb3Vwc1thXS4kb3JkZXIgfHwgMDtcclxuXHRcdFx0XHRcdHZhciBiX29yZGVyID0gc2VsZi5vcHRncm91cHNbYl0uJG9yZGVyIHx8IDA7XHJcblx0XHRcdFx0XHRyZXR1cm4gYV9vcmRlciAtIGJfb3JkZXI7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0Ly8gcmVuZGVyIG9wdGdyb3VwIGhlYWRlcnMgJiBqb2luIGdyb3Vwc1xyXG5cdFx0XHRodG1sID0gW107XHJcblx0XHRcdGZvciAoaSA9IDAsIG4gPSBncm91cHNfb3JkZXIubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdFx0b3B0Z3JvdXAgPSBncm91cHNfb3JkZXJbaV07XHJcblx0XHRcdFx0aWYgKHNlbGYub3B0Z3JvdXBzLmhhc093blByb3BlcnR5KG9wdGdyb3VwKSAmJiBncm91cHNbb3B0Z3JvdXBdLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0Ly8gcmVuZGVyIHRoZSBvcHRncm91cCBoZWFkZXIgYW5kIG9wdGlvbnMgd2l0aGluIGl0LFxyXG5cdFx0XHRcdFx0Ly8gdGhlbiBwYXNzIGl0IHRvIHRoZSB3cmFwcGVyIHRlbXBsYXRlXHJcblx0XHRcdFx0XHRodG1sX2NoaWxkcmVuID0gc2VsZi5yZW5kZXIoJ29wdGdyb3VwX2hlYWRlcicsIHNlbGYub3B0Z3JvdXBzW29wdGdyb3VwXSkgfHwgJyc7XHJcblx0XHRcdFx0XHRodG1sX2NoaWxkcmVuICs9IGdyb3Vwc1tvcHRncm91cF0uam9pbignJyk7XHJcblx0XHRcdFx0XHRodG1sLnB1c2goc2VsZi5yZW5kZXIoJ29wdGdyb3VwJywgJC5leHRlbmQoe30sIHNlbGYub3B0Z3JvdXBzW29wdGdyb3VwXSwge1xyXG5cdFx0XHRcdFx0XHRodG1sOiBodG1sX2NoaWxkcmVuXHJcblx0XHRcdFx0XHR9KSkpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRodG1sLnB1c2goZ3JvdXBzW29wdGdyb3VwXS5qb2luKCcnKSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdCRkcm9wZG93bl9jb250ZW50Lmh0bWwoaHRtbC5qb2luKCcnKSk7XHJcblx0XHJcblx0XHRcdC8vIGhpZ2hsaWdodCBtYXRjaGluZyB0ZXJtcyBpbmxpbmVcclxuXHRcdFx0aWYgKHNlbGYuc2V0dGluZ3MuaGlnaGxpZ2h0ICYmIHJlc3VsdHMucXVlcnkubGVuZ3RoICYmIHJlc3VsdHMudG9rZW5zLmxlbmd0aCkge1xyXG5cdFx0XHRcdGZvciAoaSA9IDAsIG4gPSByZXN1bHRzLnRva2Vucy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuXHRcdFx0XHRcdGhpZ2hsaWdodCgkZHJvcGRvd25fY29udGVudCwgcmVzdWx0cy50b2tlbnNbaV0ucmVnZXgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQvLyBhZGQgXCJzZWxlY3RlZFwiIGNsYXNzIHRvIHNlbGVjdGVkIG9wdGlvbnNcclxuXHRcdFx0aWYgKCFzZWxmLnNldHRpbmdzLmhpZGVTZWxlY3RlZCkge1xyXG5cdFx0XHRcdGZvciAoaSA9IDAsIG4gPSBzZWxmLml0ZW1zLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG5cdFx0XHRcdFx0c2VsZi5nZXRPcHRpb24oc2VsZi5pdGVtc1tpXSkuYWRkQ2xhc3MoJ3NlbGVjdGVkJyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIGFkZCBjcmVhdGUgb3B0aW9uXHJcblx0XHRcdGhhc19jcmVhdGVfb3B0aW9uID0gc2VsZi5jYW5DcmVhdGUocXVlcnkpO1xyXG5cdFx0XHRpZiAoaGFzX2NyZWF0ZV9vcHRpb24pIHtcclxuXHRcdFx0XHQkZHJvcGRvd25fY29udGVudC5wcmVwZW5kKHNlbGYucmVuZGVyKCdvcHRpb25fY3JlYXRlJywge2lucHV0OiBxdWVyeX0pKTtcclxuXHRcdFx0XHQkY3JlYXRlID0gJCgkZHJvcGRvd25fY29udGVudFswXS5jaGlsZE5vZGVzWzBdKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQvLyBhY3RpdmF0ZVxyXG5cdFx0XHRzZWxmLmhhc09wdGlvbnMgPSByZXN1bHRzLml0ZW1zLmxlbmd0aCA+IDAgfHwgaGFzX2NyZWF0ZV9vcHRpb247XHJcblx0XHRcdGlmIChzZWxmLmhhc09wdGlvbnMpIHtcclxuXHRcdFx0XHRpZiAocmVzdWx0cy5pdGVtcy5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0XHQkYWN0aXZlX2JlZm9yZSA9IGFjdGl2ZV9iZWZvcmUgJiYgc2VsZi5nZXRPcHRpb24oYWN0aXZlX2JlZm9yZSk7XHJcblx0XHRcdFx0XHRpZiAoJGFjdGl2ZV9iZWZvcmUgJiYgJGFjdGl2ZV9iZWZvcmUubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRcdCRhY3RpdmUgPSAkYWN0aXZlX2JlZm9yZTtcclxuXHRcdFx0XHRcdH0gZWxzZSBpZiAoc2VsZi5zZXR0aW5ncy5tb2RlID09PSAnc2luZ2xlJyAmJiBzZWxmLml0ZW1zLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHQkYWN0aXZlID0gc2VsZi5nZXRPcHRpb24oc2VsZi5pdGVtc1swXSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAoISRhY3RpdmUgfHwgISRhY3RpdmUubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRcdGlmICgkY3JlYXRlICYmICFzZWxmLnNldHRpbmdzLmFkZFByZWNlZGVuY2UpIHtcclxuXHRcdFx0XHRcdFx0XHQkYWN0aXZlID0gc2VsZi5nZXRBZGphY2VudE9wdGlvbigkY3JlYXRlLCAxKTtcclxuXHRcdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0XHQkYWN0aXZlID0gJGRyb3Bkb3duX2NvbnRlbnQuZmluZCgnW2RhdGEtc2VsZWN0YWJsZV06Zmlyc3QnKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHQkYWN0aXZlID0gJGNyZWF0ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0c2VsZi5zZXRBY3RpdmVPcHRpb24oJGFjdGl2ZSk7XHJcblx0XHRcdFx0aWYgKHRyaWdnZXJEcm9wZG93biAmJiAhc2VsZi5pc09wZW4pIHsgc2VsZi5vcGVuKCk7IH1cclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRzZWxmLnNldEFjdGl2ZU9wdGlvbihudWxsKTtcclxuXHRcdFx0XHRpZiAodHJpZ2dlckRyb3Bkb3duICYmIHNlbGYuaXNPcGVuKSB7IHNlbGYuY2xvc2UoKTsgfVxyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBBZGRzIGFuIGF2YWlsYWJsZSBvcHRpb24uIElmIGl0IGFscmVhZHkgZXhpc3RzLFxyXG5cdFx0ICogbm90aGluZyB3aWxsIGhhcHBlbi4gTm90ZTogdGhpcyBkb2VzIG5vdCByZWZyZXNoXHJcblx0XHQgKiB0aGUgb3B0aW9ucyBsaXN0IGRyb3Bkb3duICh1c2UgYHJlZnJlc2hPcHRpb25zYFxyXG5cdFx0ICogZm9yIHRoYXQpLlxyXG5cdFx0ICpcclxuXHRcdCAqIFVzYWdlOlxyXG5cdFx0ICpcclxuXHRcdCAqICAgdGhpcy5hZGRPcHRpb24oZGF0YSlcclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdHxhcnJheX0gZGF0YVxyXG5cdFx0ICovXHJcblx0XHRhZGRPcHRpb246IGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFx0dmFyIGksIG4sIHZhbHVlLCBzZWxmID0gdGhpcztcclxuXHRcclxuXHRcdFx0aWYgKCQuaXNBcnJheShkYXRhKSkge1xyXG5cdFx0XHRcdGZvciAoaSA9IDAsIG4gPSBkYXRhLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG5cdFx0XHRcdFx0c2VsZi5hZGRPcHRpb24oZGF0YVtpXSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRpZiAodmFsdWUgPSBzZWxmLnJlZ2lzdGVyT3B0aW9uKGRhdGEpKSB7XHJcblx0XHRcdFx0c2VsZi51c2VyT3B0aW9uc1t2YWx1ZV0gPSB0cnVlO1xyXG5cdFx0XHRcdHNlbGYubGFzdFF1ZXJ5ID0gbnVsbDtcclxuXHRcdFx0XHRzZWxmLnRyaWdnZXIoJ29wdGlvbl9hZGQnLCB2YWx1ZSwgZGF0YSk7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFJlZ2lzdGVycyBhbiBvcHRpb24gdG8gdGhlIHBvb2wgb2Ygb3B0aW9ucy5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZGF0YVxyXG5cdFx0ICogQHJldHVybiB7Ym9vbGVhbnxzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdHJlZ2lzdGVyT3B0aW9uOiBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcdHZhciBrZXkgPSBoYXNoX2tleShkYXRhW3RoaXMuc2V0dGluZ3MudmFsdWVGaWVsZF0pO1xyXG5cdFx0XHRpZiAoIWtleSB8fCB0aGlzLm9wdGlvbnMuaGFzT3duUHJvcGVydHkoa2V5KSkgcmV0dXJuIGZhbHNlO1xyXG5cdFx0XHRkYXRhLiRvcmRlciA9IGRhdGEuJG9yZGVyIHx8ICsrdGhpcy5vcmRlcjtcclxuXHRcdFx0dGhpcy5vcHRpb25zW2tleV0gPSBkYXRhO1xyXG5cdFx0XHRyZXR1cm4ga2V5O1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogUmVnaXN0ZXJzIGFuIG9wdGlvbiBncm91cCB0byB0aGUgcG9vbCBvZiBvcHRpb24gZ3JvdXBzLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBkYXRhXHJcblx0XHQgKiBAcmV0dXJuIHtib29sZWFufHN0cmluZ31cclxuXHRcdCAqL1xyXG5cdFx0cmVnaXN0ZXJPcHRpb25Hcm91cDogZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0XHR2YXIga2V5ID0gaGFzaF9rZXkoZGF0YVt0aGlzLnNldHRpbmdzLm9wdGdyb3VwVmFsdWVGaWVsZF0pO1xyXG5cdFx0XHRpZiAoIWtleSkgcmV0dXJuIGZhbHNlO1xyXG5cdFxyXG5cdFx0XHRkYXRhLiRvcmRlciA9IGRhdGEuJG9yZGVyIHx8ICsrdGhpcy5vcmRlcjtcclxuXHRcdFx0dGhpcy5vcHRncm91cHNba2V5XSA9IGRhdGE7XHJcblx0XHRcdHJldHVybiBrZXk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBSZWdpc3RlcnMgYSBuZXcgb3B0Z3JvdXAgZm9yIG9wdGlvbnNcclxuXHRcdCAqIHRvIGJlIGJ1Y2tldGVkIGludG8uXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGlkXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZGF0YVxyXG5cdFx0ICovXHJcblx0XHRhZGRPcHRpb25Hcm91cDogZnVuY3Rpb24oaWQsIGRhdGEpIHtcclxuXHRcdFx0ZGF0YVt0aGlzLnNldHRpbmdzLm9wdGdyb3VwVmFsdWVGaWVsZF0gPSBpZDtcclxuXHRcdFx0aWYgKGlkID0gdGhpcy5yZWdpc3Rlck9wdGlvbkdyb3VwKGRhdGEpKSB7XHJcblx0XHRcdFx0dGhpcy50cmlnZ2VyKCdvcHRncm91cF9hZGQnLCBpZCwgZGF0YSk7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFJlbW92ZXMgYW4gZXhpc3Rpbmcgb3B0aW9uIGdyb3VwLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBpZFxyXG5cdFx0ICovXHJcblx0XHRyZW1vdmVPcHRpb25Hcm91cDogZnVuY3Rpb24oaWQpIHtcclxuXHRcdFx0aWYgKHRoaXMub3B0Z3JvdXBzLmhhc093blByb3BlcnR5KGlkKSkge1xyXG5cdFx0XHRcdGRlbGV0ZSB0aGlzLm9wdGdyb3Vwc1tpZF07XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJDYWNoZSA9IHt9O1xyXG5cdFx0XHRcdHRoaXMudHJpZ2dlcignb3B0Z3JvdXBfcmVtb3ZlJywgaWQpO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBDbGVhcnMgYWxsIGV4aXN0aW5nIG9wdGlvbiBncm91cHMuXHJcblx0XHQgKi9cclxuXHRcdGNsZWFyT3B0aW9uR3JvdXBzOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dGhpcy5vcHRncm91cHMgPSB7fTtcclxuXHRcdFx0dGhpcy5yZW5kZXJDYWNoZSA9IHt9O1xyXG5cdFx0XHR0aGlzLnRyaWdnZXIoJ29wdGdyb3VwX2NsZWFyJyk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBVcGRhdGVzIGFuIG9wdGlvbiBhdmFpbGFibGUgZm9yIHNlbGVjdGlvbi4gSWZcclxuXHRcdCAqIGl0IGlzIHZpc2libGUgaW4gdGhlIHNlbGVjdGVkIGl0ZW1zIG9yIG9wdGlvbnNcclxuXHRcdCAqIGRyb3Bkb3duLCBpdCB3aWxsIGJlIHJlLXJlbmRlcmVkIGF1dG9tYXRpY2FsbHkuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZGF0YVxyXG5cdFx0ICovXHJcblx0XHR1cGRhdGVPcHRpb246IGZ1bmN0aW9uKHZhbHVlLCBkYXRhKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0dmFyICRpdGVtLCAkaXRlbV9uZXc7XHJcblx0XHRcdHZhciB2YWx1ZV9uZXcsIGluZGV4X2l0ZW0sIGNhY2hlX2l0ZW1zLCBjYWNoZV9vcHRpb25zLCBvcmRlcl9vbGQ7XHJcblx0XHJcblx0XHRcdHZhbHVlICAgICA9IGhhc2hfa2V5KHZhbHVlKTtcclxuXHRcdFx0dmFsdWVfbmV3ID0gaGFzaF9rZXkoZGF0YVtzZWxmLnNldHRpbmdzLnZhbHVlRmllbGRdKTtcclxuXHRcclxuXHRcdFx0Ly8gc2FuaXR5IGNoZWNrc1xyXG5cdFx0XHRpZiAodmFsdWUgPT09IG51bGwpIHJldHVybjtcclxuXHRcdFx0aWYgKCFzZWxmLm9wdGlvbnMuaGFzT3duUHJvcGVydHkodmFsdWUpKSByZXR1cm47XHJcblx0XHRcdGlmICh0eXBlb2YgdmFsdWVfbmV3ICE9PSAnc3RyaW5nJykgdGhyb3cgbmV3IEVycm9yKCdWYWx1ZSBtdXN0IGJlIHNldCBpbiBvcHRpb24gZGF0YScpO1xyXG5cdFxyXG5cdFx0XHRvcmRlcl9vbGQgPSBzZWxmLm9wdGlvbnNbdmFsdWVdLiRvcmRlcjtcclxuXHRcclxuXHRcdFx0Ly8gdXBkYXRlIHJlZmVyZW5jZXNcclxuXHRcdFx0aWYgKHZhbHVlX25ldyAhPT0gdmFsdWUpIHtcclxuXHRcdFx0XHRkZWxldGUgc2VsZi5vcHRpb25zW3ZhbHVlXTtcclxuXHRcdFx0XHRpbmRleF9pdGVtID0gc2VsZi5pdGVtcy5pbmRleE9mKHZhbHVlKTtcclxuXHRcdFx0XHRpZiAoaW5kZXhfaXRlbSAhPT0gLTEpIHtcclxuXHRcdFx0XHRcdHNlbGYuaXRlbXMuc3BsaWNlKGluZGV4X2l0ZW0sIDEsIHZhbHVlX25ldyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdGRhdGEuJG9yZGVyID0gZGF0YS4kb3JkZXIgfHwgb3JkZXJfb2xkO1xyXG5cdFx0XHRzZWxmLm9wdGlvbnNbdmFsdWVfbmV3XSA9IGRhdGE7XHJcblx0XHJcblx0XHRcdC8vIGludmFsaWRhdGUgcmVuZGVyIGNhY2hlXHJcblx0XHRcdGNhY2hlX2l0ZW1zID0gc2VsZi5yZW5kZXJDYWNoZVsnaXRlbSddO1xyXG5cdFx0XHRjYWNoZV9vcHRpb25zID0gc2VsZi5yZW5kZXJDYWNoZVsnb3B0aW9uJ107XHJcblx0XHJcblx0XHRcdGlmIChjYWNoZV9pdGVtcykge1xyXG5cdFx0XHRcdGRlbGV0ZSBjYWNoZV9pdGVtc1t2YWx1ZV07XHJcblx0XHRcdFx0ZGVsZXRlIGNhY2hlX2l0ZW1zW3ZhbHVlX25ld107XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKGNhY2hlX29wdGlvbnMpIHtcclxuXHRcdFx0XHRkZWxldGUgY2FjaGVfb3B0aW9uc1t2YWx1ZV07XHJcblx0XHRcdFx0ZGVsZXRlIGNhY2hlX29wdGlvbnNbdmFsdWVfbmV3XTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQvLyB1cGRhdGUgdGhlIGl0ZW0gaWYgaXQncyBzZWxlY3RlZFxyXG5cdFx0XHRpZiAoc2VsZi5pdGVtcy5pbmRleE9mKHZhbHVlX25ldykgIT09IC0xKSB7XHJcblx0XHRcdFx0JGl0ZW0gPSBzZWxmLmdldEl0ZW0odmFsdWUpO1xyXG5cdFx0XHRcdCRpdGVtX25ldyA9ICQoc2VsZi5yZW5kZXIoJ2l0ZW0nLCBkYXRhKSk7XHJcblx0XHRcdFx0aWYgKCRpdGVtLmhhc0NsYXNzKCdhY3RpdmUnKSkgJGl0ZW1fbmV3LmFkZENsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHQkaXRlbS5yZXBsYWNlV2l0aCgkaXRlbV9uZXcpO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIGludmFsaWRhdGUgbGFzdCBxdWVyeSBiZWNhdXNlIHdlIG1pZ2h0IGhhdmUgdXBkYXRlZCB0aGUgc29ydEZpZWxkXHJcblx0XHRcdHNlbGYubGFzdFF1ZXJ5ID0gbnVsbDtcclxuXHRcclxuXHRcdFx0Ly8gdXBkYXRlIGRyb3Bkb3duIGNvbnRlbnRzXHJcblx0XHRcdGlmIChzZWxmLmlzT3Blbikge1xyXG5cdFx0XHRcdHNlbGYucmVmcmVzaE9wdGlvbnMoZmFsc2UpO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBSZW1vdmVzIGEgc2luZ2xlIG9wdGlvbi5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuXHRcdCAqIEBwYXJhbSB7Ym9vbGVhbn0gc2lsZW50XHJcblx0XHQgKi9cclxuXHRcdHJlbW92ZU9wdGlvbjogZnVuY3Rpb24odmFsdWUsIHNpbGVudCkge1xyXG5cdFx0XHR2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHRcdHZhbHVlID0gaGFzaF9rZXkodmFsdWUpO1xyXG5cdFxyXG5cdFx0XHR2YXIgY2FjaGVfaXRlbXMgPSBzZWxmLnJlbmRlckNhY2hlWydpdGVtJ107XHJcblx0XHRcdHZhciBjYWNoZV9vcHRpb25zID0gc2VsZi5yZW5kZXJDYWNoZVsnb3B0aW9uJ107XHJcblx0XHRcdGlmIChjYWNoZV9pdGVtcykgZGVsZXRlIGNhY2hlX2l0ZW1zW3ZhbHVlXTtcclxuXHRcdFx0aWYgKGNhY2hlX29wdGlvbnMpIGRlbGV0ZSBjYWNoZV9vcHRpb25zW3ZhbHVlXTtcclxuXHRcclxuXHRcdFx0ZGVsZXRlIHNlbGYudXNlck9wdGlvbnNbdmFsdWVdO1xyXG5cdFx0XHRkZWxldGUgc2VsZi5vcHRpb25zW3ZhbHVlXTtcclxuXHRcdFx0c2VsZi5sYXN0UXVlcnkgPSBudWxsO1xyXG5cdFx0XHRzZWxmLnRyaWdnZXIoJ29wdGlvbl9yZW1vdmUnLCB2YWx1ZSk7XHJcblx0XHRcdHNlbGYucmVtb3ZlSXRlbSh2YWx1ZSwgc2lsZW50KTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIENsZWFycyBhbGwgb3B0aW9ucy5cclxuXHRcdCAqL1xyXG5cdFx0Y2xlYXJPcHRpb25zOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdFx0XHRzZWxmLmxvYWRlZFNlYXJjaGVzID0ge307XHJcblx0XHRcdHNlbGYudXNlck9wdGlvbnMgPSB7fTtcclxuXHRcdFx0c2VsZi5yZW5kZXJDYWNoZSA9IHt9O1xyXG5cdFx0XHRzZWxmLm9wdGlvbnMgPSBzZWxmLnNpZnRlci5pdGVtcyA9IHt9O1xyXG5cdFx0XHRzZWxmLmxhc3RRdWVyeSA9IG51bGw7XHJcblx0XHRcdHNlbGYudHJpZ2dlcignb3B0aW9uX2NsZWFyJyk7XHJcblx0XHRcdHNlbGYuY2xlYXIoKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFJldHVybnMgdGhlIGpRdWVyeSBlbGVtZW50IG9mIHRoZSBvcHRpb25cclxuXHRcdCAqIG1hdGNoaW5nIHRoZSBnaXZlbiB2YWx1ZS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuXHRcdCAqIEByZXR1cm5zIHtvYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGdldE9wdGlvbjogZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuZ2V0RWxlbWVudFdpdGhWYWx1ZSh2YWx1ZSwgdGhpcy4kZHJvcGRvd25fY29udGVudC5maW5kKCdbZGF0YS1zZWxlY3RhYmxlXScpKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFJldHVybnMgdGhlIGpRdWVyeSBlbGVtZW50IG9mIHRoZSBuZXh0IG9yXHJcblx0XHQgKiBwcmV2aW91cyBzZWxlY3RhYmxlIG9wdGlvbi5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gJG9wdGlvblxyXG5cdFx0ICogQHBhcmFtIHtpbnR9IGRpcmVjdGlvbiAgY2FuIGJlIDEgZm9yIG5leHQgb3IgLTEgZm9yIHByZXZpb3VzXHJcblx0XHQgKiBAcmV0dXJuIHtvYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGdldEFkamFjZW50T3B0aW9uOiBmdW5jdGlvbigkb3B0aW9uLCBkaXJlY3Rpb24pIHtcclxuXHRcdFx0dmFyICRvcHRpb25zID0gdGhpcy4kZHJvcGRvd24uZmluZCgnW2RhdGEtc2VsZWN0YWJsZV0nKTtcclxuXHRcdFx0dmFyIGluZGV4ICAgID0gJG9wdGlvbnMuaW5kZXgoJG9wdGlvbikgKyBkaXJlY3Rpb247XHJcblx0XHJcblx0XHRcdHJldHVybiBpbmRleCA+PSAwICYmIGluZGV4IDwgJG9wdGlvbnMubGVuZ3RoID8gJG9wdGlvbnMuZXEoaW5kZXgpIDogJCgpO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRmluZHMgdGhlIGZpcnN0IGVsZW1lbnQgd2l0aCBhIFwiZGF0YS12YWx1ZVwiIGF0dHJpYnV0ZVxyXG5cdFx0ICogdGhhdCBtYXRjaGVzIHRoZSBnaXZlbiB2YWx1ZS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge21peGVkfSB2YWx1ZVxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9ICRlbHNcclxuXHRcdCAqIEByZXR1cm4ge29iamVjdH1cclxuXHRcdCAqL1xyXG5cdFx0Z2V0RWxlbWVudFdpdGhWYWx1ZTogZnVuY3Rpb24odmFsdWUsICRlbHMpIHtcclxuXHRcdFx0dmFsdWUgPSBoYXNoX2tleSh2YWx1ZSk7XHJcblx0XHJcblx0XHRcdGlmICh0eXBlb2YgdmFsdWUgIT09ICd1bmRlZmluZWQnICYmIHZhbHVlICE9PSBudWxsKSB7XHJcblx0XHRcdFx0Zm9yICh2YXIgaSA9IDAsIG4gPSAkZWxzLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG5cdFx0XHRcdFx0aWYgKCRlbHNbaV0uZ2V0QXR0cmlidXRlKCdkYXRhLXZhbHVlJykgPT09IHZhbHVlKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiAkKCRlbHNbaV0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRyZXR1cm4gJCgpO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogUmV0dXJucyB0aGUgalF1ZXJ5IGVsZW1lbnQgb2YgdGhlIGl0ZW1cclxuXHRcdCAqIG1hdGNoaW5nIHRoZSBnaXZlbiB2YWx1ZS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuXHRcdCAqIEByZXR1cm5zIHtvYmplY3R9XHJcblx0XHQgKi9cclxuXHRcdGdldEl0ZW06IGZ1bmN0aW9uKHZhbHVlKSB7XHJcblx0XHRcdHJldHVybiB0aGlzLmdldEVsZW1lbnRXaXRoVmFsdWUodmFsdWUsIHRoaXMuJGNvbnRyb2wuY2hpbGRyZW4oKSk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBcIlNlbGVjdHNcIiBtdWx0aXBsZSBpdGVtcyBhdCBvbmNlLiBBZGRzIHRoZW0gdG8gdGhlIGxpc3RcclxuXHRcdCAqIGF0IHRoZSBjdXJyZW50IGNhcmV0IHBvc2l0aW9uLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG5cdFx0ICogQHBhcmFtIHtib29sZWFufSBzaWxlbnRcclxuXHRcdCAqL1xyXG5cdFx0YWRkSXRlbXM6IGZ1bmN0aW9uKHZhbHVlcywgc2lsZW50KSB7XHJcblx0XHRcdHZhciBpdGVtcyA9ICQuaXNBcnJheSh2YWx1ZXMpID8gdmFsdWVzIDogW3ZhbHVlc107XHJcblx0XHRcdGZvciAodmFyIGkgPSAwLCBuID0gaXRlbXMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdFx0dGhpcy5pc1BlbmRpbmcgPSAoaSA8IG4gLSAxKTtcclxuXHRcdFx0XHR0aGlzLmFkZEl0ZW0oaXRlbXNbaV0sIHNpbGVudCk7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFwiU2VsZWN0c1wiIGFuIGl0ZW0uIEFkZHMgaXQgdG8gdGhlIGxpc3RcclxuXHRcdCAqIGF0IHRoZSBjdXJyZW50IGNhcmV0IHBvc2l0aW9uLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG5cdFx0ICogQHBhcmFtIHtib29sZWFufSBzaWxlbnRcclxuXHRcdCAqL1xyXG5cdFx0YWRkSXRlbTogZnVuY3Rpb24odmFsdWUsIHNpbGVudCkge1xyXG5cdFx0XHR2YXIgZXZlbnRzID0gc2lsZW50ID8gW10gOiBbJ2NoYW5nZSddO1xyXG5cdFxyXG5cdFx0XHRkZWJvdW5jZV9ldmVudHModGhpcywgZXZlbnRzLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHR2YXIgJGl0ZW0sICRvcHRpb24sICRvcHRpb25zO1xyXG5cdFx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0XHR2YXIgaW5wdXRNb2RlID0gc2VsZi5zZXR0aW5ncy5tb2RlO1xyXG5cdFx0XHRcdHZhciBpLCBhY3RpdmUsIHZhbHVlX25leHQsIHdhc0Z1bGw7XHJcblx0XHRcdFx0dmFsdWUgPSBoYXNoX2tleSh2YWx1ZSk7XHJcblx0XHJcblx0XHRcdFx0aWYgKHNlbGYuaXRlbXMuaW5kZXhPZih2YWx1ZSkgIT09IC0xKSB7XHJcblx0XHRcdFx0XHRpZiAoaW5wdXRNb2RlID09PSAnc2luZ2xlJykgc2VsZi5jbG9zZSgpO1xyXG5cdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdH1cclxuXHRcclxuXHRcdFx0XHRpZiAoIXNlbGYub3B0aW9ucy5oYXNPd25Qcm9wZXJ0eSh2YWx1ZSkpIHJldHVybjtcclxuXHRcdFx0XHRpZiAoaW5wdXRNb2RlID09PSAnc2luZ2xlJykgc2VsZi5jbGVhcihzaWxlbnQpO1xyXG5cdFx0XHRcdGlmIChpbnB1dE1vZGUgPT09ICdtdWx0aScgJiYgc2VsZi5pc0Z1bGwoKSkgcmV0dXJuO1xyXG5cdFxyXG5cdFx0XHRcdCRpdGVtID0gJChzZWxmLnJlbmRlcignaXRlbScsIHNlbGYub3B0aW9uc1t2YWx1ZV0pKTtcclxuXHRcdFx0XHR3YXNGdWxsID0gc2VsZi5pc0Z1bGwoKTtcclxuXHRcdFx0XHRzZWxmLml0ZW1zLnNwbGljZShzZWxmLmNhcmV0UG9zLCAwLCB2YWx1ZSk7XHJcblx0XHRcdFx0c2VsZi5pbnNlcnRBdENhcmV0KCRpdGVtKTtcclxuXHRcdFx0XHRpZiAoIXNlbGYuaXNQZW5kaW5nIHx8ICghd2FzRnVsbCAmJiBzZWxmLmlzRnVsbCgpKSkge1xyXG5cdFx0XHRcdFx0c2VsZi5yZWZyZXNoU3RhdGUoKTtcclxuXHRcdFx0XHR9XHJcblx0XHJcblx0XHRcdFx0aWYgKHNlbGYuaXNTZXR1cCkge1xyXG5cdFx0XHRcdFx0JG9wdGlvbnMgPSBzZWxmLiRkcm9wZG93bl9jb250ZW50LmZpbmQoJ1tkYXRhLXNlbGVjdGFibGVdJyk7XHJcblx0XHJcblx0XHRcdFx0XHQvLyB1cGRhdGUgbWVudSAvIHJlbW92ZSB0aGUgb3B0aW9uIChpZiB0aGlzIGlzIG5vdCBvbmUgaXRlbSBiZWluZyBhZGRlZCBhcyBwYXJ0IG9mIHNlcmllcylcclxuXHRcdFx0XHRcdGlmICghc2VsZi5pc1BlbmRpbmcpIHtcclxuXHRcdFx0XHRcdFx0JG9wdGlvbiA9IHNlbGYuZ2V0T3B0aW9uKHZhbHVlKTtcclxuXHRcdFx0XHRcdFx0dmFsdWVfbmV4dCA9IHNlbGYuZ2V0QWRqYWNlbnRPcHRpb24oJG9wdGlvbiwgMSkuYXR0cignZGF0YS12YWx1ZScpO1xyXG5cdFx0XHRcdFx0XHRzZWxmLnJlZnJlc2hPcHRpb25zKHNlbGYuaXNGb2N1c2VkICYmIGlucHV0TW9kZSAhPT0gJ3NpbmdsZScpO1xyXG5cdFx0XHRcdFx0XHRpZiAodmFsdWVfbmV4dCkge1xyXG5cdFx0XHRcdFx0XHRcdHNlbGYuc2V0QWN0aXZlT3B0aW9uKHNlbGYuZ2V0T3B0aW9uKHZhbHVlX25leHQpKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRcdFx0Ly8gaGlkZSB0aGUgbWVudSBpZiB0aGUgbWF4aW11bSBudW1iZXIgb2YgaXRlbXMgaGF2ZSBiZWVuIHNlbGVjdGVkIG9yIG5vIG9wdGlvbnMgYXJlIGxlZnRcclxuXHRcdFx0XHRcdGlmICghJG9wdGlvbnMubGVuZ3RoIHx8IHNlbGYuaXNGdWxsKCkpIHtcclxuXHRcdFx0XHRcdFx0c2VsZi5jbG9zZSgpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0c2VsZi5wb3NpdGlvbkRyb3Bkb3duKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHJcblx0XHRcdFx0XHRzZWxmLnVwZGF0ZVBsYWNlaG9sZGVyKCk7XHJcblx0XHRcdFx0XHRzZWxmLnRyaWdnZXIoJ2l0ZW1fYWRkJywgdmFsdWUsICRpdGVtKTtcclxuXHRcdFx0XHRcdHNlbGYudXBkYXRlT3JpZ2luYWxJbnB1dCh7c2lsZW50OiBzaWxlbnR9KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogUmVtb3ZlcyB0aGUgc2VsZWN0ZWQgaXRlbSBtYXRjaGluZ1xyXG5cdFx0ICogdGhlIHByb3ZpZGVkIHZhbHVlLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG5cdFx0ICovXHJcblx0XHRyZW1vdmVJdGVtOiBmdW5jdGlvbih2YWx1ZSwgc2lsZW50KSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0dmFyICRpdGVtLCBpLCBpZHg7XHJcblx0XHJcblx0XHRcdCRpdGVtID0gKHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcpID8gdmFsdWUgOiBzZWxmLmdldEl0ZW0odmFsdWUpO1xyXG5cdFx0XHR2YWx1ZSA9IGhhc2hfa2V5KCRpdGVtLmF0dHIoJ2RhdGEtdmFsdWUnKSk7XHJcblx0XHRcdGkgPSBzZWxmLml0ZW1zLmluZGV4T2YodmFsdWUpO1xyXG5cdFxyXG5cdFx0XHRpZiAoaSAhPT0gLTEpIHtcclxuXHRcdFx0XHQkaXRlbS5yZW1vdmUoKTtcclxuXHRcdFx0XHRpZiAoJGl0ZW0uaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XHJcblx0XHRcdFx0XHRpZHggPSBzZWxmLiRhY3RpdmVJdGVtcy5pbmRleE9mKCRpdGVtWzBdKTtcclxuXHRcdFx0XHRcdHNlbGYuJGFjdGl2ZUl0ZW1zLnNwbGljZShpZHgsIDEpO1xyXG5cdFx0XHRcdH1cclxuXHRcclxuXHRcdFx0XHRzZWxmLml0ZW1zLnNwbGljZShpLCAxKTtcclxuXHRcdFx0XHRzZWxmLmxhc3RRdWVyeSA9IG51bGw7XHJcblx0XHRcdFx0aWYgKCFzZWxmLnNldHRpbmdzLnBlcnNpc3QgJiYgc2VsZi51c2VyT3B0aW9ucy5oYXNPd25Qcm9wZXJ0eSh2YWx1ZSkpIHtcclxuXHRcdFx0XHRcdHNlbGYucmVtb3ZlT3B0aW9uKHZhbHVlLCBzaWxlbnQpO1xyXG5cdFx0XHRcdH1cclxuXHRcclxuXHRcdFx0XHRpZiAoaSA8IHNlbGYuY2FyZXRQb3MpIHtcclxuXHRcdFx0XHRcdHNlbGYuc2V0Q2FyZXQoc2VsZi5jYXJldFBvcyAtIDEpO1xyXG5cdFx0XHRcdH1cclxuXHRcclxuXHRcdFx0XHRzZWxmLnJlZnJlc2hTdGF0ZSgpO1xyXG5cdFx0XHRcdHNlbGYudXBkYXRlUGxhY2Vob2xkZXIoKTtcclxuXHRcdFx0XHRzZWxmLnVwZGF0ZU9yaWdpbmFsSW5wdXQoe3NpbGVudDogc2lsZW50fSk7XHJcblx0XHRcdFx0c2VsZi5wb3NpdGlvbkRyb3Bkb3duKCk7XHJcblx0XHRcdFx0c2VsZi50cmlnZ2VyKCdpdGVtX3JlbW92ZScsIHZhbHVlLCAkaXRlbSk7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIEludm9rZXMgdGhlIGBjcmVhdGVgIG1ldGhvZCBwcm92aWRlZCBpbiB0aGVcclxuXHRcdCAqIHNlbGVjdGl6ZSBvcHRpb25zIHRoYXQgc2hvdWxkIHByb3ZpZGUgdGhlIGRhdGFcclxuXHRcdCAqIGZvciB0aGUgbmV3IGl0ZW0sIGdpdmVuIHRoZSB1c2VyIGlucHV0LlxyXG5cdFx0ICpcclxuXHRcdCAqIE9uY2UgdGhpcyBjb21wbGV0ZXMsIGl0IHdpbGwgYmUgYWRkZWRcclxuXHRcdCAqIHRvIHRoZSBpdGVtIGxpc3QuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlXHJcblx0XHQgKiBAcGFyYW0ge2Jvb2xlYW59IFt0cmlnZ2VyRHJvcGRvd25dXHJcblx0XHQgKiBAcGFyYW0ge2Z1bmN0aW9ufSBbY2FsbGJhY2tdXHJcblx0XHQgKiBAcmV0dXJuIHtib29sZWFufVxyXG5cdFx0ICovXHJcblx0XHRjcmVhdGVJdGVtOiBmdW5jdGlvbihpbnB1dCwgdHJpZ2dlckRyb3Bkb3duKSB7XHJcblx0XHRcdHZhciBzZWxmICA9IHRoaXM7XHJcblx0XHRcdHZhciBjYXJldCA9IHNlbGYuY2FyZXRQb3M7XHJcblx0XHRcdGlucHV0ID0gaW5wdXQgfHwgJC50cmltKHNlbGYuJGNvbnRyb2xfaW5wdXQudmFsKCkgfHwgJycpO1xyXG5cdFxyXG5cdFx0XHR2YXIgY2FsbGJhY2sgPSBhcmd1bWVudHNbYXJndW1lbnRzLmxlbmd0aCAtIDFdO1xyXG5cdFx0XHRpZiAodHlwZW9mIGNhbGxiYWNrICE9PSAnZnVuY3Rpb24nKSBjYWxsYmFjayA9IGZ1bmN0aW9uKCkge307XHJcblx0XHJcblx0XHRcdGlmICh0eXBlb2YgdHJpZ2dlckRyb3Bkb3duICE9PSAnYm9vbGVhbicpIHtcclxuXHRcdFx0XHR0cmlnZ2VyRHJvcGRvd24gPSB0cnVlO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdGlmICghc2VsZi5jYW5DcmVhdGUoaW5wdXQpKSB7XHJcblx0XHRcdFx0Y2FsbGJhY2soKTtcclxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0c2VsZi5sb2NrKCk7XHJcblx0XHJcblx0XHRcdHZhciBzZXR1cCA9ICh0eXBlb2Ygc2VsZi5zZXR0aW5ncy5jcmVhdGUgPT09ICdmdW5jdGlvbicpID8gdGhpcy5zZXR0aW5ncy5jcmVhdGUgOiBmdW5jdGlvbihpbnB1dCkge1xyXG5cdFx0XHRcdHZhciBkYXRhID0ge307XHJcblx0XHRcdFx0ZGF0YVtzZWxmLnNldHRpbmdzLmxhYmVsRmllbGRdID0gaW5wdXQ7XHJcblx0XHRcdFx0ZGF0YVtzZWxmLnNldHRpbmdzLnZhbHVlRmllbGRdID0gaW5wdXQ7XHJcblx0XHRcdFx0cmV0dXJuIGRhdGE7XHJcblx0XHRcdH07XHJcblx0XHJcblx0XHRcdHZhciBjcmVhdGUgPSBvbmNlKGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFx0XHRzZWxmLnVubG9jaygpO1xyXG5cdFxyXG5cdFx0XHRcdGlmICghZGF0YSB8fCB0eXBlb2YgZGF0YSAhPT0gJ29iamVjdCcpIHJldHVybiBjYWxsYmFjaygpO1xyXG5cdFx0XHRcdHZhciB2YWx1ZSA9IGhhc2hfa2V5KGRhdGFbc2VsZi5zZXR0aW5ncy52YWx1ZUZpZWxkXSk7XHJcblx0XHRcdFx0aWYgKHR5cGVvZiB2YWx1ZSAhPT0gJ3N0cmluZycpIHJldHVybiBjYWxsYmFjaygpO1xyXG5cdFxyXG5cdFx0XHRcdHNlbGYuc2V0VGV4dGJveFZhbHVlKCcnKTtcclxuXHRcdFx0XHRzZWxmLmFkZE9wdGlvbihkYXRhKTtcclxuXHRcdFx0XHRzZWxmLnNldENhcmV0KGNhcmV0KTtcclxuXHRcdFx0XHRzZWxmLmFkZEl0ZW0odmFsdWUpO1xyXG5cdFx0XHRcdHNlbGYucmVmcmVzaE9wdGlvbnModHJpZ2dlckRyb3Bkb3duICYmIHNlbGYuc2V0dGluZ3MubW9kZSAhPT0gJ3NpbmdsZScpO1xyXG5cdFx0XHRcdGNhbGxiYWNrKGRhdGEpO1xyXG5cdFx0XHR9KTtcclxuXHRcclxuXHRcdFx0dmFyIG91dHB1dCA9IHNldHVwLmFwcGx5KHRoaXMsIFtpbnB1dCwgY3JlYXRlXSk7XHJcblx0XHRcdGlmICh0eXBlb2Ygb3V0cHV0ICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHRcdGNyZWF0ZShvdXRwdXQpO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogUmUtcmVuZGVycyB0aGUgc2VsZWN0ZWQgaXRlbSBsaXN0cy5cclxuXHRcdCAqL1xyXG5cdFx0cmVmcmVzaEl0ZW1zOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dGhpcy5sYXN0UXVlcnkgPSBudWxsO1xyXG5cdFxyXG5cdFx0XHRpZiAodGhpcy5pc1NldHVwKSB7XHJcblx0XHRcdFx0dGhpcy5hZGRJdGVtKHRoaXMuaXRlbXMpO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdHRoaXMucmVmcmVzaFN0YXRlKCk7XHJcblx0XHRcdHRoaXMudXBkYXRlT3JpZ2luYWxJbnB1dCgpO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogVXBkYXRlcyBhbGwgc3RhdGUtZGVwZW5kZW50IGF0dHJpYnV0ZXNcclxuXHRcdCAqIGFuZCBDU1MgY2xhc3Nlcy5cclxuXHRcdCAqL1xyXG5cdFx0cmVmcmVzaFN0YXRlOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIGludmFsaWQsIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHRpZiAoc2VsZi5pc1JlcXVpcmVkKSB7XHJcblx0XHRcdFx0aWYgKHNlbGYuaXRlbXMubGVuZ3RoKSBzZWxmLmlzSW52YWxpZCA9IGZhbHNlO1xyXG5cdFx0XHRcdHNlbGYuJGNvbnRyb2xfaW5wdXQucHJvcCgncmVxdWlyZWQnLCBpbnZhbGlkKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRzZWxmLnJlZnJlc2hDbGFzc2VzKCk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBVcGRhdGVzIGFsbCBzdGF0ZS1kZXBlbmRlbnQgQ1NTIGNsYXNzZXMuXHJcblx0XHQgKi9cclxuXHRcdHJlZnJlc2hDbGFzc2VzOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIHNlbGYgICAgID0gdGhpcztcclxuXHRcdFx0dmFyIGlzRnVsbCAgID0gc2VsZi5pc0Z1bGwoKTtcclxuXHRcdFx0dmFyIGlzTG9ja2VkID0gc2VsZi5pc0xvY2tlZDtcclxuXHRcclxuXHRcdFx0c2VsZi4kd3JhcHBlclxyXG5cdFx0XHRcdC50b2dnbGVDbGFzcygncnRsJywgc2VsZi5ydGwpO1xyXG5cdFxyXG5cdFx0XHRzZWxmLiRjb250cm9sXHJcblx0XHRcdFx0LnRvZ2dsZUNsYXNzKCdmb2N1cycsIHNlbGYuaXNGb2N1c2VkKVxyXG5cdFx0XHRcdC50b2dnbGVDbGFzcygnZGlzYWJsZWQnLCBzZWxmLmlzRGlzYWJsZWQpXHJcblx0XHRcdFx0LnRvZ2dsZUNsYXNzKCdyZXF1aXJlZCcsIHNlbGYuaXNSZXF1aXJlZClcclxuXHRcdFx0XHQudG9nZ2xlQ2xhc3MoJ2ludmFsaWQnLCBzZWxmLmlzSW52YWxpZClcclxuXHRcdFx0XHQudG9nZ2xlQ2xhc3MoJ2xvY2tlZCcsIGlzTG9ja2VkKVxyXG5cdFx0XHRcdC50b2dnbGVDbGFzcygnZnVsbCcsIGlzRnVsbCkudG9nZ2xlQ2xhc3MoJ25vdC1mdWxsJywgIWlzRnVsbClcclxuXHRcdFx0XHQudG9nZ2xlQ2xhc3MoJ2lucHV0LWFjdGl2ZScsIHNlbGYuaXNGb2N1c2VkICYmICFzZWxmLmlzSW5wdXRIaWRkZW4pXHJcblx0XHRcdFx0LnRvZ2dsZUNsYXNzKCdkcm9wZG93bi1hY3RpdmUnLCBzZWxmLmlzT3BlbilcclxuXHRcdFx0XHQudG9nZ2xlQ2xhc3MoJ2hhcy1vcHRpb25zJywgISQuaXNFbXB0eU9iamVjdChzZWxmLm9wdGlvbnMpKVxyXG5cdFx0XHRcdC50b2dnbGVDbGFzcygnaGFzLWl0ZW1zJywgc2VsZi5pdGVtcy5sZW5ndGggPiAwKTtcclxuXHRcclxuXHRcdFx0c2VsZi4kY29udHJvbF9pbnB1dC5kYXRhKCdncm93JywgIWlzRnVsbCAmJiAhaXNMb2NrZWQpO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogRGV0ZXJtaW5lcyB3aGV0aGVyIG9yIG5vdCBtb3JlIGl0ZW1zIGNhbiBiZSBhZGRlZFxyXG5cdFx0ICogdG8gdGhlIGNvbnRyb2wgd2l0aG91dCBleGNlZWRpbmcgdGhlIHVzZXItZGVmaW5lZCBtYXhpbXVtLlxyXG5cdFx0ICpcclxuXHRcdCAqIEByZXR1cm5zIHtib29sZWFufVxyXG5cdFx0ICovXHJcblx0XHRpc0Z1bGw6IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5zZXR0aW5ncy5tYXhJdGVtcyAhPT0gbnVsbCAmJiB0aGlzLml0ZW1zLmxlbmd0aCA+PSB0aGlzLnNldHRpbmdzLm1heEl0ZW1zO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogUmVmcmVzaGVzIHRoZSBvcmlnaW5hbCA8c2VsZWN0PiBvciA8aW5wdXQ+XHJcblx0XHQgKiBlbGVtZW50IHRvIHJlZmxlY3QgdGhlIGN1cnJlbnQgc3RhdGUuXHJcblx0XHQgKi9cclxuXHRcdHVwZGF0ZU9yaWdpbmFsSW5wdXQ6IGZ1bmN0aW9uKG9wdHMpIHtcclxuXHRcdFx0dmFyIGksIG4sIG9wdGlvbnMsIGxhYmVsLCBzZWxmID0gdGhpcztcclxuXHRcdFx0b3B0cyA9IG9wdHMgfHwge307XHJcblx0XHJcblx0XHRcdGlmIChzZWxmLnRhZ1R5cGUgPT09IFRBR19TRUxFQ1QpIHtcclxuXHRcdFx0XHRvcHRpb25zID0gW107XHJcblx0XHRcdFx0Zm9yIChpID0gMCwgbiA9IHNlbGYuaXRlbXMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdFx0XHRsYWJlbCA9IHNlbGYub3B0aW9uc1tzZWxmLml0ZW1zW2ldXVtzZWxmLnNldHRpbmdzLmxhYmVsRmllbGRdIHx8ICcnO1xyXG5cdFx0XHRcdFx0b3B0aW9ucy5wdXNoKCc8b3B0aW9uIHZhbHVlPVwiJyArIGVzY2FwZV9odG1sKHNlbGYuaXRlbXNbaV0pICsgJ1wiIHNlbGVjdGVkPVwic2VsZWN0ZWRcIj4nICsgZXNjYXBlX2h0bWwobGFiZWwpICsgJzwvb3B0aW9uPicpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAoIW9wdGlvbnMubGVuZ3RoICYmICF0aGlzLiRpbnB1dC5hdHRyKCdtdWx0aXBsZScpKSB7XHJcblx0XHRcdFx0XHRvcHRpb25zLnB1c2goJzxvcHRpb24gdmFsdWU9XCJcIiBzZWxlY3RlZD1cInNlbGVjdGVkXCI+PC9vcHRpb24+Jyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHNlbGYuJGlucHV0Lmh0bWwob3B0aW9ucy5qb2luKCcnKSk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0c2VsZi4kaW5wdXQudmFsKHNlbGYuZ2V0VmFsdWUoKSk7XHJcblx0XHRcdFx0c2VsZi4kaW5wdXQuYXR0cigndmFsdWUnLHNlbGYuJGlucHV0LnZhbCgpKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRpZiAoc2VsZi5pc1NldHVwKSB7XHJcblx0XHRcdFx0aWYgKCFvcHRzLnNpbGVudCkge1xyXG5cdFx0XHRcdFx0c2VsZi50cmlnZ2VyKCdjaGFuZ2UnLCBzZWxmLiRpbnB1dC52YWwoKSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBTaG93cy9oaWRlIHRoZSBpbnB1dCBwbGFjZWhvbGRlciBkZXBlbmRpbmdcclxuXHRcdCAqIG9uIGlmIHRoZXJlIGl0ZW1zIGluIHRoZSBsaXN0IGFscmVhZHkuXHJcblx0XHQgKi9cclxuXHRcdHVwZGF0ZVBsYWNlaG9sZGVyOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKCF0aGlzLnNldHRpbmdzLnBsYWNlaG9sZGVyKSByZXR1cm47XHJcblx0XHRcdHZhciAkaW5wdXQgPSB0aGlzLiRjb250cm9sX2lucHV0O1xyXG5cdFxyXG5cdFx0XHRpZiAodGhpcy5pdGVtcy5sZW5ndGgpIHtcclxuXHRcdFx0XHQkaW5wdXQucmVtb3ZlQXR0cigncGxhY2Vob2xkZXInKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHQkaW5wdXQuYXR0cigncGxhY2Vob2xkZXInLCB0aGlzLnNldHRpbmdzLnBsYWNlaG9sZGVyKTtcclxuXHRcdFx0fVxyXG5cdFx0XHQkaW5wdXQudHJpZ2dlckhhbmRsZXIoJ3VwZGF0ZScsIHtmb3JjZTogdHJ1ZX0pO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogU2hvd3MgdGhlIGF1dG9jb21wbGV0ZSBkcm9wZG93biBjb250YWluaW5nXHJcblx0XHQgKiB0aGUgYXZhaWxhYmxlIG9wdGlvbnMuXHJcblx0XHQgKi9cclxuXHRcdG9wZW46IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHJcblx0XHRcdGlmIChzZWxmLmlzTG9ja2VkIHx8IHNlbGYuaXNPcGVuIHx8IChzZWxmLnNldHRpbmdzLm1vZGUgPT09ICdtdWx0aScgJiYgc2VsZi5pc0Z1bGwoKSkpIHJldHVybjtcclxuXHRcdFx0c2VsZi5mb2N1cygpO1xyXG5cdFx0XHRzZWxmLmlzT3BlbiA9IHRydWU7XHJcblx0XHRcdHNlbGYucmVmcmVzaFN0YXRlKCk7XHJcblx0XHRcdHNlbGYuJGRyb3Bkb3duLmNzcyh7dmlzaWJpbGl0eTogJ2hpZGRlbicsIGRpc3BsYXk6ICdibG9jayd9KTtcclxuXHRcdFx0c2VsZi5wb3NpdGlvbkRyb3Bkb3duKCk7XHJcblx0XHRcdHNlbGYuJGRyb3Bkb3duLmNzcyh7dmlzaWJpbGl0eTogJ3Zpc2libGUnfSk7XHJcblx0XHRcdHNlbGYudHJpZ2dlcignZHJvcGRvd25fb3BlbicsIHNlbGYuJGRyb3Bkb3duKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIENsb3NlcyB0aGUgYXV0b2NvbXBsZXRlIGRyb3Bkb3duIG1lbnUuXHJcblx0XHQgKi9cclxuXHRcdGNsb3NlOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHR2YXIgdHJpZ2dlciA9IHNlbGYuaXNPcGVuO1xyXG5cdFxyXG5cdFx0XHRpZiAoc2VsZi5zZXR0aW5ncy5tb2RlID09PSAnc2luZ2xlJyAmJiBzZWxmLml0ZW1zLmxlbmd0aCkge1xyXG5cdFx0XHRcdHNlbGYuaGlkZUlucHV0KCk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0c2VsZi5pc09wZW4gPSBmYWxzZTtcclxuXHRcdFx0c2VsZi4kZHJvcGRvd24uaGlkZSgpO1xyXG5cdFx0XHRzZWxmLnNldEFjdGl2ZU9wdGlvbihudWxsKTtcclxuXHRcdFx0c2VsZi5yZWZyZXNoU3RhdGUoKTtcclxuXHRcclxuXHRcdFx0aWYgKHRyaWdnZXIpIHNlbGYudHJpZ2dlcignZHJvcGRvd25fY2xvc2UnLCBzZWxmLiRkcm9wZG93bik7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBDYWxjdWxhdGVzIGFuZCBhcHBsaWVzIHRoZSBhcHByb3ByaWF0ZVxyXG5cdFx0ICogcG9zaXRpb24gb2YgdGhlIGRyb3Bkb3duLlxyXG5cdFx0ICovXHJcblx0XHRwb3NpdGlvbkRyb3Bkb3duOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyICRjb250cm9sID0gdGhpcy4kY29udHJvbDtcclxuXHRcdFx0dmFyIG9mZnNldCA9IHRoaXMuc2V0dGluZ3MuZHJvcGRvd25QYXJlbnQgPT09ICdib2R5JyA/ICRjb250cm9sLm9mZnNldCgpIDogJGNvbnRyb2wucG9zaXRpb24oKTtcclxuXHRcdFx0b2Zmc2V0LnRvcCArPSAkY29udHJvbC5vdXRlckhlaWdodCh0cnVlKTtcclxuXHRcclxuXHRcdFx0dGhpcy4kZHJvcGRvd24uY3NzKHtcclxuXHRcdFx0XHR3aWR0aCA6ICRjb250cm9sLm91dGVyV2lkdGgoKSxcclxuXHRcdFx0XHR0b3AgICA6IG9mZnNldC50b3AsXHJcblx0XHRcdFx0bGVmdCAgOiBvZmZzZXQubGVmdFxyXG5cdFx0XHR9KTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIFJlc2V0cyAvIGNsZWFycyBhbGwgc2VsZWN0ZWQgaXRlbXNcclxuXHRcdCAqIGZyb20gdGhlIGNvbnRyb2wuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtib29sZWFufSBzaWxlbnRcclxuXHRcdCAqL1xyXG5cdFx0Y2xlYXI6IGZ1bmN0aW9uKHNpbGVudCkge1xyXG5cdFx0XHR2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHJcblx0XHRcdGlmICghc2VsZi5pdGVtcy5sZW5ndGgpIHJldHVybjtcclxuXHRcdFx0c2VsZi4kY29udHJvbC5jaGlsZHJlbignOm5vdChpbnB1dCknKS5yZW1vdmUoKTtcclxuXHRcdFx0c2VsZi5pdGVtcyA9IFtdO1xyXG5cdFx0XHRzZWxmLmxhc3RRdWVyeSA9IG51bGw7XHJcblx0XHRcdHNlbGYuc2V0Q2FyZXQoMCk7XHJcblx0XHRcdHNlbGYuc2V0QWN0aXZlSXRlbShudWxsKTtcclxuXHRcdFx0c2VsZi51cGRhdGVQbGFjZWhvbGRlcigpO1xyXG5cdFx0XHRzZWxmLnVwZGF0ZU9yaWdpbmFsSW5wdXQoe3NpbGVudDogc2lsZW50fSk7XHJcblx0XHRcdHNlbGYucmVmcmVzaFN0YXRlKCk7XHJcblx0XHRcdHNlbGYuc2hvd0lucHV0KCk7XHJcblx0XHRcdHNlbGYudHJpZ2dlcignY2xlYXInKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIEEgaGVscGVyIG1ldGhvZCBmb3IgaW5zZXJ0aW5nIGFuIGVsZW1lbnRcclxuXHRcdCAqIGF0IHRoZSBjdXJyZW50IGNhcmV0IHBvc2l0aW9uLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSAkZWxcclxuXHRcdCAqL1xyXG5cdFx0aW5zZXJ0QXRDYXJldDogZnVuY3Rpb24oJGVsKSB7XHJcblx0XHRcdHZhciBjYXJldCA9IE1hdGgubWluKHRoaXMuY2FyZXRQb3MsIHRoaXMuaXRlbXMubGVuZ3RoKTtcclxuXHRcdFx0aWYgKGNhcmV0ID09PSAwKSB7XHJcblx0XHRcdFx0dGhpcy4kY29udHJvbC5wcmVwZW5kKCRlbCk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0JCh0aGlzLiRjb250cm9sWzBdLmNoaWxkTm9kZXNbY2FyZXRdKS5iZWZvcmUoJGVsKTtcclxuXHRcdFx0fVxyXG5cdFx0XHR0aGlzLnNldENhcmV0KGNhcmV0ICsgMSk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBSZW1vdmVzIHRoZSBjdXJyZW50IHNlbGVjdGVkIGl0ZW0ocykuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGUgKG9wdGlvbmFsKVxyXG5cdFx0ICogQHJldHVybnMge2Jvb2xlYW59XHJcblx0XHQgKi9cclxuXHRcdGRlbGV0ZVNlbGVjdGlvbjogZnVuY3Rpb24oZSkge1xyXG5cdFx0XHR2YXIgaSwgbiwgZGlyZWN0aW9uLCBzZWxlY3Rpb24sIHZhbHVlcywgY2FyZXQsIG9wdGlvbl9zZWxlY3QsICRvcHRpb25fc2VsZWN0LCAkdGFpbDtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdFx0XHRkaXJlY3Rpb24gPSAoZSAmJiBlLmtleUNvZGUgPT09IEtFWV9CQUNLU1BBQ0UpID8gLTEgOiAxO1xyXG5cdFx0XHRzZWxlY3Rpb24gPSBnZXRTZWxlY3Rpb24oc2VsZi4kY29udHJvbF9pbnB1dFswXSk7XHJcblx0XHJcblx0XHRcdGlmIChzZWxmLiRhY3RpdmVPcHRpb24gJiYgIXNlbGYuc2V0dGluZ3MuaGlkZVNlbGVjdGVkKSB7XHJcblx0XHRcdFx0b3B0aW9uX3NlbGVjdCA9IHNlbGYuZ2V0QWRqYWNlbnRPcHRpb24oc2VsZi4kYWN0aXZlT3B0aW9uLCAtMSkuYXR0cignZGF0YS12YWx1ZScpO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIGRldGVybWluZSBpdGVtcyB0aGF0IHdpbGwgYmUgcmVtb3ZlZFxyXG5cdFx0XHR2YWx1ZXMgPSBbXTtcclxuXHRcclxuXHRcdFx0aWYgKHNlbGYuJGFjdGl2ZUl0ZW1zLmxlbmd0aCkge1xyXG5cdFx0XHRcdCR0YWlsID0gc2VsZi4kY29udHJvbC5jaGlsZHJlbignLmFjdGl2ZTonICsgKGRpcmVjdGlvbiA+IDAgPyAnbGFzdCcgOiAnZmlyc3QnKSk7XHJcblx0XHRcdFx0Y2FyZXQgPSBzZWxmLiRjb250cm9sLmNoaWxkcmVuKCc6bm90KGlucHV0KScpLmluZGV4KCR0YWlsKTtcclxuXHRcdFx0XHRpZiAoZGlyZWN0aW9uID4gMCkgeyBjYXJldCsrOyB9XHJcblx0XHJcblx0XHRcdFx0Zm9yIChpID0gMCwgbiA9IHNlbGYuJGFjdGl2ZUl0ZW1zLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG5cdFx0XHRcdFx0dmFsdWVzLnB1c2goJChzZWxmLiRhY3RpdmVJdGVtc1tpXSkuYXR0cignZGF0YS12YWx1ZScpKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKGUpIHtcclxuXHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2UgaWYgKChzZWxmLmlzRm9jdXNlZCB8fCBzZWxmLnNldHRpbmdzLm1vZGUgPT09ICdzaW5nbGUnKSAmJiBzZWxmLml0ZW1zLmxlbmd0aCkge1xyXG5cdFx0XHRcdGlmIChkaXJlY3Rpb24gPCAwICYmIHNlbGVjdGlvbi5zdGFydCA9PT0gMCAmJiBzZWxlY3Rpb24ubGVuZ3RoID09PSAwKSB7XHJcblx0XHRcdFx0XHR2YWx1ZXMucHVzaChzZWxmLml0ZW1zW3NlbGYuY2FyZXRQb3MgLSAxXSk7XHJcblx0XHRcdFx0fSBlbHNlIGlmIChkaXJlY3Rpb24gPiAwICYmIHNlbGVjdGlvbi5zdGFydCA9PT0gc2VsZi4kY29udHJvbF9pbnB1dC52YWwoKS5sZW5ndGgpIHtcclxuXHRcdFx0XHRcdHZhbHVlcy5wdXNoKHNlbGYuaXRlbXNbc2VsZi5jYXJldFBvc10pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQvLyBhbGxvdyB0aGUgY2FsbGJhY2sgdG8gYWJvcnRcclxuXHRcdFx0aWYgKCF2YWx1ZXMubGVuZ3RoIHx8ICh0eXBlb2Ygc2VsZi5zZXR0aW5ncy5vbkRlbGV0ZSA9PT0gJ2Z1bmN0aW9uJyAmJiBzZWxmLnNldHRpbmdzLm9uRGVsZXRlLmFwcGx5KHNlbGYsIFt2YWx1ZXNdKSA9PT0gZmFsc2UpKSB7XHJcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIHBlcmZvcm0gcmVtb3ZhbFxyXG5cdFx0XHRpZiAodHlwZW9mIGNhcmV0ICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHRcdHNlbGYuc2V0Q2FyZXQoY2FyZXQpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHdoaWxlICh2YWx1ZXMubGVuZ3RoKSB7XHJcblx0XHRcdFx0c2VsZi5yZW1vdmVJdGVtKHZhbHVlcy5wb3AoKSk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0c2VsZi5zaG93SW5wdXQoKTtcclxuXHRcdFx0c2VsZi5wb3NpdGlvbkRyb3Bkb3duKCk7XHJcblx0XHRcdHNlbGYucmVmcmVzaE9wdGlvbnModHJ1ZSk7XHJcblx0XHJcblx0XHRcdC8vIHNlbGVjdCBwcmV2aW91cyBvcHRpb25cclxuXHRcdFx0aWYgKG9wdGlvbl9zZWxlY3QpIHtcclxuXHRcdFx0XHQkb3B0aW9uX3NlbGVjdCA9IHNlbGYuZ2V0T3B0aW9uKG9wdGlvbl9zZWxlY3QpO1xyXG5cdFx0XHRcdGlmICgkb3B0aW9uX3NlbGVjdC5sZW5ndGgpIHtcclxuXHRcdFx0XHRcdHNlbGYuc2V0QWN0aXZlT3B0aW9uKCRvcHRpb25fc2VsZWN0KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBTZWxlY3RzIHRoZSBwcmV2aW91cyAvIG5leHQgaXRlbSAoZGVwZW5kaW5nXHJcblx0XHQgKiBvbiB0aGUgYGRpcmVjdGlvbmAgYXJndW1lbnQpLlxyXG5cdFx0ICpcclxuXHRcdCAqID4gMCAtIHJpZ2h0XHJcblx0XHQgKiA8IDAgLSBsZWZ0XHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtpbnR9IGRpcmVjdGlvblxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGUgKG9wdGlvbmFsKVxyXG5cdFx0ICovXHJcblx0XHRhZHZhbmNlU2VsZWN0aW9uOiBmdW5jdGlvbihkaXJlY3Rpb24sIGUpIHtcclxuXHRcdFx0dmFyIHRhaWwsIHNlbGVjdGlvbiwgaWR4LCB2YWx1ZUxlbmd0aCwgY3Vyc29yQXRFZGdlLCAkdGFpbDtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdFx0XHRpZiAoZGlyZWN0aW9uID09PSAwKSByZXR1cm47XHJcblx0XHRcdGlmIChzZWxmLnJ0bCkgZGlyZWN0aW9uICo9IC0xO1xyXG5cdFxyXG5cdFx0XHR0YWlsID0gZGlyZWN0aW9uID4gMCA/ICdsYXN0JyA6ICdmaXJzdCc7XHJcblx0XHRcdHNlbGVjdGlvbiA9IGdldFNlbGVjdGlvbihzZWxmLiRjb250cm9sX2lucHV0WzBdKTtcclxuXHRcclxuXHRcdFx0aWYgKHNlbGYuaXNGb2N1c2VkICYmICFzZWxmLmlzSW5wdXRIaWRkZW4pIHtcclxuXHRcdFx0XHR2YWx1ZUxlbmd0aCA9IHNlbGYuJGNvbnRyb2xfaW5wdXQudmFsKCkubGVuZ3RoO1xyXG5cdFx0XHRcdGN1cnNvckF0RWRnZSA9IGRpcmVjdGlvbiA8IDBcclxuXHRcdFx0XHRcdD8gc2VsZWN0aW9uLnN0YXJ0ID09PSAwICYmIHNlbGVjdGlvbi5sZW5ndGggPT09IDBcclxuXHRcdFx0XHRcdDogc2VsZWN0aW9uLnN0YXJ0ID09PSB2YWx1ZUxlbmd0aDtcclxuXHRcclxuXHRcdFx0XHRpZiAoY3Vyc29yQXRFZGdlICYmICF2YWx1ZUxlbmd0aCkge1xyXG5cdFx0XHRcdFx0c2VsZi5hZHZhbmNlQ2FyZXQoZGlyZWN0aW9uLCBlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0JHRhaWwgPSBzZWxmLiRjb250cm9sLmNoaWxkcmVuKCcuYWN0aXZlOicgKyB0YWlsKTtcclxuXHRcdFx0XHRpZiAoJHRhaWwubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRpZHggPSBzZWxmLiRjb250cm9sLmNoaWxkcmVuKCc6bm90KGlucHV0KScpLmluZGV4KCR0YWlsKTtcclxuXHRcdFx0XHRcdHNlbGYuc2V0QWN0aXZlSXRlbShudWxsKTtcclxuXHRcdFx0XHRcdHNlbGYuc2V0Q2FyZXQoZGlyZWN0aW9uID4gMCA/IGlkeCArIDEgOiBpZHgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogTW92ZXMgdGhlIGNhcmV0IGxlZnQgLyByaWdodC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2ludH0gZGlyZWN0aW9uXHJcblx0XHQgKiBAcGFyYW0ge29iamVjdH0gZSAob3B0aW9uYWwpXHJcblx0XHQgKi9cclxuXHRcdGFkdmFuY2VDYXJldDogZnVuY3Rpb24oZGlyZWN0aW9uLCBlKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcywgZm4sICRhZGo7XHJcblx0XHJcblx0XHRcdGlmIChkaXJlY3Rpb24gPT09IDApIHJldHVybjtcclxuXHRcclxuXHRcdFx0Zm4gPSBkaXJlY3Rpb24gPiAwID8gJ25leHQnIDogJ3ByZXYnO1xyXG5cdFx0XHRpZiAoc2VsZi5pc1NoaWZ0RG93bikge1xyXG5cdFx0XHRcdCRhZGogPSBzZWxmLiRjb250cm9sX2lucHV0W2ZuXSgpO1xyXG5cdFx0XHRcdGlmICgkYWRqLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0c2VsZi5oaWRlSW5wdXQoKTtcclxuXHRcdFx0XHRcdHNlbGYuc2V0QWN0aXZlSXRlbSgkYWRqKTtcclxuXHRcdFx0XHRcdGUgJiYgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRzZWxmLnNldENhcmV0KHNlbGYuY2FyZXRQb3MgKyBkaXJlY3Rpb24pO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBNb3ZlcyB0aGUgY2FyZXQgdG8gdGhlIHNwZWNpZmllZCBpbmRleC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2ludH0gaVxyXG5cdFx0ICovXHJcblx0XHRzZXRDYXJldDogZnVuY3Rpb24oaSkge1xyXG5cdFx0XHR2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHJcblx0XHRcdGlmIChzZWxmLnNldHRpbmdzLm1vZGUgPT09ICdzaW5nbGUnKSB7XHJcblx0XHRcdFx0aSA9IHNlbGYuaXRlbXMubGVuZ3RoO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGkgPSBNYXRoLm1heCgwLCBNYXRoLm1pbihzZWxmLml0ZW1zLmxlbmd0aCwgaSkpO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdGlmKCFzZWxmLmlzUGVuZGluZykge1xyXG5cdFx0XHRcdC8vIHRoZSBpbnB1dCBtdXN0IGJlIG1vdmVkIGJ5IGxlYXZpbmcgaXQgaW4gcGxhY2UgYW5kIG1vdmluZyB0aGVcclxuXHRcdFx0XHQvLyBzaWJsaW5ncywgZHVlIHRvIHRoZSBmYWN0IHRoYXQgZm9jdXMgY2Fubm90IGJlIHJlc3RvcmVkIG9uY2UgbG9zdFxyXG5cdFx0XHRcdC8vIG9uIG1vYmlsZSB3ZWJraXQgZGV2aWNlc1xyXG5cdFx0XHRcdHZhciBqLCBuLCBmbiwgJGNoaWxkcmVuLCAkY2hpbGQ7XHJcblx0XHRcdFx0JGNoaWxkcmVuID0gc2VsZi4kY29udHJvbC5jaGlsZHJlbignOm5vdChpbnB1dCknKTtcclxuXHRcdFx0XHRmb3IgKGogPSAwLCBuID0gJGNoaWxkcmVuLmxlbmd0aDsgaiA8IG47IGorKykge1xyXG5cdFx0XHRcdFx0JGNoaWxkID0gJCgkY2hpbGRyZW5bal0pLmRldGFjaCgpO1xyXG5cdFx0XHRcdFx0aWYgKGogPCAgaSkge1xyXG5cdFx0XHRcdFx0XHRzZWxmLiRjb250cm9sX2lucHV0LmJlZm9yZSgkY2hpbGQpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0c2VsZi4kY29udHJvbC5hcHBlbmQoJGNoaWxkKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0c2VsZi5jYXJldFBvcyA9IGk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBEaXNhYmxlcyB1c2VyIGlucHV0IG9uIHRoZSBjb250cm9sLiBVc2VkIHdoaWxlXHJcblx0XHQgKiBpdGVtcyBhcmUgYmVpbmcgYXN5bmNocm9ub3VzbHkgY3JlYXRlZC5cclxuXHRcdCAqL1xyXG5cdFx0bG9jazogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHRoaXMuY2xvc2UoKTtcclxuXHRcdFx0dGhpcy5pc0xvY2tlZCA9IHRydWU7XHJcblx0XHRcdHRoaXMucmVmcmVzaFN0YXRlKCk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBSZS1lbmFibGVzIHVzZXIgaW5wdXQgb24gdGhlIGNvbnRyb2wuXHJcblx0XHQgKi9cclxuXHRcdHVubG9jazogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHRoaXMuaXNMb2NrZWQgPSBmYWxzZTtcclxuXHRcdFx0dGhpcy5yZWZyZXNoU3RhdGUoKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIERpc2FibGVzIHVzZXIgaW5wdXQgb24gdGhlIGNvbnRyb2wgY29tcGxldGVseS5cclxuXHRcdCAqIFdoaWxlIGRpc2FibGVkLCBpdCBjYW5ub3QgcmVjZWl2ZSBmb2N1cy5cclxuXHRcdCAqL1xyXG5cdFx0ZGlzYWJsZTogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0c2VsZi4kaW5wdXQucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcclxuXHRcdFx0c2VsZi4kY29udHJvbF9pbnB1dC5wcm9wKCdkaXNhYmxlZCcsIHRydWUpLnByb3AoJ3RhYmluZGV4JywgLTEpO1xyXG5cdFx0XHRzZWxmLmlzRGlzYWJsZWQgPSB0cnVlO1xyXG5cdFx0XHRzZWxmLmxvY2soKTtcclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIEVuYWJsZXMgdGhlIGNvbnRyb2wgc28gdGhhdCBpdCBjYW4gcmVzcG9uZFxyXG5cdFx0ICogdG8gZm9jdXMgYW5kIHVzZXIgaW5wdXQuXHJcblx0XHQgKi9cclxuXHRcdGVuYWJsZTogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0c2VsZi4kaW5wdXQucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XHJcblx0XHRcdHNlbGYuJGNvbnRyb2xfaW5wdXQucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSkucHJvcCgndGFiaW5kZXgnLCBzZWxmLnRhYkluZGV4KTtcclxuXHRcdFx0c2VsZi5pc0Rpc2FibGVkID0gZmFsc2U7XHJcblx0XHRcdHNlbGYudW5sb2NrKCk7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBDb21wbGV0ZWx5IGRlc3Ryb3lzIHRoZSBjb250cm9sIGFuZFxyXG5cdFx0ICogdW5iaW5kcyBhbGwgZXZlbnQgbGlzdGVuZXJzIHNvIHRoYXQgaXQgY2FuXHJcblx0XHQgKiBiZSBnYXJiYWdlIGNvbGxlY3RlZC5cclxuXHRcdCAqL1xyXG5cdFx0ZGVzdHJveTogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0dmFyIGV2ZW50TlMgPSBzZWxmLmV2ZW50TlM7XHJcblx0XHRcdHZhciByZXZlcnRTZXR0aW5ncyA9IHNlbGYucmV2ZXJ0U2V0dGluZ3M7XHJcblx0XHJcblx0XHRcdHNlbGYudHJpZ2dlcignZGVzdHJveScpO1xyXG5cdFx0XHRzZWxmLm9mZigpO1xyXG5cdFx0XHRzZWxmLiR3cmFwcGVyLnJlbW92ZSgpO1xyXG5cdFx0XHRzZWxmLiRkcm9wZG93bi5yZW1vdmUoKTtcclxuXHRcclxuXHRcdFx0c2VsZi4kaW5wdXRcclxuXHRcdFx0XHQuaHRtbCgnJylcclxuXHRcdFx0XHQuYXBwZW5kKHJldmVydFNldHRpbmdzLiRjaGlsZHJlbilcclxuXHRcdFx0XHQucmVtb3ZlQXR0cigndGFiaW5kZXgnKVxyXG5cdFx0XHRcdC5yZW1vdmVDbGFzcygnc2VsZWN0aXplZCcpXHJcblx0XHRcdFx0LmF0dHIoe3RhYmluZGV4OiByZXZlcnRTZXR0aW5ncy50YWJpbmRleH0pXHJcblx0XHRcdFx0LnNob3coKTtcclxuXHRcclxuXHRcdFx0c2VsZi4kY29udHJvbF9pbnB1dC5yZW1vdmVEYXRhKCdncm93Jyk7XHJcblx0XHRcdHNlbGYuJGlucHV0LnJlbW92ZURhdGEoJ3NlbGVjdGl6ZScpO1xyXG5cdFxyXG5cdFx0XHQkKHdpbmRvdykub2ZmKGV2ZW50TlMpO1xyXG5cdFx0XHQkKGRvY3VtZW50KS5vZmYoZXZlbnROUyk7XHJcblx0XHRcdCQoZG9jdW1lbnQuYm9keSkub2ZmKGV2ZW50TlMpO1xyXG5cdFxyXG5cdFx0XHRkZWxldGUgc2VsZi4kaW5wdXRbMF0uc2VsZWN0aXplO1xyXG5cdFx0fSxcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogQSBoZWxwZXIgbWV0aG9kIGZvciByZW5kZXJpbmcgXCJpdGVtXCIgYW5kXHJcblx0XHQgKiBcIm9wdGlvblwiIHRlbXBsYXRlcywgZ2l2ZW4gdGhlIGRhdGEuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IHRlbXBsYXRlTmFtZVxyXG5cdFx0ICogQHBhcmFtIHtvYmplY3R9IGRhdGFcclxuXHRcdCAqIEByZXR1cm5zIHtzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdHJlbmRlcjogZnVuY3Rpb24odGVtcGxhdGVOYW1lLCBkYXRhKSB7XHJcblx0XHRcdHZhciB2YWx1ZSwgaWQsIGxhYmVsO1xyXG5cdFx0XHR2YXIgaHRtbCA9ICcnO1xyXG5cdFx0XHR2YXIgY2FjaGUgPSBmYWxzZTtcclxuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHR2YXIgcmVnZXhfdGFnID0gL15bXFx0IFxcclxcbl0qPChbYS16XVthLXowLTlcXC1fXSooPzpcXDpbYS16XVthLXowLTlcXC1fXSopPykvaTtcclxuXHRcclxuXHRcdFx0aWYgKHRlbXBsYXRlTmFtZSA9PT0gJ29wdGlvbicgfHwgdGVtcGxhdGVOYW1lID09PSAnaXRlbScpIHtcclxuXHRcdFx0XHR2YWx1ZSA9IGhhc2hfa2V5KGRhdGFbc2VsZi5zZXR0aW5ncy52YWx1ZUZpZWxkXSk7XHJcblx0XHRcdFx0Y2FjaGUgPSAhIXZhbHVlO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIHB1bGwgbWFya3VwIGZyb20gY2FjaGUgaWYgaXQgZXhpc3RzXHJcblx0XHRcdGlmIChjYWNoZSkge1xyXG5cdFx0XHRcdGlmICghaXNzZXQoc2VsZi5yZW5kZXJDYWNoZVt0ZW1wbGF0ZU5hbWVdKSkge1xyXG5cdFx0XHRcdFx0c2VsZi5yZW5kZXJDYWNoZVt0ZW1wbGF0ZU5hbWVdID0ge307XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmIChzZWxmLnJlbmRlckNhY2hlW3RlbXBsYXRlTmFtZV0uaGFzT3duUHJvcGVydHkodmFsdWUpKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gc2VsZi5yZW5kZXJDYWNoZVt0ZW1wbGF0ZU5hbWVdW3ZhbHVlXTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0Ly8gcmVuZGVyIG1hcmt1cFxyXG5cdFx0XHRodG1sID0gc2VsZi5zZXR0aW5ncy5yZW5kZXJbdGVtcGxhdGVOYW1lXS5hcHBseSh0aGlzLCBbZGF0YSwgZXNjYXBlX2h0bWxdKTtcclxuXHRcclxuXHRcdFx0Ly8gYWRkIG1hbmRhdG9yeSBhdHRyaWJ1dGVzXHJcblx0XHRcdGlmICh0ZW1wbGF0ZU5hbWUgPT09ICdvcHRpb24nIHx8IHRlbXBsYXRlTmFtZSA9PT0gJ29wdGlvbl9jcmVhdGUnKSB7XHJcblx0XHRcdFx0aHRtbCA9IGh0bWwucmVwbGFjZShyZWdleF90YWcsICc8JDEgZGF0YS1zZWxlY3RhYmxlJyk7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKHRlbXBsYXRlTmFtZSA9PT0gJ29wdGdyb3VwJykge1xyXG5cdFx0XHRcdGlkID0gZGF0YVtzZWxmLnNldHRpbmdzLm9wdGdyb3VwVmFsdWVGaWVsZF0gfHwgJyc7XHJcblx0XHRcdFx0aHRtbCA9IGh0bWwucmVwbGFjZShyZWdleF90YWcsICc8JDEgZGF0YS1ncm91cD1cIicgKyBlc2NhcGVfcmVwbGFjZShlc2NhcGVfaHRtbChpZCkpICsgJ1wiJyk7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKHRlbXBsYXRlTmFtZSA9PT0gJ29wdGlvbicgfHwgdGVtcGxhdGVOYW1lID09PSAnaXRlbScpIHtcclxuXHRcdFx0XHRodG1sID0gaHRtbC5yZXBsYWNlKHJlZ2V4X3RhZywgJzwkMSBkYXRhLXZhbHVlPVwiJyArIGVzY2FwZV9yZXBsYWNlKGVzY2FwZV9odG1sKHZhbHVlIHx8ICcnKSkgKyAnXCInKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQvLyB1cGRhdGUgY2FjaGVcclxuXHRcdFx0aWYgKGNhY2hlKSB7XHJcblx0XHRcdFx0c2VsZi5yZW5kZXJDYWNoZVt0ZW1wbGF0ZU5hbWVdW3ZhbHVlXSA9IGh0bWw7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0cmV0dXJuIGh0bWw7XHJcblx0XHR9LFxyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBDbGVhcnMgdGhlIHJlbmRlciBjYWNoZSBmb3IgYSB0ZW1wbGF0ZS4gSWZcclxuXHRcdCAqIG5vIHRlbXBsYXRlIGlzIGdpdmVuLCBjbGVhcnMgYWxsIHJlbmRlclxyXG5cdFx0ICogY2FjaGVzLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSB0ZW1wbGF0ZU5hbWVcclxuXHRcdCAqL1xyXG5cdFx0Y2xlYXJDYWNoZTogZnVuY3Rpb24odGVtcGxhdGVOYW1lKSB7XHJcblx0XHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdFx0aWYgKHR5cGVvZiB0ZW1wbGF0ZU5hbWUgPT09ICd1bmRlZmluZWQnKSB7XHJcblx0XHRcdFx0c2VsZi5yZW5kZXJDYWNoZSA9IHt9O1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGRlbGV0ZSBzZWxmLnJlbmRlckNhY2hlW3RlbXBsYXRlTmFtZV07XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHJcblx0XHQvKipcclxuXHRcdCAqIERldGVybWluZXMgd2hldGhlciBvciBub3QgdG8gZGlzcGxheSB0aGVcclxuXHRcdCAqIGNyZWF0ZSBpdGVtIHByb21wdCwgZ2l2ZW4gYSB1c2VyIGlucHV0LlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBpbnB1dFxyXG5cdFx0ICogQHJldHVybiB7Ym9vbGVhbn1cclxuXHRcdCAqL1xyXG5cdFx0Y2FuQ3JlYXRlOiBmdW5jdGlvbihpbnB1dCkge1xyXG5cdFx0XHR2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHRcdGlmICghc2VsZi5zZXR0aW5ncy5jcmVhdGUpIHJldHVybiBmYWxzZTtcclxuXHRcdFx0dmFyIGZpbHRlciA9IHNlbGYuc2V0dGluZ3MuY3JlYXRlRmlsdGVyO1xyXG5cdFx0XHRyZXR1cm4gaW5wdXQubGVuZ3RoXHJcblx0XHRcdFx0JiYgKHR5cGVvZiBmaWx0ZXIgIT09ICdmdW5jdGlvbicgfHwgZmlsdGVyLmFwcGx5KHNlbGYsIFtpbnB1dF0pKVxyXG5cdFx0XHRcdCYmICh0eXBlb2YgZmlsdGVyICE9PSAnc3RyaW5nJyB8fCBuZXcgUmVnRXhwKGZpbHRlcikudGVzdChpbnB1dCkpXHJcblx0XHRcdFx0JiYgKCEoZmlsdGVyIGluc3RhbmNlb2YgUmVnRXhwKSB8fCBmaWx0ZXIudGVzdChpbnB1dCkpO1xyXG5cdFx0fVxyXG5cdFxyXG5cdH0pO1xyXG5cdFxyXG5cdFxyXG5cdFNlbGVjdGl6ZS5jb3VudCA9IDA7XHJcblx0U2VsZWN0aXplLmRlZmF1bHRzID0ge1xyXG5cdFx0b3B0aW9uczogW10sXHJcblx0XHRvcHRncm91cHM6IFtdLFxyXG5cdFxyXG5cdFx0cGx1Z2luczogW10sXHJcblx0XHRkZWxpbWl0ZXI6ICcsJyxcclxuXHRcdHNwbGl0T246IG51bGwsIC8vIHJlZ2V4cCBvciBzdHJpbmcgZm9yIHNwbGl0dGluZyB1cCB2YWx1ZXMgZnJvbSBhIHBhc3RlIGNvbW1hbmRcclxuXHRcdHBlcnNpc3Q6IHRydWUsXHJcblx0XHRkaWFjcml0aWNzOiB0cnVlLFxyXG5cdFx0Y3JlYXRlOiBmYWxzZSxcclxuXHRcdGNyZWF0ZU9uQmx1cjogZmFsc2UsXHJcblx0XHRjcmVhdGVGaWx0ZXI6IG51bGwsXHJcblx0XHRoaWdobGlnaHQ6IHRydWUsXHJcblx0XHRvcGVuT25Gb2N1czogdHJ1ZSxcclxuXHRcdG1heE9wdGlvbnM6IDEwMDAsXHJcblx0XHRtYXhJdGVtczogbnVsbCxcclxuXHRcdGhpZGVTZWxlY3RlZDogbnVsbCxcclxuXHRcdGFkZFByZWNlZGVuY2U6IGZhbHNlLFxyXG5cdFx0c2VsZWN0T25UYWI6IGZhbHNlLFxyXG5cdFx0cHJlbG9hZDogZmFsc2UsXHJcblx0XHRhbGxvd0VtcHR5T3B0aW9uOiBmYWxzZSxcclxuXHRcdGNsb3NlQWZ0ZXJTZWxlY3Q6IGZhbHNlLFxyXG5cdFxyXG5cdFx0c2Nyb2xsRHVyYXRpb246IDYwLFxyXG5cdFx0bG9hZFRocm90dGxlOiAzMDAsXHJcblx0XHRsb2FkaW5nQ2xhc3M6ICdsb2FkaW5nJyxcclxuXHRcclxuXHRcdGRhdGFBdHRyOiAnZGF0YS1kYXRhJyxcclxuXHRcdG9wdGdyb3VwRmllbGQ6ICdvcHRncm91cCcsXHJcblx0XHR2YWx1ZUZpZWxkOiAndmFsdWUnLFxyXG5cdFx0bGFiZWxGaWVsZDogJ3RleHQnLFxyXG5cdFx0b3B0Z3JvdXBMYWJlbEZpZWxkOiAnbGFiZWwnLFxyXG5cdFx0b3B0Z3JvdXBWYWx1ZUZpZWxkOiAndmFsdWUnLFxyXG5cdFx0bG9ja09wdGdyb3VwT3JkZXI6IGZhbHNlLFxyXG5cdFxyXG5cdFx0c29ydEZpZWxkOiAnJG9yZGVyJyxcclxuXHRcdHNlYXJjaEZpZWxkOiBbJ3RleHQnXSxcclxuXHRcdHNlYXJjaENvbmp1bmN0aW9uOiAnYW5kJyxcclxuXHRcclxuXHRcdG1vZGU6IG51bGwsXHJcblx0XHR3cmFwcGVyQ2xhc3M6ICdzZWxlY3RpemUtY29udHJvbCcsXHJcblx0XHRpbnB1dENsYXNzOiAnc2VsZWN0aXplLWlucHV0JyxcclxuXHRcdGRyb3Bkb3duQ2xhc3M6ICdzZWxlY3RpemUtZHJvcGRvd24nLFxyXG5cdFx0ZHJvcGRvd25Db250ZW50Q2xhc3M6ICdzZWxlY3RpemUtZHJvcGRvd24tY29udGVudCcsXHJcblx0XHJcblx0XHRkcm9wZG93blBhcmVudDogbnVsbCxcclxuXHRcclxuXHRcdGNvcHlDbGFzc2VzVG9Ecm9wZG93bjogdHJ1ZSxcclxuXHRcclxuXHRcdC8qXHJcblx0XHRsb2FkICAgICAgICAgICAgICAgICA6IG51bGwsIC8vIGZ1bmN0aW9uKHF1ZXJ5LCBjYWxsYmFjaykgeyAuLi4gfVxyXG5cdFx0c2NvcmUgICAgICAgICAgICAgICAgOiBudWxsLCAvLyBmdW5jdGlvbihzZWFyY2gpIHsgLi4uIH1cclxuXHRcdG9uSW5pdGlhbGl6ZSAgICAgICAgIDogbnVsbCwgLy8gZnVuY3Rpb24oKSB7IC4uLiB9XHJcblx0XHRvbkNoYW5nZSAgICAgICAgICAgICA6IG51bGwsIC8vIGZ1bmN0aW9uKHZhbHVlKSB7IC4uLiB9XHJcblx0XHRvbkl0ZW1BZGQgICAgICAgICAgICA6IG51bGwsIC8vIGZ1bmN0aW9uKHZhbHVlLCAkaXRlbSkgeyAuLi4gfVxyXG5cdFx0b25JdGVtUmVtb3ZlICAgICAgICAgOiBudWxsLCAvLyBmdW5jdGlvbih2YWx1ZSkgeyAuLi4gfVxyXG5cdFx0b25DbGVhciAgICAgICAgICAgICAgOiBudWxsLCAvLyBmdW5jdGlvbigpIHsgLi4uIH1cclxuXHRcdG9uT3B0aW9uQWRkICAgICAgICAgIDogbnVsbCwgLy8gZnVuY3Rpb24odmFsdWUsIGRhdGEpIHsgLi4uIH1cclxuXHRcdG9uT3B0aW9uUmVtb3ZlICAgICAgIDogbnVsbCwgLy8gZnVuY3Rpb24odmFsdWUpIHsgLi4uIH1cclxuXHRcdG9uT3B0aW9uQ2xlYXIgICAgICAgIDogbnVsbCwgLy8gZnVuY3Rpb24oKSB7IC4uLiB9XHJcblx0XHRvbk9wdGlvbkdyb3VwQWRkICAgICA6IG51bGwsIC8vIGZ1bmN0aW9uKGlkLCBkYXRhKSB7IC4uLiB9XHJcblx0XHRvbk9wdGlvbkdyb3VwUmVtb3ZlICA6IG51bGwsIC8vIGZ1bmN0aW9uKGlkKSB7IC4uLiB9XHJcblx0XHRvbk9wdGlvbkdyb3VwQ2xlYXIgICA6IG51bGwsIC8vIGZ1bmN0aW9uKCkgeyAuLi4gfVxyXG5cdFx0b25Ecm9wZG93bk9wZW4gICAgICAgOiBudWxsLCAvLyBmdW5jdGlvbigkZHJvcGRvd24pIHsgLi4uIH1cclxuXHRcdG9uRHJvcGRvd25DbG9zZSAgICAgIDogbnVsbCwgLy8gZnVuY3Rpb24oJGRyb3Bkb3duKSB7IC4uLiB9XHJcblx0XHRvblR5cGUgICAgICAgICAgICAgICA6IG51bGwsIC8vIGZ1bmN0aW9uKHN0cikgeyAuLi4gfVxyXG5cdFx0b25EZWxldGUgICAgICAgICAgICAgOiBudWxsLCAvLyBmdW5jdGlvbih2YWx1ZXMpIHsgLi4uIH1cclxuXHRcdCovXHJcblx0XHJcblx0XHRyZW5kZXI6IHtcclxuXHRcdFx0LypcclxuXHRcdFx0aXRlbTogbnVsbCxcclxuXHRcdFx0b3B0Z3JvdXA6IG51bGwsXHJcblx0XHRcdG9wdGdyb3VwX2hlYWRlcjogbnVsbCxcclxuXHRcdFx0b3B0aW9uOiBudWxsLFxyXG5cdFx0XHRvcHRpb25fY3JlYXRlOiBudWxsXHJcblx0XHRcdCovXHJcblx0XHR9XHJcblx0fTtcclxuXHRcclxuXHRcclxuXHQkLmZuLnNlbGVjdGl6ZSA9IGZ1bmN0aW9uKHNldHRpbmdzX3VzZXIpIHtcclxuXHRcdHZhciBkZWZhdWx0cyAgICAgICAgICAgICA9ICQuZm4uc2VsZWN0aXplLmRlZmF1bHRzO1xyXG5cdFx0dmFyIHNldHRpbmdzICAgICAgICAgICAgID0gJC5leHRlbmQoe30sIGRlZmF1bHRzLCBzZXR0aW5nc191c2VyKTtcclxuXHRcdHZhciBhdHRyX2RhdGEgICAgICAgICAgICA9IHNldHRpbmdzLmRhdGFBdHRyO1xyXG5cdFx0dmFyIGZpZWxkX2xhYmVsICAgICAgICAgID0gc2V0dGluZ3MubGFiZWxGaWVsZDtcclxuXHRcdHZhciBmaWVsZF92YWx1ZSAgICAgICAgICA9IHNldHRpbmdzLnZhbHVlRmllbGQ7XHJcblx0XHR2YXIgZmllbGRfb3B0Z3JvdXAgICAgICAgPSBzZXR0aW5ncy5vcHRncm91cEZpZWxkO1xyXG5cdFx0dmFyIGZpZWxkX29wdGdyb3VwX2xhYmVsID0gc2V0dGluZ3Mub3B0Z3JvdXBMYWJlbEZpZWxkO1xyXG5cdFx0dmFyIGZpZWxkX29wdGdyb3VwX3ZhbHVlID0gc2V0dGluZ3Mub3B0Z3JvdXBWYWx1ZUZpZWxkO1xyXG5cdFxyXG5cdFx0LyoqXHJcblx0XHQgKiBJbml0aWFsaXplcyBzZWxlY3RpemUgZnJvbSBhIDxpbnB1dCB0eXBlPVwidGV4dFwiPiBlbGVtZW50LlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSAkaW5wdXRcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBzZXR0aW5nc19lbGVtZW50XHJcblx0XHQgKi9cclxuXHRcdHZhciBpbml0X3RleHRib3ggPSBmdW5jdGlvbigkaW5wdXQsIHNldHRpbmdzX2VsZW1lbnQpIHtcclxuXHRcdFx0dmFyIGksIG4sIHZhbHVlcywgb3B0aW9uO1xyXG5cdFxyXG5cdFx0XHR2YXIgZGF0YV9yYXcgPSAkaW5wdXQuYXR0cihhdHRyX2RhdGEpO1xyXG5cdFxyXG5cdFx0XHRpZiAoIWRhdGFfcmF3KSB7XHJcblx0XHRcdFx0dmFyIHZhbHVlID0gJC50cmltKCRpbnB1dC52YWwoKSB8fCAnJyk7XHJcblx0XHRcdFx0aWYgKCFzZXR0aW5ncy5hbGxvd0VtcHR5T3B0aW9uICYmICF2YWx1ZS5sZW5ndGgpIHJldHVybjtcclxuXHRcdFx0XHR2YWx1ZXMgPSB2YWx1ZS5zcGxpdChzZXR0aW5ncy5kZWxpbWl0ZXIpO1xyXG5cdFx0XHRcdGZvciAoaSA9IDAsIG4gPSB2YWx1ZXMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdFx0XHRvcHRpb24gPSB7fTtcclxuXHRcdFx0XHRcdG9wdGlvbltmaWVsZF9sYWJlbF0gPSB2YWx1ZXNbaV07XHJcblx0XHRcdFx0XHRvcHRpb25bZmllbGRfdmFsdWVdID0gdmFsdWVzW2ldO1xyXG5cdFx0XHRcdFx0c2V0dGluZ3NfZWxlbWVudC5vcHRpb25zLnB1c2gob3B0aW9uKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0c2V0dGluZ3NfZWxlbWVudC5pdGVtcyA9IHZhbHVlcztcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRzZXR0aW5nc19lbGVtZW50Lm9wdGlvbnMgPSBKU09OLnBhcnNlKGRhdGFfcmF3KTtcclxuXHRcdFx0XHRmb3IgKGkgPSAwLCBuID0gc2V0dGluZ3NfZWxlbWVudC5vcHRpb25zLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG5cdFx0XHRcdFx0c2V0dGluZ3NfZWxlbWVudC5pdGVtcy5wdXNoKHNldHRpbmdzX2VsZW1lbnQub3B0aW9uc1tpXVtmaWVsZF92YWx1ZV0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogSW5pdGlhbGl6ZXMgc2VsZWN0aXplIGZyb20gYSA8c2VsZWN0PiBlbGVtZW50LlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSAkaW5wdXRcclxuXHRcdCAqIEBwYXJhbSB7b2JqZWN0fSBzZXR0aW5nc19lbGVtZW50XHJcblx0XHQgKi9cclxuXHRcdHZhciBpbml0X3NlbGVjdCA9IGZ1bmN0aW9uKCRpbnB1dCwgc2V0dGluZ3NfZWxlbWVudCkge1xyXG5cdFx0XHR2YXIgaSwgbiwgdGFnTmFtZSwgJGNoaWxkcmVuLCBvcmRlciA9IDA7XHJcblx0XHRcdHZhciBvcHRpb25zID0gc2V0dGluZ3NfZWxlbWVudC5vcHRpb25zO1xyXG5cdFx0XHR2YXIgb3B0aW9uc01hcCA9IHt9O1xyXG5cdFxyXG5cdFx0XHR2YXIgcmVhZERhdGEgPSBmdW5jdGlvbigkZWwpIHtcclxuXHRcdFx0XHR2YXIgZGF0YSA9IGF0dHJfZGF0YSAmJiAkZWwuYXR0cihhdHRyX2RhdGEpO1xyXG5cdFx0XHRcdGlmICh0eXBlb2YgZGF0YSA9PT0gJ3N0cmluZycgJiYgZGF0YS5sZW5ndGgpIHtcclxuXHRcdFx0XHRcdHJldHVybiBKU09OLnBhcnNlKGRhdGEpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRyZXR1cm4gbnVsbDtcclxuXHRcdFx0fTtcclxuXHRcclxuXHRcdFx0dmFyIGFkZE9wdGlvbiA9IGZ1bmN0aW9uKCRvcHRpb24sIGdyb3VwKSB7XHJcblx0XHRcdFx0JG9wdGlvbiA9ICQoJG9wdGlvbik7XHJcblx0XHJcblx0XHRcdFx0dmFyIHZhbHVlID0gaGFzaF9rZXkoJG9wdGlvbi5hdHRyKCd2YWx1ZScpKTtcclxuXHRcdFx0XHRpZiAoIXZhbHVlICYmICFzZXR0aW5ncy5hbGxvd0VtcHR5T3B0aW9uKSByZXR1cm47XHJcblx0XHJcblx0XHRcdFx0Ly8gaWYgdGhlIG9wdGlvbiBhbHJlYWR5IGV4aXN0cywgaXQncyBwcm9iYWJseSBiZWVuXHJcblx0XHRcdFx0Ly8gZHVwbGljYXRlZCBpbiBhbm90aGVyIG9wdGdyb3VwLiBpbiB0aGlzIGNhc2UsIHB1c2hcclxuXHRcdFx0XHQvLyB0aGUgY3VycmVudCBncm91cCB0byB0aGUgXCJvcHRncm91cFwiIHByb3BlcnR5IG9uIHRoZVxyXG5cdFx0XHRcdC8vIGV4aXN0aW5nIG9wdGlvbiBzbyB0aGF0IGl0J3MgcmVuZGVyZWQgaW4gYm90aCBwbGFjZXMuXHJcblx0XHRcdFx0aWYgKG9wdGlvbnNNYXAuaGFzT3duUHJvcGVydHkodmFsdWUpKSB7XHJcblx0XHRcdFx0XHRpZiAoZ3JvdXApIHtcclxuXHRcdFx0XHRcdFx0dmFyIGFyciA9IG9wdGlvbnNNYXBbdmFsdWVdW2ZpZWxkX29wdGdyb3VwXTtcclxuXHRcdFx0XHRcdFx0aWYgKCFhcnIpIHtcclxuXHRcdFx0XHRcdFx0XHRvcHRpb25zTWFwW3ZhbHVlXVtmaWVsZF9vcHRncm91cF0gPSBncm91cDtcclxuXHRcdFx0XHRcdFx0fSBlbHNlIGlmICghJC5pc0FycmF5KGFycikpIHtcclxuXHRcdFx0XHRcdFx0XHRvcHRpb25zTWFwW3ZhbHVlXVtmaWVsZF9vcHRncm91cF0gPSBbYXJyLCBncm91cF07XHJcblx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0YXJyLnB1c2goZ3JvdXApO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRcdHZhciBvcHRpb24gICAgICAgICAgICAgPSByZWFkRGF0YSgkb3B0aW9uKSB8fCB7fTtcclxuXHRcdFx0XHRvcHRpb25bZmllbGRfbGFiZWxdICAgID0gb3B0aW9uW2ZpZWxkX2xhYmVsXSB8fCAkb3B0aW9uLnRleHQoKTtcclxuXHRcdFx0XHRvcHRpb25bZmllbGRfdmFsdWVdICAgID0gb3B0aW9uW2ZpZWxkX3ZhbHVlXSB8fCB2YWx1ZTtcclxuXHRcdFx0XHRvcHRpb25bZmllbGRfb3B0Z3JvdXBdID0gb3B0aW9uW2ZpZWxkX29wdGdyb3VwXSB8fCBncm91cDtcclxuXHRcclxuXHRcdFx0XHRvcHRpb25zTWFwW3ZhbHVlXSA9IG9wdGlvbjtcclxuXHRcdFx0XHRvcHRpb25zLnB1c2gob3B0aW9uKTtcclxuXHRcclxuXHRcdFx0XHRpZiAoJG9wdGlvbi5pcygnOnNlbGVjdGVkJykpIHtcclxuXHRcdFx0XHRcdHNldHRpbmdzX2VsZW1lbnQuaXRlbXMucHVzaCh2YWx1ZSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cdFxyXG5cdFx0XHR2YXIgYWRkR3JvdXAgPSBmdW5jdGlvbigkb3B0Z3JvdXApIHtcclxuXHRcdFx0XHR2YXIgaSwgbiwgaWQsIG9wdGdyb3VwLCAkb3B0aW9ucztcclxuXHRcclxuXHRcdFx0XHQkb3B0Z3JvdXAgPSAkKCRvcHRncm91cCk7XHJcblx0XHRcdFx0aWQgPSAkb3B0Z3JvdXAuYXR0cignbGFiZWwnKTtcclxuXHRcclxuXHRcdFx0XHRpZiAoaWQpIHtcclxuXHRcdFx0XHRcdG9wdGdyb3VwID0gcmVhZERhdGEoJG9wdGdyb3VwKSB8fCB7fTtcclxuXHRcdFx0XHRcdG9wdGdyb3VwW2ZpZWxkX29wdGdyb3VwX2xhYmVsXSA9IGlkO1xyXG5cdFx0XHRcdFx0b3B0Z3JvdXBbZmllbGRfb3B0Z3JvdXBfdmFsdWVdID0gaWQ7XHJcblx0XHRcdFx0XHRzZXR0aW5nc19lbGVtZW50Lm9wdGdyb3Vwcy5wdXNoKG9wdGdyb3VwKTtcclxuXHRcdFx0XHR9XHJcblx0XHJcblx0XHRcdFx0JG9wdGlvbnMgPSAkKCdvcHRpb24nLCAkb3B0Z3JvdXApO1xyXG5cdFx0XHRcdGZvciAoaSA9IDAsIG4gPSAkb3B0aW9ucy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuXHRcdFx0XHRcdGFkZE9wdGlvbigkb3B0aW9uc1tpXSwgaWQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHRcclxuXHRcdFx0c2V0dGluZ3NfZWxlbWVudC5tYXhJdGVtcyA9ICRpbnB1dC5hdHRyKCdtdWx0aXBsZScpID8gbnVsbCA6IDE7XHJcblx0XHJcblx0XHRcdCRjaGlsZHJlbiA9ICRpbnB1dC5jaGlsZHJlbigpO1xyXG5cdFx0XHRmb3IgKGkgPSAwLCBuID0gJGNoaWxkcmVuLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG5cdFx0XHRcdHRhZ05hbWUgPSAkY2hpbGRyZW5baV0udGFnTmFtZS50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0XHRcdGlmICh0YWdOYW1lID09PSAnb3B0Z3JvdXAnKSB7XHJcblx0XHRcdFx0XHRhZGRHcm91cCgkY2hpbGRyZW5baV0pO1xyXG5cdFx0XHRcdH0gZWxzZSBpZiAodGFnTmFtZSA9PT0gJ29wdGlvbicpIHtcclxuXHRcdFx0XHRcdGFkZE9wdGlvbigkY2hpbGRyZW5baV0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcclxuXHRcdHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKSB7XHJcblx0XHRcdGlmICh0aGlzLnNlbGVjdGl6ZSkgcmV0dXJuO1xyXG5cdFxyXG5cdFx0XHR2YXIgaW5zdGFuY2U7XHJcblx0XHRcdHZhciAkaW5wdXQgPSAkKHRoaXMpO1xyXG5cdFx0XHR2YXIgdGFnX25hbWUgPSB0aGlzLnRhZ05hbWUudG9Mb3dlckNhc2UoKTtcclxuXHRcdFx0dmFyIHBsYWNlaG9sZGVyID0gJGlucHV0LmF0dHIoJ3BsYWNlaG9sZGVyJykgfHwgJGlucHV0LmF0dHIoJ2RhdGEtcGxhY2Vob2xkZXInKTtcclxuXHRcdFx0aWYgKCFwbGFjZWhvbGRlciAmJiAhc2V0dGluZ3MuYWxsb3dFbXB0eU9wdGlvbikge1xyXG5cdFx0XHRcdHBsYWNlaG9sZGVyID0gJGlucHV0LmNoaWxkcmVuKCdvcHRpb25bdmFsdWU9XCJcIl0nKS50ZXh0KCk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0dmFyIHNldHRpbmdzX2VsZW1lbnQgPSB7XHJcblx0XHRcdFx0J3BsYWNlaG9sZGVyJyA6IHBsYWNlaG9sZGVyLFxyXG5cdFx0XHRcdCdvcHRpb25zJyAgICAgOiBbXSxcclxuXHRcdFx0XHQnb3B0Z3JvdXBzJyAgIDogW10sXHJcblx0XHRcdFx0J2l0ZW1zJyAgICAgICA6IFtdXHJcblx0XHRcdH07XHJcblx0XHJcblx0XHRcdGlmICh0YWdfbmFtZSA9PT0gJ3NlbGVjdCcpIHtcclxuXHRcdFx0XHRpbml0X3NlbGVjdCgkaW5wdXQsIHNldHRpbmdzX2VsZW1lbnQpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGluaXRfdGV4dGJveCgkaW5wdXQsIHNldHRpbmdzX2VsZW1lbnQpO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdGluc3RhbmNlID0gbmV3IFNlbGVjdGl6ZSgkaW5wdXQsICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgc2V0dGluZ3NfZWxlbWVudCwgc2V0dGluZ3NfdXNlcikpO1xyXG5cdFx0fSk7XHJcblx0fTtcclxuXHRcclxuXHQkLmZuLnNlbGVjdGl6ZS5kZWZhdWx0cyA9IFNlbGVjdGl6ZS5kZWZhdWx0cztcclxuXHQkLmZuLnNlbGVjdGl6ZS5zdXBwb3J0ID0ge1xyXG5cdFx0dmFsaWRpdHk6IFNVUFBPUlRTX1ZBTElESVRZX0FQSVxyXG5cdH07XHJcblx0XHJcblx0XHJcblx0U2VsZWN0aXplLmRlZmluZSgnZHJhZ19kcm9wJywgZnVuY3Rpb24ob3B0aW9ucykge1xyXG5cdFx0aWYgKCEkLmZuLnNvcnRhYmxlKSB0aHJvdyBuZXcgRXJyb3IoJ1RoZSBcImRyYWdfZHJvcFwiIHBsdWdpbiByZXF1aXJlcyBqUXVlcnkgVUkgXCJzb3J0YWJsZVwiLicpO1xyXG5cdFx0aWYgKHRoaXMuc2V0dGluZ3MubW9kZSAhPT0gJ211bHRpJykgcmV0dXJuO1xyXG5cdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdFx0c2VsZi5sb2NrID0gKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgb3JpZ2luYWwgPSBzZWxmLmxvY2s7XHJcblx0XHRcdHJldHVybiBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHR2YXIgc29ydGFibGUgPSBzZWxmLiRjb250cm9sLmRhdGEoJ3NvcnRhYmxlJyk7XHJcblx0XHRcdFx0aWYgKHNvcnRhYmxlKSBzb3J0YWJsZS5kaXNhYmxlKCk7XHJcblx0XHRcdFx0cmV0dXJuIG9yaWdpbmFsLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7XHJcblx0XHRcdH07XHJcblx0XHR9KSgpO1xyXG5cdFxyXG5cdFx0c2VsZi51bmxvY2sgPSAoZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBvcmlnaW5hbCA9IHNlbGYudW5sb2NrO1xyXG5cdFx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0dmFyIHNvcnRhYmxlID0gc2VsZi4kY29udHJvbC5kYXRhKCdzb3J0YWJsZScpO1xyXG5cdFx0XHRcdGlmIChzb3J0YWJsZSkgc29ydGFibGUuZW5hYmxlKCk7XHJcblx0XHRcdFx0cmV0dXJuIG9yaWdpbmFsLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7XHJcblx0XHRcdH07XHJcblx0XHR9KSgpO1xyXG5cdFxyXG5cdFx0c2VsZi5zZXR1cCA9IChmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIG9yaWdpbmFsID0gc2VsZi5zZXR1cDtcclxuXHRcdFx0cmV0dXJuIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdG9yaWdpbmFsLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcblx0XHJcblx0XHRcdFx0dmFyICRjb250cm9sID0gc2VsZi4kY29udHJvbC5zb3J0YWJsZSh7XHJcblx0XHRcdFx0XHRpdGVtczogJ1tkYXRhLXZhbHVlXScsXHJcblx0XHRcdFx0XHRmb3JjZVBsYWNlaG9sZGVyU2l6ZTogdHJ1ZSxcclxuXHRcdFx0XHRcdGRpc2FibGVkOiBzZWxmLmlzTG9ja2VkLFxyXG5cdFx0XHRcdFx0c3RhcnQ6IGZ1bmN0aW9uKGUsIHVpKSB7XHJcblx0XHRcdFx0XHRcdHVpLnBsYWNlaG9sZGVyLmNzcygnd2lkdGgnLCB1aS5oZWxwZXIuY3NzKCd3aWR0aCcpKTtcclxuXHRcdFx0XHRcdFx0JGNvbnRyb2wuY3NzKHtvdmVyZmxvdzogJ3Zpc2libGUnfSk7XHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0c3RvcDogZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0XHRcdCRjb250cm9sLmNzcyh7b3ZlcmZsb3c6ICdoaWRkZW4nfSk7XHJcblx0XHRcdFx0XHRcdHZhciBhY3RpdmUgPSBzZWxmLiRhY3RpdmVJdGVtcyA/IHNlbGYuJGFjdGl2ZUl0ZW1zLnNsaWNlKCkgOiBudWxsO1xyXG5cdFx0XHRcdFx0XHR2YXIgdmFsdWVzID0gW107XHJcblx0XHRcdFx0XHRcdCRjb250cm9sLmNoaWxkcmVuKCdbZGF0YS12YWx1ZV0nKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRcdHZhbHVlcy5wdXNoKCQodGhpcykuYXR0cignZGF0YS12YWx1ZScpKTtcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRcdHNlbGYuc2V0VmFsdWUodmFsdWVzKTtcclxuXHRcdFx0XHRcdFx0c2VsZi5zZXRBY3RpdmVJdGVtKGFjdGl2ZSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH07XHJcblx0XHR9KSgpO1xyXG5cdFxyXG5cdH0pO1xyXG5cdFxyXG5cdFNlbGVjdGl6ZS5kZWZpbmUoJ2Ryb3Bkb3duX2hlYWRlcicsIGZ1bmN0aW9uKG9wdGlvbnMpIHtcclxuXHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcclxuXHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh7XHJcblx0XHRcdHRpdGxlICAgICAgICAgOiAnVW50aXRsZWQnLFxyXG5cdFx0XHRoZWFkZXJDbGFzcyAgIDogJ3NlbGVjdGl6ZS1kcm9wZG93bi1oZWFkZXInLFxyXG5cdFx0XHR0aXRsZVJvd0NsYXNzIDogJ3NlbGVjdGl6ZS1kcm9wZG93bi1oZWFkZXItdGl0bGUnLFxyXG5cdFx0XHRsYWJlbENsYXNzICAgIDogJ3NlbGVjdGl6ZS1kcm9wZG93bi1oZWFkZXItbGFiZWwnLFxyXG5cdFx0XHRjbG9zZUNsYXNzICAgIDogJ3NlbGVjdGl6ZS1kcm9wZG93bi1oZWFkZXItY2xvc2UnLFxyXG5cdFxyXG5cdFx0XHRodG1sOiBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcdFx0cmV0dXJuIChcclxuXHRcdFx0XHRcdCc8ZGl2IGNsYXNzPVwiJyArIGRhdGEuaGVhZGVyQ2xhc3MgKyAnXCI+JyArXHJcblx0XHRcdFx0XHRcdCc8ZGl2IGNsYXNzPVwiJyArIGRhdGEudGl0bGVSb3dDbGFzcyArICdcIj4nICtcclxuXHRcdFx0XHRcdFx0XHQnPHNwYW4gY2xhc3M9XCInICsgZGF0YS5sYWJlbENsYXNzICsgJ1wiPicgKyBkYXRhLnRpdGxlICsgJzwvc3Bhbj4nICtcclxuXHRcdFx0XHRcdFx0XHQnPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGNsYXNzPVwiJyArIGRhdGEuY2xvc2VDbGFzcyArICdcIj4mdGltZXM7PC9hPicgK1xyXG5cdFx0XHRcdFx0XHQnPC9kaXY+JyArXHJcblx0XHRcdFx0XHQnPC9kaXY+J1xyXG5cdFx0XHRcdCk7XHJcblx0XHRcdH1cclxuXHRcdH0sIG9wdGlvbnMpO1xyXG5cdFxyXG5cdFx0c2VsZi5zZXR1cCA9IChmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIG9yaWdpbmFsID0gc2VsZi5zZXR1cDtcclxuXHRcdFx0cmV0dXJuIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdG9yaWdpbmFsLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7XHJcblx0XHRcdFx0c2VsZi4kZHJvcGRvd25faGVhZGVyID0gJChvcHRpb25zLmh0bWwob3B0aW9ucykpO1xyXG5cdFx0XHRcdHNlbGYuJGRyb3Bkb3duLnByZXBlbmQoc2VsZi4kZHJvcGRvd25faGVhZGVyKTtcclxuXHRcdFx0fTtcclxuXHRcdH0pKCk7XHJcblx0XHJcblx0fSk7XHJcblx0XHJcblx0U2VsZWN0aXplLmRlZmluZSgnb3B0Z3JvdXBfY29sdW1ucycsIGZ1bmN0aW9uKG9wdGlvbnMpIHtcclxuXHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcclxuXHRcdG9wdGlvbnMgPSAkLmV4dGVuZCh7XHJcblx0XHRcdGVxdWFsaXplV2lkdGggIDogdHJ1ZSxcclxuXHRcdFx0ZXF1YWxpemVIZWlnaHQgOiB0cnVlXHJcblx0XHR9LCBvcHRpb25zKTtcclxuXHRcclxuXHRcdHRoaXMuZ2V0QWRqYWNlbnRPcHRpb24gPSBmdW5jdGlvbigkb3B0aW9uLCBkaXJlY3Rpb24pIHtcclxuXHRcdFx0dmFyICRvcHRpb25zID0gJG9wdGlvbi5jbG9zZXN0KCdbZGF0YS1ncm91cF0nKS5maW5kKCdbZGF0YS1zZWxlY3RhYmxlXScpO1xyXG5cdFx0XHR2YXIgaW5kZXggICAgPSAkb3B0aW9ucy5pbmRleCgkb3B0aW9uKSArIGRpcmVjdGlvbjtcclxuXHRcclxuXHRcdFx0cmV0dXJuIGluZGV4ID49IDAgJiYgaW5kZXggPCAkb3B0aW9ucy5sZW5ndGggPyAkb3B0aW9ucy5lcShpbmRleCkgOiAkKCk7XHJcblx0XHR9O1xyXG5cdFxyXG5cdFx0dGhpcy5vbktleURvd24gPSAoZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBvcmlnaW5hbCA9IHNlbGYub25LZXlEb3duO1xyXG5cdFx0XHRyZXR1cm4gZnVuY3Rpb24oZSkge1xyXG5cdFx0XHRcdHZhciBpbmRleCwgJG9wdGlvbiwgJG9wdGlvbnMsICRvcHRncm91cDtcclxuXHRcclxuXHRcdFx0XHRpZiAodGhpcy5pc09wZW4gJiYgKGUua2V5Q29kZSA9PT0gS0VZX0xFRlQgfHwgZS5rZXlDb2RlID09PSBLRVlfUklHSFQpKSB7XHJcblx0XHRcdFx0XHRzZWxmLmlnbm9yZUhvdmVyID0gdHJ1ZTtcclxuXHRcdFx0XHRcdCRvcHRncm91cCA9IHRoaXMuJGFjdGl2ZU9wdGlvbi5jbG9zZXN0KCdbZGF0YS1ncm91cF0nKTtcclxuXHRcdFx0XHRcdGluZGV4ID0gJG9wdGdyb3VwLmZpbmQoJ1tkYXRhLXNlbGVjdGFibGVdJykuaW5kZXgodGhpcy4kYWN0aXZlT3B0aW9uKTtcclxuXHRcclxuXHRcdFx0XHRcdGlmKGUua2V5Q29kZSA9PT0gS0VZX0xFRlQpIHtcclxuXHRcdFx0XHRcdFx0JG9wdGdyb3VwID0gJG9wdGdyb3VwLnByZXYoJ1tkYXRhLWdyb3VwXScpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0JG9wdGdyb3VwID0gJG9wdGdyb3VwLm5leHQoJ1tkYXRhLWdyb3VwXScpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRcdFx0JG9wdGlvbnMgPSAkb3B0Z3JvdXAuZmluZCgnW2RhdGEtc2VsZWN0YWJsZV0nKTtcclxuXHRcdFx0XHRcdCRvcHRpb24gID0gJG9wdGlvbnMuZXEoTWF0aC5taW4oJG9wdGlvbnMubGVuZ3RoIC0gMSwgaW5kZXgpKTtcclxuXHRcdFx0XHRcdGlmICgkb3B0aW9uLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHR0aGlzLnNldEFjdGl2ZU9wdGlvbigkb3B0aW9uKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHR9XHJcblx0XHJcblx0XHRcdFx0cmV0dXJuIG9yaWdpbmFsLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcblx0XHRcdH07XHJcblx0XHR9KSgpO1xyXG5cdFxyXG5cdFx0dmFyIGdldFNjcm9sbGJhcldpZHRoID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBkaXY7XHJcblx0XHRcdHZhciB3aWR0aCA9IGdldFNjcm9sbGJhcldpZHRoLndpZHRoO1xyXG5cdFx0XHR2YXIgZG9jID0gZG9jdW1lbnQ7XHJcblx0XHJcblx0XHRcdGlmICh0eXBlb2Ygd2lkdGggPT09ICd1bmRlZmluZWQnKSB7XHJcblx0XHRcdFx0ZGl2ID0gZG9jLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG5cdFx0XHRcdGRpdi5pbm5lckhUTUwgPSAnPGRpdiBzdHlsZT1cIndpZHRoOjUwcHg7aGVpZ2h0OjUwcHg7cG9zaXRpb246YWJzb2x1dGU7bGVmdDotNTBweDt0b3A6LTUwcHg7b3ZlcmZsb3c6YXV0bztcIj48ZGl2IHN0eWxlPVwid2lkdGg6MXB4O2hlaWdodDoxMDBweDtcIj48L2Rpdj48L2Rpdj4nO1xyXG5cdFx0XHRcdGRpdiA9IGRpdi5maXJzdENoaWxkO1xyXG5cdFx0XHRcdGRvYy5ib2R5LmFwcGVuZENoaWxkKGRpdik7XHJcblx0XHRcdFx0d2lkdGggPSBnZXRTY3JvbGxiYXJXaWR0aC53aWR0aCA9IGRpdi5vZmZzZXRXaWR0aCAtIGRpdi5jbGllbnRXaWR0aDtcclxuXHRcdFx0XHRkb2MuYm9keS5yZW1vdmVDaGlsZChkaXYpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiB3aWR0aDtcclxuXHRcdH07XHJcblx0XHJcblx0XHR2YXIgZXF1YWxpemVTaXplcyA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgaSwgbiwgaGVpZ2h0X21heCwgd2lkdGgsIHdpZHRoX2xhc3QsIHdpZHRoX3BhcmVudCwgJG9wdGdyb3VwcztcclxuXHRcclxuXHRcdFx0JG9wdGdyb3VwcyA9ICQoJ1tkYXRhLWdyb3VwXScsIHNlbGYuJGRyb3Bkb3duX2NvbnRlbnQpO1xyXG5cdFx0XHRuID0gJG9wdGdyb3Vwcy5sZW5ndGg7XHJcblx0XHRcdGlmICghbiB8fCAhc2VsZi4kZHJvcGRvd25fY29udGVudC53aWR0aCgpKSByZXR1cm47XHJcblx0XHJcblx0XHRcdGlmIChvcHRpb25zLmVxdWFsaXplSGVpZ2h0KSB7XHJcblx0XHRcdFx0aGVpZ2h0X21heCA9IDA7XHJcblx0XHRcdFx0Zm9yIChpID0gMDsgaSA8IG47IGkrKykge1xyXG5cdFx0XHRcdFx0aGVpZ2h0X21heCA9IE1hdGgubWF4KGhlaWdodF9tYXgsICRvcHRncm91cHMuZXEoaSkuaGVpZ2h0KCkpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHQkb3B0Z3JvdXBzLmNzcyh7aGVpZ2h0OiBoZWlnaHRfbWF4fSk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0aWYgKG9wdGlvbnMuZXF1YWxpemVXaWR0aCkge1xyXG5cdFx0XHRcdHdpZHRoX3BhcmVudCA9IHNlbGYuJGRyb3Bkb3duX2NvbnRlbnQuaW5uZXJXaWR0aCgpIC0gZ2V0U2Nyb2xsYmFyV2lkdGgoKTtcclxuXHRcdFx0XHR3aWR0aCA9IE1hdGgucm91bmQod2lkdGhfcGFyZW50IC8gbik7XHJcblx0XHRcdFx0JG9wdGdyb3Vwcy5jc3Moe3dpZHRoOiB3aWR0aH0pO1xyXG5cdFx0XHRcdGlmIChuID4gMSkge1xyXG5cdFx0XHRcdFx0d2lkdGhfbGFzdCA9IHdpZHRoX3BhcmVudCAtIHdpZHRoICogKG4gLSAxKTtcclxuXHRcdFx0XHRcdCRvcHRncm91cHMuZXEobiAtIDEpLmNzcyh7d2lkdGg6IHdpZHRoX2xhc3R9KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblx0XHJcblx0XHRpZiAob3B0aW9ucy5lcXVhbGl6ZUhlaWdodCB8fCBvcHRpb25zLmVxdWFsaXplV2lkdGgpIHtcclxuXHRcdFx0aG9vay5hZnRlcih0aGlzLCAncG9zaXRpb25Ecm9wZG93bicsIGVxdWFsaXplU2l6ZXMpO1xyXG5cdFx0XHRob29rLmFmdGVyKHRoaXMsICdyZWZyZXNoT3B0aW9ucycsIGVxdWFsaXplU2l6ZXMpO1xyXG5cdFx0fVxyXG5cdFxyXG5cdFxyXG5cdH0pO1xyXG5cdFxyXG5cdFNlbGVjdGl6ZS5kZWZpbmUoJ3JlbW92ZV9idXR0b24nLCBmdW5jdGlvbihvcHRpb25zKSB7XHJcblx0XHRpZiAodGhpcy5zZXR0aW5ncy5tb2RlID09PSAnc2luZ2xlJykgcmV0dXJuO1xyXG5cdFxyXG5cdFx0b3B0aW9ucyA9ICQuZXh0ZW5kKHtcclxuXHRcdFx0bGFiZWwgICAgIDogJyZ0aW1lczsnLFxyXG5cdFx0XHR0aXRsZSAgICAgOiAnUmVtb3ZlJyxcclxuXHRcdFx0Y2xhc3NOYW1lIDogJ3JlbW92ZScsXHJcblx0XHRcdGFwcGVuZCAgICA6IHRydWVcclxuXHRcdH0sIG9wdGlvbnMpO1xyXG5cdFxyXG5cdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0dmFyIGh0bWwgPSAnPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGNsYXNzPVwiJyArIG9wdGlvbnMuY2xhc3NOYW1lICsgJ1wiIHRhYmluZGV4PVwiLTFcIiB0aXRsZT1cIicgKyBlc2NhcGVfaHRtbChvcHRpb25zLnRpdGxlKSArICdcIj4nICsgb3B0aW9ucy5sYWJlbCArICc8L2E+JztcclxuXHRcclxuXHRcdC8qKlxyXG5cdFx0ICogQXBwZW5kcyBhbiBlbGVtZW50IGFzIGEgY2hpbGQgKHdpdGggcmF3IEhUTUwpLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBodG1sX2NvbnRhaW5lclxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGh0bWxfZWxlbWVudFxyXG5cdFx0ICogQHJldHVybiB7c3RyaW5nfVxyXG5cdFx0ICovXHJcblx0XHR2YXIgYXBwZW5kID0gZnVuY3Rpb24oaHRtbF9jb250YWluZXIsIGh0bWxfZWxlbWVudCkge1xyXG5cdFx0XHR2YXIgcG9zID0gaHRtbF9jb250YWluZXIuc2VhcmNoKC8oPFxcL1tePl0rPlxccyopJC8pO1xyXG5cdFx0XHRyZXR1cm4gaHRtbF9jb250YWluZXIuc3Vic3RyaW5nKDAsIHBvcykgKyBodG1sX2VsZW1lbnQgKyBodG1sX2NvbnRhaW5lci5zdWJzdHJpbmcocG9zKTtcclxuXHRcdH07XHJcblx0XHJcblx0XHR0aGlzLnNldHVwID0gKGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgb3JpZ2luYWwgPSBzZWxmLnNldHVwO1xyXG5cdFx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0Ly8gb3ZlcnJpZGUgdGhlIGl0ZW0gcmVuZGVyaW5nIG1ldGhvZCB0byBhZGQgdGhlIGJ1dHRvbiB0byBlYWNoXHJcblx0XHRcdFx0aWYgKG9wdGlvbnMuYXBwZW5kKSB7XHJcblx0XHRcdFx0XHR2YXIgcmVuZGVyX2l0ZW0gPSBzZWxmLnNldHRpbmdzLnJlbmRlci5pdGVtO1xyXG5cdFx0XHRcdFx0c2VsZi5zZXR0aW5ncy5yZW5kZXIuaXRlbSA9IGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIGFwcGVuZChyZW5kZXJfaXRlbS5hcHBseSh0aGlzLCBhcmd1bWVudHMpLCBodG1sKTtcclxuXHRcdFx0XHRcdH07XHJcblx0XHRcdFx0fVxyXG5cdFxyXG5cdFx0XHRcdG9yaWdpbmFsLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcblx0XHJcblx0XHRcdFx0Ly8gYWRkIGV2ZW50IGxpc3RlbmVyXHJcblx0XHRcdFx0dGhpcy4kY29udHJvbC5vbignY2xpY2snLCAnLicgKyBvcHRpb25zLmNsYXNzTmFtZSwgZnVuY3Rpb24oZSkge1xyXG5cdFx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdFx0aWYgKHNlbGYuaXNMb2NrZWQpIHJldHVybjtcclxuXHRcclxuXHRcdFx0XHRcdHZhciAkaXRlbSA9ICQoZS5jdXJyZW50VGFyZ2V0KS5wYXJlbnQoKTtcclxuXHRcdFx0XHRcdHNlbGYuc2V0QWN0aXZlSXRlbSgkaXRlbSk7XHJcblx0XHRcdFx0XHRpZiAoc2VsZi5kZWxldGVTZWxlY3Rpb24oKSkge1xyXG5cdFx0XHRcdFx0XHRzZWxmLnNldENhcmV0KHNlbGYuaXRlbXMubGVuZ3RoKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcclxuXHRcdFx0fTtcclxuXHRcdH0pKCk7XHJcblx0XHJcblx0fSk7XHJcblx0XHJcblx0U2VsZWN0aXplLmRlZmluZSgncmVzdG9yZV9vbl9iYWNrc3BhY2UnLCBmdW5jdGlvbihvcHRpb25zKSB7XHJcblx0XHR2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHJcblx0XHRvcHRpb25zLnRleHQgPSBvcHRpb25zLnRleHQgfHwgZnVuY3Rpb24ob3B0aW9uKSB7XHJcblx0XHRcdHJldHVybiBvcHRpb25bdGhpcy5zZXR0aW5ncy5sYWJlbEZpZWxkXTtcclxuXHRcdH07XHJcblx0XHJcblx0XHR0aGlzLm9uS2V5RG93biA9IChmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIG9yaWdpbmFsID0gc2VsZi5vbktleURvd247XHJcblx0XHRcdHJldHVybiBmdW5jdGlvbihlKSB7XHJcblx0XHRcdFx0dmFyIGluZGV4LCBvcHRpb247XHJcblx0XHRcdFx0aWYgKGUua2V5Q29kZSA9PT0gS0VZX0JBQ0tTUEFDRSAmJiB0aGlzLiRjb250cm9sX2lucHV0LnZhbCgpID09PSAnJyAmJiAhdGhpcy4kYWN0aXZlSXRlbXMubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRpbmRleCA9IHRoaXMuY2FyZXRQb3MgLSAxO1xyXG5cdFx0XHRcdFx0aWYgKGluZGV4ID49IDAgJiYgaW5kZXggPCB0aGlzLml0ZW1zLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHRvcHRpb24gPSB0aGlzLm9wdGlvbnNbdGhpcy5pdGVtc1tpbmRleF1dO1xyXG5cdFx0XHRcdFx0XHRpZiAodGhpcy5kZWxldGVTZWxlY3Rpb24oZSkpIHtcclxuXHRcdFx0XHRcdFx0XHR0aGlzLnNldFRleHRib3hWYWx1ZShvcHRpb25zLnRleHQuYXBwbHkodGhpcywgW29wdGlvbl0pKTtcclxuXHRcdFx0XHRcdFx0XHR0aGlzLnJlZnJlc2hPcHRpb25zKHRydWUpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRyZXR1cm4gb3JpZ2luYWwuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcclxuXHRcdFx0fTtcclxuXHRcdH0pKCk7XHJcblx0fSk7XHJcblx0XHJcblxyXG5cdHJldHVybiBTZWxlY3RpemU7XHJcbn0pKTsiXSwic291cmNlUm9vdCI6IiJ9