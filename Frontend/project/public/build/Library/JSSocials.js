(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Library/JSSocials"],{

/***/ "./assets/css/Library/JsSocials.css":
/*!******************************************!*\
  !*** ./assets/css/Library/JsSocials.css ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/Library/JSSocials.js":
/*!****************************************!*\
  !*** ./assets/js/Library/JSSocials.js ***!
  \****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jssocials__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jssocials */ "./node_modules/jssocials/dist/jssocials.js");
/* harmony import */ var jssocials__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jssocials__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jssocials_dist_jssocials_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jssocials/dist/jssocials.css */ "./node_modules/jssocials/dist/jssocials.css");
/* harmony import */ var jssocials_dist_jssocials_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jssocials_dist_jssocials_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jssocials_dist_jssocials_theme_classic_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jssocials/dist/jssocials-theme-classic.css */ "./node_modules/jssocials/dist/jssocials-theme-classic.css");
/* harmony import */ var jssocials_dist_jssocials_theme_classic_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jssocials_dist_jssocials_theme_classic_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _css_Library_JsSocials_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../css/Library/JsSocials.css */ "./assets/css/Library/JsSocials.css");
/* harmony import */ var _css_Library_JsSocials_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_css_Library_JsSocials_css__WEBPACK_IMPORTED_MODULE_3__);
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018-2020 FTK e.V. <ftk.de>
 *  All rights reserved
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/




 // FontAwesome 5



/***/ })

},[["./assets/js/Library/JSSocials.js","runtime","vendors~Admin/EmailCtrl~Admin/FileAudit~Admin/FileAuditCtrl~Archive/CategorizationCtrl~Archive/Recor~b3f38238","vendors~Content/Display~Library/JSSocials"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL0xpYnJhcnkvSnNTb2NpYWxzLmNzcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvTGlicmFyeS9KU1NvY2lhbHMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsdUM7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FBY2E7O0FBRWI7QUFDQTtDQUVBIiwiZmlsZSI6IkxpYnJhcnkvSlNTb2NpYWxzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIiwiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKiAgQ29weXJpZ2h0IG5vdGljZVxyXG4gKlxyXG4gKiAgKGMpIDIwMTgtMjAyMCBGVEsgZS5WLiA8ZnRrLmRlPlxyXG4gKiAgQWxsIHJpZ2h0cyByZXNlcnZlZFxyXG4gKlxyXG4gKiAgVGhpcyBzY3JpcHQgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICogIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAqICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAqICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG4gKlxyXG4gKiAgVGhpcyBjb3B5cmlnaHQgbm90aWNlIE1VU1QgQVBQRUFSIGluIGFsbCBjb3BpZXMgb2YgdGhlIHNjcmlwdCFcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCAnanNzb2NpYWxzJztcclxuaW1wb3J0ICdqc3NvY2lhbHMvZGlzdC9qc3NvY2lhbHMuY3NzJztcclxuaW1wb3J0ICdqc3NvY2lhbHMvZGlzdC9qc3NvY2lhbHMtdGhlbWUtY2xhc3NpYy5jc3MnXHJcbi8vIEZvbnRBd2Vzb21lIDVcclxuaW1wb3J0ICcuLi8uLi9jc3MvTGlicmFyeS9Kc1NvY2lhbHMuY3NzJzsiXSwic291cmNlUm9vdCI6IiJ9