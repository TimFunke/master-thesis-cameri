(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendors~Content/AssignCtrl~NER/Classifier/ModelTestCtrl~NER/CoreNlpBrat"],{

/***/ "./node_modules/chosen-js/chosen.jquery.js":
/*!*************************************************!*\
  !*** ./node_modules/chosen-js/chosen.jquery.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {(function() {
  var $, AbstractChosen, Chosen, SelectParser,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  SelectParser = (function() {
    function SelectParser() {
      this.options_index = 0;
      this.parsed = [];
    }

    SelectParser.prototype.add_node = function(child) {
      if (child.nodeName.toUpperCase() === "OPTGROUP") {
        return this.add_group(child);
      } else {
        return this.add_option(child);
      }
    };

    SelectParser.prototype.add_group = function(group) {
      var group_position, i, len, option, ref, results1;
      group_position = this.parsed.length;
      this.parsed.push({
        array_index: group_position,
        group: true,
        label: group.label,
        title: group.title ? group.title : void 0,
        children: 0,
        disabled: group.disabled,
        classes: group.className
      });
      ref = group.childNodes;
      results1 = [];
      for (i = 0, len = ref.length; i < len; i++) {
        option = ref[i];
        results1.push(this.add_option(option, group_position, group.disabled));
      }
      return results1;
    };

    SelectParser.prototype.add_option = function(option, group_position, group_disabled) {
      if (option.nodeName.toUpperCase() === "OPTION") {
        if (option.text !== "") {
          if (group_position != null) {
            this.parsed[group_position].children += 1;
          }
          this.parsed.push({
            array_index: this.parsed.length,
            options_index: this.options_index,
            value: option.value,
            text: option.text,
            html: option.innerHTML,
            title: option.title ? option.title : void 0,
            selected: option.selected,
            disabled: group_disabled === true ? group_disabled : option.disabled,
            group_array_index: group_position,
            group_label: group_position != null ? this.parsed[group_position].label : null,
            classes: option.className,
            style: option.style.cssText
          });
        } else {
          this.parsed.push({
            array_index: this.parsed.length,
            options_index: this.options_index,
            empty: true
          });
        }
        return this.options_index += 1;
      }
    };

    return SelectParser;

  })();

  SelectParser.select_to_array = function(select) {
    var child, i, len, parser, ref;
    parser = new SelectParser();
    ref = select.childNodes;
    for (i = 0, len = ref.length; i < len; i++) {
      child = ref[i];
      parser.add_node(child);
    }
    return parser.parsed;
  };

  AbstractChosen = (function() {
    function AbstractChosen(form_field, options1) {
      this.form_field = form_field;
      this.options = options1 != null ? options1 : {};
      this.label_click_handler = bind(this.label_click_handler, this);
      if (!AbstractChosen.browser_is_supported()) {
        return;
      }
      this.is_multiple = this.form_field.multiple;
      this.set_default_text();
      this.set_default_values();
      this.setup();
      this.set_up_html();
      this.register_observers();
      this.on_ready();
    }

    AbstractChosen.prototype.set_default_values = function() {
      this.click_test_action = (function(_this) {
        return function(evt) {
          return _this.test_active_click(evt);
        };
      })(this);
      this.activate_action = (function(_this) {
        return function(evt) {
          return _this.activate_field(evt);
        };
      })(this);
      this.active_field = false;
      this.mouse_on_container = false;
      this.results_showing = false;
      this.result_highlighted = null;
      this.is_rtl = this.options.rtl || /\bchosen-rtl\b/.test(this.form_field.className);
      this.allow_single_deselect = (this.options.allow_single_deselect != null) && (this.form_field.options[0] != null) && this.form_field.options[0].text === "" ? this.options.allow_single_deselect : false;
      this.disable_search_threshold = this.options.disable_search_threshold || 0;
      this.disable_search = this.options.disable_search || false;
      this.enable_split_word_search = this.options.enable_split_word_search != null ? this.options.enable_split_word_search : true;
      this.group_search = this.options.group_search != null ? this.options.group_search : true;
      this.search_contains = this.options.search_contains || false;
      this.single_backstroke_delete = this.options.single_backstroke_delete != null ? this.options.single_backstroke_delete : true;
      this.max_selected_options = this.options.max_selected_options || Infinity;
      this.inherit_select_classes = this.options.inherit_select_classes || false;
      this.display_selected_options = this.options.display_selected_options != null ? this.options.display_selected_options : true;
      this.display_disabled_options = this.options.display_disabled_options != null ? this.options.display_disabled_options : true;
      this.include_group_label_in_selected = this.options.include_group_label_in_selected || false;
      this.max_shown_results = this.options.max_shown_results || Number.POSITIVE_INFINITY;
      this.case_sensitive_search = this.options.case_sensitive_search || false;
      return this.hide_results_on_select = this.options.hide_results_on_select != null ? this.options.hide_results_on_select : true;
    };

    AbstractChosen.prototype.set_default_text = function() {
      if (this.form_field.getAttribute("data-placeholder")) {
        this.default_text = this.form_field.getAttribute("data-placeholder");
      } else if (this.is_multiple) {
        this.default_text = this.options.placeholder_text_multiple || this.options.placeholder_text || AbstractChosen.default_multiple_text;
      } else {
        this.default_text = this.options.placeholder_text_single || this.options.placeholder_text || AbstractChosen.default_single_text;
      }
      this.default_text = this.escape_html(this.default_text);
      return this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || AbstractChosen.default_no_result_text;
    };

    AbstractChosen.prototype.choice_label = function(item) {
      if (this.include_group_label_in_selected && (item.group_label != null)) {
        return "<b class='group-name'>" + (this.escape_html(item.group_label)) + "</b>" + item.html;
      } else {
        return item.html;
      }
    };

    AbstractChosen.prototype.mouse_enter = function() {
      return this.mouse_on_container = true;
    };

    AbstractChosen.prototype.mouse_leave = function() {
      return this.mouse_on_container = false;
    };

    AbstractChosen.prototype.input_focus = function(evt) {
      if (this.is_multiple) {
        if (!this.active_field) {
          return setTimeout(((function(_this) {
            return function() {
              return _this.container_mousedown();
            };
          })(this)), 50);
        }
      } else {
        if (!this.active_field) {
          return this.activate_field();
        }
      }
    };

    AbstractChosen.prototype.input_blur = function(evt) {
      if (!this.mouse_on_container) {
        this.active_field = false;
        return setTimeout(((function(_this) {
          return function() {
            return _this.blur_test();
          };
        })(this)), 100);
      }
    };

    AbstractChosen.prototype.label_click_handler = function(evt) {
      if (this.is_multiple) {
        return this.container_mousedown(evt);
      } else {
        return this.activate_field();
      }
    };

    AbstractChosen.prototype.results_option_build = function(options) {
      var content, data, data_content, i, len, ref, shown_results;
      content = '';
      shown_results = 0;
      ref = this.results_data;
      for (i = 0, len = ref.length; i < len; i++) {
        data = ref[i];
        data_content = '';
        if (data.group) {
          data_content = this.result_add_group(data);
        } else {
          data_content = this.result_add_option(data);
        }
        if (data_content !== '') {
          shown_results++;
          content += data_content;
        }
        if (options != null ? options.first : void 0) {
          if (data.selected && this.is_multiple) {
            this.choice_build(data);
          } else if (data.selected && !this.is_multiple) {
            this.single_set_selected_text(this.choice_label(data));
          }
        }
        if (shown_results >= this.max_shown_results) {
          break;
        }
      }
      return content;
    };

    AbstractChosen.prototype.result_add_option = function(option) {
      var classes, option_el;
      if (!option.search_match) {
        return '';
      }
      if (!this.include_option_in_results(option)) {
        return '';
      }
      classes = [];
      if (!option.disabled && !(option.selected && this.is_multiple)) {
        classes.push("active-result");
      }
      if (option.disabled && !(option.selected && this.is_multiple)) {
        classes.push("disabled-result");
      }
      if (option.selected) {
        classes.push("result-selected");
      }
      if (option.group_array_index != null) {
        classes.push("group-option");
      }
      if (option.classes !== "") {
        classes.push(option.classes);
      }
      option_el = document.createElement("li");
      option_el.className = classes.join(" ");
      if (option.style) {
        option_el.style.cssText = option.style;
      }
      option_el.setAttribute("data-option-array-index", option.array_index);
      option_el.innerHTML = option.highlighted_html || option.html;
      if (option.title) {
        option_el.title = option.title;
      }
      return this.outerHTML(option_el);
    };

    AbstractChosen.prototype.result_add_group = function(group) {
      var classes, group_el;
      if (!(group.search_match || group.group_match)) {
        return '';
      }
      if (!(group.active_options > 0)) {
        return '';
      }
      classes = [];
      classes.push("group-result");
      if (group.classes) {
        classes.push(group.classes);
      }
      group_el = document.createElement("li");
      group_el.className = classes.join(" ");
      group_el.innerHTML = group.highlighted_html || this.escape_html(group.label);
      if (group.title) {
        group_el.title = group.title;
      }
      return this.outerHTML(group_el);
    };

    AbstractChosen.prototype.results_update_field = function() {
      this.set_default_text();
      if (!this.is_multiple) {
        this.results_reset_cleanup();
      }
      this.result_clear_highlight();
      this.results_build();
      if (this.results_showing) {
        return this.winnow_results();
      }
    };

    AbstractChosen.prototype.reset_single_select_options = function() {
      var i, len, ref, result, results1;
      ref = this.results_data;
      results1 = [];
      for (i = 0, len = ref.length; i < len; i++) {
        result = ref[i];
        if (result.selected) {
          results1.push(result.selected = false);
        } else {
          results1.push(void 0);
        }
      }
      return results1;
    };

    AbstractChosen.prototype.results_toggle = function() {
      if (this.results_showing) {
        return this.results_hide();
      } else {
        return this.results_show();
      }
    };

    AbstractChosen.prototype.results_search = function(evt) {
      if (this.results_showing) {
        return this.winnow_results();
      } else {
        return this.results_show();
      }
    };

    AbstractChosen.prototype.winnow_results = function(options) {
      var escapedQuery, fix, i, len, option, prefix, query, ref, regex, results, results_group, search_match, startpos, suffix, text;
      this.no_results_clear();
      results = 0;
      query = this.get_search_text();
      escapedQuery = query.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
      regex = this.get_search_regex(escapedQuery);
      ref = this.results_data;
      for (i = 0, len = ref.length; i < len; i++) {
        option = ref[i];
        option.search_match = false;
        results_group = null;
        search_match = null;
        option.highlighted_html = '';
        if (this.include_option_in_results(option)) {
          if (option.group) {
            option.group_match = false;
            option.active_options = 0;
          }
          if ((option.group_array_index != null) && this.results_data[option.group_array_index]) {
            results_group = this.results_data[option.group_array_index];
            if (results_group.active_options === 0 && results_group.search_match) {
              results += 1;
            }
            results_group.active_options += 1;
          }
          text = option.group ? option.label : option.text;
          if (!(option.group && !this.group_search)) {
            search_match = this.search_string_match(text, regex);
            option.search_match = search_match != null;
            if (option.search_match && !option.group) {
              results += 1;
            }
            if (option.search_match) {
              if (query.length) {
                startpos = search_match.index;
                prefix = text.slice(0, startpos);
                fix = text.slice(startpos, startpos + query.length);
                suffix = text.slice(startpos + query.length);
                option.highlighted_html = (this.escape_html(prefix)) + "<em>" + (this.escape_html(fix)) + "</em>" + (this.escape_html(suffix));
              }
              if (results_group != null) {
                results_group.group_match = true;
              }
            } else if ((option.group_array_index != null) && this.results_data[option.group_array_index].search_match) {
              option.search_match = true;
            }
          }
        }
      }
      this.result_clear_highlight();
      if (results < 1 && query.length) {
        this.update_results_content("");
        return this.no_results(query);
      } else {
        this.update_results_content(this.results_option_build());
        if (!(options != null ? options.skip_highlight : void 0)) {
          return this.winnow_results_set_highlight();
        }
      }
    };

    AbstractChosen.prototype.get_search_regex = function(escaped_search_string) {
      var regex_flag, regex_string;
      regex_string = this.search_contains ? escaped_search_string : "(^|\\s|\\b)" + escaped_search_string + "[^\\s]*";
      if (!(this.enable_split_word_search || this.search_contains)) {
        regex_string = "^" + regex_string;
      }
      regex_flag = this.case_sensitive_search ? "" : "i";
      return new RegExp(regex_string, regex_flag);
    };

    AbstractChosen.prototype.search_string_match = function(search_string, regex) {
      var match;
      match = regex.exec(search_string);
      if (!this.search_contains && (match != null ? match[1] : void 0)) {
        match.index += 1;
      }
      return match;
    };

    AbstractChosen.prototype.choices_count = function() {
      var i, len, option, ref;
      if (this.selected_option_count != null) {
        return this.selected_option_count;
      }
      this.selected_option_count = 0;
      ref = this.form_field.options;
      for (i = 0, len = ref.length; i < len; i++) {
        option = ref[i];
        if (option.selected) {
          this.selected_option_count += 1;
        }
      }
      return this.selected_option_count;
    };

    AbstractChosen.prototype.choices_click = function(evt) {
      evt.preventDefault();
      this.activate_field();
      if (!(this.results_showing || this.is_disabled)) {
        return this.results_show();
      }
    };

    AbstractChosen.prototype.keydown_checker = function(evt) {
      var ref, stroke;
      stroke = (ref = evt.which) != null ? ref : evt.keyCode;
      this.search_field_scale();
      if (stroke !== 8 && this.pending_backstroke) {
        this.clear_backstroke();
      }
      switch (stroke) {
        case 8:
          this.backstroke_length = this.get_search_field_value().length;
          break;
        case 9:
          if (this.results_showing && !this.is_multiple) {
            this.result_select(evt);
          }
          this.mouse_on_container = false;
          break;
        case 13:
          if (this.results_showing) {
            evt.preventDefault();
          }
          break;
        case 27:
          if (this.results_showing) {
            evt.preventDefault();
          }
          break;
        case 32:
          if (this.disable_search) {
            evt.preventDefault();
          }
          break;
        case 38:
          evt.preventDefault();
          this.keyup_arrow();
          break;
        case 40:
          evt.preventDefault();
          this.keydown_arrow();
          break;
      }
    };

    AbstractChosen.prototype.keyup_checker = function(evt) {
      var ref, stroke;
      stroke = (ref = evt.which) != null ? ref : evt.keyCode;
      this.search_field_scale();
      switch (stroke) {
        case 8:
          if (this.is_multiple && this.backstroke_length < 1 && this.choices_count() > 0) {
            this.keydown_backstroke();
          } else if (!this.pending_backstroke) {
            this.result_clear_highlight();
            this.results_search();
          }
          break;
        case 13:
          evt.preventDefault();
          if (this.results_showing) {
            this.result_select(evt);
          }
          break;
        case 27:
          if (this.results_showing) {
            this.results_hide();
          }
          break;
        case 9:
        case 16:
        case 17:
        case 18:
        case 38:
        case 40:
        case 91:
          break;
        default:
          this.results_search();
          break;
      }
    };

    AbstractChosen.prototype.clipboard_event_checker = function(evt) {
      if (this.is_disabled) {
        return;
      }
      return setTimeout(((function(_this) {
        return function() {
          return _this.results_search();
        };
      })(this)), 50);
    };

    AbstractChosen.prototype.container_width = function() {
      if (this.options.width != null) {
        return this.options.width;
      } else {
        return this.form_field.offsetWidth + "px";
      }
    };

    AbstractChosen.prototype.include_option_in_results = function(option) {
      if (this.is_multiple && (!this.display_selected_options && option.selected)) {
        return false;
      }
      if (!this.display_disabled_options && option.disabled) {
        return false;
      }
      if (option.empty) {
        return false;
      }
      return true;
    };

    AbstractChosen.prototype.search_results_touchstart = function(evt) {
      this.touch_started = true;
      return this.search_results_mouseover(evt);
    };

    AbstractChosen.prototype.search_results_touchmove = function(evt) {
      this.touch_started = false;
      return this.search_results_mouseout(evt);
    };

    AbstractChosen.prototype.search_results_touchend = function(evt) {
      if (this.touch_started) {
        return this.search_results_mouseup(evt);
      }
    };

    AbstractChosen.prototype.outerHTML = function(element) {
      var tmp;
      if (element.outerHTML) {
        return element.outerHTML;
      }
      tmp = document.createElement("div");
      tmp.appendChild(element);
      return tmp.innerHTML;
    };

    AbstractChosen.prototype.get_single_html = function() {
      return "<a class=\"chosen-single chosen-default\">\n  <span>" + this.default_text + "</span>\n  <div><b></b></div>\n</a>\n<div class=\"chosen-drop\">\n  <div class=\"chosen-search\">\n    <input class=\"chosen-search-input\" type=\"text\" autocomplete=\"off\" />\n  </div>\n  <ul class=\"chosen-results\"></ul>\n</div>";
    };

    AbstractChosen.prototype.get_multi_html = function() {
      return "<ul class=\"chosen-choices\">\n  <li class=\"search-field\">\n    <input class=\"chosen-search-input\" type=\"text\" autocomplete=\"off\" value=\"" + this.default_text + "\" />\n  </li>\n</ul>\n<div class=\"chosen-drop\">\n  <ul class=\"chosen-results\"></ul>\n</div>";
    };

    AbstractChosen.prototype.get_no_results_html = function(terms) {
      return "<li class=\"no-results\">\n  " + this.results_none_found + " <span>" + (this.escape_html(terms)) + "</span>\n</li>";
    };

    AbstractChosen.browser_is_supported = function() {
      if ("Microsoft Internet Explorer" === window.navigator.appName) {
        return document.documentMode >= 8;
      }
      if (/iP(od|hone)/i.test(window.navigator.userAgent) || /IEMobile/i.test(window.navigator.userAgent) || /Windows Phone/i.test(window.navigator.userAgent) || /BlackBerry/i.test(window.navigator.userAgent) || /BB10/i.test(window.navigator.userAgent) || /Android.*Mobile/i.test(window.navigator.userAgent)) {
        return false;
      }
      return true;
    };

    AbstractChosen.default_multiple_text = "Select Some Options";

    AbstractChosen.default_single_text = "Select an Option";

    AbstractChosen.default_no_result_text = "No results match";

    return AbstractChosen;

  })();

  $ = jQuery;

  $.fn.extend({
    chosen: function(options) {
      if (!AbstractChosen.browser_is_supported()) {
        return this;
      }
      return this.each(function(input_field) {
        var $this, chosen;
        $this = $(this);
        chosen = $this.data('chosen');
        if (options === 'destroy') {
          if (chosen instanceof Chosen) {
            chosen.destroy();
          }
          return;
        }
        if (!(chosen instanceof Chosen)) {
          $this.data('chosen', new Chosen(this, options));
        }
      });
    }
  });

  Chosen = (function(superClass) {
    extend(Chosen, superClass);

    function Chosen() {
      return Chosen.__super__.constructor.apply(this, arguments);
    }

    Chosen.prototype.setup = function() {
      this.form_field_jq = $(this.form_field);
      return this.current_selectedIndex = this.form_field.selectedIndex;
    };

    Chosen.prototype.set_up_html = function() {
      var container_classes, container_props;
      container_classes = ["chosen-container"];
      container_classes.push("chosen-container-" + (this.is_multiple ? "multi" : "single"));
      if (this.inherit_select_classes && this.form_field.className) {
        container_classes.push(this.form_field.className);
      }
      if (this.is_rtl) {
        container_classes.push("chosen-rtl");
      }
      container_props = {
        'class': container_classes.join(' '),
        'title': this.form_field.title
      };
      if (this.form_field.id.length) {
        container_props.id = this.form_field.id.replace(/[^\w]/g, '_') + "_chosen";
      }
      this.container = $("<div />", container_props);
      this.container.width(this.container_width());
      if (this.is_multiple) {
        this.container.html(this.get_multi_html());
      } else {
        this.container.html(this.get_single_html());
      }
      this.form_field_jq.hide().after(this.container);
      this.dropdown = this.container.find('div.chosen-drop').first();
      this.search_field = this.container.find('input').first();
      this.search_results = this.container.find('ul.chosen-results').first();
      this.search_field_scale();
      this.search_no_results = this.container.find('li.no-results').first();
      if (this.is_multiple) {
        this.search_choices = this.container.find('ul.chosen-choices').first();
        this.search_container = this.container.find('li.search-field').first();
      } else {
        this.search_container = this.container.find('div.chosen-search').first();
        this.selected_item = this.container.find('.chosen-single').first();
      }
      this.results_build();
      this.set_tab_index();
      return this.set_label_behavior();
    };

    Chosen.prototype.on_ready = function() {
      return this.form_field_jq.trigger("chosen:ready", {
        chosen: this
      });
    };

    Chosen.prototype.register_observers = function() {
      this.container.on('touchstart.chosen', (function(_this) {
        return function(evt) {
          _this.container_mousedown(evt);
        };
      })(this));
      this.container.on('touchend.chosen', (function(_this) {
        return function(evt) {
          _this.container_mouseup(evt);
        };
      })(this));
      this.container.on('mousedown.chosen', (function(_this) {
        return function(evt) {
          _this.container_mousedown(evt);
        };
      })(this));
      this.container.on('mouseup.chosen', (function(_this) {
        return function(evt) {
          _this.container_mouseup(evt);
        };
      })(this));
      this.container.on('mouseenter.chosen', (function(_this) {
        return function(evt) {
          _this.mouse_enter(evt);
        };
      })(this));
      this.container.on('mouseleave.chosen', (function(_this) {
        return function(evt) {
          _this.mouse_leave(evt);
        };
      })(this));
      this.search_results.on('mouseup.chosen', (function(_this) {
        return function(evt) {
          _this.search_results_mouseup(evt);
        };
      })(this));
      this.search_results.on('mouseover.chosen', (function(_this) {
        return function(evt) {
          _this.search_results_mouseover(evt);
        };
      })(this));
      this.search_results.on('mouseout.chosen', (function(_this) {
        return function(evt) {
          _this.search_results_mouseout(evt);
        };
      })(this));
      this.search_results.on('mousewheel.chosen DOMMouseScroll.chosen', (function(_this) {
        return function(evt) {
          _this.search_results_mousewheel(evt);
        };
      })(this));
      this.search_results.on('touchstart.chosen', (function(_this) {
        return function(evt) {
          _this.search_results_touchstart(evt);
        };
      })(this));
      this.search_results.on('touchmove.chosen', (function(_this) {
        return function(evt) {
          _this.search_results_touchmove(evt);
        };
      })(this));
      this.search_results.on('touchend.chosen', (function(_this) {
        return function(evt) {
          _this.search_results_touchend(evt);
        };
      })(this));
      this.form_field_jq.on("chosen:updated.chosen", (function(_this) {
        return function(evt) {
          _this.results_update_field(evt);
        };
      })(this));
      this.form_field_jq.on("chosen:activate.chosen", (function(_this) {
        return function(evt) {
          _this.activate_field(evt);
        };
      })(this));
      this.form_field_jq.on("chosen:open.chosen", (function(_this) {
        return function(evt) {
          _this.container_mousedown(evt);
        };
      })(this));
      this.form_field_jq.on("chosen:close.chosen", (function(_this) {
        return function(evt) {
          _this.close_field(evt);
        };
      })(this));
      this.search_field.on('blur.chosen', (function(_this) {
        return function(evt) {
          _this.input_blur(evt);
        };
      })(this));
      this.search_field.on('keyup.chosen', (function(_this) {
        return function(evt) {
          _this.keyup_checker(evt);
        };
      })(this));
      this.search_field.on('keydown.chosen', (function(_this) {
        return function(evt) {
          _this.keydown_checker(evt);
        };
      })(this));
      this.search_field.on('focus.chosen', (function(_this) {
        return function(evt) {
          _this.input_focus(evt);
        };
      })(this));
      this.search_field.on('cut.chosen', (function(_this) {
        return function(evt) {
          _this.clipboard_event_checker(evt);
        };
      })(this));
      this.search_field.on('paste.chosen', (function(_this) {
        return function(evt) {
          _this.clipboard_event_checker(evt);
        };
      })(this));
      if (this.is_multiple) {
        return this.search_choices.on('click.chosen', (function(_this) {
          return function(evt) {
            _this.choices_click(evt);
          };
        })(this));
      } else {
        return this.container.on('click.chosen', function(evt) {
          evt.preventDefault();
        });
      }
    };

    Chosen.prototype.destroy = function() {
      $(this.container[0].ownerDocument).off('click.chosen', this.click_test_action);
      if (this.form_field_label.length > 0) {
        this.form_field_label.off('click.chosen');
      }
      if (this.search_field[0].tabIndex) {
        this.form_field_jq[0].tabIndex = this.search_field[0].tabIndex;
      }
      this.container.remove();
      this.form_field_jq.removeData('chosen');
      return this.form_field_jq.show();
    };

    Chosen.prototype.search_field_disabled = function() {
      this.is_disabled = this.form_field.disabled || this.form_field_jq.parents('fieldset').is(':disabled');
      this.container.toggleClass('chosen-disabled', this.is_disabled);
      this.search_field[0].disabled = this.is_disabled;
      if (!this.is_multiple) {
        this.selected_item.off('focus.chosen', this.activate_field);
      }
      if (this.is_disabled) {
        return this.close_field();
      } else if (!this.is_multiple) {
        return this.selected_item.on('focus.chosen', this.activate_field);
      }
    };

    Chosen.prototype.container_mousedown = function(evt) {
      var ref;
      if (this.is_disabled) {
        return;
      }
      if (evt && ((ref = evt.type) === 'mousedown' || ref === 'touchstart') && !this.results_showing) {
        evt.preventDefault();
      }
      if (!((evt != null) && ($(evt.target)).hasClass("search-choice-close"))) {
        if (!this.active_field) {
          if (this.is_multiple) {
            this.search_field.val("");
          }
          $(this.container[0].ownerDocument).on('click.chosen', this.click_test_action);
          this.results_show();
        } else if (!this.is_multiple && evt && (($(evt.target)[0] === this.selected_item[0]) || $(evt.target).parents("a.chosen-single").length)) {
          evt.preventDefault();
          this.results_toggle();
        }
        return this.activate_field();
      }
    };

    Chosen.prototype.container_mouseup = function(evt) {
      if (evt.target.nodeName === "ABBR" && !this.is_disabled) {
        return this.results_reset(evt);
      }
    };

    Chosen.prototype.search_results_mousewheel = function(evt) {
      var delta;
      if (evt.originalEvent) {
        delta = evt.originalEvent.deltaY || -evt.originalEvent.wheelDelta || evt.originalEvent.detail;
      }
      if (delta != null) {
        evt.preventDefault();
        if (evt.type === 'DOMMouseScroll') {
          delta = delta * 40;
        }
        return this.search_results.scrollTop(delta + this.search_results.scrollTop());
      }
    };

    Chosen.prototype.blur_test = function(evt) {
      if (!this.active_field && this.container.hasClass("chosen-container-active")) {
        return this.close_field();
      }
    };

    Chosen.prototype.close_field = function() {
      $(this.container[0].ownerDocument).off("click.chosen", this.click_test_action);
      this.active_field = false;
      this.results_hide();
      this.container.removeClass("chosen-container-active");
      this.clear_backstroke();
      this.show_search_field_default();
      this.search_field_scale();
      return this.search_field.blur();
    };

    Chosen.prototype.activate_field = function() {
      if (this.is_disabled) {
        return;
      }
      this.container.addClass("chosen-container-active");
      this.active_field = true;
      this.search_field.val(this.search_field.val());
      return this.search_field.focus();
    };

    Chosen.prototype.test_active_click = function(evt) {
      var active_container;
      active_container = $(evt.target).closest('.chosen-container');
      if (active_container.length && this.container[0] === active_container[0]) {
        return this.active_field = true;
      } else {
        return this.close_field();
      }
    };

    Chosen.prototype.results_build = function() {
      this.parsing = true;
      this.selected_option_count = null;
      this.results_data = SelectParser.select_to_array(this.form_field);
      if (this.is_multiple) {
        this.search_choices.find("li.search-choice").remove();
      } else {
        this.single_set_selected_text();
        if (this.disable_search || this.form_field.options.length <= this.disable_search_threshold) {
          this.search_field[0].readOnly = true;
          this.container.addClass("chosen-container-single-nosearch");
        } else {
          this.search_field[0].readOnly = false;
          this.container.removeClass("chosen-container-single-nosearch");
        }
      }
      this.update_results_content(this.results_option_build({
        first: true
      }));
      this.search_field_disabled();
      this.show_search_field_default();
      this.search_field_scale();
      return this.parsing = false;
    };

    Chosen.prototype.result_do_highlight = function(el) {
      var high_bottom, high_top, maxHeight, visible_bottom, visible_top;
      if (el.length) {
        this.result_clear_highlight();
        this.result_highlight = el;
        this.result_highlight.addClass("highlighted");
        maxHeight = parseInt(this.search_results.css("maxHeight"), 10);
        visible_top = this.search_results.scrollTop();
        visible_bottom = maxHeight + visible_top;
        high_top = this.result_highlight.position().top + this.search_results.scrollTop();
        high_bottom = high_top + this.result_highlight.outerHeight();
        if (high_bottom >= visible_bottom) {
          return this.search_results.scrollTop((high_bottom - maxHeight) > 0 ? high_bottom - maxHeight : 0);
        } else if (high_top < visible_top) {
          return this.search_results.scrollTop(high_top);
        }
      }
    };

    Chosen.prototype.result_clear_highlight = function() {
      if (this.result_highlight) {
        this.result_highlight.removeClass("highlighted");
      }
      return this.result_highlight = null;
    };

    Chosen.prototype.results_show = function() {
      if (this.is_multiple && this.max_selected_options <= this.choices_count()) {
        this.form_field_jq.trigger("chosen:maxselected", {
          chosen: this
        });
        return false;
      }
      this.container.addClass("chosen-with-drop");
      this.results_showing = true;
      this.search_field.focus();
      this.search_field.val(this.get_search_field_value());
      this.winnow_results();
      return this.form_field_jq.trigger("chosen:showing_dropdown", {
        chosen: this
      });
    };

    Chosen.prototype.update_results_content = function(content) {
      return this.search_results.html(content);
    };

    Chosen.prototype.results_hide = function() {
      if (this.results_showing) {
        this.result_clear_highlight();
        this.container.removeClass("chosen-with-drop");
        this.form_field_jq.trigger("chosen:hiding_dropdown", {
          chosen: this
        });
      }
      return this.results_showing = false;
    };

    Chosen.prototype.set_tab_index = function(el) {
      var ti;
      if (this.form_field.tabIndex) {
        ti = this.form_field.tabIndex;
        this.form_field.tabIndex = -1;
        return this.search_field[0].tabIndex = ti;
      }
    };

    Chosen.prototype.set_label_behavior = function() {
      this.form_field_label = this.form_field_jq.parents("label");
      if (!this.form_field_label.length && this.form_field.id.length) {
        this.form_field_label = $("label[for='" + this.form_field.id + "']");
      }
      if (this.form_field_label.length > 0) {
        return this.form_field_label.on('click.chosen', this.label_click_handler);
      }
    };

    Chosen.prototype.show_search_field_default = function() {
      if (this.is_multiple && this.choices_count() < 1 && !this.active_field) {
        this.search_field.val(this.default_text);
        return this.search_field.addClass("default");
      } else {
        this.search_field.val("");
        return this.search_field.removeClass("default");
      }
    };

    Chosen.prototype.search_results_mouseup = function(evt) {
      var target;
      target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
      if (target.length) {
        this.result_highlight = target;
        this.result_select(evt);
        return this.search_field.focus();
      }
    };

    Chosen.prototype.search_results_mouseover = function(evt) {
      var target;
      target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
      if (target) {
        return this.result_do_highlight(target);
      }
    };

    Chosen.prototype.search_results_mouseout = function(evt) {
      if ($(evt.target).hasClass("active-result") || $(evt.target).parents('.active-result').first()) {
        return this.result_clear_highlight();
      }
    };

    Chosen.prototype.choice_build = function(item) {
      var choice, close_link;
      choice = $('<li />', {
        "class": "search-choice"
      }).html("<span>" + (this.choice_label(item)) + "</span>");
      if (item.disabled) {
        choice.addClass('search-choice-disabled');
      } else {
        close_link = $('<a />', {
          "class": 'search-choice-close',
          'data-option-array-index': item.array_index
        });
        close_link.on('click.chosen', (function(_this) {
          return function(evt) {
            return _this.choice_destroy_link_click(evt);
          };
        })(this));
        choice.append(close_link);
      }
      return this.search_container.before(choice);
    };

    Chosen.prototype.choice_destroy_link_click = function(evt) {
      evt.preventDefault();
      evt.stopPropagation();
      if (!this.is_disabled) {
        return this.choice_destroy($(evt.target));
      }
    };

    Chosen.prototype.choice_destroy = function(link) {
      if (this.result_deselect(link[0].getAttribute("data-option-array-index"))) {
        if (this.active_field) {
          this.search_field.focus();
        } else {
          this.show_search_field_default();
        }
        if (this.is_multiple && this.choices_count() > 0 && this.get_search_field_value().length < 1) {
          this.results_hide();
        }
        link.parents('li').first().remove();
        return this.search_field_scale();
      }
    };

    Chosen.prototype.results_reset = function() {
      this.reset_single_select_options();
      this.form_field.options[0].selected = true;
      this.single_set_selected_text();
      this.show_search_field_default();
      this.results_reset_cleanup();
      this.trigger_form_field_change();
      if (this.active_field) {
        return this.results_hide();
      }
    };

    Chosen.prototype.results_reset_cleanup = function() {
      this.current_selectedIndex = this.form_field.selectedIndex;
      return this.selected_item.find("abbr").remove();
    };

    Chosen.prototype.result_select = function(evt) {
      var high, item;
      if (this.result_highlight) {
        high = this.result_highlight;
        this.result_clear_highlight();
        if (this.is_multiple && this.max_selected_options <= this.choices_count()) {
          this.form_field_jq.trigger("chosen:maxselected", {
            chosen: this
          });
          return false;
        }
        if (this.is_multiple) {
          high.removeClass("active-result");
        } else {
          this.reset_single_select_options();
        }
        high.addClass("result-selected");
        item = this.results_data[high[0].getAttribute("data-option-array-index")];
        item.selected = true;
        this.form_field.options[item.options_index].selected = true;
        this.selected_option_count = null;
        if (this.is_multiple) {
          this.choice_build(item);
        } else {
          this.single_set_selected_text(this.choice_label(item));
        }
        if (this.is_multiple && (!this.hide_results_on_select || (evt.metaKey || evt.ctrlKey))) {
          if (evt.metaKey || evt.ctrlKey) {
            this.winnow_results({
              skip_highlight: true
            });
          } else {
            this.search_field.val("");
            this.winnow_results();
          }
        } else {
          this.results_hide();
          this.show_search_field_default();
        }
        if (this.is_multiple || this.form_field.selectedIndex !== this.current_selectedIndex) {
          this.trigger_form_field_change({
            selected: this.form_field.options[item.options_index].value
          });
        }
        this.current_selectedIndex = this.form_field.selectedIndex;
        evt.preventDefault();
        return this.search_field_scale();
      }
    };

    Chosen.prototype.single_set_selected_text = function(text) {
      if (text == null) {
        text = this.default_text;
      }
      if (text === this.default_text) {
        this.selected_item.addClass("chosen-default");
      } else {
        this.single_deselect_control_build();
        this.selected_item.removeClass("chosen-default");
      }
      return this.selected_item.find("span").html(text);
    };

    Chosen.prototype.result_deselect = function(pos) {
      var result_data;
      result_data = this.results_data[pos];
      if (!this.form_field.options[result_data.options_index].disabled) {
        result_data.selected = false;
        this.form_field.options[result_data.options_index].selected = false;
        this.selected_option_count = null;
        this.result_clear_highlight();
        if (this.results_showing) {
          this.winnow_results();
        }
        this.trigger_form_field_change({
          deselected: this.form_field.options[result_data.options_index].value
        });
        this.search_field_scale();
        return true;
      } else {
        return false;
      }
    };

    Chosen.prototype.single_deselect_control_build = function() {
      if (!this.allow_single_deselect) {
        return;
      }
      if (!this.selected_item.find("abbr").length) {
        this.selected_item.find("span").first().after("<abbr class=\"search-choice-close\"></abbr>");
      }
      return this.selected_item.addClass("chosen-single-with-deselect");
    };

    Chosen.prototype.get_search_field_value = function() {
      return this.search_field.val();
    };

    Chosen.prototype.get_search_text = function() {
      return $.trim(this.get_search_field_value());
    };

    Chosen.prototype.escape_html = function(text) {
      return $('<div/>').text(text).html();
    };

    Chosen.prototype.winnow_results_set_highlight = function() {
      var do_high, selected_results;
      selected_results = !this.is_multiple ? this.search_results.find(".result-selected.active-result") : [];
      do_high = selected_results.length ? selected_results.first() : this.search_results.find(".active-result").first();
      if (do_high != null) {
        return this.result_do_highlight(do_high);
      }
    };

    Chosen.prototype.no_results = function(terms) {
      var no_results_html;
      no_results_html = this.get_no_results_html(terms);
      this.search_results.append(no_results_html);
      return this.form_field_jq.trigger("chosen:no_results", {
        chosen: this
      });
    };

    Chosen.prototype.no_results_clear = function() {
      return this.search_results.find(".no-results").remove();
    };

    Chosen.prototype.keydown_arrow = function() {
      var next_sib;
      if (this.results_showing && this.result_highlight) {
        next_sib = this.result_highlight.nextAll("li.active-result").first();
        if (next_sib) {
          return this.result_do_highlight(next_sib);
        }
      } else {
        return this.results_show();
      }
    };

    Chosen.prototype.keyup_arrow = function() {
      var prev_sibs;
      if (!this.results_showing && !this.is_multiple) {
        return this.results_show();
      } else if (this.result_highlight) {
        prev_sibs = this.result_highlight.prevAll("li.active-result");
        if (prev_sibs.length) {
          return this.result_do_highlight(prev_sibs.first());
        } else {
          if (this.choices_count() > 0) {
            this.results_hide();
          }
          return this.result_clear_highlight();
        }
      }
    };

    Chosen.prototype.keydown_backstroke = function() {
      var next_available_destroy;
      if (this.pending_backstroke) {
        this.choice_destroy(this.pending_backstroke.find("a").first());
        return this.clear_backstroke();
      } else {
        next_available_destroy = this.search_container.siblings("li.search-choice").last();
        if (next_available_destroy.length && !next_available_destroy.hasClass("search-choice-disabled")) {
          this.pending_backstroke = next_available_destroy;
          if (this.single_backstroke_delete) {
            return this.keydown_backstroke();
          } else {
            return this.pending_backstroke.addClass("search-choice-focus");
          }
        }
      }
    };

    Chosen.prototype.clear_backstroke = function() {
      if (this.pending_backstroke) {
        this.pending_backstroke.removeClass("search-choice-focus");
      }
      return this.pending_backstroke = null;
    };

    Chosen.prototype.search_field_scale = function() {
      var div, i, len, style, style_block, styles, width;
      if (!this.is_multiple) {
        return;
      }
      style_block = {
        position: 'absolute',
        left: '-1000px',
        top: '-1000px',
        display: 'none',
        whiteSpace: 'pre'
      };
      styles = ['fontSize', 'fontStyle', 'fontWeight', 'fontFamily', 'lineHeight', 'textTransform', 'letterSpacing'];
      for (i = 0, len = styles.length; i < len; i++) {
        style = styles[i];
        style_block[style] = this.search_field.css(style);
      }
      div = $('<div />').css(style_block);
      div.text(this.get_search_field_value());
      $('body').append(div);
      width = div.width() + 25;
      div.remove();
      if (this.container.is(':visible')) {
        width = Math.min(this.container.outerWidth() - 10, width);
      }
      return this.search_field.width(width);
    };

    Chosen.prototype.trigger_form_field_change = function(extra) {
      this.form_field_jq.trigger("input", extra);
      return this.form_field_jq.trigger("change", extra);
    };

    return Chosen;

  })(AbstractChosen);

}).call(this);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY2hvc2VuLWpzL2Nob3Nlbi5qcXVlcnkuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBLDRCQUE0QixtQkFBbUIsZ0NBQWdDLEdBQUcsRUFBRTtBQUNwRixzQ0FBc0MsMEJBQTBCLHlEQUF5RCxFQUFFLGtCQUFrQiwwQkFBMEIsRUFBRSxtQ0FBbUMsOEJBQThCLG9DQUFvQyxjQUFjLEVBQUU7QUFDOVIsZ0JBQWdCOztBQUVoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsbUNBQW1DLFNBQVM7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWCxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxTQUFTO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsU0FBUztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQyxTQUFTO0FBQzVDO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQSxtQ0FBbUMsU0FBUztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsU0FBUztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxHQUFHOztBQUVIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLEdBQUc7O0FBRUg7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1A7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0MsU0FBUztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxHQUFHOztBQUVILENBQUMiLCJmaWxlIjoidmVuZG9yc35Db250ZW50L0Fzc2lnbkN0cmx+TkVSL0NsYXNzaWZpZXIvTW9kZWxUZXN0Q3RybH5ORVIvQ29yZU5scEJyYXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKSB7XG4gIHZhciAkLCBBYnN0cmFjdENob3NlbiwgQ2hvc2VuLCBTZWxlY3RQYXJzZXIsXG4gICAgYmluZCA9IGZ1bmN0aW9uKGZuLCBtZSl7IHJldHVybiBmdW5jdGlvbigpeyByZXR1cm4gZm4uYXBwbHkobWUsIGFyZ3VtZW50cyk7IH07IH0sXG4gICAgZXh0ZW5kID0gZnVuY3Rpb24oY2hpbGQsIHBhcmVudCkgeyBmb3IgKHZhciBrZXkgaW4gcGFyZW50KSB7IGlmIChoYXNQcm9wLmNhbGwocGFyZW50LCBrZXkpKSBjaGlsZFtrZXldID0gcGFyZW50W2tleV07IH0gZnVuY3Rpb24gY3RvcigpIHsgdGhpcy5jb25zdHJ1Y3RvciA9IGNoaWxkOyB9IGN0b3IucHJvdG90eXBlID0gcGFyZW50LnByb3RvdHlwZTsgY2hpbGQucHJvdG90eXBlID0gbmV3IGN0b3IoKTsgY2hpbGQuX19zdXBlcl9fID0gcGFyZW50LnByb3RvdHlwZTsgcmV0dXJuIGNoaWxkOyB9LFxuICAgIGhhc1Byb3AgPSB7fS5oYXNPd25Qcm9wZXJ0eTtcblxuICBTZWxlY3RQYXJzZXIgPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gU2VsZWN0UGFyc2VyKCkge1xuICAgICAgdGhpcy5vcHRpb25zX2luZGV4ID0gMDtcbiAgICAgIHRoaXMucGFyc2VkID0gW107XG4gICAgfVxuXG4gICAgU2VsZWN0UGFyc2VyLnByb3RvdHlwZS5hZGRfbm9kZSA9IGZ1bmN0aW9uKGNoaWxkKSB7XG4gICAgICBpZiAoY2hpbGQubm9kZU5hbWUudG9VcHBlckNhc2UoKSA9PT0gXCJPUFRHUk9VUFwiKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFkZF9ncm91cChjaGlsZCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5hZGRfb3B0aW9uKGNoaWxkKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgU2VsZWN0UGFyc2VyLnByb3RvdHlwZS5hZGRfZ3JvdXAgPSBmdW5jdGlvbihncm91cCkge1xuICAgICAgdmFyIGdyb3VwX3Bvc2l0aW9uLCBpLCBsZW4sIG9wdGlvbiwgcmVmLCByZXN1bHRzMTtcbiAgICAgIGdyb3VwX3Bvc2l0aW9uID0gdGhpcy5wYXJzZWQubGVuZ3RoO1xuICAgICAgdGhpcy5wYXJzZWQucHVzaCh7XG4gICAgICAgIGFycmF5X2luZGV4OiBncm91cF9wb3NpdGlvbixcbiAgICAgICAgZ3JvdXA6IHRydWUsXG4gICAgICAgIGxhYmVsOiBncm91cC5sYWJlbCxcbiAgICAgICAgdGl0bGU6IGdyb3VwLnRpdGxlID8gZ3JvdXAudGl0bGUgOiB2b2lkIDAsXG4gICAgICAgIGNoaWxkcmVuOiAwLFxuICAgICAgICBkaXNhYmxlZDogZ3JvdXAuZGlzYWJsZWQsXG4gICAgICAgIGNsYXNzZXM6IGdyb3VwLmNsYXNzTmFtZVxuICAgICAgfSk7XG4gICAgICByZWYgPSBncm91cC5jaGlsZE5vZGVzO1xuICAgICAgcmVzdWx0czEgPSBbXTtcbiAgICAgIGZvciAoaSA9IDAsIGxlbiA9IHJlZi5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBvcHRpb24gPSByZWZbaV07XG4gICAgICAgIHJlc3VsdHMxLnB1c2godGhpcy5hZGRfb3B0aW9uKG9wdGlvbiwgZ3JvdXBfcG9zaXRpb24sIGdyb3VwLmRpc2FibGVkKSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gcmVzdWx0czE7XG4gICAgfTtcblxuICAgIFNlbGVjdFBhcnNlci5wcm90b3R5cGUuYWRkX29wdGlvbiA9IGZ1bmN0aW9uKG9wdGlvbiwgZ3JvdXBfcG9zaXRpb24sIGdyb3VwX2Rpc2FibGVkKSB7XG4gICAgICBpZiAob3B0aW9uLm5vZGVOYW1lLnRvVXBwZXJDYXNlKCkgPT09IFwiT1BUSU9OXCIpIHtcbiAgICAgICAgaWYgKG9wdGlvbi50ZXh0ICE9PSBcIlwiKSB7XG4gICAgICAgICAgaWYgKGdyb3VwX3Bvc2l0aW9uICE9IG51bGwpIHtcbiAgICAgICAgICAgIHRoaXMucGFyc2VkW2dyb3VwX3Bvc2l0aW9uXS5jaGlsZHJlbiArPSAxO1xuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLnBhcnNlZC5wdXNoKHtcbiAgICAgICAgICAgIGFycmF5X2luZGV4OiB0aGlzLnBhcnNlZC5sZW5ndGgsXG4gICAgICAgICAgICBvcHRpb25zX2luZGV4OiB0aGlzLm9wdGlvbnNfaW5kZXgsXG4gICAgICAgICAgICB2YWx1ZTogb3B0aW9uLnZhbHVlLFxuICAgICAgICAgICAgdGV4dDogb3B0aW9uLnRleHQsXG4gICAgICAgICAgICBodG1sOiBvcHRpb24uaW5uZXJIVE1MLFxuICAgICAgICAgICAgdGl0bGU6IG9wdGlvbi50aXRsZSA/IG9wdGlvbi50aXRsZSA6IHZvaWQgMCxcbiAgICAgICAgICAgIHNlbGVjdGVkOiBvcHRpb24uc2VsZWN0ZWQsXG4gICAgICAgICAgICBkaXNhYmxlZDogZ3JvdXBfZGlzYWJsZWQgPT09IHRydWUgPyBncm91cF9kaXNhYmxlZCA6IG9wdGlvbi5kaXNhYmxlZCxcbiAgICAgICAgICAgIGdyb3VwX2FycmF5X2luZGV4OiBncm91cF9wb3NpdGlvbixcbiAgICAgICAgICAgIGdyb3VwX2xhYmVsOiBncm91cF9wb3NpdGlvbiAhPSBudWxsID8gdGhpcy5wYXJzZWRbZ3JvdXBfcG9zaXRpb25dLmxhYmVsIDogbnVsbCxcbiAgICAgICAgICAgIGNsYXNzZXM6IG9wdGlvbi5jbGFzc05hbWUsXG4gICAgICAgICAgICBzdHlsZTogb3B0aW9uLnN0eWxlLmNzc1RleHRcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnBhcnNlZC5wdXNoKHtcbiAgICAgICAgICAgIGFycmF5X2luZGV4OiB0aGlzLnBhcnNlZC5sZW5ndGgsXG4gICAgICAgICAgICBvcHRpb25zX2luZGV4OiB0aGlzLm9wdGlvbnNfaW5kZXgsXG4gICAgICAgICAgICBlbXB0eTogdHJ1ZVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLm9wdGlvbnNfaW5kZXggKz0gMTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgcmV0dXJuIFNlbGVjdFBhcnNlcjtcblxuICB9KSgpO1xuXG4gIFNlbGVjdFBhcnNlci5zZWxlY3RfdG9fYXJyYXkgPSBmdW5jdGlvbihzZWxlY3QpIHtcbiAgICB2YXIgY2hpbGQsIGksIGxlbiwgcGFyc2VyLCByZWY7XG4gICAgcGFyc2VyID0gbmV3IFNlbGVjdFBhcnNlcigpO1xuICAgIHJlZiA9IHNlbGVjdC5jaGlsZE5vZGVzO1xuICAgIGZvciAoaSA9IDAsIGxlbiA9IHJlZi5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgY2hpbGQgPSByZWZbaV07XG4gICAgICBwYXJzZXIuYWRkX25vZGUoY2hpbGQpO1xuICAgIH1cbiAgICByZXR1cm4gcGFyc2VyLnBhcnNlZDtcbiAgfTtcblxuICBBYnN0cmFjdENob3NlbiA9IChmdW5jdGlvbigpIHtcbiAgICBmdW5jdGlvbiBBYnN0cmFjdENob3Nlbihmb3JtX2ZpZWxkLCBvcHRpb25zMSkge1xuICAgICAgdGhpcy5mb3JtX2ZpZWxkID0gZm9ybV9maWVsZDtcbiAgICAgIHRoaXMub3B0aW9ucyA9IG9wdGlvbnMxICE9IG51bGwgPyBvcHRpb25zMSA6IHt9O1xuICAgICAgdGhpcy5sYWJlbF9jbGlja19oYW5kbGVyID0gYmluZCh0aGlzLmxhYmVsX2NsaWNrX2hhbmRsZXIsIHRoaXMpO1xuICAgICAgaWYgKCFBYnN0cmFjdENob3Nlbi5icm93c2VyX2lzX3N1cHBvcnRlZCgpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMuaXNfbXVsdGlwbGUgPSB0aGlzLmZvcm1fZmllbGQubXVsdGlwbGU7XG4gICAgICB0aGlzLnNldF9kZWZhdWx0X3RleHQoKTtcbiAgICAgIHRoaXMuc2V0X2RlZmF1bHRfdmFsdWVzKCk7XG4gICAgICB0aGlzLnNldHVwKCk7XG4gICAgICB0aGlzLnNldF91cF9odG1sKCk7XG4gICAgICB0aGlzLnJlZ2lzdGVyX29ic2VydmVycygpO1xuICAgICAgdGhpcy5vbl9yZWFkeSgpO1xuICAgIH1cblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5zZXRfZGVmYXVsdF92YWx1ZXMgPSBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMuY2xpY2tfdGVzdF9hY3Rpb24gPSAoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIHJldHVybiBfdGhpcy50ZXN0X2FjdGl2ZV9jbGljayhldnQpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcyk7XG4gICAgICB0aGlzLmFjdGl2YXRlX2FjdGlvbiA9IChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgICAgcmV0dXJuIF90aGlzLmFjdGl2YXRlX2ZpZWxkKGV2dCk7XG4gICAgICAgIH07XG4gICAgICB9KSh0aGlzKTtcbiAgICAgIHRoaXMuYWN0aXZlX2ZpZWxkID0gZmFsc2U7XG4gICAgICB0aGlzLm1vdXNlX29uX2NvbnRhaW5lciA9IGZhbHNlO1xuICAgICAgdGhpcy5yZXN1bHRzX3Nob3dpbmcgPSBmYWxzZTtcbiAgICAgIHRoaXMucmVzdWx0X2hpZ2hsaWdodGVkID0gbnVsbDtcbiAgICAgIHRoaXMuaXNfcnRsID0gdGhpcy5vcHRpb25zLnJ0bCB8fCAvXFxiY2hvc2VuLXJ0bFxcYi8udGVzdCh0aGlzLmZvcm1fZmllbGQuY2xhc3NOYW1lKTtcbiAgICAgIHRoaXMuYWxsb3dfc2luZ2xlX2Rlc2VsZWN0ID0gKHRoaXMub3B0aW9ucy5hbGxvd19zaW5nbGVfZGVzZWxlY3QgIT0gbnVsbCkgJiYgKHRoaXMuZm9ybV9maWVsZC5vcHRpb25zWzBdICE9IG51bGwpICYmIHRoaXMuZm9ybV9maWVsZC5vcHRpb25zWzBdLnRleHQgPT09IFwiXCIgPyB0aGlzLm9wdGlvbnMuYWxsb3dfc2luZ2xlX2Rlc2VsZWN0IDogZmFsc2U7XG4gICAgICB0aGlzLmRpc2FibGVfc2VhcmNoX3RocmVzaG9sZCA9IHRoaXMub3B0aW9ucy5kaXNhYmxlX3NlYXJjaF90aHJlc2hvbGQgfHwgMDtcbiAgICAgIHRoaXMuZGlzYWJsZV9zZWFyY2ggPSB0aGlzLm9wdGlvbnMuZGlzYWJsZV9zZWFyY2ggfHwgZmFsc2U7XG4gICAgICB0aGlzLmVuYWJsZV9zcGxpdF93b3JkX3NlYXJjaCA9IHRoaXMub3B0aW9ucy5lbmFibGVfc3BsaXRfd29yZF9zZWFyY2ggIT0gbnVsbCA/IHRoaXMub3B0aW9ucy5lbmFibGVfc3BsaXRfd29yZF9zZWFyY2ggOiB0cnVlO1xuICAgICAgdGhpcy5ncm91cF9zZWFyY2ggPSB0aGlzLm9wdGlvbnMuZ3JvdXBfc2VhcmNoICE9IG51bGwgPyB0aGlzLm9wdGlvbnMuZ3JvdXBfc2VhcmNoIDogdHJ1ZTtcbiAgICAgIHRoaXMuc2VhcmNoX2NvbnRhaW5zID0gdGhpcy5vcHRpb25zLnNlYXJjaF9jb250YWlucyB8fCBmYWxzZTtcbiAgICAgIHRoaXMuc2luZ2xlX2JhY2tzdHJva2VfZGVsZXRlID0gdGhpcy5vcHRpb25zLnNpbmdsZV9iYWNrc3Ryb2tlX2RlbGV0ZSAhPSBudWxsID8gdGhpcy5vcHRpb25zLnNpbmdsZV9iYWNrc3Ryb2tlX2RlbGV0ZSA6IHRydWU7XG4gICAgICB0aGlzLm1heF9zZWxlY3RlZF9vcHRpb25zID0gdGhpcy5vcHRpb25zLm1heF9zZWxlY3RlZF9vcHRpb25zIHx8IEluZmluaXR5O1xuICAgICAgdGhpcy5pbmhlcml0X3NlbGVjdF9jbGFzc2VzID0gdGhpcy5vcHRpb25zLmluaGVyaXRfc2VsZWN0X2NsYXNzZXMgfHwgZmFsc2U7XG4gICAgICB0aGlzLmRpc3BsYXlfc2VsZWN0ZWRfb3B0aW9ucyA9IHRoaXMub3B0aW9ucy5kaXNwbGF5X3NlbGVjdGVkX29wdGlvbnMgIT0gbnVsbCA/IHRoaXMub3B0aW9ucy5kaXNwbGF5X3NlbGVjdGVkX29wdGlvbnMgOiB0cnVlO1xuICAgICAgdGhpcy5kaXNwbGF5X2Rpc2FibGVkX29wdGlvbnMgPSB0aGlzLm9wdGlvbnMuZGlzcGxheV9kaXNhYmxlZF9vcHRpb25zICE9IG51bGwgPyB0aGlzLm9wdGlvbnMuZGlzcGxheV9kaXNhYmxlZF9vcHRpb25zIDogdHJ1ZTtcbiAgICAgIHRoaXMuaW5jbHVkZV9ncm91cF9sYWJlbF9pbl9zZWxlY3RlZCA9IHRoaXMub3B0aW9ucy5pbmNsdWRlX2dyb3VwX2xhYmVsX2luX3NlbGVjdGVkIHx8IGZhbHNlO1xuICAgICAgdGhpcy5tYXhfc2hvd25fcmVzdWx0cyA9IHRoaXMub3B0aW9ucy5tYXhfc2hvd25fcmVzdWx0cyB8fCBOdW1iZXIuUE9TSVRJVkVfSU5GSU5JVFk7XG4gICAgICB0aGlzLmNhc2Vfc2Vuc2l0aXZlX3NlYXJjaCA9IHRoaXMub3B0aW9ucy5jYXNlX3NlbnNpdGl2ZV9zZWFyY2ggfHwgZmFsc2U7XG4gICAgICByZXR1cm4gdGhpcy5oaWRlX3Jlc3VsdHNfb25fc2VsZWN0ID0gdGhpcy5vcHRpb25zLmhpZGVfcmVzdWx0c19vbl9zZWxlY3QgIT0gbnVsbCA/IHRoaXMub3B0aW9ucy5oaWRlX3Jlc3VsdHNfb25fc2VsZWN0IDogdHJ1ZTtcbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLnNldF9kZWZhdWx0X3RleHQgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmICh0aGlzLmZvcm1fZmllbGQuZ2V0QXR0cmlidXRlKFwiZGF0YS1wbGFjZWhvbGRlclwiKSkge1xuICAgICAgICB0aGlzLmRlZmF1bHRfdGV4dCA9IHRoaXMuZm9ybV9maWVsZC5nZXRBdHRyaWJ1dGUoXCJkYXRhLXBsYWNlaG9sZGVyXCIpO1xuICAgICAgfSBlbHNlIGlmICh0aGlzLmlzX211bHRpcGxlKSB7XG4gICAgICAgIHRoaXMuZGVmYXVsdF90ZXh0ID0gdGhpcy5vcHRpb25zLnBsYWNlaG9sZGVyX3RleHRfbXVsdGlwbGUgfHwgdGhpcy5vcHRpb25zLnBsYWNlaG9sZGVyX3RleHQgfHwgQWJzdHJhY3RDaG9zZW4uZGVmYXVsdF9tdWx0aXBsZV90ZXh0O1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5kZWZhdWx0X3RleHQgPSB0aGlzLm9wdGlvbnMucGxhY2Vob2xkZXJfdGV4dF9zaW5nbGUgfHwgdGhpcy5vcHRpb25zLnBsYWNlaG9sZGVyX3RleHQgfHwgQWJzdHJhY3RDaG9zZW4uZGVmYXVsdF9zaW5nbGVfdGV4dDtcbiAgICAgIH1cbiAgICAgIHRoaXMuZGVmYXVsdF90ZXh0ID0gdGhpcy5lc2NhcGVfaHRtbCh0aGlzLmRlZmF1bHRfdGV4dCk7XG4gICAgICByZXR1cm4gdGhpcy5yZXN1bHRzX25vbmVfZm91bmQgPSB0aGlzLmZvcm1fZmllbGQuZ2V0QXR0cmlidXRlKFwiZGF0YS1ub19yZXN1bHRzX3RleHRcIikgfHwgdGhpcy5vcHRpb25zLm5vX3Jlc3VsdHNfdGV4dCB8fCBBYnN0cmFjdENob3Nlbi5kZWZhdWx0X25vX3Jlc3VsdF90ZXh0O1xuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUuY2hvaWNlX2xhYmVsID0gZnVuY3Rpb24oaXRlbSkge1xuICAgICAgaWYgKHRoaXMuaW5jbHVkZV9ncm91cF9sYWJlbF9pbl9zZWxlY3RlZCAmJiAoaXRlbS5ncm91cF9sYWJlbCAhPSBudWxsKSkge1xuICAgICAgICByZXR1cm4gXCI8YiBjbGFzcz0nZ3JvdXAtbmFtZSc+XCIgKyAodGhpcy5lc2NhcGVfaHRtbChpdGVtLmdyb3VwX2xhYmVsKSkgKyBcIjwvYj5cIiArIGl0ZW0uaHRtbDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBpdGVtLmh0bWw7XG4gICAgICB9XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5tb3VzZV9lbnRlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMubW91c2Vfb25fY29udGFpbmVyID0gdHJ1ZTtcbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLm1vdXNlX2xlYXZlID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5tb3VzZV9vbl9jb250YWluZXIgPSBmYWxzZTtcbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLmlucHV0X2ZvY3VzID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICBpZiAodGhpcy5pc19tdWx0aXBsZSkge1xuICAgICAgICBpZiAoIXRoaXMuYWN0aXZlX2ZpZWxkKSB7XG4gICAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoKChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuY29udGFpbmVyX21vdXNlZG93bigpO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9KSh0aGlzKSksIDUwKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKCF0aGlzLmFjdGl2ZV9maWVsZCkge1xuICAgICAgICAgIHJldHVybiB0aGlzLmFjdGl2YXRlX2ZpZWxkKCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLmlucHV0X2JsdXIgPSBmdW5jdGlvbihldnQpIHtcbiAgICAgIGlmICghdGhpcy5tb3VzZV9vbl9jb250YWluZXIpIHtcbiAgICAgICAgdGhpcy5hY3RpdmVfZmllbGQgPSBmYWxzZTtcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoKChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5ibHVyX3Rlc3QoKTtcbiAgICAgICAgICB9O1xuICAgICAgICB9KSh0aGlzKSksIDEwMCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5sYWJlbF9jbGlja19oYW5kbGVyID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICBpZiAodGhpcy5pc19tdWx0aXBsZSkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb250YWluZXJfbW91c2Vkb3duKGV2dCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5hY3RpdmF0ZV9maWVsZCgpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUucmVzdWx0c19vcHRpb25fYnVpbGQgPSBmdW5jdGlvbihvcHRpb25zKSB7XG4gICAgICB2YXIgY29udGVudCwgZGF0YSwgZGF0YV9jb250ZW50LCBpLCBsZW4sIHJlZiwgc2hvd25fcmVzdWx0cztcbiAgICAgIGNvbnRlbnQgPSAnJztcbiAgICAgIHNob3duX3Jlc3VsdHMgPSAwO1xuICAgICAgcmVmID0gdGhpcy5yZXN1bHRzX2RhdGE7XG4gICAgICBmb3IgKGkgPSAwLCBsZW4gPSByZWYubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgZGF0YSA9IHJlZltpXTtcbiAgICAgICAgZGF0YV9jb250ZW50ID0gJyc7XG4gICAgICAgIGlmIChkYXRhLmdyb3VwKSB7XG4gICAgICAgICAgZGF0YV9jb250ZW50ID0gdGhpcy5yZXN1bHRfYWRkX2dyb3VwKGRhdGEpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGRhdGFfY29udGVudCA9IHRoaXMucmVzdWx0X2FkZF9vcHRpb24oZGF0YSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGRhdGFfY29udGVudCAhPT0gJycpIHtcbiAgICAgICAgICBzaG93bl9yZXN1bHRzKys7XG4gICAgICAgICAgY29udGVudCArPSBkYXRhX2NvbnRlbnQ7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG9wdGlvbnMgIT0gbnVsbCA/IG9wdGlvbnMuZmlyc3QgOiB2b2lkIDApIHtcbiAgICAgICAgICBpZiAoZGF0YS5zZWxlY3RlZCAmJiB0aGlzLmlzX211bHRpcGxlKSB7XG4gICAgICAgICAgICB0aGlzLmNob2ljZV9idWlsZChkYXRhKTtcbiAgICAgICAgICB9IGVsc2UgaWYgKGRhdGEuc2VsZWN0ZWQgJiYgIXRoaXMuaXNfbXVsdGlwbGUpIHtcbiAgICAgICAgICAgIHRoaXMuc2luZ2xlX3NldF9zZWxlY3RlZF90ZXh0KHRoaXMuY2hvaWNlX2xhYmVsKGRhdGEpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHNob3duX3Jlc3VsdHMgPj0gdGhpcy5tYXhfc2hvd25fcmVzdWx0cykge1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gY29udGVudDtcbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLnJlc3VsdF9hZGRfb3B0aW9uID0gZnVuY3Rpb24ob3B0aW9uKSB7XG4gICAgICB2YXIgY2xhc3Nlcywgb3B0aW9uX2VsO1xuICAgICAgaWYgKCFvcHRpb24uc2VhcmNoX21hdGNoKSB7XG4gICAgICAgIHJldHVybiAnJztcbiAgICAgIH1cbiAgICAgIGlmICghdGhpcy5pbmNsdWRlX29wdGlvbl9pbl9yZXN1bHRzKG9wdGlvbikpIHtcbiAgICAgICAgcmV0dXJuICcnO1xuICAgICAgfVxuICAgICAgY2xhc3NlcyA9IFtdO1xuICAgICAgaWYgKCFvcHRpb24uZGlzYWJsZWQgJiYgIShvcHRpb24uc2VsZWN0ZWQgJiYgdGhpcy5pc19tdWx0aXBsZSkpIHtcbiAgICAgICAgY2xhc3Nlcy5wdXNoKFwiYWN0aXZlLXJlc3VsdFwiKTtcbiAgICAgIH1cbiAgICAgIGlmIChvcHRpb24uZGlzYWJsZWQgJiYgIShvcHRpb24uc2VsZWN0ZWQgJiYgdGhpcy5pc19tdWx0aXBsZSkpIHtcbiAgICAgICAgY2xhc3Nlcy5wdXNoKFwiZGlzYWJsZWQtcmVzdWx0XCIpO1xuICAgICAgfVxuICAgICAgaWYgKG9wdGlvbi5zZWxlY3RlZCkge1xuICAgICAgICBjbGFzc2VzLnB1c2goXCJyZXN1bHQtc2VsZWN0ZWRcIik7XG4gICAgICB9XG4gICAgICBpZiAob3B0aW9uLmdyb3VwX2FycmF5X2luZGV4ICE9IG51bGwpIHtcbiAgICAgICAgY2xhc3Nlcy5wdXNoKFwiZ3JvdXAtb3B0aW9uXCIpO1xuICAgICAgfVxuICAgICAgaWYgKG9wdGlvbi5jbGFzc2VzICE9PSBcIlwiKSB7XG4gICAgICAgIGNsYXNzZXMucHVzaChvcHRpb24uY2xhc3Nlcyk7XG4gICAgICB9XG4gICAgICBvcHRpb25fZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGlcIik7XG4gICAgICBvcHRpb25fZWwuY2xhc3NOYW1lID0gY2xhc3Nlcy5qb2luKFwiIFwiKTtcbiAgICAgIGlmIChvcHRpb24uc3R5bGUpIHtcbiAgICAgICAgb3B0aW9uX2VsLnN0eWxlLmNzc1RleHQgPSBvcHRpb24uc3R5bGU7XG4gICAgICB9XG4gICAgICBvcHRpb25fZWwuc2V0QXR0cmlidXRlKFwiZGF0YS1vcHRpb24tYXJyYXktaW5kZXhcIiwgb3B0aW9uLmFycmF5X2luZGV4KTtcbiAgICAgIG9wdGlvbl9lbC5pbm5lckhUTUwgPSBvcHRpb24uaGlnaGxpZ2h0ZWRfaHRtbCB8fCBvcHRpb24uaHRtbDtcbiAgICAgIGlmIChvcHRpb24udGl0bGUpIHtcbiAgICAgICAgb3B0aW9uX2VsLnRpdGxlID0gb3B0aW9uLnRpdGxlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXMub3V0ZXJIVE1MKG9wdGlvbl9lbCk7XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5yZXN1bHRfYWRkX2dyb3VwID0gZnVuY3Rpb24oZ3JvdXApIHtcbiAgICAgIHZhciBjbGFzc2VzLCBncm91cF9lbDtcbiAgICAgIGlmICghKGdyb3VwLnNlYXJjaF9tYXRjaCB8fCBncm91cC5ncm91cF9tYXRjaCkpIHtcbiAgICAgICAgcmV0dXJuICcnO1xuICAgICAgfVxuICAgICAgaWYgKCEoZ3JvdXAuYWN0aXZlX29wdGlvbnMgPiAwKSkge1xuICAgICAgICByZXR1cm4gJyc7XG4gICAgICB9XG4gICAgICBjbGFzc2VzID0gW107XG4gICAgICBjbGFzc2VzLnB1c2goXCJncm91cC1yZXN1bHRcIik7XG4gICAgICBpZiAoZ3JvdXAuY2xhc3Nlcykge1xuICAgICAgICBjbGFzc2VzLnB1c2goZ3JvdXAuY2xhc3Nlcyk7XG4gICAgICB9XG4gICAgICBncm91cF9lbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsaVwiKTtcbiAgICAgIGdyb3VwX2VsLmNsYXNzTmFtZSA9IGNsYXNzZXMuam9pbihcIiBcIik7XG4gICAgICBncm91cF9lbC5pbm5lckhUTUwgPSBncm91cC5oaWdobGlnaHRlZF9odG1sIHx8IHRoaXMuZXNjYXBlX2h0bWwoZ3JvdXAubGFiZWwpO1xuICAgICAgaWYgKGdyb3VwLnRpdGxlKSB7XG4gICAgICAgIGdyb3VwX2VsLnRpdGxlID0gZ3JvdXAudGl0bGU7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5vdXRlckhUTUwoZ3JvdXBfZWwpO1xuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUucmVzdWx0c191cGRhdGVfZmllbGQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMuc2V0X2RlZmF1bHRfdGV4dCgpO1xuICAgICAgaWYgKCF0aGlzLmlzX211bHRpcGxlKSB7XG4gICAgICAgIHRoaXMucmVzdWx0c19yZXNldF9jbGVhbnVwKCk7XG4gICAgICB9XG4gICAgICB0aGlzLnJlc3VsdF9jbGVhcl9oaWdobGlnaHQoKTtcbiAgICAgIHRoaXMucmVzdWx0c19idWlsZCgpO1xuICAgICAgaWYgKHRoaXMucmVzdWx0c19zaG93aW5nKSB7XG4gICAgICAgIHJldHVybiB0aGlzLndpbm5vd19yZXN1bHRzKCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5yZXNldF9zaW5nbGVfc2VsZWN0X29wdGlvbnMgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBpLCBsZW4sIHJlZiwgcmVzdWx0LCByZXN1bHRzMTtcbiAgICAgIHJlZiA9IHRoaXMucmVzdWx0c19kYXRhO1xuICAgICAgcmVzdWx0czEgPSBbXTtcbiAgICAgIGZvciAoaSA9IDAsIGxlbiA9IHJlZi5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICByZXN1bHQgPSByZWZbaV07XG4gICAgICAgIGlmIChyZXN1bHQuc2VsZWN0ZWQpIHtcbiAgICAgICAgICByZXN1bHRzMS5wdXNoKHJlc3VsdC5zZWxlY3RlZCA9IGZhbHNlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXN1bHRzMS5wdXNoKHZvaWQgMCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiByZXN1bHRzMTtcbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLnJlc3VsdHNfdG9nZ2xlID0gZnVuY3Rpb24oKSB7XG4gICAgICBpZiAodGhpcy5yZXN1bHRzX3Nob3dpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzdWx0c19oaWRlKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHRzX3Nob3coKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLnJlc3VsdHNfc2VhcmNoID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICBpZiAodGhpcy5yZXN1bHRzX3Nob3dpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMud2lubm93X3Jlc3VsdHMoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdHNfc2hvdygpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUud2lubm93X3Jlc3VsdHMgPSBmdW5jdGlvbihvcHRpb25zKSB7XG4gICAgICB2YXIgZXNjYXBlZFF1ZXJ5LCBmaXgsIGksIGxlbiwgb3B0aW9uLCBwcmVmaXgsIHF1ZXJ5LCByZWYsIHJlZ2V4LCByZXN1bHRzLCByZXN1bHRzX2dyb3VwLCBzZWFyY2hfbWF0Y2gsIHN0YXJ0cG9zLCBzdWZmaXgsIHRleHQ7XG4gICAgICB0aGlzLm5vX3Jlc3VsdHNfY2xlYXIoKTtcbiAgICAgIHJlc3VsdHMgPSAwO1xuICAgICAgcXVlcnkgPSB0aGlzLmdldF9zZWFyY2hfdGV4dCgpO1xuICAgICAgZXNjYXBlZFF1ZXJ5ID0gcXVlcnkucmVwbGFjZSgvWy1bXFxde30oKSorPy4sXFxcXF4kfCNcXHNdL2csIFwiXFxcXCQmXCIpO1xuICAgICAgcmVnZXggPSB0aGlzLmdldF9zZWFyY2hfcmVnZXgoZXNjYXBlZFF1ZXJ5KTtcbiAgICAgIHJlZiA9IHRoaXMucmVzdWx0c19kYXRhO1xuICAgICAgZm9yIChpID0gMCwgbGVuID0gcmVmLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgIG9wdGlvbiA9IHJlZltpXTtcbiAgICAgICAgb3B0aW9uLnNlYXJjaF9tYXRjaCA9IGZhbHNlO1xuICAgICAgICByZXN1bHRzX2dyb3VwID0gbnVsbDtcbiAgICAgICAgc2VhcmNoX21hdGNoID0gbnVsbDtcbiAgICAgICAgb3B0aW9uLmhpZ2hsaWdodGVkX2h0bWwgPSAnJztcbiAgICAgICAgaWYgKHRoaXMuaW5jbHVkZV9vcHRpb25faW5fcmVzdWx0cyhvcHRpb24pKSB7XG4gICAgICAgICAgaWYgKG9wdGlvbi5ncm91cCkge1xuICAgICAgICAgICAgb3B0aW9uLmdyb3VwX21hdGNoID0gZmFsc2U7XG4gICAgICAgICAgICBvcHRpb24uYWN0aXZlX29wdGlvbnMgPSAwO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoKG9wdGlvbi5ncm91cF9hcnJheV9pbmRleCAhPSBudWxsKSAmJiB0aGlzLnJlc3VsdHNfZGF0YVtvcHRpb24uZ3JvdXBfYXJyYXlfaW5kZXhdKSB7XG4gICAgICAgICAgICByZXN1bHRzX2dyb3VwID0gdGhpcy5yZXN1bHRzX2RhdGFbb3B0aW9uLmdyb3VwX2FycmF5X2luZGV4XTtcbiAgICAgICAgICAgIGlmIChyZXN1bHRzX2dyb3VwLmFjdGl2ZV9vcHRpb25zID09PSAwICYmIHJlc3VsdHNfZ3JvdXAuc2VhcmNoX21hdGNoKSB7XG4gICAgICAgICAgICAgIHJlc3VsdHMgKz0gMTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJlc3VsdHNfZ3JvdXAuYWN0aXZlX29wdGlvbnMgKz0gMTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGV4dCA9IG9wdGlvbi5ncm91cCA/IG9wdGlvbi5sYWJlbCA6IG9wdGlvbi50ZXh0O1xuICAgICAgICAgIGlmICghKG9wdGlvbi5ncm91cCAmJiAhdGhpcy5ncm91cF9zZWFyY2gpKSB7XG4gICAgICAgICAgICBzZWFyY2hfbWF0Y2ggPSB0aGlzLnNlYXJjaF9zdHJpbmdfbWF0Y2godGV4dCwgcmVnZXgpO1xuICAgICAgICAgICAgb3B0aW9uLnNlYXJjaF9tYXRjaCA9IHNlYXJjaF9tYXRjaCAhPSBudWxsO1xuICAgICAgICAgICAgaWYgKG9wdGlvbi5zZWFyY2hfbWF0Y2ggJiYgIW9wdGlvbi5ncm91cCkge1xuICAgICAgICAgICAgICByZXN1bHRzICs9IDE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAob3B0aW9uLnNlYXJjaF9tYXRjaCkge1xuICAgICAgICAgICAgICBpZiAocXVlcnkubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgc3RhcnRwb3MgPSBzZWFyY2hfbWF0Y2guaW5kZXg7XG4gICAgICAgICAgICAgICAgcHJlZml4ID0gdGV4dC5zbGljZSgwLCBzdGFydHBvcyk7XG4gICAgICAgICAgICAgICAgZml4ID0gdGV4dC5zbGljZShzdGFydHBvcywgc3RhcnRwb3MgKyBxdWVyeS5sZW5ndGgpO1xuICAgICAgICAgICAgICAgIHN1ZmZpeCA9IHRleHQuc2xpY2Uoc3RhcnRwb3MgKyBxdWVyeS5sZW5ndGgpO1xuICAgICAgICAgICAgICAgIG9wdGlvbi5oaWdobGlnaHRlZF9odG1sID0gKHRoaXMuZXNjYXBlX2h0bWwocHJlZml4KSkgKyBcIjxlbT5cIiArICh0aGlzLmVzY2FwZV9odG1sKGZpeCkpICsgXCI8L2VtPlwiICsgKHRoaXMuZXNjYXBlX2h0bWwoc3VmZml4KSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYgKHJlc3VsdHNfZ3JvdXAgIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHJlc3VsdHNfZ3JvdXAuZ3JvdXBfbWF0Y2ggPSB0cnVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgaWYgKChvcHRpb24uZ3JvdXBfYXJyYXlfaW5kZXggIT0gbnVsbCkgJiYgdGhpcy5yZXN1bHRzX2RhdGFbb3B0aW9uLmdyb3VwX2FycmF5X2luZGV4XS5zZWFyY2hfbWF0Y2gpIHtcbiAgICAgICAgICAgICAgb3B0aW9uLnNlYXJjaF9tYXRjaCA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICB0aGlzLnJlc3VsdF9jbGVhcl9oaWdobGlnaHQoKTtcbiAgICAgIGlmIChyZXN1bHRzIDwgMSAmJiBxdWVyeS5sZW5ndGgpIHtcbiAgICAgICAgdGhpcy51cGRhdGVfcmVzdWx0c19jb250ZW50KFwiXCIpO1xuICAgICAgICByZXR1cm4gdGhpcy5ub19yZXN1bHRzKHF1ZXJ5KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMudXBkYXRlX3Jlc3VsdHNfY29udGVudCh0aGlzLnJlc3VsdHNfb3B0aW9uX2J1aWxkKCkpO1xuICAgICAgICBpZiAoIShvcHRpb25zICE9IG51bGwgPyBvcHRpb25zLnNraXBfaGlnaGxpZ2h0IDogdm9pZCAwKSkge1xuICAgICAgICAgIHJldHVybiB0aGlzLndpbm5vd19yZXN1bHRzX3NldF9oaWdobGlnaHQoKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUuZ2V0X3NlYXJjaF9yZWdleCA9IGZ1bmN0aW9uKGVzY2FwZWRfc2VhcmNoX3N0cmluZykge1xuICAgICAgdmFyIHJlZ2V4X2ZsYWcsIHJlZ2V4X3N0cmluZztcbiAgICAgIHJlZ2V4X3N0cmluZyA9IHRoaXMuc2VhcmNoX2NvbnRhaW5zID8gZXNjYXBlZF9zZWFyY2hfc3RyaW5nIDogXCIoXnxcXFxcc3xcXFxcYilcIiArIGVzY2FwZWRfc2VhcmNoX3N0cmluZyArIFwiW15cXFxcc10qXCI7XG4gICAgICBpZiAoISh0aGlzLmVuYWJsZV9zcGxpdF93b3JkX3NlYXJjaCB8fCB0aGlzLnNlYXJjaF9jb250YWlucykpIHtcbiAgICAgICAgcmVnZXhfc3RyaW5nID0gXCJeXCIgKyByZWdleF9zdHJpbmc7XG4gICAgICB9XG4gICAgICByZWdleF9mbGFnID0gdGhpcy5jYXNlX3NlbnNpdGl2ZV9zZWFyY2ggPyBcIlwiIDogXCJpXCI7XG4gICAgICByZXR1cm4gbmV3IFJlZ0V4cChyZWdleF9zdHJpbmcsIHJlZ2V4X2ZsYWcpO1xuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUuc2VhcmNoX3N0cmluZ19tYXRjaCA9IGZ1bmN0aW9uKHNlYXJjaF9zdHJpbmcsIHJlZ2V4KSB7XG4gICAgICB2YXIgbWF0Y2g7XG4gICAgICBtYXRjaCA9IHJlZ2V4LmV4ZWMoc2VhcmNoX3N0cmluZyk7XG4gICAgICBpZiAoIXRoaXMuc2VhcmNoX2NvbnRhaW5zICYmIChtYXRjaCAhPSBudWxsID8gbWF0Y2hbMV0gOiB2b2lkIDApKSB7XG4gICAgICAgIG1hdGNoLmluZGV4ICs9IDE7XG4gICAgICB9XG4gICAgICByZXR1cm4gbWF0Y2g7XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5jaG9pY2VzX2NvdW50ID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgaSwgbGVuLCBvcHRpb24sIHJlZjtcbiAgICAgIGlmICh0aGlzLnNlbGVjdGVkX29wdGlvbl9jb3VudCAhPSBudWxsKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkX29wdGlvbl9jb3VudDtcbiAgICAgIH1cbiAgICAgIHRoaXMuc2VsZWN0ZWRfb3B0aW9uX2NvdW50ID0gMDtcbiAgICAgIHJlZiA9IHRoaXMuZm9ybV9maWVsZC5vcHRpb25zO1xuICAgICAgZm9yIChpID0gMCwgbGVuID0gcmVmLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgIG9wdGlvbiA9IHJlZltpXTtcbiAgICAgICAgaWYgKG9wdGlvbi5zZWxlY3RlZCkge1xuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRfb3B0aW9uX2NvdW50ICs9IDE7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkX29wdGlvbl9jb3VudDtcbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLmNob2ljZXNfY2xpY2sgPSBmdW5jdGlvbihldnQpIHtcbiAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgdGhpcy5hY3RpdmF0ZV9maWVsZCgpO1xuICAgICAgaWYgKCEodGhpcy5yZXN1bHRzX3Nob3dpbmcgfHwgdGhpcy5pc19kaXNhYmxlZCkpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzdWx0c19zaG93KCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5rZXlkb3duX2NoZWNrZXIgPSBmdW5jdGlvbihldnQpIHtcbiAgICAgIHZhciByZWYsIHN0cm9rZTtcbiAgICAgIHN0cm9rZSA9IChyZWYgPSBldnQud2hpY2gpICE9IG51bGwgPyByZWYgOiBldnQua2V5Q29kZTtcbiAgICAgIHRoaXMuc2VhcmNoX2ZpZWxkX3NjYWxlKCk7XG4gICAgICBpZiAoc3Ryb2tlICE9PSA4ICYmIHRoaXMucGVuZGluZ19iYWNrc3Ryb2tlKSB7XG4gICAgICAgIHRoaXMuY2xlYXJfYmFja3N0cm9rZSgpO1xuICAgICAgfVxuICAgICAgc3dpdGNoIChzdHJva2UpIHtcbiAgICAgICAgY2FzZSA4OlxuICAgICAgICAgIHRoaXMuYmFja3N0cm9rZV9sZW5ndGggPSB0aGlzLmdldF9zZWFyY2hfZmllbGRfdmFsdWUoKS5sZW5ndGg7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgOTpcbiAgICAgICAgICBpZiAodGhpcy5yZXN1bHRzX3Nob3dpbmcgJiYgIXRoaXMuaXNfbXVsdGlwbGUpIHtcbiAgICAgICAgICAgIHRoaXMucmVzdWx0X3NlbGVjdChldnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLm1vdXNlX29uX2NvbnRhaW5lciA9IGZhbHNlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDEzOlxuICAgICAgICAgIGlmICh0aGlzLnJlc3VsdHNfc2hvd2luZykge1xuICAgICAgICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDI3OlxuICAgICAgICAgIGlmICh0aGlzLnJlc3VsdHNfc2hvd2luZykge1xuICAgICAgICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDMyOlxuICAgICAgICAgIGlmICh0aGlzLmRpc2FibGVfc2VhcmNoKSB7XG4gICAgICAgICAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgMzg6XG4gICAgICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgdGhpcy5rZXl1cF9hcnJvdygpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDQwOlxuICAgICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgIHRoaXMua2V5ZG93bl9hcnJvdygpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUua2V5dXBfY2hlY2tlciA9IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgdmFyIHJlZiwgc3Ryb2tlO1xuICAgICAgc3Ryb2tlID0gKHJlZiA9IGV2dC53aGljaCkgIT0gbnVsbCA/IHJlZiA6IGV2dC5rZXlDb2RlO1xuICAgICAgdGhpcy5zZWFyY2hfZmllbGRfc2NhbGUoKTtcbiAgICAgIHN3aXRjaCAoc3Ryb2tlKSB7XG4gICAgICAgIGNhc2UgODpcbiAgICAgICAgICBpZiAodGhpcy5pc19tdWx0aXBsZSAmJiB0aGlzLmJhY2tzdHJva2VfbGVuZ3RoIDwgMSAmJiB0aGlzLmNob2ljZXNfY291bnQoKSA+IDApIHtcbiAgICAgICAgICAgIHRoaXMua2V5ZG93bl9iYWNrc3Ryb2tlKCk7XG4gICAgICAgICAgfSBlbHNlIGlmICghdGhpcy5wZW5kaW5nX2JhY2tzdHJva2UpIHtcbiAgICAgICAgICAgIHRoaXMucmVzdWx0X2NsZWFyX2hpZ2hsaWdodCgpO1xuICAgICAgICAgICAgdGhpcy5yZXN1bHRzX3NlYXJjaCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAxMzpcbiAgICAgICAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICBpZiAodGhpcy5yZXN1bHRzX3Nob3dpbmcpIHtcbiAgICAgICAgICAgIHRoaXMucmVzdWx0X3NlbGVjdChldnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAyNzpcbiAgICAgICAgICBpZiAodGhpcy5yZXN1bHRzX3Nob3dpbmcpIHtcbiAgICAgICAgICAgIHRoaXMucmVzdWx0c19oaWRlKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDk6XG4gICAgICAgIGNhc2UgMTY6XG4gICAgICAgIGNhc2UgMTc6XG4gICAgICAgIGNhc2UgMTg6XG4gICAgICAgIGNhc2UgMzg6XG4gICAgICAgIGNhc2UgNDA6XG4gICAgICAgIGNhc2UgOTE6XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgdGhpcy5yZXN1bHRzX3NlYXJjaCgpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUuY2xpcGJvYXJkX2V2ZW50X2NoZWNrZXIgPSBmdW5jdGlvbihldnQpIHtcbiAgICAgIGlmICh0aGlzLmlzX2Rpc2FibGVkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHJldHVybiBzZXRUaW1lb3V0KCgoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHJldHVybiBfdGhpcy5yZXN1bHRzX3NlYXJjaCgpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpLCA1MCk7XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5jb250YWluZXJfd2lkdGggPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMud2lkdGggIT0gbnVsbCkge1xuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25zLndpZHRoO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZm9ybV9maWVsZC5vZmZzZXRXaWR0aCArIFwicHhcIjtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLmluY2x1ZGVfb3B0aW9uX2luX3Jlc3VsdHMgPSBmdW5jdGlvbihvcHRpb24pIHtcbiAgICAgIGlmICh0aGlzLmlzX211bHRpcGxlICYmICghdGhpcy5kaXNwbGF5X3NlbGVjdGVkX29wdGlvbnMgJiYgb3B0aW9uLnNlbGVjdGVkKSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICBpZiAoIXRoaXMuZGlzcGxheV9kaXNhYmxlZF9vcHRpb25zICYmIG9wdGlvbi5kaXNhYmxlZCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICBpZiAob3B0aW9uLmVtcHR5KSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUuc2VhcmNoX3Jlc3VsdHNfdG91Y2hzdGFydCA9IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgdGhpcy50b3VjaF9zdGFydGVkID0gdHJ1ZTtcbiAgICAgIHJldHVybiB0aGlzLnNlYXJjaF9yZXN1bHRzX21vdXNlb3ZlcihldnQpO1xuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUuc2VhcmNoX3Jlc3VsdHNfdG91Y2htb3ZlID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICB0aGlzLnRvdWNoX3N0YXJ0ZWQgPSBmYWxzZTtcbiAgICAgIHJldHVybiB0aGlzLnNlYXJjaF9yZXN1bHRzX21vdXNlb3V0KGV2dCk7XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5zZWFyY2hfcmVzdWx0c190b3VjaGVuZCA9IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgaWYgKHRoaXMudG91Y2hfc3RhcnRlZCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2hfcmVzdWx0c19tb3VzZXVwKGV2dCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5vdXRlckhUTUwgPSBmdW5jdGlvbihlbGVtZW50KSB7XG4gICAgICB2YXIgdG1wO1xuICAgICAgaWYgKGVsZW1lbnQub3V0ZXJIVE1MKSB7XG4gICAgICAgIHJldHVybiBlbGVtZW50Lm91dGVySFRNTDtcbiAgICAgIH1cbiAgICAgIHRtcCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgICB0bXAuYXBwZW5kQ2hpbGQoZWxlbWVudCk7XG4gICAgICByZXR1cm4gdG1wLmlubmVySFRNTDtcbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4ucHJvdG90eXBlLmdldF9zaW5nbGVfaHRtbCA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIFwiPGEgY2xhc3M9XFxcImNob3Nlbi1zaW5nbGUgY2hvc2VuLWRlZmF1bHRcXFwiPlxcbiAgPHNwYW4+XCIgKyB0aGlzLmRlZmF1bHRfdGV4dCArIFwiPC9zcGFuPlxcbiAgPGRpdj48Yj48L2I+PC9kaXY+XFxuPC9hPlxcbjxkaXYgY2xhc3M9XFxcImNob3Nlbi1kcm9wXFxcIj5cXG4gIDxkaXYgY2xhc3M9XFxcImNob3Nlbi1zZWFyY2hcXFwiPlxcbiAgICA8aW5wdXQgY2xhc3M9XFxcImNob3Nlbi1zZWFyY2gtaW5wdXRcXFwiIHR5cGU9XFxcInRleHRcXFwiIGF1dG9jb21wbGV0ZT1cXFwib2ZmXFxcIiAvPlxcbiAgPC9kaXY+XFxuICA8dWwgY2xhc3M9XFxcImNob3Nlbi1yZXN1bHRzXFxcIj48L3VsPlxcbjwvZGl2PlwiO1xuICAgIH07XG5cbiAgICBBYnN0cmFjdENob3Nlbi5wcm90b3R5cGUuZ2V0X211bHRpX2h0bWwgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBcIjx1bCBjbGFzcz1cXFwiY2hvc2VuLWNob2ljZXNcXFwiPlxcbiAgPGxpIGNsYXNzPVxcXCJzZWFyY2gtZmllbGRcXFwiPlxcbiAgICA8aW5wdXQgY2xhc3M9XFxcImNob3Nlbi1zZWFyY2gtaW5wdXRcXFwiIHR5cGU9XFxcInRleHRcXFwiIGF1dG9jb21wbGV0ZT1cXFwib2ZmXFxcIiB2YWx1ZT1cXFwiXCIgKyB0aGlzLmRlZmF1bHRfdGV4dCArIFwiXFxcIiAvPlxcbiAgPC9saT5cXG48L3VsPlxcbjxkaXYgY2xhc3M9XFxcImNob3Nlbi1kcm9wXFxcIj5cXG4gIDx1bCBjbGFzcz1cXFwiY2hvc2VuLXJlc3VsdHNcXFwiPjwvdWw+XFxuPC9kaXY+XCI7XG4gICAgfTtcblxuICAgIEFic3RyYWN0Q2hvc2VuLnByb3RvdHlwZS5nZXRfbm9fcmVzdWx0c19odG1sID0gZnVuY3Rpb24odGVybXMpIHtcbiAgICAgIHJldHVybiBcIjxsaSBjbGFzcz1cXFwibm8tcmVzdWx0c1xcXCI+XFxuICBcIiArIHRoaXMucmVzdWx0c19ub25lX2ZvdW5kICsgXCIgPHNwYW4+XCIgKyAodGhpcy5lc2NhcGVfaHRtbCh0ZXJtcykpICsgXCI8L3NwYW4+XFxuPC9saT5cIjtcbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4uYnJvd3Nlcl9pc19zdXBwb3J0ZWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmIChcIk1pY3Jvc29mdCBJbnRlcm5ldCBFeHBsb3JlclwiID09PSB3aW5kb3cubmF2aWdhdG9yLmFwcE5hbWUpIHtcbiAgICAgICAgcmV0dXJuIGRvY3VtZW50LmRvY3VtZW50TW9kZSA+PSA4O1xuICAgICAgfVxuICAgICAgaWYgKC9pUChvZHxob25lKS9pLnRlc3Qod2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQpIHx8IC9JRU1vYmlsZS9pLnRlc3Qod2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQpIHx8IC9XaW5kb3dzIFBob25lL2kudGVzdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudCkgfHwgL0JsYWNrQmVycnkvaS50ZXN0KHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50KSB8fCAvQkIxMC9pLnRlc3Qod2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQpIHx8IC9BbmRyb2lkLipNb2JpbGUvaS50ZXN0KHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50KSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9O1xuXG4gICAgQWJzdHJhY3RDaG9zZW4uZGVmYXVsdF9tdWx0aXBsZV90ZXh0ID0gXCJTZWxlY3QgU29tZSBPcHRpb25zXCI7XG5cbiAgICBBYnN0cmFjdENob3Nlbi5kZWZhdWx0X3NpbmdsZV90ZXh0ID0gXCJTZWxlY3QgYW4gT3B0aW9uXCI7XG5cbiAgICBBYnN0cmFjdENob3Nlbi5kZWZhdWx0X25vX3Jlc3VsdF90ZXh0ID0gXCJObyByZXN1bHRzIG1hdGNoXCI7XG5cbiAgICByZXR1cm4gQWJzdHJhY3RDaG9zZW47XG5cbiAgfSkoKTtcblxuICAkID0galF1ZXJ5O1xuXG4gICQuZm4uZXh0ZW5kKHtcbiAgICBjaG9zZW46IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgIGlmICghQWJzdHJhY3RDaG9zZW4uYnJvd3Nlcl9pc19zdXBwb3J0ZWQoKSkge1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oaW5wdXRfZmllbGQpIHtcbiAgICAgICAgdmFyICR0aGlzLCBjaG9zZW47XG4gICAgICAgICR0aGlzID0gJCh0aGlzKTtcbiAgICAgICAgY2hvc2VuID0gJHRoaXMuZGF0YSgnY2hvc2VuJyk7XG4gICAgICAgIGlmIChvcHRpb25zID09PSAnZGVzdHJveScpIHtcbiAgICAgICAgICBpZiAoY2hvc2VuIGluc3RhbmNlb2YgQ2hvc2VuKSB7XG4gICAgICAgICAgICBjaG9zZW4uZGVzdHJveSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCEoY2hvc2VuIGluc3RhbmNlb2YgQ2hvc2VuKSkge1xuICAgICAgICAgICR0aGlzLmRhdGEoJ2Nob3NlbicsIG5ldyBDaG9zZW4odGhpcywgb3B0aW9ucykpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH0pO1xuXG4gIENob3NlbiA9IChmdW5jdGlvbihzdXBlckNsYXNzKSB7XG4gICAgZXh0ZW5kKENob3Nlbiwgc3VwZXJDbGFzcyk7XG5cbiAgICBmdW5jdGlvbiBDaG9zZW4oKSB7XG4gICAgICByZXR1cm4gQ2hvc2VuLl9fc3VwZXJfXy5jb25zdHJ1Y3Rvci5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgIH1cblxuICAgIENob3Nlbi5wcm90b3R5cGUuc2V0dXAgPSBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMuZm9ybV9maWVsZF9qcSA9ICQodGhpcy5mb3JtX2ZpZWxkKTtcbiAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRfc2VsZWN0ZWRJbmRleCA9IHRoaXMuZm9ybV9maWVsZC5zZWxlY3RlZEluZGV4O1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnNldF91cF9odG1sID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgY29udGFpbmVyX2NsYXNzZXMsIGNvbnRhaW5lcl9wcm9wcztcbiAgICAgIGNvbnRhaW5lcl9jbGFzc2VzID0gW1wiY2hvc2VuLWNvbnRhaW5lclwiXTtcbiAgICAgIGNvbnRhaW5lcl9jbGFzc2VzLnB1c2goXCJjaG9zZW4tY29udGFpbmVyLVwiICsgKHRoaXMuaXNfbXVsdGlwbGUgPyBcIm11bHRpXCIgOiBcInNpbmdsZVwiKSk7XG4gICAgICBpZiAodGhpcy5pbmhlcml0X3NlbGVjdF9jbGFzc2VzICYmIHRoaXMuZm9ybV9maWVsZC5jbGFzc05hbWUpIHtcbiAgICAgICAgY29udGFpbmVyX2NsYXNzZXMucHVzaCh0aGlzLmZvcm1fZmllbGQuY2xhc3NOYW1lKTtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLmlzX3J0bCkge1xuICAgICAgICBjb250YWluZXJfY2xhc3Nlcy5wdXNoKFwiY2hvc2VuLXJ0bFwiKTtcbiAgICAgIH1cbiAgICAgIGNvbnRhaW5lcl9wcm9wcyA9IHtcbiAgICAgICAgJ2NsYXNzJzogY29udGFpbmVyX2NsYXNzZXMuam9pbignICcpLFxuICAgICAgICAndGl0bGUnOiB0aGlzLmZvcm1fZmllbGQudGl0bGVcbiAgICAgIH07XG4gICAgICBpZiAodGhpcy5mb3JtX2ZpZWxkLmlkLmxlbmd0aCkge1xuICAgICAgICBjb250YWluZXJfcHJvcHMuaWQgPSB0aGlzLmZvcm1fZmllbGQuaWQucmVwbGFjZSgvW15cXHddL2csICdfJykgKyBcIl9jaG9zZW5cIjtcbiAgICAgIH1cbiAgICAgIHRoaXMuY29udGFpbmVyID0gJChcIjxkaXYgLz5cIiwgY29udGFpbmVyX3Byb3BzKTtcbiAgICAgIHRoaXMuY29udGFpbmVyLndpZHRoKHRoaXMuY29udGFpbmVyX3dpZHRoKCkpO1xuICAgICAgaWYgKHRoaXMuaXNfbXVsdGlwbGUpIHtcbiAgICAgICAgdGhpcy5jb250YWluZXIuaHRtbCh0aGlzLmdldF9tdWx0aV9odG1sKCkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jb250YWluZXIuaHRtbCh0aGlzLmdldF9zaW5nbGVfaHRtbCgpKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuZm9ybV9maWVsZF9qcS5oaWRlKCkuYWZ0ZXIodGhpcy5jb250YWluZXIpO1xuICAgICAgdGhpcy5kcm9wZG93biA9IHRoaXMuY29udGFpbmVyLmZpbmQoJ2Rpdi5jaG9zZW4tZHJvcCcpLmZpcnN0KCk7XG4gICAgICB0aGlzLnNlYXJjaF9maWVsZCA9IHRoaXMuY29udGFpbmVyLmZpbmQoJ2lucHV0JykuZmlyc3QoKTtcbiAgICAgIHRoaXMuc2VhcmNoX3Jlc3VsdHMgPSB0aGlzLmNvbnRhaW5lci5maW5kKCd1bC5jaG9zZW4tcmVzdWx0cycpLmZpcnN0KCk7XG4gICAgICB0aGlzLnNlYXJjaF9maWVsZF9zY2FsZSgpO1xuICAgICAgdGhpcy5zZWFyY2hfbm9fcmVzdWx0cyA9IHRoaXMuY29udGFpbmVyLmZpbmQoJ2xpLm5vLXJlc3VsdHMnKS5maXJzdCgpO1xuICAgICAgaWYgKHRoaXMuaXNfbXVsdGlwbGUpIHtcbiAgICAgICAgdGhpcy5zZWFyY2hfY2hvaWNlcyA9IHRoaXMuY29udGFpbmVyLmZpbmQoJ3VsLmNob3Nlbi1jaG9pY2VzJykuZmlyc3QoKTtcbiAgICAgICAgdGhpcy5zZWFyY2hfY29udGFpbmVyID0gdGhpcy5jb250YWluZXIuZmluZCgnbGkuc2VhcmNoLWZpZWxkJykuZmlyc3QoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuc2VhcmNoX2NvbnRhaW5lciA9IHRoaXMuY29udGFpbmVyLmZpbmQoJ2Rpdi5jaG9zZW4tc2VhcmNoJykuZmlyc3QoKTtcbiAgICAgICAgdGhpcy5zZWxlY3RlZF9pdGVtID0gdGhpcy5jb250YWluZXIuZmluZCgnLmNob3Nlbi1zaW5nbGUnKS5maXJzdCgpO1xuICAgICAgfVxuICAgICAgdGhpcy5yZXN1bHRzX2J1aWxkKCk7XG4gICAgICB0aGlzLnNldF90YWJfaW5kZXgoKTtcbiAgICAgIHJldHVybiB0aGlzLnNldF9sYWJlbF9iZWhhdmlvcigpO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLm9uX3JlYWR5ID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5mb3JtX2ZpZWxkX2pxLnRyaWdnZXIoXCJjaG9zZW46cmVhZHlcIiwge1xuICAgICAgICBjaG9zZW46IHRoaXNcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnJlZ2lzdGVyX29ic2VydmVycyA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5jb250YWluZXIub24oJ3RvdWNoc3RhcnQuY2hvc2VuJywgKGZ1bmN0aW9uKF90aGlzKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICBfdGhpcy5jb250YWluZXJfbW91c2Vkb3duKGV2dCk7XG4gICAgICAgIH07XG4gICAgICB9KSh0aGlzKSk7XG4gICAgICB0aGlzLmNvbnRhaW5lci5vbigndG91Y2hlbmQuY2hvc2VuJywgKGZ1bmN0aW9uKF90aGlzKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICBfdGhpcy5jb250YWluZXJfbW91c2V1cChldnQpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpO1xuICAgICAgdGhpcy5jb250YWluZXIub24oJ21vdXNlZG93bi5jaG9zZW4nLCAoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIF90aGlzLmNvbnRhaW5lcl9tb3VzZWRvd24oZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuY29udGFpbmVyLm9uKCdtb3VzZXVwLmNob3NlbicsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgICAgX3RoaXMuY29udGFpbmVyX21vdXNldXAoZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuY29udGFpbmVyLm9uKCdtb3VzZWVudGVyLmNob3NlbicsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgICAgX3RoaXMubW91c2VfZW50ZXIoZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuY29udGFpbmVyLm9uKCdtb3VzZWxlYXZlLmNob3NlbicsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgICAgX3RoaXMubW91c2VfbGVhdmUoZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuc2VhcmNoX3Jlc3VsdHMub24oJ21vdXNldXAuY2hvc2VuJywgKGZ1bmN0aW9uKF90aGlzKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICBfdGhpcy5zZWFyY2hfcmVzdWx0c19tb3VzZXVwKGV2dCk7XG4gICAgICAgIH07XG4gICAgICB9KSh0aGlzKSk7XG4gICAgICB0aGlzLnNlYXJjaF9yZXN1bHRzLm9uKCdtb3VzZW92ZXIuY2hvc2VuJywgKGZ1bmN0aW9uKF90aGlzKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICBfdGhpcy5zZWFyY2hfcmVzdWx0c19tb3VzZW92ZXIoZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuc2VhcmNoX3Jlc3VsdHMub24oJ21vdXNlb3V0LmNob3NlbicsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgICAgX3RoaXMuc2VhcmNoX3Jlc3VsdHNfbW91c2VvdXQoZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuc2VhcmNoX3Jlc3VsdHMub24oJ21vdXNld2hlZWwuY2hvc2VuIERPTU1vdXNlU2Nyb2xsLmNob3NlbicsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgICAgX3RoaXMuc2VhcmNoX3Jlc3VsdHNfbW91c2V3aGVlbChldnQpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpO1xuICAgICAgdGhpcy5zZWFyY2hfcmVzdWx0cy5vbigndG91Y2hzdGFydC5jaG9zZW4nLCAoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIF90aGlzLnNlYXJjaF9yZXN1bHRzX3RvdWNoc3RhcnQoZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuc2VhcmNoX3Jlc3VsdHMub24oJ3RvdWNobW92ZS5jaG9zZW4nLCAoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIF90aGlzLnNlYXJjaF9yZXN1bHRzX3RvdWNobW92ZShldnQpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpO1xuICAgICAgdGhpcy5zZWFyY2hfcmVzdWx0cy5vbigndG91Y2hlbmQuY2hvc2VuJywgKGZ1bmN0aW9uKF90aGlzKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICBfdGhpcy5zZWFyY2hfcmVzdWx0c190b3VjaGVuZChldnQpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpO1xuICAgICAgdGhpcy5mb3JtX2ZpZWxkX2pxLm9uKFwiY2hvc2VuOnVwZGF0ZWQuY2hvc2VuXCIsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgICAgX3RoaXMucmVzdWx0c191cGRhdGVfZmllbGQoZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuZm9ybV9maWVsZF9qcS5vbihcImNob3NlbjphY3RpdmF0ZS5jaG9zZW5cIiwgKGZ1bmN0aW9uKF90aGlzKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICBfdGhpcy5hY3RpdmF0ZV9maWVsZChldnQpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpO1xuICAgICAgdGhpcy5mb3JtX2ZpZWxkX2pxLm9uKFwiY2hvc2VuOm9wZW4uY2hvc2VuXCIsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgICAgX3RoaXMuY29udGFpbmVyX21vdXNlZG93bihldnQpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpO1xuICAgICAgdGhpcy5mb3JtX2ZpZWxkX2pxLm9uKFwiY2hvc2VuOmNsb3NlLmNob3NlblwiLCAoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIF90aGlzLmNsb3NlX2ZpZWxkKGV2dCk7XG4gICAgICAgIH07XG4gICAgICB9KSh0aGlzKSk7XG4gICAgICB0aGlzLnNlYXJjaF9maWVsZC5vbignYmx1ci5jaG9zZW4nLCAoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIF90aGlzLmlucHV0X2JsdXIoZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuc2VhcmNoX2ZpZWxkLm9uKCdrZXl1cC5jaG9zZW4nLCAoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIF90aGlzLmtleXVwX2NoZWNrZXIoZXZ0KTtcbiAgICAgICAgfTtcbiAgICAgIH0pKHRoaXMpKTtcbiAgICAgIHRoaXMuc2VhcmNoX2ZpZWxkLm9uKCdrZXlkb3duLmNob3NlbicsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgICAgX3RoaXMua2V5ZG93bl9jaGVja2VyKGV2dCk7XG4gICAgICAgIH07XG4gICAgICB9KSh0aGlzKSk7XG4gICAgICB0aGlzLnNlYXJjaF9maWVsZC5vbignZm9jdXMuY2hvc2VuJywgKGZ1bmN0aW9uKF90aGlzKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICBfdGhpcy5pbnB1dF9mb2N1cyhldnQpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpO1xuICAgICAgdGhpcy5zZWFyY2hfZmllbGQub24oJ2N1dC5jaG9zZW4nLCAoZnVuY3Rpb24oX3RoaXMpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIF90aGlzLmNsaXBib2FyZF9ldmVudF9jaGVja2VyKGV2dCk7XG4gICAgICAgIH07XG4gICAgICB9KSh0aGlzKSk7XG4gICAgICB0aGlzLnNlYXJjaF9maWVsZC5vbigncGFzdGUuY2hvc2VuJywgKGZ1bmN0aW9uKF90aGlzKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICBfdGhpcy5jbGlwYm9hcmRfZXZlbnRfY2hlY2tlcihldnQpO1xuICAgICAgICB9O1xuICAgICAgfSkodGhpcykpO1xuICAgICAgaWYgKHRoaXMuaXNfbXVsdGlwbGUpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhcmNoX2Nob2ljZXMub24oJ2NsaWNrLmNob3NlbicsIChmdW5jdGlvbihfdGhpcykge1xuICAgICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICAgIF90aGlzLmNob2ljZXNfY2xpY2soZXZ0KTtcbiAgICAgICAgICB9O1xuICAgICAgICB9KSh0aGlzKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5jb250YWluZXIub24oJ2NsaWNrLmNob3NlbicsIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24oKSB7XG4gICAgICAkKHRoaXMuY29udGFpbmVyWzBdLm93bmVyRG9jdW1lbnQpLm9mZignY2xpY2suY2hvc2VuJywgdGhpcy5jbGlja190ZXN0X2FjdGlvbik7XG4gICAgICBpZiAodGhpcy5mb3JtX2ZpZWxkX2xhYmVsLmxlbmd0aCA+IDApIHtcbiAgICAgICAgdGhpcy5mb3JtX2ZpZWxkX2xhYmVsLm9mZignY2xpY2suY2hvc2VuJyk7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5zZWFyY2hfZmllbGRbMF0udGFiSW5kZXgpIHtcbiAgICAgICAgdGhpcy5mb3JtX2ZpZWxkX2pxWzBdLnRhYkluZGV4ID0gdGhpcy5zZWFyY2hfZmllbGRbMF0udGFiSW5kZXg7XG4gICAgICB9XG4gICAgICB0aGlzLmNvbnRhaW5lci5yZW1vdmUoKTtcbiAgICAgIHRoaXMuZm9ybV9maWVsZF9qcS5yZW1vdmVEYXRhKCdjaG9zZW4nKTtcbiAgICAgIHJldHVybiB0aGlzLmZvcm1fZmllbGRfanEuc2hvdygpO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnNlYXJjaF9maWVsZF9kaXNhYmxlZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5pc19kaXNhYmxlZCA9IHRoaXMuZm9ybV9maWVsZC5kaXNhYmxlZCB8fCB0aGlzLmZvcm1fZmllbGRfanEucGFyZW50cygnZmllbGRzZXQnKS5pcygnOmRpc2FibGVkJyk7XG4gICAgICB0aGlzLmNvbnRhaW5lci50b2dnbGVDbGFzcygnY2hvc2VuLWRpc2FibGVkJywgdGhpcy5pc19kaXNhYmxlZCk7XG4gICAgICB0aGlzLnNlYXJjaF9maWVsZFswXS5kaXNhYmxlZCA9IHRoaXMuaXNfZGlzYWJsZWQ7XG4gICAgICBpZiAoIXRoaXMuaXNfbXVsdGlwbGUpIHtcbiAgICAgICAgdGhpcy5zZWxlY3RlZF9pdGVtLm9mZignZm9jdXMuY2hvc2VuJywgdGhpcy5hY3RpdmF0ZV9maWVsZCk7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5pc19kaXNhYmxlZCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jbG9zZV9maWVsZCgpO1xuICAgICAgfSBlbHNlIGlmICghdGhpcy5pc19tdWx0aXBsZSkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZF9pdGVtLm9uKCdmb2N1cy5jaG9zZW4nLCB0aGlzLmFjdGl2YXRlX2ZpZWxkKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5jb250YWluZXJfbW91c2Vkb3duID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICB2YXIgcmVmO1xuICAgICAgaWYgKHRoaXMuaXNfZGlzYWJsZWQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgaWYgKGV2dCAmJiAoKHJlZiA9IGV2dC50eXBlKSA9PT0gJ21vdXNlZG93bicgfHwgcmVmID09PSAndG91Y2hzdGFydCcpICYmICF0aGlzLnJlc3VsdHNfc2hvd2luZykge1xuICAgICAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH1cbiAgICAgIGlmICghKChldnQgIT0gbnVsbCkgJiYgKCQoZXZ0LnRhcmdldCkpLmhhc0NsYXNzKFwic2VhcmNoLWNob2ljZS1jbG9zZVwiKSkpIHtcbiAgICAgICAgaWYgKCF0aGlzLmFjdGl2ZV9maWVsZCkge1xuICAgICAgICAgIGlmICh0aGlzLmlzX211bHRpcGxlKSB7XG4gICAgICAgICAgICB0aGlzLnNlYXJjaF9maWVsZC52YWwoXCJcIik7XG4gICAgICAgICAgfVxuICAgICAgICAgICQodGhpcy5jb250YWluZXJbMF0ub3duZXJEb2N1bWVudCkub24oJ2NsaWNrLmNob3NlbicsIHRoaXMuY2xpY2tfdGVzdF9hY3Rpb24pO1xuICAgICAgICAgIHRoaXMucmVzdWx0c19zaG93KCk7XG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuaXNfbXVsdGlwbGUgJiYgZXZ0ICYmICgoJChldnQudGFyZ2V0KVswXSA9PT0gdGhpcy5zZWxlY3RlZF9pdGVtWzBdKSB8fCAkKGV2dC50YXJnZXQpLnBhcmVudHMoXCJhLmNob3Nlbi1zaW5nbGVcIikubGVuZ3RoKSkge1xuICAgICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgIHRoaXMucmVzdWx0c190b2dnbGUoKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5hY3RpdmF0ZV9maWVsZCgpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLmNvbnRhaW5lcl9tb3VzZXVwID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICBpZiAoZXZ0LnRhcmdldC5ub2RlTmFtZSA9PT0gXCJBQkJSXCIgJiYgIXRoaXMuaXNfZGlzYWJsZWQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVzdWx0c19yZXNldChldnQpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnNlYXJjaF9yZXN1bHRzX21vdXNld2hlZWwgPSBmdW5jdGlvbihldnQpIHtcbiAgICAgIHZhciBkZWx0YTtcbiAgICAgIGlmIChldnQub3JpZ2luYWxFdmVudCkge1xuICAgICAgICBkZWx0YSA9IGV2dC5vcmlnaW5hbEV2ZW50LmRlbHRhWSB8fCAtZXZ0Lm9yaWdpbmFsRXZlbnQud2hlZWxEZWx0YSB8fCBldnQub3JpZ2luYWxFdmVudC5kZXRhaWw7XG4gICAgICB9XG4gICAgICBpZiAoZGVsdGEgIT0gbnVsbCkge1xuICAgICAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgaWYgKGV2dC50eXBlID09PSAnRE9NTW91c2VTY3JvbGwnKSB7XG4gICAgICAgICAgZGVsdGEgPSBkZWx0YSAqIDQwO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLnNlYXJjaF9yZXN1bHRzLnNjcm9sbFRvcChkZWx0YSArIHRoaXMuc2VhcmNoX3Jlc3VsdHMuc2Nyb2xsVG9wKCkpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLmJsdXJfdGVzdCA9IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgaWYgKCF0aGlzLmFjdGl2ZV9maWVsZCAmJiB0aGlzLmNvbnRhaW5lci5oYXNDbGFzcyhcImNob3Nlbi1jb250YWluZXItYWN0aXZlXCIpKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNsb3NlX2ZpZWxkKCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUuY2xvc2VfZmllbGQgPSBmdW5jdGlvbigpIHtcbiAgICAgICQodGhpcy5jb250YWluZXJbMF0ub3duZXJEb2N1bWVudCkub2ZmKFwiY2xpY2suY2hvc2VuXCIsIHRoaXMuY2xpY2tfdGVzdF9hY3Rpb24pO1xuICAgICAgdGhpcy5hY3RpdmVfZmllbGQgPSBmYWxzZTtcbiAgICAgIHRoaXMucmVzdWx0c19oaWRlKCk7XG4gICAgICB0aGlzLmNvbnRhaW5lci5yZW1vdmVDbGFzcyhcImNob3Nlbi1jb250YWluZXItYWN0aXZlXCIpO1xuICAgICAgdGhpcy5jbGVhcl9iYWNrc3Ryb2tlKCk7XG4gICAgICB0aGlzLnNob3dfc2VhcmNoX2ZpZWxkX2RlZmF1bHQoKTtcbiAgICAgIHRoaXMuc2VhcmNoX2ZpZWxkX3NjYWxlKCk7XG4gICAgICByZXR1cm4gdGhpcy5zZWFyY2hfZmllbGQuYmx1cigpO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLmFjdGl2YXRlX2ZpZWxkID0gZnVuY3Rpb24oKSB7XG4gICAgICBpZiAodGhpcy5pc19kaXNhYmxlZCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB0aGlzLmNvbnRhaW5lci5hZGRDbGFzcyhcImNob3Nlbi1jb250YWluZXItYWN0aXZlXCIpO1xuICAgICAgdGhpcy5hY3RpdmVfZmllbGQgPSB0cnVlO1xuICAgICAgdGhpcy5zZWFyY2hfZmllbGQudmFsKHRoaXMuc2VhcmNoX2ZpZWxkLnZhbCgpKTtcbiAgICAgIHJldHVybiB0aGlzLnNlYXJjaF9maWVsZC5mb2N1cygpO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnRlc3RfYWN0aXZlX2NsaWNrID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICB2YXIgYWN0aXZlX2NvbnRhaW5lcjtcbiAgICAgIGFjdGl2ZV9jb250YWluZXIgPSAkKGV2dC50YXJnZXQpLmNsb3Nlc3QoJy5jaG9zZW4tY29udGFpbmVyJyk7XG4gICAgICBpZiAoYWN0aXZlX2NvbnRhaW5lci5sZW5ndGggJiYgdGhpcy5jb250YWluZXJbMF0gPT09IGFjdGl2ZV9jb250YWluZXJbMF0pIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYWN0aXZlX2ZpZWxkID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNsb3NlX2ZpZWxkKCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUucmVzdWx0c19idWlsZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5wYXJzaW5nID0gdHJ1ZTtcbiAgICAgIHRoaXMuc2VsZWN0ZWRfb3B0aW9uX2NvdW50ID0gbnVsbDtcbiAgICAgIHRoaXMucmVzdWx0c19kYXRhID0gU2VsZWN0UGFyc2VyLnNlbGVjdF90b19hcnJheSh0aGlzLmZvcm1fZmllbGQpO1xuICAgICAgaWYgKHRoaXMuaXNfbXVsdGlwbGUpIHtcbiAgICAgICAgdGhpcy5zZWFyY2hfY2hvaWNlcy5maW5kKFwibGkuc2VhcmNoLWNob2ljZVwiKS5yZW1vdmUoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuc2luZ2xlX3NldF9zZWxlY3RlZF90ZXh0KCk7XG4gICAgICAgIGlmICh0aGlzLmRpc2FibGVfc2VhcmNoIHx8IHRoaXMuZm9ybV9maWVsZC5vcHRpb25zLmxlbmd0aCA8PSB0aGlzLmRpc2FibGVfc2VhcmNoX3RocmVzaG9sZCkge1xuICAgICAgICAgIHRoaXMuc2VhcmNoX2ZpZWxkWzBdLnJlYWRPbmx5ID0gdHJ1ZTtcbiAgICAgICAgICB0aGlzLmNvbnRhaW5lci5hZGRDbGFzcyhcImNob3Nlbi1jb250YWluZXItc2luZ2xlLW5vc2VhcmNoXCIpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuc2VhcmNoX2ZpZWxkWzBdLnJlYWRPbmx5ID0gZmFsc2U7XG4gICAgICAgICAgdGhpcy5jb250YWluZXIucmVtb3ZlQ2xhc3MoXCJjaG9zZW4tY29udGFpbmVyLXNpbmdsZS1ub3NlYXJjaFwiKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgdGhpcy51cGRhdGVfcmVzdWx0c19jb250ZW50KHRoaXMucmVzdWx0c19vcHRpb25fYnVpbGQoe1xuICAgICAgICBmaXJzdDogdHJ1ZVxuICAgICAgfSkpO1xuICAgICAgdGhpcy5zZWFyY2hfZmllbGRfZGlzYWJsZWQoKTtcbiAgICAgIHRoaXMuc2hvd19zZWFyY2hfZmllbGRfZGVmYXVsdCgpO1xuICAgICAgdGhpcy5zZWFyY2hfZmllbGRfc2NhbGUoKTtcbiAgICAgIHJldHVybiB0aGlzLnBhcnNpbmcgPSBmYWxzZTtcbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5yZXN1bHRfZG9faGlnaGxpZ2h0ID0gZnVuY3Rpb24oZWwpIHtcbiAgICAgIHZhciBoaWdoX2JvdHRvbSwgaGlnaF90b3AsIG1heEhlaWdodCwgdmlzaWJsZV9ib3R0b20sIHZpc2libGVfdG9wO1xuICAgICAgaWYgKGVsLmxlbmd0aCkge1xuICAgICAgICB0aGlzLnJlc3VsdF9jbGVhcl9oaWdobGlnaHQoKTtcbiAgICAgICAgdGhpcy5yZXN1bHRfaGlnaGxpZ2h0ID0gZWw7XG4gICAgICAgIHRoaXMucmVzdWx0X2hpZ2hsaWdodC5hZGRDbGFzcyhcImhpZ2hsaWdodGVkXCIpO1xuICAgICAgICBtYXhIZWlnaHQgPSBwYXJzZUludCh0aGlzLnNlYXJjaF9yZXN1bHRzLmNzcyhcIm1heEhlaWdodFwiKSwgMTApO1xuICAgICAgICB2aXNpYmxlX3RvcCA9IHRoaXMuc2VhcmNoX3Jlc3VsdHMuc2Nyb2xsVG9wKCk7XG4gICAgICAgIHZpc2libGVfYm90dG9tID0gbWF4SGVpZ2h0ICsgdmlzaWJsZV90b3A7XG4gICAgICAgIGhpZ2hfdG9wID0gdGhpcy5yZXN1bHRfaGlnaGxpZ2h0LnBvc2l0aW9uKCkudG9wICsgdGhpcy5zZWFyY2hfcmVzdWx0cy5zY3JvbGxUb3AoKTtcbiAgICAgICAgaGlnaF9ib3R0b20gPSBoaWdoX3RvcCArIHRoaXMucmVzdWx0X2hpZ2hsaWdodC5vdXRlckhlaWdodCgpO1xuICAgICAgICBpZiAoaGlnaF9ib3R0b20gPj0gdmlzaWJsZV9ib3R0b20pIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2hfcmVzdWx0cy5zY3JvbGxUb3AoKGhpZ2hfYm90dG9tIC0gbWF4SGVpZ2h0KSA+IDAgPyBoaWdoX2JvdHRvbSAtIG1heEhlaWdodCA6IDApO1xuICAgICAgICB9IGVsc2UgaWYgKGhpZ2hfdG9wIDwgdmlzaWJsZV90b3ApIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2hfcmVzdWx0cy5zY3JvbGxUb3AoaGlnaF90b3ApO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUucmVzdWx0X2NsZWFyX2hpZ2hsaWdodCA9IGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKHRoaXMucmVzdWx0X2hpZ2hsaWdodCkge1xuICAgICAgICB0aGlzLnJlc3VsdF9oaWdobGlnaHQucmVtb3ZlQ2xhc3MoXCJoaWdobGlnaHRlZFwiKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLnJlc3VsdF9oaWdobGlnaHQgPSBudWxsO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnJlc3VsdHNfc2hvdyA9IGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKHRoaXMuaXNfbXVsdGlwbGUgJiYgdGhpcy5tYXhfc2VsZWN0ZWRfb3B0aW9ucyA8PSB0aGlzLmNob2ljZXNfY291bnQoKSkge1xuICAgICAgICB0aGlzLmZvcm1fZmllbGRfanEudHJpZ2dlcihcImNob3NlbjptYXhzZWxlY3RlZFwiLCB7XG4gICAgICAgICAgY2hvc2VuOiB0aGlzXG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICB0aGlzLmNvbnRhaW5lci5hZGRDbGFzcyhcImNob3Nlbi13aXRoLWRyb3BcIik7XG4gICAgICB0aGlzLnJlc3VsdHNfc2hvd2luZyA9IHRydWU7XG4gICAgICB0aGlzLnNlYXJjaF9maWVsZC5mb2N1cygpO1xuICAgICAgdGhpcy5zZWFyY2hfZmllbGQudmFsKHRoaXMuZ2V0X3NlYXJjaF9maWVsZF92YWx1ZSgpKTtcbiAgICAgIHRoaXMud2lubm93X3Jlc3VsdHMoKTtcbiAgICAgIHJldHVybiB0aGlzLmZvcm1fZmllbGRfanEudHJpZ2dlcihcImNob3NlbjpzaG93aW5nX2Ryb3Bkb3duXCIsIHtcbiAgICAgICAgY2hvc2VuOiB0aGlzXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS51cGRhdGVfcmVzdWx0c19jb250ZW50ID0gZnVuY3Rpb24oY29udGVudCkge1xuICAgICAgcmV0dXJuIHRoaXMuc2VhcmNoX3Jlc3VsdHMuaHRtbChjb250ZW50KTtcbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5yZXN1bHRzX2hpZGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmICh0aGlzLnJlc3VsdHNfc2hvd2luZykge1xuICAgICAgICB0aGlzLnJlc3VsdF9jbGVhcl9oaWdobGlnaHQoKTtcbiAgICAgICAgdGhpcy5jb250YWluZXIucmVtb3ZlQ2xhc3MoXCJjaG9zZW4td2l0aC1kcm9wXCIpO1xuICAgICAgICB0aGlzLmZvcm1fZmllbGRfanEudHJpZ2dlcihcImNob3NlbjpoaWRpbmdfZHJvcGRvd25cIiwge1xuICAgICAgICAgIGNob3NlbjogdGhpc1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLnJlc3VsdHNfc2hvd2luZyA9IGZhbHNlO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnNldF90YWJfaW5kZXggPSBmdW5jdGlvbihlbCkge1xuICAgICAgdmFyIHRpO1xuICAgICAgaWYgKHRoaXMuZm9ybV9maWVsZC50YWJJbmRleCkge1xuICAgICAgICB0aSA9IHRoaXMuZm9ybV9maWVsZC50YWJJbmRleDtcbiAgICAgICAgdGhpcy5mb3JtX2ZpZWxkLnRhYkluZGV4ID0gLTE7XG4gICAgICAgIHJldHVybiB0aGlzLnNlYXJjaF9maWVsZFswXS50YWJJbmRleCA9IHRpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnNldF9sYWJlbF9iZWhhdmlvciA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5mb3JtX2ZpZWxkX2xhYmVsID0gdGhpcy5mb3JtX2ZpZWxkX2pxLnBhcmVudHMoXCJsYWJlbFwiKTtcbiAgICAgIGlmICghdGhpcy5mb3JtX2ZpZWxkX2xhYmVsLmxlbmd0aCAmJiB0aGlzLmZvcm1fZmllbGQuaWQubGVuZ3RoKSB7XG4gICAgICAgIHRoaXMuZm9ybV9maWVsZF9sYWJlbCA9ICQoXCJsYWJlbFtmb3I9J1wiICsgdGhpcy5mb3JtX2ZpZWxkLmlkICsgXCInXVwiKTtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLmZvcm1fZmllbGRfbGFiZWwubGVuZ3RoID4gMCkge1xuICAgICAgICByZXR1cm4gdGhpcy5mb3JtX2ZpZWxkX2xhYmVsLm9uKCdjbGljay5jaG9zZW4nLCB0aGlzLmxhYmVsX2NsaWNrX2hhbmRsZXIpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnNob3dfc2VhcmNoX2ZpZWxkX2RlZmF1bHQgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmICh0aGlzLmlzX211bHRpcGxlICYmIHRoaXMuY2hvaWNlc19jb3VudCgpIDwgMSAmJiAhdGhpcy5hY3RpdmVfZmllbGQpIHtcbiAgICAgICAgdGhpcy5zZWFyY2hfZmllbGQudmFsKHRoaXMuZGVmYXVsdF90ZXh0KTtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhcmNoX2ZpZWxkLmFkZENsYXNzKFwiZGVmYXVsdFwiKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuc2VhcmNoX2ZpZWxkLnZhbChcIlwiKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhcmNoX2ZpZWxkLnJlbW92ZUNsYXNzKFwiZGVmYXVsdFwiKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5zZWFyY2hfcmVzdWx0c19tb3VzZXVwID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICB2YXIgdGFyZ2V0O1xuICAgICAgdGFyZ2V0ID0gJChldnQudGFyZ2V0KS5oYXNDbGFzcyhcImFjdGl2ZS1yZXN1bHRcIikgPyAkKGV2dC50YXJnZXQpIDogJChldnQudGFyZ2V0KS5wYXJlbnRzKFwiLmFjdGl2ZS1yZXN1bHRcIikuZmlyc3QoKTtcbiAgICAgIGlmICh0YXJnZXQubGVuZ3RoKSB7XG4gICAgICAgIHRoaXMucmVzdWx0X2hpZ2hsaWdodCA9IHRhcmdldDtcbiAgICAgICAgdGhpcy5yZXN1bHRfc2VsZWN0KGV2dCk7XG4gICAgICAgIHJldHVybiB0aGlzLnNlYXJjaF9maWVsZC5mb2N1cygpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnNlYXJjaF9yZXN1bHRzX21vdXNlb3ZlciA9IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgdmFyIHRhcmdldDtcbiAgICAgIHRhcmdldCA9ICQoZXZ0LnRhcmdldCkuaGFzQ2xhc3MoXCJhY3RpdmUtcmVzdWx0XCIpID8gJChldnQudGFyZ2V0KSA6ICQoZXZ0LnRhcmdldCkucGFyZW50cyhcIi5hY3RpdmUtcmVzdWx0XCIpLmZpcnN0KCk7XG4gICAgICBpZiAodGFyZ2V0KSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdF9kb19oaWdobGlnaHQodGFyZ2V0KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5zZWFyY2hfcmVzdWx0c19tb3VzZW91dCA9IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgaWYgKCQoZXZ0LnRhcmdldCkuaGFzQ2xhc3MoXCJhY3RpdmUtcmVzdWx0XCIpIHx8ICQoZXZ0LnRhcmdldCkucGFyZW50cygnLmFjdGl2ZS1yZXN1bHQnKS5maXJzdCgpKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdF9jbGVhcl9oaWdobGlnaHQoKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5jaG9pY2VfYnVpbGQgPSBmdW5jdGlvbihpdGVtKSB7XG4gICAgICB2YXIgY2hvaWNlLCBjbG9zZV9saW5rO1xuICAgICAgY2hvaWNlID0gJCgnPGxpIC8+Jywge1xuICAgICAgICBcImNsYXNzXCI6IFwic2VhcmNoLWNob2ljZVwiXG4gICAgICB9KS5odG1sKFwiPHNwYW4+XCIgKyAodGhpcy5jaG9pY2VfbGFiZWwoaXRlbSkpICsgXCI8L3NwYW4+XCIpO1xuICAgICAgaWYgKGl0ZW0uZGlzYWJsZWQpIHtcbiAgICAgICAgY2hvaWNlLmFkZENsYXNzKCdzZWFyY2gtY2hvaWNlLWRpc2FibGVkJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjbG9zZV9saW5rID0gJCgnPGEgLz4nLCB7XG4gICAgICAgICAgXCJjbGFzc1wiOiAnc2VhcmNoLWNob2ljZS1jbG9zZScsXG4gICAgICAgICAgJ2RhdGEtb3B0aW9uLWFycmF5LWluZGV4JzogaXRlbS5hcnJheV9pbmRleFxuICAgICAgICB9KTtcbiAgICAgICAgY2xvc2VfbGluay5vbignY2xpY2suY2hvc2VuJywgKGZ1bmN0aW9uKF90aGlzKSB7XG4gICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzLmNob2ljZV9kZXN0cm95X2xpbmtfY2xpY2soZXZ0KTtcbiAgICAgICAgICB9O1xuICAgICAgICB9KSh0aGlzKSk7XG4gICAgICAgIGNob2ljZS5hcHBlbmQoY2xvc2VfbGluayk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5zZWFyY2hfY29udGFpbmVyLmJlZm9yZShjaG9pY2UpO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLmNob2ljZV9kZXN0cm95X2xpbmtfY2xpY2sgPSBmdW5jdGlvbihldnQpIHtcbiAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgZXZ0LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgaWYgKCF0aGlzLmlzX2Rpc2FibGVkKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNob2ljZV9kZXN0cm95KCQoZXZ0LnRhcmdldCkpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLmNob2ljZV9kZXN0cm95ID0gZnVuY3Rpb24obGluaykge1xuICAgICAgaWYgKHRoaXMucmVzdWx0X2Rlc2VsZWN0KGxpbmtbMF0uZ2V0QXR0cmlidXRlKFwiZGF0YS1vcHRpb24tYXJyYXktaW5kZXhcIikpKSB7XG4gICAgICAgIGlmICh0aGlzLmFjdGl2ZV9maWVsZCkge1xuICAgICAgICAgIHRoaXMuc2VhcmNoX2ZpZWxkLmZvY3VzKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5zaG93X3NlYXJjaF9maWVsZF9kZWZhdWx0KCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuaXNfbXVsdGlwbGUgJiYgdGhpcy5jaG9pY2VzX2NvdW50KCkgPiAwICYmIHRoaXMuZ2V0X3NlYXJjaF9maWVsZF92YWx1ZSgpLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICB0aGlzLnJlc3VsdHNfaGlkZSgpO1xuICAgICAgICB9XG4gICAgICAgIGxpbmsucGFyZW50cygnbGknKS5maXJzdCgpLnJlbW92ZSgpO1xuICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2hfZmllbGRfc2NhbGUoKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5yZXN1bHRzX3Jlc2V0ID0gZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLnJlc2V0X3NpbmdsZV9zZWxlY3Rfb3B0aW9ucygpO1xuICAgICAgdGhpcy5mb3JtX2ZpZWxkLm9wdGlvbnNbMF0uc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgdGhpcy5zaW5nbGVfc2V0X3NlbGVjdGVkX3RleHQoKTtcbiAgICAgIHRoaXMuc2hvd19zZWFyY2hfZmllbGRfZGVmYXVsdCgpO1xuICAgICAgdGhpcy5yZXN1bHRzX3Jlc2V0X2NsZWFudXAoKTtcbiAgICAgIHRoaXMudHJpZ2dlcl9mb3JtX2ZpZWxkX2NoYW5nZSgpO1xuICAgICAgaWYgKHRoaXMuYWN0aXZlX2ZpZWxkKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdHNfaGlkZSgpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnJlc3VsdHNfcmVzZXRfY2xlYW51cCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5jdXJyZW50X3NlbGVjdGVkSW5kZXggPSB0aGlzLmZvcm1fZmllbGQuc2VsZWN0ZWRJbmRleDtcbiAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkX2l0ZW0uZmluZChcImFiYnJcIikucmVtb3ZlKCk7XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUucmVzdWx0X3NlbGVjdCA9IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgdmFyIGhpZ2gsIGl0ZW07XG4gICAgICBpZiAodGhpcy5yZXN1bHRfaGlnaGxpZ2h0KSB7XG4gICAgICAgIGhpZ2ggPSB0aGlzLnJlc3VsdF9oaWdobGlnaHQ7XG4gICAgICAgIHRoaXMucmVzdWx0X2NsZWFyX2hpZ2hsaWdodCgpO1xuICAgICAgICBpZiAodGhpcy5pc19tdWx0aXBsZSAmJiB0aGlzLm1heF9zZWxlY3RlZF9vcHRpb25zIDw9IHRoaXMuY2hvaWNlc19jb3VudCgpKSB7XG4gICAgICAgICAgdGhpcy5mb3JtX2ZpZWxkX2pxLnRyaWdnZXIoXCJjaG9zZW46bWF4c2VsZWN0ZWRcIiwge1xuICAgICAgICAgICAgY2hvc2VuOiB0aGlzXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmlzX211bHRpcGxlKSB7XG4gICAgICAgICAgaGlnaC5yZW1vdmVDbGFzcyhcImFjdGl2ZS1yZXN1bHRcIik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5yZXNldF9zaW5nbGVfc2VsZWN0X29wdGlvbnMoKTtcbiAgICAgICAgfVxuICAgICAgICBoaWdoLmFkZENsYXNzKFwicmVzdWx0LXNlbGVjdGVkXCIpO1xuICAgICAgICBpdGVtID0gdGhpcy5yZXN1bHRzX2RhdGFbaGlnaFswXS5nZXRBdHRyaWJ1dGUoXCJkYXRhLW9wdGlvbi1hcnJheS1pbmRleFwiKV07XG4gICAgICAgIGl0ZW0uc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLmZvcm1fZmllbGQub3B0aW9uc1tpdGVtLm9wdGlvbnNfaW5kZXhdLnNlbGVjdGVkID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5zZWxlY3RlZF9vcHRpb25fY291bnQgPSBudWxsO1xuICAgICAgICBpZiAodGhpcy5pc19tdWx0aXBsZSkge1xuICAgICAgICAgIHRoaXMuY2hvaWNlX2J1aWxkKGl0ZW0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuc2luZ2xlX3NldF9zZWxlY3RlZF90ZXh0KHRoaXMuY2hvaWNlX2xhYmVsKGl0ZW0pKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5pc19tdWx0aXBsZSAmJiAoIXRoaXMuaGlkZV9yZXN1bHRzX29uX3NlbGVjdCB8fCAoZXZ0Lm1ldGFLZXkgfHwgZXZ0LmN0cmxLZXkpKSkge1xuICAgICAgICAgIGlmIChldnQubWV0YUtleSB8fCBldnQuY3RybEtleSkge1xuICAgICAgICAgICAgdGhpcy53aW5ub3dfcmVzdWx0cyh7XG4gICAgICAgICAgICAgIHNraXBfaGlnaGxpZ2h0OiB0cnVlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zZWFyY2hfZmllbGQudmFsKFwiXCIpO1xuICAgICAgICAgICAgdGhpcy53aW5ub3dfcmVzdWx0cygpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnJlc3VsdHNfaGlkZSgpO1xuICAgICAgICAgIHRoaXMuc2hvd19zZWFyY2hfZmllbGRfZGVmYXVsdCgpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmlzX211bHRpcGxlIHx8IHRoaXMuZm9ybV9maWVsZC5zZWxlY3RlZEluZGV4ICE9PSB0aGlzLmN1cnJlbnRfc2VsZWN0ZWRJbmRleCkge1xuICAgICAgICAgIHRoaXMudHJpZ2dlcl9mb3JtX2ZpZWxkX2NoYW5nZSh7XG4gICAgICAgICAgICBzZWxlY3RlZDogdGhpcy5mb3JtX2ZpZWxkLm9wdGlvbnNbaXRlbS5vcHRpb25zX2luZGV4XS52YWx1ZVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY3VycmVudF9zZWxlY3RlZEluZGV4ID0gdGhpcy5mb3JtX2ZpZWxkLnNlbGVjdGVkSW5kZXg7XG4gICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2hfZmllbGRfc2NhbGUoKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5zaW5nbGVfc2V0X3NlbGVjdGVkX3RleHQgPSBmdW5jdGlvbih0ZXh0KSB7XG4gICAgICBpZiAodGV4dCA9PSBudWxsKSB7XG4gICAgICAgIHRleHQgPSB0aGlzLmRlZmF1bHRfdGV4dDtcbiAgICAgIH1cbiAgICAgIGlmICh0ZXh0ID09PSB0aGlzLmRlZmF1bHRfdGV4dCkge1xuICAgICAgICB0aGlzLnNlbGVjdGVkX2l0ZW0uYWRkQ2xhc3MoXCJjaG9zZW4tZGVmYXVsdFwiKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuc2luZ2xlX2Rlc2VsZWN0X2NvbnRyb2xfYnVpbGQoKTtcbiAgICAgICAgdGhpcy5zZWxlY3RlZF9pdGVtLnJlbW92ZUNsYXNzKFwiY2hvc2VuLWRlZmF1bHRcIik7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZF9pdGVtLmZpbmQoXCJzcGFuXCIpLmh0bWwodGV4dCk7XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUucmVzdWx0X2Rlc2VsZWN0ID0gZnVuY3Rpb24ocG9zKSB7XG4gICAgICB2YXIgcmVzdWx0X2RhdGE7XG4gICAgICByZXN1bHRfZGF0YSA9IHRoaXMucmVzdWx0c19kYXRhW3Bvc107XG4gICAgICBpZiAoIXRoaXMuZm9ybV9maWVsZC5vcHRpb25zW3Jlc3VsdF9kYXRhLm9wdGlvbnNfaW5kZXhdLmRpc2FibGVkKSB7XG4gICAgICAgIHJlc3VsdF9kYXRhLnNlbGVjdGVkID0gZmFsc2U7XG4gICAgICAgIHRoaXMuZm9ybV9maWVsZC5vcHRpb25zW3Jlc3VsdF9kYXRhLm9wdGlvbnNfaW5kZXhdLnNlbGVjdGVkID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRfb3B0aW9uX2NvdW50ID0gbnVsbDtcbiAgICAgICAgdGhpcy5yZXN1bHRfY2xlYXJfaGlnaGxpZ2h0KCk7XG4gICAgICAgIGlmICh0aGlzLnJlc3VsdHNfc2hvd2luZykge1xuICAgICAgICAgIHRoaXMud2lubm93X3Jlc3VsdHMoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnRyaWdnZXJfZm9ybV9maWVsZF9jaGFuZ2Uoe1xuICAgICAgICAgIGRlc2VsZWN0ZWQ6IHRoaXMuZm9ybV9maWVsZC5vcHRpb25zW3Jlc3VsdF9kYXRhLm9wdGlvbnNfaW5kZXhdLnZhbHVlXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLnNlYXJjaF9maWVsZF9zY2FsZSgpO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5zaW5nbGVfZGVzZWxlY3RfY29udHJvbF9idWlsZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKCF0aGlzLmFsbG93X3NpbmdsZV9kZXNlbGVjdCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBpZiAoIXRoaXMuc2VsZWN0ZWRfaXRlbS5maW5kKFwiYWJiclwiKS5sZW5ndGgpIHtcbiAgICAgICAgdGhpcy5zZWxlY3RlZF9pdGVtLmZpbmQoXCJzcGFuXCIpLmZpcnN0KCkuYWZ0ZXIoXCI8YWJiciBjbGFzcz1cXFwic2VhcmNoLWNob2ljZS1jbG9zZVxcXCI+PC9hYmJyPlwiKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkX2l0ZW0uYWRkQ2xhc3MoXCJjaG9zZW4tc2luZ2xlLXdpdGgtZGVzZWxlY3RcIik7XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUuZ2V0X3NlYXJjaF9maWVsZF92YWx1ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc2VhcmNoX2ZpZWxkLnZhbCgpO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLmdldF9zZWFyY2hfdGV4dCA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuICQudHJpbSh0aGlzLmdldF9zZWFyY2hfZmllbGRfdmFsdWUoKSk7XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUuZXNjYXBlX2h0bWwgPSBmdW5jdGlvbih0ZXh0KSB7XG4gICAgICByZXR1cm4gJCgnPGRpdi8+JykudGV4dCh0ZXh0KS5odG1sKCk7XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUud2lubm93X3Jlc3VsdHNfc2V0X2hpZ2hsaWdodCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGRvX2hpZ2gsIHNlbGVjdGVkX3Jlc3VsdHM7XG4gICAgICBzZWxlY3RlZF9yZXN1bHRzID0gIXRoaXMuaXNfbXVsdGlwbGUgPyB0aGlzLnNlYXJjaF9yZXN1bHRzLmZpbmQoXCIucmVzdWx0LXNlbGVjdGVkLmFjdGl2ZS1yZXN1bHRcIikgOiBbXTtcbiAgICAgIGRvX2hpZ2ggPSBzZWxlY3RlZF9yZXN1bHRzLmxlbmd0aCA/IHNlbGVjdGVkX3Jlc3VsdHMuZmlyc3QoKSA6IHRoaXMuc2VhcmNoX3Jlc3VsdHMuZmluZChcIi5hY3RpdmUtcmVzdWx0XCIpLmZpcnN0KCk7XG4gICAgICBpZiAoZG9faGlnaCAhPSBudWxsKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdF9kb19oaWdobGlnaHQoZG9faGlnaCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUubm9fcmVzdWx0cyA9IGZ1bmN0aW9uKHRlcm1zKSB7XG4gICAgICB2YXIgbm9fcmVzdWx0c19odG1sO1xuICAgICAgbm9fcmVzdWx0c19odG1sID0gdGhpcy5nZXRfbm9fcmVzdWx0c19odG1sKHRlcm1zKTtcbiAgICAgIHRoaXMuc2VhcmNoX3Jlc3VsdHMuYXBwZW5kKG5vX3Jlc3VsdHNfaHRtbCk7XG4gICAgICByZXR1cm4gdGhpcy5mb3JtX2ZpZWxkX2pxLnRyaWdnZXIoXCJjaG9zZW46bm9fcmVzdWx0c1wiLCB7XG4gICAgICAgIGNob3NlbjogdGhpc1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIENob3Nlbi5wcm90b3R5cGUubm9fcmVzdWx0c19jbGVhciA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc2VhcmNoX3Jlc3VsdHMuZmluZChcIi5uby1yZXN1bHRzXCIpLnJlbW92ZSgpO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLmtleWRvd25fYXJyb3cgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBuZXh0X3NpYjtcbiAgICAgIGlmICh0aGlzLnJlc3VsdHNfc2hvd2luZyAmJiB0aGlzLnJlc3VsdF9oaWdobGlnaHQpIHtcbiAgICAgICAgbmV4dF9zaWIgPSB0aGlzLnJlc3VsdF9oaWdobGlnaHQubmV4dEFsbChcImxpLmFjdGl2ZS1yZXN1bHRcIikuZmlyc3QoKTtcbiAgICAgICAgaWYgKG5leHRfc2liKSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMucmVzdWx0X2RvX2hpZ2hsaWdodChuZXh0X3NpYik7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc3VsdHNfc2hvdygpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLmtleXVwX2Fycm93ID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgcHJldl9zaWJzO1xuICAgICAgaWYgKCF0aGlzLnJlc3VsdHNfc2hvd2luZyAmJiAhdGhpcy5pc19tdWx0aXBsZSkge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHRzX3Nob3coKTtcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5yZXN1bHRfaGlnaGxpZ2h0KSB7XG4gICAgICAgIHByZXZfc2licyA9IHRoaXMucmVzdWx0X2hpZ2hsaWdodC5wcmV2QWxsKFwibGkuYWN0aXZlLXJlc3VsdFwiKTtcbiAgICAgICAgaWYgKHByZXZfc2licy5sZW5ndGgpIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHRfZG9faGlnaGxpZ2h0KHByZXZfc2licy5maXJzdCgpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAodGhpcy5jaG9pY2VzX2NvdW50KCkgPiAwKSB7XG4gICAgICAgICAgICB0aGlzLnJlc3VsdHNfaGlkZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gdGhpcy5yZXN1bHRfY2xlYXJfaGlnaGxpZ2h0KCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQ2hvc2VuLnByb3RvdHlwZS5rZXlkb3duX2JhY2tzdHJva2UgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBuZXh0X2F2YWlsYWJsZV9kZXN0cm95O1xuICAgICAgaWYgKHRoaXMucGVuZGluZ19iYWNrc3Ryb2tlKSB7XG4gICAgICAgIHRoaXMuY2hvaWNlX2Rlc3Ryb3kodGhpcy5wZW5kaW5nX2JhY2tzdHJva2UuZmluZChcImFcIikuZmlyc3QoKSk7XG4gICAgICAgIHJldHVybiB0aGlzLmNsZWFyX2JhY2tzdHJva2UoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG5leHRfYXZhaWxhYmxlX2Rlc3Ryb3kgPSB0aGlzLnNlYXJjaF9jb250YWluZXIuc2libGluZ3MoXCJsaS5zZWFyY2gtY2hvaWNlXCIpLmxhc3QoKTtcbiAgICAgICAgaWYgKG5leHRfYXZhaWxhYmxlX2Rlc3Ryb3kubGVuZ3RoICYmICFuZXh0X2F2YWlsYWJsZV9kZXN0cm95Lmhhc0NsYXNzKFwic2VhcmNoLWNob2ljZS1kaXNhYmxlZFwiKSkge1xuICAgICAgICAgIHRoaXMucGVuZGluZ19iYWNrc3Ryb2tlID0gbmV4dF9hdmFpbGFibGVfZGVzdHJveTtcbiAgICAgICAgICBpZiAodGhpcy5zaW5nbGVfYmFja3N0cm9rZV9kZWxldGUpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmtleWRvd25fYmFja3N0cm9rZSgpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5wZW5kaW5nX2JhY2tzdHJva2UuYWRkQ2xhc3MoXCJzZWFyY2gtY2hvaWNlLWZvY3VzXCIpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLmNsZWFyX2JhY2tzdHJva2UgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmICh0aGlzLnBlbmRpbmdfYmFja3N0cm9rZSkge1xuICAgICAgICB0aGlzLnBlbmRpbmdfYmFja3N0cm9rZS5yZW1vdmVDbGFzcyhcInNlYXJjaC1jaG9pY2UtZm9jdXNcIik7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5wZW5kaW5nX2JhY2tzdHJva2UgPSBudWxsO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnNlYXJjaF9maWVsZF9zY2FsZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGRpdiwgaSwgbGVuLCBzdHlsZSwgc3R5bGVfYmxvY2ssIHN0eWxlcywgd2lkdGg7XG4gICAgICBpZiAoIXRoaXMuaXNfbXVsdGlwbGUpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgc3R5bGVfYmxvY2sgPSB7XG4gICAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuICAgICAgICBsZWZ0OiAnLTEwMDBweCcsXG4gICAgICAgIHRvcDogJy0xMDAwcHgnLFxuICAgICAgICBkaXNwbGF5OiAnbm9uZScsXG4gICAgICAgIHdoaXRlU3BhY2U6ICdwcmUnXG4gICAgICB9O1xuICAgICAgc3R5bGVzID0gWydmb250U2l6ZScsICdmb250U3R5bGUnLCAnZm9udFdlaWdodCcsICdmb250RmFtaWx5JywgJ2xpbmVIZWlnaHQnLCAndGV4dFRyYW5zZm9ybScsICdsZXR0ZXJTcGFjaW5nJ107XG4gICAgICBmb3IgKGkgPSAwLCBsZW4gPSBzdHlsZXMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgc3R5bGUgPSBzdHlsZXNbaV07XG4gICAgICAgIHN0eWxlX2Jsb2NrW3N0eWxlXSA9IHRoaXMuc2VhcmNoX2ZpZWxkLmNzcyhzdHlsZSk7XG4gICAgICB9XG4gICAgICBkaXYgPSAkKCc8ZGl2IC8+JykuY3NzKHN0eWxlX2Jsb2NrKTtcbiAgICAgIGRpdi50ZXh0KHRoaXMuZ2V0X3NlYXJjaF9maWVsZF92YWx1ZSgpKTtcbiAgICAgICQoJ2JvZHknKS5hcHBlbmQoZGl2KTtcbiAgICAgIHdpZHRoID0gZGl2LndpZHRoKCkgKyAyNTtcbiAgICAgIGRpdi5yZW1vdmUoKTtcbiAgICAgIGlmICh0aGlzLmNvbnRhaW5lci5pcygnOnZpc2libGUnKSkge1xuICAgICAgICB3aWR0aCA9IE1hdGgubWluKHRoaXMuY29udGFpbmVyLm91dGVyV2lkdGgoKSAtIDEwLCB3aWR0aCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5zZWFyY2hfZmllbGQud2lkdGgod2lkdGgpO1xuICAgIH07XG5cbiAgICBDaG9zZW4ucHJvdG90eXBlLnRyaWdnZXJfZm9ybV9maWVsZF9jaGFuZ2UgPSBmdW5jdGlvbihleHRyYSkge1xuICAgICAgdGhpcy5mb3JtX2ZpZWxkX2pxLnRyaWdnZXIoXCJpbnB1dFwiLCBleHRyYSk7XG4gICAgICByZXR1cm4gdGhpcy5mb3JtX2ZpZWxkX2pxLnRyaWdnZXIoXCJjaGFuZ2VcIiwgZXh0cmEpO1xuICAgIH07XG5cbiAgICByZXR1cm4gQ2hvc2VuO1xuXG4gIH0pKEFic3RyYWN0Q2hvc2VuKTtcblxufSkuY2FsbCh0aGlzKTtcbiJdLCJzb3VyY2VSb290IjoiIn0=