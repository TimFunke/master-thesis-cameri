(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendors~Archive/SearchCtrl~BigData/SearchCtrl~Library/IonRangeSlider"],{

/***/ "./node_modules/ion-rangeslider/css/ion.rangeSlider.css":
/*!**************************************************************!*\
  !*** ./node_modules/ion-rangeslider/css/ion.rangeSlider.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./node_modules/ion-rangeslider/js/ion.rangeSlider.js":
/*!************************************************************!*\
  !*** ./node_modules/ion-rangeslider/js/ion.rangeSlider.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// Ion.RangeSlider
// version 2.3.1 Build: 382
// © Denis Ineshin, 2019
// https://github.com/IonDen
//
// Project page:    http://ionden.com/a/plugins/ion.rangeSlider/en.html
// GitHub page:     https://github.com/IonDen/ion.rangeSlider
//
// Released under MIT licence:
// http://ionden.com/a/plugins/licence-en.html
// =====================================================================================================================

;(function(factory) {
    if ((typeof jQuery === 'undefined' || !jQuery) && "function" === "function" && __webpack_require__(/*! !webpack amd options */ "./node_modules/webpack/buildin/amd-options.js")) {
        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (jQuery) {
            return factory(jQuery, document, window, navigator);
        }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else if ((typeof jQuery === 'undefined' || !jQuery) && typeof exports === "object") {
        factory(__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), document, window, navigator);
    } else {
        factory(jQuery, document, window, navigator);
    }
} (function ($, document, window, navigator, undefined) {
    "use strict";

    // =================================================================================================================
    // Service

    var plugin_count = 0;

    // IE8 fix
    var is_old_ie = (function () {
        var n = navigator.userAgent,
            r = /msie\s\d+/i,
            v;
        if (n.search(r) > 0) {
            v = r.exec(n).toString();
            v = v.split(" ")[1];
            if (v < 9) {
                $("html").addClass("lt-ie9");
                return true;
            }
        }
        return false;
    } ());
    if (!Function.prototype.bind) {
        Function.prototype.bind = function bind(that) {

            var target = this;
            var slice = [].slice;

            if (typeof target != "function") {
                throw new TypeError();
            }

            var args = slice.call(arguments, 1),
                bound = function () {

                    if (this instanceof bound) {

                        var F = function(){};
                        F.prototype = target.prototype;
                        var self = new F();

                        var result = target.apply(
                            self,
                            args.concat(slice.call(arguments))
                        );
                        if (Object(result) === result) {
                            return result;
                        }
                        return self;

                    } else {

                        return target.apply(
                            that,
                            args.concat(slice.call(arguments))
                        );

                    }

                };

            return bound;
        };
    }
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(searchElement, fromIndex) {
            var k;
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }
            var O = Object(this);
            var len = O.length >>> 0;
            if (len === 0) {
                return -1;
            }
            var n = +fromIndex || 0;
            if (Math.abs(n) === Infinity) {
                n = 0;
            }
            if (n >= len) {
                return -1;
            }
            k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
            while (k < len) {
                if (k in O && O[k] === searchElement) {
                    return k;
                }
                k++;
            }
            return -1;
        };
    }



    // =================================================================================================================
    // Template

    var base_html =
        '<span class="irs">' +
        '<span class="irs-line" tabindex="0"></span>' +
        '<span class="irs-min">0</span><span class="irs-max">1</span>' +
        '<span class="irs-from">0</span><span class="irs-to">0</span><span class="irs-single">0</span>' +
        '</span>' +
        '<span class="irs-grid"></span>';

    var single_html =
        '<span class="irs-bar irs-bar--single"></span>' +
        '<span class="irs-shadow shadow-single"></span>' +
        '<span class="irs-handle single"><i></i><i></i><i></i></span>';

    var double_html =
        '<span class="irs-bar"></span>' +
        '<span class="irs-shadow shadow-from"></span>' +
        '<span class="irs-shadow shadow-to"></span>' +
        '<span class="irs-handle from"><i></i><i></i><i></i></span>' +
        '<span class="irs-handle to"><i></i><i></i><i></i></span>';

    var disable_html =
        '<span class="irs-disable-mask"></span>';



    // =================================================================================================================
    // Core

    /**
     * Main plugin constructor
     *
     * @param input {Object} link to base input element
     * @param options {Object} slider config
     * @param plugin_count {Number}
     * @constructor
     */
    var IonRangeSlider = function (input, options, plugin_count) {
        this.VERSION = "2.3.1";
        this.input = input;
        this.plugin_count = plugin_count;
        this.current_plugin = 0;
        this.calc_count = 0;
        this.update_tm = 0;
        this.old_from = 0;
        this.old_to = 0;
        this.old_min_interval = null;
        this.raf_id = null;
        this.dragging = false;
        this.force_redraw = false;
        this.no_diapason = false;
        this.has_tab_index = true;
        this.is_key = false;
        this.is_update = false;
        this.is_start = true;
        this.is_finish = false;
        this.is_active = false;
        this.is_resize = false;
        this.is_click = false;

        options = options || {};

        // cache for links to all DOM elements
        this.$cache = {
            win: $(window),
            body: $(document.body),
            input: $(input),
            cont: null,
            rs: null,
            min: null,
            max: null,
            from: null,
            to: null,
            single: null,
            bar: null,
            line: null,
            s_single: null,
            s_from: null,
            s_to: null,
            shad_single: null,
            shad_from: null,
            shad_to: null,
            edge: null,
            grid: null,
            grid_labels: []
        };

        // storage for measure variables
        this.coords = {
            // left
            x_gap: 0,
            x_pointer: 0,

            // width
            w_rs: 0,
            w_rs_old: 0,
            w_handle: 0,

            // percents
            p_gap: 0,
            p_gap_left: 0,
            p_gap_right: 0,
            p_step: 0,
            p_pointer: 0,
            p_handle: 0,
            p_single_fake: 0,
            p_single_real: 0,
            p_from_fake: 0,
            p_from_real: 0,
            p_to_fake: 0,
            p_to_real: 0,
            p_bar_x: 0,
            p_bar_w: 0,

            // grid
            grid_gap: 0,
            big_num: 0,
            big: [],
            big_w: [],
            big_p: [],
            big_x: []
        };

        // storage for labels measure variables
        this.labels = {
            // width
            w_min: 0,
            w_max: 0,
            w_from: 0,
            w_to: 0,
            w_single: 0,

            // percents
            p_min: 0,
            p_max: 0,
            p_from_fake: 0,
            p_from_left: 0,
            p_to_fake: 0,
            p_to_left: 0,
            p_single_fake: 0,
            p_single_left: 0
        };



        /**
         * get and validate config
         */
        var $inp = this.$cache.input,
            val = $inp.prop("value"),
            config, config_from_data, prop;

        // default config
        config = {
            skin: "flat",
            type: "single",

            min: 10,
            max: 100,
            from: null,
            to: null,
            step: 1,

            min_interval: 0,
            max_interval: 0,
            drag_interval: false,

            values: [],
            p_values: [],

            from_fixed: false,
            from_min: null,
            from_max: null,
            from_shadow: false,

            to_fixed: false,
            to_min: null,
            to_max: null,
            to_shadow: false,

            prettify_enabled: true,
            prettify_separator: " ",
            prettify: null,

            force_edges: false,

            keyboard: true,

            grid: false,
            grid_margin: true,
            grid_num: 4,
            grid_snap: false,

            hide_min_max: false,
            hide_from_to: false,

            prefix: "",
            postfix: "",
            max_postfix: "",
            decorate_both: true,
            values_separator: " — ",

            input_values_separator: ";",

            disable: false,
            block: false,

            extra_classes: "",

            scope: null,
            onStart: null,
            onChange: null,
            onFinish: null,
            onUpdate: null
        };


        // check if base element is input
        if ($inp[0].nodeName !== "INPUT") {
            console && console.warn && console.warn("Base element should be <input>!", $inp[0]);
        }


        // config from data-attributes extends js config
        config_from_data = {
            skin: $inp.data("skin"),
            type: $inp.data("type"),

            min: $inp.data("min"),
            max: $inp.data("max"),
            from: $inp.data("from"),
            to: $inp.data("to"),
            step: $inp.data("step"),

            min_interval: $inp.data("minInterval"),
            max_interval: $inp.data("maxInterval"),
            drag_interval: $inp.data("dragInterval"),

            values: $inp.data("values"),

            from_fixed: $inp.data("fromFixed"),
            from_min: $inp.data("fromMin"),
            from_max: $inp.data("fromMax"),
            from_shadow: $inp.data("fromShadow"),

            to_fixed: $inp.data("toFixed"),
            to_min: $inp.data("toMin"),
            to_max: $inp.data("toMax"),
            to_shadow: $inp.data("toShadow"),

            prettify_enabled: $inp.data("prettifyEnabled"),
            prettify_separator: $inp.data("prettifySeparator"),

            force_edges: $inp.data("forceEdges"),

            keyboard: $inp.data("keyboard"),

            grid: $inp.data("grid"),
            grid_margin: $inp.data("gridMargin"),
            grid_num: $inp.data("gridNum"),
            grid_snap: $inp.data("gridSnap"),

            hide_min_max: $inp.data("hideMinMax"),
            hide_from_to: $inp.data("hideFromTo"),

            prefix: $inp.data("prefix"),
            postfix: $inp.data("postfix"),
            max_postfix: $inp.data("maxPostfix"),
            decorate_both: $inp.data("decorateBoth"),
            values_separator: $inp.data("valuesSeparator"),

            input_values_separator: $inp.data("inputValuesSeparator"),

            disable: $inp.data("disable"),
            block: $inp.data("block"),

            extra_classes: $inp.data("extraClasses"),
        };
        config_from_data.values = config_from_data.values && config_from_data.values.split(",");

        for (prop in config_from_data) {
            if (config_from_data.hasOwnProperty(prop)) {
                if (config_from_data[prop] === undefined || config_from_data[prop] === "") {
                    delete config_from_data[prop];
                }
            }
        }


        // input value extends default config
        if (val !== undefined && val !== "") {
            val = val.split(config_from_data.input_values_separator || options.input_values_separator || ";");

            if (val[0] && val[0] == +val[0]) {
                val[0] = +val[0];
            }
            if (val[1] && val[1] == +val[1]) {
                val[1] = +val[1];
            }

            if (options && options.values && options.values.length) {
                config.from = val[0] && options.values.indexOf(val[0]);
                config.to = val[1] && options.values.indexOf(val[1]);
            } else {
                config.from = val[0] && +val[0];
                config.to = val[1] && +val[1];
            }
        }



        // js config extends default config
        $.extend(config, options);


        // data config extends config
        $.extend(config, config_from_data);
        this.options = config;



        // validate config, to be sure that all data types are correct
        this.update_check = {};
        this.validate();



        // default result object, returned to callbacks
        this.result = {
            input: this.$cache.input,
            slider: null,

            min: this.options.min,
            max: this.options.max,

            from: this.options.from,
            from_percent: 0,
            from_value: null,

            to: this.options.to,
            to_percent: 0,
            to_value: null
        };



        this.init();
    };

    IonRangeSlider.prototype = {

        /**
         * Starts or updates the plugin instance
         *
         * @param [is_update] {boolean}
         */
        init: function (is_update) {
            this.no_diapason = false;
            this.coords.p_step = this.convertToPercent(this.options.step, true);

            this.target = "base";

            this.toggleInput();
            this.append();
            this.setMinMax();

            if (is_update) {
                this.force_redraw = true;
                this.calc(true);

                // callbacks called
                this.callOnUpdate();
            } else {
                this.force_redraw = true;
                this.calc(true);

                // callbacks called
                this.callOnStart();
            }

            this.updateScene();
        },

        /**
         * Appends slider template to a DOM
         */
        append: function () {
            var container_html = '<span class="irs irs--' + this.options.skin + ' js-irs-' + this.plugin_count + ' ' + this.options.extra_classes + '"></span>';
            this.$cache.input.before(container_html);
            this.$cache.input.prop("readonly", true);
            this.$cache.cont = this.$cache.input.prev();
            this.result.slider = this.$cache.cont;

            this.$cache.cont.html(base_html);
            this.$cache.rs = this.$cache.cont.find(".irs");
            this.$cache.min = this.$cache.cont.find(".irs-min");
            this.$cache.max = this.$cache.cont.find(".irs-max");
            this.$cache.from = this.$cache.cont.find(".irs-from");
            this.$cache.to = this.$cache.cont.find(".irs-to");
            this.$cache.single = this.$cache.cont.find(".irs-single");
            this.$cache.line = this.$cache.cont.find(".irs-line");
            this.$cache.grid = this.$cache.cont.find(".irs-grid");

            if (this.options.type === "single") {
                this.$cache.cont.append(single_html);
                this.$cache.bar = this.$cache.cont.find(".irs-bar");
                this.$cache.edge = this.$cache.cont.find(".irs-bar-edge");
                this.$cache.s_single = this.$cache.cont.find(".single");
                this.$cache.from[0].style.visibility = "hidden";
                this.$cache.to[0].style.visibility = "hidden";
                this.$cache.shad_single = this.$cache.cont.find(".shadow-single");
            } else {
                this.$cache.cont.append(double_html);
                this.$cache.bar = this.$cache.cont.find(".irs-bar");
                this.$cache.s_from = this.$cache.cont.find(".from");
                this.$cache.s_to = this.$cache.cont.find(".to");
                this.$cache.shad_from = this.$cache.cont.find(".shadow-from");
                this.$cache.shad_to = this.$cache.cont.find(".shadow-to");

                this.setTopHandler();
            }

            if (this.options.hide_from_to) {
                this.$cache.from[0].style.display = "none";
                this.$cache.to[0].style.display = "none";
                this.$cache.single[0].style.display = "none";
            }

            this.appendGrid();

            if (this.options.disable) {
                this.appendDisableMask();
                this.$cache.input[0].disabled = true;
            } else {
                this.$cache.input[0].disabled = false;
                this.removeDisableMask();
                this.bindEvents();
            }

            // block only if not disabled
            if (!this.options.disable) {
                if (this.options.block) {
                    this.appendDisableMask();
                } else {
                    this.removeDisableMask();
                }
            }

            if (this.options.drag_interval) {
                this.$cache.bar[0].style.cursor = "ew-resize";
            }
        },

        /**
         * Determine which handler has a priority
         * works only for double slider type
         */
        setTopHandler: function () {
            var min = this.options.min,
                max = this.options.max,
                from = this.options.from,
                to = this.options.to;

            if (from > min && to === max) {
                this.$cache.s_from.addClass("type_last");
            } else if (to < max) {
                this.$cache.s_to.addClass("type_last");
            }
        },

        /**
         * Determine which handles was clicked last
         * and which handler should have hover effect
         *
         * @param target {String}
         */
        changeLevel: function (target) {
            switch (target) {
                case "single":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_single_fake);
                    this.$cache.s_single.addClass("state_hover");
                    break;
                case "from":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake);
                    this.$cache.s_from.addClass("state_hover");
                    this.$cache.s_from.addClass("type_last");
                    this.$cache.s_to.removeClass("type_last");
                    break;
                case "to":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_to_fake);
                    this.$cache.s_to.addClass("state_hover");
                    this.$cache.s_to.addClass("type_last");
                    this.$cache.s_from.removeClass("type_last");
                    break;
                case "both":
                    this.coords.p_gap_left = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake);
                    this.coords.p_gap_right = this.toFixed(this.coords.p_to_fake - this.coords.p_pointer);
                    this.$cache.s_to.removeClass("type_last");
                    this.$cache.s_from.removeClass("type_last");
                    break;
            }
        },

        /**
         * Then slider is disabled
         * appends extra layer with opacity
         */
        appendDisableMask: function () {
            this.$cache.cont.append(disable_html);
            this.$cache.cont.addClass("irs-disabled");
        },

        /**
         * Then slider is not disabled
         * remove disable mask
         */
        removeDisableMask: function () {
            this.$cache.cont.remove(".irs-disable-mask");
            this.$cache.cont.removeClass("irs-disabled");
        },

        /**
         * Remove slider instance
         * and unbind all events
         */
        remove: function () {
            this.$cache.cont.remove();
            this.$cache.cont = null;

            this.$cache.line.off("keydown.irs_" + this.plugin_count);

            this.$cache.body.off("touchmove.irs_" + this.plugin_count);
            this.$cache.body.off("mousemove.irs_" + this.plugin_count);

            this.$cache.win.off("touchend.irs_" + this.plugin_count);
            this.$cache.win.off("mouseup.irs_" + this.plugin_count);

            if (is_old_ie) {
                this.$cache.body.off("mouseup.irs_" + this.plugin_count);
                this.$cache.body.off("mouseleave.irs_" + this.plugin_count);
            }

            this.$cache.grid_labels = [];
            this.coords.big = [];
            this.coords.big_w = [];
            this.coords.big_p = [];
            this.coords.big_x = [];

            cancelAnimationFrame(this.raf_id);
        },

        /**
         * bind all slider events
         */
        bindEvents: function () {
            if (this.no_diapason) {
                return;
            }

            this.$cache.body.on("touchmove.irs_" + this.plugin_count, this.pointerMove.bind(this));
            this.$cache.body.on("mousemove.irs_" + this.plugin_count, this.pointerMove.bind(this));

            this.$cache.win.on("touchend.irs_" + this.plugin_count, this.pointerUp.bind(this));
            this.$cache.win.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this));

            this.$cache.line.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
            this.$cache.line.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));

            this.$cache.line.on("focus.irs_" + this.plugin_count, this.pointerFocus.bind(this));

            if (this.options.drag_interval && this.options.type === "double") {
                this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "both"));
                this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "both"));
            } else {
                this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
            }

            if (this.options.type === "single") {
                this.$cache.single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                this.$cache.s_single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                this.$cache.shad_single.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));

                this.$cache.single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                this.$cache.s_single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
                this.$cache.edge.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                this.$cache.shad_single.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
            } else {
                this.$cache.single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, null));
                this.$cache.single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, null));

                this.$cache.from.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                this.$cache.s_from.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                this.$cache.to.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                this.$cache.s_to.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                this.$cache.shad_from.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                this.$cache.shad_to.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));

                this.$cache.from.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                this.$cache.s_from.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
                this.$cache.to.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                this.$cache.s_to.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
                this.$cache.shad_from.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
                this.$cache.shad_to.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
            }

            if (this.options.keyboard) {
                this.$cache.line.on("keydown.irs_" + this.plugin_count, this.key.bind(this, "keyboard"));
            }

            if (is_old_ie) {
                this.$cache.body.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this));
                this.$cache.body.on("mouseleave.irs_" + this.plugin_count, this.pointerUp.bind(this));
            }
        },

        /**
         * Focus with tabIndex
         *
         * @param e {Object} event object
         */
        pointerFocus: function (e) {
            if (!this.target) {
                var x;
                var $handle;

                if (this.options.type === "single") {
                    $handle = this.$cache.single;
                } else {
                    $handle = this.$cache.from;
                }

                x = $handle.offset().left;
                x += ($handle.width() / 2) - 1;

                this.pointerClick("single", {preventDefault: function () {}, pageX: x});
            }
        },

        /**
         * Mousemove or touchmove
         * only for handlers
         *
         * @param e {Object} event object
         */
        pointerMove: function (e) {
            if (!this.dragging) {
                return;
            }

            var x = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
            this.coords.x_pointer = x - this.coords.x_gap;

            this.calc();
        },

        /**
         * Mouseup or touchend
         * only for handlers
         *
         * @param e {Object} event object
         */
        pointerUp: function (e) {
            if (this.current_plugin !== this.plugin_count) {
                return;
            }

            if (this.is_active) {
                this.is_active = false;
            } else {
                return;
            }

            this.$cache.cont.find(".state_hover").removeClass("state_hover");

            this.force_redraw = true;

            if (is_old_ie) {
                $("*").prop("unselectable", false);
            }

            this.updateScene();
            this.restoreOriginalMinInterval();

            // callbacks call
            if ($.contains(this.$cache.cont[0], e.target) || this.dragging) {
                this.callOnFinish();
            }

            this.dragging = false;
        },

        /**
         * Mousedown or touchstart
         * only for handlers
         *
         * @param target {String|null}
         * @param e {Object} event object
         */
        pointerDown: function (target, e) {
            e.preventDefault();
            var x = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
            if (e.button === 2) {
                return;
            }

            if (target === "both") {
                this.setTempMinInterval();
            }

            if (!target) {
                target = this.target || "from";
            }

            this.current_plugin = this.plugin_count;
            this.target = target;

            this.is_active = true;
            this.dragging = true;

            this.coords.x_gap = this.$cache.rs.offset().left;
            this.coords.x_pointer = x - this.coords.x_gap;

            this.calcPointerPercent();
            this.changeLevel(target);

            if (is_old_ie) {
                $("*").prop("unselectable", true);
            }

            this.$cache.line.trigger("focus");

            this.updateScene();
        },

        /**
         * Mousedown or touchstart
         * for other slider elements, like diapason line
         *
         * @param target {String}
         * @param e {Object} event object
         */
        pointerClick: function (target, e) {
            e.preventDefault();
            var x = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
            if (e.button === 2) {
                return;
            }

            this.current_plugin = this.plugin_count;
            this.target = target;

            this.is_click = true;
            this.coords.x_gap = this.$cache.rs.offset().left;
            this.coords.x_pointer = +(x - this.coords.x_gap).toFixed();

            this.force_redraw = true;
            this.calc();

            this.$cache.line.trigger("focus");
        },

        /**
         * Keyborard controls for focused slider
         *
         * @param target {String}
         * @param e {Object} event object
         * @returns {boolean|undefined}
         */
        key: function (target, e) {
            if (this.current_plugin !== this.plugin_count || e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
                return;
            }

            switch (e.which) {
                case 83: // W
                case 65: // A
                case 40: // DOWN
                case 37: // LEFT
                    e.preventDefault();
                    this.moveByKey(false);
                    break;

                case 87: // S
                case 68: // D
                case 38: // UP
                case 39: // RIGHT
                    e.preventDefault();
                    this.moveByKey(true);
                    break;
            }

            return true;
        },

        /**
         * Move by key
         *
         * @param right {boolean} direction to move
         */
        moveByKey: function (right) {
            var p = this.coords.p_pointer;
            var p_step = (this.options.max - this.options.min) / 100;
            p_step = this.options.step / p_step;

            if (right) {
                p += p_step;
            } else {
                p -= p_step;
            }

            this.coords.x_pointer = this.toFixed(this.coords.w_rs / 100 * p);
            this.is_key = true;
            this.calc();
        },

        /**
         * Set visibility and content
         * of Min and Max labels
         */
        setMinMax: function () {
            if (!this.options) {
                return;
            }

            if (this.options.hide_min_max) {
                this.$cache.min[0].style.display = "none";
                this.$cache.max[0].style.display = "none";
                return;
            }

            if (this.options.values.length) {
                this.$cache.min.html(this.decorate(this.options.p_values[this.options.min]));
                this.$cache.max.html(this.decorate(this.options.p_values[this.options.max]));
            } else {
                var min_pretty = this._prettify(this.options.min);
                var max_pretty = this._prettify(this.options.max);

                this.result.min_pretty = min_pretty;
                this.result.max_pretty = max_pretty;

                this.$cache.min.html(this.decorate(min_pretty, this.options.min));
                this.$cache.max.html(this.decorate(max_pretty, this.options.max));
            }

            this.labels.w_min = this.$cache.min.outerWidth(false);
            this.labels.w_max = this.$cache.max.outerWidth(false);
        },

        /**
         * Then dragging interval, prevent interval collapsing
         * using min_interval option
         */
        setTempMinInterval: function () {
            var interval = this.result.to - this.result.from;

            if (this.old_min_interval === null) {
                this.old_min_interval = this.options.min_interval;
            }

            this.options.min_interval = interval;
        },

        /**
         * Restore min_interval option to original
         */
        restoreOriginalMinInterval: function () {
            if (this.old_min_interval !== null) {
                this.options.min_interval = this.old_min_interval;
                this.old_min_interval = null;
            }
        },



        // =============================================================================================================
        // Calculations

        /**
         * All calculations and measures start here
         *
         * @param update {boolean=}
         */
        calc: function (update) {
            if (!this.options) {
                return;
            }

            this.calc_count++;

            if (this.calc_count === 10 || update) {
                this.calc_count = 0;
                this.coords.w_rs = this.$cache.rs.outerWidth(false);

                this.calcHandlePercent();
            }

            if (!this.coords.w_rs) {
                return;
            }

            this.calcPointerPercent();
            var handle_x = this.getHandleX();


            if (this.target === "both") {
                this.coords.p_gap = 0;
                handle_x = this.getHandleX();
            }

            if (this.target === "click") {
                this.coords.p_gap = this.coords.p_handle / 2;
                handle_x = this.getHandleX();

                if (this.options.drag_interval) {
                    this.target = "both_one";
                } else {
                    this.target = this.chooseHandle(handle_x);
                }
            }

            switch (this.target) {
                case "base":
                    var w = (this.options.max - this.options.min) / 100,
                        f = (this.result.from - this.options.min) / w,
                        t = (this.result.to - this.options.min) / w;

                    this.coords.p_single_real = this.toFixed(f);
                    this.coords.p_from_real = this.toFixed(f);
                    this.coords.p_to_real = this.toFixed(t);

                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);

                    this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real);
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);

                    this.target = null;

                    break;

                case "single":
                    if (this.options.from_fixed) {
                        break;
                    }

                    this.coords.p_single_real = this.convertToRealPercent(handle_x);
                    this.coords.p_single_real = this.calcWithStep(this.coords.p_single_real);
                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);

                    this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real);

                    break;

                case "from":
                    if (this.options.from_fixed) {
                        break;
                    }

                    this.coords.p_from_real = this.convertToRealPercent(handle_x);
                    this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real);
                    if (this.coords.p_from_real > this.coords.p_to_real) {
                        this.coords.p_from_real = this.coords.p_to_real;
                    }
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
                    this.coords.p_from_real = this.checkMaxInterval(this.coords.p_from_real, this.coords.p_to_real, "from");

                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);

                    break;

                case "to":
                    if (this.options.to_fixed) {
                        break;
                    }

                    this.coords.p_to_real = this.convertToRealPercent(handle_x);
                    this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real);
                    if (this.coords.p_to_real < this.coords.p_from_real) {
                        this.coords.p_to_real = this.coords.p_from_real;
                    }
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
                    this.coords.p_to_real = this.checkMaxInterval(this.coords.p_to_real, this.coords.p_from_real, "to");

                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);

                    break;

                case "both":
                    if (this.options.from_fixed || this.options.to_fixed) {
                        break;
                    }

                    handle_x = this.toFixed(handle_x + (this.coords.p_handle * 0.001));

                    this.coords.p_from_real = this.convertToRealPercent(handle_x) - this.coords.p_gap_left;
                    this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);

                    this.coords.p_to_real = this.convertToRealPercent(handle_x) + this.coords.p_gap_right;
                    this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);

                    break;

                case "both_one":
                    if (this.options.from_fixed || this.options.to_fixed) {
                        break;
                    }

                    var real_x = this.convertToRealPercent(handle_x),
                        from = this.result.from_percent,
                        to = this.result.to_percent,
                        full = to - from,
                        half = full / 2,
                        new_from = real_x - half,
                        new_to = real_x + half;

                    if (new_from < 0) {
                        new_from = 0;
                        new_to = new_from + full;
                    }

                    if (new_to > 100) {
                        new_to = 100;
                        new_from = new_to - full;
                    }

                    this.coords.p_from_real = this.calcWithStep(new_from);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);

                    this.coords.p_to_real = this.calcWithStep(new_to);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);

                    break;
            }

            if (this.options.type === "single") {
                this.coords.p_bar_x = (this.coords.p_handle / 2);
                this.coords.p_bar_w = this.coords.p_single_fake;

                this.result.from_percent = this.coords.p_single_real;
                this.result.from = this.convertToValue(this.coords.p_single_real);
                this.result.from_pretty = this._prettify(this.result.from);

                if (this.options.values.length) {
                    this.result.from_value = this.options.values[this.result.from];
                }
            } else {
                this.coords.p_bar_x = this.toFixed(this.coords.p_from_fake + (this.coords.p_handle / 2));
                this.coords.p_bar_w = this.toFixed(this.coords.p_to_fake - this.coords.p_from_fake);

                this.result.from_percent = this.coords.p_from_real;
                this.result.from = this.convertToValue(this.coords.p_from_real);
                this.result.from_pretty = this._prettify(this.result.from);
                this.result.to_percent = this.coords.p_to_real;
                this.result.to = this.convertToValue(this.coords.p_to_real);
                this.result.to_pretty = this._prettify(this.result.to);

                if (this.options.values.length) {
                    this.result.from_value = this.options.values[this.result.from];
                    this.result.to_value = this.options.values[this.result.to];
                }
            }

            this.calcMinMax();
            this.calcLabels();
        },


        /**
         * calculates pointer X in percent
         */
        calcPointerPercent: function () {
            if (!this.coords.w_rs) {
                this.coords.p_pointer = 0;
                return;
            }

            if (this.coords.x_pointer < 0 || isNaN(this.coords.x_pointer)  ) {
                this.coords.x_pointer = 0;
            } else if (this.coords.x_pointer > this.coords.w_rs) {
                this.coords.x_pointer = this.coords.w_rs;
            }

            this.coords.p_pointer = this.toFixed(this.coords.x_pointer / this.coords.w_rs * 100);
        },

        convertToRealPercent: function (fake) {
            var full = 100 - this.coords.p_handle;
            return fake / full * 100;
        },

        convertToFakePercent: function (real) {
            var full = 100 - this.coords.p_handle;
            return real / 100 * full;
        },

        getHandleX: function () {
            var max = 100 - this.coords.p_handle,
                x = this.toFixed(this.coords.p_pointer - this.coords.p_gap);

            if (x < 0) {
                x = 0;
            } else if (x > max) {
                x = max;
            }

            return x;
        },

        calcHandlePercent: function () {
            if (this.options.type === "single") {
                this.coords.w_handle = this.$cache.s_single.outerWidth(false);
            } else {
                this.coords.w_handle = this.$cache.s_from.outerWidth(false);
            }

            this.coords.p_handle = this.toFixed(this.coords.w_handle / this.coords.w_rs * 100);
        },

        /**
         * Find closest handle to pointer click
         *
         * @param real_x {Number}
         * @returns {String}
         */
        chooseHandle: function (real_x) {
            if (this.options.type === "single") {
                return "single";
            } else {
                var m_point = this.coords.p_from_real + ((this.coords.p_to_real - this.coords.p_from_real) / 2);
                if (real_x >= m_point) {
                    return this.options.to_fixed ? "from" : "to";
                } else {
                    return this.options.from_fixed ? "to" : "from";
                }
            }
        },

        /**
         * Measure Min and Max labels width in percent
         */
        calcMinMax: function () {
            if (!this.coords.w_rs) {
                return;
            }

            this.labels.p_min = this.labels.w_min / this.coords.w_rs * 100;
            this.labels.p_max = this.labels.w_max / this.coords.w_rs * 100;
        },

        /**
         * Measure labels width and X in percent
         */
        calcLabels: function () {
            if (!this.coords.w_rs || this.options.hide_from_to) {
                return;
            }

            if (this.options.type === "single") {

                this.labels.w_single = this.$cache.single.outerWidth(false);
                this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100;
                this.labels.p_single_left = this.coords.p_single_fake + (this.coords.p_handle / 2) - (this.labels.p_single_fake / 2);
                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single_fake);

            } else {

                this.labels.w_from = this.$cache.from.outerWidth(false);
                this.labels.p_from_fake = this.labels.w_from / this.coords.w_rs * 100;
                this.labels.p_from_left = this.coords.p_from_fake + (this.coords.p_handle / 2) - (this.labels.p_from_fake / 2);
                this.labels.p_from_left = this.toFixed(this.labels.p_from_left);
                this.labels.p_from_left = this.checkEdges(this.labels.p_from_left, this.labels.p_from_fake);

                this.labels.w_to = this.$cache.to.outerWidth(false);
                this.labels.p_to_fake = this.labels.w_to / this.coords.w_rs * 100;
                this.labels.p_to_left = this.coords.p_to_fake + (this.coords.p_handle / 2) - (this.labels.p_to_fake / 2);
                this.labels.p_to_left = this.toFixed(this.labels.p_to_left);
                this.labels.p_to_left = this.checkEdges(this.labels.p_to_left, this.labels.p_to_fake);

                this.labels.w_single = this.$cache.single.outerWidth(false);
                this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100;
                this.labels.p_single_left = ((this.labels.p_from_left + this.labels.p_to_left + this.labels.p_to_fake) / 2) - (this.labels.p_single_fake / 2);
                this.labels.p_single_left = this.toFixed(this.labels.p_single_left);
                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single_fake);

            }
        },



        // =============================================================================================================
        // Drawings

        /**
         * Main function called in request animation frame
         * to update everything
         */
        updateScene: function () {
            if (this.raf_id) {
                cancelAnimationFrame(this.raf_id);
                this.raf_id = null;
            }

            clearTimeout(this.update_tm);
            this.update_tm = null;

            if (!this.options) {
                return;
            }

            this.drawHandles();

            if (this.is_active) {
                this.raf_id = requestAnimationFrame(this.updateScene.bind(this));
            } else {
                this.update_tm = setTimeout(this.updateScene.bind(this), 300);
            }
        },

        /**
         * Draw handles
         */
        drawHandles: function () {
            this.coords.w_rs = this.$cache.rs.outerWidth(false);

            if (!this.coords.w_rs) {
                return;
            }

            if (this.coords.w_rs !== this.coords.w_rs_old) {
                this.target = "base";
                this.is_resize = true;
            }

            if (this.coords.w_rs !== this.coords.w_rs_old || this.force_redraw) {
                this.setMinMax();
                this.calc(true);
                this.drawLabels();
                if (this.options.grid) {
                    this.calcGridMargin();
                    this.calcGridLabels();
                }
                this.force_redraw = true;
                this.coords.w_rs_old = this.coords.w_rs;
                this.drawShadow();
            }

            if (!this.coords.w_rs) {
                return;
            }

            if (!this.dragging && !this.force_redraw && !this.is_key) {
                return;
            }

            if (this.old_from !== this.result.from || this.old_to !== this.result.to || this.force_redraw || this.is_key) {

                this.drawLabels();

                this.$cache.bar[0].style.left = this.coords.p_bar_x + "%";
                this.$cache.bar[0].style.width = this.coords.p_bar_w + "%";

                if (this.options.type === "single") {
                    this.$cache.bar[0].style.left = 0;
                    this.$cache.bar[0].style.width = this.coords.p_bar_w + this.coords.p_bar_x + "%";

                    this.$cache.s_single[0].style.left = this.coords.p_single_fake + "%";

                    this.$cache.single[0].style.left = this.labels.p_single_left + "%";
                } else {
                    this.$cache.s_from[0].style.left = this.coords.p_from_fake + "%";
                    this.$cache.s_to[0].style.left = this.coords.p_to_fake + "%";

                    if (this.old_from !== this.result.from || this.force_redraw) {
                        this.$cache.from[0].style.left = this.labels.p_from_left + "%";
                    }
                    if (this.old_to !== this.result.to || this.force_redraw) {
                        this.$cache.to[0].style.left = this.labels.p_to_left + "%";
                    }

                    this.$cache.single[0].style.left = this.labels.p_single_left + "%";
                }

                this.writeToInput();

                if ((this.old_from !== this.result.from || this.old_to !== this.result.to) && !this.is_start) {
                    this.$cache.input.trigger("change");
                    this.$cache.input.trigger("input");
                }

                this.old_from = this.result.from;
                this.old_to = this.result.to;

                // callbacks call
                if (!this.is_resize && !this.is_update && !this.is_start && !this.is_finish) {
                    this.callOnChange();
                }
                if (this.is_key || this.is_click) {
                    this.is_key = false;
                    this.is_click = false;
                    this.callOnFinish();
                }

                this.is_update = false;
                this.is_resize = false;
                this.is_finish = false;
            }

            this.is_start = false;
            this.is_key = false;
            this.is_click = false;
            this.force_redraw = false;
        },

        /**
         * Draw labels
         * measure labels collisions
         * collapse close labels
         */
        drawLabels: function () {
            if (!this.options) {
                return;
            }

            var values_num = this.options.values.length;
            var p_values = this.options.p_values;
            var text_single;
            var text_from;
            var text_to;
            var from_pretty;
            var to_pretty;

            if (this.options.hide_from_to) {
                return;
            }

            if (this.options.type === "single") {

                if (values_num) {
                    text_single = this.decorate(p_values[this.result.from]);
                    this.$cache.single.html(text_single);
                } else {
                    from_pretty = this._prettify(this.result.from);

                    text_single = this.decorate(from_pretty, this.result.from);
                    this.$cache.single.html(text_single);
                }

                this.calcLabels();

                if (this.labels.p_single_left < this.labels.p_min + 1) {
                    this.$cache.min[0].style.visibility = "hidden";
                } else {
                    this.$cache.min[0].style.visibility = "visible";
                }

                if (this.labels.p_single_left + this.labels.p_single_fake > 100 - this.labels.p_max - 1) {
                    this.$cache.max[0].style.visibility = "hidden";
                } else {
                    this.$cache.max[0].style.visibility = "visible";
                }

            } else {

                if (values_num) {

                    if (this.options.decorate_both) {
                        text_single = this.decorate(p_values[this.result.from]);
                        text_single += this.options.values_separator;
                        text_single += this.decorate(p_values[this.result.to]);
                    } else {
                        text_single = this.decorate(p_values[this.result.from] + this.options.values_separator + p_values[this.result.to]);
                    }
                    text_from = this.decorate(p_values[this.result.from]);
                    text_to = this.decorate(p_values[this.result.to]);

                    this.$cache.single.html(text_single);
                    this.$cache.from.html(text_from);
                    this.$cache.to.html(text_to);

                } else {
                    from_pretty = this._prettify(this.result.from);
                    to_pretty = this._prettify(this.result.to);

                    if (this.options.decorate_both) {
                        text_single = this.decorate(from_pretty, this.result.from);
                        text_single += this.options.values_separator;
                        text_single += this.decorate(to_pretty, this.result.to);
                    } else {
                        text_single = this.decorate(from_pretty + this.options.values_separator + to_pretty, this.result.to);
                    }
                    text_from = this.decorate(from_pretty, this.result.from);
                    text_to = this.decorate(to_pretty, this.result.to);

                    this.$cache.single.html(text_single);
                    this.$cache.from.html(text_from);
                    this.$cache.to.html(text_to);

                }

                this.calcLabels();

                var min = Math.min(this.labels.p_single_left, this.labels.p_from_left),
                    single_left = this.labels.p_single_left + this.labels.p_single_fake,
                    to_left = this.labels.p_to_left + this.labels.p_to_fake,
                    max = Math.max(single_left, to_left);

                if (this.labels.p_from_left + this.labels.p_from_fake >= this.labels.p_to_left) {
                    this.$cache.from[0].style.visibility = "hidden";
                    this.$cache.to[0].style.visibility = "hidden";
                    this.$cache.single[0].style.visibility = "visible";

                    if (this.result.from === this.result.to) {
                        if (this.target === "from") {
                            this.$cache.from[0].style.visibility = "visible";
                        } else if (this.target === "to") {
                            this.$cache.to[0].style.visibility = "visible";
                        } else if (!this.target) {
                            this.$cache.from[0].style.visibility = "visible";
                        }
                        this.$cache.single[0].style.visibility = "hidden";
                        max = to_left;
                    } else {
                        this.$cache.from[0].style.visibility = "hidden";
                        this.$cache.to[0].style.visibility = "hidden";
                        this.$cache.single[0].style.visibility = "visible";
                        max = Math.max(single_left, to_left);
                    }
                } else {
                    this.$cache.from[0].style.visibility = "visible";
                    this.$cache.to[0].style.visibility = "visible";
                    this.$cache.single[0].style.visibility = "hidden";
                }

                if (min < this.labels.p_min + 1) {
                    this.$cache.min[0].style.visibility = "hidden";
                } else {
                    this.$cache.min[0].style.visibility = "visible";
                }

                if (max > 100 - this.labels.p_max - 1) {
                    this.$cache.max[0].style.visibility = "hidden";
                } else {
                    this.$cache.max[0].style.visibility = "visible";
                }

            }
        },

        /**
         * Draw shadow intervals
         */
        drawShadow: function () {
            var o = this.options,
                c = this.$cache,

                is_from_min = typeof o.from_min === "number" && !isNaN(o.from_min),
                is_from_max = typeof o.from_max === "number" && !isNaN(o.from_max),
                is_to_min = typeof o.to_min === "number" && !isNaN(o.to_min),
                is_to_max = typeof o.to_max === "number" && !isNaN(o.to_max),

                from_min,
                from_max,
                to_min,
                to_max;

            if (o.type === "single") {
                if (o.from_shadow && (is_from_min || is_from_max)) {
                    from_min = this.convertToPercent(is_from_min ? o.from_min : o.min);
                    from_max = this.convertToPercent(is_from_max ? o.from_max : o.max) - from_min;
                    from_min = this.toFixed(from_min - (this.coords.p_handle / 100 * from_min));
                    from_max = this.toFixed(from_max - (this.coords.p_handle / 100 * from_max));
                    from_min = from_min + (this.coords.p_handle / 2);

                    c.shad_single[0].style.display = "block";
                    c.shad_single[0].style.left = from_min + "%";
                    c.shad_single[0].style.width = from_max + "%";
                } else {
                    c.shad_single[0].style.display = "none";
                }
            } else {
                if (o.from_shadow && (is_from_min || is_from_max)) {
                    from_min = this.convertToPercent(is_from_min ? o.from_min : o.min);
                    from_max = this.convertToPercent(is_from_max ? o.from_max : o.max) - from_min;
                    from_min = this.toFixed(from_min - (this.coords.p_handle / 100 * from_min));
                    from_max = this.toFixed(from_max - (this.coords.p_handle / 100 * from_max));
                    from_min = from_min + (this.coords.p_handle / 2);

                    c.shad_from[0].style.display = "block";
                    c.shad_from[0].style.left = from_min + "%";
                    c.shad_from[0].style.width = from_max + "%";
                } else {
                    c.shad_from[0].style.display = "none";
                }

                if (o.to_shadow && (is_to_min || is_to_max)) {
                    to_min = this.convertToPercent(is_to_min ? o.to_min : o.min);
                    to_max = this.convertToPercent(is_to_max ? o.to_max : o.max) - to_min;
                    to_min = this.toFixed(to_min - (this.coords.p_handle / 100 * to_min));
                    to_max = this.toFixed(to_max - (this.coords.p_handle / 100 * to_max));
                    to_min = to_min + (this.coords.p_handle / 2);

                    c.shad_to[0].style.display = "block";
                    c.shad_to[0].style.left = to_min + "%";
                    c.shad_to[0].style.width = to_max + "%";
                } else {
                    c.shad_to[0].style.display = "none";
                }
            }
        },



        /**
         * Write values to input element
         */
        writeToInput: function () {
            if (this.options.type === "single") {
                if (this.options.values.length) {
                    this.$cache.input.prop("value", this.result.from_value);
                } else {
                    this.$cache.input.prop("value", this.result.from);
                }
                this.$cache.input.data("from", this.result.from);
            } else {
                if (this.options.values.length) {
                    this.$cache.input.prop("value", this.result.from_value + this.options.input_values_separator + this.result.to_value);
                } else {
                    this.$cache.input.prop("value", this.result.from + this.options.input_values_separator + this.result.to);
                }
                this.$cache.input.data("from", this.result.from);
                this.$cache.input.data("to", this.result.to);
            }
        },



        // =============================================================================================================
        // Callbacks

        callOnStart: function () {
            this.writeToInput();

            if (this.options.onStart && typeof this.options.onStart === "function") {
                if (this.options.scope) {
                    this.options.onStart.call(this.options.scope, this.result);
                } else {
                    this.options.onStart(this.result);
                }
            }
        },
        callOnChange: function () {
            this.writeToInput();

            if (this.options.onChange && typeof this.options.onChange === "function") {
                if (this.options.scope) {
                    this.options.onChange.call(this.options.scope, this.result);
                } else {
                    this.options.onChange(this.result);
                }
            }
        },
        callOnFinish: function () {
            this.writeToInput();

            if (this.options.onFinish && typeof this.options.onFinish === "function") {
                if (this.options.scope) {
                    this.options.onFinish.call(this.options.scope, this.result);
                } else {
                    this.options.onFinish(this.result);
                }
            }
        },
        callOnUpdate: function () {
            this.writeToInput();

            if (this.options.onUpdate && typeof this.options.onUpdate === "function") {
                if (this.options.scope) {
                    this.options.onUpdate.call(this.options.scope, this.result);
                } else {
                    this.options.onUpdate(this.result);
                }
            }
        },




        // =============================================================================================================
        // Service methods

        toggleInput: function () {
            this.$cache.input.toggleClass("irs-hidden-input");

            if (this.has_tab_index) {
                this.$cache.input.prop("tabindex", -1);
            } else {
                this.$cache.input.removeProp("tabindex");
            }

            this.has_tab_index = !this.has_tab_index;
        },

        /**
         * Convert real value to percent
         *
         * @param value {Number} X in real
         * @param no_min {boolean=} don't use min value
         * @returns {Number} X in percent
         */
        convertToPercent: function (value, no_min) {
            var diapason = this.options.max - this.options.min,
                one_percent = diapason / 100,
                val, percent;

            if (!diapason) {
                this.no_diapason = true;
                return 0;
            }

            if (no_min) {
                val = value;
            } else {
                val = value - this.options.min;
            }

            percent = val / one_percent;

            return this.toFixed(percent);
        },

        /**
         * Convert percent to real values
         *
         * @param percent {Number} X in percent
         * @returns {Number} X in real
         */
        convertToValue: function (percent) {
            var min = this.options.min,
                max = this.options.max,
                min_decimals = min.toString().split(".")[1],
                max_decimals = max.toString().split(".")[1],
                min_length, max_length,
                avg_decimals = 0,
                abs = 0;

            if (percent === 0) {
                return this.options.min;
            }
            if (percent === 100) {
                return this.options.max;
            }


            if (min_decimals) {
                min_length = min_decimals.length;
                avg_decimals = min_length;
            }
            if (max_decimals) {
                max_length = max_decimals.length;
                avg_decimals = max_length;
            }
            if (min_length && max_length) {
                avg_decimals = (min_length >= max_length) ? min_length : max_length;
            }

            if (min < 0) {
                abs = Math.abs(min);
                min = +(min + abs).toFixed(avg_decimals);
                max = +(max + abs).toFixed(avg_decimals);
            }

            var number = ((max - min) / 100 * percent) + min,
                string = this.options.step.toString().split(".")[1],
                result;

            if (string) {
                number = +number.toFixed(string.length);
            } else {
                number = number / this.options.step;
                number = number * this.options.step;

                number = +number.toFixed(0);
            }

            if (abs) {
                number -= abs;
            }

            if (string) {
                result = +number.toFixed(string.length);
            } else {
                result = this.toFixed(number);
            }

            if (result < this.options.min) {
                result = this.options.min;
            } else if (result > this.options.max) {
                result = this.options.max;
            }

            return result;
        },

        /**
         * Round percent value with step
         *
         * @param percent {Number}
         * @returns percent {Number} rounded
         */
        calcWithStep: function (percent) {
            var rounded = Math.round(percent / this.coords.p_step) * this.coords.p_step;

            if (rounded > 100) {
                rounded = 100;
            }
            if (percent === 100) {
                rounded = 100;
            }

            return this.toFixed(rounded);
        },

        checkMinInterval: function (p_current, p_next, type) {
            var o = this.options,
                current,
                next;

            if (!o.min_interval) {
                return p_current;
            }

            current = this.convertToValue(p_current);
            next = this.convertToValue(p_next);

            if (type === "from") {

                if (next - current < o.min_interval) {
                    current = next - o.min_interval;
                }

            } else {

                if (current - next < o.min_interval) {
                    current = next + o.min_interval;
                }

            }

            return this.convertToPercent(current);
        },

        checkMaxInterval: function (p_current, p_next, type) {
            var o = this.options,
                current,
                next;

            if (!o.max_interval) {
                return p_current;
            }

            current = this.convertToValue(p_current);
            next = this.convertToValue(p_next);

            if (type === "from") {

                if (next - current > o.max_interval) {
                    current = next - o.max_interval;
                }

            } else {

                if (current - next > o.max_interval) {
                    current = next + o.max_interval;
                }

            }

            return this.convertToPercent(current);
        },

        checkDiapason: function (p_num, min, max) {
            var num = this.convertToValue(p_num),
                o = this.options;

            if (typeof min !== "number") {
                min = o.min;
            }

            if (typeof max !== "number") {
                max = o.max;
            }

            if (num < min) {
                num = min;
            }

            if (num > max) {
                num = max;
            }

            return this.convertToPercent(num);
        },

        toFixed: function (num) {
            num = num.toFixed(20);
            return +num;
        },

        _prettify: function (num) {
            if (!this.options.prettify_enabled) {
                return num;
            }

            if (this.options.prettify && typeof this.options.prettify === "function") {
                return this.options.prettify(num);
            } else {
                return this.prettify(num);
            }
        },

        prettify: function (num) {
            var n = num.toString();
            return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + this.options.prettify_separator);
        },

        checkEdges: function (left, width) {
            if (!this.options.force_edges) {
                return this.toFixed(left);
            }

            if (left < 0) {
                left = 0;
            } else if (left > 100 - width) {
                left = 100 - width;
            }

            return this.toFixed(left);
        },

        validate: function () {
            var o = this.options,
                r = this.result,
                v = o.values,
                vl = v.length,
                value,
                i;

            if (typeof o.min === "string") o.min = +o.min;
            if (typeof o.max === "string") o.max = +o.max;
            if (typeof o.from === "string") o.from = +o.from;
            if (typeof o.to === "string") o.to = +o.to;
            if (typeof o.step === "string") o.step = +o.step;

            if (typeof o.from_min === "string") o.from_min = +o.from_min;
            if (typeof o.from_max === "string") o.from_max = +o.from_max;
            if (typeof o.to_min === "string") o.to_min = +o.to_min;
            if (typeof o.to_max === "string") o.to_max = +o.to_max;

            if (typeof o.grid_num === "string") o.grid_num = +o.grid_num;

            if (o.max < o.min) {
                o.max = o.min;
            }

            if (vl) {
                o.p_values = [];
                o.min = 0;
                o.max = vl - 1;
                o.step = 1;
                o.grid_num = o.max;
                o.grid_snap = true;

                for (i = 0; i < vl; i++) {
                    value = +v[i];

                    if (!isNaN(value)) {
                        v[i] = value;
                        value = this._prettify(value);
                    } else {
                        value = v[i];
                    }

                    o.p_values.push(value);
                }
            }

            if (typeof o.from !== "number" || isNaN(o.from)) {
                o.from = o.min;
            }

            if (typeof o.to !== "number" || isNaN(o.to)) {
                o.to = o.max;
            }

            if (o.type === "single") {

                if (o.from < o.min) o.from = o.min;
                if (o.from > o.max) o.from = o.max;

            } else {

                if (o.from < o.min) o.from = o.min;
                if (o.from > o.max) o.from = o.max;

                if (o.to < o.min) o.to = o.min;
                if (o.to > o.max) o.to = o.max;

                if (this.update_check.from) {

                    if (this.update_check.from !== o.from) {
                        if (o.from > o.to) o.from = o.to;
                    }
                    if (this.update_check.to !== o.to) {
                        if (o.to < o.from) o.to = o.from;
                    }

                }

                if (o.from > o.to) o.from = o.to;
                if (o.to < o.from) o.to = o.from;

            }

            if (typeof o.step !== "number" || isNaN(o.step) || !o.step || o.step < 0) {
                o.step = 1;
            }

            if (typeof o.from_min === "number" && o.from < o.from_min) {
                o.from = o.from_min;
            }

            if (typeof o.from_max === "number" && o.from > o.from_max) {
                o.from = o.from_max;
            }

            if (typeof o.to_min === "number" && o.to < o.to_min) {
                o.to = o.to_min;
            }

            if (typeof o.to_max === "number" && o.from > o.to_max) {
                o.to = o.to_max;
            }

            if (r) {
                if (r.min !== o.min) {
                    r.min = o.min;
                }

                if (r.max !== o.max) {
                    r.max = o.max;
                }

                if (r.from < r.min || r.from > r.max) {
                    r.from = o.from;
                }

                if (r.to < r.min || r.to > r.max) {
                    r.to = o.to;
                }
            }

            if (typeof o.min_interval !== "number" || isNaN(o.min_interval) || !o.min_interval || o.min_interval < 0) {
                o.min_interval = 0;
            }

            if (typeof o.max_interval !== "number" || isNaN(o.max_interval) || !o.max_interval || o.max_interval < 0) {
                o.max_interval = 0;
            }

            if (o.min_interval && o.min_interval > o.max - o.min) {
                o.min_interval = o.max - o.min;
            }

            if (o.max_interval && o.max_interval > o.max - o.min) {
                o.max_interval = o.max - o.min;
            }
        },

        decorate: function (num, original) {
            var decorated = "",
                o = this.options;

            if (o.prefix) {
                decorated += o.prefix;
            }

            decorated += num;

            if (o.max_postfix) {
                if (o.values.length && num === o.p_values[o.max]) {
                    decorated += o.max_postfix;
                    if (o.postfix) {
                        decorated += " ";
                    }
                } else if (original === o.max) {
                    decorated += o.max_postfix;
                    if (o.postfix) {
                        decorated += " ";
                    }
                }
            }

            if (o.postfix) {
                decorated += o.postfix;
            }

            return decorated;
        },

        updateFrom: function () {
            this.result.from = this.options.from;
            this.result.from_percent = this.convertToPercent(this.result.from);
            this.result.from_pretty = this._prettify(this.result.from);
            if (this.options.values) {
                this.result.from_value = this.options.values[this.result.from];
            }
        },

        updateTo: function () {
            this.result.to = this.options.to;
            this.result.to_percent = this.convertToPercent(this.result.to);
            this.result.to_pretty = this._prettify(this.result.to);
            if (this.options.values) {
                this.result.to_value = this.options.values[this.result.to];
            }
        },

        updateResult: function () {
            this.result.min = this.options.min;
            this.result.max = this.options.max;
            this.updateFrom();
            this.updateTo();
        },


        // =============================================================================================================
        // Grid

        appendGrid: function () {
            if (!this.options.grid) {
                return;
            }

            var o = this.options,
                i, z,

                total = o.max - o.min,
                big_num = o.grid_num,
                big_p = 0,
                big_w = 0,

                small_max = 4,
                local_small_max,
                small_p,
                small_w = 0,

                result,
                html = '';



            this.calcGridMargin();

            if (o.grid_snap) {
                big_num = total / o.step;
            }

            if (big_num > 50) big_num = 50;
            big_p = this.toFixed(100 / big_num);

            if (big_num > 4) {
                small_max = 3;
            }
            if (big_num > 7) {
                small_max = 2;
            }
            if (big_num > 14) {
                small_max = 1;
            }
            if (big_num > 28) {
                small_max = 0;
            }

            for (i = 0; i < big_num + 1; i++) {
                local_small_max = small_max;

                big_w = this.toFixed(big_p * i);

                if (big_w > 100) {
                    big_w = 100;
                }
                this.coords.big[i] = big_w;

                small_p = (big_w - (big_p * (i - 1))) / (local_small_max + 1);

                for (z = 1; z <= local_small_max; z++) {
                    if (big_w === 0) {
                        break;
                    }

                    small_w = this.toFixed(big_w - (small_p * z));

                    html += '<span class="irs-grid-pol small" style="left: ' + small_w + '%"></span>';
                }

                html += '<span class="irs-grid-pol" style="left: ' + big_w + '%"></span>';

                result = this.convertToValue(big_w);
                if (o.values.length) {
                    result = o.p_values[result];
                } else {
                    result = this._prettify(result);
                }

                html += '<span class="irs-grid-text js-grid-text-' + i + '" style="left: ' + big_w + '%">' + result + '</span>';
            }
            this.coords.big_num = Math.ceil(big_num + 1);



            this.$cache.cont.addClass("irs-with-grid");
            this.$cache.grid.html(html);
            this.cacheGridLabels();
        },

        cacheGridLabels: function () {
            var $label, i,
                num = this.coords.big_num;

            for (i = 0; i < num; i++) {
                $label = this.$cache.grid.find(".js-grid-text-" + i);
                this.$cache.grid_labels.push($label);
            }

            this.calcGridLabels();
        },

        calcGridLabels: function () {
            var i, label, start = [], finish = [],
                num = this.coords.big_num;

            for (i = 0; i < num; i++) {
                this.coords.big_w[i] = this.$cache.grid_labels[i].outerWidth(false);
                this.coords.big_p[i] = this.toFixed(this.coords.big_w[i] / this.coords.w_rs * 100);
                this.coords.big_x[i] = this.toFixed(this.coords.big_p[i] / 2);

                start[i] = this.toFixed(this.coords.big[i] - this.coords.big_x[i]);
                finish[i] = this.toFixed(start[i] + this.coords.big_p[i]);
            }

            if (this.options.force_edges) {
                if (start[0] < -this.coords.grid_gap) {
                    start[0] = -this.coords.grid_gap;
                    finish[0] = this.toFixed(start[0] + this.coords.big_p[0]);

                    this.coords.big_x[0] = this.coords.grid_gap;
                }

                if (finish[num - 1] > 100 + this.coords.grid_gap) {
                    finish[num - 1] = 100 + this.coords.grid_gap;
                    start[num - 1] = this.toFixed(finish[num - 1] - this.coords.big_p[num - 1]);

                    this.coords.big_x[num - 1] = this.toFixed(this.coords.big_p[num - 1] - this.coords.grid_gap);
                }
            }

            this.calcGridCollision(2, start, finish);
            this.calcGridCollision(4, start, finish);

            for (i = 0; i < num; i++) {
                label = this.$cache.grid_labels[i][0];

                if (this.coords.big_x[i] !== Number.POSITIVE_INFINITY) {
                    label.style.marginLeft = -this.coords.big_x[i] + "%";
                }
            }
        },

        // Collisions Calc Beta
        // TODO: Refactor then have plenty of time
        calcGridCollision: function (step, start, finish) {
            var i, next_i, label,
                num = this.coords.big_num;

            for (i = 0; i < num; i += step) {
                next_i = i + (step / 2);
                if (next_i >= num) {
                    break;
                }

                label = this.$cache.grid_labels[next_i][0];

                if (finish[i] <= start[next_i]) {
                    label.style.visibility = "visible";
                } else {
                    label.style.visibility = "hidden";
                }
            }
        },

        calcGridMargin: function () {
            if (!this.options.grid_margin) {
                return;
            }

            this.coords.w_rs = this.$cache.rs.outerWidth(false);
            if (!this.coords.w_rs) {
                return;
            }

            if (this.options.type === "single") {
                this.coords.w_handle = this.$cache.s_single.outerWidth(false);
            } else {
                this.coords.w_handle = this.$cache.s_from.outerWidth(false);
            }
            this.coords.p_handle = this.toFixed(this.coords.w_handle  / this.coords.w_rs * 100);
            this.coords.grid_gap = this.toFixed((this.coords.p_handle / 2) - 0.1);

            this.$cache.grid[0].style.width = this.toFixed(100 - this.coords.p_handle) + "%";
            this.$cache.grid[0].style.left = this.coords.grid_gap + "%";
        },



        // =============================================================================================================
        // Public methods

        update: function (options) {
            if (!this.input) {
                return;
            }

            this.is_update = true;

            this.options.from = this.result.from;
            this.options.to = this.result.to;
            this.update_check.from = this.result.from;
            this.update_check.to = this.result.to;

            this.options = $.extend(this.options, options);
            this.validate();
            this.updateResult(options);

            this.toggleInput();
            this.remove();
            this.init(true);
        },

        reset: function () {
            if (!this.input) {
                return;
            }

            this.updateResult();
            this.update();
        },

        destroy: function () {
            if (!this.input) {
                return;
            }

            this.toggleInput();
            this.$cache.input.prop("readonly", false);
            $.data(this.input, "ionRangeSlider", null);

            this.remove();
            this.input = null;
            this.options = null;
        }
    };

    $.fn.ionRangeSlider = function (options) {
        return this.each(function() {
            if (!$.data(this, "ionRangeSlider")) {
                $.data(this, "ionRangeSlider", new IonRangeSlider(this, options, plugin_count++));
            }
        });
    };



    // =================================================================================================================
    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

    // MIT license

    (function() {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                || window[vendors[x]+'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                    timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function(id) {
                clearTimeout(id);
            };
    }());

}));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./node_modules/webpack/buildin/amd-options.js":
/*!****************************************!*\
  !*** (webpack)/buildin/amd-options.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {/* globals __webpack_amd_options__ */
module.exports = __webpack_amd_options__;

/* WEBPACK VAR INJECTION */}.call(this, {}))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvaW9uLXJhbmdlc2xpZGVyL2Nzcy9pb24ucmFuZ2VTbGlkZXIuY3NzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9pb24tcmFuZ2VzbGlkZXIvanMvaW9uLnJhbmdlU2xpZGVyLmpzIiwid2VicGFjazovLy8od2VicGFjaykvYnVpbGRpbi9hbWQtb3B0aW9ucy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSx1Qzs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUM7QUFDRCxzREFBc0QsVUFBYSxtQkFBbUIsZ0dBQVU7QUFDaEcsUUFBUSxpQ0FBTyxDQUFDLHlFQUFRLENBQUMsbUNBQUU7QUFDM0I7QUFDQSxTQUFTO0FBQUEsb0dBQUM7QUFDVixLQUFLO0FBQ0wsZ0JBQWdCLG1CQUFPLENBQUMsb0RBQVE7QUFDaEMsS0FBSztBQUNMO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxxQkFBcUI7O0FBRXJCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7OztBQUlBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLE9BQU87QUFDNUIsdUJBQXVCLE9BQU87QUFDOUIsNEJBQTRCO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxzQ0FBc0M7O0FBRXRDO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSwyR0FBMkc7O0FBRTNHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsT0FBTztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDZDQUE2Qyw4QkFBOEIsV0FBVztBQUN0RjtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsT0FBTztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLE9BQU87QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCO0FBQzFCLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCO0FBQzFCLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQjtBQUMxQixxQkFBcUIsT0FBTztBQUM1QixxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QixRQUFRO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOzs7O0FBSVQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEI7QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7OztBQUdUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBOztBQUVBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTs7QUFFQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQjtBQUMxQixxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsYUFBYTs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFNBQVM7Ozs7QUFJVDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBOztBQUVBLGFBQWE7O0FBRWI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGlCQUFpQjtBQUNqQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBOztBQUVBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7OztBQUlUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOzs7O0FBSVQ7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQSxTQUFTOzs7OztBQUtUO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QixPQUFPO0FBQ2hDLDBCQUEwQixTQUFTO0FBQ25DLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBOztBQUVBOztBQUVBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsT0FBTztBQUNsQyxxQkFBcUIsT0FBTztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBOztBQUVBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQSwyQkFBMkI7QUFDM0IsNkJBQTZCLE9BQU87QUFDcEM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGFBQWE7O0FBRWI7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsYUFBYTs7QUFFYjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0Esa0NBQWtDLElBQUk7QUFDdEMsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDJCQUEyQixRQUFRO0FBQ25DOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUEsYUFBYTs7QUFFYjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7O0FBR1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7Ozs7QUFJQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsdUJBQXVCLGlCQUFpQjtBQUN4Qzs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSwyQkFBMkIsc0JBQXNCO0FBQ2pEO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBOztBQUVBLHVCQUF1QixTQUFTO0FBQ2hDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBOztBQUVBLHVCQUF1QixTQUFTO0FBQ2hDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUJBQXVCLFNBQVM7QUFDaEM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsdUJBQXVCLFNBQVM7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7Ozs7QUFJVDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7Ozs7QUFJQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLHFEQUFxRDtBQUMzRTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVEQUF1RCxpQ0FBaUMsRUFBRTtBQUMxRjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUwsQ0FBQzs7Ozs7Ozs7Ozs7OztBQ2g1RUQ7QUFDQSIsImZpbGUiOiJ2ZW5kb3JzfkFyY2hpdmUvU2VhcmNoQ3RybH5CaWdEYXRhL1NlYXJjaEN0cmx+TGlicmFyeS9Jb25SYW5nZVNsaWRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsIi8vIElvbi5SYW5nZVNsaWRlclxuLy8gdmVyc2lvbiAyLjMuMSBCdWlsZDogMzgyXG4vLyDCqSBEZW5pcyBJbmVzaGluLCAyMDE5XG4vLyBodHRwczovL2dpdGh1Yi5jb20vSW9uRGVuXG4vL1xuLy8gUHJvamVjdCBwYWdlOiAgICBodHRwOi8vaW9uZGVuLmNvbS9hL3BsdWdpbnMvaW9uLnJhbmdlU2xpZGVyL2VuLmh0bWxcbi8vIEdpdEh1YiBwYWdlOiAgICAgaHR0cHM6Ly9naXRodWIuY29tL0lvbkRlbi9pb24ucmFuZ2VTbGlkZXJcbi8vXG4vLyBSZWxlYXNlZCB1bmRlciBNSVQgbGljZW5jZTpcbi8vIGh0dHA6Ly9pb25kZW4uY29tL2EvcGx1Z2lucy9saWNlbmNlLWVuLmh0bWxcbi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG47KGZ1bmN0aW9uKGZhY3RvcnkpIHtcbiAgICBpZiAoKHR5cGVvZiBqUXVlcnkgPT09ICd1bmRlZmluZWQnIHx8ICFqUXVlcnkpICYmIHR5cGVvZiBkZWZpbmUgPT09IFwiZnVuY3Rpb25cIiAmJiBkZWZpbmUuYW1kKSB7XG4gICAgICAgIGRlZmluZShbXCJqcXVlcnlcIl0sIGZ1bmN0aW9uIChqUXVlcnkpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWN0b3J5KGpRdWVyeSwgZG9jdW1lbnQsIHdpbmRvdywgbmF2aWdhdG9yKTtcbiAgICAgICAgfSk7XG4gICAgfSBlbHNlIGlmICgodHlwZW9mIGpRdWVyeSA9PT0gJ3VuZGVmaW5lZCcgfHwgIWpRdWVyeSkgJiYgdHlwZW9mIGV4cG9ydHMgPT09IFwib2JqZWN0XCIpIHtcbiAgICAgICAgZmFjdG9yeShyZXF1aXJlKFwianF1ZXJ5XCIpLCBkb2N1bWVudCwgd2luZG93LCBuYXZpZ2F0b3IpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGZhY3RvcnkoalF1ZXJ5LCBkb2N1bWVudCwgd2luZG93LCBuYXZpZ2F0b3IpO1xuICAgIH1cbn0gKGZ1bmN0aW9uICgkLCBkb2N1bWVudCwgd2luZG93LCBuYXZpZ2F0b3IsIHVuZGVmaW5lZCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICAvLyBTZXJ2aWNlXG5cbiAgICB2YXIgcGx1Z2luX2NvdW50ID0gMDtcblxuICAgIC8vIElFOCBmaXhcbiAgICB2YXIgaXNfb2xkX2llID0gKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIG4gPSBuYXZpZ2F0b3IudXNlckFnZW50LFxuICAgICAgICAgICAgciA9IC9tc2llXFxzXFxkKy9pLFxuICAgICAgICAgICAgdjtcbiAgICAgICAgaWYgKG4uc2VhcmNoKHIpID4gMCkge1xuICAgICAgICAgICAgdiA9IHIuZXhlYyhuKS50b1N0cmluZygpO1xuICAgICAgICAgICAgdiA9IHYuc3BsaXQoXCIgXCIpWzFdO1xuICAgICAgICAgICAgaWYgKHYgPCA5KSB7XG4gICAgICAgICAgICAgICAgJChcImh0bWxcIikuYWRkQ2xhc3MoXCJsdC1pZTlcIik7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0gKCkpO1xuICAgIGlmICghRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQpIHtcbiAgICAgICAgRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQgPSBmdW5jdGlvbiBiaW5kKHRoYXQpIHtcblxuICAgICAgICAgICAgdmFyIHRhcmdldCA9IHRoaXM7XG4gICAgICAgICAgICB2YXIgc2xpY2UgPSBbXS5zbGljZTtcblxuICAgICAgICAgICAgaWYgKHR5cGVvZiB0YXJnZXQgIT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcigpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgYXJncyA9IHNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcbiAgICAgICAgICAgICAgICBib3VuZCA9IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcyBpbnN0YW5jZW9mIGJvdW5kKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBGID0gZnVuY3Rpb24oKXt9O1xuICAgICAgICAgICAgICAgICAgICAgICAgRi5wcm90b3R5cGUgPSB0YXJnZXQucHJvdG90eXBlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHNlbGYgPSBuZXcgRigpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcmVzdWx0ID0gdGFyZ2V0LmFwcGx5KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJncy5jb25jYXQoc2xpY2UuY2FsbChhcmd1bWVudHMpKVxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChPYmplY3QocmVzdWx0KSA9PT0gcmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBzZWxmO1xuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0YXJnZXQuYXBwbHkoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmdzLmNvbmNhdChzbGljZS5jYWxsKGFyZ3VtZW50cykpXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHJldHVybiBib3VuZDtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgaWYgKCFBcnJheS5wcm90b3R5cGUuaW5kZXhPZikge1xuICAgICAgICBBcnJheS5wcm90b3R5cGUuaW5kZXhPZiA9IGZ1bmN0aW9uKHNlYXJjaEVsZW1lbnQsIGZyb21JbmRleCkge1xuICAgICAgICAgICAgdmFyIGs7XG4gICAgICAgICAgICBpZiAodGhpcyA9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignXCJ0aGlzXCIgaXMgbnVsbCBvciBub3QgZGVmaW5lZCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIE8gPSBPYmplY3QodGhpcyk7XG4gICAgICAgICAgICB2YXIgbGVuID0gTy5sZW5ndGggPj4+IDA7XG4gICAgICAgICAgICBpZiAobGVuID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIG4gPSArZnJvbUluZGV4IHx8IDA7XG4gICAgICAgICAgICBpZiAoTWF0aC5hYnMobikgPT09IEluZmluaXR5KSB7XG4gICAgICAgICAgICAgICAgbiA9IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAobiA+PSBsZW4pIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gLTE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBrID0gTWF0aC5tYXgobiA+PSAwID8gbiA6IGxlbiAtIE1hdGguYWJzKG4pLCAwKTtcbiAgICAgICAgICAgIHdoaWxlIChrIDwgbGVuKSB7XG4gICAgICAgICAgICAgICAgaWYgKGsgaW4gTyAmJiBPW2tdID09PSBzZWFyY2hFbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBrKys7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gLTE7XG4gICAgICAgIH07XG4gICAgfVxuXG5cblxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gICAgLy8gVGVtcGxhdGVcblxuICAgIHZhciBiYXNlX2h0bWwgPVxuICAgICAgICAnPHNwYW4gY2xhc3M9XCJpcnNcIj4nICtcbiAgICAgICAgJzxzcGFuIGNsYXNzPVwiaXJzLWxpbmVcIiB0YWJpbmRleD1cIjBcIj48L3NwYW4+JyArXG4gICAgICAgICc8c3BhbiBjbGFzcz1cImlycy1taW5cIj4wPC9zcGFuPjxzcGFuIGNsYXNzPVwiaXJzLW1heFwiPjE8L3NwYW4+JyArXG4gICAgICAgICc8c3BhbiBjbGFzcz1cImlycy1mcm9tXCI+MDwvc3Bhbj48c3BhbiBjbGFzcz1cImlycy10b1wiPjA8L3NwYW4+PHNwYW4gY2xhc3M9XCJpcnMtc2luZ2xlXCI+MDwvc3Bhbj4nICtcbiAgICAgICAgJzwvc3Bhbj4nICtcbiAgICAgICAgJzxzcGFuIGNsYXNzPVwiaXJzLWdyaWRcIj48L3NwYW4+JztcblxuICAgIHZhciBzaW5nbGVfaHRtbCA9XG4gICAgICAgICc8c3BhbiBjbGFzcz1cImlycy1iYXIgaXJzLWJhci0tc2luZ2xlXCI+PC9zcGFuPicgK1xuICAgICAgICAnPHNwYW4gY2xhc3M9XCJpcnMtc2hhZG93IHNoYWRvdy1zaW5nbGVcIj48L3NwYW4+JyArXG4gICAgICAgICc8c3BhbiBjbGFzcz1cImlycy1oYW5kbGUgc2luZ2xlXCI+PGk+PC9pPjxpPjwvaT48aT48L2k+PC9zcGFuPic7XG5cbiAgICB2YXIgZG91YmxlX2h0bWwgPVxuICAgICAgICAnPHNwYW4gY2xhc3M9XCJpcnMtYmFyXCI+PC9zcGFuPicgK1xuICAgICAgICAnPHNwYW4gY2xhc3M9XCJpcnMtc2hhZG93IHNoYWRvdy1mcm9tXCI+PC9zcGFuPicgK1xuICAgICAgICAnPHNwYW4gY2xhc3M9XCJpcnMtc2hhZG93IHNoYWRvdy10b1wiPjwvc3Bhbj4nICtcbiAgICAgICAgJzxzcGFuIGNsYXNzPVwiaXJzLWhhbmRsZSBmcm9tXCI+PGk+PC9pPjxpPjwvaT48aT48L2k+PC9zcGFuPicgK1xuICAgICAgICAnPHNwYW4gY2xhc3M9XCJpcnMtaGFuZGxlIHRvXCI+PGk+PC9pPjxpPjwvaT48aT48L2k+PC9zcGFuPic7XG5cbiAgICB2YXIgZGlzYWJsZV9odG1sID1cbiAgICAgICAgJzxzcGFuIGNsYXNzPVwiaXJzLWRpc2FibGUtbWFza1wiPjwvc3Bhbj4nO1xuXG5cblxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gICAgLy8gQ29yZVxuXG4gICAgLyoqXG4gICAgICogTWFpbiBwbHVnaW4gY29uc3RydWN0b3JcbiAgICAgKlxuICAgICAqIEBwYXJhbSBpbnB1dCB7T2JqZWN0fSBsaW5rIHRvIGJhc2UgaW5wdXQgZWxlbWVudFxuICAgICAqIEBwYXJhbSBvcHRpb25zIHtPYmplY3R9IHNsaWRlciBjb25maWdcbiAgICAgKiBAcGFyYW0gcGx1Z2luX2NvdW50IHtOdW1iZXJ9XG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgdmFyIElvblJhbmdlU2xpZGVyID0gZnVuY3Rpb24gKGlucHV0LCBvcHRpb25zLCBwbHVnaW5fY291bnQpIHtcbiAgICAgICAgdGhpcy5WRVJTSU9OID0gXCIyLjMuMVwiO1xuICAgICAgICB0aGlzLmlucHV0ID0gaW5wdXQ7XG4gICAgICAgIHRoaXMucGx1Z2luX2NvdW50ID0gcGx1Z2luX2NvdW50O1xuICAgICAgICB0aGlzLmN1cnJlbnRfcGx1Z2luID0gMDtcbiAgICAgICAgdGhpcy5jYWxjX2NvdW50ID0gMDtcbiAgICAgICAgdGhpcy51cGRhdGVfdG0gPSAwO1xuICAgICAgICB0aGlzLm9sZF9mcm9tID0gMDtcbiAgICAgICAgdGhpcy5vbGRfdG8gPSAwO1xuICAgICAgICB0aGlzLm9sZF9taW5faW50ZXJ2YWwgPSBudWxsO1xuICAgICAgICB0aGlzLnJhZl9pZCA9IG51bGw7XG4gICAgICAgIHRoaXMuZHJhZ2dpbmcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5mb3JjZV9yZWRyYXcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5ub19kaWFwYXNvbiA9IGZhbHNlO1xuICAgICAgICB0aGlzLmhhc190YWJfaW5kZXggPSB0cnVlO1xuICAgICAgICB0aGlzLmlzX2tleSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmlzX3VwZGF0ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmlzX3N0YXJ0ID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5pc19maW5pc2ggPSBmYWxzZTtcbiAgICAgICAgdGhpcy5pc19hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5pc19yZXNpemUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5pc19jbGljayA9IGZhbHNlO1xuXG4gICAgICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuXG4gICAgICAgIC8vIGNhY2hlIGZvciBsaW5rcyB0byBhbGwgRE9NIGVsZW1lbnRzXG4gICAgICAgIHRoaXMuJGNhY2hlID0ge1xuICAgICAgICAgICAgd2luOiAkKHdpbmRvdyksXG4gICAgICAgICAgICBib2R5OiAkKGRvY3VtZW50LmJvZHkpLFxuICAgICAgICAgICAgaW5wdXQ6ICQoaW5wdXQpLFxuICAgICAgICAgICAgY29udDogbnVsbCxcbiAgICAgICAgICAgIHJzOiBudWxsLFxuICAgICAgICAgICAgbWluOiBudWxsLFxuICAgICAgICAgICAgbWF4OiBudWxsLFxuICAgICAgICAgICAgZnJvbTogbnVsbCxcbiAgICAgICAgICAgIHRvOiBudWxsLFxuICAgICAgICAgICAgc2luZ2xlOiBudWxsLFxuICAgICAgICAgICAgYmFyOiBudWxsLFxuICAgICAgICAgICAgbGluZTogbnVsbCxcbiAgICAgICAgICAgIHNfc2luZ2xlOiBudWxsLFxuICAgICAgICAgICAgc19mcm9tOiBudWxsLFxuICAgICAgICAgICAgc190bzogbnVsbCxcbiAgICAgICAgICAgIHNoYWRfc2luZ2xlOiBudWxsLFxuICAgICAgICAgICAgc2hhZF9mcm9tOiBudWxsLFxuICAgICAgICAgICAgc2hhZF90bzogbnVsbCxcbiAgICAgICAgICAgIGVkZ2U6IG51bGwsXG4gICAgICAgICAgICBncmlkOiBudWxsLFxuICAgICAgICAgICAgZ3JpZF9sYWJlbHM6IFtdXG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gc3RvcmFnZSBmb3IgbWVhc3VyZSB2YXJpYWJsZXNcbiAgICAgICAgdGhpcy5jb29yZHMgPSB7XG4gICAgICAgICAgICAvLyBsZWZ0XG4gICAgICAgICAgICB4X2dhcDogMCxcbiAgICAgICAgICAgIHhfcG9pbnRlcjogMCxcblxuICAgICAgICAgICAgLy8gd2lkdGhcbiAgICAgICAgICAgIHdfcnM6IDAsXG4gICAgICAgICAgICB3X3JzX29sZDogMCxcbiAgICAgICAgICAgIHdfaGFuZGxlOiAwLFxuXG4gICAgICAgICAgICAvLyBwZXJjZW50c1xuICAgICAgICAgICAgcF9nYXA6IDAsXG4gICAgICAgICAgICBwX2dhcF9sZWZ0OiAwLFxuICAgICAgICAgICAgcF9nYXBfcmlnaHQ6IDAsXG4gICAgICAgICAgICBwX3N0ZXA6IDAsXG4gICAgICAgICAgICBwX3BvaW50ZXI6IDAsXG4gICAgICAgICAgICBwX2hhbmRsZTogMCxcbiAgICAgICAgICAgIHBfc2luZ2xlX2Zha2U6IDAsXG4gICAgICAgICAgICBwX3NpbmdsZV9yZWFsOiAwLFxuICAgICAgICAgICAgcF9mcm9tX2Zha2U6IDAsXG4gICAgICAgICAgICBwX2Zyb21fcmVhbDogMCxcbiAgICAgICAgICAgIHBfdG9fZmFrZTogMCxcbiAgICAgICAgICAgIHBfdG9fcmVhbDogMCxcbiAgICAgICAgICAgIHBfYmFyX3g6IDAsXG4gICAgICAgICAgICBwX2Jhcl93OiAwLFxuXG4gICAgICAgICAgICAvLyBncmlkXG4gICAgICAgICAgICBncmlkX2dhcDogMCxcbiAgICAgICAgICAgIGJpZ19udW06IDAsXG4gICAgICAgICAgICBiaWc6IFtdLFxuICAgICAgICAgICAgYmlnX3c6IFtdLFxuICAgICAgICAgICAgYmlnX3A6IFtdLFxuICAgICAgICAgICAgYmlnX3g6IFtdXG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gc3RvcmFnZSBmb3IgbGFiZWxzIG1lYXN1cmUgdmFyaWFibGVzXG4gICAgICAgIHRoaXMubGFiZWxzID0ge1xuICAgICAgICAgICAgLy8gd2lkdGhcbiAgICAgICAgICAgIHdfbWluOiAwLFxuICAgICAgICAgICAgd19tYXg6IDAsXG4gICAgICAgICAgICB3X2Zyb206IDAsXG4gICAgICAgICAgICB3X3RvOiAwLFxuICAgICAgICAgICAgd19zaW5nbGU6IDAsXG5cbiAgICAgICAgICAgIC8vIHBlcmNlbnRzXG4gICAgICAgICAgICBwX21pbjogMCxcbiAgICAgICAgICAgIHBfbWF4OiAwLFxuICAgICAgICAgICAgcF9mcm9tX2Zha2U6IDAsXG4gICAgICAgICAgICBwX2Zyb21fbGVmdDogMCxcbiAgICAgICAgICAgIHBfdG9fZmFrZTogMCxcbiAgICAgICAgICAgIHBfdG9fbGVmdDogMCxcbiAgICAgICAgICAgIHBfc2luZ2xlX2Zha2U6IDAsXG4gICAgICAgICAgICBwX3NpbmdsZV9sZWZ0OiAwXG4gICAgICAgIH07XG5cblxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBnZXQgYW5kIHZhbGlkYXRlIGNvbmZpZ1xuICAgICAgICAgKi9cbiAgICAgICAgdmFyICRpbnAgPSB0aGlzLiRjYWNoZS5pbnB1dCxcbiAgICAgICAgICAgIHZhbCA9ICRpbnAucHJvcChcInZhbHVlXCIpLFxuICAgICAgICAgICAgY29uZmlnLCBjb25maWdfZnJvbV9kYXRhLCBwcm9wO1xuXG4gICAgICAgIC8vIGRlZmF1bHQgY29uZmlnXG4gICAgICAgIGNvbmZpZyA9IHtcbiAgICAgICAgICAgIHNraW46IFwiZmxhdFwiLFxuICAgICAgICAgICAgdHlwZTogXCJzaW5nbGVcIixcblxuICAgICAgICAgICAgbWluOiAxMCxcbiAgICAgICAgICAgIG1heDogMTAwLFxuICAgICAgICAgICAgZnJvbTogbnVsbCxcbiAgICAgICAgICAgIHRvOiBudWxsLFxuICAgICAgICAgICAgc3RlcDogMSxcblxuICAgICAgICAgICAgbWluX2ludGVydmFsOiAwLFxuICAgICAgICAgICAgbWF4X2ludGVydmFsOiAwLFxuICAgICAgICAgICAgZHJhZ19pbnRlcnZhbDogZmFsc2UsXG5cbiAgICAgICAgICAgIHZhbHVlczogW10sXG4gICAgICAgICAgICBwX3ZhbHVlczogW10sXG5cbiAgICAgICAgICAgIGZyb21fZml4ZWQ6IGZhbHNlLFxuICAgICAgICAgICAgZnJvbV9taW46IG51bGwsXG4gICAgICAgICAgICBmcm9tX21heDogbnVsbCxcbiAgICAgICAgICAgIGZyb21fc2hhZG93OiBmYWxzZSxcblxuICAgICAgICAgICAgdG9fZml4ZWQ6IGZhbHNlLFxuICAgICAgICAgICAgdG9fbWluOiBudWxsLFxuICAgICAgICAgICAgdG9fbWF4OiBudWxsLFxuICAgICAgICAgICAgdG9fc2hhZG93OiBmYWxzZSxcblxuICAgICAgICAgICAgcHJldHRpZnlfZW5hYmxlZDogdHJ1ZSxcbiAgICAgICAgICAgIHByZXR0aWZ5X3NlcGFyYXRvcjogXCIgXCIsXG4gICAgICAgICAgICBwcmV0dGlmeTogbnVsbCxcblxuICAgICAgICAgICAgZm9yY2VfZWRnZXM6IGZhbHNlLFxuXG4gICAgICAgICAgICBrZXlib2FyZDogdHJ1ZSxcblxuICAgICAgICAgICAgZ3JpZDogZmFsc2UsXG4gICAgICAgICAgICBncmlkX21hcmdpbjogdHJ1ZSxcbiAgICAgICAgICAgIGdyaWRfbnVtOiA0LFxuICAgICAgICAgICAgZ3JpZF9zbmFwOiBmYWxzZSxcblxuICAgICAgICAgICAgaGlkZV9taW5fbWF4OiBmYWxzZSxcbiAgICAgICAgICAgIGhpZGVfZnJvbV90bzogZmFsc2UsXG5cbiAgICAgICAgICAgIHByZWZpeDogXCJcIixcbiAgICAgICAgICAgIHBvc3RmaXg6IFwiXCIsXG4gICAgICAgICAgICBtYXhfcG9zdGZpeDogXCJcIixcbiAgICAgICAgICAgIGRlY29yYXRlX2JvdGg6IHRydWUsXG4gICAgICAgICAgICB2YWx1ZXNfc2VwYXJhdG9yOiBcIiDigJQgXCIsXG5cbiAgICAgICAgICAgIGlucHV0X3ZhbHVlc19zZXBhcmF0b3I6IFwiO1wiLFxuXG4gICAgICAgICAgICBkaXNhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgIGJsb2NrOiBmYWxzZSxcblxuICAgICAgICAgICAgZXh0cmFfY2xhc3NlczogXCJcIixcblxuICAgICAgICAgICAgc2NvcGU6IG51bGwsXG4gICAgICAgICAgICBvblN0YXJ0OiBudWxsLFxuICAgICAgICAgICAgb25DaGFuZ2U6IG51bGwsXG4gICAgICAgICAgICBvbkZpbmlzaDogbnVsbCxcbiAgICAgICAgICAgIG9uVXBkYXRlOiBudWxsXG4gICAgICAgIH07XG5cblxuICAgICAgICAvLyBjaGVjayBpZiBiYXNlIGVsZW1lbnQgaXMgaW5wdXRcbiAgICAgICAgaWYgKCRpbnBbMF0ubm9kZU5hbWUgIT09IFwiSU5QVVRcIikge1xuICAgICAgICAgICAgY29uc29sZSAmJiBjb25zb2xlLndhcm4gJiYgY29uc29sZS53YXJuKFwiQmFzZSBlbGVtZW50IHNob3VsZCBiZSA8aW5wdXQ+IVwiLCAkaW5wWzBdKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgLy8gY29uZmlnIGZyb20gZGF0YS1hdHRyaWJ1dGVzIGV4dGVuZHMganMgY29uZmlnXG4gICAgICAgIGNvbmZpZ19mcm9tX2RhdGEgPSB7XG4gICAgICAgICAgICBza2luOiAkaW5wLmRhdGEoXCJza2luXCIpLFxuICAgICAgICAgICAgdHlwZTogJGlucC5kYXRhKFwidHlwZVwiKSxcblxuICAgICAgICAgICAgbWluOiAkaW5wLmRhdGEoXCJtaW5cIiksXG4gICAgICAgICAgICBtYXg6ICRpbnAuZGF0YShcIm1heFwiKSxcbiAgICAgICAgICAgIGZyb206ICRpbnAuZGF0YShcImZyb21cIiksXG4gICAgICAgICAgICB0bzogJGlucC5kYXRhKFwidG9cIiksXG4gICAgICAgICAgICBzdGVwOiAkaW5wLmRhdGEoXCJzdGVwXCIpLFxuXG4gICAgICAgICAgICBtaW5faW50ZXJ2YWw6ICRpbnAuZGF0YShcIm1pbkludGVydmFsXCIpLFxuICAgICAgICAgICAgbWF4X2ludGVydmFsOiAkaW5wLmRhdGEoXCJtYXhJbnRlcnZhbFwiKSxcbiAgICAgICAgICAgIGRyYWdfaW50ZXJ2YWw6ICRpbnAuZGF0YShcImRyYWdJbnRlcnZhbFwiKSxcblxuICAgICAgICAgICAgdmFsdWVzOiAkaW5wLmRhdGEoXCJ2YWx1ZXNcIiksXG5cbiAgICAgICAgICAgIGZyb21fZml4ZWQ6ICRpbnAuZGF0YShcImZyb21GaXhlZFwiKSxcbiAgICAgICAgICAgIGZyb21fbWluOiAkaW5wLmRhdGEoXCJmcm9tTWluXCIpLFxuICAgICAgICAgICAgZnJvbV9tYXg6ICRpbnAuZGF0YShcImZyb21NYXhcIiksXG4gICAgICAgICAgICBmcm9tX3NoYWRvdzogJGlucC5kYXRhKFwiZnJvbVNoYWRvd1wiKSxcblxuICAgICAgICAgICAgdG9fZml4ZWQ6ICRpbnAuZGF0YShcInRvRml4ZWRcIiksXG4gICAgICAgICAgICB0b19taW46ICRpbnAuZGF0YShcInRvTWluXCIpLFxuICAgICAgICAgICAgdG9fbWF4OiAkaW5wLmRhdGEoXCJ0b01heFwiKSxcbiAgICAgICAgICAgIHRvX3NoYWRvdzogJGlucC5kYXRhKFwidG9TaGFkb3dcIiksXG5cbiAgICAgICAgICAgIHByZXR0aWZ5X2VuYWJsZWQ6ICRpbnAuZGF0YShcInByZXR0aWZ5RW5hYmxlZFwiKSxcbiAgICAgICAgICAgIHByZXR0aWZ5X3NlcGFyYXRvcjogJGlucC5kYXRhKFwicHJldHRpZnlTZXBhcmF0b3JcIiksXG5cbiAgICAgICAgICAgIGZvcmNlX2VkZ2VzOiAkaW5wLmRhdGEoXCJmb3JjZUVkZ2VzXCIpLFxuXG4gICAgICAgICAgICBrZXlib2FyZDogJGlucC5kYXRhKFwia2V5Ym9hcmRcIiksXG5cbiAgICAgICAgICAgIGdyaWQ6ICRpbnAuZGF0YShcImdyaWRcIiksXG4gICAgICAgICAgICBncmlkX21hcmdpbjogJGlucC5kYXRhKFwiZ3JpZE1hcmdpblwiKSxcbiAgICAgICAgICAgIGdyaWRfbnVtOiAkaW5wLmRhdGEoXCJncmlkTnVtXCIpLFxuICAgICAgICAgICAgZ3JpZF9zbmFwOiAkaW5wLmRhdGEoXCJncmlkU25hcFwiKSxcblxuICAgICAgICAgICAgaGlkZV9taW5fbWF4OiAkaW5wLmRhdGEoXCJoaWRlTWluTWF4XCIpLFxuICAgICAgICAgICAgaGlkZV9mcm9tX3RvOiAkaW5wLmRhdGEoXCJoaWRlRnJvbVRvXCIpLFxuXG4gICAgICAgICAgICBwcmVmaXg6ICRpbnAuZGF0YShcInByZWZpeFwiKSxcbiAgICAgICAgICAgIHBvc3RmaXg6ICRpbnAuZGF0YShcInBvc3RmaXhcIiksXG4gICAgICAgICAgICBtYXhfcG9zdGZpeDogJGlucC5kYXRhKFwibWF4UG9zdGZpeFwiKSxcbiAgICAgICAgICAgIGRlY29yYXRlX2JvdGg6ICRpbnAuZGF0YShcImRlY29yYXRlQm90aFwiKSxcbiAgICAgICAgICAgIHZhbHVlc19zZXBhcmF0b3I6ICRpbnAuZGF0YShcInZhbHVlc1NlcGFyYXRvclwiKSxcblxuICAgICAgICAgICAgaW5wdXRfdmFsdWVzX3NlcGFyYXRvcjogJGlucC5kYXRhKFwiaW5wdXRWYWx1ZXNTZXBhcmF0b3JcIiksXG5cbiAgICAgICAgICAgIGRpc2FibGU6ICRpbnAuZGF0YShcImRpc2FibGVcIiksXG4gICAgICAgICAgICBibG9jazogJGlucC5kYXRhKFwiYmxvY2tcIiksXG5cbiAgICAgICAgICAgIGV4dHJhX2NsYXNzZXM6ICRpbnAuZGF0YShcImV4dHJhQ2xhc3Nlc1wiKSxcbiAgICAgICAgfTtcbiAgICAgICAgY29uZmlnX2Zyb21fZGF0YS52YWx1ZXMgPSBjb25maWdfZnJvbV9kYXRhLnZhbHVlcyAmJiBjb25maWdfZnJvbV9kYXRhLnZhbHVlcy5zcGxpdChcIixcIik7XG5cbiAgICAgICAgZm9yIChwcm9wIGluIGNvbmZpZ19mcm9tX2RhdGEpIHtcbiAgICAgICAgICAgIGlmIChjb25maWdfZnJvbV9kYXRhLmhhc093blByb3BlcnR5KHByb3ApKSB7XG4gICAgICAgICAgICAgICAgaWYgKGNvbmZpZ19mcm9tX2RhdGFbcHJvcF0gPT09IHVuZGVmaW5lZCB8fCBjb25maWdfZnJvbV9kYXRhW3Byb3BdID09PSBcIlwiKSB7XG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBjb25maWdfZnJvbV9kYXRhW3Byb3BdO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG5cbiAgICAgICAgLy8gaW5wdXQgdmFsdWUgZXh0ZW5kcyBkZWZhdWx0IGNvbmZpZ1xuICAgICAgICBpZiAodmFsICE9PSB1bmRlZmluZWQgJiYgdmFsICE9PSBcIlwiKSB7XG4gICAgICAgICAgICB2YWwgPSB2YWwuc3BsaXQoY29uZmlnX2Zyb21fZGF0YS5pbnB1dF92YWx1ZXNfc2VwYXJhdG9yIHx8IG9wdGlvbnMuaW5wdXRfdmFsdWVzX3NlcGFyYXRvciB8fCBcIjtcIik7XG5cbiAgICAgICAgICAgIGlmICh2YWxbMF0gJiYgdmFsWzBdID09ICt2YWxbMF0pIHtcbiAgICAgICAgICAgICAgICB2YWxbMF0gPSArdmFsWzBdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHZhbFsxXSAmJiB2YWxbMV0gPT0gK3ZhbFsxXSkge1xuICAgICAgICAgICAgICAgIHZhbFsxXSA9ICt2YWxbMV07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChvcHRpb25zICYmIG9wdGlvbnMudmFsdWVzICYmIG9wdGlvbnMudmFsdWVzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgIGNvbmZpZy5mcm9tID0gdmFsWzBdICYmIG9wdGlvbnMudmFsdWVzLmluZGV4T2YodmFsWzBdKTtcbiAgICAgICAgICAgICAgICBjb25maWcudG8gPSB2YWxbMV0gJiYgb3B0aW9ucy52YWx1ZXMuaW5kZXhPZih2YWxbMV0pO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25maWcuZnJvbSA9IHZhbFswXSAmJiArdmFsWzBdO1xuICAgICAgICAgICAgICAgIGNvbmZpZy50byA9IHZhbFsxXSAmJiArdmFsWzFdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cblxuXG4gICAgICAgIC8vIGpzIGNvbmZpZyBleHRlbmRzIGRlZmF1bHQgY29uZmlnXG4gICAgICAgICQuZXh0ZW5kKGNvbmZpZywgb3B0aW9ucyk7XG5cblxuICAgICAgICAvLyBkYXRhIGNvbmZpZyBleHRlbmRzIGNvbmZpZ1xuICAgICAgICAkLmV4dGVuZChjb25maWcsIGNvbmZpZ19mcm9tX2RhdGEpO1xuICAgICAgICB0aGlzLm9wdGlvbnMgPSBjb25maWc7XG5cblxuXG4gICAgICAgIC8vIHZhbGlkYXRlIGNvbmZpZywgdG8gYmUgc3VyZSB0aGF0IGFsbCBkYXRhIHR5cGVzIGFyZSBjb3JyZWN0XG4gICAgICAgIHRoaXMudXBkYXRlX2NoZWNrID0ge307XG4gICAgICAgIHRoaXMudmFsaWRhdGUoKTtcblxuXG5cbiAgICAgICAgLy8gZGVmYXVsdCByZXN1bHQgb2JqZWN0LCByZXR1cm5lZCB0byBjYWxsYmFja3NcbiAgICAgICAgdGhpcy5yZXN1bHQgPSB7XG4gICAgICAgICAgICBpbnB1dDogdGhpcy4kY2FjaGUuaW5wdXQsXG4gICAgICAgICAgICBzbGlkZXI6IG51bGwsXG5cbiAgICAgICAgICAgIG1pbjogdGhpcy5vcHRpb25zLm1pbixcbiAgICAgICAgICAgIG1heDogdGhpcy5vcHRpb25zLm1heCxcblxuICAgICAgICAgICAgZnJvbTogdGhpcy5vcHRpb25zLmZyb20sXG4gICAgICAgICAgICBmcm9tX3BlcmNlbnQ6IDAsXG4gICAgICAgICAgICBmcm9tX3ZhbHVlOiBudWxsLFxuXG4gICAgICAgICAgICB0bzogdGhpcy5vcHRpb25zLnRvLFxuICAgICAgICAgICAgdG9fcGVyY2VudDogMCxcbiAgICAgICAgICAgIHRvX3ZhbHVlOiBudWxsXG4gICAgICAgIH07XG5cblxuXG4gICAgICAgIHRoaXMuaW5pdCgpO1xuICAgIH07XG5cbiAgICBJb25SYW5nZVNsaWRlci5wcm90b3R5cGUgPSB7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFN0YXJ0cyBvciB1cGRhdGVzIHRoZSBwbHVnaW4gaW5zdGFuY2VcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIFtpc191cGRhdGVdIHtib29sZWFufVxuICAgICAgICAgKi9cbiAgICAgICAgaW5pdDogZnVuY3Rpb24gKGlzX3VwZGF0ZSkge1xuICAgICAgICAgICAgdGhpcy5ub19kaWFwYXNvbiA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5jb29yZHMucF9zdGVwID0gdGhpcy5jb252ZXJ0VG9QZXJjZW50KHRoaXMub3B0aW9ucy5zdGVwLCB0cnVlKTtcblxuICAgICAgICAgICAgdGhpcy50YXJnZXQgPSBcImJhc2VcIjtcblxuICAgICAgICAgICAgdGhpcy50b2dnbGVJbnB1dCgpO1xuICAgICAgICAgICAgdGhpcy5hcHBlbmQoKTtcbiAgICAgICAgICAgIHRoaXMuc2V0TWluTWF4KCk7XG5cbiAgICAgICAgICAgIGlmIChpc191cGRhdGUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmZvcmNlX3JlZHJhdyA9IHRydWU7XG4gICAgICAgICAgICAgICAgdGhpcy5jYWxjKHRydWUpO1xuXG4gICAgICAgICAgICAgICAgLy8gY2FsbGJhY2tzIGNhbGxlZFxuICAgICAgICAgICAgICAgIHRoaXMuY2FsbE9uVXBkYXRlKCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuZm9yY2VfcmVkcmF3ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB0aGlzLmNhbGModHJ1ZSk7XG5cbiAgICAgICAgICAgICAgICAvLyBjYWxsYmFja3MgY2FsbGVkXG4gICAgICAgICAgICAgICAgdGhpcy5jYWxsT25TdGFydCgpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVNjZW5lKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEFwcGVuZHMgc2xpZGVyIHRlbXBsYXRlIHRvIGEgRE9NXG4gICAgICAgICAqL1xuICAgICAgICBhcHBlbmQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBjb250YWluZXJfaHRtbCA9ICc8c3BhbiBjbGFzcz1cImlycyBpcnMtLScgKyB0aGlzLm9wdGlvbnMuc2tpbiArICcganMtaXJzLScgKyB0aGlzLnBsdWdpbl9jb3VudCArICcgJyArIHRoaXMub3B0aW9ucy5leHRyYV9jbGFzc2VzICsgJ1wiPjwvc3Bhbj4nO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUuaW5wdXQuYmVmb3JlKGNvbnRhaW5lcl9odG1sKTtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LnByb3AoXCJyZWFkb25seVwiLCB0cnVlKTtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmNvbnQgPSB0aGlzLiRjYWNoZS5pbnB1dC5wcmV2KCk7XG4gICAgICAgICAgICB0aGlzLnJlc3VsdC5zbGlkZXIgPSB0aGlzLiRjYWNoZS5jb250O1xuXG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5jb250Lmh0bWwoYmFzZV9odG1sKTtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLnJzID0gdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLmlyc1wiKTtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLm1pbiA9IHRoaXMuJGNhY2hlLmNvbnQuZmluZChcIi5pcnMtbWluXCIpO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUubWF4ID0gdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLmlycy1tYXhcIik7XG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5mcm9tID0gdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLmlycy1mcm9tXCIpO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUudG8gPSB0aGlzLiRjYWNoZS5jb250LmZpbmQoXCIuaXJzLXRvXCIpO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUuc2luZ2xlID0gdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLmlycy1zaW5nbGVcIik7XG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5saW5lID0gdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLmlycy1saW5lXCIpO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUuZ3JpZCA9IHRoaXMuJGNhY2hlLmNvbnQuZmluZChcIi5pcnMtZ3JpZFwiKTtcblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy50eXBlID09PSBcInNpbmdsZVwiKSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuY29udC5hcHBlbmQoc2luZ2xlX2h0bWwpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmJhciA9IHRoaXMuJGNhY2hlLmNvbnQuZmluZChcIi5pcnMtYmFyXCIpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmVkZ2UgPSB0aGlzLiRjYWNoZS5jb250LmZpbmQoXCIuaXJzLWJhci1lZGdlXCIpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNfc2luZ2xlID0gdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLnNpbmdsZVwiKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5mcm9tWzBdLnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnRvWzBdLnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNoYWRfc2luZ2xlID0gdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLnNoYWRvdy1zaW5nbGVcIik7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmNvbnQuYXBwZW5kKGRvdWJsZV9odG1sKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5iYXIgPSB0aGlzLiRjYWNoZS5jb250LmZpbmQoXCIuaXJzLWJhclwiKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX2Zyb20gPSB0aGlzLiRjYWNoZS5jb250LmZpbmQoXCIuZnJvbVwiKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX3RvID0gdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLnRvXCIpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNoYWRfZnJvbSA9IHRoaXMuJGNhY2hlLmNvbnQuZmluZChcIi5zaGFkb3ctZnJvbVwiKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zaGFkX3RvID0gdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLnNoYWRvdy10b1wiKTtcblxuICAgICAgICAgICAgICAgIHRoaXMuc2V0VG9wSGFuZGxlcigpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmhpZGVfZnJvbV90bykge1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmZyb21bMF0uc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnRvWzBdLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zaW5nbGVbMF0uc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmFwcGVuZEdyaWQoKTtcblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5kaXNhYmxlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5hcHBlbmREaXNhYmxlTWFzaygpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0WzBdLmRpc2FibGVkID0gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuaW5wdXRbMF0uZGlzYWJsZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB0aGlzLnJlbW92ZURpc2FibGVNYXNrKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5iaW5kRXZlbnRzKCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIGJsb2NrIG9ubHkgaWYgbm90IGRpc2FibGVkXG4gICAgICAgICAgICBpZiAoIXRoaXMub3B0aW9ucy5kaXNhYmxlKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5ibG9jaykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcGVuZERpc2FibGVNYXNrKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVEaXNhYmxlTWFzaygpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5kcmFnX2ludGVydmFsKSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuYmFyWzBdLnN0eWxlLmN1cnNvciA9IFwiZXctcmVzaXplXCI7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIERldGVybWluZSB3aGljaCBoYW5kbGVyIGhhcyBhIHByaW9yaXR5XG4gICAgICAgICAqIHdvcmtzIG9ubHkgZm9yIGRvdWJsZSBzbGlkZXIgdHlwZVxuICAgICAgICAgKi9cbiAgICAgICAgc2V0VG9wSGFuZGxlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIG1pbiA9IHRoaXMub3B0aW9ucy5taW4sXG4gICAgICAgICAgICAgICAgbWF4ID0gdGhpcy5vcHRpb25zLm1heCxcbiAgICAgICAgICAgICAgICBmcm9tID0gdGhpcy5vcHRpb25zLmZyb20sXG4gICAgICAgICAgICAgICAgdG8gPSB0aGlzLm9wdGlvbnMudG87XG5cbiAgICAgICAgICAgIGlmIChmcm9tID4gbWluICYmIHRvID09PSBtYXgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX2Zyb20uYWRkQ2xhc3MoXCJ0eXBlX2xhc3RcIik7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRvIDwgbWF4KSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc190by5hZGRDbGFzcyhcInR5cGVfbGFzdFwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogRGV0ZXJtaW5lIHdoaWNoIGhhbmRsZXMgd2FzIGNsaWNrZWQgbGFzdFxuICAgICAgICAgKiBhbmQgd2hpY2ggaGFuZGxlciBzaG91bGQgaGF2ZSBob3ZlciBlZmZlY3RcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHRhcmdldCB7U3RyaW5nfVxuICAgICAgICAgKi9cbiAgICAgICAgY2hhbmdlTGV2ZWw6IGZ1bmN0aW9uICh0YXJnZXQpIHtcbiAgICAgICAgICAgIHN3aXRjaCAodGFyZ2V0KSB7XG4gICAgICAgICAgICAgICAgY2FzZSBcInNpbmdsZVwiOlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX2dhcCA9IHRoaXMudG9GaXhlZCh0aGlzLmNvb3Jkcy5wX3BvaW50ZXIgLSB0aGlzLmNvb3Jkcy5wX3NpbmdsZV9mYWtlKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc19zaW5nbGUuYWRkQ2xhc3MoXCJzdGF0ZV9ob3ZlclwiKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSBcImZyb21cIjpcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9nYXAgPSB0aGlzLnRvRml4ZWQodGhpcy5jb29yZHMucF9wb2ludGVyIC0gdGhpcy5jb29yZHMucF9mcm9tX2Zha2UpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX2Zyb20uYWRkQ2xhc3MoXCJzdGF0ZV9ob3ZlclwiKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc19mcm9tLmFkZENsYXNzKFwidHlwZV9sYXN0XCIpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX3RvLnJlbW92ZUNsYXNzKFwidHlwZV9sYXN0XCIpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIFwidG9cIjpcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9nYXAgPSB0aGlzLnRvRml4ZWQodGhpcy5jb29yZHMucF9wb2ludGVyIC0gdGhpcy5jb29yZHMucF90b19mYWtlKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc190by5hZGRDbGFzcyhcInN0YXRlX2hvdmVyXCIpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX3RvLmFkZENsYXNzKFwidHlwZV9sYXN0XCIpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX2Zyb20ucmVtb3ZlQ2xhc3MoXCJ0eXBlX2xhc3RcIik7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgXCJib3RoXCI6XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfZ2FwX2xlZnQgPSB0aGlzLnRvRml4ZWQodGhpcy5jb29yZHMucF9wb2ludGVyIC0gdGhpcy5jb29yZHMucF9mcm9tX2Zha2UpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX2dhcF9yaWdodCA9IHRoaXMudG9GaXhlZCh0aGlzLmNvb3Jkcy5wX3RvX2Zha2UgLSB0aGlzLmNvb3Jkcy5wX3BvaW50ZXIpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX3RvLnJlbW92ZUNsYXNzKFwidHlwZV9sYXN0XCIpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX2Zyb20ucmVtb3ZlQ2xhc3MoXCJ0eXBlX2xhc3RcIik7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBUaGVuIHNsaWRlciBpcyBkaXNhYmxlZFxuICAgICAgICAgKiBhcHBlbmRzIGV4dHJhIGxheWVyIHdpdGggb3BhY2l0eVxuICAgICAgICAgKi9cbiAgICAgICAgYXBwZW5kRGlzYWJsZU1hc2s6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmNvbnQuYXBwZW5kKGRpc2FibGVfaHRtbCk7XG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5jb250LmFkZENsYXNzKFwiaXJzLWRpc2FibGVkXCIpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBUaGVuIHNsaWRlciBpcyBub3QgZGlzYWJsZWRcbiAgICAgICAgICogcmVtb3ZlIGRpc2FibGUgbWFza1xuICAgICAgICAgKi9cbiAgICAgICAgcmVtb3ZlRGlzYWJsZU1hc2s6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmNvbnQucmVtb3ZlKFwiLmlycy1kaXNhYmxlLW1hc2tcIik7XG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5jb250LnJlbW92ZUNsYXNzKFwiaXJzLWRpc2FibGVkXCIpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZW1vdmUgc2xpZGVyIGluc3RhbmNlXG4gICAgICAgICAqIGFuZCB1bmJpbmQgYWxsIGV2ZW50c1xuICAgICAgICAgKi9cbiAgICAgICAgcmVtb3ZlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5jb250LnJlbW92ZSgpO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUuY29udCA9IG51bGw7XG5cbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmxpbmUub2ZmKFwia2V5ZG93bi5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCk7XG5cbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmJvZHkub2ZmKFwidG91Y2htb3ZlLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50KTtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmJvZHkub2ZmKFwibW91c2Vtb3ZlLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50KTtcblxuICAgICAgICAgICAgdGhpcy4kY2FjaGUud2luLm9mZihcInRvdWNoZW5kLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50KTtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLndpbi5vZmYoXCJtb3VzZXVwLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50KTtcblxuICAgICAgICAgICAgaWYgKGlzX29sZF9pZSkge1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmJvZHkub2ZmKFwibW91c2V1cC5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCk7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuYm9keS5vZmYoXCJtb3VzZWxlYXZlLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy4kY2FjaGUuZ3JpZF9sYWJlbHMgPSBbXTtcbiAgICAgICAgICAgIHRoaXMuY29vcmRzLmJpZyA9IFtdO1xuICAgICAgICAgICAgdGhpcy5jb29yZHMuYmlnX3cgPSBbXTtcbiAgICAgICAgICAgIHRoaXMuY29vcmRzLmJpZ19wID0gW107XG4gICAgICAgICAgICB0aGlzLmNvb3Jkcy5iaWdfeCA9IFtdO1xuXG4gICAgICAgICAgICBjYW5jZWxBbmltYXRpb25GcmFtZSh0aGlzLnJhZl9pZCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIGJpbmQgYWxsIHNsaWRlciBldmVudHNcbiAgICAgICAgICovXG4gICAgICAgIGJpbmRFdmVudHM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLm5vX2RpYXBhc29uKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5ib2R5Lm9uKFwidG91Y2htb3ZlLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJNb3ZlLmJpbmQodGhpcykpO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUuYm9keS5vbihcIm1vdXNlbW92ZS5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyTW92ZS5iaW5kKHRoaXMpKTtcblxuICAgICAgICAgICAgdGhpcy4kY2FjaGUud2luLm9uKFwidG91Y2hlbmQuaXJzX1wiICsgdGhpcy5wbHVnaW5fY291bnQsIHRoaXMucG9pbnRlclVwLmJpbmQodGhpcykpO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUud2luLm9uKFwibW91c2V1cC5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyVXAuYmluZCh0aGlzKSk7XG5cbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmxpbmUub24oXCJ0b3VjaHN0YXJ0Lmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJDbGljay5iaW5kKHRoaXMsIFwiY2xpY2tcIikpO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUubGluZS5vbihcIm1vdXNlZG93bi5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyQ2xpY2suYmluZCh0aGlzLCBcImNsaWNrXCIpKTtcblxuICAgICAgICAgICAgdGhpcy4kY2FjaGUubGluZS5vbihcImZvY3VzLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJGb2N1cy5iaW5kKHRoaXMpKTtcblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5kcmFnX2ludGVydmFsICYmIHRoaXMub3B0aW9ucy50eXBlID09PSBcImRvdWJsZVwiKSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuYmFyLm9uKFwidG91Y2hzdGFydC5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyRG93bi5iaW5kKHRoaXMsIFwiYm90aFwiKSk7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuYmFyLm9uKFwibW91c2Vkb3duLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJEb3duLmJpbmQodGhpcywgXCJib3RoXCIpKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuYmFyLm9uKFwidG91Y2hzdGFydC5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyQ2xpY2suYmluZCh0aGlzLCBcImNsaWNrXCIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5iYXIub24oXCJtb3VzZWRvd24uaXJzX1wiICsgdGhpcy5wbHVnaW5fY291bnQsIHRoaXMucG9pbnRlckNsaWNrLmJpbmQodGhpcywgXCJjbGlja1wiKSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMudHlwZSA9PT0gXCJzaW5nbGVcIikge1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNpbmdsZS5vbihcInRvdWNoc3RhcnQuaXJzX1wiICsgdGhpcy5wbHVnaW5fY291bnQsIHRoaXMucG9pbnRlckRvd24uYmluZCh0aGlzLCBcInNpbmdsZVwiKSk7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc19zaW5nbGUub24oXCJ0b3VjaHN0YXJ0Lmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJEb3duLmJpbmQodGhpcywgXCJzaW5nbGVcIikpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNoYWRfc2luZ2xlLm9uKFwidG91Y2hzdGFydC5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyQ2xpY2suYmluZCh0aGlzLCBcImNsaWNrXCIpKTtcblxuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNpbmdsZS5vbihcIm1vdXNlZG93bi5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyRG93bi5iaW5kKHRoaXMsIFwic2luZ2xlXCIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX3NpbmdsZS5vbihcIm1vdXNlZG93bi5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyRG93bi5iaW5kKHRoaXMsIFwic2luZ2xlXCIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5lZGdlLm9uKFwibW91c2Vkb3duLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJDbGljay5iaW5kKHRoaXMsIFwiY2xpY2tcIikpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNoYWRfc2luZ2xlLm9uKFwibW91c2Vkb3duLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJDbGljay5iaW5kKHRoaXMsIFwiY2xpY2tcIikpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zaW5nbGUub24oXCJ0b3VjaHN0YXJ0Lmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJEb3duLmJpbmQodGhpcywgbnVsbCkpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNpbmdsZS5vbihcIm1vdXNlZG93bi5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyRG93bi5iaW5kKHRoaXMsIG51bGwpKTtcblxuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmZyb20ub24oXCJ0b3VjaHN0YXJ0Lmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJEb3duLmJpbmQodGhpcywgXCJmcm9tXCIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX2Zyb20ub24oXCJ0b3VjaHN0YXJ0Lmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJEb3duLmJpbmQodGhpcywgXCJmcm9tXCIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS50by5vbihcInRvdWNoc3RhcnQuaXJzX1wiICsgdGhpcy5wbHVnaW5fY291bnQsIHRoaXMucG9pbnRlckRvd24uYmluZCh0aGlzLCBcInRvXCIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX3RvLm9uKFwidG91Y2hzdGFydC5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyRG93bi5iaW5kKHRoaXMsIFwidG9cIikpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNoYWRfZnJvbS5vbihcInRvdWNoc3RhcnQuaXJzX1wiICsgdGhpcy5wbHVnaW5fY291bnQsIHRoaXMucG9pbnRlckNsaWNrLmJpbmQodGhpcywgXCJjbGlja1wiKSk7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc2hhZF90by5vbihcInRvdWNoc3RhcnQuaXJzX1wiICsgdGhpcy5wbHVnaW5fY291bnQsIHRoaXMucG9pbnRlckNsaWNrLmJpbmQodGhpcywgXCJjbGlja1wiKSk7XG5cbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5mcm9tLm9uKFwibW91c2Vkb3duLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJEb3duLmJpbmQodGhpcywgXCJmcm9tXCIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX2Zyb20ub24oXCJtb3VzZWRvd24uaXJzX1wiICsgdGhpcy5wbHVnaW5fY291bnQsIHRoaXMucG9pbnRlckRvd24uYmluZCh0aGlzLCBcImZyb21cIikpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnRvLm9uKFwibW91c2Vkb3duLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJEb3duLmJpbmQodGhpcywgXCJ0b1wiKSk7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc190by5vbihcIm1vdXNlZG93bi5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyRG93bi5iaW5kKHRoaXMsIFwidG9cIikpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNoYWRfZnJvbS5vbihcIm1vdXNlZG93bi5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyQ2xpY2suYmluZCh0aGlzLCBcImNsaWNrXCIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zaGFkX3RvLm9uKFwibW91c2Vkb3duLmlyc19cIiArIHRoaXMucGx1Z2luX2NvdW50LCB0aGlzLnBvaW50ZXJDbGljay5iaW5kKHRoaXMsIFwiY2xpY2tcIikpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmtleWJvYXJkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUubGluZS5vbihcImtleWRvd24uaXJzX1wiICsgdGhpcy5wbHVnaW5fY291bnQsIHRoaXMua2V5LmJpbmQodGhpcywgXCJrZXlib2FyZFwiKSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChpc19vbGRfaWUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5ib2R5Lm9uKFwibW91c2V1cC5pcnNfXCIgKyB0aGlzLnBsdWdpbl9jb3VudCwgdGhpcy5wb2ludGVyVXAuYmluZCh0aGlzKSk7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuYm9keS5vbihcIm1vdXNlbGVhdmUuaXJzX1wiICsgdGhpcy5wbHVnaW5fY291bnQsIHRoaXMucG9pbnRlclVwLmJpbmQodGhpcykpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBGb2N1cyB3aXRoIHRhYkluZGV4XG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSBlIHtPYmplY3R9IGV2ZW50IG9iamVjdFxuICAgICAgICAgKi9cbiAgICAgICAgcG9pbnRlckZvY3VzOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgaWYgKCF0aGlzLnRhcmdldCkge1xuICAgICAgICAgICAgICAgIHZhciB4O1xuICAgICAgICAgICAgICAgIHZhciAkaGFuZGxlO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy50eXBlID09PSBcInNpbmdsZVwiKSB7XG4gICAgICAgICAgICAgICAgICAgICRoYW5kbGUgPSB0aGlzLiRjYWNoZS5zaW5nbGU7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgJGhhbmRsZSA9IHRoaXMuJGNhY2hlLmZyb207XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgeCA9ICRoYW5kbGUub2Zmc2V0KCkubGVmdDtcbiAgICAgICAgICAgICAgICB4ICs9ICgkaGFuZGxlLndpZHRoKCkgLyAyKSAtIDE7XG5cbiAgICAgICAgICAgICAgICB0aGlzLnBvaW50ZXJDbGljayhcInNpbmdsZVwiLCB7cHJldmVudERlZmF1bHQ6IGZ1bmN0aW9uICgpIHt9LCBwYWdlWDogeH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBNb3VzZW1vdmUgb3IgdG91Y2htb3ZlXG4gICAgICAgICAqIG9ubHkgZm9yIGhhbmRsZXJzXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSBlIHtPYmplY3R9IGV2ZW50IG9iamVjdFxuICAgICAgICAgKi9cbiAgICAgICAgcG9pbnRlck1vdmU6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMuZHJhZ2dpbmcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHZhciB4ID0gZS5wYWdlWCB8fCBlLm9yaWdpbmFsRXZlbnQudG91Y2hlcyAmJiBlLm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXS5wYWdlWDtcbiAgICAgICAgICAgIHRoaXMuY29vcmRzLnhfcG9pbnRlciA9IHggLSB0aGlzLmNvb3Jkcy54X2dhcDtcblxuICAgICAgICAgICAgdGhpcy5jYWxjKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIE1vdXNldXAgb3IgdG91Y2hlbmRcbiAgICAgICAgICogb25seSBmb3IgaGFuZGxlcnNcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIGUge09iamVjdH0gZXZlbnQgb2JqZWN0XG4gICAgICAgICAqL1xuICAgICAgICBwb2ludGVyVXA6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50X3BsdWdpbiAhPT0gdGhpcy5wbHVnaW5fY291bnQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmlzX2FjdGl2ZSkge1xuICAgICAgICAgICAgICAgIHRoaXMuaXNfYWN0aXZlID0gZmFsc2U7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy4kY2FjaGUuY29udC5maW5kKFwiLnN0YXRlX2hvdmVyXCIpLnJlbW92ZUNsYXNzKFwic3RhdGVfaG92ZXJcIik7XG5cbiAgICAgICAgICAgIHRoaXMuZm9yY2VfcmVkcmF3ID0gdHJ1ZTtcblxuICAgICAgICAgICAgaWYgKGlzX29sZF9pZSkge1xuICAgICAgICAgICAgICAgICQoXCIqXCIpLnByb3AoXCJ1bnNlbGVjdGFibGVcIiwgZmFsc2UpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVNjZW5lKCk7XG4gICAgICAgICAgICB0aGlzLnJlc3RvcmVPcmlnaW5hbE1pbkludGVydmFsKCk7XG5cbiAgICAgICAgICAgIC8vIGNhbGxiYWNrcyBjYWxsXG4gICAgICAgICAgICBpZiAoJC5jb250YWlucyh0aGlzLiRjYWNoZS5jb250WzBdLCBlLnRhcmdldCkgfHwgdGhpcy5kcmFnZ2luZykge1xuICAgICAgICAgICAgICAgIHRoaXMuY2FsbE9uRmluaXNoKCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuZHJhZ2dpbmcgPSBmYWxzZTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogTW91c2Vkb3duIG9yIHRvdWNoc3RhcnRcbiAgICAgICAgICogb25seSBmb3IgaGFuZGxlcnNcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHRhcmdldCB7U3RyaW5nfG51bGx9XG4gICAgICAgICAqIEBwYXJhbSBlIHtPYmplY3R9IGV2ZW50IG9iamVjdFxuICAgICAgICAgKi9cbiAgICAgICAgcG9pbnRlckRvd246IGZ1bmN0aW9uICh0YXJnZXQsIGUpIHtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHZhciB4ID0gZS5wYWdlWCB8fCBlLm9yaWdpbmFsRXZlbnQudG91Y2hlcyAmJiBlLm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXS5wYWdlWDtcbiAgICAgICAgICAgIGlmIChlLmJ1dHRvbiA9PT0gMikge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRhcmdldCA9PT0gXCJib3RoXCIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFRlbXBNaW5JbnRlcnZhbCgpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIXRhcmdldCkge1xuICAgICAgICAgICAgICAgIHRhcmdldCA9IHRoaXMudGFyZ2V0IHx8IFwiZnJvbVwiO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRfcGx1Z2luID0gdGhpcy5wbHVnaW5fY291bnQ7XG4gICAgICAgICAgICB0aGlzLnRhcmdldCA9IHRhcmdldDtcblxuICAgICAgICAgICAgdGhpcy5pc19hY3RpdmUgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5kcmFnZ2luZyA9IHRydWU7XG5cbiAgICAgICAgICAgIHRoaXMuY29vcmRzLnhfZ2FwID0gdGhpcy4kY2FjaGUucnMub2Zmc2V0KCkubGVmdDtcbiAgICAgICAgICAgIHRoaXMuY29vcmRzLnhfcG9pbnRlciA9IHggLSB0aGlzLmNvb3Jkcy54X2dhcDtcblxuICAgICAgICAgICAgdGhpcy5jYWxjUG9pbnRlclBlcmNlbnQoKTtcbiAgICAgICAgICAgIHRoaXMuY2hhbmdlTGV2ZWwodGFyZ2V0KTtcblxuICAgICAgICAgICAgaWYgKGlzX29sZF9pZSkge1xuICAgICAgICAgICAgICAgICQoXCIqXCIpLnByb3AoXCJ1bnNlbGVjdGFibGVcIiwgdHJ1ZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmxpbmUudHJpZ2dlcihcImZvY3VzXCIpO1xuXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVNjZW5lKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIE1vdXNlZG93biBvciB0b3VjaHN0YXJ0XG4gICAgICAgICAqIGZvciBvdGhlciBzbGlkZXIgZWxlbWVudHMsIGxpa2UgZGlhcGFzb24gbGluZVxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0gdGFyZ2V0IHtTdHJpbmd9XG4gICAgICAgICAqIEBwYXJhbSBlIHtPYmplY3R9IGV2ZW50IG9iamVjdFxuICAgICAgICAgKi9cbiAgICAgICAgcG9pbnRlckNsaWNrOiBmdW5jdGlvbiAodGFyZ2V0LCBlKSB7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB2YXIgeCA9IGUucGFnZVggfHwgZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXMgJiYgZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF0ucGFnZVg7XG4gICAgICAgICAgICBpZiAoZS5idXR0b24gPT09IDIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuY3VycmVudF9wbHVnaW4gPSB0aGlzLnBsdWdpbl9jb3VudDtcbiAgICAgICAgICAgIHRoaXMudGFyZ2V0ID0gdGFyZ2V0O1xuXG4gICAgICAgICAgICB0aGlzLmlzX2NsaWNrID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuY29vcmRzLnhfZ2FwID0gdGhpcy4kY2FjaGUucnMub2Zmc2V0KCkubGVmdDtcbiAgICAgICAgICAgIHRoaXMuY29vcmRzLnhfcG9pbnRlciA9ICsoeCAtIHRoaXMuY29vcmRzLnhfZ2FwKS50b0ZpeGVkKCk7XG5cbiAgICAgICAgICAgIHRoaXMuZm9yY2VfcmVkcmF3ID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuY2FsYygpO1xuXG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5saW5lLnRyaWdnZXIoXCJmb2N1c1wiKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogS2V5Ym9yYXJkIGNvbnRyb2xzIGZvciBmb2N1c2VkIHNsaWRlclxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0gdGFyZ2V0IHtTdHJpbmd9XG4gICAgICAgICAqIEBwYXJhbSBlIHtPYmplY3R9IGV2ZW50IG9iamVjdFxuICAgICAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbnx1bmRlZmluZWR9XG4gICAgICAgICAqL1xuICAgICAgICBrZXk6IGZ1bmN0aW9uICh0YXJnZXQsIGUpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRfcGx1Z2luICE9PSB0aGlzLnBsdWdpbl9jb3VudCB8fCBlLmFsdEtleSB8fCBlLmN0cmxLZXkgfHwgZS5zaGlmdEtleSB8fCBlLm1ldGFLZXkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHN3aXRjaCAoZS53aGljaCkge1xuICAgICAgICAgICAgICAgIGNhc2UgODM6IC8vIFdcbiAgICAgICAgICAgICAgICBjYXNlIDY1OiAvLyBBXG4gICAgICAgICAgICAgICAgY2FzZSA0MDogLy8gRE9XTlxuICAgICAgICAgICAgICAgIGNhc2UgMzc6IC8vIExFRlRcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVCeUtleShmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICAgICAgY2FzZSA4NzogLy8gU1xuICAgICAgICAgICAgICAgIGNhc2UgNjg6IC8vIERcbiAgICAgICAgICAgICAgICBjYXNlIDM4OiAvLyBVUFxuICAgICAgICAgICAgICAgIGNhc2UgMzk6IC8vIFJJR0hUXG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tb3ZlQnlLZXkodHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogTW92ZSBieSBrZXlcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHJpZ2h0IHtib29sZWFufSBkaXJlY3Rpb24gdG8gbW92ZVxuICAgICAgICAgKi9cbiAgICAgICAgbW92ZUJ5S2V5OiBmdW5jdGlvbiAocmlnaHQpIHtcbiAgICAgICAgICAgIHZhciBwID0gdGhpcy5jb29yZHMucF9wb2ludGVyO1xuICAgICAgICAgICAgdmFyIHBfc3RlcCA9ICh0aGlzLm9wdGlvbnMubWF4IC0gdGhpcy5vcHRpb25zLm1pbikgLyAxMDA7XG4gICAgICAgICAgICBwX3N0ZXAgPSB0aGlzLm9wdGlvbnMuc3RlcCAvIHBfc3RlcDtcblxuICAgICAgICAgICAgaWYgKHJpZ2h0KSB7XG4gICAgICAgICAgICAgICAgcCArPSBwX3N0ZXA7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHAgLT0gcF9zdGVwO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmNvb3Jkcy54X3BvaW50ZXIgPSB0aGlzLnRvRml4ZWQodGhpcy5jb29yZHMud19ycyAvIDEwMCAqIHApO1xuICAgICAgICAgICAgdGhpcy5pc19rZXkgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5jYWxjKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNldCB2aXNpYmlsaXR5IGFuZCBjb250ZW50XG4gICAgICAgICAqIG9mIE1pbiBhbmQgTWF4IGxhYmVsc1xuICAgICAgICAgKi9cbiAgICAgICAgc2V0TWluTWF4OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMub3B0aW9ucykge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5oaWRlX21pbl9tYXgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5taW5bMF0uc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLm1heFswXS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnZhbHVlcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5taW4uaHRtbCh0aGlzLmRlY29yYXRlKHRoaXMub3B0aW9ucy5wX3ZhbHVlc1t0aGlzLm9wdGlvbnMubWluXSkpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLm1heC5odG1sKHRoaXMuZGVjb3JhdGUodGhpcy5vcHRpb25zLnBfdmFsdWVzW3RoaXMub3B0aW9ucy5tYXhdKSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhciBtaW5fcHJldHR5ID0gdGhpcy5fcHJldHRpZnkodGhpcy5vcHRpb25zLm1pbik7XG4gICAgICAgICAgICAgICAgdmFyIG1heF9wcmV0dHkgPSB0aGlzLl9wcmV0dGlmeSh0aGlzLm9wdGlvbnMubWF4KTtcblxuICAgICAgICAgICAgICAgIHRoaXMucmVzdWx0Lm1pbl9wcmV0dHkgPSBtaW5fcHJldHR5O1xuICAgICAgICAgICAgICAgIHRoaXMucmVzdWx0Lm1heF9wcmV0dHkgPSBtYXhfcHJldHR5O1xuXG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUubWluLmh0bWwodGhpcy5kZWNvcmF0ZShtaW5fcHJldHR5LCB0aGlzLm9wdGlvbnMubWluKSk7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUubWF4Lmh0bWwodGhpcy5kZWNvcmF0ZShtYXhfcHJldHR5LCB0aGlzLm9wdGlvbnMubWF4KSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMubGFiZWxzLndfbWluID0gdGhpcy4kY2FjaGUubWluLm91dGVyV2lkdGgoZmFsc2UpO1xuICAgICAgICAgICAgdGhpcy5sYWJlbHMud19tYXggPSB0aGlzLiRjYWNoZS5tYXgub3V0ZXJXaWR0aChmYWxzZSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFRoZW4gZHJhZ2dpbmcgaW50ZXJ2YWwsIHByZXZlbnQgaW50ZXJ2YWwgY29sbGFwc2luZ1xuICAgICAgICAgKiB1c2luZyBtaW5faW50ZXJ2YWwgb3B0aW9uXG4gICAgICAgICAqL1xuICAgICAgICBzZXRUZW1wTWluSW50ZXJ2YWw6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBpbnRlcnZhbCA9IHRoaXMucmVzdWx0LnRvIC0gdGhpcy5yZXN1bHQuZnJvbTtcblxuICAgICAgICAgICAgaWYgKHRoaXMub2xkX21pbl9pbnRlcnZhbCA9PT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHRoaXMub2xkX21pbl9pbnRlcnZhbCA9IHRoaXMub3B0aW9ucy5taW5faW50ZXJ2YWw7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5taW5faW50ZXJ2YWwgPSBpbnRlcnZhbDtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmVzdG9yZSBtaW5faW50ZXJ2YWwgb3B0aW9uIHRvIG9yaWdpbmFsXG4gICAgICAgICAqL1xuICAgICAgICByZXN0b3JlT3JpZ2luYWxNaW5JbnRlcnZhbDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYgKHRoaXMub2xkX21pbl9pbnRlcnZhbCAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucy5taW5faW50ZXJ2YWwgPSB0aGlzLm9sZF9taW5faW50ZXJ2YWw7XG4gICAgICAgICAgICAgICAgdGhpcy5vbGRfbWluX2ludGVydmFsID0gbnVsbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuXG5cbiAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICAgICAgICAvLyBDYWxjdWxhdGlvbnNcblxuICAgICAgICAvKipcbiAgICAgICAgICogQWxsIGNhbGN1bGF0aW9ucyBhbmQgbWVhc3VyZXMgc3RhcnQgaGVyZVxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0gdXBkYXRlIHtib29sZWFuPX1cbiAgICAgICAgICovXG4gICAgICAgIGNhbGM6IGZ1bmN0aW9uICh1cGRhdGUpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5vcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmNhbGNfY291bnQrKztcblxuICAgICAgICAgICAgaWYgKHRoaXMuY2FsY19jb3VudCA9PT0gMTAgfHwgdXBkYXRlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jYWxjX2NvdW50ID0gMDtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy53X3JzID0gdGhpcy4kY2FjaGUucnMub3V0ZXJXaWR0aChmYWxzZSk7XG5cbiAgICAgICAgICAgICAgICB0aGlzLmNhbGNIYW5kbGVQZXJjZW50KCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICghdGhpcy5jb29yZHMud19ycykge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5jYWxjUG9pbnRlclBlcmNlbnQoKTtcbiAgICAgICAgICAgIHZhciBoYW5kbGVfeCA9IHRoaXMuZ2V0SGFuZGxlWCgpO1xuXG5cbiAgICAgICAgICAgIGlmICh0aGlzLnRhcmdldCA9PT0gXCJib3RoXCIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX2dhcCA9IDA7XG4gICAgICAgICAgICAgICAgaGFuZGxlX3ggPSB0aGlzLmdldEhhbmRsZVgoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMudGFyZ2V0ID09PSBcImNsaWNrXCIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX2dhcCA9IHRoaXMuY29vcmRzLnBfaGFuZGxlIC8gMjtcbiAgICAgICAgICAgICAgICBoYW5kbGVfeCA9IHRoaXMuZ2V0SGFuZGxlWCgpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5kcmFnX2ludGVydmFsKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFyZ2V0ID0gXCJib3RoX29uZVwiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFyZ2V0ID0gdGhpcy5jaG9vc2VIYW5kbGUoaGFuZGxlX3gpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgc3dpdGNoICh0aGlzLnRhcmdldCkge1xuICAgICAgICAgICAgICAgIGNhc2UgXCJiYXNlXCI6XG4gICAgICAgICAgICAgICAgICAgIHZhciB3ID0gKHRoaXMub3B0aW9ucy5tYXggLSB0aGlzLm9wdGlvbnMubWluKSAvIDEwMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGYgPSAodGhpcy5yZXN1bHQuZnJvbSAtIHRoaXMub3B0aW9ucy5taW4pIC8gdyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHQgPSAodGhpcy5yZXN1bHQudG8gLSB0aGlzLm9wdGlvbnMubWluKSAvIHc7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9zaW5nbGVfcmVhbCA9IHRoaXMudG9GaXhlZChmKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgPSB0aGlzLnRvRml4ZWQoZik7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfdG9fcmVhbCA9IHRoaXMudG9GaXhlZCh0KTtcblxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX3NpbmdsZV9yZWFsID0gdGhpcy5jaGVja0RpYXBhc29uKHRoaXMuY29vcmRzLnBfc2luZ2xlX3JlYWwsIHRoaXMub3B0aW9ucy5mcm9tX21pbiwgdGhpcy5vcHRpb25zLmZyb21fbWF4KTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgPSB0aGlzLmNoZWNrRGlhcGFzb24odGhpcy5jb29yZHMucF9mcm9tX3JlYWwsIHRoaXMub3B0aW9ucy5mcm9tX21pbiwgdGhpcy5vcHRpb25zLmZyb21fbWF4KTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF90b19yZWFsID0gdGhpcy5jaGVja0RpYXBhc29uKHRoaXMuY29vcmRzLnBfdG9fcmVhbCwgdGhpcy5vcHRpb25zLnRvX21pbiwgdGhpcy5vcHRpb25zLnRvX21heCk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9zaW5nbGVfZmFrZSA9IHRoaXMuY29udmVydFRvRmFrZVBlcmNlbnQodGhpcy5jb29yZHMucF9zaW5nbGVfcmVhbCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfZnJvbV9mYWtlID0gdGhpcy5jb252ZXJ0VG9GYWtlUGVyY2VudCh0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfdG9fZmFrZSA9IHRoaXMuY29udmVydFRvRmFrZVBlcmNlbnQodGhpcy5jb29yZHMucF90b19yZWFsKTtcblxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhcmdldCA9IG51bGw7XG5cbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBjYXNlIFwic2luZ2xlXCI6XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuZnJvbV9maXhlZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX3NpbmdsZV9yZWFsID0gdGhpcy5jb252ZXJ0VG9SZWFsUGVyY2VudChoYW5kbGVfeCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfc2luZ2xlX3JlYWwgPSB0aGlzLmNhbGNXaXRoU3RlcCh0aGlzLmNvb3Jkcy5wX3NpbmdsZV9yZWFsKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9zaW5nbGVfcmVhbCA9IHRoaXMuY2hlY2tEaWFwYXNvbih0aGlzLmNvb3Jkcy5wX3NpbmdsZV9yZWFsLCB0aGlzLm9wdGlvbnMuZnJvbV9taW4sIHRoaXMub3B0aW9ucy5mcm9tX21heCk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9zaW5nbGVfZmFrZSA9IHRoaXMuY29udmVydFRvRmFrZVBlcmNlbnQodGhpcy5jb29yZHMucF9zaW5nbGVfcmVhbCk7XG5cbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBjYXNlIFwiZnJvbVwiOlxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmZyb21fZml4ZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgPSB0aGlzLmNvbnZlcnRUb1JlYWxQZXJjZW50KGhhbmRsZV94KTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgPSB0aGlzLmNhbGNXaXRoU3RlcCh0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCk7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCA+IHRoaXMuY29vcmRzLnBfdG9fcmVhbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgPSB0aGlzLmNvb3Jkcy5wX3RvX3JlYWw7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgPSB0aGlzLmNoZWNrRGlhcGFzb24odGhpcy5jb29yZHMucF9mcm9tX3JlYWwsIHRoaXMub3B0aW9ucy5mcm9tX21pbiwgdGhpcy5vcHRpb25zLmZyb21fbWF4KTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgPSB0aGlzLmNoZWNrTWluSW50ZXJ2YWwodGhpcy5jb29yZHMucF9mcm9tX3JlYWwsIHRoaXMuY29vcmRzLnBfdG9fcmVhbCwgXCJmcm9tXCIpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCA9IHRoaXMuY2hlY2tNYXhJbnRlcnZhbCh0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCwgdGhpcy5jb29yZHMucF90b19yZWFsLCBcImZyb21cIik7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX2Zha2UgPSB0aGlzLmNvbnZlcnRUb0Zha2VQZXJjZW50KHRoaXMuY29vcmRzLnBfZnJvbV9yZWFsKTtcblxuICAgICAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgICAgIGNhc2UgXCJ0b1wiOlxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnRvX2ZpeGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfdG9fcmVhbCA9IHRoaXMuY29udmVydFRvUmVhbFBlcmNlbnQoaGFuZGxlX3gpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX3RvX3JlYWwgPSB0aGlzLmNhbGNXaXRoU3RlcCh0aGlzLmNvb3Jkcy5wX3RvX3JlYWwpO1xuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5jb29yZHMucF90b19yZWFsIDwgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfdG9fcmVhbCA9IHRoaXMuY29vcmRzLnBfZnJvbV9yZWFsO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfdG9fcmVhbCA9IHRoaXMuY2hlY2tEaWFwYXNvbih0aGlzLmNvb3Jkcy5wX3RvX3JlYWwsIHRoaXMub3B0aW9ucy50b19taW4sIHRoaXMub3B0aW9ucy50b19tYXgpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX3RvX3JlYWwgPSB0aGlzLmNoZWNrTWluSW50ZXJ2YWwodGhpcy5jb29yZHMucF90b19yZWFsLCB0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCwgXCJ0b1wiKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF90b19yZWFsID0gdGhpcy5jaGVja01heEludGVydmFsKHRoaXMuY29vcmRzLnBfdG9fcmVhbCwgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwsIFwidG9cIik7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF90b19mYWtlID0gdGhpcy5jb252ZXJ0VG9GYWtlUGVyY2VudCh0aGlzLmNvb3Jkcy5wX3RvX3JlYWwpO1xuXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICAgICAgY2FzZSBcImJvdGhcIjpcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5mcm9tX2ZpeGVkIHx8IHRoaXMub3B0aW9ucy50b19maXhlZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVfeCA9IHRoaXMudG9GaXhlZChoYW5kbGVfeCArICh0aGlzLmNvb3Jkcy5wX2hhbmRsZSAqIDAuMDAxKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgPSB0aGlzLmNvbnZlcnRUb1JlYWxQZXJjZW50KGhhbmRsZV94KSAtIHRoaXMuY29vcmRzLnBfZ2FwX2xlZnQ7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfZnJvbV9yZWFsID0gdGhpcy5jYWxjV2l0aFN0ZXAodGhpcy5jb29yZHMucF9mcm9tX3JlYWwpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCA9IHRoaXMuY2hlY2tEaWFwYXNvbih0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCwgdGhpcy5vcHRpb25zLmZyb21fbWluLCB0aGlzLm9wdGlvbnMuZnJvbV9tYXgpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCA9IHRoaXMuY2hlY2tNaW5JbnRlcnZhbCh0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCwgdGhpcy5jb29yZHMucF90b19yZWFsLCBcImZyb21cIik7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfZnJvbV9mYWtlID0gdGhpcy5jb252ZXJ0VG9GYWtlUGVyY2VudCh0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF90b19yZWFsID0gdGhpcy5jb252ZXJ0VG9SZWFsUGVyY2VudChoYW5kbGVfeCkgKyB0aGlzLmNvb3Jkcy5wX2dhcF9yaWdodDtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF90b19yZWFsID0gdGhpcy5jYWxjV2l0aFN0ZXAodGhpcy5jb29yZHMucF90b19yZWFsKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF90b19yZWFsID0gdGhpcy5jaGVja0RpYXBhc29uKHRoaXMuY29vcmRzLnBfdG9fcmVhbCwgdGhpcy5vcHRpb25zLnRvX21pbiwgdGhpcy5vcHRpb25zLnRvX21heCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfdG9fcmVhbCA9IHRoaXMuY2hlY2tNaW5JbnRlcnZhbCh0aGlzLmNvb3Jkcy5wX3RvX3JlYWwsIHRoaXMuY29vcmRzLnBfZnJvbV9yZWFsLCBcInRvXCIpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX3RvX2Zha2UgPSB0aGlzLmNvbnZlcnRUb0Zha2VQZXJjZW50KHRoaXMuY29vcmRzLnBfdG9fcmVhbCk7XG5cbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBjYXNlIFwiYm90aF9vbmVcIjpcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5mcm9tX2ZpeGVkIHx8IHRoaXMub3B0aW9ucy50b19maXhlZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICB2YXIgcmVhbF94ID0gdGhpcy5jb252ZXJ0VG9SZWFsUGVyY2VudChoYW5kbGVfeCksXG4gICAgICAgICAgICAgICAgICAgICAgICBmcm9tID0gdGhpcy5yZXN1bHQuZnJvbV9wZXJjZW50LFxuICAgICAgICAgICAgICAgICAgICAgICAgdG8gPSB0aGlzLnJlc3VsdC50b19wZXJjZW50LFxuICAgICAgICAgICAgICAgICAgICAgICAgZnVsbCA9IHRvIC0gZnJvbSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbGYgPSBmdWxsIC8gMixcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld19mcm9tID0gcmVhbF94IC0gaGFsZixcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld190byA9IHJlYWxfeCArIGhhbGY7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKG5ld19mcm9tIDwgMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbmV3X2Zyb20gPSAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgbmV3X3RvID0gbmV3X2Zyb20gKyBmdWxsO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKG5ld190byA+IDEwMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbmV3X3RvID0gMTAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgbmV3X2Zyb20gPSBuZXdfdG8gLSBmdWxsO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgPSB0aGlzLmNhbGNXaXRoU3RlcChuZXdfZnJvbSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfZnJvbV9yZWFsID0gdGhpcy5jaGVja0RpYXBhc29uKHRoaXMuY29vcmRzLnBfZnJvbV9yZWFsLCB0aGlzLm9wdGlvbnMuZnJvbV9taW4sIHRoaXMub3B0aW9ucy5mcm9tX21heCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfZnJvbV9mYWtlID0gdGhpcy5jb252ZXJ0VG9GYWtlUGVyY2VudCh0aGlzLmNvb3Jkcy5wX2Zyb21fcmVhbCk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF90b19yZWFsID0gdGhpcy5jYWxjV2l0aFN0ZXAobmV3X3RvKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF90b19yZWFsID0gdGhpcy5jaGVja0RpYXBhc29uKHRoaXMuY29vcmRzLnBfdG9fcmVhbCwgdGhpcy5vcHRpb25zLnRvX21pbiwgdGhpcy5vcHRpb25zLnRvX21heCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfdG9fZmFrZSA9IHRoaXMuY29udmVydFRvRmFrZVBlcmNlbnQodGhpcy5jb29yZHMucF90b19yZWFsKTtcblxuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy50eXBlID09PSBcInNpbmdsZVwiKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9iYXJfeCA9ICh0aGlzLmNvb3Jkcy5wX2hhbmRsZSAvIDIpO1xuICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfYmFyX3cgPSB0aGlzLmNvb3Jkcy5wX3NpbmdsZV9mYWtlO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5yZXN1bHQuZnJvbV9wZXJjZW50ID0gdGhpcy5jb29yZHMucF9zaW5nbGVfcmVhbDtcbiAgICAgICAgICAgICAgICB0aGlzLnJlc3VsdC5mcm9tID0gdGhpcy5jb252ZXJ0VG9WYWx1ZSh0aGlzLmNvb3Jkcy5wX3NpbmdsZV9yZWFsKTtcbiAgICAgICAgICAgICAgICB0aGlzLnJlc3VsdC5mcm9tX3ByZXR0eSA9IHRoaXMuX3ByZXR0aWZ5KHRoaXMucmVzdWx0LmZyb20pO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy52YWx1ZXMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVzdWx0LmZyb21fdmFsdWUgPSB0aGlzLm9wdGlvbnMudmFsdWVzW3RoaXMucmVzdWx0LmZyb21dO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb29yZHMucF9iYXJfeCA9IHRoaXMudG9GaXhlZCh0aGlzLmNvb3Jkcy5wX2Zyb21fZmFrZSArICh0aGlzLmNvb3Jkcy5wX2hhbmRsZSAvIDIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5wX2Jhcl93ID0gdGhpcy50b0ZpeGVkKHRoaXMuY29vcmRzLnBfdG9fZmFrZSAtIHRoaXMuY29vcmRzLnBfZnJvbV9mYWtlKTtcblxuICAgICAgICAgICAgICAgIHRoaXMucmVzdWx0LmZyb21fcGVyY2VudCA9IHRoaXMuY29vcmRzLnBfZnJvbV9yZWFsO1xuICAgICAgICAgICAgICAgIHRoaXMucmVzdWx0LmZyb20gPSB0aGlzLmNvbnZlcnRUb1ZhbHVlKHRoaXMuY29vcmRzLnBfZnJvbV9yZWFsKTtcbiAgICAgICAgICAgICAgICB0aGlzLnJlc3VsdC5mcm9tX3ByZXR0eSA9IHRoaXMuX3ByZXR0aWZ5KHRoaXMucmVzdWx0LmZyb20pO1xuICAgICAgICAgICAgICAgIHRoaXMucmVzdWx0LnRvX3BlcmNlbnQgPSB0aGlzLmNvb3Jkcy5wX3RvX3JlYWw7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXN1bHQudG8gPSB0aGlzLmNvbnZlcnRUb1ZhbHVlKHRoaXMuY29vcmRzLnBfdG9fcmVhbCk7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXN1bHQudG9fcHJldHR5ID0gdGhpcy5fcHJldHRpZnkodGhpcy5yZXN1bHQudG8pO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy52YWx1ZXMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVzdWx0LmZyb21fdmFsdWUgPSB0aGlzLm9wdGlvbnMudmFsdWVzW3RoaXMucmVzdWx0LmZyb21dO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc3VsdC50b192YWx1ZSA9IHRoaXMub3B0aW9ucy52YWx1ZXNbdGhpcy5yZXN1bHQudG9dO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5jYWxjTWluTWF4KCk7XG4gICAgICAgICAgICB0aGlzLmNhbGNMYWJlbHMoKTtcbiAgICAgICAgfSxcblxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBjYWxjdWxhdGVzIHBvaW50ZXIgWCBpbiBwZXJjZW50XG4gICAgICAgICAqL1xuICAgICAgICBjYWxjUG9pbnRlclBlcmNlbnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5jb29yZHMud19ycykge1xuICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfcG9pbnRlciA9IDA7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5jb29yZHMueF9wb2ludGVyIDwgMCB8fCBpc05hTih0aGlzLmNvb3Jkcy54X3BvaW50ZXIpICApIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy54X3BvaW50ZXIgPSAwO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmNvb3Jkcy54X3BvaW50ZXIgPiB0aGlzLmNvb3Jkcy53X3JzKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb29yZHMueF9wb2ludGVyID0gdGhpcy5jb29yZHMud19ycztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5jb29yZHMucF9wb2ludGVyID0gdGhpcy50b0ZpeGVkKHRoaXMuY29vcmRzLnhfcG9pbnRlciAvIHRoaXMuY29vcmRzLndfcnMgKiAxMDApO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNvbnZlcnRUb1JlYWxQZXJjZW50OiBmdW5jdGlvbiAoZmFrZSkge1xuICAgICAgICAgICAgdmFyIGZ1bGwgPSAxMDAgLSB0aGlzLmNvb3Jkcy5wX2hhbmRsZTtcbiAgICAgICAgICAgIHJldHVybiBmYWtlIC8gZnVsbCAqIDEwMDtcbiAgICAgICAgfSxcblxuICAgICAgICBjb252ZXJ0VG9GYWtlUGVyY2VudDogZnVuY3Rpb24gKHJlYWwpIHtcbiAgICAgICAgICAgIHZhciBmdWxsID0gMTAwIC0gdGhpcy5jb29yZHMucF9oYW5kbGU7XG4gICAgICAgICAgICByZXR1cm4gcmVhbCAvIDEwMCAqIGZ1bGw7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0SGFuZGxlWDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIG1heCA9IDEwMCAtIHRoaXMuY29vcmRzLnBfaGFuZGxlLFxuICAgICAgICAgICAgICAgIHggPSB0aGlzLnRvRml4ZWQodGhpcy5jb29yZHMucF9wb2ludGVyIC0gdGhpcy5jb29yZHMucF9nYXApO1xuXG4gICAgICAgICAgICBpZiAoeCA8IDApIHtcbiAgICAgICAgICAgICAgICB4ID0gMDtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoeCA+IG1heCkge1xuICAgICAgICAgICAgICAgIHggPSBtYXg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiB4O1xuICAgICAgICB9LFxuXG4gICAgICAgIGNhbGNIYW5kbGVQZXJjZW50OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnR5cGUgPT09IFwic2luZ2xlXCIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy53X2hhbmRsZSA9IHRoaXMuJGNhY2hlLnNfc2luZ2xlLm91dGVyV2lkdGgoZmFsc2UpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy53X2hhbmRsZSA9IHRoaXMuJGNhY2hlLnNfZnJvbS5vdXRlcldpZHRoKGZhbHNlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5jb29yZHMucF9oYW5kbGUgPSB0aGlzLnRvRml4ZWQodGhpcy5jb29yZHMud19oYW5kbGUgLyB0aGlzLmNvb3Jkcy53X3JzICogMTAwKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogRmluZCBjbG9zZXN0IGhhbmRsZSB0byBwb2ludGVyIGNsaWNrXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSByZWFsX3gge051bWJlcn1cbiAgICAgICAgICogQHJldHVybnMge1N0cmluZ31cbiAgICAgICAgICovXG4gICAgICAgIGNob29zZUhhbmRsZTogZnVuY3Rpb24gKHJlYWxfeCkge1xuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy50eXBlID09PSBcInNpbmdsZVwiKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFwic2luZ2xlXCI7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhciBtX3BvaW50ID0gdGhpcy5jb29yZHMucF9mcm9tX3JlYWwgKyAoKHRoaXMuY29vcmRzLnBfdG9fcmVhbCAtIHRoaXMuY29vcmRzLnBfZnJvbV9yZWFsKSAvIDIpO1xuICAgICAgICAgICAgICAgIGlmIChyZWFsX3ggPj0gbV9wb2ludCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25zLnRvX2ZpeGVkID8gXCJmcm9tXCIgOiBcInRvXCI7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5mcm9tX2ZpeGVkID8gXCJ0b1wiIDogXCJmcm9tXCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBNZWFzdXJlIE1pbiBhbmQgTWF4IGxhYmVscyB3aWR0aCBpbiBwZXJjZW50XG4gICAgICAgICAqL1xuICAgICAgICBjYWxjTWluTWF4OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMuY29vcmRzLndfcnMpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfbWluID0gdGhpcy5sYWJlbHMud19taW4gLyB0aGlzLmNvb3Jkcy53X3JzICogMTAwO1xuICAgICAgICAgICAgdGhpcy5sYWJlbHMucF9tYXggPSB0aGlzLmxhYmVscy53X21heCAvIHRoaXMuY29vcmRzLndfcnMgKiAxMDA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIE1lYXN1cmUgbGFiZWxzIHdpZHRoIGFuZCBYIGluIHBlcmNlbnRcbiAgICAgICAgICovXG4gICAgICAgIGNhbGNMYWJlbHM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5jb29yZHMud19ycyB8fCB0aGlzLm9wdGlvbnMuaGlkZV9mcm9tX3RvKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnR5cGUgPT09IFwic2luZ2xlXCIpIHtcblxuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLndfc2luZ2xlID0gdGhpcy4kY2FjaGUuc2luZ2xlLm91dGVyV2lkdGgoZmFsc2UpO1xuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfc2luZ2xlX2Zha2UgPSB0aGlzLmxhYmVscy53X3NpbmdsZSAvIHRoaXMuY29vcmRzLndfcnMgKiAxMDA7XG4gICAgICAgICAgICAgICAgdGhpcy5sYWJlbHMucF9zaW5nbGVfbGVmdCA9IHRoaXMuY29vcmRzLnBfc2luZ2xlX2Zha2UgKyAodGhpcy5jb29yZHMucF9oYW5kbGUgLyAyKSAtICh0aGlzLmxhYmVscy5wX3NpbmdsZV9mYWtlIC8gMik7XG4gICAgICAgICAgICAgICAgdGhpcy5sYWJlbHMucF9zaW5nbGVfbGVmdCA9IHRoaXMuY2hlY2tFZGdlcyh0aGlzLmxhYmVscy5wX3NpbmdsZV9sZWZ0LCB0aGlzLmxhYmVscy5wX3NpbmdsZV9mYWtlKTtcblxuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLndfZnJvbSA9IHRoaXMuJGNhY2hlLmZyb20ub3V0ZXJXaWR0aChmYWxzZSk7XG4gICAgICAgICAgICAgICAgdGhpcy5sYWJlbHMucF9mcm9tX2Zha2UgPSB0aGlzLmxhYmVscy53X2Zyb20gLyB0aGlzLmNvb3Jkcy53X3JzICogMTAwO1xuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfZnJvbV9sZWZ0ID0gdGhpcy5jb29yZHMucF9mcm9tX2Zha2UgKyAodGhpcy5jb29yZHMucF9oYW5kbGUgLyAyKSAtICh0aGlzLmxhYmVscy5wX2Zyb21fZmFrZSAvIDIpO1xuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfZnJvbV9sZWZ0ID0gdGhpcy50b0ZpeGVkKHRoaXMubGFiZWxzLnBfZnJvbV9sZWZ0KTtcbiAgICAgICAgICAgICAgICB0aGlzLmxhYmVscy5wX2Zyb21fbGVmdCA9IHRoaXMuY2hlY2tFZGdlcyh0aGlzLmxhYmVscy5wX2Zyb21fbGVmdCwgdGhpcy5sYWJlbHMucF9mcm9tX2Zha2UpO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5sYWJlbHMud190byA9IHRoaXMuJGNhY2hlLnRvLm91dGVyV2lkdGgoZmFsc2UpO1xuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfdG9fZmFrZSA9IHRoaXMubGFiZWxzLndfdG8gLyB0aGlzLmNvb3Jkcy53X3JzICogMTAwO1xuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfdG9fbGVmdCA9IHRoaXMuY29vcmRzLnBfdG9fZmFrZSArICh0aGlzLmNvb3Jkcy5wX2hhbmRsZSAvIDIpIC0gKHRoaXMubGFiZWxzLnBfdG9fZmFrZSAvIDIpO1xuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfdG9fbGVmdCA9IHRoaXMudG9GaXhlZCh0aGlzLmxhYmVscy5wX3RvX2xlZnQpO1xuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfdG9fbGVmdCA9IHRoaXMuY2hlY2tFZGdlcyh0aGlzLmxhYmVscy5wX3RvX2xlZnQsIHRoaXMubGFiZWxzLnBfdG9fZmFrZSk7XG5cbiAgICAgICAgICAgICAgICB0aGlzLmxhYmVscy53X3NpbmdsZSA9IHRoaXMuJGNhY2hlLnNpbmdsZS5vdXRlcldpZHRoKGZhbHNlKTtcbiAgICAgICAgICAgICAgICB0aGlzLmxhYmVscy5wX3NpbmdsZV9mYWtlID0gdGhpcy5sYWJlbHMud19zaW5nbGUgLyB0aGlzLmNvb3Jkcy53X3JzICogMTAwO1xuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfc2luZ2xlX2xlZnQgPSAoKHRoaXMubGFiZWxzLnBfZnJvbV9sZWZ0ICsgdGhpcy5sYWJlbHMucF90b19sZWZ0ICsgdGhpcy5sYWJlbHMucF90b19mYWtlKSAvIDIpIC0gKHRoaXMubGFiZWxzLnBfc2luZ2xlX2Zha2UgLyAyKTtcbiAgICAgICAgICAgICAgICB0aGlzLmxhYmVscy5wX3NpbmdsZV9sZWZ0ID0gdGhpcy50b0ZpeGVkKHRoaXMubGFiZWxzLnBfc2luZ2xlX2xlZnQpO1xuICAgICAgICAgICAgICAgIHRoaXMubGFiZWxzLnBfc2luZ2xlX2xlZnQgPSB0aGlzLmNoZWNrRWRnZXModGhpcy5sYWJlbHMucF9zaW5nbGVfbGVmdCwgdGhpcy5sYWJlbHMucF9zaW5nbGVfZmFrZSk7XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuXG5cbiAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICAgICAgICAvLyBEcmF3aW5nc1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBNYWluIGZ1bmN0aW9uIGNhbGxlZCBpbiByZXF1ZXN0IGFuaW1hdGlvbiBmcmFtZVxuICAgICAgICAgKiB0byB1cGRhdGUgZXZlcnl0aGluZ1xuICAgICAgICAgKi9cbiAgICAgICAgdXBkYXRlU2NlbmU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnJhZl9pZCkge1xuICAgICAgICAgICAgICAgIGNhbmNlbEFuaW1hdGlvbkZyYW1lKHRoaXMucmFmX2lkKTtcbiAgICAgICAgICAgICAgICB0aGlzLnJhZl9pZCA9IG51bGw7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLnVwZGF0ZV90bSk7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZV90bSA9IG51bGw7XG5cbiAgICAgICAgICAgIGlmICghdGhpcy5vcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmRyYXdIYW5kbGVzKCk7XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmlzX2FjdGl2ZSkge1xuICAgICAgICAgICAgICAgIHRoaXMucmFmX2lkID0gcmVxdWVzdEFuaW1hdGlvbkZyYW1lKHRoaXMudXBkYXRlU2NlbmUuYmluZCh0aGlzKSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlX3RtID0gc2V0VGltZW91dCh0aGlzLnVwZGF0ZVNjZW5lLmJpbmQodGhpcyksIDMwMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIERyYXcgaGFuZGxlc1xuICAgICAgICAgKi9cbiAgICAgICAgZHJhd0hhbmRsZXM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMuY29vcmRzLndfcnMgPSB0aGlzLiRjYWNoZS5ycy5vdXRlcldpZHRoKGZhbHNlKTtcblxuICAgICAgICAgICAgaWYgKCF0aGlzLmNvb3Jkcy53X3JzKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5jb29yZHMud19ycyAhPT0gdGhpcy5jb29yZHMud19yc19vbGQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnRhcmdldCA9IFwiYmFzZVwiO1xuICAgICAgICAgICAgICAgIHRoaXMuaXNfcmVzaXplID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMuY29vcmRzLndfcnMgIT09IHRoaXMuY29vcmRzLndfcnNfb2xkIHx8IHRoaXMuZm9yY2VfcmVkcmF3KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRNaW5NYXgoKTtcbiAgICAgICAgICAgICAgICB0aGlzLmNhbGModHJ1ZSk7XG4gICAgICAgICAgICAgICAgdGhpcy5kcmF3TGFiZWxzKCk7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5ncmlkKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2FsY0dyaWRNYXJnaW4oKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jYWxjR3JpZExhYmVscygpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLmZvcmNlX3JlZHJhdyA9IHRydWU7XG4gICAgICAgICAgICAgICAgdGhpcy5jb29yZHMud19yc19vbGQgPSB0aGlzLmNvb3Jkcy53X3JzO1xuICAgICAgICAgICAgICAgIHRoaXMuZHJhd1NoYWRvdygpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIXRoaXMuY29vcmRzLndfcnMpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICghdGhpcy5kcmFnZ2luZyAmJiAhdGhpcy5mb3JjZV9yZWRyYXcgJiYgIXRoaXMuaXNfa2V5KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vbGRfZnJvbSAhPT0gdGhpcy5yZXN1bHQuZnJvbSB8fCB0aGlzLm9sZF90byAhPT0gdGhpcy5yZXN1bHQudG8gfHwgdGhpcy5mb3JjZV9yZWRyYXcgfHwgdGhpcy5pc19rZXkpIHtcblxuICAgICAgICAgICAgICAgIHRoaXMuZHJhd0xhYmVscygpO1xuXG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuYmFyWzBdLnN0eWxlLmxlZnQgPSB0aGlzLmNvb3Jkcy5wX2Jhcl94ICsgXCIlXCI7XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuYmFyWzBdLnN0eWxlLndpZHRoID0gdGhpcy5jb29yZHMucF9iYXJfdyArIFwiJVwiO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy50eXBlID09PSBcInNpbmdsZVwiKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmJhclswXS5zdHlsZS5sZWZ0ID0gMDtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuYmFyWzBdLnN0eWxlLndpZHRoID0gdGhpcy5jb29yZHMucF9iYXJfdyArIHRoaXMuY29vcmRzLnBfYmFyX3ggKyBcIiVcIjtcblxuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zX3NpbmdsZVswXS5zdHlsZS5sZWZ0ID0gdGhpcy5jb29yZHMucF9zaW5nbGVfZmFrZSArIFwiJVwiO1xuXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNpbmdsZVswXS5zdHlsZS5sZWZ0ID0gdGhpcy5sYWJlbHMucF9zaW5nbGVfbGVmdCArIFwiJVwiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNfZnJvbVswXS5zdHlsZS5sZWZ0ID0gdGhpcy5jb29yZHMucF9mcm9tX2Zha2UgKyBcIiVcIjtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc190b1swXS5zdHlsZS5sZWZ0ID0gdGhpcy5jb29yZHMucF90b19mYWtlICsgXCIlXCI7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMub2xkX2Zyb20gIT09IHRoaXMucmVzdWx0LmZyb20gfHwgdGhpcy5mb3JjZV9yZWRyYXcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmZyb21bMF0uc3R5bGUubGVmdCA9IHRoaXMubGFiZWxzLnBfZnJvbV9sZWZ0ICsgXCIlXCI7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMub2xkX3RvICE9PSB0aGlzLnJlc3VsdC50byB8fCB0aGlzLmZvcmNlX3JlZHJhdykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUudG9bMF0uc3R5bGUubGVmdCA9IHRoaXMubGFiZWxzLnBfdG9fbGVmdCArIFwiJVwiO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc2luZ2xlWzBdLnN0eWxlLmxlZnQgPSB0aGlzLmxhYmVscy5wX3NpbmdsZV9sZWZ0ICsgXCIlXCI7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdGhpcy53cml0ZVRvSW5wdXQoKTtcblxuICAgICAgICAgICAgICAgIGlmICgodGhpcy5vbGRfZnJvbSAhPT0gdGhpcy5yZXN1bHQuZnJvbSB8fCB0aGlzLm9sZF90byAhPT0gdGhpcy5yZXN1bHQudG8pICYmICF0aGlzLmlzX3N0YXJ0KSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LnRyaWdnZXIoXCJjaGFuZ2VcIik7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LnRyaWdnZXIoXCJpbnB1dFwiKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0aGlzLm9sZF9mcm9tID0gdGhpcy5yZXN1bHQuZnJvbTtcbiAgICAgICAgICAgICAgICB0aGlzLm9sZF90byA9IHRoaXMucmVzdWx0LnRvO1xuXG4gICAgICAgICAgICAgICAgLy8gY2FsbGJhY2tzIGNhbGxcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuaXNfcmVzaXplICYmICF0aGlzLmlzX3VwZGF0ZSAmJiAhdGhpcy5pc19zdGFydCAmJiAhdGhpcy5pc19maW5pc2gpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jYWxsT25DaGFuZ2UoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNfa2V5IHx8IHRoaXMuaXNfY2xpY2spIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc19rZXkgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc19jbGljayA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbGxPbkZpbmlzaCgpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHRoaXMuaXNfdXBkYXRlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgdGhpcy5pc19yZXNpemUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB0aGlzLmlzX2ZpbmlzaCA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmlzX3N0YXJ0ID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLmlzX2tleSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5pc19jbGljayA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5mb3JjZV9yZWRyYXcgPSBmYWxzZTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogRHJhdyBsYWJlbHNcbiAgICAgICAgICogbWVhc3VyZSBsYWJlbHMgY29sbGlzaW9uc1xuICAgICAgICAgKiBjb2xsYXBzZSBjbG9zZSBsYWJlbHNcbiAgICAgICAgICovXG4gICAgICAgIGRyYXdMYWJlbHM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5vcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgdmFsdWVzX251bSA9IHRoaXMub3B0aW9ucy52YWx1ZXMubGVuZ3RoO1xuICAgICAgICAgICAgdmFyIHBfdmFsdWVzID0gdGhpcy5vcHRpb25zLnBfdmFsdWVzO1xuICAgICAgICAgICAgdmFyIHRleHRfc2luZ2xlO1xuICAgICAgICAgICAgdmFyIHRleHRfZnJvbTtcbiAgICAgICAgICAgIHZhciB0ZXh0X3RvO1xuICAgICAgICAgICAgdmFyIGZyb21fcHJldHR5O1xuICAgICAgICAgICAgdmFyIHRvX3ByZXR0eTtcblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5oaWRlX2Zyb21fdG8pIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMudHlwZSA9PT0gXCJzaW5nbGVcIikge1xuXG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlc19udW0pIHtcbiAgICAgICAgICAgICAgICAgICAgdGV4dF9zaW5nbGUgPSB0aGlzLmRlY29yYXRlKHBfdmFsdWVzW3RoaXMucmVzdWx0LmZyb21dKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc2luZ2xlLmh0bWwodGV4dF9zaW5nbGUpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGZyb21fcHJldHR5ID0gdGhpcy5fcHJldHRpZnkodGhpcy5yZXN1bHQuZnJvbSk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGV4dF9zaW5nbGUgPSB0aGlzLmRlY29yYXRlKGZyb21fcHJldHR5LCB0aGlzLnJlc3VsdC5mcm9tKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc2luZ2xlLmh0bWwodGV4dF9zaW5nbGUpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHRoaXMuY2FsY0xhYmVscygpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubGFiZWxzLnBfc2luZ2xlX2xlZnQgPCB0aGlzLmxhYmVscy5wX21pbiArIDEpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUubWluWzBdLnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLm1pblswXS5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubGFiZWxzLnBfc2luZ2xlX2xlZnQgKyB0aGlzLmxhYmVscy5wX3NpbmdsZV9mYWtlID4gMTAwIC0gdGhpcy5sYWJlbHMucF9tYXggLSAxKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLm1heFswXS5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIjtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5tYXhbMF0uc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIGlmICh2YWx1ZXNfbnVtKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5kZWNvcmF0ZV9ib3RoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0X3NpbmdsZSA9IHRoaXMuZGVjb3JhdGUocF92YWx1ZXNbdGhpcy5yZXN1bHQuZnJvbV0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dF9zaW5nbGUgKz0gdGhpcy5vcHRpb25zLnZhbHVlc19zZXBhcmF0b3I7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0X3NpbmdsZSArPSB0aGlzLmRlY29yYXRlKHBfdmFsdWVzW3RoaXMucmVzdWx0LnRvXSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0X3NpbmdsZSA9IHRoaXMuZGVjb3JhdGUocF92YWx1ZXNbdGhpcy5yZXN1bHQuZnJvbV0gKyB0aGlzLm9wdGlvbnMudmFsdWVzX3NlcGFyYXRvciArIHBfdmFsdWVzW3RoaXMucmVzdWx0LnRvXSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdGV4dF9mcm9tID0gdGhpcy5kZWNvcmF0ZShwX3ZhbHVlc1t0aGlzLnJlc3VsdC5mcm9tXSk7XG4gICAgICAgICAgICAgICAgICAgIHRleHRfdG8gPSB0aGlzLmRlY29yYXRlKHBfdmFsdWVzW3RoaXMucmVzdWx0LnRvXSk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc2luZ2xlLmh0bWwodGV4dF9zaW5nbGUpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5mcm9tLmh0bWwodGV4dF9mcm9tKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUudG8uaHRtbCh0ZXh0X3RvKTtcblxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGZyb21fcHJldHR5ID0gdGhpcy5fcHJldHRpZnkodGhpcy5yZXN1bHQuZnJvbSk7XG4gICAgICAgICAgICAgICAgICAgIHRvX3ByZXR0eSA9IHRoaXMuX3ByZXR0aWZ5KHRoaXMucmVzdWx0LnRvKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmRlY29yYXRlX2JvdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHRfc2luZ2xlID0gdGhpcy5kZWNvcmF0ZShmcm9tX3ByZXR0eSwgdGhpcy5yZXN1bHQuZnJvbSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0X3NpbmdsZSArPSB0aGlzLm9wdGlvbnMudmFsdWVzX3NlcGFyYXRvcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHRfc2luZ2xlICs9IHRoaXMuZGVjb3JhdGUodG9fcHJldHR5LCB0aGlzLnJlc3VsdC50byk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0X3NpbmdsZSA9IHRoaXMuZGVjb3JhdGUoZnJvbV9wcmV0dHkgKyB0aGlzLm9wdGlvbnMudmFsdWVzX3NlcGFyYXRvciArIHRvX3ByZXR0eSwgdGhpcy5yZXN1bHQudG8pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRleHRfZnJvbSA9IHRoaXMuZGVjb3JhdGUoZnJvbV9wcmV0dHksIHRoaXMucmVzdWx0LmZyb20pO1xuICAgICAgICAgICAgICAgICAgICB0ZXh0X3RvID0gdGhpcy5kZWNvcmF0ZSh0b19wcmV0dHksIHRoaXMucmVzdWx0LnRvKTtcblxuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zaW5nbGUuaHRtbCh0ZXh0X3NpbmdsZSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmZyb20uaHRtbCh0ZXh0X2Zyb20pO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS50by5odG1sKHRleHRfdG8pO1xuXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdGhpcy5jYWxjTGFiZWxzKCk7XG5cbiAgICAgICAgICAgICAgICB2YXIgbWluID0gTWF0aC5taW4odGhpcy5sYWJlbHMucF9zaW5nbGVfbGVmdCwgdGhpcy5sYWJlbHMucF9mcm9tX2xlZnQpLFxuICAgICAgICAgICAgICAgICAgICBzaW5nbGVfbGVmdCA9IHRoaXMubGFiZWxzLnBfc2luZ2xlX2xlZnQgKyB0aGlzLmxhYmVscy5wX3NpbmdsZV9mYWtlLFxuICAgICAgICAgICAgICAgICAgICB0b19sZWZ0ID0gdGhpcy5sYWJlbHMucF90b19sZWZ0ICsgdGhpcy5sYWJlbHMucF90b19mYWtlLFxuICAgICAgICAgICAgICAgICAgICBtYXggPSBNYXRoLm1heChzaW5nbGVfbGVmdCwgdG9fbGVmdCk7XG5cbiAgICAgICAgICAgICAgICBpZiAodGhpcy5sYWJlbHMucF9mcm9tX2xlZnQgKyB0aGlzLmxhYmVscy5wX2Zyb21fZmFrZSA+PSB0aGlzLmxhYmVscy5wX3RvX2xlZnQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuZnJvbVswXS5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIjtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUudG9bMF0uc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNpbmdsZVswXS5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucmVzdWx0LmZyb20gPT09IHRoaXMucmVzdWx0LnRvKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50YXJnZXQgPT09IFwiZnJvbVwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuZnJvbVswXS5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudGFyZ2V0ID09PSBcInRvXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS50b1swXS5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLnRhcmdldCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmZyb21bMF0uc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuc2luZ2xlWzBdLnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWF4ID0gdG9fbGVmdDtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmZyb21bMF0uc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS50b1swXS5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIjtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLnNpbmdsZVswXS5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXggPSBNYXRoLm1heChzaW5nbGVfbGVmdCwgdG9fbGVmdCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5mcm9tWzBdLnN0eWxlLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUudG9bMF0uc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5zaW5nbGVbMF0uc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKG1pbiA8IHRoaXMubGFiZWxzLnBfbWluICsgMSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5taW5bMF0uc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUubWluWzBdLnN0eWxlLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAobWF4ID4gMTAwIC0gdGhpcy5sYWJlbHMucF9tYXggLSAxKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLm1heFswXS5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIjtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5tYXhbMF0uc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBEcmF3IHNoYWRvdyBpbnRlcnZhbHNcbiAgICAgICAgICovXG4gICAgICAgIGRyYXdTaGFkb3c6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBvID0gdGhpcy5vcHRpb25zLFxuICAgICAgICAgICAgICAgIGMgPSB0aGlzLiRjYWNoZSxcblxuICAgICAgICAgICAgICAgIGlzX2Zyb21fbWluID0gdHlwZW9mIG8uZnJvbV9taW4gPT09IFwibnVtYmVyXCIgJiYgIWlzTmFOKG8uZnJvbV9taW4pLFxuICAgICAgICAgICAgICAgIGlzX2Zyb21fbWF4ID0gdHlwZW9mIG8uZnJvbV9tYXggPT09IFwibnVtYmVyXCIgJiYgIWlzTmFOKG8uZnJvbV9tYXgpLFxuICAgICAgICAgICAgICAgIGlzX3RvX21pbiA9IHR5cGVvZiBvLnRvX21pbiA9PT0gXCJudW1iZXJcIiAmJiAhaXNOYU4oby50b19taW4pLFxuICAgICAgICAgICAgICAgIGlzX3RvX21heCA9IHR5cGVvZiBvLnRvX21heCA9PT0gXCJudW1iZXJcIiAmJiAhaXNOYU4oby50b19tYXgpLFxuXG4gICAgICAgICAgICAgICAgZnJvbV9taW4sXG4gICAgICAgICAgICAgICAgZnJvbV9tYXgsXG4gICAgICAgICAgICAgICAgdG9fbWluLFxuICAgICAgICAgICAgICAgIHRvX21heDtcblxuICAgICAgICAgICAgaWYgKG8udHlwZSA9PT0gXCJzaW5nbGVcIikge1xuICAgICAgICAgICAgICAgIGlmIChvLmZyb21fc2hhZG93ICYmIChpc19mcm9tX21pbiB8fCBpc19mcm9tX21heCkpIHtcbiAgICAgICAgICAgICAgICAgICAgZnJvbV9taW4gPSB0aGlzLmNvbnZlcnRUb1BlcmNlbnQoaXNfZnJvbV9taW4gPyBvLmZyb21fbWluIDogby5taW4pO1xuICAgICAgICAgICAgICAgICAgICBmcm9tX21heCA9IHRoaXMuY29udmVydFRvUGVyY2VudChpc19mcm9tX21heCA/IG8uZnJvbV9tYXggOiBvLm1heCkgLSBmcm9tX21pbjtcbiAgICAgICAgICAgICAgICAgICAgZnJvbV9taW4gPSB0aGlzLnRvRml4ZWQoZnJvbV9taW4gLSAodGhpcy5jb29yZHMucF9oYW5kbGUgLyAxMDAgKiBmcm9tX21pbikpO1xuICAgICAgICAgICAgICAgICAgICBmcm9tX21heCA9IHRoaXMudG9GaXhlZChmcm9tX21heCAtICh0aGlzLmNvb3Jkcy5wX2hhbmRsZSAvIDEwMCAqIGZyb21fbWF4KSk7XG4gICAgICAgICAgICAgICAgICAgIGZyb21fbWluID0gZnJvbV9taW4gKyAodGhpcy5jb29yZHMucF9oYW5kbGUgLyAyKTtcblxuICAgICAgICAgICAgICAgICAgICBjLnNoYWRfc2luZ2xlWzBdLnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XG4gICAgICAgICAgICAgICAgICAgIGMuc2hhZF9zaW5nbGVbMF0uc3R5bGUubGVmdCA9IGZyb21fbWluICsgXCIlXCI7XG4gICAgICAgICAgICAgICAgICAgIGMuc2hhZF9zaW5nbGVbMF0uc3R5bGUud2lkdGggPSBmcm9tX21heCArIFwiJVwiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGMuc2hhZF9zaW5nbGVbMF0uc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaWYgKG8uZnJvbV9zaGFkb3cgJiYgKGlzX2Zyb21fbWluIHx8IGlzX2Zyb21fbWF4KSkge1xuICAgICAgICAgICAgICAgICAgICBmcm9tX21pbiA9IHRoaXMuY29udmVydFRvUGVyY2VudChpc19mcm9tX21pbiA/IG8uZnJvbV9taW4gOiBvLm1pbik7XG4gICAgICAgICAgICAgICAgICAgIGZyb21fbWF4ID0gdGhpcy5jb252ZXJ0VG9QZXJjZW50KGlzX2Zyb21fbWF4ID8gby5mcm9tX21heCA6IG8ubWF4KSAtIGZyb21fbWluO1xuICAgICAgICAgICAgICAgICAgICBmcm9tX21pbiA9IHRoaXMudG9GaXhlZChmcm9tX21pbiAtICh0aGlzLmNvb3Jkcy5wX2hhbmRsZSAvIDEwMCAqIGZyb21fbWluKSk7XG4gICAgICAgICAgICAgICAgICAgIGZyb21fbWF4ID0gdGhpcy50b0ZpeGVkKGZyb21fbWF4IC0gKHRoaXMuY29vcmRzLnBfaGFuZGxlIC8gMTAwICogZnJvbV9tYXgpKTtcbiAgICAgICAgICAgICAgICAgICAgZnJvbV9taW4gPSBmcm9tX21pbiArICh0aGlzLmNvb3Jkcy5wX2hhbmRsZSAvIDIpO1xuXG4gICAgICAgICAgICAgICAgICAgIGMuc2hhZF9mcm9tWzBdLnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XG4gICAgICAgICAgICAgICAgICAgIGMuc2hhZF9mcm9tWzBdLnN0eWxlLmxlZnQgPSBmcm9tX21pbiArIFwiJVwiO1xuICAgICAgICAgICAgICAgICAgICBjLnNoYWRfZnJvbVswXS5zdHlsZS53aWR0aCA9IGZyb21fbWF4ICsgXCIlXCI7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgYy5zaGFkX2Zyb21bMF0uc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChvLnRvX3NoYWRvdyAmJiAoaXNfdG9fbWluIHx8IGlzX3RvX21heCkpIHtcbiAgICAgICAgICAgICAgICAgICAgdG9fbWluID0gdGhpcy5jb252ZXJ0VG9QZXJjZW50KGlzX3RvX21pbiA/IG8udG9fbWluIDogby5taW4pO1xuICAgICAgICAgICAgICAgICAgICB0b19tYXggPSB0aGlzLmNvbnZlcnRUb1BlcmNlbnQoaXNfdG9fbWF4ID8gby50b19tYXggOiBvLm1heCkgLSB0b19taW47XG4gICAgICAgICAgICAgICAgICAgIHRvX21pbiA9IHRoaXMudG9GaXhlZCh0b19taW4gLSAodGhpcy5jb29yZHMucF9oYW5kbGUgLyAxMDAgKiB0b19taW4pKTtcbiAgICAgICAgICAgICAgICAgICAgdG9fbWF4ID0gdGhpcy50b0ZpeGVkKHRvX21heCAtICh0aGlzLmNvb3Jkcy5wX2hhbmRsZSAvIDEwMCAqIHRvX21heCkpO1xuICAgICAgICAgICAgICAgICAgICB0b19taW4gPSB0b19taW4gKyAodGhpcy5jb29yZHMucF9oYW5kbGUgLyAyKTtcblxuICAgICAgICAgICAgICAgICAgICBjLnNoYWRfdG9bMF0uc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcbiAgICAgICAgICAgICAgICAgICAgYy5zaGFkX3RvWzBdLnN0eWxlLmxlZnQgPSB0b19taW4gKyBcIiVcIjtcbiAgICAgICAgICAgICAgICAgICAgYy5zaGFkX3RvWzBdLnN0eWxlLndpZHRoID0gdG9fbWF4ICsgXCIlXCI7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgYy5zaGFkX3RvWzBdLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cblxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBXcml0ZSB2YWx1ZXMgdG8gaW5wdXQgZWxlbWVudFxuICAgICAgICAgKi9cbiAgICAgICAgd3JpdGVUb0lucHV0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnR5cGUgPT09IFwic2luZ2xlXCIpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnZhbHVlcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuaW5wdXQucHJvcChcInZhbHVlXCIsIHRoaXMucmVzdWx0LmZyb21fdmFsdWUpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LnByb3AoXCJ2YWx1ZVwiLCB0aGlzLnJlc3VsdC5mcm9tKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy4kY2FjaGUuaW5wdXQuZGF0YShcImZyb21cIiwgdGhpcy5yZXN1bHQuZnJvbSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMudmFsdWVzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5pbnB1dC5wcm9wKFwidmFsdWVcIiwgdGhpcy5yZXN1bHQuZnJvbV92YWx1ZSArIHRoaXMub3B0aW9ucy5pbnB1dF92YWx1ZXNfc2VwYXJhdG9yICsgdGhpcy5yZXN1bHQudG9fdmFsdWUpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LnByb3AoXCJ2YWx1ZVwiLCB0aGlzLnJlc3VsdC5mcm9tICsgdGhpcy5vcHRpb25zLmlucHV0X3ZhbHVlc19zZXBhcmF0b3IgKyB0aGlzLnJlc3VsdC50byk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LmRhdGEoXCJmcm9tXCIsIHRoaXMucmVzdWx0LmZyb20pO1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LmRhdGEoXCJ0b1wiLCB0aGlzLnJlc3VsdC50byk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cblxuXG4gICAgICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICAgICAgLy8gQ2FsbGJhY2tzXG5cbiAgICAgICAgY2FsbE9uU3RhcnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMud3JpdGVUb0lucHV0KCk7XG5cbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMub25TdGFydCAmJiB0eXBlb2YgdGhpcy5vcHRpb25zLm9uU3RhcnQgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuc2NvcGUpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLm9uU3RhcnQuY2FsbCh0aGlzLm9wdGlvbnMuc2NvcGUsIHRoaXMucmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMub25TdGFydCh0aGlzLnJlc3VsdCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBjYWxsT25DaGFuZ2U6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMud3JpdGVUb0lucHV0KCk7XG5cbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMub25DaGFuZ2UgJiYgdHlwZW9mIHRoaXMub3B0aW9ucy5vbkNoYW5nZSA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5zY29wZSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMub25DaGFuZ2UuY2FsbCh0aGlzLm9wdGlvbnMuc2NvcGUsIHRoaXMucmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMub25DaGFuZ2UodGhpcy5yZXN1bHQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgY2FsbE9uRmluaXNoOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB0aGlzLndyaXRlVG9JbnB1dCgpO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLm9uRmluaXNoICYmIHR5cGVvZiB0aGlzLm9wdGlvbnMub25GaW5pc2ggPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuc2NvcGUpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLm9uRmluaXNoLmNhbGwodGhpcy5vcHRpb25zLnNjb3BlLCB0aGlzLnJlc3VsdCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLm9uRmluaXNoKHRoaXMucmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGNhbGxPblVwZGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdGhpcy53cml0ZVRvSW5wdXQoKTtcblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5vblVwZGF0ZSAmJiB0eXBlb2YgdGhpcy5vcHRpb25zLm9uVXBkYXRlID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnNjb3BlKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucy5vblVwZGF0ZS5jYWxsKHRoaXMub3B0aW9ucy5zY29wZSwgdGhpcy5yZXN1bHQpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucy5vblVwZGF0ZSh0aGlzLnJlc3VsdCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG5cblxuXG4gICAgICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICAgICAgLy8gU2VydmljZSBtZXRob2RzXG5cbiAgICAgICAgdG9nZ2xlSW5wdXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LnRvZ2dsZUNsYXNzKFwiaXJzLWhpZGRlbi1pbnB1dFwiKTtcblxuICAgICAgICAgICAgaWYgKHRoaXMuaGFzX3RhYl9pbmRleCkge1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LnByb3AoXCJ0YWJpbmRleFwiLCAtMSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuJGNhY2hlLmlucHV0LnJlbW92ZVByb3AoXCJ0YWJpbmRleFwiKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5oYXNfdGFiX2luZGV4ID0gIXRoaXMuaGFzX3RhYl9pbmRleDtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQ29udmVydCByZWFsIHZhbHVlIHRvIHBlcmNlbnRcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHZhbHVlIHtOdW1iZXJ9IFggaW4gcmVhbFxuICAgICAgICAgKiBAcGFyYW0gbm9fbWluIHtib29sZWFuPX0gZG9uJ3QgdXNlIG1pbiB2YWx1ZVxuICAgICAgICAgKiBAcmV0dXJucyB7TnVtYmVyfSBYIGluIHBlcmNlbnRcbiAgICAgICAgICovXG4gICAgICAgIGNvbnZlcnRUb1BlcmNlbnQ6IGZ1bmN0aW9uICh2YWx1ZSwgbm9fbWluKSB7XG4gICAgICAgICAgICB2YXIgZGlhcGFzb24gPSB0aGlzLm9wdGlvbnMubWF4IC0gdGhpcy5vcHRpb25zLm1pbixcbiAgICAgICAgICAgICAgICBvbmVfcGVyY2VudCA9IGRpYXBhc29uIC8gMTAwLFxuICAgICAgICAgICAgICAgIHZhbCwgcGVyY2VudDtcblxuICAgICAgICAgICAgaWYgKCFkaWFwYXNvbikge1xuICAgICAgICAgICAgICAgIHRoaXMubm9fZGlhcGFzb24gPSB0cnVlO1xuICAgICAgICAgICAgICAgIHJldHVybiAwO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAobm9fbWluKSB7XG4gICAgICAgICAgICAgICAgdmFsID0gdmFsdWU7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhbCA9IHZhbHVlIC0gdGhpcy5vcHRpb25zLm1pbjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcGVyY2VudCA9IHZhbCAvIG9uZV9wZXJjZW50O1xuXG4gICAgICAgICAgICByZXR1cm4gdGhpcy50b0ZpeGVkKHBlcmNlbnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBDb252ZXJ0IHBlcmNlbnQgdG8gcmVhbCB2YWx1ZXNcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHBlcmNlbnQge051bWJlcn0gWCBpbiBwZXJjZW50XG4gICAgICAgICAqIEByZXR1cm5zIHtOdW1iZXJ9IFggaW4gcmVhbFxuICAgICAgICAgKi9cbiAgICAgICAgY29udmVydFRvVmFsdWU6IGZ1bmN0aW9uIChwZXJjZW50KSB7XG4gICAgICAgICAgICB2YXIgbWluID0gdGhpcy5vcHRpb25zLm1pbixcbiAgICAgICAgICAgICAgICBtYXggPSB0aGlzLm9wdGlvbnMubWF4LFxuICAgICAgICAgICAgICAgIG1pbl9kZWNpbWFscyA9IG1pbi50b1N0cmluZygpLnNwbGl0KFwiLlwiKVsxXSxcbiAgICAgICAgICAgICAgICBtYXhfZGVjaW1hbHMgPSBtYXgudG9TdHJpbmcoKS5zcGxpdChcIi5cIilbMV0sXG4gICAgICAgICAgICAgICAgbWluX2xlbmd0aCwgbWF4X2xlbmd0aCxcbiAgICAgICAgICAgICAgICBhdmdfZGVjaW1hbHMgPSAwLFxuICAgICAgICAgICAgICAgIGFicyA9IDA7XG5cbiAgICAgICAgICAgIGlmIChwZXJjZW50ID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5taW47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAocGVyY2VudCA9PT0gMTAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5tYXg7XG4gICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgaWYgKG1pbl9kZWNpbWFscykge1xuICAgICAgICAgICAgICAgIG1pbl9sZW5ndGggPSBtaW5fZGVjaW1hbHMubGVuZ3RoO1xuICAgICAgICAgICAgICAgIGF2Z19kZWNpbWFscyA9IG1pbl9sZW5ndGg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAobWF4X2RlY2ltYWxzKSB7XG4gICAgICAgICAgICAgICAgbWF4X2xlbmd0aCA9IG1heF9kZWNpbWFscy5sZW5ndGg7XG4gICAgICAgICAgICAgICAgYXZnX2RlY2ltYWxzID0gbWF4X2xlbmd0aDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChtaW5fbGVuZ3RoICYmIG1heF9sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICBhdmdfZGVjaW1hbHMgPSAobWluX2xlbmd0aCA+PSBtYXhfbGVuZ3RoKSA/IG1pbl9sZW5ndGggOiBtYXhfbGVuZ3RoO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAobWluIDwgMCkge1xuICAgICAgICAgICAgICAgIGFicyA9IE1hdGguYWJzKG1pbik7XG4gICAgICAgICAgICAgICAgbWluID0gKyhtaW4gKyBhYnMpLnRvRml4ZWQoYXZnX2RlY2ltYWxzKTtcbiAgICAgICAgICAgICAgICBtYXggPSArKG1heCArIGFicykudG9GaXhlZChhdmdfZGVjaW1hbHMpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgbnVtYmVyID0gKChtYXggLSBtaW4pIC8gMTAwICogcGVyY2VudCkgKyBtaW4sXG4gICAgICAgICAgICAgICAgc3RyaW5nID0gdGhpcy5vcHRpb25zLnN0ZXAudG9TdHJpbmcoKS5zcGxpdChcIi5cIilbMV0sXG4gICAgICAgICAgICAgICAgcmVzdWx0O1xuXG4gICAgICAgICAgICBpZiAoc3RyaW5nKSB7XG4gICAgICAgICAgICAgICAgbnVtYmVyID0gK251bWJlci50b0ZpeGVkKHN0cmluZy5sZW5ndGgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBudW1iZXIgPSBudW1iZXIgLyB0aGlzLm9wdGlvbnMuc3RlcDtcbiAgICAgICAgICAgICAgICBudW1iZXIgPSBudW1iZXIgKiB0aGlzLm9wdGlvbnMuc3RlcDtcblxuICAgICAgICAgICAgICAgIG51bWJlciA9ICtudW1iZXIudG9GaXhlZCgwKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKGFicykge1xuICAgICAgICAgICAgICAgIG51bWJlciAtPSBhYnM7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChzdHJpbmcpIHtcbiAgICAgICAgICAgICAgICByZXN1bHQgPSArbnVtYmVyLnRvRml4ZWQoc3RyaW5nLmxlbmd0aCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMudG9GaXhlZChudW1iZXIpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAocmVzdWx0IDwgdGhpcy5vcHRpb25zLm1pbikge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMub3B0aW9ucy5taW47XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJlc3VsdCA+IHRoaXMub3B0aW9ucy5tYXgpIHtcbiAgICAgICAgICAgICAgICByZXN1bHQgPSB0aGlzLm9wdGlvbnMubWF4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSb3VuZCBwZXJjZW50IHZhbHVlIHdpdGggc3RlcFxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0gcGVyY2VudCB7TnVtYmVyfVxuICAgICAgICAgKiBAcmV0dXJucyBwZXJjZW50IHtOdW1iZXJ9IHJvdW5kZWRcbiAgICAgICAgICovXG4gICAgICAgIGNhbGNXaXRoU3RlcDogZnVuY3Rpb24gKHBlcmNlbnQpIHtcbiAgICAgICAgICAgIHZhciByb3VuZGVkID0gTWF0aC5yb3VuZChwZXJjZW50IC8gdGhpcy5jb29yZHMucF9zdGVwKSAqIHRoaXMuY29vcmRzLnBfc3RlcDtcblxuICAgICAgICAgICAgaWYgKHJvdW5kZWQgPiAxMDApIHtcbiAgICAgICAgICAgICAgICByb3VuZGVkID0gMTAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHBlcmNlbnQgPT09IDEwMCkge1xuICAgICAgICAgICAgICAgIHJvdW5kZWQgPSAxMDA7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRvRml4ZWQocm91bmRlZCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2hlY2tNaW5JbnRlcnZhbDogZnVuY3Rpb24gKHBfY3VycmVudCwgcF9uZXh0LCB0eXBlKSB7XG4gICAgICAgICAgICB2YXIgbyA9IHRoaXMub3B0aW9ucyxcbiAgICAgICAgICAgICAgICBjdXJyZW50LFxuICAgICAgICAgICAgICAgIG5leHQ7XG5cbiAgICAgICAgICAgIGlmICghby5taW5faW50ZXJ2YWwpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcF9jdXJyZW50O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjdXJyZW50ID0gdGhpcy5jb252ZXJ0VG9WYWx1ZShwX2N1cnJlbnQpO1xuICAgICAgICAgICAgbmV4dCA9IHRoaXMuY29udmVydFRvVmFsdWUocF9uZXh0KTtcblxuICAgICAgICAgICAgaWYgKHR5cGUgPT09IFwiZnJvbVwiKSB7XG5cbiAgICAgICAgICAgICAgICBpZiAobmV4dCAtIGN1cnJlbnQgPCBvLm1pbl9pbnRlcnZhbCkge1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50ID0gbmV4dCAtIG8ubWluX2ludGVydmFsO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIGlmIChjdXJyZW50IC0gbmV4dCA8IG8ubWluX2ludGVydmFsKSB7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnQgPSBuZXh0ICsgby5taW5faW50ZXJ2YWw7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbnZlcnRUb1BlcmNlbnQoY3VycmVudCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2hlY2tNYXhJbnRlcnZhbDogZnVuY3Rpb24gKHBfY3VycmVudCwgcF9uZXh0LCB0eXBlKSB7XG4gICAgICAgICAgICB2YXIgbyA9IHRoaXMub3B0aW9ucyxcbiAgICAgICAgICAgICAgICBjdXJyZW50LFxuICAgICAgICAgICAgICAgIG5leHQ7XG5cbiAgICAgICAgICAgIGlmICghby5tYXhfaW50ZXJ2YWwpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcF9jdXJyZW50O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjdXJyZW50ID0gdGhpcy5jb252ZXJ0VG9WYWx1ZShwX2N1cnJlbnQpO1xuICAgICAgICAgICAgbmV4dCA9IHRoaXMuY29udmVydFRvVmFsdWUocF9uZXh0KTtcblxuICAgICAgICAgICAgaWYgKHR5cGUgPT09IFwiZnJvbVwiKSB7XG5cbiAgICAgICAgICAgICAgICBpZiAobmV4dCAtIGN1cnJlbnQgPiBvLm1heF9pbnRlcnZhbCkge1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50ID0gbmV4dCAtIG8ubWF4X2ludGVydmFsO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIGlmIChjdXJyZW50IC0gbmV4dCA+IG8ubWF4X2ludGVydmFsKSB7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnQgPSBuZXh0ICsgby5tYXhfaW50ZXJ2YWw7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbnZlcnRUb1BlcmNlbnQoY3VycmVudCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2hlY2tEaWFwYXNvbjogZnVuY3Rpb24gKHBfbnVtLCBtaW4sIG1heCkge1xuICAgICAgICAgICAgdmFyIG51bSA9IHRoaXMuY29udmVydFRvVmFsdWUocF9udW0pLFxuICAgICAgICAgICAgICAgIG8gPSB0aGlzLm9wdGlvbnM7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgbWluICE9PSBcIm51bWJlclwiKSB7XG4gICAgICAgICAgICAgICAgbWluID0gby5taW47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgbWF4ICE9PSBcIm51bWJlclwiKSB7XG4gICAgICAgICAgICAgICAgbWF4ID0gby5tYXg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChudW0gPCBtaW4pIHtcbiAgICAgICAgICAgICAgICBudW0gPSBtaW47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChudW0gPiBtYXgpIHtcbiAgICAgICAgICAgICAgICBudW0gPSBtYXg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbnZlcnRUb1BlcmNlbnQobnVtKTtcbiAgICAgICAgfSxcblxuICAgICAgICB0b0ZpeGVkOiBmdW5jdGlvbiAobnVtKSB7XG4gICAgICAgICAgICBudW0gPSBudW0udG9GaXhlZCgyMCk7XG4gICAgICAgICAgICByZXR1cm4gK251bTtcbiAgICAgICAgfSxcblxuICAgICAgICBfcHJldHRpZnk6IGZ1bmN0aW9uIChudW0pIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5vcHRpb25zLnByZXR0aWZ5X2VuYWJsZWQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVtO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnByZXR0aWZ5ICYmIHR5cGVvZiB0aGlzLm9wdGlvbnMucHJldHRpZnkgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLm9wdGlvbnMucHJldHRpZnkobnVtKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucHJldHRpZnkobnVtKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBwcmV0dGlmeTogZnVuY3Rpb24gKG51bSkge1xuICAgICAgICAgICAgdmFyIG4gPSBudW0udG9TdHJpbmcoKTtcbiAgICAgICAgICAgIHJldHVybiBuLnJlcGxhY2UoLyhcXGR7MSwzfSg/PSg/OlxcZFxcZFxcZCkrKD8hXFxkKSkpL2csIFwiJDFcIiArIHRoaXMub3B0aW9ucy5wcmV0dGlmeV9zZXBhcmF0b3IpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNoZWNrRWRnZXM6IGZ1bmN0aW9uIChsZWZ0LCB3aWR0aCkge1xuICAgICAgICAgICAgaWYgKCF0aGlzLm9wdGlvbnMuZm9yY2VfZWRnZXMpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy50b0ZpeGVkKGxlZnQpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAobGVmdCA8IDApIHtcbiAgICAgICAgICAgICAgICBsZWZ0ID0gMDtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAobGVmdCA+IDEwMCAtIHdpZHRoKSB7XG4gICAgICAgICAgICAgICAgbGVmdCA9IDEwMCAtIHdpZHRoO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gdGhpcy50b0ZpeGVkKGxlZnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHZhbGlkYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgbyA9IHRoaXMub3B0aW9ucyxcbiAgICAgICAgICAgICAgICByID0gdGhpcy5yZXN1bHQsXG4gICAgICAgICAgICAgICAgdiA9IG8udmFsdWVzLFxuICAgICAgICAgICAgICAgIHZsID0gdi5sZW5ndGgsXG4gICAgICAgICAgICAgICAgdmFsdWUsXG4gICAgICAgICAgICAgICAgaTtcblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBvLm1pbiA9PT0gXCJzdHJpbmdcIikgby5taW4gPSArby5taW47XG4gICAgICAgICAgICBpZiAodHlwZW9mIG8ubWF4ID09PSBcInN0cmluZ1wiKSBvLm1heCA9ICtvLm1heDtcbiAgICAgICAgICAgIGlmICh0eXBlb2Ygby5mcm9tID09PSBcInN0cmluZ1wiKSBvLmZyb20gPSArby5mcm9tO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBvLnRvID09PSBcInN0cmluZ1wiKSBvLnRvID0gK28udG87XG4gICAgICAgICAgICBpZiAodHlwZW9mIG8uc3RlcCA9PT0gXCJzdHJpbmdcIikgby5zdGVwID0gK28uc3RlcDtcblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBvLmZyb21fbWluID09PSBcInN0cmluZ1wiKSBvLmZyb21fbWluID0gK28uZnJvbV9taW47XG4gICAgICAgICAgICBpZiAodHlwZW9mIG8uZnJvbV9tYXggPT09IFwic3RyaW5nXCIpIG8uZnJvbV9tYXggPSArby5mcm9tX21heDtcbiAgICAgICAgICAgIGlmICh0eXBlb2Ygby50b19taW4gPT09IFwic3RyaW5nXCIpIG8udG9fbWluID0gK28udG9fbWluO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBvLnRvX21heCA9PT0gXCJzdHJpbmdcIikgby50b19tYXggPSArby50b19tYXg7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2Ygby5ncmlkX251bSA9PT0gXCJzdHJpbmdcIikgby5ncmlkX251bSA9ICtvLmdyaWRfbnVtO1xuXG4gICAgICAgICAgICBpZiAoby5tYXggPCBvLm1pbikge1xuICAgICAgICAgICAgICAgIG8ubWF4ID0gby5taW47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh2bCkge1xuICAgICAgICAgICAgICAgIG8ucF92YWx1ZXMgPSBbXTtcbiAgICAgICAgICAgICAgICBvLm1pbiA9IDA7XG4gICAgICAgICAgICAgICAgby5tYXggPSB2bCAtIDE7XG4gICAgICAgICAgICAgICAgby5zdGVwID0gMTtcbiAgICAgICAgICAgICAgICBvLmdyaWRfbnVtID0gby5tYXg7XG4gICAgICAgICAgICAgICAgby5ncmlkX3NuYXAgPSB0cnVlO1xuXG4gICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IHZsOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSArdltpXTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoIWlzTmFOKHZhbHVlKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdltpXSA9IHZhbHVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSB0aGlzLl9wcmV0dGlmeSh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSA9IHZbaV07XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBvLnBfdmFsdWVzLnB1c2godmFsdWUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBvLmZyb20gIT09IFwibnVtYmVyXCIgfHwgaXNOYU4oby5mcm9tKSkge1xuICAgICAgICAgICAgICAgIG8uZnJvbSA9IG8ubWluO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodHlwZW9mIG8udG8gIT09IFwibnVtYmVyXCIgfHwgaXNOYU4oby50bykpIHtcbiAgICAgICAgICAgICAgICBvLnRvID0gby5tYXg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChvLnR5cGUgPT09IFwic2luZ2xlXCIpIHtcblxuICAgICAgICAgICAgICAgIGlmIChvLmZyb20gPCBvLm1pbikgby5mcm9tID0gby5taW47XG4gICAgICAgICAgICAgICAgaWYgKG8uZnJvbSA+IG8ubWF4KSBvLmZyb20gPSBvLm1heDtcblxuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIGlmIChvLmZyb20gPCBvLm1pbikgby5mcm9tID0gby5taW47XG4gICAgICAgICAgICAgICAgaWYgKG8uZnJvbSA+IG8ubWF4KSBvLmZyb20gPSBvLm1heDtcblxuICAgICAgICAgICAgICAgIGlmIChvLnRvIDwgby5taW4pIG8udG8gPSBvLm1pbjtcbiAgICAgICAgICAgICAgICBpZiAoby50byA+IG8ubWF4KSBvLnRvID0gby5tYXg7XG5cbiAgICAgICAgICAgICAgICBpZiAodGhpcy51cGRhdGVfY2hlY2suZnJvbSkge1xuXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnVwZGF0ZV9jaGVjay5mcm9tICE9PSBvLmZyb20pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvLmZyb20gPiBvLnRvKSBvLmZyb20gPSBvLnRvO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnVwZGF0ZV9jaGVjay50byAhPT0gby50bykge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG8udG8gPCBvLmZyb20pIG8udG8gPSBvLmZyb207XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChvLmZyb20gPiBvLnRvKSBvLmZyb20gPSBvLnRvO1xuICAgICAgICAgICAgICAgIGlmIChvLnRvIDwgby5mcm9tKSBvLnRvID0gby5mcm9tO1xuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2Ygby5zdGVwICE9PSBcIm51bWJlclwiIHx8IGlzTmFOKG8uc3RlcCkgfHwgIW8uc3RlcCB8fCBvLnN0ZXAgPCAwKSB7XG4gICAgICAgICAgICAgICAgby5zdGVwID0gMTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBvLmZyb21fbWluID09PSBcIm51bWJlclwiICYmIG8uZnJvbSA8IG8uZnJvbV9taW4pIHtcbiAgICAgICAgICAgICAgICBvLmZyb20gPSBvLmZyb21fbWluO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodHlwZW9mIG8uZnJvbV9tYXggPT09IFwibnVtYmVyXCIgJiYgby5mcm9tID4gby5mcm9tX21heCkge1xuICAgICAgICAgICAgICAgIG8uZnJvbSA9IG8uZnJvbV9tYXg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2Ygby50b19taW4gPT09IFwibnVtYmVyXCIgJiYgby50byA8IG8udG9fbWluKSB7XG4gICAgICAgICAgICAgICAgby50byA9IG8udG9fbWluO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodHlwZW9mIG8udG9fbWF4ID09PSBcIm51bWJlclwiICYmIG8uZnJvbSA+IG8udG9fbWF4KSB7XG4gICAgICAgICAgICAgICAgby50byA9IG8udG9fbWF4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAocikge1xuICAgICAgICAgICAgICAgIGlmIChyLm1pbiAhPT0gby5taW4pIHtcbiAgICAgICAgICAgICAgICAgICAgci5taW4gPSBvLm1pbjtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoci5tYXggIT09IG8ubWF4KSB7XG4gICAgICAgICAgICAgICAgICAgIHIubWF4ID0gby5tYXg7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHIuZnJvbSA8IHIubWluIHx8IHIuZnJvbSA+IHIubWF4KSB7XG4gICAgICAgICAgICAgICAgICAgIHIuZnJvbSA9IG8uZnJvbTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoci50byA8IHIubWluIHx8IHIudG8gPiByLm1heCkge1xuICAgICAgICAgICAgICAgICAgICByLnRvID0gby50bztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2Ygby5taW5faW50ZXJ2YWwgIT09IFwibnVtYmVyXCIgfHwgaXNOYU4oby5taW5faW50ZXJ2YWwpIHx8ICFvLm1pbl9pbnRlcnZhbCB8fCBvLm1pbl9pbnRlcnZhbCA8IDApIHtcbiAgICAgICAgICAgICAgICBvLm1pbl9pbnRlcnZhbCA9IDA7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2Ygby5tYXhfaW50ZXJ2YWwgIT09IFwibnVtYmVyXCIgfHwgaXNOYU4oby5tYXhfaW50ZXJ2YWwpIHx8ICFvLm1heF9pbnRlcnZhbCB8fCBvLm1heF9pbnRlcnZhbCA8IDApIHtcbiAgICAgICAgICAgICAgICBvLm1heF9pbnRlcnZhbCA9IDA7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChvLm1pbl9pbnRlcnZhbCAmJiBvLm1pbl9pbnRlcnZhbCA+IG8ubWF4IC0gby5taW4pIHtcbiAgICAgICAgICAgICAgICBvLm1pbl9pbnRlcnZhbCA9IG8ubWF4IC0gby5taW47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChvLm1heF9pbnRlcnZhbCAmJiBvLm1heF9pbnRlcnZhbCA+IG8ubWF4IC0gby5taW4pIHtcbiAgICAgICAgICAgICAgICBvLm1heF9pbnRlcnZhbCA9IG8ubWF4IC0gby5taW47XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVjb3JhdGU6IGZ1bmN0aW9uIChudW0sIG9yaWdpbmFsKSB7XG4gICAgICAgICAgICB2YXIgZGVjb3JhdGVkID0gXCJcIixcbiAgICAgICAgICAgICAgICBvID0gdGhpcy5vcHRpb25zO1xuXG4gICAgICAgICAgICBpZiAoby5wcmVmaXgpIHtcbiAgICAgICAgICAgICAgICBkZWNvcmF0ZWQgKz0gby5wcmVmaXg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGRlY29yYXRlZCArPSBudW07XG5cbiAgICAgICAgICAgIGlmIChvLm1heF9wb3N0Zml4KSB7XG4gICAgICAgICAgICAgICAgaWYgKG8udmFsdWVzLmxlbmd0aCAmJiBudW0gPT09IG8ucF92YWx1ZXNbby5tYXhdKSB7XG4gICAgICAgICAgICAgICAgICAgIGRlY29yYXRlZCArPSBvLm1heF9wb3N0Zml4O1xuICAgICAgICAgICAgICAgICAgICBpZiAoby5wb3N0Zml4KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkZWNvcmF0ZWQgKz0gXCIgXCI7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKG9yaWdpbmFsID09PSBvLm1heCkge1xuICAgICAgICAgICAgICAgICAgICBkZWNvcmF0ZWQgKz0gby5tYXhfcG9zdGZpeDtcbiAgICAgICAgICAgICAgICAgICAgaWYgKG8ucG9zdGZpeCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGVjb3JhdGVkICs9IFwiIFwiO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoby5wb3N0Zml4KSB7XG4gICAgICAgICAgICAgICAgZGVjb3JhdGVkICs9IG8ucG9zdGZpeDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGRlY29yYXRlZDtcbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGVGcm9tOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB0aGlzLnJlc3VsdC5mcm9tID0gdGhpcy5vcHRpb25zLmZyb207XG4gICAgICAgICAgICB0aGlzLnJlc3VsdC5mcm9tX3BlcmNlbnQgPSB0aGlzLmNvbnZlcnRUb1BlcmNlbnQodGhpcy5yZXN1bHQuZnJvbSk7XG4gICAgICAgICAgICB0aGlzLnJlc3VsdC5mcm9tX3ByZXR0eSA9IHRoaXMuX3ByZXR0aWZ5KHRoaXMucmVzdWx0LmZyb20pO1xuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy52YWx1ZXMpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnJlc3VsdC5mcm9tX3ZhbHVlID0gdGhpcy5vcHRpb25zLnZhbHVlc1t0aGlzLnJlc3VsdC5mcm9tXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGVUbzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdGhpcy5yZXN1bHQudG8gPSB0aGlzLm9wdGlvbnMudG87XG4gICAgICAgICAgICB0aGlzLnJlc3VsdC50b19wZXJjZW50ID0gdGhpcy5jb252ZXJ0VG9QZXJjZW50KHRoaXMucmVzdWx0LnRvKTtcbiAgICAgICAgICAgIHRoaXMucmVzdWx0LnRvX3ByZXR0eSA9IHRoaXMuX3ByZXR0aWZ5KHRoaXMucmVzdWx0LnRvKTtcbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMudmFsdWVzKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXN1bHQudG9fdmFsdWUgPSB0aGlzLm9wdGlvbnMudmFsdWVzW3RoaXMucmVzdWx0LnRvXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGVSZXN1bHQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMucmVzdWx0Lm1pbiA9IHRoaXMub3B0aW9ucy5taW47XG4gICAgICAgICAgICB0aGlzLnJlc3VsdC5tYXggPSB0aGlzLm9wdGlvbnMubWF4O1xuICAgICAgICAgICAgdGhpcy51cGRhdGVGcm9tKCk7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVRvKCk7XG4gICAgICAgIH0sXG5cblxuICAgICAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gICAgICAgIC8vIEdyaWRcblxuICAgICAgICBhcHBlbmRHcmlkOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMub3B0aW9ucy5ncmlkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgbyA9IHRoaXMub3B0aW9ucyxcbiAgICAgICAgICAgICAgICBpLCB6LFxuXG4gICAgICAgICAgICAgICAgdG90YWwgPSBvLm1heCAtIG8ubWluLFxuICAgICAgICAgICAgICAgIGJpZ19udW0gPSBvLmdyaWRfbnVtLFxuICAgICAgICAgICAgICAgIGJpZ19wID0gMCxcbiAgICAgICAgICAgICAgICBiaWdfdyA9IDAsXG5cbiAgICAgICAgICAgICAgICBzbWFsbF9tYXggPSA0LFxuICAgICAgICAgICAgICAgIGxvY2FsX3NtYWxsX21heCxcbiAgICAgICAgICAgICAgICBzbWFsbF9wLFxuICAgICAgICAgICAgICAgIHNtYWxsX3cgPSAwLFxuXG4gICAgICAgICAgICAgICAgcmVzdWx0LFxuICAgICAgICAgICAgICAgIGh0bWwgPSAnJztcblxuXG5cbiAgICAgICAgICAgIHRoaXMuY2FsY0dyaWRNYXJnaW4oKTtcblxuICAgICAgICAgICAgaWYgKG8uZ3JpZF9zbmFwKSB7XG4gICAgICAgICAgICAgICAgYmlnX251bSA9IHRvdGFsIC8gby5zdGVwO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoYmlnX251bSA+IDUwKSBiaWdfbnVtID0gNTA7XG4gICAgICAgICAgICBiaWdfcCA9IHRoaXMudG9GaXhlZCgxMDAgLyBiaWdfbnVtKTtcblxuICAgICAgICAgICAgaWYgKGJpZ19udW0gPiA0KSB7XG4gICAgICAgICAgICAgICAgc21hbGxfbWF4ID0gMztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChiaWdfbnVtID4gNykge1xuICAgICAgICAgICAgICAgIHNtYWxsX21heCA9IDI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoYmlnX251bSA+IDE0KSB7XG4gICAgICAgICAgICAgICAgc21hbGxfbWF4ID0gMTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChiaWdfbnVtID4gMjgpIHtcbiAgICAgICAgICAgICAgICBzbWFsbF9tYXggPSAwO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgYmlnX251bSArIDE7IGkrKykge1xuICAgICAgICAgICAgICAgIGxvY2FsX3NtYWxsX21heCA9IHNtYWxsX21heDtcblxuICAgICAgICAgICAgICAgIGJpZ193ID0gdGhpcy50b0ZpeGVkKGJpZ19wICogaSk7XG5cbiAgICAgICAgICAgICAgICBpZiAoYmlnX3cgPiAxMDApIHtcbiAgICAgICAgICAgICAgICAgICAgYmlnX3cgPSAxMDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLmJpZ1tpXSA9IGJpZ193O1xuXG4gICAgICAgICAgICAgICAgc21hbGxfcCA9IChiaWdfdyAtIChiaWdfcCAqIChpIC0gMSkpKSAvIChsb2NhbF9zbWFsbF9tYXggKyAxKTtcblxuICAgICAgICAgICAgICAgIGZvciAoeiA9IDE7IHogPD0gbG9jYWxfc21hbGxfbWF4OyB6KyspIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGJpZ193ID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIHNtYWxsX3cgPSB0aGlzLnRvRml4ZWQoYmlnX3cgLSAoc21hbGxfcCAqIHopKTtcblxuICAgICAgICAgICAgICAgICAgICBodG1sICs9ICc8c3BhbiBjbGFzcz1cImlycy1ncmlkLXBvbCBzbWFsbFwiIHN0eWxlPVwibGVmdDogJyArIHNtYWxsX3cgKyAnJVwiPjwvc3Bhbj4nO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGh0bWwgKz0gJzxzcGFuIGNsYXNzPVwiaXJzLWdyaWQtcG9sXCIgc3R5bGU9XCJsZWZ0OiAnICsgYmlnX3cgKyAnJVwiPjwvc3Bhbj4nO1xuXG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gdGhpcy5jb252ZXJ0VG9WYWx1ZShiaWdfdyk7XG4gICAgICAgICAgICAgICAgaWYgKG8udmFsdWVzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBvLnBfdmFsdWVzW3Jlc3VsdF07XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gdGhpcy5fcHJldHRpZnkocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBodG1sICs9ICc8c3BhbiBjbGFzcz1cImlycy1ncmlkLXRleHQganMtZ3JpZC10ZXh0LScgKyBpICsgJ1wiIHN0eWxlPVwibGVmdDogJyArIGJpZ193ICsgJyVcIj4nICsgcmVzdWx0ICsgJzwvc3Bhbj4nO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5jb29yZHMuYmlnX251bSA9IE1hdGguY2VpbChiaWdfbnVtICsgMSk7XG5cblxuXG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5jb250LmFkZENsYXNzKFwiaXJzLXdpdGgtZ3JpZFwiKTtcbiAgICAgICAgICAgIHRoaXMuJGNhY2hlLmdyaWQuaHRtbChodG1sKTtcbiAgICAgICAgICAgIHRoaXMuY2FjaGVHcmlkTGFiZWxzKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2FjaGVHcmlkTGFiZWxzOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgJGxhYmVsLCBpLFxuICAgICAgICAgICAgICAgIG51bSA9IHRoaXMuY29vcmRzLmJpZ19udW07XG5cbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBudW07IGkrKykge1xuICAgICAgICAgICAgICAgICRsYWJlbCA9IHRoaXMuJGNhY2hlLmdyaWQuZmluZChcIi5qcy1ncmlkLXRleHQtXCIgKyBpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRjYWNoZS5ncmlkX2xhYmVscy5wdXNoKCRsYWJlbCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuY2FsY0dyaWRMYWJlbHMoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBjYWxjR3JpZExhYmVsczogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGksIGxhYmVsLCBzdGFydCA9IFtdLCBmaW5pc2ggPSBbXSxcbiAgICAgICAgICAgICAgICBudW0gPSB0aGlzLmNvb3Jkcy5iaWdfbnVtO1xuXG4gICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgbnVtOyBpKyspIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5iaWdfd1tpXSA9IHRoaXMuJGNhY2hlLmdyaWRfbGFiZWxzW2ldLm91dGVyV2lkdGgoZmFsc2UpO1xuICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLmJpZ19wW2ldID0gdGhpcy50b0ZpeGVkKHRoaXMuY29vcmRzLmJpZ193W2ldIC8gdGhpcy5jb29yZHMud19ycyAqIDEwMCk7XG4gICAgICAgICAgICAgICAgdGhpcy5jb29yZHMuYmlnX3hbaV0gPSB0aGlzLnRvRml4ZWQodGhpcy5jb29yZHMuYmlnX3BbaV0gLyAyKTtcblxuICAgICAgICAgICAgICAgIHN0YXJ0W2ldID0gdGhpcy50b0ZpeGVkKHRoaXMuY29vcmRzLmJpZ1tpXSAtIHRoaXMuY29vcmRzLmJpZ194W2ldKTtcbiAgICAgICAgICAgICAgICBmaW5pc2hbaV0gPSB0aGlzLnRvRml4ZWQoc3RhcnRbaV0gKyB0aGlzLmNvb3Jkcy5iaWdfcFtpXSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuZm9yY2VfZWRnZXMpIHtcbiAgICAgICAgICAgICAgICBpZiAoc3RhcnRbMF0gPCAtdGhpcy5jb29yZHMuZ3JpZF9nYXApIHtcbiAgICAgICAgICAgICAgICAgICAgc3RhcnRbMF0gPSAtdGhpcy5jb29yZHMuZ3JpZF9nYXA7XG4gICAgICAgICAgICAgICAgICAgIGZpbmlzaFswXSA9IHRoaXMudG9GaXhlZChzdGFydFswXSArIHRoaXMuY29vcmRzLmJpZ19wWzBdKTtcblxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy5iaWdfeFswXSA9IHRoaXMuY29vcmRzLmdyaWRfZ2FwO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChmaW5pc2hbbnVtIC0gMV0gPiAxMDAgKyB0aGlzLmNvb3Jkcy5ncmlkX2dhcCkge1xuICAgICAgICAgICAgICAgICAgICBmaW5pc2hbbnVtIC0gMV0gPSAxMDAgKyB0aGlzLmNvb3Jkcy5ncmlkX2dhcDtcbiAgICAgICAgICAgICAgICAgICAgc3RhcnRbbnVtIC0gMV0gPSB0aGlzLnRvRml4ZWQoZmluaXNoW251bSAtIDFdIC0gdGhpcy5jb29yZHMuYmlnX3BbbnVtIC0gMV0pO1xuXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29vcmRzLmJpZ194W251bSAtIDFdID0gdGhpcy50b0ZpeGVkKHRoaXMuY29vcmRzLmJpZ19wW251bSAtIDFdIC0gdGhpcy5jb29yZHMuZ3JpZF9nYXApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5jYWxjR3JpZENvbGxpc2lvbigyLCBzdGFydCwgZmluaXNoKTtcbiAgICAgICAgICAgIHRoaXMuY2FsY0dyaWRDb2xsaXNpb24oNCwgc3RhcnQsIGZpbmlzaCk7XG5cbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBudW07IGkrKykge1xuICAgICAgICAgICAgICAgIGxhYmVsID0gdGhpcy4kY2FjaGUuZ3JpZF9sYWJlbHNbaV1bMF07XG5cbiAgICAgICAgICAgICAgICBpZiAodGhpcy5jb29yZHMuYmlnX3hbaV0gIT09IE51bWJlci5QT1NJVElWRV9JTkZJTklUWSkge1xuICAgICAgICAgICAgICAgICAgICBsYWJlbC5zdHlsZS5tYXJnaW5MZWZ0ID0gLXRoaXMuY29vcmRzLmJpZ194W2ldICsgXCIlXCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8vIENvbGxpc2lvbnMgQ2FsYyBCZXRhXG4gICAgICAgIC8vIFRPRE86IFJlZmFjdG9yIHRoZW4gaGF2ZSBwbGVudHkgb2YgdGltZVxuICAgICAgICBjYWxjR3JpZENvbGxpc2lvbjogZnVuY3Rpb24gKHN0ZXAsIHN0YXJ0LCBmaW5pc2gpIHtcbiAgICAgICAgICAgIHZhciBpLCBuZXh0X2ksIGxhYmVsLFxuICAgICAgICAgICAgICAgIG51bSA9IHRoaXMuY29vcmRzLmJpZ19udW07XG5cbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBudW07IGkgKz0gc3RlcCkge1xuICAgICAgICAgICAgICAgIG5leHRfaSA9IGkgKyAoc3RlcCAvIDIpO1xuICAgICAgICAgICAgICAgIGlmIChuZXh0X2kgPj0gbnVtKSB7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGxhYmVsID0gdGhpcy4kY2FjaGUuZ3JpZF9sYWJlbHNbbmV4dF9pXVswXTtcblxuICAgICAgICAgICAgICAgIGlmIChmaW5pc2hbaV0gPD0gc3RhcnRbbmV4dF9pXSkge1xuICAgICAgICAgICAgICAgICAgICBsYWJlbC5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbGFiZWwuc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGNhbGNHcmlkTWFyZ2luOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMub3B0aW9ucy5ncmlkX21hcmdpbikge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5jb29yZHMud19ycyA9IHRoaXMuJGNhY2hlLnJzLm91dGVyV2lkdGgoZmFsc2UpO1xuICAgICAgICAgICAgaWYgKCF0aGlzLmNvb3Jkcy53X3JzKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnR5cGUgPT09IFwic2luZ2xlXCIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy53X2hhbmRsZSA9IHRoaXMuJGNhY2hlLnNfc2luZ2xlLm91dGVyV2lkdGgoZmFsc2UpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvb3Jkcy53X2hhbmRsZSA9IHRoaXMuJGNhY2hlLnNfZnJvbS5vdXRlcldpZHRoKGZhbHNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuY29vcmRzLnBfaGFuZGxlID0gdGhpcy50b0ZpeGVkKHRoaXMuY29vcmRzLndfaGFuZGxlICAvIHRoaXMuY29vcmRzLndfcnMgKiAxMDApO1xuICAgICAgICAgICAgdGhpcy5jb29yZHMuZ3JpZF9nYXAgPSB0aGlzLnRvRml4ZWQoKHRoaXMuY29vcmRzLnBfaGFuZGxlIC8gMikgLSAwLjEpO1xuXG4gICAgICAgICAgICB0aGlzLiRjYWNoZS5ncmlkWzBdLnN0eWxlLndpZHRoID0gdGhpcy50b0ZpeGVkKDEwMCAtIHRoaXMuY29vcmRzLnBfaGFuZGxlKSArIFwiJVwiO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUuZ3JpZFswXS5zdHlsZS5sZWZ0ID0gdGhpcy5jb29yZHMuZ3JpZF9nYXAgKyBcIiVcIjtcbiAgICAgICAgfSxcblxuXG5cbiAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICAgICAgICAvLyBQdWJsaWMgbWV0aG9kc1xuXG4gICAgICAgIHVwZGF0ZTogZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5pbnB1dCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5pc191cGRhdGUgPSB0cnVlO1xuXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMuZnJvbSA9IHRoaXMucmVzdWx0LmZyb207XG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMudG8gPSB0aGlzLnJlc3VsdC50bztcbiAgICAgICAgICAgIHRoaXMudXBkYXRlX2NoZWNrLmZyb20gPSB0aGlzLnJlc3VsdC5mcm9tO1xuICAgICAgICAgICAgdGhpcy51cGRhdGVfY2hlY2sudG8gPSB0aGlzLnJlc3VsdC50bztcblxuICAgICAgICAgICAgdGhpcy5vcHRpb25zID0gJC5leHRlbmQodGhpcy5vcHRpb25zLCBvcHRpb25zKTtcbiAgICAgICAgICAgIHRoaXMudmFsaWRhdGUoKTtcbiAgICAgICAgICAgIHRoaXMudXBkYXRlUmVzdWx0KG9wdGlvbnMpO1xuXG4gICAgICAgICAgICB0aGlzLnRvZ2dsZUlucHV0KCk7XG4gICAgICAgICAgICB0aGlzLnJlbW92ZSgpO1xuICAgICAgICAgICAgdGhpcy5pbml0KHRydWUpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlc2V0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMuaW5wdXQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMudXBkYXRlUmVzdWx0KCk7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZSgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5pbnB1dCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy50b2dnbGVJbnB1dCgpO1xuICAgICAgICAgICAgdGhpcy4kY2FjaGUuaW5wdXQucHJvcChcInJlYWRvbmx5XCIsIGZhbHNlKTtcbiAgICAgICAgICAgICQuZGF0YSh0aGlzLmlucHV0LCBcImlvblJhbmdlU2xpZGVyXCIsIG51bGwpO1xuXG4gICAgICAgICAgICB0aGlzLnJlbW92ZSgpO1xuICAgICAgICAgICAgdGhpcy5pbnB1dCA9IG51bGw7XG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMgPSBudWxsO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgICQuZm4uaW9uUmFuZ2VTbGlkZXIgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYgKCEkLmRhdGEodGhpcywgXCJpb25SYW5nZVNsaWRlclwiKSkge1xuICAgICAgICAgICAgICAgICQuZGF0YSh0aGlzLCBcImlvblJhbmdlU2xpZGVyXCIsIG5ldyBJb25SYW5nZVNsaWRlcih0aGlzLCBvcHRpb25zLCBwbHVnaW5fY291bnQrKykpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9O1xuXG5cblxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gICAgLy8gaHR0cDovL3BhdWxpcmlzaC5jb20vMjAxMS9yZXF1ZXN0YW5pbWF0aW9uZnJhbWUtZm9yLXNtYXJ0LWFuaW1hdGluZy9cbiAgICAvLyBodHRwOi8vbXkub3BlcmEuY29tL2Vtb2xsZXIvYmxvZy8yMDExLzEyLzIwL3JlcXVlc3RhbmltYXRpb25mcmFtZS1mb3Itc21hcnQtZXItYW5pbWF0aW5nXG5cbiAgICAvLyByZXF1ZXN0QW5pbWF0aW9uRnJhbWUgcG9seWZpbGwgYnkgRXJpayBNw7ZsbGVyLiBmaXhlcyBmcm9tIFBhdWwgSXJpc2ggYW5kIFRpbm8gWmlqZGVsXG5cbiAgICAvLyBNSVQgbGljZW5zZVxuXG4gICAgKGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgbGFzdFRpbWUgPSAwO1xuICAgICAgICB2YXIgdmVuZG9ycyA9IFsnbXMnLCAnbW96JywgJ3dlYmtpdCcsICdvJ107XG4gICAgICAgIGZvcih2YXIgeCA9IDA7IHggPCB2ZW5kb3JzLmxlbmd0aCAmJiAhd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZTsgKyt4KSB7XG4gICAgICAgICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93W3ZlbmRvcnNbeF0rJ1JlcXVlc3RBbmltYXRpb25GcmFtZSddO1xuICAgICAgICAgICAgd2luZG93LmNhbmNlbEFuaW1hdGlvbkZyYW1lID0gd2luZG93W3ZlbmRvcnNbeF0rJ0NhbmNlbEFuaW1hdGlvbkZyYW1lJ11cbiAgICAgICAgICAgICAgICB8fCB3aW5kb3dbdmVuZG9yc1t4XSsnQ2FuY2VsUmVxdWVzdEFuaW1hdGlvbkZyYW1lJ107XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIXdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUpXG4gICAgICAgICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gZnVuY3Rpb24oY2FsbGJhY2ssIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICB2YXIgY3VyclRpbWUgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgICB2YXIgdGltZVRvQ2FsbCA9IE1hdGgubWF4KDAsIDE2IC0gKGN1cnJUaW1lIC0gbGFzdFRpbWUpKTtcbiAgICAgICAgICAgICAgICB2YXIgaWQgPSB3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbigpIHsgY2FsbGJhY2soY3VyclRpbWUgKyB0aW1lVG9DYWxsKTsgfSxcbiAgICAgICAgICAgICAgICAgICAgdGltZVRvQ2FsbCk7XG4gICAgICAgICAgICAgICAgbGFzdFRpbWUgPSBjdXJyVGltZSArIHRpbWVUb0NhbGw7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGlkO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICBpZiAoIXdpbmRvdy5jYW5jZWxBbmltYXRpb25GcmFtZSlcbiAgICAgICAgICAgIHdpbmRvdy5jYW5jZWxBbmltYXRpb25GcmFtZSA9IGZ1bmN0aW9uKGlkKSB7XG4gICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KGlkKTtcbiAgICAgICAgICAgIH07XG4gICAgfSgpKTtcblxufSkpO1xuIiwiLyogZ2xvYmFscyBfX3dlYnBhY2tfYW1kX29wdGlvbnNfXyAqL1xubW9kdWxlLmV4cG9ydHMgPSBfX3dlYnBhY2tfYW1kX29wdGlvbnNfXztcbiJdLCJzb3VyY2VSb290IjoiIn0=