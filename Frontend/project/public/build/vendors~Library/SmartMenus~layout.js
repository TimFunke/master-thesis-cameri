(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendors~Library/SmartMenus~layout"],{

/***/ "./node_modules/smartmenus-bootstrap-4/jquery.smartmenus.bootstrap-4.css":
/*!*******************************************************************************!*\
  !*** ./node_modules/smartmenus-bootstrap-4/jquery.smartmenus.bootstrap-4.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./node_modules/smartmenus-bootstrap-4/jquery.smartmenus.bootstrap-4.js":
/*!******************************************************************************!*\
  !*** ./node_modules/smartmenus-bootstrap-4/jquery.smartmenus.bootstrap-4.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * SmartMenus jQuery Plugin Bootstrap 4 Addon - v0.1.0 - September 17, 2017
 * http://www.smartmenus.org/
 *
 * Copyright Vasil Dinkov, Vadikom Web Ltd.
 * http://vadikom.com
 *
 * Licensed MIT
 */

(function(factory) {
	if (true) {
		// AMD
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! smartmenus */ "./node_modules/smartmenus-bootstrap-4/node_modules/smartmenus/dist/jquery.smartmenus.js")], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}
} (function($) {

	$.extend($.SmartMenus.Bootstrap = {}, {
		keydownFix: false,
		init: function() {
			// init all navbars that don't have the "data-sm-skip" attribute set
			var $navbars = $('ul.navbar-nav:not([data-sm-skip])');
			$navbars.each(function() {
				var $this = $(this),
					obj = $this.data('smartmenus');
				// if this navbar is not initialized
				if (!obj) {
					var skipBehavior = $this.is('[data-sm-skip-collapsible-behavior]'),
						rightAligned = $this.hasClass('ml-auto') || $this.prevAll('.mr-auto').length > 0;

					$this.smartmenus({
							// these are some good default options that should work for all
							subMenusSubOffsetX: 2,
							subMenusSubOffsetY: -9,
							subIndicators: !skipBehavior,
							collapsibleShowFunction: null,
							collapsibleHideFunction: null,
							rightToLeftSubMenus: rightAligned,
							bottomToTopSubMenus: $this.closest('.fixed-bottom').length > 0,
							// custom option(s) for the Bootstrap 4 addon
							bootstrapHighlightClasses: 'text-dark bg-light'
						})
						.on({
							// set/unset proper Bootstrap classes for some menu elements
							'show.smapi': function(e, menu) {
								var $menu = $(menu),
									$scrollArrows = $menu.dataSM('scroll-arrows');
								if ($scrollArrows) {
									$scrollArrows.css('background-color', $menu.css('background-color'));
								}
								$menu.parent().addClass('show');
								if (obj.opts.keepHighlighted && $menu.dataSM('level') > 2) {
									$menu.prevAll('a').addClass(obj.opts.bootstrapHighlightClasses);
								}
							},
							'hide.smapi': function(e, menu) {
								var $menu = $(menu);
								$menu.parent().removeClass('show');
								if (obj.opts.keepHighlighted && $menu.dataSM('level') > 2) {
									$menu.prevAll('a').removeClass(obj.opts.bootstrapHighlightClasses);
								}
							}
						});

					obj = $this.data('smartmenus');

					function onInit() {
						// set Bootstrap's "active" class to SmartMenus "current" items (should someone decide to enable markCurrentItem: true)
						$this.find('a.current').each(function() {
							var $this = $(this);
							// dropdown items require the class to be set to the A's while for nav items it should be set to the parent LI's
							($this.hasClass('dropdown-item') ? $this : $this.parent()).addClass('active');
						});
						// parent items fixes
						$this.find('a.has-submenu').each(function() {
							var $this = $(this);
							// remove Bootstrap required attributes that might cause conflicting issues with the SmartMenus script
							if ($this.is('[data-toggle="dropdown"]')) {
								$this.dataSM('bs-data-toggle-dropdown', true).removeAttr('data-toggle');
							}
							// remove Bootstrap's carets generating class
							if (!skipBehavior && $this.hasClass('dropdown-toggle')) {
								$this.dataSM('bs-dropdown-toggle', true).removeClass('dropdown-toggle');
							}
						});
					}

					onInit();

					function onBeforeDestroy() {
						$this.find('a.current').each(function() {
							var $this = $(this);
							($this.hasClass('active') ? $this : $this.parent()).removeClass('active');
						});
						$this.find('a.has-submenu').each(function() {
							var $this = $(this);
							if ($this.dataSM('bs-dropdown-toggle')) {
								$this.addClass('dropdown-toggle').removeDataSM('bs-dropdown-toggle');
							}
							if ($this.dataSM('bs-data-toggle-dropdown')) {
								$this.attr('data-toggle', 'dropdown').removeDataSM('bs-data-toggle-dropdown');
							}
						});
					}

					// custom "refresh" method for Bootstrap
					obj.refresh = function() {
						$.SmartMenus.prototype.refresh.call(this);
						onInit();
						// update collapsible detection
						detectCollapsible(true);
					};

					// custom "destroy" method for Bootstrap
					obj.destroy = function(refresh) {
						onBeforeDestroy();
						$.SmartMenus.prototype.destroy.call(this, refresh);
					};

					// keep Bootstrap's default behavior (i.e. use the whole item area just as a sub menu toggle)
					if (skipBehavior) {
						obj.opts.collapsibleBehavior = 'toggle';
					}

					// onresize detect when the navbar becomes collapsible and add it the "sm-collapsible" class
					var winW;
					function detectCollapsible(force) {
						var newW = obj.getViewportWidth();
						if (newW != winW || force) {
							if (obj.isCollapsible()) {
								$this.addClass('sm-collapsible');
							} else {
								$this.removeClass('sm-collapsible');
							}
							winW = newW;
						}
					}
					detectCollapsible();
					$(window).on('resize.smartmenus' + obj.rootId, detectCollapsible);
				}
			});
			// keydown fix for Bootstrap 4 conflict
			if ($navbars.length && !$.SmartMenus.Bootstrap.keydownFix) {
				// unhook BS keydown handler for all dropdowns
				$(document).off('keydown.bs.dropdown.data-api', '.dropdown-menu');
				// restore BS keydown handler for dropdowns that are not inside SmartMenus navbars
				// SmartMenus won't add the "show" class so it's handy here
				if ($.fn.dropdown && $.fn.dropdown.Constructor) {
					$(document).on('keydown.bs.dropdown.data-api', '.dropdown-menu.show', $.fn.dropdown.Constructor._dataApiKeydownHandler);
				}
				$.SmartMenus.Bootstrap.keydownFix = true;
			}
		}
	});

	// init ondomready
	$($.SmartMenus.Bootstrap.init);

	return $;
}));


/***/ }),

/***/ "./node_modules/smartmenus-bootstrap-4/node_modules/smartmenus/dist/jquery.smartmenus.js":
/*!***********************************************************************************************!*\
  !*** ./node_modules/smartmenus-bootstrap-4/node_modules/smartmenus/dist/jquery.smartmenus.js ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * SmartMenus jQuery Plugin - v1.1.1 - July 23, 2020
 * http://www.smartmenus.org/
 *
 * Copyright Vasil Dinkov, Vadikom Web Ltd.
 * http://vadikom.com
 *
 * Licensed MIT
 */

(function(factory) {
	if (true) {
		// AMD
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}
} (function($) {

	var menuTrees = [],
		mouse = false, // optimize for touch by default - we will detect for mouse input
		touchEvents = 'ontouchstart' in window, // we use this just to choose between toucn and pointer events, not for touch screen detection
		mouseDetectionEnabled = false,
		requestAnimationFrame = window.requestAnimationFrame || function(callback) { return setTimeout(callback, 1000 / 60); },
		cancelAnimationFrame = window.cancelAnimationFrame || function(id) { clearTimeout(id); },
		canAnimate = !!$.fn.animate;

	// Handle detection for mouse input (i.e. desktop browsers, tablets with a mouse, etc.)
	function initMouseDetection(disable) {
		var eNS = '.smartmenus_mouse';
		if (!mouseDetectionEnabled && !disable) {
			// if we get two consecutive mousemoves within 2 pixels from each other and within 300ms, we assume a real mouse/cursor is present
			// in practice, this seems like impossible to trick unintentianally with a real mouse and a pretty safe detection on touch devices (even with older browsers that do not support touch events)
			var firstTime = true,
				lastMove = null,
				events = {
					'mousemove': function(e) {
						var thisMove = { x: e.pageX, y: e.pageY, timeStamp: new Date().getTime() };
						if (lastMove) {
							var deltaX = Math.abs(lastMove.x - thisMove.x),
								deltaY = Math.abs(lastMove.y - thisMove.y);
		 					if ((deltaX > 0 || deltaY > 0) && deltaX <= 4 && deltaY <= 4 && thisMove.timeStamp - lastMove.timeStamp <= 300) {
								mouse = true;
								// if this is the first check after page load, check if we are not over some item by chance and call the mouseenter handler if yes
								if (firstTime) {
									var $a = $(e.target).closest('a');
									if ($a.is('a')) {
										$.each(menuTrees, function() {
											if ($.contains(this.$root[0], $a[0])) {
												this.itemEnter({ currentTarget: $a[0] });
												return false;
											}
										});
									}
									firstTime = false;
								}
							}
						}
						lastMove = thisMove;
					}
				};
			events[touchEvents ? 'touchstart' : 'pointerover pointermove pointerout MSPointerOver MSPointerMove MSPointerOut'] = function(e) {
				if (isTouchEvent(e.originalEvent)) {
					mouse = false;
				}
			};
			$(document).on(getEventsNS(events, eNS));
			mouseDetectionEnabled = true;
		} else if (mouseDetectionEnabled && disable) {
			$(document).off(eNS);
			mouseDetectionEnabled = false;
		}
	}

	function isTouchEvent(e) {
		return !/^(4|mouse)$/.test(e.pointerType);
	}

	// returns a jQuery on() ready object
	function getEventsNS(events, eNS) {
		if (!eNS) {
			eNS = '';
		}
		var eventsNS = {};
		for (var i in events) {
			eventsNS[i.split(' ').join(eNS + ' ') + eNS] = events[i];
		}
		return eventsNS;
	}

	$.SmartMenus = function(elm, options) {
		this.$root = $(elm);
		this.opts = options;
		this.rootId = ''; // internal
		this.accessIdPrefix = '';
		this.$subArrow = null;
		this.activatedItems = []; // stores last activated A's for each level
		this.visibleSubMenus = []; // stores visible sub menus UL's (might be in no particular order)
		this.showTimeout = 0;
		this.hideTimeout = 0;
		this.scrollTimeout = 0;
		this.clickActivated = false;
		this.focusActivated = false;
		this.zIndexInc = 0;
		this.idInc = 0;
		this.$firstLink = null; // we'll use these for some tests
		this.$firstSub = null; // at runtime so we'll cache them
		this.disabled = false;
		this.$disableOverlay = null;
		this.$touchScrollingSub = null;
		this.cssTransforms3d = 'perspective' in elm.style || 'webkitPerspective' in elm.style;
		this.wasCollapsible = false;
		this.init();
	};

	$.extend($.SmartMenus, {
		hideAll: function() {
			$.each(menuTrees, function() {
				this.menuHideAll();
			});
		},
		destroy: function() {
			while (menuTrees.length) {
				menuTrees[0].destroy();
			}
			initMouseDetection(true);
		},
		prototype: {
			init: function(refresh) {
				var self = this;

				if (!refresh) {
					menuTrees.push(this);

					this.rootId = (new Date().getTime() + Math.random() + '').replace(/\D/g, '');
					this.accessIdPrefix = 'sm-' + this.rootId + '-';

					if (this.$root.hasClass('sm-rtl')) {
						this.opts.rightToLeftSubMenus = true;
					}

					// init root (main menu)
					var eNS = '.smartmenus';
					this.$root
						.data('smartmenus', this)
						.attr('data-smartmenus-id', this.rootId)
						.dataSM('level', 1)
						.on(getEventsNS({
							'mouseover focusin': $.proxy(this.rootOver, this),
							'mouseout focusout': $.proxy(this.rootOut, this),
							'keydown': $.proxy(this.rootKeyDown, this)
						}, eNS))
						.on(getEventsNS({
							'mouseenter': $.proxy(this.itemEnter, this),
							'mouseleave': $.proxy(this.itemLeave, this),
							'mousedown': $.proxy(this.itemDown, this),
							'focus': $.proxy(this.itemFocus, this),
							'blur': $.proxy(this.itemBlur, this),
							'click': $.proxy(this.itemClick, this)
						}, eNS), 'a');

					// hide menus on tap or click outside the root UL
					eNS += this.rootId;
					if (this.opts.hideOnClick) {
						$(document).on(getEventsNS({
							'touchstart': $.proxy(this.docTouchStart, this),
							'touchmove': $.proxy(this.docTouchMove, this),
							'touchend': $.proxy(this.docTouchEnd, this),
							// for Opera Mobile < 11.5, webOS browser, etc. we'll check click too
							'click': $.proxy(this.docClick, this)
						}, eNS));
					}
					// hide sub menus on resize
					$(window).on(getEventsNS({ 'resize orientationchange': $.proxy(this.winResize, this) }, eNS));

					if (this.opts.subIndicators) {
						this.$subArrow = $('<span/>').addClass('sub-arrow');
						if (this.opts.subIndicatorsText) {
							this.$subArrow.html(this.opts.subIndicatorsText);
						}
					}

					// make sure mouse detection is enabled
					initMouseDetection();
				}

				// init sub menus
				this.$firstSub = this.$root.find('ul').each(function() { self.menuInit($(this)); }).eq(0);

				this.$firstLink = this.$root.find('a').eq(0);

				// find current item
				if (this.opts.markCurrentItem) {
					var reDefaultDoc = /(index|default)\.[^#\?\/]*/i,
						reHash = /#.*/,
						locHref = window.location.href.replace(reDefaultDoc, ''),
						locHrefNoHash = locHref.replace(reHash, '');
					this.$root.find('a:not(.mega-menu a)').each(function() {
						var href = this.href.replace(reDefaultDoc, ''),
							$this = $(this);
						if (href == locHref || href == locHrefNoHash) {
							$this.addClass('current');
							if (self.opts.markCurrentTree) {
								$this.parentsUntil('[data-smartmenus-id]', 'ul').each(function() {
									$(this).dataSM('parent-a').addClass('current');
								});
							}
						}
					});
				}

				// save initial state
				this.wasCollapsible = this.isCollapsible();
			},
			destroy: function(refresh) {
				if (!refresh) {
					var eNS = '.smartmenus';
					this.$root
						.removeData('smartmenus')
						.removeAttr('data-smartmenus-id')
						.removeDataSM('level')
						.off(eNS);
					eNS += this.rootId;
					$(document).off(eNS);
					$(window).off(eNS);
					if (this.opts.subIndicators) {
						this.$subArrow = null;
					}
				}
				this.menuHideAll();
				var self = this;
				this.$root.find('ul').each(function() {
						var $this = $(this);
						if ($this.dataSM('scroll-arrows')) {
							$this.dataSM('scroll-arrows').remove();
						}
						if ($this.dataSM('shown-before')) {
							if (self.opts.subMenusMinWidth || self.opts.subMenusMaxWidth) {
								$this.css({ width: '', minWidth: '', maxWidth: '' }).removeClass('sm-nowrap');
							}
							if ($this.dataSM('scroll-arrows')) {
								$this.dataSM('scroll-arrows').remove();
							}
							$this.css({ zIndex: '', top: '', left: '', marginLeft: '', marginTop: '', display: '' });
						}
						if (($this.attr('id') || '').indexOf(self.accessIdPrefix) == 0) {
							$this.removeAttr('id');
						}
					})
					.removeDataSM('in-mega')
					.removeDataSM('shown-before')
					.removeDataSM('scroll-arrows')
					.removeDataSM('parent-a')
					.removeDataSM('level')
					.removeDataSM('beforefirstshowfired')
					.removeAttr('role')
					.removeAttr('aria-hidden')
					.removeAttr('aria-labelledby')
					.removeAttr('aria-expanded');
				this.$root.find('a.has-submenu').each(function() {
						var $this = $(this);
						if ($this.attr('id').indexOf(self.accessIdPrefix) == 0) {
							$this.removeAttr('id');
						}
					})
					.removeClass('has-submenu')
					.removeDataSM('sub')
					.removeAttr('aria-haspopup')
					.removeAttr('aria-controls')
					.removeAttr('aria-expanded')
					.closest('li').removeDataSM('sub');
				if (this.opts.subIndicators) {
					this.$root.find('span.sub-arrow').remove();
				}
				if (this.opts.markCurrentItem) {
					this.$root.find('a.current').removeClass('current');
				}
				if (!refresh) {
					this.$root = null;
					this.$firstLink = null;
					this.$firstSub = null;
					if (this.$disableOverlay) {
						this.$disableOverlay.remove();
						this.$disableOverlay = null;
					}
					menuTrees.splice($.inArray(this, menuTrees), 1);
				}
			},
			disable: function(noOverlay) {
				if (!this.disabled) {
					this.menuHideAll();
					// display overlay over the menu to prevent interaction
					if (!noOverlay && !this.opts.isPopup && this.$root.is(':visible')) {
						var pos = this.$root.offset();
						this.$disableOverlay = $('<div class="sm-jquery-disable-overlay"/>').css({
							position: 'absolute',
							top: pos.top,
							left: pos.left,
							width: this.$root.outerWidth(),
							height: this.$root.outerHeight(),
							zIndex: this.getStartZIndex(true),
							opacity: 0
						}).appendTo(document.body);
					}
					this.disabled = true;
				}
			},
			docClick: function(e) {
				if (this.$touchScrollingSub) {
					this.$touchScrollingSub = null;
					return;
				}
				// hide on any click outside the menu or on a menu link
				if (this.visibleSubMenus.length && !$.contains(this.$root[0], e.target) || $(e.target).closest('a').length) {
					this.menuHideAll();
				}
			},
			docTouchEnd: function(e) {
				if (!this.lastTouch) {
					return;
				}
				if (this.visibleSubMenus.length && (this.lastTouch.x2 === undefined || this.lastTouch.x1 == this.lastTouch.x2) && (this.lastTouch.y2 === undefined || this.lastTouch.y1 == this.lastTouch.y2) && (!this.lastTouch.target || !$.contains(this.$root[0], this.lastTouch.target))) {
					if (this.hideTimeout) {
						clearTimeout(this.hideTimeout);
						this.hideTimeout = 0;
					}
					// hide with a delay to prevent triggering accidental unwanted click on some page element
					var self = this;
					this.hideTimeout = setTimeout(function() { self.menuHideAll(); }, 350);
				}
				this.lastTouch = null;
			},
			docTouchMove: function(e) {
				if (!this.lastTouch) {
					return;
				}
				var touchPoint = e.originalEvent.touches[0];
				this.lastTouch.x2 = touchPoint.pageX;
				this.lastTouch.y2 = touchPoint.pageY;
			},
			docTouchStart: function(e) {
				var touchPoint = e.originalEvent.touches[0];
				this.lastTouch = { x1: touchPoint.pageX, y1: touchPoint.pageY, target: touchPoint.target };
			},
			enable: function() {
				if (this.disabled) {
					if (this.$disableOverlay) {
						this.$disableOverlay.remove();
						this.$disableOverlay = null;
					}
					this.disabled = false;
				}
			},
			getClosestMenu: function(elm) {
				var $closestMenu = $(elm).closest('ul');
				while ($closestMenu.dataSM('in-mega')) {
					$closestMenu = $closestMenu.parent().closest('ul');
				}
				return $closestMenu[0] || null;
			},
			getHeight: function($elm) {
				return this.getOffset($elm, true);
			},
			// returns precise width/height float values
			getOffset: function($elm, height) {
				var old;
				if ($elm.css('display') == 'none') {
					old = { position: $elm[0].style.position, visibility: $elm[0].style.visibility };
					$elm.css({ position: 'absolute', visibility: 'hidden' }).show();
				}
				var box = $elm[0].getBoundingClientRect && $elm[0].getBoundingClientRect(),
					val = box && (height ? box.height || box.bottom - box.top : box.width || box.right - box.left);
				if (!val && val !== 0) {
					val = height ? $elm[0].offsetHeight : $elm[0].offsetWidth;
				}
				if (old) {
					$elm.hide().css(old);
				}
				return val;
			},
			getStartZIndex: function(root) {
				var zIndex = parseInt(this[root ? '$root' : '$firstSub'].css('z-index'));
				if (!root && isNaN(zIndex)) {
					zIndex = parseInt(this.$root.css('z-index'));
				}
				return !isNaN(zIndex) ? zIndex : 1;
			},
			getTouchPoint: function(e) {
				return e.touches && e.touches[0] || e.changedTouches && e.changedTouches[0] || e;
			},
			getViewport: function(height) {
				var name = height ? 'Height' : 'Width',
					val = document.documentElement['client' + name],
					val2 = window['inner' + name];
				if (val2) {
					val = Math.min(val, val2);
				}
				return val;
			},
			getViewportHeight: function() {
				return this.getViewport(true);
			},
			getViewportWidth: function() {
				return this.getViewport();
			},
			getWidth: function($elm) {
				return this.getOffset($elm);
			},
			handleEvents: function() {
				return !this.disabled && this.isCSSOn();
			},
			handleItemEvents: function($a) {
				return this.handleEvents() && !this.isLinkInMegaMenu($a);
			},
			isCollapsible: function() {
				return this.$firstSub.css('position') == 'static';
			},
			isCSSOn: function() {
				return this.$firstLink.css('display') != 'inline';
			},
			isFixed: function() {
				var isFixed = this.$root.css('position') == 'fixed';
				if (!isFixed) {
					this.$root.parentsUntil('body').each(function() {
						if ($(this).css('position') == 'fixed') {
							isFixed = true;
							return false;
						}
					});
				}
				return isFixed;
			},
			isLinkInMegaMenu: function($a) {
				return $(this.getClosestMenu($a[0])).hasClass('mega-menu');
			},
			isTouchMode: function() {
				return !mouse || this.opts.noMouseOver || this.isCollapsible();
			},
			itemActivate: function($a, hideDeeperSubs) {
				var $ul = $a.closest('ul'),
					level = $ul.dataSM('level');
				// if for some reason the parent item is not activated (e.g. this is an API call to activate the item), activate all parent items first
				if (level > 1 && (!this.activatedItems[level - 2] || this.activatedItems[level - 2][0] != $ul.dataSM('parent-a')[0])) {
					var self = this;
					$($ul.parentsUntil('[data-smartmenus-id]', 'ul').get().reverse()).add($ul).each(function() {
						self.itemActivate($(this).dataSM('parent-a'));
					});
				}
				// hide any visible deeper level sub menus
				if (!this.isCollapsible() || hideDeeperSubs) {
					this.menuHideSubMenus(!this.activatedItems[level - 1] || this.activatedItems[level - 1][0] != $a[0] ? level - 1 : level);
				}
				// save new active item for this level
				this.activatedItems[level - 1] = $a;
				if (this.$root.triggerHandler('activate.smapi', $a[0]) === false) {
					return;
				}
				// show the sub menu if this item has one
				var $sub = $a.dataSM('sub');
				if ($sub && (this.isTouchMode() || (!this.opts.showOnClick || this.clickActivated))) {
					this.menuShow($sub);
				}
			},
			itemBlur: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				this.$root.triggerHandler('blur.smapi', $a[0]);
			},
			itemClick: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				if (this.$touchScrollingSub && this.$touchScrollingSub[0] == $a.closest('ul')[0]) {
					this.$touchScrollingSub = null;
					e.stopPropagation();
					return false;
				}
				if (this.$root.triggerHandler('click.smapi', $a[0]) === false) {
					return false;
				}
				var $sub = $a.dataSM('sub'),
					firstLevelSub = $sub ? $sub.dataSM('level') == 2 : false;
				if ($sub) {
					var subArrowClicked = $(e.target).is('.sub-arrow'),
						collapsible = this.isCollapsible(),
						behaviorToggle = /toggle$/.test(this.opts.collapsibleBehavior),
						behaviorLink = /link$/.test(this.opts.collapsibleBehavior),
						behaviorAccordion = /^accordion/.test(this.opts.collapsibleBehavior);
					// if the sub is hidden, try to show it
					if (!$sub.is(':visible')) {
						if (!behaviorLink || !collapsible || subArrowClicked) {
							if (!collapsible && this.opts.showOnClick && firstLevelSub) {
								this.clickActivated = true;
							}
							// try to activate the item and show the sub
							this.itemActivate($a, behaviorAccordion);
							// if "itemActivate" showed the sub, prevent the click so that the link is not loaded
							// if it couldn't show it, then the sub menus are disabled with an !important declaration (e.g. via mobile styles) so let the link get loaded
							if ($sub.is(':visible')) {
								this.focusActivated = true;
								return false;
							}
						}
					// if the sub is visible and showOnClick: true, hide the sub
					} else if (!collapsible && this.opts.showOnClick && firstLevelSub) {
						this.menuHide($sub);
						this.clickActivated = false;
						this.focusActivated = false;
						return false;
					// if the sub is visible and we are in collapsible mode
					} else if (collapsible && (behaviorToggle || subArrowClicked)) {
						this.itemActivate($a, behaviorAccordion);
						this.menuHide($sub);
						return false;
					}
				}
				if (!collapsible && this.opts.showOnClick && firstLevelSub || $a.hasClass('disabled') || this.$root.triggerHandler('select.smapi', $a[0]) === false) {
					return false;
				}
			},
			itemDown: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				$a.dataSM('mousedown', true);
			},
			itemEnter: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				if (!this.isTouchMode()) {
					if (this.showTimeout) {
						clearTimeout(this.showTimeout);
						this.showTimeout = 0;
					}
					var self = this;
					this.showTimeout = setTimeout(function() { self.itemActivate($a); }, this.opts.showOnClick && $a.closest('ul').dataSM('level') == 1 ? 1 : this.opts.showTimeout);
				}
				this.$root.triggerHandler('mouseenter.smapi', $a[0]);
			},
			itemFocus: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				// fix (the mousedown check): in some browsers a tap/click produces consecutive focus + click events so we don't need to activate the item on focus
				if (this.focusActivated && (!this.isTouchMode() || !$a.dataSM('mousedown')) && (!this.activatedItems.length || this.activatedItems[this.activatedItems.length - 1][0] != $a[0])) {
					this.itemActivate($a, true);
				}
				this.$root.triggerHandler('focus.smapi', $a[0]);
			},
			itemLeave: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				if (!this.isTouchMode()) {
					$a[0].blur();
					if (this.showTimeout) {
						clearTimeout(this.showTimeout);
						this.showTimeout = 0;
					}
				}
				$a.removeDataSM('mousedown');
				this.$root.triggerHandler('mouseleave.smapi', $a[0]);
			},
			menuHide: function($sub) {
				if (this.$root.triggerHandler('beforehide.smapi', $sub[0]) === false) {
					return;
				}
				if (canAnimate) {
					$sub.stop(true, true);
				}
				if ($sub.css('display') != 'none') {
					var complete = function() {
						// unset z-index
						$sub.css('z-index', '');
					};
					// if sub is collapsible (mobile view)
					if (this.isCollapsible()) {
						if (canAnimate && this.opts.collapsibleHideFunction) {
							this.opts.collapsibleHideFunction.call(this, $sub, complete);
						} else {
							$sub.hide(this.opts.collapsibleHideDuration, complete);
						}
					} else {
						if (canAnimate && this.opts.hideFunction) {
							this.opts.hideFunction.call(this, $sub, complete);
						} else {
							$sub.hide(this.opts.hideDuration, complete);
						}
					}
					// deactivate scrolling if it is activated for this sub
					if ($sub.dataSM('scroll')) {
						this.menuScrollStop($sub);
						$sub.css({ 'touch-action': '', '-ms-touch-action': '', '-webkit-transform': '', transform: '' })
							.off('.smartmenus_scroll').removeDataSM('scroll').dataSM('scroll-arrows').hide();
					}
					// unhighlight parent item + accessibility
					$sub.dataSM('parent-a').removeClass('highlighted').attr('aria-expanded', 'false');
					$sub.attr({
						'aria-expanded': 'false',
						'aria-hidden': 'true'
					});
					var level = $sub.dataSM('level');
					this.activatedItems.splice(level - 1, 1);
					this.visibleSubMenus.splice($.inArray($sub, this.visibleSubMenus), 1);
					this.$root.triggerHandler('hide.smapi', $sub[0]);
				}
			},
			menuHideAll: function() {
				if (this.showTimeout) {
					clearTimeout(this.showTimeout);
					this.showTimeout = 0;
				}
				// hide all subs
				// if it's a popup, this.visibleSubMenus[0] is the root UL
				var level = this.opts.isPopup ? 1 : 0;
				for (var i = this.visibleSubMenus.length - 1; i >= level; i--) {
					this.menuHide(this.visibleSubMenus[i]);
				}
				// hide root if it's popup
				if (this.opts.isPopup) {
					if (canAnimate) {
						this.$root.stop(true, true);
					}
					if (this.$root.is(':visible')) {
						if (canAnimate && this.opts.hideFunction) {
							this.opts.hideFunction.call(this, this.$root);
						} else {
							this.$root.hide(this.opts.hideDuration);
						}
					}
				}
				this.activatedItems = [];
				this.visibleSubMenus = [];
				this.clickActivated = false;
				this.focusActivated = false;
				// reset z-index increment
				this.zIndexInc = 0;
				this.$root.triggerHandler('hideAll.smapi');
			},
			menuHideSubMenus: function(level) {
				for (var i = this.activatedItems.length - 1; i >= level; i--) {
					var $sub = this.activatedItems[i].dataSM('sub');
					if ($sub) {
						this.menuHide($sub);
					}
				}
			},
			menuInit: function($ul) {
				if (!$ul.dataSM('in-mega')) {
					// mark UL's in mega drop downs (if any) so we can neglect them
					if ($ul.hasClass('mega-menu')) {
						$ul.find('ul').dataSM('in-mega', true);
					}
					// get level (much faster than, for example, using parentsUntil)
					var level = 2,
						par = $ul[0];
					while ((par = par.parentNode.parentNode) != this.$root[0]) {
						level++;
					}
					// cache stuff for quick access
					var $a = $ul.prevAll('a').eq(-1);
					// if the link is nested (e.g. in a heading)
					if (!$a.length) {
						$a = $ul.prevAll().find('a').eq(-1);
					}
					$a.addClass('has-submenu').dataSM('sub', $ul);
					$ul.dataSM('parent-a', $a)
						.dataSM('level', level)
						.parent().dataSM('sub', $ul);
					// accessibility
					var aId = $a.attr('id') || this.accessIdPrefix + (++this.idInc),
						ulId = $ul.attr('id') || this.accessIdPrefix + (++this.idInc);
					$a.attr({
						id: aId,
						'aria-haspopup': 'true',
						'aria-controls': ulId,
						'aria-expanded': 'false'
					});
					$ul.attr({
						id: ulId,
						'role': 'group',
						'aria-hidden': 'true',
						'aria-labelledby': aId,
						'aria-expanded': 'false'
					});
					// add sub indicator to parent item
					if (this.opts.subIndicators) {
						$a[this.opts.subIndicatorsPos](this.$subArrow.clone());
					}
				}
			},
			menuPosition: function($sub) {
				var $a = $sub.dataSM('parent-a'),
					$li = $a.closest('li'),
					$ul = $li.parent(),
					level = $sub.dataSM('level'),
					subW = this.getWidth($sub),
					subH = this.getHeight($sub),
					itemOffset = $a.offset(),
					itemX = itemOffset.left,
					itemY = itemOffset.top,
					itemW = this.getWidth($a),
					itemH = this.getHeight($a),
					$win = $(window),
					winX = $win.scrollLeft(),
					winY = $win.scrollTop(),
					winW = this.getViewportWidth(),
					winH = this.getViewportHeight(),
					horizontalParent = $ul.parent().is('[data-sm-horizontal-sub]') || level == 2 && !$ul.hasClass('sm-vertical'),
					rightToLeft = this.opts.rightToLeftSubMenus && !$li.is('[data-sm-reverse]') || !this.opts.rightToLeftSubMenus && $li.is('[data-sm-reverse]'),
					subOffsetX = level == 2 ? this.opts.mainMenuSubOffsetX : this.opts.subMenusSubOffsetX,
					subOffsetY = level == 2 ? this.opts.mainMenuSubOffsetY : this.opts.subMenusSubOffsetY,
					x, y;
				if (horizontalParent) {
					x = rightToLeft ? itemW - subW - subOffsetX : subOffsetX;
					y = this.opts.bottomToTopSubMenus ? -subH - subOffsetY : itemH + subOffsetY;
				} else {
					x = rightToLeft ? subOffsetX - subW : itemW - subOffsetX;
					y = this.opts.bottomToTopSubMenus ? itemH - subOffsetY - subH : subOffsetY;
				}
				if (this.opts.keepInViewport) {
					var absX = itemX + x,
						absY = itemY + y;
					if (rightToLeft && absX < winX) {
						x = horizontalParent ? winX - absX + x : itemW - subOffsetX;
					} else if (!rightToLeft && absX + subW > winX + winW) {
						x = horizontalParent ? winX + winW - subW - absX + x : subOffsetX - subW;
					}
					if (!horizontalParent) {
						if (subH < winH && absY + subH > winY + winH) {
							y += winY + winH - subH - absY;
						} else if (subH >= winH || absY < winY) {
							y += winY - absY;
						}
					}
					// do we need scrolling?
					// 0.49 used for better precision when dealing with float values
					if (horizontalParent && (absY + subH > winY + winH + 0.49 || absY < winY) || !horizontalParent && subH > winH + 0.49) {
						var self = this;
						if (!$sub.dataSM('scroll-arrows')) {
							$sub.dataSM('scroll-arrows', $([$('<span class="scroll-up"><span class="scroll-up-arrow"></span></span>')[0], $('<span class="scroll-down"><span class="scroll-down-arrow"></span></span>')[0]])
								.on({
									mouseenter: function() {
										$sub.dataSM('scroll').up = $(this).hasClass('scroll-up');
										self.menuScroll($sub);
									},
									mouseleave: function(e) {
										self.menuScrollStop($sub);
										self.menuScrollOut($sub, e);
									},
									'mousewheel DOMMouseScroll': function(e) { e.preventDefault(); }
								})
								.insertAfter($sub)
							);
						}
						// bind scroll events and save scroll data for this sub
						var eNS = '.smartmenus_scroll';
						$sub.dataSM('scroll', {
								y: this.cssTransforms3d ? 0 : y - itemH,
								step: 1,
								// cache stuff for faster recalcs later
								itemH: itemH,
								subH: subH,
								arrowDownH: this.getHeight($sub.dataSM('scroll-arrows').eq(1))
							})
							.on(getEventsNS({
								'mouseover': function(e) { self.menuScrollOver($sub, e); },
								'mouseout': function(e) { self.menuScrollOut($sub, e); },
								'mousewheel DOMMouseScroll': function(e) { self.menuScrollMousewheel($sub, e); }
							}, eNS))
							.dataSM('scroll-arrows').css({ top: 'auto', left: '0', marginLeft: x + (parseInt($sub.css('border-left-width')) || 0), width: subW - (parseInt($sub.css('border-left-width')) || 0) - (parseInt($sub.css('border-right-width')) || 0), zIndex: $sub.css('z-index') })
								.eq(horizontalParent && this.opts.bottomToTopSubMenus ? 0 : 1).show();
						// when a menu tree is fixed positioned we allow scrolling via touch too
						// since there is no other way to access such long sub menus if no mouse is present
						if (this.isFixed()) {
							var events = {};
							events[touchEvents ? 'touchstart touchmove touchend' : 'pointerdown pointermove pointerup MSPointerDown MSPointerMove MSPointerUp'] = function(e) {
								self.menuScrollTouch($sub, e);
							};
							$sub.css({ 'touch-action': 'none', '-ms-touch-action': 'none' }).on(getEventsNS(events, eNS));
						}
					}
				}
				$sub.css({ top: 'auto', left: '0', marginLeft: x, marginTop: y - itemH });
			},
			menuScroll: function($sub, once, step) {
				var data = $sub.dataSM('scroll'),
					$arrows = $sub.dataSM('scroll-arrows'),
					end = data.up ? data.upEnd : data.downEnd,
					diff;
				if (!once && data.momentum) {
					data.momentum *= 0.92;
					diff = data.momentum;
					if (diff < 0.5) {
						this.menuScrollStop($sub);
						return;
					}
				} else {
					diff = step || (once || !this.opts.scrollAccelerate ? this.opts.scrollStep : Math.floor(data.step));
				}
				// hide any visible deeper level sub menus
				var level = $sub.dataSM('level');
				if (this.activatedItems[level - 1] && this.activatedItems[level - 1].dataSM('sub') && this.activatedItems[level - 1].dataSM('sub').is(':visible')) {
					this.menuHideSubMenus(level - 1);
				}
				data.y = data.up && end <= data.y || !data.up && end >= data.y ? data.y : (Math.abs(end - data.y) > diff ? data.y + (data.up ? diff : -diff) : end);
				$sub.css(this.cssTransforms3d ? { '-webkit-transform': 'translate3d(0, ' + data.y + 'px, 0)', transform: 'translate3d(0, ' + data.y + 'px, 0)' } : { marginTop: data.y });
				// show opposite arrow if appropriate
				if (mouse && (data.up && data.y > data.downEnd || !data.up && data.y < data.upEnd)) {
					$arrows.eq(data.up ? 1 : 0).show();
				}
				// if we've reached the end
				if (data.y == end) {
					if (mouse) {
						$arrows.eq(data.up ? 0 : 1).hide();
					}
					this.menuScrollStop($sub);
				} else if (!once) {
					if (this.opts.scrollAccelerate && data.step < this.opts.scrollStep) {
						data.step += 0.2;
					}
					var self = this;
					this.scrollTimeout = requestAnimationFrame(function() { self.menuScroll($sub); });
				}
			},
			menuScrollMousewheel: function($sub, e) {
				if (this.getClosestMenu(e.target) == $sub[0]) {
					e = e.originalEvent;
					var up = (e.wheelDelta || -e.detail) > 0;
					if ($sub.dataSM('scroll-arrows').eq(up ? 0 : 1).is(':visible')) {
						$sub.dataSM('scroll').up = up;
						this.menuScroll($sub, true);
					}
				}
				e.preventDefault();
			},
			menuScrollOut: function($sub, e) {
				if (mouse) {
					if (!/^scroll-(up|down)/.test((e.relatedTarget || '').className) && ($sub[0] != e.relatedTarget && !$.contains($sub[0], e.relatedTarget) || this.getClosestMenu(e.relatedTarget) != $sub[0])) {
						$sub.dataSM('scroll-arrows').css('visibility', 'hidden');
					}
				}
			},
			menuScrollOver: function($sub, e) {
				if (mouse) {
					if (!/^scroll-(up|down)/.test(e.target.className) && this.getClosestMenu(e.target) == $sub[0]) {
						this.menuScrollRefreshData($sub);
						var data = $sub.dataSM('scroll'),
							upEnd = $(window).scrollTop() - $sub.dataSM('parent-a').offset().top - data.itemH;
						$sub.dataSM('scroll-arrows').eq(0).css('margin-top', upEnd).end()
							.eq(1).css('margin-top', upEnd + this.getViewportHeight() - data.arrowDownH).end()
							.css('visibility', 'visible');
					}
				}
			},
			menuScrollRefreshData: function($sub) {
				var data = $sub.dataSM('scroll'),
					upEnd = $(window).scrollTop() - $sub.dataSM('parent-a').offset().top - data.itemH;
				if (this.cssTransforms3d) {
					upEnd = -(parseFloat($sub.css('margin-top')) - upEnd);
				}
				$.extend(data, {
					upEnd: upEnd,
					downEnd: upEnd + this.getViewportHeight() - data.subH
				});
			},
			menuScrollStop: function($sub) {
				if (this.scrollTimeout) {
					cancelAnimationFrame(this.scrollTimeout);
					this.scrollTimeout = 0;
					$sub.dataSM('scroll').step = 1;
					return true;
				}
			},
			menuScrollTouch: function($sub, e) {
				e = e.originalEvent;
				if (isTouchEvent(e)) {
					var touchPoint = this.getTouchPoint(e);
					// neglect event if we touched a visible deeper level sub menu
					if (this.getClosestMenu(touchPoint.target) == $sub[0]) {
						var data = $sub.dataSM('scroll');
						if (/(start|down)$/i.test(e.type)) {
							if (this.menuScrollStop($sub)) {
								// if we were scrolling, just stop and don't activate any link on the first touch
								e.preventDefault();
								this.$touchScrollingSub = $sub;
							} else {
								this.$touchScrollingSub = null;
							}
							// update scroll data since the user might have zoomed, etc.
							this.menuScrollRefreshData($sub);
							// extend it with the touch properties
							$.extend(data, {
								touchStartY: touchPoint.pageY,
								touchStartTime: e.timeStamp
							});
						} else if (/move$/i.test(e.type)) {
							var prevY = data.touchY !== undefined ? data.touchY : data.touchStartY;
							if (prevY !== undefined && prevY != touchPoint.pageY) {
								this.$touchScrollingSub = $sub;
								var up = prevY < touchPoint.pageY;
								// changed direction? reset...
								if (data.up !== undefined && data.up != up) {
									$.extend(data, {
										touchStartY: touchPoint.pageY,
										touchStartTime: e.timeStamp
									});
								}
								$.extend(data, {
									up: up,
									touchY: touchPoint.pageY
								});
								this.menuScroll($sub, true, Math.abs(touchPoint.pageY - prevY));
							}
							e.preventDefault();
						} else { // touchend/pointerup
							if (data.touchY !== undefined) {
								if (data.momentum = Math.pow(Math.abs(touchPoint.pageY - data.touchStartY) / (e.timeStamp - data.touchStartTime), 2) * 15) {
									this.menuScrollStop($sub);
									this.menuScroll($sub);
									e.preventDefault();
								}
								delete data.touchY;
							}
						}
					}
				}
			},
			menuShow: function($sub) {
				if (!$sub.dataSM('beforefirstshowfired')) {
					$sub.dataSM('beforefirstshowfired', true);
					if (this.$root.triggerHandler('beforefirstshow.smapi', $sub[0]) === false) {
						return;
					}
				}
				if (this.$root.triggerHandler('beforeshow.smapi', $sub[0]) === false) {
					return;
				}
				$sub.dataSM('shown-before', true);
				if (canAnimate) {
					$sub.stop(true, true);
				}
				if (!$sub.is(':visible')) {
					// highlight parent item
					var $a = $sub.dataSM('parent-a'),
						collapsible = this.isCollapsible();
					if (this.opts.keepHighlighted || collapsible) {
						$a.addClass('highlighted');
					}
					if (collapsible) {
						$sub.removeClass('sm-nowrap').css({ zIndex: '', width: 'auto', minWidth: '', maxWidth: '', top: '', left: '', marginLeft: '', marginTop: '' });
					} else {
						// set z-index
						$sub.css('z-index', this.zIndexInc = (this.zIndexInc || this.getStartZIndex()) + 1);
						// min/max-width fix - no way to rely purely on CSS as all UL's are nested
						if (this.opts.subMenusMinWidth || this.opts.subMenusMaxWidth) {
							$sub.css({ width: 'auto', minWidth: '', maxWidth: '' }).addClass('sm-nowrap');
							if (this.opts.subMenusMinWidth) {
							 	$sub.css('min-width', this.opts.subMenusMinWidth);
							}
							if (this.opts.subMenusMaxWidth) {
							 	var noMaxWidth = this.getWidth($sub);
							 	$sub.css('max-width', this.opts.subMenusMaxWidth);
								if (noMaxWidth > this.getWidth($sub)) {
									$sub.removeClass('sm-nowrap').css('width', this.opts.subMenusMaxWidth);
								}
							}
						}
						this.menuPosition($sub);
					}
					var complete = function() {
						// fix: "overflow: hidden;" is not reset on animation complete in jQuery < 1.9.0 in Chrome when global "box-sizing: border-box;" is used
						$sub.css('overflow', '');
					};
					// if sub is collapsible (mobile view)
					if (collapsible) {
						if (canAnimate && this.opts.collapsibleShowFunction) {
							this.opts.collapsibleShowFunction.call(this, $sub, complete);
						} else {
							$sub.show(this.opts.collapsibleShowDuration, complete);
						}
					} else {
						if (canAnimate && this.opts.showFunction) {
							this.opts.showFunction.call(this, $sub, complete);
						} else {
							$sub.show(this.opts.showDuration, complete);
						}
					}
					// accessibility
					$a.attr('aria-expanded', 'true');
					$sub.attr({
						'aria-expanded': 'true',
						'aria-hidden': 'false'
					});
					// store sub menu in visible array
					this.visibleSubMenus.push($sub);
					this.$root.triggerHandler('show.smapi', $sub[0]);
				}
			},
			popupHide: function(noHideTimeout) {
				if (this.hideTimeout) {
					clearTimeout(this.hideTimeout);
					this.hideTimeout = 0;
				}
				var self = this;
				this.hideTimeout = setTimeout(function() {
					self.menuHideAll();
				}, noHideTimeout ? 1 : this.opts.hideTimeout);
			},
			popupShow: function(left, top) {
				if (!this.opts.isPopup) {
					alert('SmartMenus jQuery Error:\n\nIf you want to show this menu via the "popupShow" method, set the isPopup:true option.');
					return;
				}
				if (this.hideTimeout) {
					clearTimeout(this.hideTimeout);
					this.hideTimeout = 0;
				}
				this.$root.dataSM('shown-before', true);
				if (canAnimate) {
					this.$root.stop(true, true);
				}
				if (!this.$root.is(':visible')) {
					this.$root.css({ left: left, top: top });
					// show menu
					var self = this,
						complete = function() {
							self.$root.css('overflow', '');
						};
					if (canAnimate && this.opts.showFunction) {
						this.opts.showFunction.call(this, this.$root, complete);
					} else {
						this.$root.show(this.opts.showDuration, complete);
					}
					this.visibleSubMenus[0] = this.$root;
				}
			},
			refresh: function() {
				this.destroy(true);
				this.init(true);
			},
			rootKeyDown: function(e) {
				if (!this.handleEvents()) {
					return;
				}
				switch (e.keyCode) {
					case 27: // reset on Esc
						var $activeTopItem = this.activatedItems[0];
						if ($activeTopItem) {
							this.menuHideAll();
							$activeTopItem[0].focus();
							var $sub = $activeTopItem.dataSM('sub');
							if ($sub) {
								this.menuHide($sub);
							}
						}
						break;
					case 32: // activate item's sub on Space
						var $target = $(e.target);
						if ($target.is('a') && this.handleItemEvents($target)) {
							var $sub = $target.dataSM('sub');
							if ($sub && !$sub.is(':visible')) {
								this.itemClick({ currentTarget: e.target });
								e.preventDefault();
							}
						}
						break;
				}
			},
			rootOut: function(e) {
				if (!this.handleEvents() || this.isTouchMode() || e.target == this.$root[0]) {
					return;
				}
				if (this.hideTimeout) {
					clearTimeout(this.hideTimeout);
					this.hideTimeout = 0;
				}
				if (!this.opts.showOnClick || !this.opts.hideOnClick) {
					var self = this;
					this.hideTimeout = setTimeout(function() { self.menuHideAll(); }, this.opts.hideTimeout);
				}
			},
			rootOver: function(e) {
				if (!this.handleEvents() || this.isTouchMode() || e.target == this.$root[0]) {
					return;
				}
				if (this.hideTimeout) {
					clearTimeout(this.hideTimeout);
					this.hideTimeout = 0;
				}
			},
			winResize: function(e) {
				if (!this.handleEvents()) {
					// we still need to resize the disable overlay if it's visible
					if (this.$disableOverlay) {
						var pos = this.$root.offset();
	 					this.$disableOverlay.css({
							top: pos.top,
							left: pos.left,
							width: this.$root.outerWidth(),
							height: this.$root.outerHeight()
						});
					}
					return;
				}
				// hide sub menus on resize - on mobile do it only on orientation change
				if (!('onorientationchange' in window) || e.type == 'orientationchange') {
					var collapsible = this.isCollapsible();
					// if it was collapsible before resize and still is, don't do it
					if (!(this.wasCollapsible && collapsible)) { 
						if (this.activatedItems.length) {
							this.activatedItems[this.activatedItems.length - 1][0].blur();
						}
						this.menuHideAll();
					}
					this.wasCollapsible = collapsible;
				}
			}
		}
	});

	$.fn.dataSM = function(key, val) {
		if (val) {
			return this.data(key + '_smartmenus', val);
		}
		return this.data(key + '_smartmenus');
	};

	$.fn.removeDataSM = function(key) {
		return this.removeData(key + '_smartmenus');
	};

	$.fn.smartmenus = function(options) {
		if (typeof options == 'string') {
			var args = arguments,
				method = options;
			Array.prototype.shift.call(args);
			return this.each(function() {
				var smartmenus = $(this).data('smartmenus');
				if (smartmenus && smartmenus[method]) {
					smartmenus[method].apply(smartmenus, args);
				}
			});
		}
		return this.each(function() {
			// [data-sm-options] attribute on the root UL
			var dataOpts = $(this).data('sm-options') || null;
			if (dataOpts && typeof dataOpts !== 'object') {
				try {
					dataOpts = eval('(' + dataOpts + ')');
				} catch(e) {
					dataOpts = null;
					alert('ERROR\n\nSmartMenus jQuery init:\nInvalid "data-sm-options" attribute value syntax.');
				};
			}
			new $.SmartMenus(this, $.extend({}, $.fn.smartmenus.defaults, options, dataOpts));
		});
	};

	// default settings
	$.fn.smartmenus.defaults = {
		isPopup:		false,		// is this a popup menu (can be shown via the popupShow/popupHide methods) or a permanent menu bar
		mainMenuSubOffsetX:	0,		// pixels offset from default position
		mainMenuSubOffsetY:	0,		// pixels offset from default position
		subMenusSubOffsetX:	0,		// pixels offset from default position
		subMenusSubOffsetY:	0,		// pixels offset from default position
		subMenusMinWidth:	'10em',		// min-width for the sub menus (any CSS unit) - if set, the fixed width set in CSS will be ignored
		subMenusMaxWidth:	'20em',		// max-width for the sub menus (any CSS unit) - if set, the fixed width set in CSS will be ignored
		subIndicators: 		true,		// create sub menu indicators - creates a SPAN and inserts it in the A
		subIndicatorsPos: 	'append',	// position of the SPAN relative to the menu item content ('append', 'prepend')
		subIndicatorsText:	'',		// [optionally] add text in the SPAN (e.g. '+') (you may want to check the CSS for the sub indicators too)
		scrollStep: 		30,		// pixels step when scrolling long sub menus that do not fit in the viewport height
		scrollAccelerate:	true,		// accelerate scrolling or use a fixed step
		showTimeout:		250,		// timeout before showing the sub menus
		hideTimeout:		500,		// timeout before hiding the sub menus
		showDuration:		0,		// duration for show animation - set to 0 for no animation - matters only if showFunction:null
		showFunction:		null,		// custom function to use when showing a sub menu (the default is the jQuery 'show')
							// don't forget to call complete() at the end of whatever you do
							// e.g.: function($ul, complete) { $ul.fadeIn(250, complete); }
		hideDuration:		0,		// duration for hide animation - set to 0 for no animation - matters only if hideFunction:null
		hideFunction:		function($ul, complete) { $ul.fadeOut(200, complete); },	// custom function to use when hiding a sub menu (the default is the jQuery 'hide')
							// don't forget to call complete() at the end of whatever you do
							// e.g.: function($ul, complete) { $ul.fadeOut(250, complete); }
		collapsibleShowDuration:0,		// duration for show animation for collapsible sub menus - matters only if collapsibleShowFunction:null
		collapsibleShowFunction:function($ul, complete) { $ul.slideDown(200, complete); },	// custom function to use when showing a collapsible sub menu
							// (i.e. when mobile styles are used to make the sub menus collapsible)
		collapsibleHideDuration:0,		// duration for hide animation for collapsible sub menus - matters only if collapsibleHideFunction:null
		collapsibleHideFunction:function($ul, complete) { $ul.slideUp(200, complete); },	// custom function to use when hiding a collapsible sub menu
							// (i.e. when mobile styles are used to make the sub menus collapsible)
		showOnClick:		false,		// show the first-level sub menus onclick instead of onmouseover (i.e. mimic desktop app menus) (matters only for mouse input)
		hideOnClick:		true,		// hide the sub menus on click/tap anywhere on the page
		noMouseOver:		false,		// disable sub menus activation onmouseover (i.e. behave like in touch mode - use just mouse clicks) (matters only for mouse input)
		keepInViewport:		true,		// reposition the sub menus if needed to make sure they always appear inside the viewport
		keepHighlighted:	true,		// keep all ancestor items of the current sub menu highlighted (adds the 'highlighted' class to the A's)
		markCurrentItem:	false,		// automatically add the 'current' class to the A element of the item linking to the current URL
		markCurrentTree:	true,		// add the 'current' class also to the A elements of all ancestor items of the current item
		rightToLeftSubMenus:	false,		// right to left display of the sub menus (check the CSS for the sub indicators' position)
		bottomToTopSubMenus:	false,		// bottom to top display of the sub menus
		collapsibleBehavior:	'default'	// parent items behavior in collapsible (mobile) view ('default', 'toggle', 'link', 'accordion', 'accordion-toggle', 'accordion-link')
							// 'default' - first tap on parent item expands sub, second tap loads its link
							// 'toggle' - the whole parent item acts just as a toggle button for its sub menu (expands/collapses on each tap)
							// 'link' - the parent item acts as a regular item (first tap loads its link), the sub menu can be expanded only via the +/- button
							// 'accordion' - like 'default' but on expand also resets any visible sub menus from deeper levels or other branches
							// 'accordion-toggle' - like 'toggle' but on expand also resets any visible sub menus from deeper levels or other branches
							// 'accordion-link' - like 'link' but on expand also resets any visible sub menus from deeper levels or other branches
	};

	return $;
}));

/***/ }),

/***/ "./node_modules/smartmenus/dist/jquery.smartmenus.js":
/*!***********************************************************!*\
  !*** ./node_modules/smartmenus/dist/jquery.smartmenus.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * SmartMenus jQuery Plugin - v1.1.0 - September 17, 2017
 * http://www.smartmenus.org/
 *
 * Copyright Vasil Dinkov, Vadikom Web Ltd.
 * http://vadikom.com
 *
 * Licensed MIT
 */

(function(factory) {
	if (true) {
		// AMD
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}
} (function($) {

	var menuTrees = [],
		mouse = false, // optimize for touch by default - we will detect for mouse input
		touchEvents = 'ontouchstart' in window, // we use this just to choose between toucn and pointer events, not for touch screen detection
		mouseDetectionEnabled = false,
		requestAnimationFrame = window.requestAnimationFrame || function(callback) { return setTimeout(callback, 1000 / 60); },
		cancelAnimationFrame = window.cancelAnimationFrame || function(id) { clearTimeout(id); },
		canAnimate = !!$.fn.animate;

	// Handle detection for mouse input (i.e. desktop browsers, tablets with a mouse, etc.)
	function initMouseDetection(disable) {
		var eNS = '.smartmenus_mouse';
		if (!mouseDetectionEnabled && !disable) {
			// if we get two consecutive mousemoves within 2 pixels from each other and within 300ms, we assume a real mouse/cursor is present
			// in practice, this seems like impossible to trick unintentianally with a real mouse and a pretty safe detection on touch devices (even with older browsers that do not support touch events)
			var firstTime = true,
				lastMove = null,
				events = {
					'mousemove': function(e) {
						var thisMove = { x: e.pageX, y: e.pageY, timeStamp: new Date().getTime() };
						if (lastMove) {
							var deltaX = Math.abs(lastMove.x - thisMove.x),
								deltaY = Math.abs(lastMove.y - thisMove.y);
		 					if ((deltaX > 0 || deltaY > 0) && deltaX <= 2 && deltaY <= 2 && thisMove.timeStamp - lastMove.timeStamp <= 300) {
								mouse = true;
								// if this is the first check after page load, check if we are not over some item by chance and call the mouseenter handler if yes
								if (firstTime) {
									var $a = $(e.target).closest('a');
									if ($a.is('a')) {
										$.each(menuTrees, function() {
											if ($.contains(this.$root[0], $a[0])) {
												this.itemEnter({ currentTarget: $a[0] });
												return false;
											}
										});
									}
									firstTime = false;
								}
							}
						}
						lastMove = thisMove;
					}
				};
			events[touchEvents ? 'touchstart' : 'pointerover pointermove pointerout MSPointerOver MSPointerMove MSPointerOut'] = function(e) {
				if (isTouchEvent(e.originalEvent)) {
					mouse = false;
				}
			};
			$(document).on(getEventsNS(events, eNS));
			mouseDetectionEnabled = true;
		} else if (mouseDetectionEnabled && disable) {
			$(document).off(eNS);
			mouseDetectionEnabled = false;
		}
	}

	function isTouchEvent(e) {
		return !/^(4|mouse)$/.test(e.pointerType);
	}

	// returns a jQuery on() ready object
	function getEventsNS(events, eNS) {
		if (!eNS) {
			eNS = '';
		}
		var eventsNS = {};
		for (var i in events) {
			eventsNS[i.split(' ').join(eNS + ' ') + eNS] = events[i];
		}
		return eventsNS;
	}

	$.SmartMenus = function(elm, options) {
		this.$root = $(elm);
		this.opts = options;
		this.rootId = ''; // internal
		this.accessIdPrefix = '';
		this.$subArrow = null;
		this.activatedItems = []; // stores last activated A's for each level
		this.visibleSubMenus = []; // stores visible sub menus UL's (might be in no particular order)
		this.showTimeout = 0;
		this.hideTimeout = 0;
		this.scrollTimeout = 0;
		this.clickActivated = false;
		this.focusActivated = false;
		this.zIndexInc = 0;
		this.idInc = 0;
		this.$firstLink = null; // we'll use these for some tests
		this.$firstSub = null; // at runtime so we'll cache them
		this.disabled = false;
		this.$disableOverlay = null;
		this.$touchScrollingSub = null;
		this.cssTransforms3d = 'perspective' in elm.style || 'webkitPerspective' in elm.style;
		this.wasCollapsible = false;
		this.init();
	};

	$.extend($.SmartMenus, {
		hideAll: function() {
			$.each(menuTrees, function() {
				this.menuHideAll();
			});
		},
		destroy: function() {
			while (menuTrees.length) {
				menuTrees[0].destroy();
			}
			initMouseDetection(true);
		},
		prototype: {
			init: function(refresh) {
				var self = this;

				if (!refresh) {
					menuTrees.push(this);

					this.rootId = (new Date().getTime() + Math.random() + '').replace(/\D/g, '');
					this.accessIdPrefix = 'sm-' + this.rootId + '-';

					if (this.$root.hasClass('sm-rtl')) {
						this.opts.rightToLeftSubMenus = true;
					}

					// init root (main menu)
					var eNS = '.smartmenus';
					this.$root
						.data('smartmenus', this)
						.attr('data-smartmenus-id', this.rootId)
						.dataSM('level', 1)
						.on(getEventsNS({
							'mouseover focusin': $.proxy(this.rootOver, this),
							'mouseout focusout': $.proxy(this.rootOut, this),
							'keydown': $.proxy(this.rootKeyDown, this)
						}, eNS))
						.on(getEventsNS({
							'mouseenter': $.proxy(this.itemEnter, this),
							'mouseleave': $.proxy(this.itemLeave, this),
							'mousedown': $.proxy(this.itemDown, this),
							'focus': $.proxy(this.itemFocus, this),
							'blur': $.proxy(this.itemBlur, this),
							'click': $.proxy(this.itemClick, this)
						}, eNS), 'a');

					// hide menus on tap or click outside the root UL
					eNS += this.rootId;
					if (this.opts.hideOnClick) {
						$(document).on(getEventsNS({
							'touchstart': $.proxy(this.docTouchStart, this),
							'touchmove': $.proxy(this.docTouchMove, this),
							'touchend': $.proxy(this.docTouchEnd, this),
							// for Opera Mobile < 11.5, webOS browser, etc. we'll check click too
							'click': $.proxy(this.docClick, this)
						}, eNS));
					}
					// hide sub menus on resize
					$(window).on(getEventsNS({ 'resize orientationchange': $.proxy(this.winResize, this) }, eNS));

					if (this.opts.subIndicators) {
						this.$subArrow = $('<span/>').addClass('sub-arrow');
						if (this.opts.subIndicatorsText) {
							this.$subArrow.html(this.opts.subIndicatorsText);
						}
					}

					// make sure mouse detection is enabled
					initMouseDetection();
				}

				// init sub menus
				this.$firstSub = this.$root.find('ul').each(function() { self.menuInit($(this)); }).eq(0);

				this.$firstLink = this.$root.find('a').eq(0);

				// find current item
				if (this.opts.markCurrentItem) {
					var reDefaultDoc = /(index|default)\.[^#\?\/]*/i,
						reHash = /#.*/,
						locHref = window.location.href.replace(reDefaultDoc, ''),
						locHrefNoHash = locHref.replace(reHash, '');
					this.$root.find('a').each(function() {
						var href = this.href.replace(reDefaultDoc, ''),
							$this = $(this);
						if (href == locHref || href == locHrefNoHash) {
							$this.addClass('current');
							if (self.opts.markCurrentTree) {
								$this.parentsUntil('[data-smartmenus-id]', 'ul').each(function() {
									$(this).dataSM('parent-a').addClass('current');
								});
							}
						}
					});
				}

				// save initial state
				this.wasCollapsible = this.isCollapsible();
			},
			destroy: function(refresh) {
				if (!refresh) {
					var eNS = '.smartmenus';
					this.$root
						.removeData('smartmenus')
						.removeAttr('data-smartmenus-id')
						.removeDataSM('level')
						.off(eNS);
					eNS += this.rootId;
					$(document).off(eNS);
					$(window).off(eNS);
					if (this.opts.subIndicators) {
						this.$subArrow = null;
					}
				}
				this.menuHideAll();
				var self = this;
				this.$root.find('ul').each(function() {
						var $this = $(this);
						if ($this.dataSM('scroll-arrows')) {
							$this.dataSM('scroll-arrows').remove();
						}
						if ($this.dataSM('shown-before')) {
							if (self.opts.subMenusMinWidth || self.opts.subMenusMaxWidth) {
								$this.css({ width: '', minWidth: '', maxWidth: '' }).removeClass('sm-nowrap');
							}
							if ($this.dataSM('scroll-arrows')) {
								$this.dataSM('scroll-arrows').remove();
							}
							$this.css({ zIndex: '', top: '', left: '', marginLeft: '', marginTop: '', display: '' });
						}
						if (($this.attr('id') || '').indexOf(self.accessIdPrefix) == 0) {
							$this.removeAttr('id');
						}
					})
					.removeDataSM('in-mega')
					.removeDataSM('shown-before')
					.removeDataSM('scroll-arrows')
					.removeDataSM('parent-a')
					.removeDataSM('level')
					.removeDataSM('beforefirstshowfired')
					.removeAttr('role')
					.removeAttr('aria-hidden')
					.removeAttr('aria-labelledby')
					.removeAttr('aria-expanded');
				this.$root.find('a.has-submenu').each(function() {
						var $this = $(this);
						if ($this.attr('id').indexOf(self.accessIdPrefix) == 0) {
							$this.removeAttr('id');
						}
					})
					.removeClass('has-submenu')
					.removeDataSM('sub')
					.removeAttr('aria-haspopup')
					.removeAttr('aria-controls')
					.removeAttr('aria-expanded')
					.closest('li').removeDataSM('sub');
				if (this.opts.subIndicators) {
					this.$root.find('span.sub-arrow').remove();
				}
				if (this.opts.markCurrentItem) {
					this.$root.find('a.current').removeClass('current');
				}
				if (!refresh) {
					this.$root = null;
					this.$firstLink = null;
					this.$firstSub = null;
					if (this.$disableOverlay) {
						this.$disableOverlay.remove();
						this.$disableOverlay = null;
					}
					menuTrees.splice($.inArray(this, menuTrees), 1);
				}
			},
			disable: function(noOverlay) {
				if (!this.disabled) {
					this.menuHideAll();
					// display overlay over the menu to prevent interaction
					if (!noOverlay && !this.opts.isPopup && this.$root.is(':visible')) {
						var pos = this.$root.offset();
						this.$disableOverlay = $('<div class="sm-jquery-disable-overlay"/>').css({
							position: 'absolute',
							top: pos.top,
							left: pos.left,
							width: this.$root.outerWidth(),
							height: this.$root.outerHeight(),
							zIndex: this.getStartZIndex(true),
							opacity: 0
						}).appendTo(document.body);
					}
					this.disabled = true;
				}
			},
			docClick: function(e) {
				if (this.$touchScrollingSub) {
					this.$touchScrollingSub = null;
					return;
				}
				// hide on any click outside the menu or on a menu link
				if (this.visibleSubMenus.length && !$.contains(this.$root[0], e.target) || $(e.target).closest('a').length) {
					this.menuHideAll();
				}
			},
			docTouchEnd: function(e) {
				if (!this.lastTouch) {
					return;
				}
				if (this.visibleSubMenus.length && (this.lastTouch.x2 === undefined || this.lastTouch.x1 == this.lastTouch.x2) && (this.lastTouch.y2 === undefined || this.lastTouch.y1 == this.lastTouch.y2) && (!this.lastTouch.target || !$.contains(this.$root[0], this.lastTouch.target))) {
					if (this.hideTimeout) {
						clearTimeout(this.hideTimeout);
						this.hideTimeout = 0;
					}
					// hide with a delay to prevent triggering accidental unwanted click on some page element
					var self = this;
					this.hideTimeout = setTimeout(function() { self.menuHideAll(); }, 350);
				}
				this.lastTouch = null;
			},
			docTouchMove: function(e) {
				if (!this.lastTouch) {
					return;
				}
				var touchPoint = e.originalEvent.touches[0];
				this.lastTouch.x2 = touchPoint.pageX;
				this.lastTouch.y2 = touchPoint.pageY;
			},
			docTouchStart: function(e) {
				var touchPoint = e.originalEvent.touches[0];
				this.lastTouch = { x1: touchPoint.pageX, y1: touchPoint.pageY, target: touchPoint.target };
			},
			enable: function() {
				if (this.disabled) {
					if (this.$disableOverlay) {
						this.$disableOverlay.remove();
						this.$disableOverlay = null;
					}
					this.disabled = false;
				}
			},
			getClosestMenu: function(elm) {
				var $closestMenu = $(elm).closest('ul');
				while ($closestMenu.dataSM('in-mega')) {
					$closestMenu = $closestMenu.parent().closest('ul');
				}
				return $closestMenu[0] || null;
			},
			getHeight: function($elm) {
				return this.getOffset($elm, true);
			},
			// returns precise width/height float values
			getOffset: function($elm, height) {
				var old;
				if ($elm.css('display') == 'none') {
					old = { position: $elm[0].style.position, visibility: $elm[0].style.visibility };
					$elm.css({ position: 'absolute', visibility: 'hidden' }).show();
				}
				var box = $elm[0].getBoundingClientRect && $elm[0].getBoundingClientRect(),
					val = box && (height ? box.height || box.bottom - box.top : box.width || box.right - box.left);
				if (!val && val !== 0) {
					val = height ? $elm[0].offsetHeight : $elm[0].offsetWidth;
				}
				if (old) {
					$elm.hide().css(old);
				}
				return val;
			},
			getStartZIndex: function(root) {
				var zIndex = parseInt(this[root ? '$root' : '$firstSub'].css('z-index'));
				if (!root && isNaN(zIndex)) {
					zIndex = parseInt(this.$root.css('z-index'));
				}
				return !isNaN(zIndex) ? zIndex : 1;
			},
			getTouchPoint: function(e) {
				return e.touches && e.touches[0] || e.changedTouches && e.changedTouches[0] || e;
			},
			getViewport: function(height) {
				var name = height ? 'Height' : 'Width',
					val = document.documentElement['client' + name],
					val2 = window['inner' + name];
				if (val2) {
					val = Math.min(val, val2);
				}
				return val;
			},
			getViewportHeight: function() {
				return this.getViewport(true);
			},
			getViewportWidth: function() {
				return this.getViewport();
			},
			getWidth: function($elm) {
				return this.getOffset($elm);
			},
			handleEvents: function() {
				return !this.disabled && this.isCSSOn();
			},
			handleItemEvents: function($a) {
				return this.handleEvents() && !this.isLinkInMegaMenu($a);
			},
			isCollapsible: function() {
				return this.$firstSub.css('position') == 'static';
			},
			isCSSOn: function() {
				return this.$firstLink.css('display') != 'inline';
			},
			isFixed: function() {
				var isFixed = this.$root.css('position') == 'fixed';
				if (!isFixed) {
					this.$root.parentsUntil('body').each(function() {
						if ($(this).css('position') == 'fixed') {
							isFixed = true;
							return false;
						}
					});
				}
				return isFixed;
			},
			isLinkInMegaMenu: function($a) {
				return $(this.getClosestMenu($a[0])).hasClass('mega-menu');
			},
			isTouchMode: function() {
				return !mouse || this.opts.noMouseOver || this.isCollapsible();
			},
			itemActivate: function($a, hideDeeperSubs) {
				var $ul = $a.closest('ul'),
					level = $ul.dataSM('level');
				// if for some reason the parent item is not activated (e.g. this is an API call to activate the item), activate all parent items first
				if (level > 1 && (!this.activatedItems[level - 2] || this.activatedItems[level - 2][0] != $ul.dataSM('parent-a')[0])) {
					var self = this;
					$($ul.parentsUntil('[data-smartmenus-id]', 'ul').get().reverse()).add($ul).each(function() {
						self.itemActivate($(this).dataSM('parent-a'));
					});
				}
				// hide any visible deeper level sub menus
				if (!this.isCollapsible() || hideDeeperSubs) {
					this.menuHideSubMenus(!this.activatedItems[level - 1] || this.activatedItems[level - 1][0] != $a[0] ? level - 1 : level);
				}
				// save new active item for this level
				this.activatedItems[level - 1] = $a;
				if (this.$root.triggerHandler('activate.smapi', $a[0]) === false) {
					return;
				}
				// show the sub menu if this item has one
				var $sub = $a.dataSM('sub');
				if ($sub && (this.isTouchMode() || (!this.opts.showOnClick || this.clickActivated))) {
					this.menuShow($sub);
				}
			},
			itemBlur: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				this.$root.triggerHandler('blur.smapi', $a[0]);
			},
			itemClick: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				if (this.$touchScrollingSub && this.$touchScrollingSub[0] == $a.closest('ul')[0]) {
					this.$touchScrollingSub = null;
					e.stopPropagation();
					return false;
				}
				if (this.$root.triggerHandler('click.smapi', $a[0]) === false) {
					return false;
				}
				var subArrowClicked = $(e.target).is('.sub-arrow'),
					$sub = $a.dataSM('sub'),
					firstLevelSub = $sub ? $sub.dataSM('level') == 2 : false,
					collapsible = this.isCollapsible(),
					behaviorToggle = /toggle$/.test(this.opts.collapsibleBehavior),
					behaviorLink = /link$/.test(this.opts.collapsibleBehavior),
					behaviorAccordion = /^accordion/.test(this.opts.collapsibleBehavior);
				// if the sub is hidden, try to show it
				if ($sub && !$sub.is(':visible')) {
					if (!behaviorLink || !collapsible || subArrowClicked) {
						if (this.opts.showOnClick && firstLevelSub) {
							this.clickActivated = true;
						}
						// try to activate the item and show the sub
						this.itemActivate($a, behaviorAccordion);
						// if "itemActivate" showed the sub, prevent the click so that the link is not loaded
						// if it couldn't show it, then the sub menus are disabled with an !important declaration (e.g. via mobile styles) so let the link get loaded
						if ($sub.is(':visible')) {
							this.focusActivated = true;
							return false;
						}
					}
				// if the sub is visible and we are in collapsible mode
				} else if (collapsible && (behaviorToggle || subArrowClicked)) {
					this.itemActivate($a, behaviorAccordion);
					this.menuHide($sub);
					if (behaviorToggle) {
						this.focusActivated = false;
					}
					return false;
				}
				if (this.opts.showOnClick && firstLevelSub || $a.hasClass('disabled') || this.$root.triggerHandler('select.smapi', $a[0]) === false) {
					return false;
				}
			},
			itemDown: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				$a.dataSM('mousedown', true);
			},
			itemEnter: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				if (!this.isTouchMode()) {
					if (this.showTimeout) {
						clearTimeout(this.showTimeout);
						this.showTimeout = 0;
					}
					var self = this;
					this.showTimeout = setTimeout(function() { self.itemActivate($a); }, this.opts.showOnClick && $a.closest('ul').dataSM('level') == 1 ? 1 : this.opts.showTimeout);
				}
				this.$root.triggerHandler('mouseenter.smapi', $a[0]);
			},
			itemFocus: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				// fix (the mousedown check): in some browsers a tap/click produces consecutive focus + click events so we don't need to activate the item on focus
				if (this.focusActivated && (!this.isTouchMode() || !$a.dataSM('mousedown')) && (!this.activatedItems.length || this.activatedItems[this.activatedItems.length - 1][0] != $a[0])) {
					this.itemActivate($a, true);
				}
				this.$root.triggerHandler('focus.smapi', $a[0]);
			},
			itemLeave: function(e) {
				var $a = $(e.currentTarget);
				if (!this.handleItemEvents($a)) {
					return;
				}
				if (!this.isTouchMode()) {
					$a[0].blur();
					if (this.showTimeout) {
						clearTimeout(this.showTimeout);
						this.showTimeout = 0;
					}
				}
				$a.removeDataSM('mousedown');
				this.$root.triggerHandler('mouseleave.smapi', $a[0]);
			},
			menuHide: function($sub) {
				if (this.$root.triggerHandler('beforehide.smapi', $sub[0]) === false) {
					return;
				}
				if (canAnimate) {
					$sub.stop(true, true);
				}
				if ($sub.css('display') != 'none') {
					var complete = function() {
						// unset z-index
						$sub.css('z-index', '');
					};
					// if sub is collapsible (mobile view)
					if (this.isCollapsible()) {
						if (canAnimate && this.opts.collapsibleHideFunction) {
							this.opts.collapsibleHideFunction.call(this, $sub, complete);
						} else {
							$sub.hide(this.opts.collapsibleHideDuration, complete);
						}
					} else {
						if (canAnimate && this.opts.hideFunction) {
							this.opts.hideFunction.call(this, $sub, complete);
						} else {
							$sub.hide(this.opts.hideDuration, complete);
						}
					}
					// deactivate scrolling if it is activated for this sub
					if ($sub.dataSM('scroll')) {
						this.menuScrollStop($sub);
						$sub.css({ 'touch-action': '', '-ms-touch-action': '', '-webkit-transform': '', transform: '' })
							.off('.smartmenus_scroll').removeDataSM('scroll').dataSM('scroll-arrows').hide();
					}
					// unhighlight parent item + accessibility
					$sub.dataSM('parent-a').removeClass('highlighted').attr('aria-expanded', 'false');
					$sub.attr({
						'aria-expanded': 'false',
						'aria-hidden': 'true'
					});
					var level = $sub.dataSM('level');
					this.activatedItems.splice(level - 1, 1);
					this.visibleSubMenus.splice($.inArray($sub, this.visibleSubMenus), 1);
					this.$root.triggerHandler('hide.smapi', $sub[0]);
				}
			},
			menuHideAll: function() {
				if (this.showTimeout) {
					clearTimeout(this.showTimeout);
					this.showTimeout = 0;
				}
				// hide all subs
				// if it's a popup, this.visibleSubMenus[0] is the root UL
				var level = this.opts.isPopup ? 1 : 0;
				for (var i = this.visibleSubMenus.length - 1; i >= level; i--) {
					this.menuHide(this.visibleSubMenus[i]);
				}
				// hide root if it's popup
				if (this.opts.isPopup) {
					if (canAnimate) {
						this.$root.stop(true, true);
					}
					if (this.$root.is(':visible')) {
						if (canAnimate && this.opts.hideFunction) {
							this.opts.hideFunction.call(this, this.$root);
						} else {
							this.$root.hide(this.opts.hideDuration);
						}
					}
				}
				this.activatedItems = [];
				this.visibleSubMenus = [];
				this.clickActivated = false;
				this.focusActivated = false;
				// reset z-index increment
				this.zIndexInc = 0;
				this.$root.triggerHandler('hideAll.smapi');
			},
			menuHideSubMenus: function(level) {
				for (var i = this.activatedItems.length - 1; i >= level; i--) {
					var $sub = this.activatedItems[i].dataSM('sub');
					if ($sub) {
						this.menuHide($sub);
					}
				}
			},
			menuInit: function($ul) {
				if (!$ul.dataSM('in-mega')) {
					// mark UL's in mega drop downs (if any) so we can neglect them
					if ($ul.hasClass('mega-menu')) {
						$ul.find('ul').dataSM('in-mega', true);
					}
					// get level (much faster than, for example, using parentsUntil)
					var level = 2,
						par = $ul[0];
					while ((par = par.parentNode.parentNode) != this.$root[0]) {
						level++;
					}
					// cache stuff for quick access
					var $a = $ul.prevAll('a').eq(-1);
					// if the link is nested (e.g. in a heading)
					if (!$a.length) {
						$a = $ul.prevAll().find('a').eq(-1);
					}
					$a.addClass('has-submenu').dataSM('sub', $ul);
					$ul.dataSM('parent-a', $a)
						.dataSM('level', level)
						.parent().dataSM('sub', $ul);
					// accessibility
					var aId = $a.attr('id') || this.accessIdPrefix + (++this.idInc),
						ulId = $ul.attr('id') || this.accessIdPrefix + (++this.idInc);
					$a.attr({
						id: aId,
						'aria-haspopup': 'true',
						'aria-controls': ulId,
						'aria-expanded': 'false'
					});
					$ul.attr({
						id: ulId,
						'role': 'group',
						'aria-hidden': 'true',
						'aria-labelledby': aId,
						'aria-expanded': 'false'
					});
					// add sub indicator to parent item
					if (this.opts.subIndicators) {
						$a[this.opts.subIndicatorsPos](this.$subArrow.clone());
					}
				}
			},
			menuPosition: function($sub) {
				var $a = $sub.dataSM('parent-a'),
					$li = $a.closest('li'),
					$ul = $li.parent(),
					level = $sub.dataSM('level'),
					subW = this.getWidth($sub),
					subH = this.getHeight($sub),
					itemOffset = $a.offset(),
					itemX = itemOffset.left,
					itemY = itemOffset.top,
					itemW = this.getWidth($a),
					itemH = this.getHeight($a),
					$win = $(window),
					winX = $win.scrollLeft(),
					winY = $win.scrollTop(),
					winW = this.getViewportWidth(),
					winH = this.getViewportHeight(),
					horizontalParent = $ul.parent().is('[data-sm-horizontal-sub]') || level == 2 && !$ul.hasClass('sm-vertical'),
					rightToLeft = this.opts.rightToLeftSubMenus && !$li.is('[data-sm-reverse]') || !this.opts.rightToLeftSubMenus && $li.is('[data-sm-reverse]'),
					subOffsetX = level == 2 ? this.opts.mainMenuSubOffsetX : this.opts.subMenusSubOffsetX,
					subOffsetY = level == 2 ? this.opts.mainMenuSubOffsetY : this.opts.subMenusSubOffsetY,
					x, y;
				if (horizontalParent) {
					x = rightToLeft ? itemW - subW - subOffsetX : subOffsetX;
					y = this.opts.bottomToTopSubMenus ? -subH - subOffsetY : itemH + subOffsetY;
				} else {
					x = rightToLeft ? subOffsetX - subW : itemW - subOffsetX;
					y = this.opts.bottomToTopSubMenus ? itemH - subOffsetY - subH : subOffsetY;
				}
				if (this.opts.keepInViewport) {
					var absX = itemX + x,
						absY = itemY + y;
					if (rightToLeft && absX < winX) {
						x = horizontalParent ? winX - absX + x : itemW - subOffsetX;
					} else if (!rightToLeft && absX + subW > winX + winW) {
						x = horizontalParent ? winX + winW - subW - absX + x : subOffsetX - subW;
					}
					if (!horizontalParent) {
						if (subH < winH && absY + subH > winY + winH) {
							y += winY + winH - subH - absY;
						} else if (subH >= winH || absY < winY) {
							y += winY - absY;
						}
					}
					// do we need scrolling?
					// 0.49 used for better precision when dealing with float values
					if (horizontalParent && (absY + subH > winY + winH + 0.49 || absY < winY) || !horizontalParent && subH > winH + 0.49) {
						var self = this;
						if (!$sub.dataSM('scroll-arrows')) {
							$sub.dataSM('scroll-arrows', $([$('<span class="scroll-up"><span class="scroll-up-arrow"></span></span>')[0], $('<span class="scroll-down"><span class="scroll-down-arrow"></span></span>')[0]])
								.on({
									mouseenter: function() {
										$sub.dataSM('scroll').up = $(this).hasClass('scroll-up');
										self.menuScroll($sub);
									},
									mouseleave: function(e) {
										self.menuScrollStop($sub);
										self.menuScrollOut($sub, e);
									},
									'mousewheel DOMMouseScroll': function(e) { e.preventDefault(); }
								})
								.insertAfter($sub)
							);
						}
						// bind scroll events and save scroll data for this sub
						var eNS = '.smartmenus_scroll';
						$sub.dataSM('scroll', {
								y: this.cssTransforms3d ? 0 : y - itemH,
								step: 1,
								// cache stuff for faster recalcs later
								itemH: itemH,
								subH: subH,
								arrowDownH: this.getHeight($sub.dataSM('scroll-arrows').eq(1))
							})
							.on(getEventsNS({
								'mouseover': function(e) { self.menuScrollOver($sub, e); },
								'mouseout': function(e) { self.menuScrollOut($sub, e); },
								'mousewheel DOMMouseScroll': function(e) { self.menuScrollMousewheel($sub, e); }
							}, eNS))
							.dataSM('scroll-arrows').css({ top: 'auto', left: '0', marginLeft: x + (parseInt($sub.css('border-left-width')) || 0), width: subW - (parseInt($sub.css('border-left-width')) || 0) - (parseInt($sub.css('border-right-width')) || 0), zIndex: $sub.css('z-index') })
								.eq(horizontalParent && this.opts.bottomToTopSubMenus ? 0 : 1).show();
						// when a menu tree is fixed positioned we allow scrolling via touch too
						// since there is no other way to access such long sub menus if no mouse is present
						if (this.isFixed()) {
							var events = {};
							events[touchEvents ? 'touchstart touchmove touchend' : 'pointerdown pointermove pointerup MSPointerDown MSPointerMove MSPointerUp'] = function(e) {
								self.menuScrollTouch($sub, e);
							};
							$sub.css({ 'touch-action': 'none', '-ms-touch-action': 'none' }).on(getEventsNS(events, eNS));
						}
					}
				}
				$sub.css({ top: 'auto', left: '0', marginLeft: x, marginTop: y - itemH });
			},
			menuScroll: function($sub, once, step) {
				var data = $sub.dataSM('scroll'),
					$arrows = $sub.dataSM('scroll-arrows'),
					end = data.up ? data.upEnd : data.downEnd,
					diff;
				if (!once && data.momentum) {
					data.momentum *= 0.92;
					diff = data.momentum;
					if (diff < 0.5) {
						this.menuScrollStop($sub);
						return;
					}
				} else {
					diff = step || (once || !this.opts.scrollAccelerate ? this.opts.scrollStep : Math.floor(data.step));
				}
				// hide any visible deeper level sub menus
				var level = $sub.dataSM('level');
				if (this.activatedItems[level - 1] && this.activatedItems[level - 1].dataSM('sub') && this.activatedItems[level - 1].dataSM('sub').is(':visible')) {
					this.menuHideSubMenus(level - 1);
				}
				data.y = data.up && end <= data.y || !data.up && end >= data.y ? data.y : (Math.abs(end - data.y) > diff ? data.y + (data.up ? diff : -diff) : end);
				$sub.css(this.cssTransforms3d ? { '-webkit-transform': 'translate3d(0, ' + data.y + 'px, 0)', transform: 'translate3d(0, ' + data.y + 'px, 0)' } : { marginTop: data.y });
				// show opposite arrow if appropriate
				if (mouse && (data.up && data.y > data.downEnd || !data.up && data.y < data.upEnd)) {
					$arrows.eq(data.up ? 1 : 0).show();
				}
				// if we've reached the end
				if (data.y == end) {
					if (mouse) {
						$arrows.eq(data.up ? 0 : 1).hide();
					}
					this.menuScrollStop($sub);
				} else if (!once) {
					if (this.opts.scrollAccelerate && data.step < this.opts.scrollStep) {
						data.step += 0.2;
					}
					var self = this;
					this.scrollTimeout = requestAnimationFrame(function() { self.menuScroll($sub); });
				}
			},
			menuScrollMousewheel: function($sub, e) {
				if (this.getClosestMenu(e.target) == $sub[0]) {
					e = e.originalEvent;
					var up = (e.wheelDelta || -e.detail) > 0;
					if ($sub.dataSM('scroll-arrows').eq(up ? 0 : 1).is(':visible')) {
						$sub.dataSM('scroll').up = up;
						this.menuScroll($sub, true);
					}
				}
				e.preventDefault();
			},
			menuScrollOut: function($sub, e) {
				if (mouse) {
					if (!/^scroll-(up|down)/.test((e.relatedTarget || '').className) && ($sub[0] != e.relatedTarget && !$.contains($sub[0], e.relatedTarget) || this.getClosestMenu(e.relatedTarget) != $sub[0])) {
						$sub.dataSM('scroll-arrows').css('visibility', 'hidden');
					}
				}
			},
			menuScrollOver: function($sub, e) {
				if (mouse) {
					if (!/^scroll-(up|down)/.test(e.target.className) && this.getClosestMenu(e.target) == $sub[0]) {
						this.menuScrollRefreshData($sub);
						var data = $sub.dataSM('scroll'),
							upEnd = $(window).scrollTop() - $sub.dataSM('parent-a').offset().top - data.itemH;
						$sub.dataSM('scroll-arrows').eq(0).css('margin-top', upEnd).end()
							.eq(1).css('margin-top', upEnd + this.getViewportHeight() - data.arrowDownH).end()
							.css('visibility', 'visible');
					}
				}
			},
			menuScrollRefreshData: function($sub) {
				var data = $sub.dataSM('scroll'),
					upEnd = $(window).scrollTop() - $sub.dataSM('parent-a').offset().top - data.itemH;
				if (this.cssTransforms3d) {
					upEnd = -(parseFloat($sub.css('margin-top')) - upEnd);
				}
				$.extend(data, {
					upEnd: upEnd,
					downEnd: upEnd + this.getViewportHeight() - data.subH
				});
			},
			menuScrollStop: function($sub) {
				if (this.scrollTimeout) {
					cancelAnimationFrame(this.scrollTimeout);
					this.scrollTimeout = 0;
					$sub.dataSM('scroll').step = 1;
					return true;
				}
			},
			menuScrollTouch: function($sub, e) {
				e = e.originalEvent;
				if (isTouchEvent(e)) {
					var touchPoint = this.getTouchPoint(e);
					// neglect event if we touched a visible deeper level sub menu
					if (this.getClosestMenu(touchPoint.target) == $sub[0]) {
						var data = $sub.dataSM('scroll');
						if (/(start|down)$/i.test(e.type)) {
							if (this.menuScrollStop($sub)) {
								// if we were scrolling, just stop and don't activate any link on the first touch
								e.preventDefault();
								this.$touchScrollingSub = $sub;
							} else {
								this.$touchScrollingSub = null;
							}
							// update scroll data since the user might have zoomed, etc.
							this.menuScrollRefreshData($sub);
							// extend it with the touch properties
							$.extend(data, {
								touchStartY: touchPoint.pageY,
								touchStartTime: e.timeStamp
							});
						} else if (/move$/i.test(e.type)) {
							var prevY = data.touchY !== undefined ? data.touchY : data.touchStartY;
							if (prevY !== undefined && prevY != touchPoint.pageY) {
								this.$touchScrollingSub = $sub;
								var up = prevY < touchPoint.pageY;
								// changed direction? reset...
								if (data.up !== undefined && data.up != up) {
									$.extend(data, {
										touchStartY: touchPoint.pageY,
										touchStartTime: e.timeStamp
									});
								}
								$.extend(data, {
									up: up,
									touchY: touchPoint.pageY
								});
								this.menuScroll($sub, true, Math.abs(touchPoint.pageY - prevY));
							}
							e.preventDefault();
						} else { // touchend/pointerup
							if (data.touchY !== undefined) {
								if (data.momentum = Math.pow(Math.abs(touchPoint.pageY - data.touchStartY) / (e.timeStamp - data.touchStartTime), 2) * 15) {
									this.menuScrollStop($sub);
									this.menuScroll($sub);
									e.preventDefault();
								}
								delete data.touchY;
							}
						}
					}
				}
			},
			menuShow: function($sub) {
				if (!$sub.dataSM('beforefirstshowfired')) {
					$sub.dataSM('beforefirstshowfired', true);
					if (this.$root.triggerHandler('beforefirstshow.smapi', $sub[0]) === false) {
						return;
					}
				}
				if (this.$root.triggerHandler('beforeshow.smapi', $sub[0]) === false) {
					return;
				}
				$sub.dataSM('shown-before', true);
				if (canAnimate) {
					$sub.stop(true, true);
				}
				if (!$sub.is(':visible')) {
					// highlight parent item
					var $a = $sub.dataSM('parent-a'),
						collapsible = this.isCollapsible();
					if (this.opts.keepHighlighted || collapsible) {
						$a.addClass('highlighted');
					}
					if (collapsible) {
						$sub.removeClass('sm-nowrap').css({ zIndex: '', width: 'auto', minWidth: '', maxWidth: '', top: '', left: '', marginLeft: '', marginTop: '' });
					} else {
						// set z-index
						$sub.css('z-index', this.zIndexInc = (this.zIndexInc || this.getStartZIndex()) + 1);
						// min/max-width fix - no way to rely purely on CSS as all UL's are nested
						if (this.opts.subMenusMinWidth || this.opts.subMenusMaxWidth) {
							$sub.css({ width: 'auto', minWidth: '', maxWidth: '' }).addClass('sm-nowrap');
							if (this.opts.subMenusMinWidth) {
							 	$sub.css('min-width', this.opts.subMenusMinWidth);
							}
							if (this.opts.subMenusMaxWidth) {
							 	var noMaxWidth = this.getWidth($sub);
							 	$sub.css('max-width', this.opts.subMenusMaxWidth);
								if (noMaxWidth > this.getWidth($sub)) {
									$sub.removeClass('sm-nowrap').css('width', this.opts.subMenusMaxWidth);
								}
							}
						}
						this.menuPosition($sub);
					}
					var complete = function() {
						// fix: "overflow: hidden;" is not reset on animation complete in jQuery < 1.9.0 in Chrome when global "box-sizing: border-box;" is used
						$sub.css('overflow', '');
					};
					// if sub is collapsible (mobile view)
					if (collapsible) {
						if (canAnimate && this.opts.collapsibleShowFunction) {
							this.opts.collapsibleShowFunction.call(this, $sub, complete);
						} else {
							$sub.show(this.opts.collapsibleShowDuration, complete);
						}
					} else {
						if (canAnimate && this.opts.showFunction) {
							this.opts.showFunction.call(this, $sub, complete);
						} else {
							$sub.show(this.opts.showDuration, complete);
						}
					}
					// accessibility
					$a.attr('aria-expanded', 'true');
					$sub.attr({
						'aria-expanded': 'true',
						'aria-hidden': 'false'
					});
					// store sub menu in visible array
					this.visibleSubMenus.push($sub);
					this.$root.triggerHandler('show.smapi', $sub[0]);
				}
			},
			popupHide: function(noHideTimeout) {
				if (this.hideTimeout) {
					clearTimeout(this.hideTimeout);
					this.hideTimeout = 0;
				}
				var self = this;
				this.hideTimeout = setTimeout(function() {
					self.menuHideAll();
				}, noHideTimeout ? 1 : this.opts.hideTimeout);
			},
			popupShow: function(left, top) {
				if (!this.opts.isPopup) {
					alert('SmartMenus jQuery Error:\n\nIf you want to show this menu via the "popupShow" method, set the isPopup:true option.');
					return;
				}
				if (this.hideTimeout) {
					clearTimeout(this.hideTimeout);
					this.hideTimeout = 0;
				}
				this.$root.dataSM('shown-before', true);
				if (canAnimate) {
					this.$root.stop(true, true);
				}
				if (!this.$root.is(':visible')) {
					this.$root.css({ left: left, top: top });
					// show menu
					var self = this,
						complete = function() {
							self.$root.css('overflow', '');
						};
					if (canAnimate && this.opts.showFunction) {
						this.opts.showFunction.call(this, this.$root, complete);
					} else {
						this.$root.show(this.opts.showDuration, complete);
					}
					this.visibleSubMenus[0] = this.$root;
				}
			},
			refresh: function() {
				this.destroy(true);
				this.init(true);
			},
			rootKeyDown: function(e) {
				if (!this.handleEvents()) {
					return;
				}
				switch (e.keyCode) {
					case 27: // reset on Esc
						var $activeTopItem = this.activatedItems[0];
						if ($activeTopItem) {
							this.menuHideAll();
							$activeTopItem[0].focus();
							var $sub = $activeTopItem.dataSM('sub');
							if ($sub) {
								this.menuHide($sub);
							}
						}
						break;
					case 32: // activate item's sub on Space
						var $target = $(e.target);
						if ($target.is('a') && this.handleItemEvents($target)) {
							var $sub = $target.dataSM('sub');
							if ($sub && !$sub.is(':visible')) {
								this.itemClick({ currentTarget: e.target });
								e.preventDefault();
							}
						}
						break;
				}
			},
			rootOut: function(e) {
				if (!this.handleEvents() || this.isTouchMode() || e.target == this.$root[0]) {
					return;
				}
				if (this.hideTimeout) {
					clearTimeout(this.hideTimeout);
					this.hideTimeout = 0;
				}
				if (!this.opts.showOnClick || !this.opts.hideOnClick) {
					var self = this;
					this.hideTimeout = setTimeout(function() { self.menuHideAll(); }, this.opts.hideTimeout);
				}
			},
			rootOver: function(e) {
				if (!this.handleEvents() || this.isTouchMode() || e.target == this.$root[0]) {
					return;
				}
				if (this.hideTimeout) {
					clearTimeout(this.hideTimeout);
					this.hideTimeout = 0;
				}
			},
			winResize: function(e) {
				if (!this.handleEvents()) {
					// we still need to resize the disable overlay if it's visible
					if (this.$disableOverlay) {
						var pos = this.$root.offset();
	 					this.$disableOverlay.css({
							top: pos.top,
							left: pos.left,
							width: this.$root.outerWidth(),
							height: this.$root.outerHeight()
						});
					}
					return;
				}
				// hide sub menus on resize - on mobile do it only on orientation change
				if (!('onorientationchange' in window) || e.type == 'orientationchange') {
					var collapsible = this.isCollapsible();
					// if it was collapsible before resize and still is, don't do it
					if (!(this.wasCollapsible && collapsible)) { 
						if (this.activatedItems.length) {
							this.activatedItems[this.activatedItems.length - 1][0].blur();
						}
						this.menuHideAll();
					}
					this.wasCollapsible = collapsible;
				}
			}
		}
	});

	$.fn.dataSM = function(key, val) {
		if (val) {
			return this.data(key + '_smartmenus', val);
		}
		return this.data(key + '_smartmenus');
	};

	$.fn.removeDataSM = function(key) {
		return this.removeData(key + '_smartmenus');
	};

	$.fn.smartmenus = function(options) {
		if (typeof options == 'string') {
			var args = arguments,
				method = options;
			Array.prototype.shift.call(args);
			return this.each(function() {
				var smartmenus = $(this).data('smartmenus');
				if (smartmenus && smartmenus[method]) {
					smartmenus[method].apply(smartmenus, args);
				}
			});
		}
		return this.each(function() {
			// [data-sm-options] attribute on the root UL
			var dataOpts = $(this).data('sm-options') || null;
			if (dataOpts) {
				try {
					dataOpts = eval('(' + dataOpts + ')');
				} catch(e) {
					dataOpts = null;
					alert('ERROR\n\nSmartMenus jQuery init:\nInvalid "data-sm-options" attribute value syntax.');
				};
			}
			new $.SmartMenus(this, $.extend({}, $.fn.smartmenus.defaults, options, dataOpts));
		});
	};

	// default settings
	$.fn.smartmenus.defaults = {
		isPopup:		false,		// is this a popup menu (can be shown via the popupShow/popupHide methods) or a permanent menu bar
		mainMenuSubOffsetX:	0,		// pixels offset from default position
		mainMenuSubOffsetY:	0,		// pixels offset from default position
		subMenusSubOffsetX:	0,		// pixels offset from default position
		subMenusSubOffsetY:	0,		// pixels offset from default position
		subMenusMinWidth:	'10em',		// min-width for the sub menus (any CSS unit) - if set, the fixed width set in CSS will be ignored
		subMenusMaxWidth:	'20em',		// max-width for the sub menus (any CSS unit) - if set, the fixed width set in CSS will be ignored
		subIndicators: 		true,		// create sub menu indicators - creates a SPAN and inserts it in the A
		subIndicatorsPos: 	'append',	// position of the SPAN relative to the menu item content ('append', 'prepend')
		subIndicatorsText:	'',		// [optionally] add text in the SPAN (e.g. '+') (you may want to check the CSS for the sub indicators too)
		scrollStep: 		30,		// pixels step when scrolling long sub menus that do not fit in the viewport height
		scrollAccelerate:	true,		// accelerate scrolling or use a fixed step
		showTimeout:		250,		// timeout before showing the sub menus
		hideTimeout:		500,		// timeout before hiding the sub menus
		showDuration:		0,		// duration for show animation - set to 0 for no animation - matters only if showFunction:null
		showFunction:		null,		// custom function to use when showing a sub menu (the default is the jQuery 'show')
							// don't forget to call complete() at the end of whatever you do
							// e.g.: function($ul, complete) { $ul.fadeIn(250, complete); }
		hideDuration:		0,		// duration for hide animation - set to 0 for no animation - matters only if hideFunction:null
		hideFunction:		function($ul, complete) { $ul.fadeOut(200, complete); },	// custom function to use when hiding a sub menu (the default is the jQuery 'hide')
							// don't forget to call complete() at the end of whatever you do
							// e.g.: function($ul, complete) { $ul.fadeOut(250, complete); }
		collapsibleShowDuration:0,		// duration for show animation for collapsible sub menus - matters only if collapsibleShowFunction:null
		collapsibleShowFunction:function($ul, complete) { $ul.slideDown(200, complete); },	// custom function to use when showing a collapsible sub menu
							// (i.e. when mobile styles are used to make the sub menus collapsible)
		collapsibleHideDuration:0,		// duration for hide animation for collapsible sub menus - matters only if collapsibleHideFunction:null
		collapsibleHideFunction:function($ul, complete) { $ul.slideUp(200, complete); },	// custom function to use when hiding a collapsible sub menu
							// (i.e. when mobile styles are used to make the sub menus collapsible)
		showOnClick:		false,		// show the first-level sub menus onclick instead of onmouseover (i.e. mimic desktop app menus) (matters only for mouse input)
		hideOnClick:		true,		// hide the sub menus on click/tap anywhere on the page
		noMouseOver:		false,		// disable sub menus activation onmouseover (i.e. behave like in touch mode - use just mouse clicks) (matters only for mouse input)
		keepInViewport:		true,		// reposition the sub menus if needed to make sure they always appear inside the viewport
		keepHighlighted:	true,		// keep all ancestor items of the current sub menu highlighted (adds the 'highlighted' class to the A's)
		markCurrentItem:	false,		// automatically add the 'current' class to the A element of the item linking to the current URL
		markCurrentTree:	true,		// add the 'current' class also to the A elements of all ancestor items of the current item
		rightToLeftSubMenus:	false,		// right to left display of the sub menus (check the CSS for the sub indicators' position)
		bottomToTopSubMenus:	false,		// bottom to top display of the sub menus
		collapsibleBehavior:	'default'	// parent items behavior in collapsible (mobile) view ('default', 'toggle', 'link', 'accordion', 'accordion-toggle', 'accordion-link')
							// 'default' - first tap on parent item expands sub, second tap loads its link
							// 'toggle' - the whole parent item acts just as a toggle button for its sub menu (expands/collapses on each tap)
							// 'link' - the parent item acts as a regular item (first tap loads its link), the sub menu can be expanded only via the +/- button
							// 'accordion' - like 'default' but on expand also resets any visible sub menus from deeper levels or other branches
							// 'accordion-toggle' - like 'toggle' but on expand also resets any visible sub menus from deeper levels or other branches
							// 'accordion-link' - like 'link' but on expand also resets any visible sub menus from deeper levels or other branches
	};

	return $;
}));

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc21hcnRtZW51cy1ib290c3RyYXAtNC9qcXVlcnkuc21hcnRtZW51cy5ib290c3RyYXAtNC5jc3MiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3NtYXJ0bWVudXMtYm9vdHN0cmFwLTQvanF1ZXJ5LnNtYXJ0bWVudXMuYm9vdHN0cmFwLTQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3NtYXJ0bWVudXMtYm9vdHN0cmFwLTQvbm9kZV9tb2R1bGVzL3NtYXJ0bWVudXMvZGlzdC9qcXVlcnkuc21hcnRtZW51cy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc21hcnRtZW51cy9kaXN0L2pxdWVyeS5zbWFydG1lbnVzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLHVDOzs7Ozs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUssSUFBMEM7QUFDL0M7QUFDQSxFQUFFLGlDQUFPLENBQUMseUVBQVEsRUFBRSxnSUFBWSxDQUFDLG9DQUFFLE9BQU87QUFBQTtBQUFBO0FBQUEsb0dBQUM7QUFDM0MsRUFBRSxNQUFNLEVBTU47QUFDRixDQUFDOztBQUVELHFDQUFxQztBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7O0FBRUE7QUFDQSxDQUFDOzs7Ozs7Ozs7Ozs7QUNyS0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSyxJQUEwQztBQUMvQztBQUNBLEVBQUUsaUNBQU8sQ0FBQyx5RUFBUSxDQUFDLG9DQUFFLE9BQU87QUFBQTtBQUFBO0FBQUEsb0dBQUM7QUFDN0IsRUFBRSxNQUFNLEVBTU47QUFDRixDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEVBQThFLHdDQUF3QyxFQUFFO0FBQ3hILHNFQUFzRSxrQkFBa0IsRUFBRTtBQUMxRjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCLHVCQUF1QjtBQUNuRDtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQiw0QkFBNEI7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekIsd0JBQXdCO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLCtCQUErQiw0REFBNEQ7O0FBRTNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw0REFBNEQsd0JBQXdCLEVBQUU7O0FBRXRGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLE1BQU07QUFDTjs7QUFFQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQix3Q0FBd0M7QUFDM0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsNEVBQTRFO0FBQzlGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtDQUErQyxvQkFBb0IsRUFBRTtBQUNyRTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1osZUFBZSw2Q0FBNkM7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtDQUErQyx1QkFBdUIsRUFBRTtBQUN4RTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixxRkFBcUY7QUFDckc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQWlELFlBQVk7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBLGdEQUFnRCxZQUFZO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVU7QUFDVjtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1YsbURBQW1ELG9CQUFvQjtBQUN2RSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0Esa0NBQWtDLDhCQUE4QixFQUFFO0FBQ2xFLGlDQUFpQyw2QkFBNkIsRUFBRTtBQUNoRSxrREFBa0Qsb0NBQW9DO0FBQ3RGLFFBQVE7QUFDUixxQ0FBcUMsc09BQXNPO0FBQzNRO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIscURBQXFEO0FBQ3RFO0FBQ0E7QUFDQTtBQUNBLGNBQWMsOERBQThEO0FBQzVFLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQywrR0FBK0csSUFBSSxvQkFBb0I7QUFDNUs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0REFBNEQsdUJBQXVCLEVBQUU7QUFDckY7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUixPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLE9BQU8sT0FBTztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUMsMEdBQTBHO0FBQ25KLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQiw0Q0FBNEM7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQyxxR0FBcUc7QUFDckk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsdUJBQXVCO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsMEJBQTBCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQ0FBK0Msb0JBQW9CLEVBQUU7QUFDckU7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUNBQXFDO0FBQ3JDLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QywyQkFBMkI7QUFDcEU7QUFDQSwwQ0FBMEMsNEJBQTRCLEVBQUU7QUFDeEU7QUFDQSx5Q0FBeUMsNEJBQTRCO0FBQ3JFO0FBQ0EsbURBQW1ELDhCQUE4QixFQUFFO0FBQ25GO0FBQ0E7QUFDQSxtREFBbUQsNEJBQTRCLEVBQUU7QUFDakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQ0FBQyxHOzs7Ozs7Ozs7OztBQ3BzQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSyxJQUEwQztBQUMvQztBQUNBLEVBQUUsaUNBQU8sQ0FBQyx5RUFBUSxDQUFDLG9DQUFFLE9BQU87QUFBQTtBQUFBO0FBQUEsb0dBQUM7QUFDN0IsRUFBRSxNQUFNLEVBTU47QUFDRixDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEVBQThFLHdDQUF3QyxFQUFFO0FBQ3hILHNFQUFzRSxrQkFBa0IsRUFBRTtBQUMxRjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCLHVCQUF1QjtBQUNuRDtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQiw0QkFBNEI7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekIsd0JBQXdCO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLCtCQUErQiw0REFBNEQ7O0FBRTNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw0REFBNEQsd0JBQXdCLEVBQUU7O0FBRXRGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLE1BQU07QUFDTjs7QUFFQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQix3Q0FBd0M7QUFDM0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsNEVBQTRFO0FBQzlGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtDQUErQyxvQkFBb0IsRUFBRTtBQUNyRTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1osZUFBZSw2Q0FBNkM7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtDQUErQyx1QkFBdUIsRUFBRTtBQUN4RTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixxRkFBcUY7QUFDckc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQWlELFlBQVk7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBLGdEQUFnRCxZQUFZO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVU7QUFDVjtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1YsbURBQW1ELG9CQUFvQjtBQUN2RSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0Esa0NBQWtDLDhCQUE4QixFQUFFO0FBQ2xFLGlDQUFpQyw2QkFBNkIsRUFBRTtBQUNoRSxrREFBa0Qsb0NBQW9DO0FBQ3RGLFFBQVE7QUFDUixxQ0FBcUMsc09BQXNPO0FBQzNRO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIscURBQXFEO0FBQ3RFO0FBQ0E7QUFDQTtBQUNBLGNBQWMsOERBQThEO0FBQzVFLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQywrR0FBK0csSUFBSSxvQkFBb0I7QUFDNUs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0REFBNEQsdUJBQXVCLEVBQUU7QUFDckY7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUixPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLE9BQU8sT0FBTztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUMsMEdBQTBHO0FBQ25KLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQiw0Q0FBNEM7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQyxxR0FBcUc7QUFDckk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsdUJBQXVCO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsMEJBQTBCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQ0FBK0Msb0JBQW9CLEVBQUU7QUFDckU7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUNBQXFDO0FBQ3JDLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QywyQkFBMkI7QUFDcEU7QUFDQSwwQ0FBMEMsNEJBQTRCLEVBQUU7QUFDeEU7QUFDQSx5Q0FBeUMsNEJBQTRCO0FBQ3JFO0FBQ0EsbURBQW1ELDhCQUE4QixFQUFFO0FBQ25GO0FBQ0E7QUFDQSxtREFBbUQsNEJBQTRCLEVBQUU7QUFDakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQ0FBQyxHIiwiZmlsZSI6InZlbmRvcnN+TGlicmFyeS9TbWFydE1lbnVzfmxheW91dC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsIi8qIVxuICogU21hcnRNZW51cyBqUXVlcnkgUGx1Z2luIEJvb3RzdHJhcCA0IEFkZG9uIC0gdjAuMS4wIC0gU2VwdGVtYmVyIDE3LCAyMDE3XG4gKiBodHRwOi8vd3d3LnNtYXJ0bWVudXMub3JnL1xuICpcbiAqIENvcHlyaWdodCBWYXNpbCBEaW5rb3YsIFZhZGlrb20gV2ViIEx0ZC5cbiAqIGh0dHA6Ly92YWRpa29tLmNvbVxuICpcbiAqIExpY2Vuc2VkIE1JVFxuICovXG5cbihmdW5jdGlvbihmYWN0b3J5KSB7XG5cdGlmICh0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpIHtcblx0XHQvLyBBTURcblx0XHRkZWZpbmUoWydqcXVlcnknLCAnc21hcnRtZW51cyddLCBmYWN0b3J5KTtcblx0fSBlbHNlIGlmICh0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlLmV4cG9ydHMgPT09ICdvYmplY3QnKSB7XG5cdFx0Ly8gQ29tbW9uSlNcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkocmVxdWlyZSgnanF1ZXJ5JykpO1xuXHR9IGVsc2Uge1xuXHRcdC8vIEdsb2JhbCBqUXVlcnlcblx0XHRmYWN0b3J5KGpRdWVyeSk7XG5cdH1cbn0gKGZ1bmN0aW9uKCQpIHtcblxuXHQkLmV4dGVuZCgkLlNtYXJ0TWVudXMuQm9vdHN0cmFwID0ge30sIHtcblx0XHRrZXlkb3duRml4OiBmYWxzZSxcblx0XHRpbml0OiBmdW5jdGlvbigpIHtcblx0XHRcdC8vIGluaXQgYWxsIG5hdmJhcnMgdGhhdCBkb24ndCBoYXZlIHRoZSBcImRhdGEtc20tc2tpcFwiIGF0dHJpYnV0ZSBzZXRcblx0XHRcdHZhciAkbmF2YmFycyA9ICQoJ3VsLm5hdmJhci1uYXY6bm90KFtkYXRhLXNtLXNraXBdKScpO1xuXHRcdFx0JG5hdmJhcnMuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyICR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFx0XHRvYmogPSAkdGhpcy5kYXRhKCdzbWFydG1lbnVzJyk7XG5cdFx0XHRcdC8vIGlmIHRoaXMgbmF2YmFyIGlzIG5vdCBpbml0aWFsaXplZFxuXHRcdFx0XHRpZiAoIW9iaikge1xuXHRcdFx0XHRcdHZhciBza2lwQmVoYXZpb3IgPSAkdGhpcy5pcygnW2RhdGEtc20tc2tpcC1jb2xsYXBzaWJsZS1iZWhhdmlvcl0nKSxcblx0XHRcdFx0XHRcdHJpZ2h0QWxpZ25lZCA9ICR0aGlzLmhhc0NsYXNzKCdtbC1hdXRvJykgfHwgJHRoaXMucHJldkFsbCgnLm1yLWF1dG8nKS5sZW5ndGggPiAwO1xuXG5cdFx0XHRcdFx0JHRoaXMuc21hcnRtZW51cyh7XG5cdFx0XHRcdFx0XHRcdC8vIHRoZXNlIGFyZSBzb21lIGdvb2QgZGVmYXVsdCBvcHRpb25zIHRoYXQgc2hvdWxkIHdvcmsgZm9yIGFsbFxuXHRcdFx0XHRcdFx0XHRzdWJNZW51c1N1Yk9mZnNldFg6IDIsXG5cdFx0XHRcdFx0XHRcdHN1Yk1lbnVzU3ViT2Zmc2V0WTogLTksXG5cdFx0XHRcdFx0XHRcdHN1YkluZGljYXRvcnM6ICFza2lwQmVoYXZpb3IsXG5cdFx0XHRcdFx0XHRcdGNvbGxhcHNpYmxlU2hvd0Z1bmN0aW9uOiBudWxsLFxuXHRcdFx0XHRcdFx0XHRjb2xsYXBzaWJsZUhpZGVGdW5jdGlvbjogbnVsbCxcblx0XHRcdFx0XHRcdFx0cmlnaHRUb0xlZnRTdWJNZW51czogcmlnaHRBbGlnbmVkLFxuXHRcdFx0XHRcdFx0XHRib3R0b21Ub1RvcFN1Yk1lbnVzOiAkdGhpcy5jbG9zZXN0KCcuZml4ZWQtYm90dG9tJykubGVuZ3RoID4gMCxcblx0XHRcdFx0XHRcdFx0Ly8gY3VzdG9tIG9wdGlvbihzKSBmb3IgdGhlIEJvb3RzdHJhcCA0IGFkZG9uXG5cdFx0XHRcdFx0XHRcdGJvb3RzdHJhcEhpZ2hsaWdodENsYXNzZXM6ICd0ZXh0LWRhcmsgYmctbGlnaHQnXG5cdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0Lm9uKHtcblx0XHRcdFx0XHRcdFx0Ly8gc2V0L3Vuc2V0IHByb3BlciBCb290c3RyYXAgY2xhc3NlcyBmb3Igc29tZSBtZW51IGVsZW1lbnRzXG5cdFx0XHRcdFx0XHRcdCdzaG93LnNtYXBpJzogZnVuY3Rpb24oZSwgbWVudSkge1xuXHRcdFx0XHRcdFx0XHRcdHZhciAkbWVudSA9ICQobWVudSksXG5cdFx0XHRcdFx0XHRcdFx0XHQkc2Nyb2xsQXJyb3dzID0gJG1lbnUuZGF0YVNNKCdzY3JvbGwtYXJyb3dzJyk7XG5cdFx0XHRcdFx0XHRcdFx0aWYgKCRzY3JvbGxBcnJvd3MpIHtcblx0XHRcdFx0XHRcdFx0XHRcdCRzY3JvbGxBcnJvd3MuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJywgJG1lbnUuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJykpO1xuXHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0XHQkbWVudS5wYXJlbnQoKS5hZGRDbGFzcygnc2hvdycpO1xuXHRcdFx0XHRcdFx0XHRcdGlmIChvYmoub3B0cy5rZWVwSGlnaGxpZ2h0ZWQgJiYgJG1lbnUuZGF0YVNNKCdsZXZlbCcpID4gMikge1xuXHRcdFx0XHRcdFx0XHRcdFx0JG1lbnUucHJldkFsbCgnYScpLmFkZENsYXNzKG9iai5vcHRzLmJvb3RzdHJhcEhpZ2hsaWdodENsYXNzZXMpO1xuXHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0J2hpZGUuc21hcGknOiBmdW5jdGlvbihlLCBtZW51KSB7XG5cdFx0XHRcdFx0XHRcdFx0dmFyICRtZW51ID0gJChtZW51KTtcblx0XHRcdFx0XHRcdFx0XHQkbWVudS5wYXJlbnQoKS5yZW1vdmVDbGFzcygnc2hvdycpO1xuXHRcdFx0XHRcdFx0XHRcdGlmIChvYmoub3B0cy5rZWVwSGlnaGxpZ2h0ZWQgJiYgJG1lbnUuZGF0YVNNKCdsZXZlbCcpID4gMikge1xuXHRcdFx0XHRcdFx0XHRcdFx0JG1lbnUucHJldkFsbCgnYScpLnJlbW92ZUNsYXNzKG9iai5vcHRzLmJvb3RzdHJhcEhpZ2hsaWdodENsYXNzZXMpO1xuXHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHRvYmogPSAkdGhpcy5kYXRhKCdzbWFydG1lbnVzJyk7XG5cblx0XHRcdFx0XHRmdW5jdGlvbiBvbkluaXQoKSB7XG5cdFx0XHRcdFx0XHQvLyBzZXQgQm9vdHN0cmFwJ3MgXCJhY3RpdmVcIiBjbGFzcyB0byBTbWFydE1lbnVzIFwiY3VycmVudFwiIGl0ZW1zIChzaG91bGQgc29tZW9uZSBkZWNpZGUgdG8gZW5hYmxlIG1hcmtDdXJyZW50SXRlbTogdHJ1ZSlcblx0XHRcdFx0XHRcdCR0aGlzLmZpbmQoJ2EuY3VycmVudCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XHRcdFx0XHRcdC8vIGRyb3Bkb3duIGl0ZW1zIHJlcXVpcmUgdGhlIGNsYXNzIHRvIGJlIHNldCB0byB0aGUgQSdzIHdoaWxlIGZvciBuYXYgaXRlbXMgaXQgc2hvdWxkIGJlIHNldCB0byB0aGUgcGFyZW50IExJJ3Ncblx0XHRcdFx0XHRcdFx0KCR0aGlzLmhhc0NsYXNzKCdkcm9wZG93bi1pdGVtJykgPyAkdGhpcyA6ICR0aGlzLnBhcmVudCgpKS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdC8vIHBhcmVudCBpdGVtcyBmaXhlc1xuXHRcdFx0XHRcdFx0JHRoaXMuZmluZCgnYS5oYXMtc3VibWVudScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XHRcdFx0XHRcdC8vIHJlbW92ZSBCb290c3RyYXAgcmVxdWlyZWQgYXR0cmlidXRlcyB0aGF0IG1pZ2h0IGNhdXNlIGNvbmZsaWN0aW5nIGlzc3VlcyB3aXRoIHRoZSBTbWFydE1lbnVzIHNjcmlwdFxuXHRcdFx0XHRcdFx0XHRpZiAoJHRoaXMuaXMoJ1tkYXRhLXRvZ2dsZT1cImRyb3Bkb3duXCJdJykpIHtcblx0XHRcdFx0XHRcdFx0XHQkdGhpcy5kYXRhU00oJ2JzLWRhdGEtdG9nZ2xlLWRyb3Bkb3duJywgdHJ1ZSkucmVtb3ZlQXR0cignZGF0YS10b2dnbGUnKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHQvLyByZW1vdmUgQm9vdHN0cmFwJ3MgY2FyZXRzIGdlbmVyYXRpbmcgY2xhc3Ncblx0XHRcdFx0XHRcdFx0aWYgKCFza2lwQmVoYXZpb3IgJiYgJHRoaXMuaGFzQ2xhc3MoJ2Ryb3Bkb3duLXRvZ2dsZScpKSB7XG5cdFx0XHRcdFx0XHRcdFx0JHRoaXMuZGF0YVNNKCdicy1kcm9wZG93bi10b2dnbGUnLCB0cnVlKS5yZW1vdmVDbGFzcygnZHJvcGRvd24tdG9nZ2xlJyk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdG9uSW5pdCgpO1xuXG5cdFx0XHRcdFx0ZnVuY3Rpb24gb25CZWZvcmVEZXN0cm95KCkge1xuXHRcdFx0XHRcdFx0JHRoaXMuZmluZCgnYS5jdXJyZW50JykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0dmFyICR0aGlzID0gJCh0aGlzKTtcblx0XHRcdFx0XHRcdFx0KCR0aGlzLmhhc0NsYXNzKCdhY3RpdmUnKSA/ICR0aGlzIDogJHRoaXMucGFyZW50KCkpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0JHRoaXMuZmluZCgnYS5oYXMtc3VibWVudScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XHRcdFx0XHRcdGlmICgkdGhpcy5kYXRhU00oJ2JzLWRyb3Bkb3duLXRvZ2dsZScpKSB7XG5cdFx0XHRcdFx0XHRcdFx0JHRoaXMuYWRkQ2xhc3MoJ2Ryb3Bkb3duLXRvZ2dsZScpLnJlbW92ZURhdGFTTSgnYnMtZHJvcGRvd24tdG9nZ2xlJyk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0aWYgKCR0aGlzLmRhdGFTTSgnYnMtZGF0YS10b2dnbGUtZHJvcGRvd24nKSkge1xuXHRcdFx0XHRcdFx0XHRcdCR0aGlzLmF0dHIoJ2RhdGEtdG9nZ2xlJywgJ2Ryb3Bkb3duJykucmVtb3ZlRGF0YVNNKCdicy1kYXRhLXRvZ2dsZS1kcm9wZG93bicpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHQvLyBjdXN0b20gXCJyZWZyZXNoXCIgbWV0aG9kIGZvciBCb290c3RyYXBcblx0XHRcdFx0XHRvYmoucmVmcmVzaCA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0JC5TbWFydE1lbnVzLnByb3RvdHlwZS5yZWZyZXNoLmNhbGwodGhpcyk7XG5cdFx0XHRcdFx0XHRvbkluaXQoKTtcblx0XHRcdFx0XHRcdC8vIHVwZGF0ZSBjb2xsYXBzaWJsZSBkZXRlY3Rpb25cblx0XHRcdFx0XHRcdGRldGVjdENvbGxhcHNpYmxlKHRydWUpO1xuXHRcdFx0XHRcdH07XG5cblx0XHRcdFx0XHQvLyBjdXN0b20gXCJkZXN0cm95XCIgbWV0aG9kIGZvciBCb290c3RyYXBcblx0XHRcdFx0XHRvYmouZGVzdHJveSA9IGZ1bmN0aW9uKHJlZnJlc2gpIHtcblx0XHRcdFx0XHRcdG9uQmVmb3JlRGVzdHJveSgpO1xuXHRcdFx0XHRcdFx0JC5TbWFydE1lbnVzLnByb3RvdHlwZS5kZXN0cm95LmNhbGwodGhpcywgcmVmcmVzaCk7XG5cdFx0XHRcdFx0fTtcblxuXHRcdFx0XHRcdC8vIGtlZXAgQm9vdHN0cmFwJ3MgZGVmYXVsdCBiZWhhdmlvciAoaS5lLiB1c2UgdGhlIHdob2xlIGl0ZW0gYXJlYSBqdXN0IGFzIGEgc3ViIG1lbnUgdG9nZ2xlKVxuXHRcdFx0XHRcdGlmIChza2lwQmVoYXZpb3IpIHtcblx0XHRcdFx0XHRcdG9iai5vcHRzLmNvbGxhcHNpYmxlQmVoYXZpb3IgPSAndG9nZ2xlJztcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHQvLyBvbnJlc2l6ZSBkZXRlY3Qgd2hlbiB0aGUgbmF2YmFyIGJlY29tZXMgY29sbGFwc2libGUgYW5kIGFkZCBpdCB0aGUgXCJzbS1jb2xsYXBzaWJsZVwiIGNsYXNzXG5cdFx0XHRcdFx0dmFyIHdpblc7XG5cdFx0XHRcdFx0ZnVuY3Rpb24gZGV0ZWN0Q29sbGFwc2libGUoZm9yY2UpIHtcblx0XHRcdFx0XHRcdHZhciBuZXdXID0gb2JqLmdldFZpZXdwb3J0V2lkdGgoKTtcblx0XHRcdFx0XHRcdGlmIChuZXdXICE9IHdpblcgfHwgZm9yY2UpIHtcblx0XHRcdFx0XHRcdFx0aWYgKG9iai5pc0NvbGxhcHNpYmxlKCkpIHtcblx0XHRcdFx0XHRcdFx0XHQkdGhpcy5hZGRDbGFzcygnc20tY29sbGFwc2libGUnKTtcblx0XHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0XHQkdGhpcy5yZW1vdmVDbGFzcygnc20tY29sbGFwc2libGUnKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHR3aW5XID0gbmV3Vztcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZGV0ZWN0Q29sbGFwc2libGUoKTtcblx0XHRcdFx0XHQkKHdpbmRvdykub24oJ3Jlc2l6ZS5zbWFydG1lbnVzJyArIG9iai5yb290SWQsIGRldGVjdENvbGxhcHNpYmxlKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHQvLyBrZXlkb3duIGZpeCBmb3IgQm9vdHN0cmFwIDQgY29uZmxpY3Rcblx0XHRcdGlmICgkbmF2YmFycy5sZW5ndGggJiYgISQuU21hcnRNZW51cy5Cb290c3RyYXAua2V5ZG93bkZpeCkge1xuXHRcdFx0XHQvLyB1bmhvb2sgQlMga2V5ZG93biBoYW5kbGVyIGZvciBhbGwgZHJvcGRvd25zXG5cdFx0XHRcdCQoZG9jdW1lbnQpLm9mZigna2V5ZG93bi5icy5kcm9wZG93bi5kYXRhLWFwaScsICcuZHJvcGRvd24tbWVudScpO1xuXHRcdFx0XHQvLyByZXN0b3JlIEJTIGtleWRvd24gaGFuZGxlciBmb3IgZHJvcGRvd25zIHRoYXQgYXJlIG5vdCBpbnNpZGUgU21hcnRNZW51cyBuYXZiYXJzXG5cdFx0XHRcdC8vIFNtYXJ0TWVudXMgd29uJ3QgYWRkIHRoZSBcInNob3dcIiBjbGFzcyBzbyBpdCdzIGhhbmR5IGhlcmVcblx0XHRcdFx0aWYgKCQuZm4uZHJvcGRvd24gJiYgJC5mbi5kcm9wZG93bi5Db25zdHJ1Y3Rvcikge1xuXHRcdFx0XHRcdCQoZG9jdW1lbnQpLm9uKCdrZXlkb3duLmJzLmRyb3Bkb3duLmRhdGEtYXBpJywgJy5kcm9wZG93bi1tZW51LnNob3cnLCAkLmZuLmRyb3Bkb3duLkNvbnN0cnVjdG9yLl9kYXRhQXBpS2V5ZG93bkhhbmRsZXIpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdCQuU21hcnRNZW51cy5Cb290c3RyYXAua2V5ZG93bkZpeCA9IHRydWU7XG5cdFx0XHR9XG5cdFx0fVxuXHR9KTtcblxuXHQvLyBpbml0IG9uZG9tcmVhZHlcblx0JCgkLlNtYXJ0TWVudXMuQm9vdHN0cmFwLmluaXQpO1xuXG5cdHJldHVybiAkO1xufSkpO1xuIiwiLyohXG4gKiBTbWFydE1lbnVzIGpRdWVyeSBQbHVnaW4gLSB2MS4xLjEgLSBKdWx5IDIzLCAyMDIwXG4gKiBodHRwOi8vd3d3LnNtYXJ0bWVudXMub3JnL1xuICpcbiAqIENvcHlyaWdodCBWYXNpbCBEaW5rb3YsIFZhZGlrb20gV2ViIEx0ZC5cbiAqIGh0dHA6Ly92YWRpa29tLmNvbVxuICpcbiAqIExpY2Vuc2VkIE1JVFxuICovXG5cbihmdW5jdGlvbihmYWN0b3J5KSB7XG5cdGlmICh0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpIHtcblx0XHQvLyBBTURcblx0XHRkZWZpbmUoWydqcXVlcnknXSwgZmFjdG9yeSk7XG5cdH0gZWxzZSBpZiAodHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZS5leHBvcnRzID09PSAnb2JqZWN0Jykge1xuXHRcdC8vIENvbW1vbkpTXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KHJlcXVpcmUoJ2pxdWVyeScpKTtcblx0fSBlbHNlIHtcblx0XHQvLyBHbG9iYWwgalF1ZXJ5XG5cdFx0ZmFjdG9yeShqUXVlcnkpO1xuXHR9XG59IChmdW5jdGlvbigkKSB7XG5cblx0dmFyIG1lbnVUcmVlcyA9IFtdLFxuXHRcdG1vdXNlID0gZmFsc2UsIC8vIG9wdGltaXplIGZvciB0b3VjaCBieSBkZWZhdWx0IC0gd2Ugd2lsbCBkZXRlY3QgZm9yIG1vdXNlIGlucHV0XG5cdFx0dG91Y2hFdmVudHMgPSAnb250b3VjaHN0YXJ0JyBpbiB3aW5kb3csIC8vIHdlIHVzZSB0aGlzIGp1c3QgdG8gY2hvb3NlIGJldHdlZW4gdG91Y24gYW5kIHBvaW50ZXIgZXZlbnRzLCBub3QgZm9yIHRvdWNoIHNjcmVlbiBkZXRlY3Rpb25cblx0XHRtb3VzZURldGVjdGlvbkVuYWJsZWQgPSBmYWxzZSxcblx0XHRyZXF1ZXN0QW5pbWF0aW9uRnJhbWUgPSB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8IGZ1bmN0aW9uKGNhbGxiYWNrKSB7IHJldHVybiBzZXRUaW1lb3V0KGNhbGxiYWNrLCAxMDAwIC8gNjApOyB9LFxuXHRcdGNhbmNlbEFuaW1hdGlvbkZyYW1lID0gd2luZG93LmNhbmNlbEFuaW1hdGlvbkZyYW1lIHx8IGZ1bmN0aW9uKGlkKSB7IGNsZWFyVGltZW91dChpZCk7IH0sXG5cdFx0Y2FuQW5pbWF0ZSA9ICEhJC5mbi5hbmltYXRlO1xuXG5cdC8vIEhhbmRsZSBkZXRlY3Rpb24gZm9yIG1vdXNlIGlucHV0IChpLmUuIGRlc2t0b3AgYnJvd3NlcnMsIHRhYmxldHMgd2l0aCBhIG1vdXNlLCBldGMuKVxuXHRmdW5jdGlvbiBpbml0TW91c2VEZXRlY3Rpb24oZGlzYWJsZSkge1xuXHRcdHZhciBlTlMgPSAnLnNtYXJ0bWVudXNfbW91c2UnO1xuXHRcdGlmICghbW91c2VEZXRlY3Rpb25FbmFibGVkICYmICFkaXNhYmxlKSB7XG5cdFx0XHQvLyBpZiB3ZSBnZXQgdHdvIGNvbnNlY3V0aXZlIG1vdXNlbW92ZXMgd2l0aGluIDIgcGl4ZWxzIGZyb20gZWFjaCBvdGhlciBhbmQgd2l0aGluIDMwMG1zLCB3ZSBhc3N1bWUgYSByZWFsIG1vdXNlL2N1cnNvciBpcyBwcmVzZW50XG5cdFx0XHQvLyBpbiBwcmFjdGljZSwgdGhpcyBzZWVtcyBsaWtlIGltcG9zc2libGUgdG8gdHJpY2sgdW5pbnRlbnRpYW5hbGx5IHdpdGggYSByZWFsIG1vdXNlIGFuZCBhIHByZXR0eSBzYWZlIGRldGVjdGlvbiBvbiB0b3VjaCBkZXZpY2VzIChldmVuIHdpdGggb2xkZXIgYnJvd3NlcnMgdGhhdCBkbyBub3Qgc3VwcG9ydCB0b3VjaCBldmVudHMpXG5cdFx0XHR2YXIgZmlyc3RUaW1lID0gdHJ1ZSxcblx0XHRcdFx0bGFzdE1vdmUgPSBudWxsLFxuXHRcdFx0XHRldmVudHMgPSB7XG5cdFx0XHRcdFx0J21vdXNlbW92ZSc6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0XHRcdHZhciB0aGlzTW92ZSA9IHsgeDogZS5wYWdlWCwgeTogZS5wYWdlWSwgdGltZVN0YW1wOiBuZXcgRGF0ZSgpLmdldFRpbWUoKSB9O1xuXHRcdFx0XHRcdFx0aWYgKGxhc3RNb3ZlKSB7XG5cdFx0XHRcdFx0XHRcdHZhciBkZWx0YVggPSBNYXRoLmFicyhsYXN0TW92ZS54IC0gdGhpc01vdmUueCksXG5cdFx0XHRcdFx0XHRcdFx0ZGVsdGFZID0gTWF0aC5hYnMobGFzdE1vdmUueSAtIHRoaXNNb3ZlLnkpO1xuXHRcdCBcdFx0XHRcdFx0aWYgKChkZWx0YVggPiAwIHx8IGRlbHRhWSA+IDApICYmIGRlbHRhWCA8PSA0ICYmIGRlbHRhWSA8PSA0ICYmIHRoaXNNb3ZlLnRpbWVTdGFtcCAtIGxhc3RNb3ZlLnRpbWVTdGFtcCA8PSAzMDApIHtcblx0XHRcdFx0XHRcdFx0XHRtb3VzZSA9IHRydWU7XG5cdFx0XHRcdFx0XHRcdFx0Ly8gaWYgdGhpcyBpcyB0aGUgZmlyc3QgY2hlY2sgYWZ0ZXIgcGFnZSBsb2FkLCBjaGVjayBpZiB3ZSBhcmUgbm90IG92ZXIgc29tZSBpdGVtIGJ5IGNoYW5jZSBhbmQgY2FsbCB0aGUgbW91c2VlbnRlciBoYW5kbGVyIGlmIHllc1xuXHRcdFx0XHRcdFx0XHRcdGlmIChmaXJzdFRpbWUpIHtcblx0XHRcdFx0XHRcdFx0XHRcdHZhciAkYSA9ICQoZS50YXJnZXQpLmNsb3Nlc3QoJ2EnKTtcblx0XHRcdFx0XHRcdFx0XHRcdGlmICgkYS5pcygnYScpKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdCQuZWFjaChtZW51VHJlZXMsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGlmICgkLmNvbnRhaW5zKHRoaXMuJHJvb3RbMF0sICRhWzBdKSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0dGhpcy5pdGVtRW50ZXIoeyBjdXJyZW50VGFyZ2V0OiAkYVswXSB9KTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRcdFx0Zmlyc3RUaW1lID0gZmFsc2U7XG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRsYXN0TW92ZSA9IHRoaXNNb3ZlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fTtcblx0XHRcdGV2ZW50c1t0b3VjaEV2ZW50cyA/ICd0b3VjaHN0YXJ0JyA6ICdwb2ludGVyb3ZlciBwb2ludGVybW92ZSBwb2ludGVyb3V0IE1TUG9pbnRlck92ZXIgTVNQb2ludGVyTW92ZSBNU1BvaW50ZXJPdXQnXSA9IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0aWYgKGlzVG91Y2hFdmVudChlLm9yaWdpbmFsRXZlbnQpKSB7XG5cdFx0XHRcdFx0bW91c2UgPSBmYWxzZTtcblx0XHRcdFx0fVxuXHRcdFx0fTtcblx0XHRcdCQoZG9jdW1lbnQpLm9uKGdldEV2ZW50c05TKGV2ZW50cywgZU5TKSk7XG5cdFx0XHRtb3VzZURldGVjdGlvbkVuYWJsZWQgPSB0cnVlO1xuXHRcdH0gZWxzZSBpZiAobW91c2VEZXRlY3Rpb25FbmFibGVkICYmIGRpc2FibGUpIHtcblx0XHRcdCQoZG9jdW1lbnQpLm9mZihlTlMpO1xuXHRcdFx0bW91c2VEZXRlY3Rpb25FbmFibGVkID0gZmFsc2U7XG5cdFx0fVxuXHR9XG5cblx0ZnVuY3Rpb24gaXNUb3VjaEV2ZW50KGUpIHtcblx0XHRyZXR1cm4gIS9eKDR8bW91c2UpJC8udGVzdChlLnBvaW50ZXJUeXBlKTtcblx0fVxuXG5cdC8vIHJldHVybnMgYSBqUXVlcnkgb24oKSByZWFkeSBvYmplY3Rcblx0ZnVuY3Rpb24gZ2V0RXZlbnRzTlMoZXZlbnRzLCBlTlMpIHtcblx0XHRpZiAoIWVOUykge1xuXHRcdFx0ZU5TID0gJyc7XG5cdFx0fVxuXHRcdHZhciBldmVudHNOUyA9IHt9O1xuXHRcdGZvciAodmFyIGkgaW4gZXZlbnRzKSB7XG5cdFx0XHRldmVudHNOU1tpLnNwbGl0KCcgJykuam9pbihlTlMgKyAnICcpICsgZU5TXSA9IGV2ZW50c1tpXTtcblx0XHR9XG5cdFx0cmV0dXJuIGV2ZW50c05TO1xuXHR9XG5cblx0JC5TbWFydE1lbnVzID0gZnVuY3Rpb24oZWxtLCBvcHRpb25zKSB7XG5cdFx0dGhpcy4kcm9vdCA9ICQoZWxtKTtcblx0XHR0aGlzLm9wdHMgPSBvcHRpb25zO1xuXHRcdHRoaXMucm9vdElkID0gJyc7IC8vIGludGVybmFsXG5cdFx0dGhpcy5hY2Nlc3NJZFByZWZpeCA9ICcnO1xuXHRcdHRoaXMuJHN1YkFycm93ID0gbnVsbDtcblx0XHR0aGlzLmFjdGl2YXRlZEl0ZW1zID0gW107IC8vIHN0b3JlcyBsYXN0IGFjdGl2YXRlZCBBJ3MgZm9yIGVhY2ggbGV2ZWxcblx0XHR0aGlzLnZpc2libGVTdWJNZW51cyA9IFtdOyAvLyBzdG9yZXMgdmlzaWJsZSBzdWIgbWVudXMgVUwncyAobWlnaHQgYmUgaW4gbm8gcGFydGljdWxhciBvcmRlcilcblx0XHR0aGlzLnNob3dUaW1lb3V0ID0gMDtcblx0XHR0aGlzLmhpZGVUaW1lb3V0ID0gMDtcblx0XHR0aGlzLnNjcm9sbFRpbWVvdXQgPSAwO1xuXHRcdHRoaXMuY2xpY2tBY3RpdmF0ZWQgPSBmYWxzZTtcblx0XHR0aGlzLmZvY3VzQWN0aXZhdGVkID0gZmFsc2U7XG5cdFx0dGhpcy56SW5kZXhJbmMgPSAwO1xuXHRcdHRoaXMuaWRJbmMgPSAwO1xuXHRcdHRoaXMuJGZpcnN0TGluayA9IG51bGw7IC8vIHdlJ2xsIHVzZSB0aGVzZSBmb3Igc29tZSB0ZXN0c1xuXHRcdHRoaXMuJGZpcnN0U3ViID0gbnVsbDsgLy8gYXQgcnVudGltZSBzbyB3ZSdsbCBjYWNoZSB0aGVtXG5cdFx0dGhpcy5kaXNhYmxlZCA9IGZhbHNlO1xuXHRcdHRoaXMuJGRpc2FibGVPdmVybGF5ID0gbnVsbDtcblx0XHR0aGlzLiR0b3VjaFNjcm9sbGluZ1N1YiA9IG51bGw7XG5cdFx0dGhpcy5jc3NUcmFuc2Zvcm1zM2QgPSAncGVyc3BlY3RpdmUnIGluIGVsbS5zdHlsZSB8fCAnd2Via2l0UGVyc3BlY3RpdmUnIGluIGVsbS5zdHlsZTtcblx0XHR0aGlzLndhc0NvbGxhcHNpYmxlID0gZmFsc2U7XG5cdFx0dGhpcy5pbml0KCk7XG5cdH07XG5cblx0JC5leHRlbmQoJC5TbWFydE1lbnVzLCB7XG5cdFx0aGlkZUFsbDogZnVuY3Rpb24oKSB7XG5cdFx0XHQkLmVhY2gobWVudVRyZWVzLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0dGhpcy5tZW51SGlkZUFsbCgpO1xuXHRcdFx0fSk7XG5cdFx0fSxcblx0XHRkZXN0cm95OiBmdW5jdGlvbigpIHtcblx0XHRcdHdoaWxlIChtZW51VHJlZXMubGVuZ3RoKSB7XG5cdFx0XHRcdG1lbnVUcmVlc1swXS5kZXN0cm95KCk7XG5cdFx0XHR9XG5cdFx0XHRpbml0TW91c2VEZXRlY3Rpb24odHJ1ZSk7XG5cdFx0fSxcblx0XHRwcm90b3R5cGU6IHtcblx0XHRcdGluaXQ6IGZ1bmN0aW9uKHJlZnJlc2gpIHtcblx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXG5cdFx0XHRcdGlmICghcmVmcmVzaCkge1xuXHRcdFx0XHRcdG1lbnVUcmVlcy5wdXNoKHRoaXMpO1xuXG5cdFx0XHRcdFx0dGhpcy5yb290SWQgPSAobmV3IERhdGUoKS5nZXRUaW1lKCkgKyBNYXRoLnJhbmRvbSgpICsgJycpLnJlcGxhY2UoL1xcRC9nLCAnJyk7XG5cdFx0XHRcdFx0dGhpcy5hY2Nlc3NJZFByZWZpeCA9ICdzbS0nICsgdGhpcy5yb290SWQgKyAnLSc7XG5cblx0XHRcdFx0XHRpZiAodGhpcy4kcm9vdC5oYXNDbGFzcygnc20tcnRsJykpIHtcblx0XHRcdFx0XHRcdHRoaXMub3B0cy5yaWdodFRvTGVmdFN1Yk1lbnVzID0gdHJ1ZTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHQvLyBpbml0IHJvb3QgKG1haW4gbWVudSlcblx0XHRcdFx0XHR2YXIgZU5TID0gJy5zbWFydG1lbnVzJztcblx0XHRcdFx0XHR0aGlzLiRyb290XG5cdFx0XHRcdFx0XHQuZGF0YSgnc21hcnRtZW51cycsIHRoaXMpXG5cdFx0XHRcdFx0XHQuYXR0cignZGF0YS1zbWFydG1lbnVzLWlkJywgdGhpcy5yb290SWQpXG5cdFx0XHRcdFx0XHQuZGF0YVNNKCdsZXZlbCcsIDEpXG5cdFx0XHRcdFx0XHQub24oZ2V0RXZlbnRzTlMoe1xuXHRcdFx0XHRcdFx0XHQnbW91c2VvdmVyIGZvY3VzaW4nOiAkLnByb3h5KHRoaXMucm9vdE92ZXIsIHRoaXMpLFxuXHRcdFx0XHRcdFx0XHQnbW91c2VvdXQgZm9jdXNvdXQnOiAkLnByb3h5KHRoaXMucm9vdE91dCwgdGhpcyksXG5cdFx0XHRcdFx0XHRcdCdrZXlkb3duJzogJC5wcm94eSh0aGlzLnJvb3RLZXlEb3duLCB0aGlzKVxuXHRcdFx0XHRcdFx0fSwgZU5TKSlcblx0XHRcdFx0XHRcdC5vbihnZXRFdmVudHNOUyh7XG5cdFx0XHRcdFx0XHRcdCdtb3VzZWVudGVyJzogJC5wcm94eSh0aGlzLml0ZW1FbnRlciwgdGhpcyksXG5cdFx0XHRcdFx0XHRcdCdtb3VzZWxlYXZlJzogJC5wcm94eSh0aGlzLml0ZW1MZWF2ZSwgdGhpcyksXG5cdFx0XHRcdFx0XHRcdCdtb3VzZWRvd24nOiAkLnByb3h5KHRoaXMuaXRlbURvd24sIHRoaXMpLFxuXHRcdFx0XHRcdFx0XHQnZm9jdXMnOiAkLnByb3h5KHRoaXMuaXRlbUZvY3VzLCB0aGlzKSxcblx0XHRcdFx0XHRcdFx0J2JsdXInOiAkLnByb3h5KHRoaXMuaXRlbUJsdXIsIHRoaXMpLFxuXHRcdFx0XHRcdFx0XHQnY2xpY2snOiAkLnByb3h5KHRoaXMuaXRlbUNsaWNrLCB0aGlzKVxuXHRcdFx0XHRcdFx0fSwgZU5TKSwgJ2EnKTtcblxuXHRcdFx0XHRcdC8vIGhpZGUgbWVudXMgb24gdGFwIG9yIGNsaWNrIG91dHNpZGUgdGhlIHJvb3QgVUxcblx0XHRcdFx0XHRlTlMgKz0gdGhpcy5yb290SWQ7XG5cdFx0XHRcdFx0aWYgKHRoaXMub3B0cy5oaWRlT25DbGljaykge1xuXHRcdFx0XHRcdFx0JChkb2N1bWVudCkub24oZ2V0RXZlbnRzTlMoe1xuXHRcdFx0XHRcdFx0XHQndG91Y2hzdGFydCc6ICQucHJveHkodGhpcy5kb2NUb3VjaFN0YXJ0LCB0aGlzKSxcblx0XHRcdFx0XHRcdFx0J3RvdWNobW92ZSc6ICQucHJveHkodGhpcy5kb2NUb3VjaE1vdmUsIHRoaXMpLFxuXHRcdFx0XHRcdFx0XHQndG91Y2hlbmQnOiAkLnByb3h5KHRoaXMuZG9jVG91Y2hFbmQsIHRoaXMpLFxuXHRcdFx0XHRcdFx0XHQvLyBmb3IgT3BlcmEgTW9iaWxlIDwgMTEuNSwgd2ViT1MgYnJvd3NlciwgZXRjLiB3ZSdsbCBjaGVjayBjbGljayB0b29cblx0XHRcdFx0XHRcdFx0J2NsaWNrJzogJC5wcm94eSh0aGlzLmRvY0NsaWNrLCB0aGlzKVxuXHRcdFx0XHRcdFx0fSwgZU5TKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdC8vIGhpZGUgc3ViIG1lbnVzIG9uIHJlc2l6ZVxuXHRcdFx0XHRcdCQod2luZG93KS5vbihnZXRFdmVudHNOUyh7ICdyZXNpemUgb3JpZW50YXRpb25jaGFuZ2UnOiAkLnByb3h5KHRoaXMud2luUmVzaXplLCB0aGlzKSB9LCBlTlMpKTtcblxuXHRcdFx0XHRcdGlmICh0aGlzLm9wdHMuc3ViSW5kaWNhdG9ycykge1xuXHRcdFx0XHRcdFx0dGhpcy4kc3ViQXJyb3cgPSAkKCc8c3Bhbi8+JykuYWRkQ2xhc3MoJ3N1Yi1hcnJvdycpO1xuXHRcdFx0XHRcdFx0aWYgKHRoaXMub3B0cy5zdWJJbmRpY2F0b3JzVGV4dCkge1xuXHRcdFx0XHRcdFx0XHR0aGlzLiRzdWJBcnJvdy5odG1sKHRoaXMub3B0cy5zdWJJbmRpY2F0b3JzVGV4dCk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0Ly8gbWFrZSBzdXJlIG1vdXNlIGRldGVjdGlvbiBpcyBlbmFibGVkXG5cdFx0XHRcdFx0aW5pdE1vdXNlRGV0ZWN0aW9uKCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvLyBpbml0IHN1YiBtZW51c1xuXHRcdFx0XHR0aGlzLiRmaXJzdFN1YiA9IHRoaXMuJHJvb3QuZmluZCgndWwnKS5lYWNoKGZ1bmN0aW9uKCkgeyBzZWxmLm1lbnVJbml0KCQodGhpcykpOyB9KS5lcSgwKTtcblxuXHRcdFx0XHR0aGlzLiRmaXJzdExpbmsgPSB0aGlzLiRyb290LmZpbmQoJ2EnKS5lcSgwKTtcblxuXHRcdFx0XHQvLyBmaW5kIGN1cnJlbnQgaXRlbVxuXHRcdFx0XHRpZiAodGhpcy5vcHRzLm1hcmtDdXJyZW50SXRlbSkge1xuXHRcdFx0XHRcdHZhciByZURlZmF1bHREb2MgPSAvKGluZGV4fGRlZmF1bHQpXFwuW14jXFw/XFwvXSovaSxcblx0XHRcdFx0XHRcdHJlSGFzaCA9IC8jLiovLFxuXHRcdFx0XHRcdFx0bG9jSHJlZiA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmLnJlcGxhY2UocmVEZWZhdWx0RG9jLCAnJyksXG5cdFx0XHRcdFx0XHRsb2NIcmVmTm9IYXNoID0gbG9jSHJlZi5yZXBsYWNlKHJlSGFzaCwgJycpO1xuXHRcdFx0XHRcdHRoaXMuJHJvb3QuZmluZCgnYTpub3QoLm1lZ2EtbWVudSBhKScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHR2YXIgaHJlZiA9IHRoaXMuaHJlZi5yZXBsYWNlKHJlRGVmYXVsdERvYywgJycpLFxuXHRcdFx0XHRcdFx0XHQkdGhpcyA9ICQodGhpcyk7XG5cdFx0XHRcdFx0XHRpZiAoaHJlZiA9PSBsb2NIcmVmIHx8IGhyZWYgPT0gbG9jSHJlZk5vSGFzaCkge1xuXHRcdFx0XHRcdFx0XHQkdGhpcy5hZGRDbGFzcygnY3VycmVudCcpO1xuXHRcdFx0XHRcdFx0XHRpZiAoc2VsZi5vcHRzLm1hcmtDdXJyZW50VHJlZSkge1xuXHRcdFx0XHRcdFx0XHRcdCR0aGlzLnBhcmVudHNVbnRpbCgnW2RhdGEtc21hcnRtZW51cy1pZF0nLCAndWwnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0JCh0aGlzKS5kYXRhU00oJ3BhcmVudC1hJykuYWRkQ2xhc3MoJ2N1cnJlbnQnKTtcblx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly8gc2F2ZSBpbml0aWFsIHN0YXRlXG5cdFx0XHRcdHRoaXMud2FzQ29sbGFwc2libGUgPSB0aGlzLmlzQ29sbGFwc2libGUoKTtcblx0XHRcdH0sXG5cdFx0XHRkZXN0cm95OiBmdW5jdGlvbihyZWZyZXNoKSB7XG5cdFx0XHRcdGlmICghcmVmcmVzaCkge1xuXHRcdFx0XHRcdHZhciBlTlMgPSAnLnNtYXJ0bWVudXMnO1xuXHRcdFx0XHRcdHRoaXMuJHJvb3Rcblx0XHRcdFx0XHRcdC5yZW1vdmVEYXRhKCdzbWFydG1lbnVzJylcblx0XHRcdFx0XHRcdC5yZW1vdmVBdHRyKCdkYXRhLXNtYXJ0bWVudXMtaWQnKVxuXHRcdFx0XHRcdFx0LnJlbW92ZURhdGFTTSgnbGV2ZWwnKVxuXHRcdFx0XHRcdFx0Lm9mZihlTlMpO1xuXHRcdFx0XHRcdGVOUyArPSB0aGlzLnJvb3RJZDtcblx0XHRcdFx0XHQkKGRvY3VtZW50KS5vZmYoZU5TKTtcblx0XHRcdFx0XHQkKHdpbmRvdykub2ZmKGVOUyk7XG5cdFx0XHRcdFx0aWYgKHRoaXMub3B0cy5zdWJJbmRpY2F0b3JzKSB7XG5cdFx0XHRcdFx0XHR0aGlzLiRzdWJBcnJvdyA9IG51bGw7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRcdHRoaXMubWVudUhpZGVBbGwoKTtcblx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XHR0aGlzLiRyb290LmZpbmQoJ3VsJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XHRcdFx0XHRpZiAoJHRoaXMuZGF0YVNNKCdzY3JvbGwtYXJyb3dzJykpIHtcblx0XHRcdFx0XHRcdFx0JHRoaXMuZGF0YVNNKCdzY3JvbGwtYXJyb3dzJykucmVtb3ZlKCk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRpZiAoJHRoaXMuZGF0YVNNKCdzaG93bi1iZWZvcmUnKSkge1xuXHRcdFx0XHRcdFx0XHRpZiAoc2VsZi5vcHRzLnN1Yk1lbnVzTWluV2lkdGggfHwgc2VsZi5vcHRzLnN1Yk1lbnVzTWF4V2lkdGgpIHtcblx0XHRcdFx0XHRcdFx0XHQkdGhpcy5jc3MoeyB3aWR0aDogJycsIG1pbldpZHRoOiAnJywgbWF4V2lkdGg6ICcnIH0pLnJlbW92ZUNsYXNzKCdzbS1ub3dyYXAnKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRpZiAoJHRoaXMuZGF0YVNNKCdzY3JvbGwtYXJyb3dzJykpIHtcblx0XHRcdFx0XHRcdFx0XHQkdGhpcy5kYXRhU00oJ3Njcm9sbC1hcnJvd3MnKS5yZW1vdmUoKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHQkdGhpcy5jc3MoeyB6SW5kZXg6ICcnLCB0b3A6ICcnLCBsZWZ0OiAnJywgbWFyZ2luTGVmdDogJycsIG1hcmdpblRvcDogJycsIGRpc3BsYXk6ICcnIH0pO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0aWYgKCgkdGhpcy5hdHRyKCdpZCcpIHx8ICcnKS5pbmRleE9mKHNlbGYuYWNjZXNzSWRQcmVmaXgpID09IDApIHtcblx0XHRcdFx0XHRcdFx0JHRoaXMucmVtb3ZlQXR0cignaWQnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5yZW1vdmVEYXRhU00oJ2luLW1lZ2EnKVxuXHRcdFx0XHRcdC5yZW1vdmVEYXRhU00oJ3Nob3duLWJlZm9yZScpXG5cdFx0XHRcdFx0LnJlbW92ZURhdGFTTSgnc2Nyb2xsLWFycm93cycpXG5cdFx0XHRcdFx0LnJlbW92ZURhdGFTTSgncGFyZW50LWEnKVxuXHRcdFx0XHRcdC5yZW1vdmVEYXRhU00oJ2xldmVsJylcblx0XHRcdFx0XHQucmVtb3ZlRGF0YVNNKCdiZWZvcmVmaXJzdHNob3dmaXJlZCcpXG5cdFx0XHRcdFx0LnJlbW92ZUF0dHIoJ3JvbGUnKVxuXHRcdFx0XHRcdC5yZW1vdmVBdHRyKCdhcmlhLWhpZGRlbicpXG5cdFx0XHRcdFx0LnJlbW92ZUF0dHIoJ2FyaWEtbGFiZWxsZWRieScpXG5cdFx0XHRcdFx0LnJlbW92ZUF0dHIoJ2FyaWEtZXhwYW5kZWQnKTtcblx0XHRcdFx0dGhpcy4kcm9vdC5maW5kKCdhLmhhcy1zdWJtZW51JykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyk7XG5cdFx0XHRcdFx0XHRpZiAoJHRoaXMuYXR0cignaWQnKS5pbmRleE9mKHNlbGYuYWNjZXNzSWRQcmVmaXgpID09IDApIHtcblx0XHRcdFx0XHRcdFx0JHRoaXMucmVtb3ZlQXR0cignaWQnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5yZW1vdmVDbGFzcygnaGFzLXN1Ym1lbnUnKVxuXHRcdFx0XHRcdC5yZW1vdmVEYXRhU00oJ3N1YicpXG5cdFx0XHRcdFx0LnJlbW92ZUF0dHIoJ2FyaWEtaGFzcG9wdXAnKVxuXHRcdFx0XHRcdC5yZW1vdmVBdHRyKCdhcmlhLWNvbnRyb2xzJylcblx0XHRcdFx0XHQucmVtb3ZlQXR0cignYXJpYS1leHBhbmRlZCcpXG5cdFx0XHRcdFx0LmNsb3Nlc3QoJ2xpJykucmVtb3ZlRGF0YVNNKCdzdWInKTtcblx0XHRcdFx0aWYgKHRoaXMub3B0cy5zdWJJbmRpY2F0b3JzKSB7XG5cdFx0XHRcdFx0dGhpcy4kcm9vdC5maW5kKCdzcGFuLnN1Yi1hcnJvdycpLnJlbW92ZSgpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0aGlzLm9wdHMubWFya0N1cnJlbnRJdGVtKSB7XG5cdFx0XHRcdFx0dGhpcy4kcm9vdC5maW5kKCdhLmN1cnJlbnQnKS5yZW1vdmVDbGFzcygnY3VycmVudCcpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICghcmVmcmVzaCkge1xuXHRcdFx0XHRcdHRoaXMuJHJvb3QgPSBudWxsO1xuXHRcdFx0XHRcdHRoaXMuJGZpcnN0TGluayA9IG51bGw7XG5cdFx0XHRcdFx0dGhpcy4kZmlyc3RTdWIgPSBudWxsO1xuXHRcdFx0XHRcdGlmICh0aGlzLiRkaXNhYmxlT3ZlcmxheSkge1xuXHRcdFx0XHRcdFx0dGhpcy4kZGlzYWJsZU92ZXJsYXkucmVtb3ZlKCk7XG5cdFx0XHRcdFx0XHR0aGlzLiRkaXNhYmxlT3ZlcmxheSA9IG51bGw7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdG1lbnVUcmVlcy5zcGxpY2UoJC5pbkFycmF5KHRoaXMsIG1lbnVUcmVlcyksIDEpO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0ZGlzYWJsZTogZnVuY3Rpb24obm9PdmVybGF5KSB7XG5cdFx0XHRcdGlmICghdGhpcy5kaXNhYmxlZCkge1xuXHRcdFx0XHRcdHRoaXMubWVudUhpZGVBbGwoKTtcblx0XHRcdFx0XHQvLyBkaXNwbGF5IG92ZXJsYXkgb3ZlciB0aGUgbWVudSB0byBwcmV2ZW50IGludGVyYWN0aW9uXG5cdFx0XHRcdFx0aWYgKCFub092ZXJsYXkgJiYgIXRoaXMub3B0cy5pc1BvcHVwICYmIHRoaXMuJHJvb3QuaXMoJzp2aXNpYmxlJykpIHtcblx0XHRcdFx0XHRcdHZhciBwb3MgPSB0aGlzLiRyb290Lm9mZnNldCgpO1xuXHRcdFx0XHRcdFx0dGhpcy4kZGlzYWJsZU92ZXJsYXkgPSAkKCc8ZGl2IGNsYXNzPVwic20tanF1ZXJ5LWRpc2FibGUtb3ZlcmxheVwiLz4nKS5jc3Moe1xuXHRcdFx0XHRcdFx0XHRwb3NpdGlvbjogJ2Fic29sdXRlJyxcblx0XHRcdFx0XHRcdFx0dG9wOiBwb3MudG9wLFxuXHRcdFx0XHRcdFx0XHRsZWZ0OiBwb3MubGVmdCxcblx0XHRcdFx0XHRcdFx0d2lkdGg6IHRoaXMuJHJvb3Qub3V0ZXJXaWR0aCgpLFxuXHRcdFx0XHRcdFx0XHRoZWlnaHQ6IHRoaXMuJHJvb3Qub3V0ZXJIZWlnaHQoKSxcblx0XHRcdFx0XHRcdFx0ekluZGV4OiB0aGlzLmdldFN0YXJ0WkluZGV4KHRydWUpLFxuXHRcdFx0XHRcdFx0XHRvcGFjaXR5OiAwXG5cdFx0XHRcdFx0XHR9KS5hcHBlbmRUbyhkb2N1bWVudC5ib2R5KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dGhpcy5kaXNhYmxlZCA9IHRydWU7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRkb2NDbGljazogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRpZiAodGhpcy4kdG91Y2hTY3JvbGxpbmdTdWIpIHtcblx0XHRcdFx0XHR0aGlzLiR0b3VjaFNjcm9sbGluZ1N1YiA9IG51bGw7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdC8vIGhpZGUgb24gYW55IGNsaWNrIG91dHNpZGUgdGhlIG1lbnUgb3Igb24gYSBtZW51IGxpbmtcblx0XHRcdFx0aWYgKHRoaXMudmlzaWJsZVN1Yk1lbnVzLmxlbmd0aCAmJiAhJC5jb250YWlucyh0aGlzLiRyb290WzBdLCBlLnRhcmdldCkgfHwgJChlLnRhcmdldCkuY2xvc2VzdCgnYScpLmxlbmd0aCkge1xuXHRcdFx0XHRcdHRoaXMubWVudUhpZGVBbGwoKTtcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdGRvY1RvdWNoRW5kOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdGlmICghdGhpcy5sYXN0VG91Y2gpIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKHRoaXMudmlzaWJsZVN1Yk1lbnVzLmxlbmd0aCAmJiAodGhpcy5sYXN0VG91Y2gueDIgPT09IHVuZGVmaW5lZCB8fCB0aGlzLmxhc3RUb3VjaC54MSA9PSB0aGlzLmxhc3RUb3VjaC54MikgJiYgKHRoaXMubGFzdFRvdWNoLnkyID09PSB1bmRlZmluZWQgfHwgdGhpcy5sYXN0VG91Y2gueTEgPT0gdGhpcy5sYXN0VG91Y2gueTIpICYmICghdGhpcy5sYXN0VG91Y2gudGFyZ2V0IHx8ICEkLmNvbnRhaW5zKHRoaXMuJHJvb3RbMF0sIHRoaXMubGFzdFRvdWNoLnRhcmdldCkpKSB7XG5cdFx0XHRcdFx0aWYgKHRoaXMuaGlkZVRpbWVvdXQpIHtcblx0XHRcdFx0XHRcdGNsZWFyVGltZW91dCh0aGlzLmhpZGVUaW1lb3V0KTtcblx0XHRcdFx0XHRcdHRoaXMuaGlkZVRpbWVvdXQgPSAwO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQvLyBoaWRlIHdpdGggYSBkZWxheSB0byBwcmV2ZW50IHRyaWdnZXJpbmcgYWNjaWRlbnRhbCB1bndhbnRlZCBjbGljayBvbiBzb21lIHBhZ2UgZWxlbWVudFxuXHRcdFx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdFx0XHR0aGlzLmhpZGVUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHsgc2VsZi5tZW51SGlkZUFsbCgpOyB9LCAzNTApO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHRoaXMubGFzdFRvdWNoID0gbnVsbDtcblx0XHRcdH0sXG5cdFx0XHRkb2NUb3VjaE1vdmU6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0aWYgKCF0aGlzLmxhc3RUb3VjaCkge1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHR2YXIgdG91Y2hQb2ludCA9IGUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdO1xuXHRcdFx0XHR0aGlzLmxhc3RUb3VjaC54MiA9IHRvdWNoUG9pbnQucGFnZVg7XG5cdFx0XHRcdHRoaXMubGFzdFRvdWNoLnkyID0gdG91Y2hQb2ludC5wYWdlWTtcblx0XHRcdH0sXG5cdFx0XHRkb2NUb3VjaFN0YXJ0OiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdHZhciB0b3VjaFBvaW50ID0gZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF07XG5cdFx0XHRcdHRoaXMubGFzdFRvdWNoID0geyB4MTogdG91Y2hQb2ludC5wYWdlWCwgeTE6IHRvdWNoUG9pbnQucGFnZVksIHRhcmdldDogdG91Y2hQb2ludC50YXJnZXQgfTtcblx0XHRcdH0sXG5cdFx0XHRlbmFibGU6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRpZiAodGhpcy5kaXNhYmxlZCkge1xuXHRcdFx0XHRcdGlmICh0aGlzLiRkaXNhYmxlT3ZlcmxheSkge1xuXHRcdFx0XHRcdFx0dGhpcy4kZGlzYWJsZU92ZXJsYXkucmVtb3ZlKCk7XG5cdFx0XHRcdFx0XHR0aGlzLiRkaXNhYmxlT3ZlcmxheSA9IG51bGw7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHRoaXMuZGlzYWJsZWQgPSBmYWxzZTtcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdGdldENsb3Nlc3RNZW51OiBmdW5jdGlvbihlbG0pIHtcblx0XHRcdFx0dmFyICRjbG9zZXN0TWVudSA9ICQoZWxtKS5jbG9zZXN0KCd1bCcpO1xuXHRcdFx0XHR3aGlsZSAoJGNsb3Nlc3RNZW51LmRhdGFTTSgnaW4tbWVnYScpKSB7XG5cdFx0XHRcdFx0JGNsb3Nlc3RNZW51ID0gJGNsb3Nlc3RNZW51LnBhcmVudCgpLmNsb3Nlc3QoJ3VsJyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0cmV0dXJuICRjbG9zZXN0TWVudVswXSB8fCBudWxsO1xuXHRcdFx0fSxcblx0XHRcdGdldEhlaWdodDogZnVuY3Rpb24oJGVsbSkge1xuXHRcdFx0XHRyZXR1cm4gdGhpcy5nZXRPZmZzZXQoJGVsbSwgdHJ1ZSk7XG5cdFx0XHR9LFxuXHRcdFx0Ly8gcmV0dXJucyBwcmVjaXNlIHdpZHRoL2hlaWdodCBmbG9hdCB2YWx1ZXNcblx0XHRcdGdldE9mZnNldDogZnVuY3Rpb24oJGVsbSwgaGVpZ2h0KSB7XG5cdFx0XHRcdHZhciBvbGQ7XG5cdFx0XHRcdGlmICgkZWxtLmNzcygnZGlzcGxheScpID09ICdub25lJykge1xuXHRcdFx0XHRcdG9sZCA9IHsgcG9zaXRpb246ICRlbG1bMF0uc3R5bGUucG9zaXRpb24sIHZpc2liaWxpdHk6ICRlbG1bMF0uc3R5bGUudmlzaWJpbGl0eSB9O1xuXHRcdFx0XHRcdCRlbG0uY3NzKHsgcG9zaXRpb246ICdhYnNvbHV0ZScsIHZpc2liaWxpdHk6ICdoaWRkZW4nIH0pLnNob3coKTtcblx0XHRcdFx0fVxuXHRcdFx0XHR2YXIgYm94ID0gJGVsbVswXS5nZXRCb3VuZGluZ0NsaWVudFJlY3QgJiYgJGVsbVswXS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxcblx0XHRcdFx0XHR2YWwgPSBib3ggJiYgKGhlaWdodCA/IGJveC5oZWlnaHQgfHwgYm94LmJvdHRvbSAtIGJveC50b3AgOiBib3gud2lkdGggfHwgYm94LnJpZ2h0IC0gYm94LmxlZnQpO1xuXHRcdFx0XHRpZiAoIXZhbCAmJiB2YWwgIT09IDApIHtcblx0XHRcdFx0XHR2YWwgPSBoZWlnaHQgPyAkZWxtWzBdLm9mZnNldEhlaWdodCA6ICRlbG1bMF0ub2Zmc2V0V2lkdGg7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKG9sZCkge1xuXHRcdFx0XHRcdCRlbG0uaGlkZSgpLmNzcyhvbGQpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHJldHVybiB2YWw7XG5cdFx0XHR9LFxuXHRcdFx0Z2V0U3RhcnRaSW5kZXg6IGZ1bmN0aW9uKHJvb3QpIHtcblx0XHRcdFx0dmFyIHpJbmRleCA9IHBhcnNlSW50KHRoaXNbcm9vdCA/ICckcm9vdCcgOiAnJGZpcnN0U3ViJ10uY3NzKCd6LWluZGV4JykpO1xuXHRcdFx0XHRpZiAoIXJvb3QgJiYgaXNOYU4oekluZGV4KSkge1xuXHRcdFx0XHRcdHpJbmRleCA9IHBhcnNlSW50KHRoaXMuJHJvb3QuY3NzKCd6LWluZGV4JykpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHJldHVybiAhaXNOYU4oekluZGV4KSA/IHpJbmRleCA6IDE7XG5cdFx0XHR9LFxuXHRcdFx0Z2V0VG91Y2hQb2ludDogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRyZXR1cm4gZS50b3VjaGVzICYmIGUudG91Y2hlc1swXSB8fCBlLmNoYW5nZWRUb3VjaGVzICYmIGUuY2hhbmdlZFRvdWNoZXNbMF0gfHwgZTtcblx0XHRcdH0sXG5cdFx0XHRnZXRWaWV3cG9ydDogZnVuY3Rpb24oaGVpZ2h0KSB7XG5cdFx0XHRcdHZhciBuYW1lID0gaGVpZ2h0ID8gJ0hlaWdodCcgOiAnV2lkdGgnLFxuXHRcdFx0XHRcdHZhbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudFsnY2xpZW50JyArIG5hbWVdLFxuXHRcdFx0XHRcdHZhbDIgPSB3aW5kb3dbJ2lubmVyJyArIG5hbWVdO1xuXHRcdFx0XHRpZiAodmFsMikge1xuXHRcdFx0XHRcdHZhbCA9IE1hdGgubWluKHZhbCwgdmFsMik7XG5cdFx0XHRcdH1cblx0XHRcdFx0cmV0dXJuIHZhbDtcblx0XHRcdH0sXG5cdFx0XHRnZXRWaWV3cG9ydEhlaWdodDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLmdldFZpZXdwb3J0KHRydWUpO1xuXHRcdFx0fSxcblx0XHRcdGdldFZpZXdwb3J0V2lkdGg6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gdGhpcy5nZXRWaWV3cG9ydCgpO1xuXHRcdFx0fSxcblx0XHRcdGdldFdpZHRoOiBmdW5jdGlvbigkZWxtKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLmdldE9mZnNldCgkZWxtKTtcblx0XHRcdH0sXG5cdFx0XHRoYW5kbGVFdmVudHM6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gIXRoaXMuZGlzYWJsZWQgJiYgdGhpcy5pc0NTU09uKCk7XG5cdFx0XHR9LFxuXHRcdFx0aGFuZGxlSXRlbUV2ZW50czogZnVuY3Rpb24oJGEpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMuaGFuZGxlRXZlbnRzKCkgJiYgIXRoaXMuaXNMaW5rSW5NZWdhTWVudSgkYSk7XG5cdFx0XHR9LFxuXHRcdFx0aXNDb2xsYXBzaWJsZTogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLiRmaXJzdFN1Yi5jc3MoJ3Bvc2l0aW9uJykgPT0gJ3N0YXRpYyc7XG5cdFx0XHR9LFxuXHRcdFx0aXNDU1NPbjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLiRmaXJzdExpbmsuY3NzKCdkaXNwbGF5JykgIT0gJ2lubGluZSc7XG5cdFx0XHR9LFxuXHRcdFx0aXNGaXhlZDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHZhciBpc0ZpeGVkID0gdGhpcy4kcm9vdC5jc3MoJ3Bvc2l0aW9uJykgPT0gJ2ZpeGVkJztcblx0XHRcdFx0aWYgKCFpc0ZpeGVkKSB7XG5cdFx0XHRcdFx0dGhpcy4kcm9vdC5wYXJlbnRzVW50aWwoJ2JvZHknKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0aWYgKCQodGhpcykuY3NzKCdwb3NpdGlvbicpID09ICdmaXhlZCcpIHtcblx0XHRcdFx0XHRcdFx0aXNGaXhlZCA9IHRydWU7XG5cdFx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fVxuXHRcdFx0XHRyZXR1cm4gaXNGaXhlZDtcblx0XHRcdH0sXG5cdFx0XHRpc0xpbmtJbk1lZ2FNZW51OiBmdW5jdGlvbigkYSkge1xuXHRcdFx0XHRyZXR1cm4gJCh0aGlzLmdldENsb3Nlc3RNZW51KCRhWzBdKSkuaGFzQ2xhc3MoJ21lZ2EtbWVudScpO1xuXHRcdFx0fSxcblx0XHRcdGlzVG91Y2hNb2RlOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0cmV0dXJuICFtb3VzZSB8fCB0aGlzLm9wdHMubm9Nb3VzZU92ZXIgfHwgdGhpcy5pc0NvbGxhcHNpYmxlKCk7XG5cdFx0XHR9LFxuXHRcdFx0aXRlbUFjdGl2YXRlOiBmdW5jdGlvbigkYSwgaGlkZURlZXBlclN1YnMpIHtcblx0XHRcdFx0dmFyICR1bCA9ICRhLmNsb3Nlc3QoJ3VsJyksXG5cdFx0XHRcdFx0bGV2ZWwgPSAkdWwuZGF0YVNNKCdsZXZlbCcpO1xuXHRcdFx0XHQvLyBpZiBmb3Igc29tZSByZWFzb24gdGhlIHBhcmVudCBpdGVtIGlzIG5vdCBhY3RpdmF0ZWQgKGUuZy4gdGhpcyBpcyBhbiBBUEkgY2FsbCB0byBhY3RpdmF0ZSB0aGUgaXRlbSksIGFjdGl2YXRlIGFsbCBwYXJlbnQgaXRlbXMgZmlyc3Rcblx0XHRcdFx0aWYgKGxldmVsID4gMSAmJiAoIXRoaXMuYWN0aXZhdGVkSXRlbXNbbGV2ZWwgLSAyXSB8fCB0aGlzLmFjdGl2YXRlZEl0ZW1zW2xldmVsIC0gMl1bMF0gIT0gJHVsLmRhdGFTTSgncGFyZW50LWEnKVswXSkpIHtcblx0XHRcdFx0XHR2YXIgc2VsZiA9IHRoaXM7XG5cdFx0XHRcdFx0JCgkdWwucGFyZW50c1VudGlsKCdbZGF0YS1zbWFydG1lbnVzLWlkXScsICd1bCcpLmdldCgpLnJldmVyc2UoKSkuYWRkKCR1bCkuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdHNlbGYuaXRlbUFjdGl2YXRlKCQodGhpcykuZGF0YVNNKCdwYXJlbnQtYScpKTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fVxuXHRcdFx0XHQvLyBoaWRlIGFueSB2aXNpYmxlIGRlZXBlciBsZXZlbCBzdWIgbWVudXNcblx0XHRcdFx0aWYgKCF0aGlzLmlzQ29sbGFwc2libGUoKSB8fCBoaWRlRGVlcGVyU3Vicykge1xuXHRcdFx0XHRcdHRoaXMubWVudUhpZGVTdWJNZW51cyghdGhpcy5hY3RpdmF0ZWRJdGVtc1tsZXZlbCAtIDFdIHx8IHRoaXMuYWN0aXZhdGVkSXRlbXNbbGV2ZWwgLSAxXVswXSAhPSAkYVswXSA/IGxldmVsIC0gMSA6IGxldmVsKTtcblx0XHRcdFx0fVxuXHRcdFx0XHQvLyBzYXZlIG5ldyBhY3RpdmUgaXRlbSBmb3IgdGhpcyBsZXZlbFxuXHRcdFx0XHR0aGlzLmFjdGl2YXRlZEl0ZW1zW2xldmVsIC0gMV0gPSAkYTtcblx0XHRcdFx0aWYgKHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ2FjdGl2YXRlLnNtYXBpJywgJGFbMF0pID09PSBmYWxzZSkge1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHQvLyBzaG93IHRoZSBzdWIgbWVudSBpZiB0aGlzIGl0ZW0gaGFzIG9uZVxuXHRcdFx0XHR2YXIgJHN1YiA9ICRhLmRhdGFTTSgnc3ViJyk7XG5cdFx0XHRcdGlmICgkc3ViICYmICh0aGlzLmlzVG91Y2hNb2RlKCkgfHwgKCF0aGlzLm9wdHMuc2hvd09uQ2xpY2sgfHwgdGhpcy5jbGlja0FjdGl2YXRlZCkpKSB7XG5cdFx0XHRcdFx0dGhpcy5tZW51U2hvdygkc3ViKTtcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdGl0ZW1CbHVyOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdHZhciAkYSA9ICQoZS5jdXJyZW50VGFyZ2V0KTtcblx0XHRcdFx0aWYgKCF0aGlzLmhhbmRsZUl0ZW1FdmVudHMoJGEpKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ2JsdXIuc21hcGknLCAkYVswXSk7XG5cdFx0XHR9LFxuXHRcdFx0aXRlbUNsaWNrOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdHZhciAkYSA9ICQoZS5jdXJyZW50VGFyZ2V0KTtcblx0XHRcdFx0aWYgKCF0aGlzLmhhbmRsZUl0ZW1FdmVudHMoJGEpKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0aGlzLiR0b3VjaFNjcm9sbGluZ1N1YiAmJiB0aGlzLiR0b3VjaFNjcm9sbGluZ1N1YlswXSA9PSAkYS5jbG9zZXN0KCd1bCcpWzBdKSB7XG5cdFx0XHRcdFx0dGhpcy4kdG91Y2hTY3JvbGxpbmdTdWIgPSBudWxsO1xuXHRcdFx0XHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdjbGljay5zbWFwaScsICRhWzBdKSA9PT0gZmFsc2UpIHtcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdH1cblx0XHRcdFx0dmFyICRzdWIgPSAkYS5kYXRhU00oJ3N1YicpLFxuXHRcdFx0XHRcdGZpcnN0TGV2ZWxTdWIgPSAkc3ViID8gJHN1Yi5kYXRhU00oJ2xldmVsJykgPT0gMiA6IGZhbHNlO1xuXHRcdFx0XHRpZiAoJHN1Yikge1xuXHRcdFx0XHRcdHZhciBzdWJBcnJvd0NsaWNrZWQgPSAkKGUudGFyZ2V0KS5pcygnLnN1Yi1hcnJvdycpLFxuXHRcdFx0XHRcdFx0Y29sbGFwc2libGUgPSB0aGlzLmlzQ29sbGFwc2libGUoKSxcblx0XHRcdFx0XHRcdGJlaGF2aW9yVG9nZ2xlID0gL3RvZ2dsZSQvLnRlc3QodGhpcy5vcHRzLmNvbGxhcHNpYmxlQmVoYXZpb3IpLFxuXHRcdFx0XHRcdFx0YmVoYXZpb3JMaW5rID0gL2xpbmskLy50ZXN0KHRoaXMub3B0cy5jb2xsYXBzaWJsZUJlaGF2aW9yKSxcblx0XHRcdFx0XHRcdGJlaGF2aW9yQWNjb3JkaW9uID0gL15hY2NvcmRpb24vLnRlc3QodGhpcy5vcHRzLmNvbGxhcHNpYmxlQmVoYXZpb3IpO1xuXHRcdFx0XHRcdC8vIGlmIHRoZSBzdWIgaXMgaGlkZGVuLCB0cnkgdG8gc2hvdyBpdFxuXHRcdFx0XHRcdGlmICghJHN1Yi5pcygnOnZpc2libGUnKSkge1xuXHRcdFx0XHRcdFx0aWYgKCFiZWhhdmlvckxpbmsgfHwgIWNvbGxhcHNpYmxlIHx8IHN1YkFycm93Q2xpY2tlZCkge1xuXHRcdFx0XHRcdFx0XHRpZiAoIWNvbGxhcHNpYmxlICYmIHRoaXMub3B0cy5zaG93T25DbGljayAmJiBmaXJzdExldmVsU3ViKSB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5jbGlja0FjdGl2YXRlZCA9IHRydWU7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0Ly8gdHJ5IHRvIGFjdGl2YXRlIHRoZSBpdGVtIGFuZCBzaG93IHRoZSBzdWJcblx0XHRcdFx0XHRcdFx0dGhpcy5pdGVtQWN0aXZhdGUoJGEsIGJlaGF2aW9yQWNjb3JkaW9uKTtcblx0XHRcdFx0XHRcdFx0Ly8gaWYgXCJpdGVtQWN0aXZhdGVcIiBzaG93ZWQgdGhlIHN1YiwgcHJldmVudCB0aGUgY2xpY2sgc28gdGhhdCB0aGUgbGluayBpcyBub3QgbG9hZGVkXG5cdFx0XHRcdFx0XHRcdC8vIGlmIGl0IGNvdWxkbid0IHNob3cgaXQsIHRoZW4gdGhlIHN1YiBtZW51cyBhcmUgZGlzYWJsZWQgd2l0aCBhbiAhaW1wb3J0YW50IGRlY2xhcmF0aW9uIChlLmcuIHZpYSBtb2JpbGUgc3R5bGVzKSBzbyBsZXQgdGhlIGxpbmsgZ2V0IGxvYWRlZFxuXHRcdFx0XHRcdFx0XHRpZiAoJHN1Yi5pcygnOnZpc2libGUnKSkge1xuXHRcdFx0XHRcdFx0XHRcdHRoaXMuZm9jdXNBY3RpdmF0ZWQgPSB0cnVlO1xuXHRcdFx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdC8vIGlmIHRoZSBzdWIgaXMgdmlzaWJsZSBhbmQgc2hvd09uQ2xpY2s6IHRydWUsIGhpZGUgdGhlIHN1YlxuXHRcdFx0XHRcdH0gZWxzZSBpZiAoIWNvbGxhcHNpYmxlICYmIHRoaXMub3B0cy5zaG93T25DbGljayAmJiBmaXJzdExldmVsU3ViKSB7XG5cdFx0XHRcdFx0XHR0aGlzLm1lbnVIaWRlKCRzdWIpO1xuXHRcdFx0XHRcdFx0dGhpcy5jbGlja0FjdGl2YXRlZCA9IGZhbHNlO1xuXHRcdFx0XHRcdFx0dGhpcy5mb2N1c0FjdGl2YXRlZCA9IGZhbHNlO1xuXHRcdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHRcdC8vIGlmIHRoZSBzdWIgaXMgdmlzaWJsZSBhbmQgd2UgYXJlIGluIGNvbGxhcHNpYmxlIG1vZGVcblx0XHRcdFx0XHR9IGVsc2UgaWYgKGNvbGxhcHNpYmxlICYmIChiZWhhdmlvclRvZ2dsZSB8fCBzdWJBcnJvd0NsaWNrZWQpKSB7XG5cdFx0XHRcdFx0XHR0aGlzLml0ZW1BY3RpdmF0ZSgkYSwgYmVoYXZpb3JBY2NvcmRpb24pO1xuXHRcdFx0XHRcdFx0dGhpcy5tZW51SGlkZSgkc3ViKTtcblx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCFjb2xsYXBzaWJsZSAmJiB0aGlzLm9wdHMuc2hvd09uQ2xpY2sgJiYgZmlyc3RMZXZlbFN1YiB8fCAkYS5oYXNDbGFzcygnZGlzYWJsZWQnKSB8fCB0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdzZWxlY3Quc21hcGknLCAkYVswXSkgPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0aXRlbURvd246IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0dmFyICRhID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuXHRcdFx0XHRpZiAoIXRoaXMuaGFuZGxlSXRlbUV2ZW50cygkYSkpIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0JGEuZGF0YVNNKCdtb3VzZWRvd24nLCB0cnVlKTtcblx0XHRcdH0sXG5cdFx0XHRpdGVtRW50ZXI6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0dmFyICRhID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuXHRcdFx0XHRpZiAoIXRoaXMuaGFuZGxlSXRlbUV2ZW50cygkYSkpIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCF0aGlzLmlzVG91Y2hNb2RlKCkpIHtcblx0XHRcdFx0XHRpZiAodGhpcy5zaG93VGltZW91dCkge1xuXHRcdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHRoaXMuc2hvd1RpbWVvdXQpO1xuXHRcdFx0XHRcdFx0dGhpcy5zaG93VGltZW91dCA9IDA7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdFx0XHR0aGlzLnNob3dUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHsgc2VsZi5pdGVtQWN0aXZhdGUoJGEpOyB9LCB0aGlzLm9wdHMuc2hvd09uQ2xpY2sgJiYgJGEuY2xvc2VzdCgndWwnKS5kYXRhU00oJ2xldmVsJykgPT0gMSA/IDEgOiB0aGlzLm9wdHMuc2hvd1RpbWVvdXQpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ21vdXNlZW50ZXIuc21hcGknLCAkYVswXSk7XG5cdFx0XHR9LFxuXHRcdFx0aXRlbUZvY3VzOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdHZhciAkYSA9ICQoZS5jdXJyZW50VGFyZ2V0KTtcblx0XHRcdFx0aWYgKCF0aGlzLmhhbmRsZUl0ZW1FdmVudHMoJGEpKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdC8vIGZpeCAodGhlIG1vdXNlZG93biBjaGVjayk6IGluIHNvbWUgYnJvd3NlcnMgYSB0YXAvY2xpY2sgcHJvZHVjZXMgY29uc2VjdXRpdmUgZm9jdXMgKyBjbGljayBldmVudHMgc28gd2UgZG9uJ3QgbmVlZCB0byBhY3RpdmF0ZSB0aGUgaXRlbSBvbiBmb2N1c1xuXHRcdFx0XHRpZiAodGhpcy5mb2N1c0FjdGl2YXRlZCAmJiAoIXRoaXMuaXNUb3VjaE1vZGUoKSB8fCAhJGEuZGF0YVNNKCdtb3VzZWRvd24nKSkgJiYgKCF0aGlzLmFjdGl2YXRlZEl0ZW1zLmxlbmd0aCB8fCB0aGlzLmFjdGl2YXRlZEl0ZW1zW3RoaXMuYWN0aXZhdGVkSXRlbXMubGVuZ3RoIC0gMV1bMF0gIT0gJGFbMF0pKSB7XG5cdFx0XHRcdFx0dGhpcy5pdGVtQWN0aXZhdGUoJGEsIHRydWUpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ2ZvY3VzLnNtYXBpJywgJGFbMF0pO1xuXHRcdFx0fSxcblx0XHRcdGl0ZW1MZWF2ZTogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHR2YXIgJGEgPSAkKGUuY3VycmVudFRhcmdldCk7XG5cdFx0XHRcdGlmICghdGhpcy5oYW5kbGVJdGVtRXZlbnRzKCRhKSkge1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoIXRoaXMuaXNUb3VjaE1vZGUoKSkge1xuXHRcdFx0XHRcdCRhWzBdLmJsdXIoKTtcblx0XHRcdFx0XHRpZiAodGhpcy5zaG93VGltZW91dCkge1xuXHRcdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHRoaXMuc2hvd1RpbWVvdXQpO1xuXHRcdFx0XHRcdFx0dGhpcy5zaG93VGltZW91dCA9IDA7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRcdCRhLnJlbW92ZURhdGFTTSgnbW91c2Vkb3duJyk7XG5cdFx0XHRcdHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ21vdXNlbGVhdmUuc21hcGknLCAkYVswXSk7XG5cdFx0XHR9LFxuXHRcdFx0bWVudUhpZGU6IGZ1bmN0aW9uKCRzdWIpIHtcblx0XHRcdFx0aWYgKHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ2JlZm9yZWhpZGUuc21hcGknLCAkc3ViWzBdKSA9PT0gZmFsc2UpIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKGNhbkFuaW1hdGUpIHtcblx0XHRcdFx0XHQkc3ViLnN0b3AodHJ1ZSwgdHJ1ZSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCRzdWIuY3NzKCdkaXNwbGF5JykgIT0gJ25vbmUnKSB7XG5cdFx0XHRcdFx0dmFyIGNvbXBsZXRlID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHQvLyB1bnNldCB6LWluZGV4XG5cdFx0XHRcdFx0XHQkc3ViLmNzcygnei1pbmRleCcsICcnKTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdC8vIGlmIHN1YiBpcyBjb2xsYXBzaWJsZSAobW9iaWxlIHZpZXcpXG5cdFx0XHRcdFx0aWYgKHRoaXMuaXNDb2xsYXBzaWJsZSgpKSB7XG5cdFx0XHRcdFx0XHRpZiAoY2FuQW5pbWF0ZSAmJiB0aGlzLm9wdHMuY29sbGFwc2libGVIaWRlRnVuY3Rpb24pIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5vcHRzLmNvbGxhcHNpYmxlSGlkZUZ1bmN0aW9uLmNhbGwodGhpcywgJHN1YiwgY29tcGxldGUpO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0JHN1Yi5oaWRlKHRoaXMub3B0cy5jb2xsYXBzaWJsZUhpZGVEdXJhdGlvbiwgY29tcGxldGUpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRpZiAoY2FuQW5pbWF0ZSAmJiB0aGlzLm9wdHMuaGlkZUZ1bmN0aW9uKSB7XG5cdFx0XHRcdFx0XHRcdHRoaXMub3B0cy5oaWRlRnVuY3Rpb24uY2FsbCh0aGlzLCAkc3ViLCBjb21wbGV0ZSk7XG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHQkc3ViLmhpZGUodGhpcy5vcHRzLmhpZGVEdXJhdGlvbiwgY29tcGxldGUpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQvLyBkZWFjdGl2YXRlIHNjcm9sbGluZyBpZiBpdCBpcyBhY3RpdmF0ZWQgZm9yIHRoaXMgc3ViXG5cdFx0XHRcdFx0aWYgKCRzdWIuZGF0YVNNKCdzY3JvbGwnKSkge1xuXHRcdFx0XHRcdFx0dGhpcy5tZW51U2Nyb2xsU3RvcCgkc3ViKTtcblx0XHRcdFx0XHRcdCRzdWIuY3NzKHsgJ3RvdWNoLWFjdGlvbic6ICcnLCAnLW1zLXRvdWNoLWFjdGlvbic6ICcnLCAnLXdlYmtpdC10cmFuc2Zvcm0nOiAnJywgdHJhbnNmb3JtOiAnJyB9KVxuXHRcdFx0XHRcdFx0XHQub2ZmKCcuc21hcnRtZW51c19zY3JvbGwnKS5yZW1vdmVEYXRhU00oJ3Njcm9sbCcpLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLmhpZGUoKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0Ly8gdW5oaWdobGlnaHQgcGFyZW50IGl0ZW0gKyBhY2Nlc3NpYmlsaXR5XG5cdFx0XHRcdFx0JHN1Yi5kYXRhU00oJ3BhcmVudC1hJykucmVtb3ZlQ2xhc3MoJ2hpZ2hsaWdodGVkJykuYXR0cignYXJpYS1leHBhbmRlZCcsICdmYWxzZScpO1xuXHRcdFx0XHRcdCRzdWIuYXR0cih7XG5cdFx0XHRcdFx0XHQnYXJpYS1leHBhbmRlZCc6ICdmYWxzZScsXG5cdFx0XHRcdFx0XHQnYXJpYS1oaWRkZW4nOiAndHJ1ZSdcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR2YXIgbGV2ZWwgPSAkc3ViLmRhdGFTTSgnbGV2ZWwnKTtcblx0XHRcdFx0XHR0aGlzLmFjdGl2YXRlZEl0ZW1zLnNwbGljZShsZXZlbCAtIDEsIDEpO1xuXHRcdFx0XHRcdHRoaXMudmlzaWJsZVN1Yk1lbnVzLnNwbGljZSgkLmluQXJyYXkoJHN1YiwgdGhpcy52aXNpYmxlU3ViTWVudXMpLCAxKTtcblx0XHRcdFx0XHR0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdoaWRlLnNtYXBpJywgJHN1YlswXSk7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRtZW51SGlkZUFsbDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGlmICh0aGlzLnNob3dUaW1lb3V0KSB7XG5cdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHRoaXMuc2hvd1RpbWVvdXQpO1xuXHRcdFx0XHRcdHRoaXMuc2hvd1RpbWVvdXQgPSAwO1xuXHRcdFx0XHR9XG5cdFx0XHRcdC8vIGhpZGUgYWxsIHN1YnNcblx0XHRcdFx0Ly8gaWYgaXQncyBhIHBvcHVwLCB0aGlzLnZpc2libGVTdWJNZW51c1swXSBpcyB0aGUgcm9vdCBVTFxuXHRcdFx0XHR2YXIgbGV2ZWwgPSB0aGlzLm9wdHMuaXNQb3B1cCA/IDEgOiAwO1xuXHRcdFx0XHRmb3IgKHZhciBpID0gdGhpcy52aXNpYmxlU3ViTWVudXMubGVuZ3RoIC0gMTsgaSA+PSBsZXZlbDsgaS0tKSB7XG5cdFx0XHRcdFx0dGhpcy5tZW51SGlkZSh0aGlzLnZpc2libGVTdWJNZW51c1tpXSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8gaGlkZSByb290IGlmIGl0J3MgcG9wdXBcblx0XHRcdFx0aWYgKHRoaXMub3B0cy5pc1BvcHVwKSB7XG5cdFx0XHRcdFx0aWYgKGNhbkFuaW1hdGUpIHtcblx0XHRcdFx0XHRcdHRoaXMuJHJvb3Quc3RvcCh0cnVlLCB0cnVlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aWYgKHRoaXMuJHJvb3QuaXMoJzp2aXNpYmxlJykpIHtcblx0XHRcdFx0XHRcdGlmIChjYW5BbmltYXRlICYmIHRoaXMub3B0cy5oaWRlRnVuY3Rpb24pIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5vcHRzLmhpZGVGdW5jdGlvbi5jYWxsKHRoaXMsIHRoaXMuJHJvb3QpO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0dGhpcy4kcm9vdC5oaWRlKHRoaXMub3B0cy5oaWRlRHVyYXRpb24pO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XHR0aGlzLmFjdGl2YXRlZEl0ZW1zID0gW107XG5cdFx0XHRcdHRoaXMudmlzaWJsZVN1Yk1lbnVzID0gW107XG5cdFx0XHRcdHRoaXMuY2xpY2tBY3RpdmF0ZWQgPSBmYWxzZTtcblx0XHRcdFx0dGhpcy5mb2N1c0FjdGl2YXRlZCA9IGZhbHNlO1xuXHRcdFx0XHQvLyByZXNldCB6LWluZGV4IGluY3JlbWVudFxuXHRcdFx0XHR0aGlzLnpJbmRleEluYyA9IDA7XG5cdFx0XHRcdHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ2hpZGVBbGwuc21hcGknKTtcblx0XHRcdH0sXG5cdFx0XHRtZW51SGlkZVN1Yk1lbnVzOiBmdW5jdGlvbihsZXZlbCkge1xuXHRcdFx0XHRmb3IgKHZhciBpID0gdGhpcy5hY3RpdmF0ZWRJdGVtcy5sZW5ndGggLSAxOyBpID49IGxldmVsOyBpLS0pIHtcblx0XHRcdFx0XHR2YXIgJHN1YiA9IHRoaXMuYWN0aXZhdGVkSXRlbXNbaV0uZGF0YVNNKCdzdWInKTtcblx0XHRcdFx0XHRpZiAoJHN1Yikge1xuXHRcdFx0XHRcdFx0dGhpcy5tZW51SGlkZSgkc3ViKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRtZW51SW5pdDogZnVuY3Rpb24oJHVsKSB7XG5cdFx0XHRcdGlmICghJHVsLmRhdGFTTSgnaW4tbWVnYScpKSB7XG5cdFx0XHRcdFx0Ly8gbWFyayBVTCdzIGluIG1lZ2EgZHJvcCBkb3ducyAoaWYgYW55KSBzbyB3ZSBjYW4gbmVnbGVjdCB0aGVtXG5cdFx0XHRcdFx0aWYgKCR1bC5oYXNDbGFzcygnbWVnYS1tZW51JykpIHtcblx0XHRcdFx0XHRcdCR1bC5maW5kKCd1bCcpLmRhdGFTTSgnaW4tbWVnYScsIHRydWUpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQvLyBnZXQgbGV2ZWwgKG11Y2ggZmFzdGVyIHRoYW4sIGZvciBleGFtcGxlLCB1c2luZyBwYXJlbnRzVW50aWwpXG5cdFx0XHRcdFx0dmFyIGxldmVsID0gMixcblx0XHRcdFx0XHRcdHBhciA9ICR1bFswXTtcblx0XHRcdFx0XHR3aGlsZSAoKHBhciA9IHBhci5wYXJlbnROb2RlLnBhcmVudE5vZGUpICE9IHRoaXMuJHJvb3RbMF0pIHtcblx0XHRcdFx0XHRcdGxldmVsKys7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdC8vIGNhY2hlIHN0dWZmIGZvciBxdWljayBhY2Nlc3Ncblx0XHRcdFx0XHR2YXIgJGEgPSAkdWwucHJldkFsbCgnYScpLmVxKC0xKTtcblx0XHRcdFx0XHQvLyBpZiB0aGUgbGluayBpcyBuZXN0ZWQgKGUuZy4gaW4gYSBoZWFkaW5nKVxuXHRcdFx0XHRcdGlmICghJGEubGVuZ3RoKSB7XG5cdFx0XHRcdFx0XHQkYSA9ICR1bC5wcmV2QWxsKCkuZmluZCgnYScpLmVxKC0xKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0JGEuYWRkQ2xhc3MoJ2hhcy1zdWJtZW51JykuZGF0YVNNKCdzdWInLCAkdWwpO1xuXHRcdFx0XHRcdCR1bC5kYXRhU00oJ3BhcmVudC1hJywgJGEpXG5cdFx0XHRcdFx0XHQuZGF0YVNNKCdsZXZlbCcsIGxldmVsKVxuXHRcdFx0XHRcdFx0LnBhcmVudCgpLmRhdGFTTSgnc3ViJywgJHVsKTtcblx0XHRcdFx0XHQvLyBhY2Nlc3NpYmlsaXR5XG5cdFx0XHRcdFx0dmFyIGFJZCA9ICRhLmF0dHIoJ2lkJykgfHwgdGhpcy5hY2Nlc3NJZFByZWZpeCArICgrK3RoaXMuaWRJbmMpLFxuXHRcdFx0XHRcdFx0dWxJZCA9ICR1bC5hdHRyKCdpZCcpIHx8IHRoaXMuYWNjZXNzSWRQcmVmaXggKyAoKyt0aGlzLmlkSW5jKTtcblx0XHRcdFx0XHQkYS5hdHRyKHtcblx0XHRcdFx0XHRcdGlkOiBhSWQsXG5cdFx0XHRcdFx0XHQnYXJpYS1oYXNwb3B1cCc6ICd0cnVlJyxcblx0XHRcdFx0XHRcdCdhcmlhLWNvbnRyb2xzJzogdWxJZCxcblx0XHRcdFx0XHRcdCdhcmlhLWV4cGFuZGVkJzogJ2ZhbHNlJ1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdCR1bC5hdHRyKHtcblx0XHRcdFx0XHRcdGlkOiB1bElkLFxuXHRcdFx0XHRcdFx0J3JvbGUnOiAnZ3JvdXAnLFxuXHRcdFx0XHRcdFx0J2FyaWEtaGlkZGVuJzogJ3RydWUnLFxuXHRcdFx0XHRcdFx0J2FyaWEtbGFiZWxsZWRieSc6IGFJZCxcblx0XHRcdFx0XHRcdCdhcmlhLWV4cGFuZGVkJzogJ2ZhbHNlJ1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdC8vIGFkZCBzdWIgaW5kaWNhdG9yIHRvIHBhcmVudCBpdGVtXG5cdFx0XHRcdFx0aWYgKHRoaXMub3B0cy5zdWJJbmRpY2F0b3JzKSB7XG5cdFx0XHRcdFx0XHQkYVt0aGlzLm9wdHMuc3ViSW5kaWNhdG9yc1Bvc10odGhpcy4kc3ViQXJyb3cuY2xvbmUoKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0bWVudVBvc2l0aW9uOiBmdW5jdGlvbigkc3ViKSB7XG5cdFx0XHRcdHZhciAkYSA9ICRzdWIuZGF0YVNNKCdwYXJlbnQtYScpLFxuXHRcdFx0XHRcdCRsaSA9ICRhLmNsb3Nlc3QoJ2xpJyksXG5cdFx0XHRcdFx0JHVsID0gJGxpLnBhcmVudCgpLFxuXHRcdFx0XHRcdGxldmVsID0gJHN1Yi5kYXRhU00oJ2xldmVsJyksXG5cdFx0XHRcdFx0c3ViVyA9IHRoaXMuZ2V0V2lkdGgoJHN1YiksXG5cdFx0XHRcdFx0c3ViSCA9IHRoaXMuZ2V0SGVpZ2h0KCRzdWIpLFxuXHRcdFx0XHRcdGl0ZW1PZmZzZXQgPSAkYS5vZmZzZXQoKSxcblx0XHRcdFx0XHRpdGVtWCA9IGl0ZW1PZmZzZXQubGVmdCxcblx0XHRcdFx0XHRpdGVtWSA9IGl0ZW1PZmZzZXQudG9wLFxuXHRcdFx0XHRcdGl0ZW1XID0gdGhpcy5nZXRXaWR0aCgkYSksXG5cdFx0XHRcdFx0aXRlbUggPSB0aGlzLmdldEhlaWdodCgkYSksXG5cdFx0XHRcdFx0JHdpbiA9ICQod2luZG93KSxcblx0XHRcdFx0XHR3aW5YID0gJHdpbi5zY3JvbGxMZWZ0KCksXG5cdFx0XHRcdFx0d2luWSA9ICR3aW4uc2Nyb2xsVG9wKCksXG5cdFx0XHRcdFx0d2luVyA9IHRoaXMuZ2V0Vmlld3BvcnRXaWR0aCgpLFxuXHRcdFx0XHRcdHdpbkggPSB0aGlzLmdldFZpZXdwb3J0SGVpZ2h0KCksXG5cdFx0XHRcdFx0aG9yaXpvbnRhbFBhcmVudCA9ICR1bC5wYXJlbnQoKS5pcygnW2RhdGEtc20taG9yaXpvbnRhbC1zdWJdJykgfHwgbGV2ZWwgPT0gMiAmJiAhJHVsLmhhc0NsYXNzKCdzbS12ZXJ0aWNhbCcpLFxuXHRcdFx0XHRcdHJpZ2h0VG9MZWZ0ID0gdGhpcy5vcHRzLnJpZ2h0VG9MZWZ0U3ViTWVudXMgJiYgISRsaS5pcygnW2RhdGEtc20tcmV2ZXJzZV0nKSB8fCAhdGhpcy5vcHRzLnJpZ2h0VG9MZWZ0U3ViTWVudXMgJiYgJGxpLmlzKCdbZGF0YS1zbS1yZXZlcnNlXScpLFxuXHRcdFx0XHRcdHN1Yk9mZnNldFggPSBsZXZlbCA9PSAyID8gdGhpcy5vcHRzLm1haW5NZW51U3ViT2Zmc2V0WCA6IHRoaXMub3B0cy5zdWJNZW51c1N1Yk9mZnNldFgsXG5cdFx0XHRcdFx0c3ViT2Zmc2V0WSA9IGxldmVsID09IDIgPyB0aGlzLm9wdHMubWFpbk1lbnVTdWJPZmZzZXRZIDogdGhpcy5vcHRzLnN1Yk1lbnVzU3ViT2Zmc2V0WSxcblx0XHRcdFx0XHR4LCB5O1xuXHRcdFx0XHRpZiAoaG9yaXpvbnRhbFBhcmVudCkge1xuXHRcdFx0XHRcdHggPSByaWdodFRvTGVmdCA/IGl0ZW1XIC0gc3ViVyAtIHN1Yk9mZnNldFggOiBzdWJPZmZzZXRYO1xuXHRcdFx0XHRcdHkgPSB0aGlzLm9wdHMuYm90dG9tVG9Ub3BTdWJNZW51cyA/IC1zdWJIIC0gc3ViT2Zmc2V0WSA6IGl0ZW1IICsgc3ViT2Zmc2V0WTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR4ID0gcmlnaHRUb0xlZnQgPyBzdWJPZmZzZXRYIC0gc3ViVyA6IGl0ZW1XIC0gc3ViT2Zmc2V0WDtcblx0XHRcdFx0XHR5ID0gdGhpcy5vcHRzLmJvdHRvbVRvVG9wU3ViTWVudXMgPyBpdGVtSCAtIHN1Yk9mZnNldFkgLSBzdWJIIDogc3ViT2Zmc2V0WTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAodGhpcy5vcHRzLmtlZXBJblZpZXdwb3J0KSB7XG5cdFx0XHRcdFx0dmFyIGFic1ggPSBpdGVtWCArIHgsXG5cdFx0XHRcdFx0XHRhYnNZID0gaXRlbVkgKyB5O1xuXHRcdFx0XHRcdGlmIChyaWdodFRvTGVmdCAmJiBhYnNYIDwgd2luWCkge1xuXHRcdFx0XHRcdFx0eCA9IGhvcml6b250YWxQYXJlbnQgPyB3aW5YIC0gYWJzWCArIHggOiBpdGVtVyAtIHN1Yk9mZnNldFg7XG5cdFx0XHRcdFx0fSBlbHNlIGlmICghcmlnaHRUb0xlZnQgJiYgYWJzWCArIHN1YlcgPiB3aW5YICsgd2luVykge1xuXHRcdFx0XHRcdFx0eCA9IGhvcml6b250YWxQYXJlbnQgPyB3aW5YICsgd2luVyAtIHN1YlcgLSBhYnNYICsgeCA6IHN1Yk9mZnNldFggLSBzdWJXO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRpZiAoIWhvcml6b250YWxQYXJlbnQpIHtcblx0XHRcdFx0XHRcdGlmIChzdWJIIDwgd2luSCAmJiBhYnNZICsgc3ViSCA+IHdpblkgKyB3aW5IKSB7XG5cdFx0XHRcdFx0XHRcdHkgKz0gd2luWSArIHdpbkggLSBzdWJIIC0gYWJzWTtcblx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoc3ViSCA+PSB3aW5IIHx8IGFic1kgPCB3aW5ZKSB7XG5cdFx0XHRcdFx0XHRcdHkgKz0gd2luWSAtIGFic1k7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdC8vIGRvIHdlIG5lZWQgc2Nyb2xsaW5nP1xuXHRcdFx0XHRcdC8vIDAuNDkgdXNlZCBmb3IgYmV0dGVyIHByZWNpc2lvbiB3aGVuIGRlYWxpbmcgd2l0aCBmbG9hdCB2YWx1ZXNcblx0XHRcdFx0XHRpZiAoaG9yaXpvbnRhbFBhcmVudCAmJiAoYWJzWSArIHN1YkggPiB3aW5ZICsgd2luSCArIDAuNDkgfHwgYWJzWSA8IHdpblkpIHx8ICFob3Jpem9udGFsUGFyZW50ICYmIHN1YkggPiB3aW5IICsgMC40OSkge1xuXHRcdFx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XHRcdFx0aWYgKCEkc3ViLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpKSB7XG5cdFx0XHRcdFx0XHRcdCRzdWIuZGF0YVNNKCdzY3JvbGwtYXJyb3dzJywgJChbJCgnPHNwYW4gY2xhc3M9XCJzY3JvbGwtdXBcIj48c3BhbiBjbGFzcz1cInNjcm9sbC11cC1hcnJvd1wiPjwvc3Bhbj48L3NwYW4+JylbMF0sICQoJzxzcGFuIGNsYXNzPVwic2Nyb2xsLWRvd25cIj48c3BhbiBjbGFzcz1cInNjcm9sbC1kb3duLWFycm93XCI+PC9zcGFuPjwvc3Bhbj4nKVswXV0pXG5cdFx0XHRcdFx0XHRcdFx0Lm9uKHtcblx0XHRcdFx0XHRcdFx0XHRcdG1vdXNlZW50ZXI6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHQkc3ViLmRhdGFTTSgnc2Nyb2xsJykudXAgPSAkKHRoaXMpLmhhc0NsYXNzKCdzY3JvbGwtdXAnKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0c2VsZi5tZW51U2Nyb2xsKCRzdWIpO1xuXHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdG1vdXNlbGVhdmU6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0c2VsZi5tZW51U2Nyb2xsU3RvcCgkc3ViKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0c2VsZi5tZW51U2Nyb2xsT3V0KCRzdWIsIGUpO1xuXHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdCdtb3VzZXdoZWVsIERPTU1vdXNlU2Nyb2xsJzogZnVuY3Rpb24oZSkgeyBlLnByZXZlbnREZWZhdWx0KCk7IH1cblx0XHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHRcdC5pbnNlcnRBZnRlcigkc3ViKVxuXHRcdFx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0Ly8gYmluZCBzY3JvbGwgZXZlbnRzIGFuZCBzYXZlIHNjcm9sbCBkYXRhIGZvciB0aGlzIHN1YlxuXHRcdFx0XHRcdFx0dmFyIGVOUyA9ICcuc21hcnRtZW51c19zY3JvbGwnO1xuXHRcdFx0XHRcdFx0JHN1Yi5kYXRhU00oJ3Njcm9sbCcsIHtcblx0XHRcdFx0XHRcdFx0XHR5OiB0aGlzLmNzc1RyYW5zZm9ybXMzZCA/IDAgOiB5IC0gaXRlbUgsXG5cdFx0XHRcdFx0XHRcdFx0c3RlcDogMSxcblx0XHRcdFx0XHRcdFx0XHQvLyBjYWNoZSBzdHVmZiBmb3IgZmFzdGVyIHJlY2FsY3MgbGF0ZXJcblx0XHRcdFx0XHRcdFx0XHRpdGVtSDogaXRlbUgsXG5cdFx0XHRcdFx0XHRcdFx0c3ViSDogc3ViSCxcblx0XHRcdFx0XHRcdFx0XHRhcnJvd0Rvd25IOiB0aGlzLmdldEhlaWdodCgkc3ViLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLmVxKDEpKVxuXHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHQub24oZ2V0RXZlbnRzTlMoe1xuXHRcdFx0XHRcdFx0XHRcdCdtb3VzZW92ZXInOiBmdW5jdGlvbihlKSB7IHNlbGYubWVudVNjcm9sbE92ZXIoJHN1YiwgZSk7IH0sXG5cdFx0XHRcdFx0XHRcdFx0J21vdXNlb3V0JzogZnVuY3Rpb24oZSkgeyBzZWxmLm1lbnVTY3JvbGxPdXQoJHN1YiwgZSk7IH0sXG5cdFx0XHRcdFx0XHRcdFx0J21vdXNld2hlZWwgRE9NTW91c2VTY3JvbGwnOiBmdW5jdGlvbihlKSB7IHNlbGYubWVudVNjcm9sbE1vdXNld2hlZWwoJHN1YiwgZSk7IH1cblx0XHRcdFx0XHRcdFx0fSwgZU5TKSlcblx0XHRcdFx0XHRcdFx0LmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLmNzcyh7IHRvcDogJ2F1dG8nLCBsZWZ0OiAnMCcsIG1hcmdpbkxlZnQ6IHggKyAocGFyc2VJbnQoJHN1Yi5jc3MoJ2JvcmRlci1sZWZ0LXdpZHRoJykpIHx8IDApLCB3aWR0aDogc3ViVyAtIChwYXJzZUludCgkc3ViLmNzcygnYm9yZGVyLWxlZnQtd2lkdGgnKSkgfHwgMCkgLSAocGFyc2VJbnQoJHN1Yi5jc3MoJ2JvcmRlci1yaWdodC13aWR0aCcpKSB8fCAwKSwgekluZGV4OiAkc3ViLmNzcygnei1pbmRleCcpIH0pXG5cdFx0XHRcdFx0XHRcdFx0LmVxKGhvcml6b250YWxQYXJlbnQgJiYgdGhpcy5vcHRzLmJvdHRvbVRvVG9wU3ViTWVudXMgPyAwIDogMSkuc2hvdygpO1xuXHRcdFx0XHRcdFx0Ly8gd2hlbiBhIG1lbnUgdHJlZSBpcyBmaXhlZCBwb3NpdGlvbmVkIHdlIGFsbG93IHNjcm9sbGluZyB2aWEgdG91Y2ggdG9vXG5cdFx0XHRcdFx0XHQvLyBzaW5jZSB0aGVyZSBpcyBubyBvdGhlciB3YXkgdG8gYWNjZXNzIHN1Y2ggbG9uZyBzdWIgbWVudXMgaWYgbm8gbW91c2UgaXMgcHJlc2VudFxuXHRcdFx0XHRcdFx0aWYgKHRoaXMuaXNGaXhlZCgpKSB7XG5cdFx0XHRcdFx0XHRcdHZhciBldmVudHMgPSB7fTtcblx0XHRcdFx0XHRcdFx0ZXZlbnRzW3RvdWNoRXZlbnRzID8gJ3RvdWNoc3RhcnQgdG91Y2htb3ZlIHRvdWNoZW5kJyA6ICdwb2ludGVyZG93biBwb2ludGVybW92ZSBwb2ludGVydXAgTVNQb2ludGVyRG93biBNU1BvaW50ZXJNb3ZlIE1TUG9pbnRlclVwJ10gPSBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdFx0XHRcdFx0c2VsZi5tZW51U2Nyb2xsVG91Y2goJHN1YiwgZSk7XG5cdFx0XHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0XHRcdCRzdWIuY3NzKHsgJ3RvdWNoLWFjdGlvbic6ICdub25lJywgJy1tcy10b3VjaC1hY3Rpb24nOiAnbm9uZScgfSkub24oZ2V0RXZlbnRzTlMoZXZlbnRzLCBlTlMpKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdFx0JHN1Yi5jc3MoeyB0b3A6ICdhdXRvJywgbGVmdDogJzAnLCBtYXJnaW5MZWZ0OiB4LCBtYXJnaW5Ub3A6IHkgLSBpdGVtSCB9KTtcblx0XHRcdH0sXG5cdFx0XHRtZW51U2Nyb2xsOiBmdW5jdGlvbigkc3ViLCBvbmNlLCBzdGVwKSB7XG5cdFx0XHRcdHZhciBkYXRhID0gJHN1Yi5kYXRhU00oJ3Njcm9sbCcpLFxuXHRcdFx0XHRcdCRhcnJvd3MgPSAkc3ViLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLFxuXHRcdFx0XHRcdGVuZCA9IGRhdGEudXAgPyBkYXRhLnVwRW5kIDogZGF0YS5kb3duRW5kLFxuXHRcdFx0XHRcdGRpZmY7XG5cdFx0XHRcdGlmICghb25jZSAmJiBkYXRhLm1vbWVudHVtKSB7XG5cdFx0XHRcdFx0ZGF0YS5tb21lbnR1bSAqPSAwLjkyO1xuXHRcdFx0XHRcdGRpZmYgPSBkYXRhLm1vbWVudHVtO1xuXHRcdFx0XHRcdGlmIChkaWZmIDwgMC41KSB7XG5cdFx0XHRcdFx0XHR0aGlzLm1lbnVTY3JvbGxTdG9wKCRzdWIpO1xuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRkaWZmID0gc3RlcCB8fCAob25jZSB8fCAhdGhpcy5vcHRzLnNjcm9sbEFjY2VsZXJhdGUgPyB0aGlzLm9wdHMuc2Nyb2xsU3RlcCA6IE1hdGguZmxvb3IoZGF0YS5zdGVwKSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8gaGlkZSBhbnkgdmlzaWJsZSBkZWVwZXIgbGV2ZWwgc3ViIG1lbnVzXG5cdFx0XHRcdHZhciBsZXZlbCA9ICRzdWIuZGF0YVNNKCdsZXZlbCcpO1xuXHRcdFx0XHRpZiAodGhpcy5hY3RpdmF0ZWRJdGVtc1tsZXZlbCAtIDFdICYmIHRoaXMuYWN0aXZhdGVkSXRlbXNbbGV2ZWwgLSAxXS5kYXRhU00oJ3N1YicpICYmIHRoaXMuYWN0aXZhdGVkSXRlbXNbbGV2ZWwgLSAxXS5kYXRhU00oJ3N1YicpLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0dGhpcy5tZW51SGlkZVN1Yk1lbnVzKGxldmVsIC0gMSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZGF0YS55ID0gZGF0YS51cCAmJiBlbmQgPD0gZGF0YS55IHx8ICFkYXRhLnVwICYmIGVuZCA+PSBkYXRhLnkgPyBkYXRhLnkgOiAoTWF0aC5hYnMoZW5kIC0gZGF0YS55KSA+IGRpZmYgPyBkYXRhLnkgKyAoZGF0YS51cCA/IGRpZmYgOiAtZGlmZikgOiBlbmQpO1xuXHRcdFx0XHQkc3ViLmNzcyh0aGlzLmNzc1RyYW5zZm9ybXMzZCA/IHsgJy13ZWJraXQtdHJhbnNmb3JtJzogJ3RyYW5zbGF0ZTNkKDAsICcgKyBkYXRhLnkgKyAncHgsIDApJywgdHJhbnNmb3JtOiAndHJhbnNsYXRlM2QoMCwgJyArIGRhdGEueSArICdweCwgMCknIH0gOiB7IG1hcmdpblRvcDogZGF0YS55IH0pO1xuXHRcdFx0XHQvLyBzaG93IG9wcG9zaXRlIGFycm93IGlmIGFwcHJvcHJpYXRlXG5cdFx0XHRcdGlmIChtb3VzZSAmJiAoZGF0YS51cCAmJiBkYXRhLnkgPiBkYXRhLmRvd25FbmQgfHwgIWRhdGEudXAgJiYgZGF0YS55IDwgZGF0YS51cEVuZCkpIHtcblx0XHRcdFx0XHQkYXJyb3dzLmVxKGRhdGEudXAgPyAxIDogMCkuc2hvdygpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdC8vIGlmIHdlJ3ZlIHJlYWNoZWQgdGhlIGVuZFxuXHRcdFx0XHRpZiAoZGF0YS55ID09IGVuZCkge1xuXHRcdFx0XHRcdGlmIChtb3VzZSkge1xuXHRcdFx0XHRcdFx0JGFycm93cy5lcShkYXRhLnVwID8gMCA6IDEpLmhpZGUoKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dGhpcy5tZW51U2Nyb2xsU3RvcCgkc3ViKTtcblx0XHRcdFx0fSBlbHNlIGlmICghb25jZSkge1xuXHRcdFx0XHRcdGlmICh0aGlzLm9wdHMuc2Nyb2xsQWNjZWxlcmF0ZSAmJiBkYXRhLnN0ZXAgPCB0aGlzLm9wdHMuc2Nyb2xsU3RlcCkge1xuXHRcdFx0XHRcdFx0ZGF0YS5zdGVwICs9IDAuMjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XHRcdHRoaXMuc2Nyb2xsVGltZW91dCA9IHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbigpIHsgc2VsZi5tZW51U2Nyb2xsKCRzdWIpOyB9KTtcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdG1lbnVTY3JvbGxNb3VzZXdoZWVsOiBmdW5jdGlvbigkc3ViLCBlKSB7XG5cdFx0XHRcdGlmICh0aGlzLmdldENsb3Nlc3RNZW51KGUudGFyZ2V0KSA9PSAkc3ViWzBdKSB7XG5cdFx0XHRcdFx0ZSA9IGUub3JpZ2luYWxFdmVudDtcblx0XHRcdFx0XHR2YXIgdXAgPSAoZS53aGVlbERlbHRhIHx8IC1lLmRldGFpbCkgPiAwO1xuXHRcdFx0XHRcdGlmICgkc3ViLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLmVxKHVwID8gMCA6IDEpLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0XHQkc3ViLmRhdGFTTSgnc2Nyb2xsJykudXAgPSB1cDtcblx0XHRcdFx0XHRcdHRoaXMubWVudVNjcm9sbCgkc3ViLCB0cnVlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0fSxcblx0XHRcdG1lbnVTY3JvbGxPdXQ6IGZ1bmN0aW9uKCRzdWIsIGUpIHtcblx0XHRcdFx0aWYgKG1vdXNlKSB7XG5cdFx0XHRcdFx0aWYgKCEvXnNjcm9sbC0odXB8ZG93bikvLnRlc3QoKGUucmVsYXRlZFRhcmdldCB8fCAnJykuY2xhc3NOYW1lKSAmJiAoJHN1YlswXSAhPSBlLnJlbGF0ZWRUYXJnZXQgJiYgISQuY29udGFpbnMoJHN1YlswXSwgZS5yZWxhdGVkVGFyZ2V0KSB8fCB0aGlzLmdldENsb3Nlc3RNZW51KGUucmVsYXRlZFRhcmdldCkgIT0gJHN1YlswXSkpIHtcblx0XHRcdFx0XHRcdCRzdWIuZGF0YVNNKCdzY3JvbGwtYXJyb3dzJykuY3NzKCd2aXNpYmlsaXR5JywgJ2hpZGRlbicpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdG1lbnVTY3JvbGxPdmVyOiBmdW5jdGlvbigkc3ViLCBlKSB7XG5cdFx0XHRcdGlmIChtb3VzZSkge1xuXHRcdFx0XHRcdGlmICghL15zY3JvbGwtKHVwfGRvd24pLy50ZXN0KGUudGFyZ2V0LmNsYXNzTmFtZSkgJiYgdGhpcy5nZXRDbG9zZXN0TWVudShlLnRhcmdldCkgPT0gJHN1YlswXSkge1xuXHRcdFx0XHRcdFx0dGhpcy5tZW51U2Nyb2xsUmVmcmVzaERhdGEoJHN1Yik7XG5cdFx0XHRcdFx0XHR2YXIgZGF0YSA9ICRzdWIuZGF0YVNNKCdzY3JvbGwnKSxcblx0XHRcdFx0XHRcdFx0dXBFbmQgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgLSAkc3ViLmRhdGFTTSgncGFyZW50LWEnKS5vZmZzZXQoKS50b3AgLSBkYXRhLml0ZW1IO1xuXHRcdFx0XHRcdFx0JHN1Yi5kYXRhU00oJ3Njcm9sbC1hcnJvd3MnKS5lcSgwKS5jc3MoJ21hcmdpbi10b3AnLCB1cEVuZCkuZW5kKClcblx0XHRcdFx0XHRcdFx0LmVxKDEpLmNzcygnbWFyZ2luLXRvcCcsIHVwRW5kICsgdGhpcy5nZXRWaWV3cG9ydEhlaWdodCgpIC0gZGF0YS5hcnJvd0Rvd25IKS5lbmQoKVxuXHRcdFx0XHRcdFx0XHQuY3NzKCd2aXNpYmlsaXR5JywgJ3Zpc2libGUnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRtZW51U2Nyb2xsUmVmcmVzaERhdGE6IGZ1bmN0aW9uKCRzdWIpIHtcblx0XHRcdFx0dmFyIGRhdGEgPSAkc3ViLmRhdGFTTSgnc2Nyb2xsJyksXG5cdFx0XHRcdFx0dXBFbmQgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgLSAkc3ViLmRhdGFTTSgncGFyZW50LWEnKS5vZmZzZXQoKS50b3AgLSBkYXRhLml0ZW1IO1xuXHRcdFx0XHRpZiAodGhpcy5jc3NUcmFuc2Zvcm1zM2QpIHtcblx0XHRcdFx0XHR1cEVuZCA9IC0ocGFyc2VGbG9hdCgkc3ViLmNzcygnbWFyZ2luLXRvcCcpKSAtIHVwRW5kKTtcblx0XHRcdFx0fVxuXHRcdFx0XHQkLmV4dGVuZChkYXRhLCB7XG5cdFx0XHRcdFx0dXBFbmQ6IHVwRW5kLFxuXHRcdFx0XHRcdGRvd25FbmQ6IHVwRW5kICsgdGhpcy5nZXRWaWV3cG9ydEhlaWdodCgpIC0gZGF0YS5zdWJIXG5cdFx0XHRcdH0pO1xuXHRcdFx0fSxcblx0XHRcdG1lbnVTY3JvbGxTdG9wOiBmdW5jdGlvbigkc3ViKSB7XG5cdFx0XHRcdGlmICh0aGlzLnNjcm9sbFRpbWVvdXQpIHtcblx0XHRcdFx0XHRjYW5jZWxBbmltYXRpb25GcmFtZSh0aGlzLnNjcm9sbFRpbWVvdXQpO1xuXHRcdFx0XHRcdHRoaXMuc2Nyb2xsVGltZW91dCA9IDA7XG5cdFx0XHRcdFx0JHN1Yi5kYXRhU00oJ3Njcm9sbCcpLnN0ZXAgPSAxO1xuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0bWVudVNjcm9sbFRvdWNoOiBmdW5jdGlvbigkc3ViLCBlKSB7XG5cdFx0XHRcdGUgPSBlLm9yaWdpbmFsRXZlbnQ7XG5cdFx0XHRcdGlmIChpc1RvdWNoRXZlbnQoZSkpIHtcblx0XHRcdFx0XHR2YXIgdG91Y2hQb2ludCA9IHRoaXMuZ2V0VG91Y2hQb2ludChlKTtcblx0XHRcdFx0XHQvLyBuZWdsZWN0IGV2ZW50IGlmIHdlIHRvdWNoZWQgYSB2aXNpYmxlIGRlZXBlciBsZXZlbCBzdWIgbWVudVxuXHRcdFx0XHRcdGlmICh0aGlzLmdldENsb3Nlc3RNZW51KHRvdWNoUG9pbnQudGFyZ2V0KSA9PSAkc3ViWzBdKSB7XG5cdFx0XHRcdFx0XHR2YXIgZGF0YSA9ICRzdWIuZGF0YVNNKCdzY3JvbGwnKTtcblx0XHRcdFx0XHRcdGlmICgvKHN0YXJ0fGRvd24pJC9pLnRlc3QoZS50eXBlKSkge1xuXHRcdFx0XHRcdFx0XHRpZiAodGhpcy5tZW51U2Nyb2xsU3RvcCgkc3ViKSkge1xuXHRcdFx0XHRcdFx0XHRcdC8vIGlmIHdlIHdlcmUgc2Nyb2xsaW5nLCBqdXN0IHN0b3AgYW5kIGRvbid0IGFjdGl2YXRlIGFueSBsaW5rIG9uIHRoZSBmaXJzdCB0b3VjaFxuXHRcdFx0XHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHRcdFx0XHR0aGlzLiR0b3VjaFNjcm9sbGluZ1N1YiA9ICRzdWI7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy4kdG91Y2hTY3JvbGxpbmdTdWIgPSBudWxsO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdC8vIHVwZGF0ZSBzY3JvbGwgZGF0YSBzaW5jZSB0aGUgdXNlciBtaWdodCBoYXZlIHpvb21lZCwgZXRjLlxuXHRcdFx0XHRcdFx0XHR0aGlzLm1lbnVTY3JvbGxSZWZyZXNoRGF0YSgkc3ViKTtcblx0XHRcdFx0XHRcdFx0Ly8gZXh0ZW5kIGl0IHdpdGggdGhlIHRvdWNoIHByb3BlcnRpZXNcblx0XHRcdFx0XHRcdFx0JC5leHRlbmQoZGF0YSwge1xuXHRcdFx0XHRcdFx0XHRcdHRvdWNoU3RhcnRZOiB0b3VjaFBvaW50LnBhZ2VZLFxuXHRcdFx0XHRcdFx0XHRcdHRvdWNoU3RhcnRUaW1lOiBlLnRpbWVTdGFtcFxuXHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoL21vdmUkL2kudGVzdChlLnR5cGUpKSB7XG5cdFx0XHRcdFx0XHRcdHZhciBwcmV2WSA9IGRhdGEudG91Y2hZICE9PSB1bmRlZmluZWQgPyBkYXRhLnRvdWNoWSA6IGRhdGEudG91Y2hTdGFydFk7XG5cdFx0XHRcdFx0XHRcdGlmIChwcmV2WSAhPT0gdW5kZWZpbmVkICYmIHByZXZZICE9IHRvdWNoUG9pbnQucGFnZVkpIHtcblx0XHRcdFx0XHRcdFx0XHR0aGlzLiR0b3VjaFNjcm9sbGluZ1N1YiA9ICRzdWI7XG5cdFx0XHRcdFx0XHRcdFx0dmFyIHVwID0gcHJldlkgPCB0b3VjaFBvaW50LnBhZ2VZO1xuXHRcdFx0XHRcdFx0XHRcdC8vIGNoYW5nZWQgZGlyZWN0aW9uPyByZXNldC4uLlxuXHRcdFx0XHRcdFx0XHRcdGlmIChkYXRhLnVwICE9PSB1bmRlZmluZWQgJiYgZGF0YS51cCAhPSB1cCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0JC5leHRlbmQoZGF0YSwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR0b3VjaFN0YXJ0WTogdG91Y2hQb2ludC5wYWdlWSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0dG91Y2hTdGFydFRpbWU6IGUudGltZVN0YW1wXG5cdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFx0JC5leHRlbmQoZGF0YSwge1xuXHRcdFx0XHRcdFx0XHRcdFx0dXA6IHVwLFxuXHRcdFx0XHRcdFx0XHRcdFx0dG91Y2hZOiB0b3VjaFBvaW50LnBhZ2VZXG5cdFx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5tZW51U2Nyb2xsKCRzdWIsIHRydWUsIE1hdGguYWJzKHRvdWNoUG9pbnQucGFnZVkgLSBwcmV2WSkpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7IC8vIHRvdWNoZW5kL3BvaW50ZXJ1cFxuXHRcdFx0XHRcdFx0XHRpZiAoZGF0YS50b3VjaFkgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRcdFx0XHRcdGlmIChkYXRhLm1vbWVudHVtID0gTWF0aC5wb3coTWF0aC5hYnModG91Y2hQb2ludC5wYWdlWSAtIGRhdGEudG91Y2hTdGFydFkpIC8gKGUudGltZVN0YW1wIC0gZGF0YS50b3VjaFN0YXJ0VGltZSksIDIpICogMTUpIHtcblx0XHRcdFx0XHRcdFx0XHRcdHRoaXMubWVudVNjcm9sbFN0b3AoJHN1Yik7XG5cdFx0XHRcdFx0XHRcdFx0XHR0aGlzLm1lbnVTY3JvbGwoJHN1Yik7XG5cdFx0XHRcdFx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRcdGRlbGV0ZSBkYXRhLnRvdWNoWTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdG1lbnVTaG93OiBmdW5jdGlvbigkc3ViKSB7XG5cdFx0XHRcdGlmICghJHN1Yi5kYXRhU00oJ2JlZm9yZWZpcnN0c2hvd2ZpcmVkJykpIHtcblx0XHRcdFx0XHQkc3ViLmRhdGFTTSgnYmVmb3JlZmlyc3RzaG93ZmlyZWQnLCB0cnVlKTtcblx0XHRcdFx0XHRpZiAodGhpcy4kcm9vdC50cmlnZ2VySGFuZGxlcignYmVmb3JlZmlyc3RzaG93LnNtYXBpJywgJHN1YlswXSkgPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdiZWZvcmVzaG93LnNtYXBpJywgJHN1YlswXSkgPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdCRzdWIuZGF0YVNNKCdzaG93bi1iZWZvcmUnLCB0cnVlKTtcblx0XHRcdFx0aWYgKGNhbkFuaW1hdGUpIHtcblx0XHRcdFx0XHQkc3ViLnN0b3AodHJ1ZSwgdHJ1ZSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCEkc3ViLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0Ly8gaGlnaGxpZ2h0IHBhcmVudCBpdGVtXG5cdFx0XHRcdFx0dmFyICRhID0gJHN1Yi5kYXRhU00oJ3BhcmVudC1hJyksXG5cdFx0XHRcdFx0XHRjb2xsYXBzaWJsZSA9IHRoaXMuaXNDb2xsYXBzaWJsZSgpO1xuXHRcdFx0XHRcdGlmICh0aGlzLm9wdHMua2VlcEhpZ2hsaWdodGVkIHx8IGNvbGxhcHNpYmxlKSB7XG5cdFx0XHRcdFx0XHQkYS5hZGRDbGFzcygnaGlnaGxpZ2h0ZWQnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aWYgKGNvbGxhcHNpYmxlKSB7XG5cdFx0XHRcdFx0XHQkc3ViLnJlbW92ZUNsYXNzKCdzbS1ub3dyYXAnKS5jc3MoeyB6SW5kZXg6ICcnLCB3aWR0aDogJ2F1dG8nLCBtaW5XaWR0aDogJycsIG1heFdpZHRoOiAnJywgdG9wOiAnJywgbGVmdDogJycsIG1hcmdpbkxlZnQ6ICcnLCBtYXJnaW5Ub3A6ICcnIH0pO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHQvLyBzZXQgei1pbmRleFxuXHRcdFx0XHRcdFx0JHN1Yi5jc3MoJ3otaW5kZXgnLCB0aGlzLnpJbmRleEluYyA9ICh0aGlzLnpJbmRleEluYyB8fCB0aGlzLmdldFN0YXJ0WkluZGV4KCkpICsgMSk7XG5cdFx0XHRcdFx0XHQvLyBtaW4vbWF4LXdpZHRoIGZpeCAtIG5vIHdheSB0byByZWx5IHB1cmVseSBvbiBDU1MgYXMgYWxsIFVMJ3MgYXJlIG5lc3RlZFxuXHRcdFx0XHRcdFx0aWYgKHRoaXMub3B0cy5zdWJNZW51c01pbldpZHRoIHx8IHRoaXMub3B0cy5zdWJNZW51c01heFdpZHRoKSB7XG5cdFx0XHRcdFx0XHRcdCRzdWIuY3NzKHsgd2lkdGg6ICdhdXRvJywgbWluV2lkdGg6ICcnLCBtYXhXaWR0aDogJycgfSkuYWRkQ2xhc3MoJ3NtLW5vd3JhcCcpO1xuXHRcdFx0XHRcdFx0XHRpZiAodGhpcy5vcHRzLnN1Yk1lbnVzTWluV2lkdGgpIHtcblx0XHRcdFx0XHRcdFx0IFx0JHN1Yi5jc3MoJ21pbi13aWR0aCcsIHRoaXMub3B0cy5zdWJNZW51c01pbldpZHRoKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRpZiAodGhpcy5vcHRzLnN1Yk1lbnVzTWF4V2lkdGgpIHtcblx0XHRcdFx0XHRcdFx0IFx0dmFyIG5vTWF4V2lkdGggPSB0aGlzLmdldFdpZHRoKCRzdWIpO1xuXHRcdFx0XHRcdFx0XHQgXHQkc3ViLmNzcygnbWF4LXdpZHRoJywgdGhpcy5vcHRzLnN1Yk1lbnVzTWF4V2lkdGgpO1xuXHRcdFx0XHRcdFx0XHRcdGlmIChub01heFdpZHRoID4gdGhpcy5nZXRXaWR0aCgkc3ViKSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0JHN1Yi5yZW1vdmVDbGFzcygnc20tbm93cmFwJykuY3NzKCd3aWR0aCcsIHRoaXMub3B0cy5zdWJNZW51c01heFdpZHRoKTtcblx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdHRoaXMubWVudVBvc2l0aW9uKCRzdWIpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR2YXIgY29tcGxldGUgPSBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdC8vIGZpeDogXCJvdmVyZmxvdzogaGlkZGVuO1wiIGlzIG5vdCByZXNldCBvbiBhbmltYXRpb24gY29tcGxldGUgaW4galF1ZXJ5IDwgMS45LjAgaW4gQ2hyb21lIHdoZW4gZ2xvYmFsIFwiYm94LXNpemluZzogYm9yZGVyLWJveDtcIiBpcyB1c2VkXG5cdFx0XHRcdFx0XHQkc3ViLmNzcygnb3ZlcmZsb3cnLCAnJyk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0XHQvLyBpZiBzdWIgaXMgY29sbGFwc2libGUgKG1vYmlsZSB2aWV3KVxuXHRcdFx0XHRcdGlmIChjb2xsYXBzaWJsZSkge1xuXHRcdFx0XHRcdFx0aWYgKGNhbkFuaW1hdGUgJiYgdGhpcy5vcHRzLmNvbGxhcHNpYmxlU2hvd0Z1bmN0aW9uKSB7XG5cdFx0XHRcdFx0XHRcdHRoaXMub3B0cy5jb2xsYXBzaWJsZVNob3dGdW5jdGlvbi5jYWxsKHRoaXMsICRzdWIsIGNvbXBsZXRlKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdCRzdWIuc2hvdyh0aGlzLm9wdHMuY29sbGFwc2libGVTaG93RHVyYXRpb24sIGNvbXBsZXRlKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0aWYgKGNhbkFuaW1hdGUgJiYgdGhpcy5vcHRzLnNob3dGdW5jdGlvbikge1xuXHRcdFx0XHRcdFx0XHR0aGlzLm9wdHMuc2hvd0Z1bmN0aW9uLmNhbGwodGhpcywgJHN1YiwgY29tcGxldGUpO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0JHN1Yi5zaG93KHRoaXMub3B0cy5zaG93RHVyYXRpb24sIGNvbXBsZXRlKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0Ly8gYWNjZXNzaWJpbGl0eVxuXHRcdFx0XHRcdCRhLmF0dHIoJ2FyaWEtZXhwYW5kZWQnLCAndHJ1ZScpO1xuXHRcdFx0XHRcdCRzdWIuYXR0cih7XG5cdFx0XHRcdFx0XHQnYXJpYS1leHBhbmRlZCc6ICd0cnVlJyxcblx0XHRcdFx0XHRcdCdhcmlhLWhpZGRlbic6ICdmYWxzZSdcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHQvLyBzdG9yZSBzdWIgbWVudSBpbiB2aXNpYmxlIGFycmF5XG5cdFx0XHRcdFx0dGhpcy52aXNpYmxlU3ViTWVudXMucHVzaCgkc3ViKTtcblx0XHRcdFx0XHR0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdzaG93LnNtYXBpJywgJHN1YlswXSk7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRwb3B1cEhpZGU6IGZ1bmN0aW9uKG5vSGlkZVRpbWVvdXQpIHtcblx0XHRcdFx0aWYgKHRoaXMuaGlkZVRpbWVvdXQpIHtcblx0XHRcdFx0XHRjbGVhclRpbWVvdXQodGhpcy5oaWRlVGltZW91dCk7XG5cdFx0XHRcdFx0dGhpcy5oaWRlVGltZW91dCA9IDA7XG5cdFx0XHRcdH1cblx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XHR0aGlzLmhpZGVUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRzZWxmLm1lbnVIaWRlQWxsKCk7XG5cdFx0XHRcdH0sIG5vSGlkZVRpbWVvdXQgPyAxIDogdGhpcy5vcHRzLmhpZGVUaW1lb3V0KTtcblx0XHRcdH0sXG5cdFx0XHRwb3B1cFNob3c6IGZ1bmN0aW9uKGxlZnQsIHRvcCkge1xuXHRcdFx0XHRpZiAoIXRoaXMub3B0cy5pc1BvcHVwKSB7XG5cdFx0XHRcdFx0YWxlcnQoJ1NtYXJ0TWVudXMgalF1ZXJ5IEVycm9yOlxcblxcbklmIHlvdSB3YW50IHRvIHNob3cgdGhpcyBtZW51IHZpYSB0aGUgXCJwb3B1cFNob3dcIiBtZXRob2QsIHNldCB0aGUgaXNQb3B1cDp0cnVlIG9wdGlvbi4nKTtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKHRoaXMuaGlkZVRpbWVvdXQpIHtcblx0XHRcdFx0XHRjbGVhclRpbWVvdXQodGhpcy5oaWRlVGltZW91dCk7XG5cdFx0XHRcdFx0dGhpcy5oaWRlVGltZW91dCA9IDA7XG5cdFx0XHRcdH1cblx0XHRcdFx0dGhpcy4kcm9vdC5kYXRhU00oJ3Nob3duLWJlZm9yZScsIHRydWUpO1xuXHRcdFx0XHRpZiAoY2FuQW5pbWF0ZSkge1xuXHRcdFx0XHRcdHRoaXMuJHJvb3Quc3RvcCh0cnVlLCB0cnVlKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoIXRoaXMuJHJvb3QuaXMoJzp2aXNpYmxlJykpIHtcblx0XHRcdFx0XHR0aGlzLiRyb290LmNzcyh7IGxlZnQ6IGxlZnQsIHRvcDogdG9wIH0pO1xuXHRcdFx0XHRcdC8vIHNob3cgbWVudVxuXHRcdFx0XHRcdHZhciBzZWxmID0gdGhpcyxcblx0XHRcdFx0XHRcdGNvbXBsZXRlID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdHNlbGYuJHJvb3QuY3NzKCdvdmVyZmxvdycsICcnKTtcblx0XHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0aWYgKGNhbkFuaW1hdGUgJiYgdGhpcy5vcHRzLnNob3dGdW5jdGlvbikge1xuXHRcdFx0XHRcdFx0dGhpcy5vcHRzLnNob3dGdW5jdGlvbi5jYWxsKHRoaXMsIHRoaXMuJHJvb3QsIGNvbXBsZXRlKTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy4kcm9vdC5zaG93KHRoaXMub3B0cy5zaG93RHVyYXRpb24sIGNvbXBsZXRlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dGhpcy52aXNpYmxlU3ViTWVudXNbMF0gPSB0aGlzLiRyb290O1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0cmVmcmVzaDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHRoaXMuZGVzdHJveSh0cnVlKTtcblx0XHRcdFx0dGhpcy5pbml0KHRydWUpO1xuXHRcdFx0fSxcblx0XHRcdHJvb3RLZXlEb3duOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdGlmICghdGhpcy5oYW5kbGVFdmVudHMoKSkge1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHRzd2l0Y2ggKGUua2V5Q29kZSkge1xuXHRcdFx0XHRcdGNhc2UgMjc6IC8vIHJlc2V0IG9uIEVzY1xuXHRcdFx0XHRcdFx0dmFyICRhY3RpdmVUb3BJdGVtID0gdGhpcy5hY3RpdmF0ZWRJdGVtc1swXTtcblx0XHRcdFx0XHRcdGlmICgkYWN0aXZlVG9wSXRlbSkge1xuXHRcdFx0XHRcdFx0XHR0aGlzLm1lbnVIaWRlQWxsKCk7XG5cdFx0XHRcdFx0XHRcdCRhY3RpdmVUb3BJdGVtWzBdLmZvY3VzKCk7XG5cdFx0XHRcdFx0XHRcdHZhciAkc3ViID0gJGFjdGl2ZVRvcEl0ZW0uZGF0YVNNKCdzdWInKTtcblx0XHRcdFx0XHRcdFx0aWYgKCRzdWIpIHtcblx0XHRcdFx0XHRcdFx0XHR0aGlzLm1lbnVIaWRlKCRzdWIpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRjYXNlIDMyOiAvLyBhY3RpdmF0ZSBpdGVtJ3Mgc3ViIG9uIFNwYWNlXG5cdFx0XHRcdFx0XHR2YXIgJHRhcmdldCA9ICQoZS50YXJnZXQpO1xuXHRcdFx0XHRcdFx0aWYgKCR0YXJnZXQuaXMoJ2EnKSAmJiB0aGlzLmhhbmRsZUl0ZW1FdmVudHMoJHRhcmdldCkpIHtcblx0XHRcdFx0XHRcdFx0dmFyICRzdWIgPSAkdGFyZ2V0LmRhdGFTTSgnc3ViJyk7XG5cdFx0XHRcdFx0XHRcdGlmICgkc3ViICYmICEkc3ViLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5pdGVtQ2xpY2soeyBjdXJyZW50VGFyZ2V0OiBlLnRhcmdldCB9KTtcblx0XHRcdFx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0cm9vdE91dDogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRpZiAoIXRoaXMuaGFuZGxlRXZlbnRzKCkgfHwgdGhpcy5pc1RvdWNoTW9kZSgpIHx8IGUudGFyZ2V0ID09IHRoaXMuJHJvb3RbMF0pIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKHRoaXMuaGlkZVRpbWVvdXQpIHtcblx0XHRcdFx0XHRjbGVhclRpbWVvdXQodGhpcy5oaWRlVGltZW91dCk7XG5cdFx0XHRcdFx0dGhpcy5oaWRlVGltZW91dCA9IDA7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCF0aGlzLm9wdHMuc2hvd09uQ2xpY2sgfHwgIXRoaXMub3B0cy5oaWRlT25DbGljaykge1xuXHRcdFx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdFx0XHR0aGlzLmhpZGVUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHsgc2VsZi5tZW51SGlkZUFsbCgpOyB9LCB0aGlzLm9wdHMuaGlkZVRpbWVvdXQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0cm9vdE92ZXI6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0aWYgKCF0aGlzLmhhbmRsZUV2ZW50cygpIHx8IHRoaXMuaXNUb3VjaE1vZGUoKSB8fCBlLnRhcmdldCA9PSB0aGlzLiRyb290WzBdKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0aGlzLmhpZGVUaW1lb3V0KSB7XG5cdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHRoaXMuaGlkZVRpbWVvdXQpO1xuXHRcdFx0XHRcdHRoaXMuaGlkZVRpbWVvdXQgPSAwO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0d2luUmVzaXplOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdGlmICghdGhpcy5oYW5kbGVFdmVudHMoKSkge1xuXHRcdFx0XHRcdC8vIHdlIHN0aWxsIG5lZWQgdG8gcmVzaXplIHRoZSBkaXNhYmxlIG92ZXJsYXkgaWYgaXQncyB2aXNpYmxlXG5cdFx0XHRcdFx0aWYgKHRoaXMuJGRpc2FibGVPdmVybGF5KSB7XG5cdFx0XHRcdFx0XHR2YXIgcG9zID0gdGhpcy4kcm9vdC5vZmZzZXQoKTtcblx0IFx0XHRcdFx0XHR0aGlzLiRkaXNhYmxlT3ZlcmxheS5jc3Moe1xuXHRcdFx0XHRcdFx0XHR0b3A6IHBvcy50b3AsXG5cdFx0XHRcdFx0XHRcdGxlZnQ6IHBvcy5sZWZ0LFxuXHRcdFx0XHRcdFx0XHR3aWR0aDogdGhpcy4kcm9vdC5vdXRlcldpZHRoKCksXG5cdFx0XHRcdFx0XHRcdGhlaWdodDogdGhpcy4kcm9vdC5vdXRlckhlaWdodCgpXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdC8vIGhpZGUgc3ViIG1lbnVzIG9uIHJlc2l6ZSAtIG9uIG1vYmlsZSBkbyBpdCBvbmx5IG9uIG9yaWVudGF0aW9uIGNoYW5nZVxuXHRcdFx0XHRpZiAoISgnb25vcmllbnRhdGlvbmNoYW5nZScgaW4gd2luZG93KSB8fCBlLnR5cGUgPT0gJ29yaWVudGF0aW9uY2hhbmdlJykge1xuXHRcdFx0XHRcdHZhciBjb2xsYXBzaWJsZSA9IHRoaXMuaXNDb2xsYXBzaWJsZSgpO1xuXHRcdFx0XHRcdC8vIGlmIGl0IHdhcyBjb2xsYXBzaWJsZSBiZWZvcmUgcmVzaXplIGFuZCBzdGlsbCBpcywgZG9uJ3QgZG8gaXRcblx0XHRcdFx0XHRpZiAoISh0aGlzLndhc0NvbGxhcHNpYmxlICYmIGNvbGxhcHNpYmxlKSkgeyBcblx0XHRcdFx0XHRcdGlmICh0aGlzLmFjdGl2YXRlZEl0ZW1zLmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0XHR0aGlzLmFjdGl2YXRlZEl0ZW1zW3RoaXMuYWN0aXZhdGVkSXRlbXMubGVuZ3RoIC0gMV1bMF0uYmx1cigpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0dGhpcy5tZW51SGlkZUFsbCgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR0aGlzLndhc0NvbGxhcHNpYmxlID0gY29sbGFwc2libGU7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdH0pO1xuXG5cdCQuZm4uZGF0YVNNID0gZnVuY3Rpb24oa2V5LCB2YWwpIHtcblx0XHRpZiAodmFsKSB7XG5cdFx0XHRyZXR1cm4gdGhpcy5kYXRhKGtleSArICdfc21hcnRtZW51cycsIHZhbCk7XG5cdFx0fVxuXHRcdHJldHVybiB0aGlzLmRhdGEoa2V5ICsgJ19zbWFydG1lbnVzJyk7XG5cdH07XG5cblx0JC5mbi5yZW1vdmVEYXRhU00gPSBmdW5jdGlvbihrZXkpIHtcblx0XHRyZXR1cm4gdGhpcy5yZW1vdmVEYXRhKGtleSArICdfc21hcnRtZW51cycpO1xuXHR9O1xuXG5cdCQuZm4uc21hcnRtZW51cyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcblx0XHRpZiAodHlwZW9mIG9wdGlvbnMgPT0gJ3N0cmluZycpIHtcblx0XHRcdHZhciBhcmdzID0gYXJndW1lbnRzLFxuXHRcdFx0XHRtZXRob2QgPSBvcHRpb25zO1xuXHRcdFx0QXJyYXkucHJvdG90eXBlLnNoaWZ0LmNhbGwoYXJncyk7XG5cdFx0XHRyZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgc21hcnRtZW51cyA9ICQodGhpcykuZGF0YSgnc21hcnRtZW51cycpO1xuXHRcdFx0XHRpZiAoc21hcnRtZW51cyAmJiBzbWFydG1lbnVzW21ldGhvZF0pIHtcblx0XHRcdFx0XHRzbWFydG1lbnVzW21ldGhvZF0uYXBwbHkoc21hcnRtZW51cywgYXJncyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRyZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0Ly8gW2RhdGEtc20tb3B0aW9uc10gYXR0cmlidXRlIG9uIHRoZSByb290IFVMXG5cdFx0XHR2YXIgZGF0YU9wdHMgPSAkKHRoaXMpLmRhdGEoJ3NtLW9wdGlvbnMnKSB8fCBudWxsO1xuXHRcdFx0aWYgKGRhdGFPcHRzICYmIHR5cGVvZiBkYXRhT3B0cyAhPT0gJ29iamVjdCcpIHtcblx0XHRcdFx0dHJ5IHtcblx0XHRcdFx0XHRkYXRhT3B0cyA9IGV2YWwoJygnICsgZGF0YU9wdHMgKyAnKScpO1xuXHRcdFx0XHR9IGNhdGNoKGUpIHtcblx0XHRcdFx0XHRkYXRhT3B0cyA9IG51bGw7XG5cdFx0XHRcdFx0YWxlcnQoJ0VSUk9SXFxuXFxuU21hcnRNZW51cyBqUXVlcnkgaW5pdDpcXG5JbnZhbGlkIFwiZGF0YS1zbS1vcHRpb25zXCIgYXR0cmlidXRlIHZhbHVlIHN5bnRheC4nKTtcblx0XHRcdFx0fTtcblx0XHRcdH1cblx0XHRcdG5ldyAkLlNtYXJ0TWVudXModGhpcywgJC5leHRlbmQoe30sICQuZm4uc21hcnRtZW51cy5kZWZhdWx0cywgb3B0aW9ucywgZGF0YU9wdHMpKTtcblx0XHR9KTtcblx0fTtcblxuXHQvLyBkZWZhdWx0IHNldHRpbmdzXG5cdCQuZm4uc21hcnRtZW51cy5kZWZhdWx0cyA9IHtcblx0XHRpc1BvcHVwOlx0XHRmYWxzZSxcdFx0Ly8gaXMgdGhpcyBhIHBvcHVwIG1lbnUgKGNhbiBiZSBzaG93biB2aWEgdGhlIHBvcHVwU2hvdy9wb3B1cEhpZGUgbWV0aG9kcykgb3IgYSBwZXJtYW5lbnQgbWVudSBiYXJcblx0XHRtYWluTWVudVN1Yk9mZnNldFg6XHQwLFx0XHQvLyBwaXhlbHMgb2Zmc2V0IGZyb20gZGVmYXVsdCBwb3NpdGlvblxuXHRcdG1haW5NZW51U3ViT2Zmc2V0WTpcdDAsXHRcdC8vIHBpeGVscyBvZmZzZXQgZnJvbSBkZWZhdWx0IHBvc2l0aW9uXG5cdFx0c3ViTWVudXNTdWJPZmZzZXRYOlx0MCxcdFx0Ly8gcGl4ZWxzIG9mZnNldCBmcm9tIGRlZmF1bHQgcG9zaXRpb25cblx0XHRzdWJNZW51c1N1Yk9mZnNldFk6XHQwLFx0XHQvLyBwaXhlbHMgb2Zmc2V0IGZyb20gZGVmYXVsdCBwb3NpdGlvblxuXHRcdHN1Yk1lbnVzTWluV2lkdGg6XHQnMTBlbScsXHRcdC8vIG1pbi13aWR0aCBmb3IgdGhlIHN1YiBtZW51cyAoYW55IENTUyB1bml0KSAtIGlmIHNldCwgdGhlIGZpeGVkIHdpZHRoIHNldCBpbiBDU1Mgd2lsbCBiZSBpZ25vcmVkXG5cdFx0c3ViTWVudXNNYXhXaWR0aDpcdCcyMGVtJyxcdFx0Ly8gbWF4LXdpZHRoIGZvciB0aGUgc3ViIG1lbnVzIChhbnkgQ1NTIHVuaXQpIC0gaWYgc2V0LCB0aGUgZml4ZWQgd2lkdGggc2V0IGluIENTUyB3aWxsIGJlIGlnbm9yZWRcblx0XHRzdWJJbmRpY2F0b3JzOiBcdFx0dHJ1ZSxcdFx0Ly8gY3JlYXRlIHN1YiBtZW51IGluZGljYXRvcnMgLSBjcmVhdGVzIGEgU1BBTiBhbmQgaW5zZXJ0cyBpdCBpbiB0aGUgQVxuXHRcdHN1YkluZGljYXRvcnNQb3M6IFx0J2FwcGVuZCcsXHQvLyBwb3NpdGlvbiBvZiB0aGUgU1BBTiByZWxhdGl2ZSB0byB0aGUgbWVudSBpdGVtIGNvbnRlbnQgKCdhcHBlbmQnLCAncHJlcGVuZCcpXG5cdFx0c3ViSW5kaWNhdG9yc1RleHQ6XHQnJyxcdFx0Ly8gW29wdGlvbmFsbHldIGFkZCB0ZXh0IGluIHRoZSBTUEFOIChlLmcuICcrJykgKHlvdSBtYXkgd2FudCB0byBjaGVjayB0aGUgQ1NTIGZvciB0aGUgc3ViIGluZGljYXRvcnMgdG9vKVxuXHRcdHNjcm9sbFN0ZXA6IFx0XHQzMCxcdFx0Ly8gcGl4ZWxzIHN0ZXAgd2hlbiBzY3JvbGxpbmcgbG9uZyBzdWIgbWVudXMgdGhhdCBkbyBub3QgZml0IGluIHRoZSB2aWV3cG9ydCBoZWlnaHRcblx0XHRzY3JvbGxBY2NlbGVyYXRlOlx0dHJ1ZSxcdFx0Ly8gYWNjZWxlcmF0ZSBzY3JvbGxpbmcgb3IgdXNlIGEgZml4ZWQgc3RlcFxuXHRcdHNob3dUaW1lb3V0Olx0XHQyNTAsXHRcdC8vIHRpbWVvdXQgYmVmb3JlIHNob3dpbmcgdGhlIHN1YiBtZW51c1xuXHRcdGhpZGVUaW1lb3V0Olx0XHQ1MDAsXHRcdC8vIHRpbWVvdXQgYmVmb3JlIGhpZGluZyB0aGUgc3ViIG1lbnVzXG5cdFx0c2hvd0R1cmF0aW9uOlx0XHQwLFx0XHQvLyBkdXJhdGlvbiBmb3Igc2hvdyBhbmltYXRpb24gLSBzZXQgdG8gMCBmb3Igbm8gYW5pbWF0aW9uIC0gbWF0dGVycyBvbmx5IGlmIHNob3dGdW5jdGlvbjpudWxsXG5cdFx0c2hvd0Z1bmN0aW9uOlx0XHRudWxsLFx0XHQvLyBjdXN0b20gZnVuY3Rpb24gdG8gdXNlIHdoZW4gc2hvd2luZyBhIHN1YiBtZW51ICh0aGUgZGVmYXVsdCBpcyB0aGUgalF1ZXJ5ICdzaG93Jylcblx0XHRcdFx0XHRcdFx0Ly8gZG9uJ3QgZm9yZ2V0IHRvIGNhbGwgY29tcGxldGUoKSBhdCB0aGUgZW5kIG9mIHdoYXRldmVyIHlvdSBkb1xuXHRcdFx0XHRcdFx0XHQvLyBlLmcuOiBmdW5jdGlvbigkdWwsIGNvbXBsZXRlKSB7ICR1bC5mYWRlSW4oMjUwLCBjb21wbGV0ZSk7IH1cblx0XHRoaWRlRHVyYXRpb246XHRcdDAsXHRcdC8vIGR1cmF0aW9uIGZvciBoaWRlIGFuaW1hdGlvbiAtIHNldCB0byAwIGZvciBubyBhbmltYXRpb24gLSBtYXR0ZXJzIG9ubHkgaWYgaGlkZUZ1bmN0aW9uOm51bGxcblx0XHRoaWRlRnVuY3Rpb246XHRcdGZ1bmN0aW9uKCR1bCwgY29tcGxldGUpIHsgJHVsLmZhZGVPdXQoMjAwLCBjb21wbGV0ZSk7IH0sXHQvLyBjdXN0b20gZnVuY3Rpb24gdG8gdXNlIHdoZW4gaGlkaW5nIGEgc3ViIG1lbnUgKHRoZSBkZWZhdWx0IGlzIHRoZSBqUXVlcnkgJ2hpZGUnKVxuXHRcdFx0XHRcdFx0XHQvLyBkb24ndCBmb3JnZXQgdG8gY2FsbCBjb21wbGV0ZSgpIGF0IHRoZSBlbmQgb2Ygd2hhdGV2ZXIgeW91IGRvXG5cdFx0XHRcdFx0XHRcdC8vIGUuZy46IGZ1bmN0aW9uKCR1bCwgY29tcGxldGUpIHsgJHVsLmZhZGVPdXQoMjUwLCBjb21wbGV0ZSk7IH1cblx0XHRjb2xsYXBzaWJsZVNob3dEdXJhdGlvbjowLFx0XHQvLyBkdXJhdGlvbiBmb3Igc2hvdyBhbmltYXRpb24gZm9yIGNvbGxhcHNpYmxlIHN1YiBtZW51cyAtIG1hdHRlcnMgb25seSBpZiBjb2xsYXBzaWJsZVNob3dGdW5jdGlvbjpudWxsXG5cdFx0Y29sbGFwc2libGVTaG93RnVuY3Rpb246ZnVuY3Rpb24oJHVsLCBjb21wbGV0ZSkgeyAkdWwuc2xpZGVEb3duKDIwMCwgY29tcGxldGUpOyB9LFx0Ly8gY3VzdG9tIGZ1bmN0aW9uIHRvIHVzZSB3aGVuIHNob3dpbmcgYSBjb2xsYXBzaWJsZSBzdWIgbWVudVxuXHRcdFx0XHRcdFx0XHQvLyAoaS5lLiB3aGVuIG1vYmlsZSBzdHlsZXMgYXJlIHVzZWQgdG8gbWFrZSB0aGUgc3ViIG1lbnVzIGNvbGxhcHNpYmxlKVxuXHRcdGNvbGxhcHNpYmxlSGlkZUR1cmF0aW9uOjAsXHRcdC8vIGR1cmF0aW9uIGZvciBoaWRlIGFuaW1hdGlvbiBmb3IgY29sbGFwc2libGUgc3ViIG1lbnVzIC0gbWF0dGVycyBvbmx5IGlmIGNvbGxhcHNpYmxlSGlkZUZ1bmN0aW9uOm51bGxcblx0XHRjb2xsYXBzaWJsZUhpZGVGdW5jdGlvbjpmdW5jdGlvbigkdWwsIGNvbXBsZXRlKSB7ICR1bC5zbGlkZVVwKDIwMCwgY29tcGxldGUpOyB9LFx0Ly8gY3VzdG9tIGZ1bmN0aW9uIHRvIHVzZSB3aGVuIGhpZGluZyBhIGNvbGxhcHNpYmxlIHN1YiBtZW51XG5cdFx0XHRcdFx0XHRcdC8vIChpLmUuIHdoZW4gbW9iaWxlIHN0eWxlcyBhcmUgdXNlZCB0byBtYWtlIHRoZSBzdWIgbWVudXMgY29sbGFwc2libGUpXG5cdFx0c2hvd09uQ2xpY2s6XHRcdGZhbHNlLFx0XHQvLyBzaG93IHRoZSBmaXJzdC1sZXZlbCBzdWIgbWVudXMgb25jbGljayBpbnN0ZWFkIG9mIG9ubW91c2VvdmVyIChpLmUuIG1pbWljIGRlc2t0b3AgYXBwIG1lbnVzKSAobWF0dGVycyBvbmx5IGZvciBtb3VzZSBpbnB1dClcblx0XHRoaWRlT25DbGljazpcdFx0dHJ1ZSxcdFx0Ly8gaGlkZSB0aGUgc3ViIG1lbnVzIG9uIGNsaWNrL3RhcCBhbnl3aGVyZSBvbiB0aGUgcGFnZVxuXHRcdG5vTW91c2VPdmVyOlx0XHRmYWxzZSxcdFx0Ly8gZGlzYWJsZSBzdWIgbWVudXMgYWN0aXZhdGlvbiBvbm1vdXNlb3ZlciAoaS5lLiBiZWhhdmUgbGlrZSBpbiB0b3VjaCBtb2RlIC0gdXNlIGp1c3QgbW91c2UgY2xpY2tzKSAobWF0dGVycyBvbmx5IGZvciBtb3VzZSBpbnB1dClcblx0XHRrZWVwSW5WaWV3cG9ydDpcdFx0dHJ1ZSxcdFx0Ly8gcmVwb3NpdGlvbiB0aGUgc3ViIG1lbnVzIGlmIG5lZWRlZCB0byBtYWtlIHN1cmUgdGhleSBhbHdheXMgYXBwZWFyIGluc2lkZSB0aGUgdmlld3BvcnRcblx0XHRrZWVwSGlnaGxpZ2h0ZWQ6XHR0cnVlLFx0XHQvLyBrZWVwIGFsbCBhbmNlc3RvciBpdGVtcyBvZiB0aGUgY3VycmVudCBzdWIgbWVudSBoaWdobGlnaHRlZCAoYWRkcyB0aGUgJ2hpZ2hsaWdodGVkJyBjbGFzcyB0byB0aGUgQSdzKVxuXHRcdG1hcmtDdXJyZW50SXRlbTpcdGZhbHNlLFx0XHQvLyBhdXRvbWF0aWNhbGx5IGFkZCB0aGUgJ2N1cnJlbnQnIGNsYXNzIHRvIHRoZSBBIGVsZW1lbnQgb2YgdGhlIGl0ZW0gbGlua2luZyB0byB0aGUgY3VycmVudCBVUkxcblx0XHRtYXJrQ3VycmVudFRyZWU6XHR0cnVlLFx0XHQvLyBhZGQgdGhlICdjdXJyZW50JyBjbGFzcyBhbHNvIHRvIHRoZSBBIGVsZW1lbnRzIG9mIGFsbCBhbmNlc3RvciBpdGVtcyBvZiB0aGUgY3VycmVudCBpdGVtXG5cdFx0cmlnaHRUb0xlZnRTdWJNZW51czpcdGZhbHNlLFx0XHQvLyByaWdodCB0byBsZWZ0IGRpc3BsYXkgb2YgdGhlIHN1YiBtZW51cyAoY2hlY2sgdGhlIENTUyBmb3IgdGhlIHN1YiBpbmRpY2F0b3JzJyBwb3NpdGlvbilcblx0XHRib3R0b21Ub1RvcFN1Yk1lbnVzOlx0ZmFsc2UsXHRcdC8vIGJvdHRvbSB0byB0b3AgZGlzcGxheSBvZiB0aGUgc3ViIG1lbnVzXG5cdFx0Y29sbGFwc2libGVCZWhhdmlvcjpcdCdkZWZhdWx0J1x0Ly8gcGFyZW50IGl0ZW1zIGJlaGF2aW9yIGluIGNvbGxhcHNpYmxlIChtb2JpbGUpIHZpZXcgKCdkZWZhdWx0JywgJ3RvZ2dsZScsICdsaW5rJywgJ2FjY29yZGlvbicsICdhY2NvcmRpb24tdG9nZ2xlJywgJ2FjY29yZGlvbi1saW5rJylcblx0XHRcdFx0XHRcdFx0Ly8gJ2RlZmF1bHQnIC0gZmlyc3QgdGFwIG9uIHBhcmVudCBpdGVtIGV4cGFuZHMgc3ViLCBzZWNvbmQgdGFwIGxvYWRzIGl0cyBsaW5rXG5cdFx0XHRcdFx0XHRcdC8vICd0b2dnbGUnIC0gdGhlIHdob2xlIHBhcmVudCBpdGVtIGFjdHMganVzdCBhcyBhIHRvZ2dsZSBidXR0b24gZm9yIGl0cyBzdWIgbWVudSAoZXhwYW5kcy9jb2xsYXBzZXMgb24gZWFjaCB0YXApXG5cdFx0XHRcdFx0XHRcdC8vICdsaW5rJyAtIHRoZSBwYXJlbnQgaXRlbSBhY3RzIGFzIGEgcmVndWxhciBpdGVtIChmaXJzdCB0YXAgbG9hZHMgaXRzIGxpbmspLCB0aGUgc3ViIG1lbnUgY2FuIGJlIGV4cGFuZGVkIG9ubHkgdmlhIHRoZSArLy0gYnV0dG9uXG5cdFx0XHRcdFx0XHRcdC8vICdhY2NvcmRpb24nIC0gbGlrZSAnZGVmYXVsdCcgYnV0IG9uIGV4cGFuZCBhbHNvIHJlc2V0cyBhbnkgdmlzaWJsZSBzdWIgbWVudXMgZnJvbSBkZWVwZXIgbGV2ZWxzIG9yIG90aGVyIGJyYW5jaGVzXG5cdFx0XHRcdFx0XHRcdC8vICdhY2NvcmRpb24tdG9nZ2xlJyAtIGxpa2UgJ3RvZ2dsZScgYnV0IG9uIGV4cGFuZCBhbHNvIHJlc2V0cyBhbnkgdmlzaWJsZSBzdWIgbWVudXMgZnJvbSBkZWVwZXIgbGV2ZWxzIG9yIG90aGVyIGJyYW5jaGVzXG5cdFx0XHRcdFx0XHRcdC8vICdhY2NvcmRpb24tbGluaycgLSBsaWtlICdsaW5rJyBidXQgb24gZXhwYW5kIGFsc28gcmVzZXRzIGFueSB2aXNpYmxlIHN1YiBtZW51cyBmcm9tIGRlZXBlciBsZXZlbHMgb3Igb3RoZXIgYnJhbmNoZXNcblx0fTtcblxuXHRyZXR1cm4gJDtcbn0pKTsiLCIvKiFcbiAqIFNtYXJ0TWVudXMgalF1ZXJ5IFBsdWdpbiAtIHYxLjEuMCAtIFNlcHRlbWJlciAxNywgMjAxN1xuICogaHR0cDovL3d3dy5zbWFydG1lbnVzLm9yZy9cbiAqXG4gKiBDb3B5cmlnaHQgVmFzaWwgRGlua292LCBWYWRpa29tIFdlYiBMdGQuXG4gKiBodHRwOi8vdmFkaWtvbS5jb21cbiAqXG4gKiBMaWNlbnNlZCBNSVRcbiAqL1xuXG4oZnVuY3Rpb24oZmFjdG9yeSkge1xuXHRpZiAodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSB7XG5cdFx0Ly8gQU1EXG5cdFx0ZGVmaW5lKFsnanF1ZXJ5J10sIGZhY3RvcnkpO1xuXHR9IGVsc2UgaWYgKHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUuZXhwb3J0cyA9PT0gJ29iamVjdCcpIHtcblx0XHQvLyBDb21tb25KU1xuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeShyZXF1aXJlKCdqcXVlcnknKSk7XG5cdH0gZWxzZSB7XG5cdFx0Ly8gR2xvYmFsIGpRdWVyeVxuXHRcdGZhY3RvcnkoalF1ZXJ5KTtcblx0fVxufSAoZnVuY3Rpb24oJCkge1xuXG5cdHZhciBtZW51VHJlZXMgPSBbXSxcblx0XHRtb3VzZSA9IGZhbHNlLCAvLyBvcHRpbWl6ZSBmb3IgdG91Y2ggYnkgZGVmYXVsdCAtIHdlIHdpbGwgZGV0ZWN0IGZvciBtb3VzZSBpbnB1dFxuXHRcdHRvdWNoRXZlbnRzID0gJ29udG91Y2hzdGFydCcgaW4gd2luZG93LCAvLyB3ZSB1c2UgdGhpcyBqdXN0IHRvIGNob29zZSBiZXR3ZWVuIHRvdWNuIGFuZCBwb2ludGVyIGV2ZW50cywgbm90IGZvciB0b3VjaCBzY3JlZW4gZGV0ZWN0aW9uXG5cdFx0bW91c2VEZXRlY3Rpb25FbmFibGVkID0gZmFsc2UsXG5cdFx0cmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSB8fCBmdW5jdGlvbihjYWxsYmFjaykgeyByZXR1cm4gc2V0VGltZW91dChjYWxsYmFjaywgMTAwMCAvIDYwKTsgfSxcblx0XHRjYW5jZWxBbmltYXRpb25GcmFtZSA9IHdpbmRvdy5jYW5jZWxBbmltYXRpb25GcmFtZSB8fCBmdW5jdGlvbihpZCkgeyBjbGVhclRpbWVvdXQoaWQpOyB9LFxuXHRcdGNhbkFuaW1hdGUgPSAhISQuZm4uYW5pbWF0ZTtcblxuXHQvLyBIYW5kbGUgZGV0ZWN0aW9uIGZvciBtb3VzZSBpbnB1dCAoaS5lLiBkZXNrdG9wIGJyb3dzZXJzLCB0YWJsZXRzIHdpdGggYSBtb3VzZSwgZXRjLilcblx0ZnVuY3Rpb24gaW5pdE1vdXNlRGV0ZWN0aW9uKGRpc2FibGUpIHtcblx0XHR2YXIgZU5TID0gJy5zbWFydG1lbnVzX21vdXNlJztcblx0XHRpZiAoIW1vdXNlRGV0ZWN0aW9uRW5hYmxlZCAmJiAhZGlzYWJsZSkge1xuXHRcdFx0Ly8gaWYgd2UgZ2V0IHR3byBjb25zZWN1dGl2ZSBtb3VzZW1vdmVzIHdpdGhpbiAyIHBpeGVscyBmcm9tIGVhY2ggb3RoZXIgYW5kIHdpdGhpbiAzMDBtcywgd2UgYXNzdW1lIGEgcmVhbCBtb3VzZS9jdXJzb3IgaXMgcHJlc2VudFxuXHRcdFx0Ly8gaW4gcHJhY3RpY2UsIHRoaXMgc2VlbXMgbGlrZSBpbXBvc3NpYmxlIHRvIHRyaWNrIHVuaW50ZW50aWFuYWxseSB3aXRoIGEgcmVhbCBtb3VzZSBhbmQgYSBwcmV0dHkgc2FmZSBkZXRlY3Rpb24gb24gdG91Y2ggZGV2aWNlcyAoZXZlbiB3aXRoIG9sZGVyIGJyb3dzZXJzIHRoYXQgZG8gbm90IHN1cHBvcnQgdG91Y2ggZXZlbnRzKVxuXHRcdFx0dmFyIGZpcnN0VGltZSA9IHRydWUsXG5cdFx0XHRcdGxhc3RNb3ZlID0gbnVsbCxcblx0XHRcdFx0ZXZlbnRzID0ge1xuXHRcdFx0XHRcdCdtb3VzZW1vdmUnOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdFx0XHR2YXIgdGhpc01vdmUgPSB7IHg6IGUucGFnZVgsIHk6IGUucGFnZVksIHRpbWVTdGFtcDogbmV3IERhdGUoKS5nZXRUaW1lKCkgfTtcblx0XHRcdFx0XHRcdGlmIChsYXN0TW92ZSkge1xuXHRcdFx0XHRcdFx0XHR2YXIgZGVsdGFYID0gTWF0aC5hYnMobGFzdE1vdmUueCAtIHRoaXNNb3ZlLngpLFxuXHRcdFx0XHRcdFx0XHRcdGRlbHRhWSA9IE1hdGguYWJzKGxhc3RNb3ZlLnkgLSB0aGlzTW92ZS55KTtcblx0XHQgXHRcdFx0XHRcdGlmICgoZGVsdGFYID4gMCB8fCBkZWx0YVkgPiAwKSAmJiBkZWx0YVggPD0gMiAmJiBkZWx0YVkgPD0gMiAmJiB0aGlzTW92ZS50aW1lU3RhbXAgLSBsYXN0TW92ZS50aW1lU3RhbXAgPD0gMzAwKSB7XG5cdFx0XHRcdFx0XHRcdFx0bW91c2UgPSB0cnVlO1xuXHRcdFx0XHRcdFx0XHRcdC8vIGlmIHRoaXMgaXMgdGhlIGZpcnN0IGNoZWNrIGFmdGVyIHBhZ2UgbG9hZCwgY2hlY2sgaWYgd2UgYXJlIG5vdCBvdmVyIHNvbWUgaXRlbSBieSBjaGFuY2UgYW5kIGNhbGwgdGhlIG1vdXNlZW50ZXIgaGFuZGxlciBpZiB5ZXNcblx0XHRcdFx0XHRcdFx0XHRpZiAoZmlyc3RUaW1lKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHR2YXIgJGEgPSAkKGUudGFyZ2V0KS5jbG9zZXN0KCdhJyk7XG5cdFx0XHRcdFx0XHRcdFx0XHRpZiAoJGEuaXMoJ2EnKSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHQkLmVhY2gobWVudVRyZWVzLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpZiAoJC5jb250YWlucyh0aGlzLiRyb290WzBdLCAkYVswXSkpIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuaXRlbUVudGVyKHsgY3VycmVudFRhcmdldDogJGFbMF0gfSk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0XHRcdGZpcnN0VGltZSA9IGZhbHNlO1xuXHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0bGFzdE1vdmUgPSB0aGlzTW92ZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH07XG5cdFx0XHRldmVudHNbdG91Y2hFdmVudHMgPyAndG91Y2hzdGFydCcgOiAncG9pbnRlcm92ZXIgcG9pbnRlcm1vdmUgcG9pbnRlcm91dCBNU1BvaW50ZXJPdmVyIE1TUG9pbnRlck1vdmUgTVNQb2ludGVyT3V0J10gPSBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdGlmIChpc1RvdWNoRXZlbnQoZS5vcmlnaW5hbEV2ZW50KSkge1xuXHRcdFx0XHRcdG1vdXNlID0gZmFsc2U7XG5cdFx0XHRcdH1cblx0XHRcdH07XG5cdFx0XHQkKGRvY3VtZW50KS5vbihnZXRFdmVudHNOUyhldmVudHMsIGVOUykpO1xuXHRcdFx0bW91c2VEZXRlY3Rpb25FbmFibGVkID0gdHJ1ZTtcblx0XHR9IGVsc2UgaWYgKG1vdXNlRGV0ZWN0aW9uRW5hYmxlZCAmJiBkaXNhYmxlKSB7XG5cdFx0XHQkKGRvY3VtZW50KS5vZmYoZU5TKTtcblx0XHRcdG1vdXNlRGV0ZWN0aW9uRW5hYmxlZCA9IGZhbHNlO1xuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIGlzVG91Y2hFdmVudChlKSB7XG5cdFx0cmV0dXJuICEvXig0fG1vdXNlKSQvLnRlc3QoZS5wb2ludGVyVHlwZSk7XG5cdH1cblxuXHQvLyByZXR1cm5zIGEgalF1ZXJ5IG9uKCkgcmVhZHkgb2JqZWN0XG5cdGZ1bmN0aW9uIGdldEV2ZW50c05TKGV2ZW50cywgZU5TKSB7XG5cdFx0aWYgKCFlTlMpIHtcblx0XHRcdGVOUyA9ICcnO1xuXHRcdH1cblx0XHR2YXIgZXZlbnRzTlMgPSB7fTtcblx0XHRmb3IgKHZhciBpIGluIGV2ZW50cykge1xuXHRcdFx0ZXZlbnRzTlNbaS5zcGxpdCgnICcpLmpvaW4oZU5TICsgJyAnKSArIGVOU10gPSBldmVudHNbaV07XG5cdFx0fVxuXHRcdHJldHVybiBldmVudHNOUztcblx0fVxuXG5cdCQuU21hcnRNZW51cyA9IGZ1bmN0aW9uKGVsbSwgb3B0aW9ucykge1xuXHRcdHRoaXMuJHJvb3QgPSAkKGVsbSk7XG5cdFx0dGhpcy5vcHRzID0gb3B0aW9ucztcblx0XHR0aGlzLnJvb3RJZCA9ICcnOyAvLyBpbnRlcm5hbFxuXHRcdHRoaXMuYWNjZXNzSWRQcmVmaXggPSAnJztcblx0XHR0aGlzLiRzdWJBcnJvdyA9IG51bGw7XG5cdFx0dGhpcy5hY3RpdmF0ZWRJdGVtcyA9IFtdOyAvLyBzdG9yZXMgbGFzdCBhY3RpdmF0ZWQgQSdzIGZvciBlYWNoIGxldmVsXG5cdFx0dGhpcy52aXNpYmxlU3ViTWVudXMgPSBbXTsgLy8gc3RvcmVzIHZpc2libGUgc3ViIG1lbnVzIFVMJ3MgKG1pZ2h0IGJlIGluIG5vIHBhcnRpY3VsYXIgb3JkZXIpXG5cdFx0dGhpcy5zaG93VGltZW91dCA9IDA7XG5cdFx0dGhpcy5oaWRlVGltZW91dCA9IDA7XG5cdFx0dGhpcy5zY3JvbGxUaW1lb3V0ID0gMDtcblx0XHR0aGlzLmNsaWNrQWN0aXZhdGVkID0gZmFsc2U7XG5cdFx0dGhpcy5mb2N1c0FjdGl2YXRlZCA9IGZhbHNlO1xuXHRcdHRoaXMuekluZGV4SW5jID0gMDtcblx0XHR0aGlzLmlkSW5jID0gMDtcblx0XHR0aGlzLiRmaXJzdExpbmsgPSBudWxsOyAvLyB3ZSdsbCB1c2UgdGhlc2UgZm9yIHNvbWUgdGVzdHNcblx0XHR0aGlzLiRmaXJzdFN1YiA9IG51bGw7IC8vIGF0IHJ1bnRpbWUgc28gd2UnbGwgY2FjaGUgdGhlbVxuXHRcdHRoaXMuZGlzYWJsZWQgPSBmYWxzZTtcblx0XHR0aGlzLiRkaXNhYmxlT3ZlcmxheSA9IG51bGw7XG5cdFx0dGhpcy4kdG91Y2hTY3JvbGxpbmdTdWIgPSBudWxsO1xuXHRcdHRoaXMuY3NzVHJhbnNmb3JtczNkID0gJ3BlcnNwZWN0aXZlJyBpbiBlbG0uc3R5bGUgfHwgJ3dlYmtpdFBlcnNwZWN0aXZlJyBpbiBlbG0uc3R5bGU7XG5cdFx0dGhpcy53YXNDb2xsYXBzaWJsZSA9IGZhbHNlO1xuXHRcdHRoaXMuaW5pdCgpO1xuXHR9O1xuXG5cdCQuZXh0ZW5kKCQuU21hcnRNZW51cywge1xuXHRcdGhpZGVBbGw6IGZ1bmN0aW9uKCkge1xuXHRcdFx0JC5lYWNoKG1lbnVUcmVlcywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHRoaXMubWVudUhpZGVBbGwoKTtcblx0XHRcdH0pO1xuXHRcdH0sXG5cdFx0ZGVzdHJveTogZnVuY3Rpb24oKSB7XG5cdFx0XHR3aGlsZSAobWVudVRyZWVzLmxlbmd0aCkge1xuXHRcdFx0XHRtZW51VHJlZXNbMF0uZGVzdHJveSgpO1xuXHRcdFx0fVxuXHRcdFx0aW5pdE1vdXNlRGV0ZWN0aW9uKHRydWUpO1xuXHRcdH0sXG5cdFx0cHJvdG90eXBlOiB7XG5cdFx0XHRpbml0OiBmdW5jdGlvbihyZWZyZXNoKSB7XG5cdFx0XHRcdHZhciBzZWxmID0gdGhpcztcblxuXHRcdFx0XHRpZiAoIXJlZnJlc2gpIHtcblx0XHRcdFx0XHRtZW51VHJlZXMucHVzaCh0aGlzKTtcblxuXHRcdFx0XHRcdHRoaXMucm9vdElkID0gKG5ldyBEYXRlKCkuZ2V0VGltZSgpICsgTWF0aC5yYW5kb20oKSArICcnKS5yZXBsYWNlKC9cXEQvZywgJycpO1xuXHRcdFx0XHRcdHRoaXMuYWNjZXNzSWRQcmVmaXggPSAnc20tJyArIHRoaXMucm9vdElkICsgJy0nO1xuXG5cdFx0XHRcdFx0aWYgKHRoaXMuJHJvb3QuaGFzQ2xhc3MoJ3NtLXJ0bCcpKSB7XG5cdFx0XHRcdFx0XHR0aGlzLm9wdHMucmlnaHRUb0xlZnRTdWJNZW51cyA9IHRydWU7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0Ly8gaW5pdCByb290IChtYWluIG1lbnUpXG5cdFx0XHRcdFx0dmFyIGVOUyA9ICcuc21hcnRtZW51cyc7XG5cdFx0XHRcdFx0dGhpcy4kcm9vdFxuXHRcdFx0XHRcdFx0LmRhdGEoJ3NtYXJ0bWVudXMnLCB0aGlzKVxuXHRcdFx0XHRcdFx0LmF0dHIoJ2RhdGEtc21hcnRtZW51cy1pZCcsIHRoaXMucm9vdElkKVxuXHRcdFx0XHRcdFx0LmRhdGFTTSgnbGV2ZWwnLCAxKVxuXHRcdFx0XHRcdFx0Lm9uKGdldEV2ZW50c05TKHtcblx0XHRcdFx0XHRcdFx0J21vdXNlb3ZlciBmb2N1c2luJzogJC5wcm94eSh0aGlzLnJvb3RPdmVyLCB0aGlzKSxcblx0XHRcdFx0XHRcdFx0J21vdXNlb3V0IGZvY3Vzb3V0JzogJC5wcm94eSh0aGlzLnJvb3RPdXQsIHRoaXMpLFxuXHRcdFx0XHRcdFx0XHQna2V5ZG93bic6ICQucHJveHkodGhpcy5yb290S2V5RG93biwgdGhpcylcblx0XHRcdFx0XHRcdH0sIGVOUykpXG5cdFx0XHRcdFx0XHQub24oZ2V0RXZlbnRzTlMoe1xuXHRcdFx0XHRcdFx0XHQnbW91c2VlbnRlcic6ICQucHJveHkodGhpcy5pdGVtRW50ZXIsIHRoaXMpLFxuXHRcdFx0XHRcdFx0XHQnbW91c2VsZWF2ZSc6ICQucHJveHkodGhpcy5pdGVtTGVhdmUsIHRoaXMpLFxuXHRcdFx0XHRcdFx0XHQnbW91c2Vkb3duJzogJC5wcm94eSh0aGlzLml0ZW1Eb3duLCB0aGlzKSxcblx0XHRcdFx0XHRcdFx0J2ZvY3VzJzogJC5wcm94eSh0aGlzLml0ZW1Gb2N1cywgdGhpcyksXG5cdFx0XHRcdFx0XHRcdCdibHVyJzogJC5wcm94eSh0aGlzLml0ZW1CbHVyLCB0aGlzKSxcblx0XHRcdFx0XHRcdFx0J2NsaWNrJzogJC5wcm94eSh0aGlzLml0ZW1DbGljaywgdGhpcylcblx0XHRcdFx0XHRcdH0sIGVOUyksICdhJyk7XG5cblx0XHRcdFx0XHQvLyBoaWRlIG1lbnVzIG9uIHRhcCBvciBjbGljayBvdXRzaWRlIHRoZSByb290IFVMXG5cdFx0XHRcdFx0ZU5TICs9IHRoaXMucm9vdElkO1xuXHRcdFx0XHRcdGlmICh0aGlzLm9wdHMuaGlkZU9uQ2xpY2spIHtcblx0XHRcdFx0XHRcdCQoZG9jdW1lbnQpLm9uKGdldEV2ZW50c05TKHtcblx0XHRcdFx0XHRcdFx0J3RvdWNoc3RhcnQnOiAkLnByb3h5KHRoaXMuZG9jVG91Y2hTdGFydCwgdGhpcyksXG5cdFx0XHRcdFx0XHRcdCd0b3VjaG1vdmUnOiAkLnByb3h5KHRoaXMuZG9jVG91Y2hNb3ZlLCB0aGlzKSxcblx0XHRcdFx0XHRcdFx0J3RvdWNoZW5kJzogJC5wcm94eSh0aGlzLmRvY1RvdWNoRW5kLCB0aGlzKSxcblx0XHRcdFx0XHRcdFx0Ly8gZm9yIE9wZXJhIE1vYmlsZSA8IDExLjUsIHdlYk9TIGJyb3dzZXIsIGV0Yy4gd2UnbGwgY2hlY2sgY2xpY2sgdG9vXG5cdFx0XHRcdFx0XHRcdCdjbGljayc6ICQucHJveHkodGhpcy5kb2NDbGljaywgdGhpcylcblx0XHRcdFx0XHRcdH0sIGVOUykpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQvLyBoaWRlIHN1YiBtZW51cyBvbiByZXNpemVcblx0XHRcdFx0XHQkKHdpbmRvdykub24oZ2V0RXZlbnRzTlMoeyAncmVzaXplIG9yaWVudGF0aW9uY2hhbmdlJzogJC5wcm94eSh0aGlzLndpblJlc2l6ZSwgdGhpcykgfSwgZU5TKSk7XG5cblx0XHRcdFx0XHRpZiAodGhpcy5vcHRzLnN1YkluZGljYXRvcnMpIHtcblx0XHRcdFx0XHRcdHRoaXMuJHN1YkFycm93ID0gJCgnPHNwYW4vPicpLmFkZENsYXNzKCdzdWItYXJyb3cnKTtcblx0XHRcdFx0XHRcdGlmICh0aGlzLm9wdHMuc3ViSW5kaWNhdG9yc1RleHQpIHtcblx0XHRcdFx0XHRcdFx0dGhpcy4kc3ViQXJyb3cuaHRtbCh0aGlzLm9wdHMuc3ViSW5kaWNhdG9yc1RleHQpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdC8vIG1ha2Ugc3VyZSBtb3VzZSBkZXRlY3Rpb24gaXMgZW5hYmxlZFxuXHRcdFx0XHRcdGluaXRNb3VzZURldGVjdGlvbigpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly8gaW5pdCBzdWIgbWVudXNcblx0XHRcdFx0dGhpcy4kZmlyc3RTdWIgPSB0aGlzLiRyb290LmZpbmQoJ3VsJykuZWFjaChmdW5jdGlvbigpIHsgc2VsZi5tZW51SW5pdCgkKHRoaXMpKTsgfSkuZXEoMCk7XG5cblx0XHRcdFx0dGhpcy4kZmlyc3RMaW5rID0gdGhpcy4kcm9vdC5maW5kKCdhJykuZXEoMCk7XG5cblx0XHRcdFx0Ly8gZmluZCBjdXJyZW50IGl0ZW1cblx0XHRcdFx0aWYgKHRoaXMub3B0cy5tYXJrQ3VycmVudEl0ZW0pIHtcblx0XHRcdFx0XHR2YXIgcmVEZWZhdWx0RG9jID0gLyhpbmRleHxkZWZhdWx0KVxcLlteI1xcP1xcL10qL2ksXG5cdFx0XHRcdFx0XHRyZUhhc2ggPSAvIy4qLyxcblx0XHRcdFx0XHRcdGxvY0hyZWYgPSB3aW5kb3cubG9jYXRpb24uaHJlZi5yZXBsYWNlKHJlRGVmYXVsdERvYywgJycpLFxuXHRcdFx0XHRcdFx0bG9jSHJlZk5vSGFzaCA9IGxvY0hyZWYucmVwbGFjZShyZUhhc2gsICcnKTtcblx0XHRcdFx0XHR0aGlzLiRyb290LmZpbmQoJ2EnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0dmFyIGhyZWYgPSB0aGlzLmhyZWYucmVwbGFjZShyZURlZmF1bHREb2MsICcnKSxcblx0XHRcdFx0XHRcdFx0JHRoaXMgPSAkKHRoaXMpO1xuXHRcdFx0XHRcdFx0aWYgKGhyZWYgPT0gbG9jSHJlZiB8fCBocmVmID09IGxvY0hyZWZOb0hhc2gpIHtcblx0XHRcdFx0XHRcdFx0JHRoaXMuYWRkQ2xhc3MoJ2N1cnJlbnQnKTtcblx0XHRcdFx0XHRcdFx0aWYgKHNlbGYub3B0cy5tYXJrQ3VycmVudFRyZWUpIHtcblx0XHRcdFx0XHRcdFx0XHQkdGhpcy5wYXJlbnRzVW50aWwoJ1tkYXRhLXNtYXJ0bWVudXMtaWRdJywgJ3VsJykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0XHRcdCQodGhpcykuZGF0YVNNKCdwYXJlbnQtYScpLmFkZENsYXNzKCdjdXJyZW50Jyk7XG5cdFx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8vIHNhdmUgaW5pdGlhbCBzdGF0ZVxuXHRcdFx0XHR0aGlzLndhc0NvbGxhcHNpYmxlID0gdGhpcy5pc0NvbGxhcHNpYmxlKCk7XG5cdFx0XHR9LFxuXHRcdFx0ZGVzdHJveTogZnVuY3Rpb24ocmVmcmVzaCkge1xuXHRcdFx0XHRpZiAoIXJlZnJlc2gpIHtcblx0XHRcdFx0XHR2YXIgZU5TID0gJy5zbWFydG1lbnVzJztcblx0XHRcdFx0XHR0aGlzLiRyb290XG5cdFx0XHRcdFx0XHQucmVtb3ZlRGF0YSgnc21hcnRtZW51cycpXG5cdFx0XHRcdFx0XHQucmVtb3ZlQXR0cignZGF0YS1zbWFydG1lbnVzLWlkJylcblx0XHRcdFx0XHRcdC5yZW1vdmVEYXRhU00oJ2xldmVsJylcblx0XHRcdFx0XHRcdC5vZmYoZU5TKTtcblx0XHRcdFx0XHRlTlMgKz0gdGhpcy5yb290SWQ7XG5cdFx0XHRcdFx0JChkb2N1bWVudCkub2ZmKGVOUyk7XG5cdFx0XHRcdFx0JCh3aW5kb3cpLm9mZihlTlMpO1xuXHRcdFx0XHRcdGlmICh0aGlzLm9wdHMuc3ViSW5kaWNhdG9ycykge1xuXHRcdFx0XHRcdFx0dGhpcy4kc3ViQXJyb3cgPSBudWxsO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XHR0aGlzLm1lbnVIaWRlQWxsKCk7XG5cdFx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdFx0dGhpcy4kcm9vdC5maW5kKCd1bCcpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHR2YXIgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFx0XHRcdFx0aWYgKCR0aGlzLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpKSB7XG5cdFx0XHRcdFx0XHRcdCR0aGlzLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLnJlbW92ZSgpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0aWYgKCR0aGlzLmRhdGFTTSgnc2hvd24tYmVmb3JlJykpIHtcblx0XHRcdFx0XHRcdFx0aWYgKHNlbGYub3B0cy5zdWJNZW51c01pbldpZHRoIHx8IHNlbGYub3B0cy5zdWJNZW51c01heFdpZHRoKSB7XG5cdFx0XHRcdFx0XHRcdFx0JHRoaXMuY3NzKHsgd2lkdGg6ICcnLCBtaW5XaWR0aDogJycsIG1heFdpZHRoOiAnJyB9KS5yZW1vdmVDbGFzcygnc20tbm93cmFwJyk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0aWYgKCR0aGlzLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpKSB7XG5cdFx0XHRcdFx0XHRcdFx0JHRoaXMuZGF0YVNNKCdzY3JvbGwtYXJyb3dzJykucmVtb3ZlKCk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0JHRoaXMuY3NzKHsgekluZGV4OiAnJywgdG9wOiAnJywgbGVmdDogJycsIG1hcmdpbkxlZnQ6ICcnLCBtYXJnaW5Ub3A6ICcnLCBkaXNwbGF5OiAnJyB9KTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdGlmICgoJHRoaXMuYXR0cignaWQnKSB8fCAnJykuaW5kZXhPZihzZWxmLmFjY2Vzc0lkUHJlZml4KSA9PSAwKSB7XG5cdFx0XHRcdFx0XHRcdCR0aGlzLnJlbW92ZUF0dHIoJ2lkJyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQucmVtb3ZlRGF0YVNNKCdpbi1tZWdhJylcblx0XHRcdFx0XHQucmVtb3ZlRGF0YVNNKCdzaG93bi1iZWZvcmUnKVxuXHRcdFx0XHRcdC5yZW1vdmVEYXRhU00oJ3Njcm9sbC1hcnJvd3MnKVxuXHRcdFx0XHRcdC5yZW1vdmVEYXRhU00oJ3BhcmVudC1hJylcblx0XHRcdFx0XHQucmVtb3ZlRGF0YVNNKCdsZXZlbCcpXG5cdFx0XHRcdFx0LnJlbW92ZURhdGFTTSgnYmVmb3JlZmlyc3RzaG93ZmlyZWQnKVxuXHRcdFx0XHRcdC5yZW1vdmVBdHRyKCdyb2xlJylcblx0XHRcdFx0XHQucmVtb3ZlQXR0cignYXJpYS1oaWRkZW4nKVxuXHRcdFx0XHRcdC5yZW1vdmVBdHRyKCdhcmlhLWxhYmVsbGVkYnknKVxuXHRcdFx0XHRcdC5yZW1vdmVBdHRyKCdhcmlhLWV4cGFuZGVkJyk7XG5cdFx0XHRcdHRoaXMuJHJvb3QuZmluZCgnYS5oYXMtc3VibWVudScpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHR2YXIgJHRoaXMgPSAkKHRoaXMpO1xuXHRcdFx0XHRcdFx0aWYgKCR0aGlzLmF0dHIoJ2lkJykuaW5kZXhPZihzZWxmLmFjY2Vzc0lkUHJlZml4KSA9PSAwKSB7XG5cdFx0XHRcdFx0XHRcdCR0aGlzLnJlbW92ZUF0dHIoJ2lkJyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQucmVtb3ZlQ2xhc3MoJ2hhcy1zdWJtZW51Jylcblx0XHRcdFx0XHQucmVtb3ZlRGF0YVNNKCdzdWInKVxuXHRcdFx0XHRcdC5yZW1vdmVBdHRyKCdhcmlhLWhhc3BvcHVwJylcblx0XHRcdFx0XHQucmVtb3ZlQXR0cignYXJpYS1jb250cm9scycpXG5cdFx0XHRcdFx0LnJlbW92ZUF0dHIoJ2FyaWEtZXhwYW5kZWQnKVxuXHRcdFx0XHRcdC5jbG9zZXN0KCdsaScpLnJlbW92ZURhdGFTTSgnc3ViJyk7XG5cdFx0XHRcdGlmICh0aGlzLm9wdHMuc3ViSW5kaWNhdG9ycykge1xuXHRcdFx0XHRcdHRoaXMuJHJvb3QuZmluZCgnc3Bhbi5zdWItYXJyb3cnKS5yZW1vdmUoKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAodGhpcy5vcHRzLm1hcmtDdXJyZW50SXRlbSkge1xuXHRcdFx0XHRcdHRoaXMuJHJvb3QuZmluZCgnYS5jdXJyZW50JykucmVtb3ZlQ2xhc3MoJ2N1cnJlbnQnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoIXJlZnJlc2gpIHtcblx0XHRcdFx0XHR0aGlzLiRyb290ID0gbnVsbDtcblx0XHRcdFx0XHR0aGlzLiRmaXJzdExpbmsgPSBudWxsO1xuXHRcdFx0XHRcdHRoaXMuJGZpcnN0U3ViID0gbnVsbDtcblx0XHRcdFx0XHRpZiAodGhpcy4kZGlzYWJsZU92ZXJsYXkpIHtcblx0XHRcdFx0XHRcdHRoaXMuJGRpc2FibGVPdmVybGF5LnJlbW92ZSgpO1xuXHRcdFx0XHRcdFx0dGhpcy4kZGlzYWJsZU92ZXJsYXkgPSBudWxsO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRtZW51VHJlZXMuc3BsaWNlKCQuaW5BcnJheSh0aGlzLCBtZW51VHJlZXMpLCAxKTtcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdGRpc2FibGU6IGZ1bmN0aW9uKG5vT3ZlcmxheSkge1xuXHRcdFx0XHRpZiAoIXRoaXMuZGlzYWJsZWQpIHtcblx0XHRcdFx0XHR0aGlzLm1lbnVIaWRlQWxsKCk7XG5cdFx0XHRcdFx0Ly8gZGlzcGxheSBvdmVybGF5IG92ZXIgdGhlIG1lbnUgdG8gcHJldmVudCBpbnRlcmFjdGlvblxuXHRcdFx0XHRcdGlmICghbm9PdmVybGF5ICYmICF0aGlzLm9wdHMuaXNQb3B1cCAmJiB0aGlzLiRyb290LmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0XHR2YXIgcG9zID0gdGhpcy4kcm9vdC5vZmZzZXQoKTtcblx0XHRcdFx0XHRcdHRoaXMuJGRpc2FibGVPdmVybGF5ID0gJCgnPGRpdiBjbGFzcz1cInNtLWpxdWVyeS1kaXNhYmxlLW92ZXJsYXlcIi8+JykuY3NzKHtcblx0XHRcdFx0XHRcdFx0cG9zaXRpb246ICdhYnNvbHV0ZScsXG5cdFx0XHRcdFx0XHRcdHRvcDogcG9zLnRvcCxcblx0XHRcdFx0XHRcdFx0bGVmdDogcG9zLmxlZnQsXG5cdFx0XHRcdFx0XHRcdHdpZHRoOiB0aGlzLiRyb290Lm91dGVyV2lkdGgoKSxcblx0XHRcdFx0XHRcdFx0aGVpZ2h0OiB0aGlzLiRyb290Lm91dGVySGVpZ2h0KCksXG5cdFx0XHRcdFx0XHRcdHpJbmRleDogdGhpcy5nZXRTdGFydFpJbmRleCh0cnVlKSxcblx0XHRcdFx0XHRcdFx0b3BhY2l0eTogMFxuXHRcdFx0XHRcdFx0fSkuYXBwZW5kVG8oZG9jdW1lbnQuYm9keSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHRoaXMuZGlzYWJsZWQgPSB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0ZG9jQ2xpY2s6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0aWYgKHRoaXMuJHRvdWNoU2Nyb2xsaW5nU3ViKSB7XG5cdFx0XHRcdFx0dGhpcy4kdG91Y2hTY3JvbGxpbmdTdWIgPSBudWxsO1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHQvLyBoaWRlIG9uIGFueSBjbGljayBvdXRzaWRlIHRoZSBtZW51IG9yIG9uIGEgbWVudSBsaW5rXG5cdFx0XHRcdGlmICh0aGlzLnZpc2libGVTdWJNZW51cy5sZW5ndGggJiYgISQuY29udGFpbnModGhpcy4kcm9vdFswXSwgZS50YXJnZXQpIHx8ICQoZS50YXJnZXQpLmNsb3Nlc3QoJ2EnKS5sZW5ndGgpIHtcblx0XHRcdFx0XHR0aGlzLm1lbnVIaWRlQWxsKCk7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRkb2NUb3VjaEVuZDogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRpZiAoIXRoaXMubGFzdFRvdWNoKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0aGlzLnZpc2libGVTdWJNZW51cy5sZW5ndGggJiYgKHRoaXMubGFzdFRvdWNoLngyID09PSB1bmRlZmluZWQgfHwgdGhpcy5sYXN0VG91Y2gueDEgPT0gdGhpcy5sYXN0VG91Y2gueDIpICYmICh0aGlzLmxhc3RUb3VjaC55MiA9PT0gdW5kZWZpbmVkIHx8IHRoaXMubGFzdFRvdWNoLnkxID09IHRoaXMubGFzdFRvdWNoLnkyKSAmJiAoIXRoaXMubGFzdFRvdWNoLnRhcmdldCB8fCAhJC5jb250YWlucyh0aGlzLiRyb290WzBdLCB0aGlzLmxhc3RUb3VjaC50YXJnZXQpKSkge1xuXHRcdFx0XHRcdGlmICh0aGlzLmhpZGVUaW1lb3V0KSB7XG5cdFx0XHRcdFx0XHRjbGVhclRpbWVvdXQodGhpcy5oaWRlVGltZW91dCk7XG5cdFx0XHRcdFx0XHR0aGlzLmhpZGVUaW1lb3V0ID0gMDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0Ly8gaGlkZSB3aXRoIGEgZGVsYXkgdG8gcHJldmVudCB0cmlnZ2VyaW5nIGFjY2lkZW50YWwgdW53YW50ZWQgY2xpY2sgb24gc29tZSBwYWdlIGVsZW1lbnRcblx0XHRcdFx0XHR2YXIgc2VsZiA9IHRoaXM7XG5cdFx0XHRcdFx0dGhpcy5oaWRlVGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7IHNlbGYubWVudUhpZGVBbGwoKTsgfSwgMzUwKTtcblx0XHRcdFx0fVxuXHRcdFx0XHR0aGlzLmxhc3RUb3VjaCA9IG51bGw7XG5cdFx0XHR9LFxuXHRcdFx0ZG9jVG91Y2hNb3ZlOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdGlmICghdGhpcy5sYXN0VG91Y2gpIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0dmFyIHRvdWNoUG9pbnQgPSBlLm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXTtcblx0XHRcdFx0dGhpcy5sYXN0VG91Y2gueDIgPSB0b3VjaFBvaW50LnBhZ2VYO1xuXHRcdFx0XHR0aGlzLmxhc3RUb3VjaC55MiA9IHRvdWNoUG9pbnQucGFnZVk7XG5cdFx0XHR9LFxuXHRcdFx0ZG9jVG91Y2hTdGFydDogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHR2YXIgdG91Y2hQb2ludCA9IGUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdO1xuXHRcdFx0XHR0aGlzLmxhc3RUb3VjaCA9IHsgeDE6IHRvdWNoUG9pbnQucGFnZVgsIHkxOiB0b3VjaFBvaW50LnBhZ2VZLCB0YXJnZXQ6IHRvdWNoUG9pbnQudGFyZ2V0IH07XG5cdFx0XHR9LFxuXHRcdFx0ZW5hYmxlOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0aWYgKHRoaXMuZGlzYWJsZWQpIHtcblx0XHRcdFx0XHRpZiAodGhpcy4kZGlzYWJsZU92ZXJsYXkpIHtcblx0XHRcdFx0XHRcdHRoaXMuJGRpc2FibGVPdmVybGF5LnJlbW92ZSgpO1xuXHRcdFx0XHRcdFx0dGhpcy4kZGlzYWJsZU92ZXJsYXkgPSBudWxsO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR0aGlzLmRpc2FibGVkID0gZmFsc2U7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRnZXRDbG9zZXN0TWVudTogZnVuY3Rpb24oZWxtKSB7XG5cdFx0XHRcdHZhciAkY2xvc2VzdE1lbnUgPSAkKGVsbSkuY2xvc2VzdCgndWwnKTtcblx0XHRcdFx0d2hpbGUgKCRjbG9zZXN0TWVudS5kYXRhU00oJ2luLW1lZ2EnKSkge1xuXHRcdFx0XHRcdCRjbG9zZXN0TWVudSA9ICRjbG9zZXN0TWVudS5wYXJlbnQoKS5jbG9zZXN0KCd1bCcpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHJldHVybiAkY2xvc2VzdE1lbnVbMF0gfHwgbnVsbDtcblx0XHRcdH0sXG5cdFx0XHRnZXRIZWlnaHQ6IGZ1bmN0aW9uKCRlbG0pIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMuZ2V0T2Zmc2V0KCRlbG0sIHRydWUpO1xuXHRcdFx0fSxcblx0XHRcdC8vIHJldHVybnMgcHJlY2lzZSB3aWR0aC9oZWlnaHQgZmxvYXQgdmFsdWVzXG5cdFx0XHRnZXRPZmZzZXQ6IGZ1bmN0aW9uKCRlbG0sIGhlaWdodCkge1xuXHRcdFx0XHR2YXIgb2xkO1xuXHRcdFx0XHRpZiAoJGVsbS5jc3MoJ2Rpc3BsYXknKSA9PSAnbm9uZScpIHtcblx0XHRcdFx0XHRvbGQgPSB7IHBvc2l0aW9uOiAkZWxtWzBdLnN0eWxlLnBvc2l0aW9uLCB2aXNpYmlsaXR5OiAkZWxtWzBdLnN0eWxlLnZpc2liaWxpdHkgfTtcblx0XHRcdFx0XHQkZWxtLmNzcyh7IHBvc2l0aW9uOiAnYWJzb2x1dGUnLCB2aXNpYmlsaXR5OiAnaGlkZGVuJyB9KS5zaG93KCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0dmFyIGJveCA9ICRlbG1bMF0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0ICYmICRlbG1bMF0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXG5cdFx0XHRcdFx0dmFsID0gYm94ICYmIChoZWlnaHQgPyBib3guaGVpZ2h0IHx8IGJveC5ib3R0b20gLSBib3gudG9wIDogYm94LndpZHRoIHx8IGJveC5yaWdodCAtIGJveC5sZWZ0KTtcblx0XHRcdFx0aWYgKCF2YWwgJiYgdmFsICE9PSAwKSB7XG5cdFx0XHRcdFx0dmFsID0gaGVpZ2h0ID8gJGVsbVswXS5vZmZzZXRIZWlnaHQgOiAkZWxtWzBdLm9mZnNldFdpZHRoO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmIChvbGQpIHtcblx0XHRcdFx0XHQkZWxtLmhpZGUoKS5jc3Mob2xkKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRyZXR1cm4gdmFsO1xuXHRcdFx0fSxcblx0XHRcdGdldFN0YXJ0WkluZGV4OiBmdW5jdGlvbihyb290KSB7XG5cdFx0XHRcdHZhciB6SW5kZXggPSBwYXJzZUludCh0aGlzW3Jvb3QgPyAnJHJvb3QnIDogJyRmaXJzdFN1YiddLmNzcygnei1pbmRleCcpKTtcblx0XHRcdFx0aWYgKCFyb290ICYmIGlzTmFOKHpJbmRleCkpIHtcblx0XHRcdFx0XHR6SW5kZXggPSBwYXJzZUludCh0aGlzLiRyb290LmNzcygnei1pbmRleCcpKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRyZXR1cm4gIWlzTmFOKHpJbmRleCkgPyB6SW5kZXggOiAxO1xuXHRcdFx0fSxcblx0XHRcdGdldFRvdWNoUG9pbnQ6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0cmV0dXJuIGUudG91Y2hlcyAmJiBlLnRvdWNoZXNbMF0gfHwgZS5jaGFuZ2VkVG91Y2hlcyAmJiBlLmNoYW5nZWRUb3VjaGVzWzBdIHx8IGU7XG5cdFx0XHR9LFxuXHRcdFx0Z2V0Vmlld3BvcnQ6IGZ1bmN0aW9uKGhlaWdodCkge1xuXHRcdFx0XHR2YXIgbmFtZSA9IGhlaWdodCA/ICdIZWlnaHQnIDogJ1dpZHRoJyxcblx0XHRcdFx0XHR2YWwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnRbJ2NsaWVudCcgKyBuYW1lXSxcblx0XHRcdFx0XHR2YWwyID0gd2luZG93Wydpbm5lcicgKyBuYW1lXTtcblx0XHRcdFx0aWYgKHZhbDIpIHtcblx0XHRcdFx0XHR2YWwgPSBNYXRoLm1pbih2YWwsIHZhbDIpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHJldHVybiB2YWw7XG5cdFx0XHR9LFxuXHRcdFx0Z2V0Vmlld3BvcnRIZWlnaHQ6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gdGhpcy5nZXRWaWV3cG9ydCh0cnVlKTtcblx0XHRcdH0sXG5cdFx0XHRnZXRWaWV3cG9ydFdpZHRoOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMuZ2V0Vmlld3BvcnQoKTtcblx0XHRcdH0sXG5cdFx0XHRnZXRXaWR0aDogZnVuY3Rpb24oJGVsbSkge1xuXHRcdFx0XHRyZXR1cm4gdGhpcy5nZXRPZmZzZXQoJGVsbSk7XG5cdFx0XHR9LFxuXHRcdFx0aGFuZGxlRXZlbnRzOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0cmV0dXJuICF0aGlzLmRpc2FibGVkICYmIHRoaXMuaXNDU1NPbigpO1xuXHRcdFx0fSxcblx0XHRcdGhhbmRsZUl0ZW1FdmVudHM6IGZ1bmN0aW9uKCRhKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLmhhbmRsZUV2ZW50cygpICYmICF0aGlzLmlzTGlua0luTWVnYU1lbnUoJGEpO1xuXHRcdFx0fSxcblx0XHRcdGlzQ29sbGFwc2libGU6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gdGhpcy4kZmlyc3RTdWIuY3NzKCdwb3NpdGlvbicpID09ICdzdGF0aWMnO1xuXHRcdFx0fSxcblx0XHRcdGlzQ1NTT246IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gdGhpcy4kZmlyc3RMaW5rLmNzcygnZGlzcGxheScpICE9ICdpbmxpbmUnO1xuXHRcdFx0fSxcblx0XHRcdGlzRml4ZWQ6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgaXNGaXhlZCA9IHRoaXMuJHJvb3QuY3NzKCdwb3NpdGlvbicpID09ICdmaXhlZCc7XG5cdFx0XHRcdGlmICghaXNGaXhlZCkge1xuXHRcdFx0XHRcdHRoaXMuJHJvb3QucGFyZW50c1VudGlsKCdib2R5JykuZWFjaChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdGlmICgkKHRoaXMpLmNzcygncG9zaXRpb24nKSA9PSAnZml4ZWQnKSB7XG5cdFx0XHRcdFx0XHRcdGlzRml4ZWQgPSB0cnVlO1xuXHRcdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0cmV0dXJuIGlzRml4ZWQ7XG5cdFx0XHR9LFxuXHRcdFx0aXNMaW5rSW5NZWdhTWVudTogZnVuY3Rpb24oJGEpIHtcblx0XHRcdFx0cmV0dXJuICQodGhpcy5nZXRDbG9zZXN0TWVudSgkYVswXSkpLmhhc0NsYXNzKCdtZWdhLW1lbnUnKTtcblx0XHRcdH0sXG5cdFx0XHRpc1RvdWNoTW9kZTogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHJldHVybiAhbW91c2UgfHwgdGhpcy5vcHRzLm5vTW91c2VPdmVyIHx8IHRoaXMuaXNDb2xsYXBzaWJsZSgpO1xuXHRcdFx0fSxcblx0XHRcdGl0ZW1BY3RpdmF0ZTogZnVuY3Rpb24oJGEsIGhpZGVEZWVwZXJTdWJzKSB7XG5cdFx0XHRcdHZhciAkdWwgPSAkYS5jbG9zZXN0KCd1bCcpLFxuXHRcdFx0XHRcdGxldmVsID0gJHVsLmRhdGFTTSgnbGV2ZWwnKTtcblx0XHRcdFx0Ly8gaWYgZm9yIHNvbWUgcmVhc29uIHRoZSBwYXJlbnQgaXRlbSBpcyBub3QgYWN0aXZhdGVkIChlLmcuIHRoaXMgaXMgYW4gQVBJIGNhbGwgdG8gYWN0aXZhdGUgdGhlIGl0ZW0pLCBhY3RpdmF0ZSBhbGwgcGFyZW50IGl0ZW1zIGZpcnN0XG5cdFx0XHRcdGlmIChsZXZlbCA+IDEgJiYgKCF0aGlzLmFjdGl2YXRlZEl0ZW1zW2xldmVsIC0gMl0gfHwgdGhpcy5hY3RpdmF0ZWRJdGVtc1tsZXZlbCAtIDJdWzBdICE9ICR1bC5kYXRhU00oJ3BhcmVudC1hJylbMF0pKSB7XG5cdFx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XHRcdCQoJHVsLnBhcmVudHNVbnRpbCgnW2RhdGEtc21hcnRtZW51cy1pZF0nLCAndWwnKS5nZXQoKS5yZXZlcnNlKCkpLmFkZCgkdWwpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRzZWxmLml0ZW1BY3RpdmF0ZSgkKHRoaXMpLmRhdGFTTSgncGFyZW50LWEnKSk7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8gaGlkZSBhbnkgdmlzaWJsZSBkZWVwZXIgbGV2ZWwgc3ViIG1lbnVzXG5cdFx0XHRcdGlmICghdGhpcy5pc0NvbGxhcHNpYmxlKCkgfHwgaGlkZURlZXBlclN1YnMpIHtcblx0XHRcdFx0XHR0aGlzLm1lbnVIaWRlU3ViTWVudXMoIXRoaXMuYWN0aXZhdGVkSXRlbXNbbGV2ZWwgLSAxXSB8fCB0aGlzLmFjdGl2YXRlZEl0ZW1zW2xldmVsIC0gMV1bMF0gIT0gJGFbMF0gPyBsZXZlbCAtIDEgOiBsZXZlbCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8gc2F2ZSBuZXcgYWN0aXZlIGl0ZW0gZm9yIHRoaXMgbGV2ZWxcblx0XHRcdFx0dGhpcy5hY3RpdmF0ZWRJdGVtc1tsZXZlbCAtIDFdID0gJGE7XG5cdFx0XHRcdGlmICh0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdhY3RpdmF0ZS5zbWFwaScsICRhWzBdKSA9PT0gZmFsc2UpIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8gc2hvdyB0aGUgc3ViIG1lbnUgaWYgdGhpcyBpdGVtIGhhcyBvbmVcblx0XHRcdFx0dmFyICRzdWIgPSAkYS5kYXRhU00oJ3N1YicpO1xuXHRcdFx0XHRpZiAoJHN1YiAmJiAodGhpcy5pc1RvdWNoTW9kZSgpIHx8ICghdGhpcy5vcHRzLnNob3dPbkNsaWNrIHx8IHRoaXMuY2xpY2tBY3RpdmF0ZWQpKSkge1xuXHRcdFx0XHRcdHRoaXMubWVudVNob3coJHN1Yik7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRpdGVtQmx1cjogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHR2YXIgJGEgPSAkKGUuY3VycmVudFRhcmdldCk7XG5cdFx0XHRcdGlmICghdGhpcy5oYW5kbGVJdGVtRXZlbnRzKCRhKSkge1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHR0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdibHVyLnNtYXBpJywgJGFbMF0pO1xuXHRcdFx0fSxcblx0XHRcdGl0ZW1DbGljazogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHR2YXIgJGEgPSAkKGUuY3VycmVudFRhcmdldCk7XG5cdFx0XHRcdGlmICghdGhpcy5oYW5kbGVJdGVtRXZlbnRzKCRhKSkge1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAodGhpcy4kdG91Y2hTY3JvbGxpbmdTdWIgJiYgdGhpcy4kdG91Y2hTY3JvbGxpbmdTdWJbMF0gPT0gJGEuY2xvc2VzdCgndWwnKVswXSkge1xuXHRcdFx0XHRcdHRoaXMuJHRvdWNoU2Nyb2xsaW5nU3ViID0gbnVsbDtcblx0XHRcdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAodGhpcy4kcm9vdC50cmlnZ2VySGFuZGxlcignY2xpY2suc21hcGknLCAkYVswXSkgPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHZhciBzdWJBcnJvd0NsaWNrZWQgPSAkKGUudGFyZ2V0KS5pcygnLnN1Yi1hcnJvdycpLFxuXHRcdFx0XHRcdCRzdWIgPSAkYS5kYXRhU00oJ3N1YicpLFxuXHRcdFx0XHRcdGZpcnN0TGV2ZWxTdWIgPSAkc3ViID8gJHN1Yi5kYXRhU00oJ2xldmVsJykgPT0gMiA6IGZhbHNlLFxuXHRcdFx0XHRcdGNvbGxhcHNpYmxlID0gdGhpcy5pc0NvbGxhcHNpYmxlKCksXG5cdFx0XHRcdFx0YmVoYXZpb3JUb2dnbGUgPSAvdG9nZ2xlJC8udGVzdCh0aGlzLm9wdHMuY29sbGFwc2libGVCZWhhdmlvciksXG5cdFx0XHRcdFx0YmVoYXZpb3JMaW5rID0gL2xpbmskLy50ZXN0KHRoaXMub3B0cy5jb2xsYXBzaWJsZUJlaGF2aW9yKSxcblx0XHRcdFx0XHRiZWhhdmlvckFjY29yZGlvbiA9IC9eYWNjb3JkaW9uLy50ZXN0KHRoaXMub3B0cy5jb2xsYXBzaWJsZUJlaGF2aW9yKTtcblx0XHRcdFx0Ly8gaWYgdGhlIHN1YiBpcyBoaWRkZW4sIHRyeSB0byBzaG93IGl0XG5cdFx0XHRcdGlmICgkc3ViICYmICEkc3ViLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0aWYgKCFiZWhhdmlvckxpbmsgfHwgIWNvbGxhcHNpYmxlIHx8IHN1YkFycm93Q2xpY2tlZCkge1xuXHRcdFx0XHRcdFx0aWYgKHRoaXMub3B0cy5zaG93T25DbGljayAmJiBmaXJzdExldmVsU3ViKSB7XG5cdFx0XHRcdFx0XHRcdHRoaXMuY2xpY2tBY3RpdmF0ZWQgPSB0cnVlO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0Ly8gdHJ5IHRvIGFjdGl2YXRlIHRoZSBpdGVtIGFuZCBzaG93IHRoZSBzdWJcblx0XHRcdFx0XHRcdHRoaXMuaXRlbUFjdGl2YXRlKCRhLCBiZWhhdmlvckFjY29yZGlvbik7XG5cdFx0XHRcdFx0XHQvLyBpZiBcIml0ZW1BY3RpdmF0ZVwiIHNob3dlZCB0aGUgc3ViLCBwcmV2ZW50IHRoZSBjbGljayBzbyB0aGF0IHRoZSBsaW5rIGlzIG5vdCBsb2FkZWRcblx0XHRcdFx0XHRcdC8vIGlmIGl0IGNvdWxkbid0IHNob3cgaXQsIHRoZW4gdGhlIHN1YiBtZW51cyBhcmUgZGlzYWJsZWQgd2l0aCBhbiAhaW1wb3J0YW50IGRlY2xhcmF0aW9uIChlLmcuIHZpYSBtb2JpbGUgc3R5bGVzKSBzbyBsZXQgdGhlIGxpbmsgZ2V0IGxvYWRlZFxuXHRcdFx0XHRcdFx0aWYgKCRzdWIuaXMoJzp2aXNpYmxlJykpIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5mb2N1c0FjdGl2YXRlZCA9IHRydWU7XG5cdFx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdC8vIGlmIHRoZSBzdWIgaXMgdmlzaWJsZSBhbmQgd2UgYXJlIGluIGNvbGxhcHNpYmxlIG1vZGVcblx0XHRcdFx0fSBlbHNlIGlmIChjb2xsYXBzaWJsZSAmJiAoYmVoYXZpb3JUb2dnbGUgfHwgc3ViQXJyb3dDbGlja2VkKSkge1xuXHRcdFx0XHRcdHRoaXMuaXRlbUFjdGl2YXRlKCRhLCBiZWhhdmlvckFjY29yZGlvbik7XG5cdFx0XHRcdFx0dGhpcy5tZW51SGlkZSgkc3ViKTtcblx0XHRcdFx0XHRpZiAoYmVoYXZpb3JUb2dnbGUpIHtcblx0XHRcdFx0XHRcdHRoaXMuZm9jdXNBY3RpdmF0ZWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0aGlzLm9wdHMuc2hvd09uQ2xpY2sgJiYgZmlyc3RMZXZlbFN1YiB8fCAkYS5oYXNDbGFzcygnZGlzYWJsZWQnKSB8fCB0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdzZWxlY3Quc21hcGknLCAkYVswXSkgPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0aXRlbURvd246IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0dmFyICRhID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuXHRcdFx0XHRpZiAoIXRoaXMuaGFuZGxlSXRlbUV2ZW50cygkYSkpIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0JGEuZGF0YVNNKCdtb3VzZWRvd24nLCB0cnVlKTtcblx0XHRcdH0sXG5cdFx0XHRpdGVtRW50ZXI6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0dmFyICRhID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuXHRcdFx0XHRpZiAoIXRoaXMuaGFuZGxlSXRlbUV2ZW50cygkYSkpIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCF0aGlzLmlzVG91Y2hNb2RlKCkpIHtcblx0XHRcdFx0XHRpZiAodGhpcy5zaG93VGltZW91dCkge1xuXHRcdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHRoaXMuc2hvd1RpbWVvdXQpO1xuXHRcdFx0XHRcdFx0dGhpcy5zaG93VGltZW91dCA9IDA7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdFx0XHR0aGlzLnNob3dUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHsgc2VsZi5pdGVtQWN0aXZhdGUoJGEpOyB9LCB0aGlzLm9wdHMuc2hvd09uQ2xpY2sgJiYgJGEuY2xvc2VzdCgndWwnKS5kYXRhU00oJ2xldmVsJykgPT0gMSA/IDEgOiB0aGlzLm9wdHMuc2hvd1RpbWVvdXQpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ21vdXNlZW50ZXIuc21hcGknLCAkYVswXSk7XG5cdFx0XHR9LFxuXHRcdFx0aXRlbUZvY3VzOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdHZhciAkYSA9ICQoZS5jdXJyZW50VGFyZ2V0KTtcblx0XHRcdFx0aWYgKCF0aGlzLmhhbmRsZUl0ZW1FdmVudHMoJGEpKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdC8vIGZpeCAodGhlIG1vdXNlZG93biBjaGVjayk6IGluIHNvbWUgYnJvd3NlcnMgYSB0YXAvY2xpY2sgcHJvZHVjZXMgY29uc2VjdXRpdmUgZm9jdXMgKyBjbGljayBldmVudHMgc28gd2UgZG9uJ3QgbmVlZCB0byBhY3RpdmF0ZSB0aGUgaXRlbSBvbiBmb2N1c1xuXHRcdFx0XHRpZiAodGhpcy5mb2N1c0FjdGl2YXRlZCAmJiAoIXRoaXMuaXNUb3VjaE1vZGUoKSB8fCAhJGEuZGF0YVNNKCdtb3VzZWRvd24nKSkgJiYgKCF0aGlzLmFjdGl2YXRlZEl0ZW1zLmxlbmd0aCB8fCB0aGlzLmFjdGl2YXRlZEl0ZW1zW3RoaXMuYWN0aXZhdGVkSXRlbXMubGVuZ3RoIC0gMV1bMF0gIT0gJGFbMF0pKSB7XG5cdFx0XHRcdFx0dGhpcy5pdGVtQWN0aXZhdGUoJGEsIHRydWUpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ2ZvY3VzLnNtYXBpJywgJGFbMF0pO1xuXHRcdFx0fSxcblx0XHRcdGl0ZW1MZWF2ZTogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHR2YXIgJGEgPSAkKGUuY3VycmVudFRhcmdldCk7XG5cdFx0XHRcdGlmICghdGhpcy5oYW5kbGVJdGVtRXZlbnRzKCRhKSkge1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoIXRoaXMuaXNUb3VjaE1vZGUoKSkge1xuXHRcdFx0XHRcdCRhWzBdLmJsdXIoKTtcblx0XHRcdFx0XHRpZiAodGhpcy5zaG93VGltZW91dCkge1xuXHRcdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHRoaXMuc2hvd1RpbWVvdXQpO1xuXHRcdFx0XHRcdFx0dGhpcy5zaG93VGltZW91dCA9IDA7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRcdCRhLnJlbW92ZURhdGFTTSgnbW91c2Vkb3duJyk7XG5cdFx0XHRcdHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ21vdXNlbGVhdmUuc21hcGknLCAkYVswXSk7XG5cdFx0XHR9LFxuXHRcdFx0bWVudUhpZGU6IGZ1bmN0aW9uKCRzdWIpIHtcblx0XHRcdFx0aWYgKHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ2JlZm9yZWhpZGUuc21hcGknLCAkc3ViWzBdKSA9PT0gZmFsc2UpIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKGNhbkFuaW1hdGUpIHtcblx0XHRcdFx0XHQkc3ViLnN0b3AodHJ1ZSwgdHJ1ZSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCRzdWIuY3NzKCdkaXNwbGF5JykgIT0gJ25vbmUnKSB7XG5cdFx0XHRcdFx0dmFyIGNvbXBsZXRlID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHQvLyB1bnNldCB6LWluZGV4XG5cdFx0XHRcdFx0XHQkc3ViLmNzcygnei1pbmRleCcsICcnKTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdC8vIGlmIHN1YiBpcyBjb2xsYXBzaWJsZSAobW9iaWxlIHZpZXcpXG5cdFx0XHRcdFx0aWYgKHRoaXMuaXNDb2xsYXBzaWJsZSgpKSB7XG5cdFx0XHRcdFx0XHRpZiAoY2FuQW5pbWF0ZSAmJiB0aGlzLm9wdHMuY29sbGFwc2libGVIaWRlRnVuY3Rpb24pIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5vcHRzLmNvbGxhcHNpYmxlSGlkZUZ1bmN0aW9uLmNhbGwodGhpcywgJHN1YiwgY29tcGxldGUpO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0JHN1Yi5oaWRlKHRoaXMub3B0cy5jb2xsYXBzaWJsZUhpZGVEdXJhdGlvbiwgY29tcGxldGUpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRpZiAoY2FuQW5pbWF0ZSAmJiB0aGlzLm9wdHMuaGlkZUZ1bmN0aW9uKSB7XG5cdFx0XHRcdFx0XHRcdHRoaXMub3B0cy5oaWRlRnVuY3Rpb24uY2FsbCh0aGlzLCAkc3ViLCBjb21wbGV0ZSk7XG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHQkc3ViLmhpZGUodGhpcy5vcHRzLmhpZGVEdXJhdGlvbiwgY29tcGxldGUpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQvLyBkZWFjdGl2YXRlIHNjcm9sbGluZyBpZiBpdCBpcyBhY3RpdmF0ZWQgZm9yIHRoaXMgc3ViXG5cdFx0XHRcdFx0aWYgKCRzdWIuZGF0YVNNKCdzY3JvbGwnKSkge1xuXHRcdFx0XHRcdFx0dGhpcy5tZW51U2Nyb2xsU3RvcCgkc3ViKTtcblx0XHRcdFx0XHRcdCRzdWIuY3NzKHsgJ3RvdWNoLWFjdGlvbic6ICcnLCAnLW1zLXRvdWNoLWFjdGlvbic6ICcnLCAnLXdlYmtpdC10cmFuc2Zvcm0nOiAnJywgdHJhbnNmb3JtOiAnJyB9KVxuXHRcdFx0XHRcdFx0XHQub2ZmKCcuc21hcnRtZW51c19zY3JvbGwnKS5yZW1vdmVEYXRhU00oJ3Njcm9sbCcpLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLmhpZGUoKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0Ly8gdW5oaWdobGlnaHQgcGFyZW50IGl0ZW0gKyBhY2Nlc3NpYmlsaXR5XG5cdFx0XHRcdFx0JHN1Yi5kYXRhU00oJ3BhcmVudC1hJykucmVtb3ZlQ2xhc3MoJ2hpZ2hsaWdodGVkJykuYXR0cignYXJpYS1leHBhbmRlZCcsICdmYWxzZScpO1xuXHRcdFx0XHRcdCRzdWIuYXR0cih7XG5cdFx0XHRcdFx0XHQnYXJpYS1leHBhbmRlZCc6ICdmYWxzZScsXG5cdFx0XHRcdFx0XHQnYXJpYS1oaWRkZW4nOiAndHJ1ZSdcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR2YXIgbGV2ZWwgPSAkc3ViLmRhdGFTTSgnbGV2ZWwnKTtcblx0XHRcdFx0XHR0aGlzLmFjdGl2YXRlZEl0ZW1zLnNwbGljZShsZXZlbCAtIDEsIDEpO1xuXHRcdFx0XHRcdHRoaXMudmlzaWJsZVN1Yk1lbnVzLnNwbGljZSgkLmluQXJyYXkoJHN1YiwgdGhpcy52aXNpYmxlU3ViTWVudXMpLCAxKTtcblx0XHRcdFx0XHR0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdoaWRlLnNtYXBpJywgJHN1YlswXSk7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRtZW51SGlkZUFsbDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGlmICh0aGlzLnNob3dUaW1lb3V0KSB7XG5cdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHRoaXMuc2hvd1RpbWVvdXQpO1xuXHRcdFx0XHRcdHRoaXMuc2hvd1RpbWVvdXQgPSAwO1xuXHRcdFx0XHR9XG5cdFx0XHRcdC8vIGhpZGUgYWxsIHN1YnNcblx0XHRcdFx0Ly8gaWYgaXQncyBhIHBvcHVwLCB0aGlzLnZpc2libGVTdWJNZW51c1swXSBpcyB0aGUgcm9vdCBVTFxuXHRcdFx0XHR2YXIgbGV2ZWwgPSB0aGlzLm9wdHMuaXNQb3B1cCA/IDEgOiAwO1xuXHRcdFx0XHRmb3IgKHZhciBpID0gdGhpcy52aXNpYmxlU3ViTWVudXMubGVuZ3RoIC0gMTsgaSA+PSBsZXZlbDsgaS0tKSB7XG5cdFx0XHRcdFx0dGhpcy5tZW51SGlkZSh0aGlzLnZpc2libGVTdWJNZW51c1tpXSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8gaGlkZSByb290IGlmIGl0J3MgcG9wdXBcblx0XHRcdFx0aWYgKHRoaXMub3B0cy5pc1BvcHVwKSB7XG5cdFx0XHRcdFx0aWYgKGNhbkFuaW1hdGUpIHtcblx0XHRcdFx0XHRcdHRoaXMuJHJvb3Quc3RvcCh0cnVlLCB0cnVlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aWYgKHRoaXMuJHJvb3QuaXMoJzp2aXNpYmxlJykpIHtcblx0XHRcdFx0XHRcdGlmIChjYW5BbmltYXRlICYmIHRoaXMub3B0cy5oaWRlRnVuY3Rpb24pIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5vcHRzLmhpZGVGdW5jdGlvbi5jYWxsKHRoaXMsIHRoaXMuJHJvb3QpO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0dGhpcy4kcm9vdC5oaWRlKHRoaXMub3B0cy5oaWRlRHVyYXRpb24pO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XHR0aGlzLmFjdGl2YXRlZEl0ZW1zID0gW107XG5cdFx0XHRcdHRoaXMudmlzaWJsZVN1Yk1lbnVzID0gW107XG5cdFx0XHRcdHRoaXMuY2xpY2tBY3RpdmF0ZWQgPSBmYWxzZTtcblx0XHRcdFx0dGhpcy5mb2N1c0FjdGl2YXRlZCA9IGZhbHNlO1xuXHRcdFx0XHQvLyByZXNldCB6LWluZGV4IGluY3JlbWVudFxuXHRcdFx0XHR0aGlzLnpJbmRleEluYyA9IDA7XG5cdFx0XHRcdHRoaXMuJHJvb3QudHJpZ2dlckhhbmRsZXIoJ2hpZGVBbGwuc21hcGknKTtcblx0XHRcdH0sXG5cdFx0XHRtZW51SGlkZVN1Yk1lbnVzOiBmdW5jdGlvbihsZXZlbCkge1xuXHRcdFx0XHRmb3IgKHZhciBpID0gdGhpcy5hY3RpdmF0ZWRJdGVtcy5sZW5ndGggLSAxOyBpID49IGxldmVsOyBpLS0pIHtcblx0XHRcdFx0XHR2YXIgJHN1YiA9IHRoaXMuYWN0aXZhdGVkSXRlbXNbaV0uZGF0YVNNKCdzdWInKTtcblx0XHRcdFx0XHRpZiAoJHN1Yikge1xuXHRcdFx0XHRcdFx0dGhpcy5tZW51SGlkZSgkc3ViKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRtZW51SW5pdDogZnVuY3Rpb24oJHVsKSB7XG5cdFx0XHRcdGlmICghJHVsLmRhdGFTTSgnaW4tbWVnYScpKSB7XG5cdFx0XHRcdFx0Ly8gbWFyayBVTCdzIGluIG1lZ2EgZHJvcCBkb3ducyAoaWYgYW55KSBzbyB3ZSBjYW4gbmVnbGVjdCB0aGVtXG5cdFx0XHRcdFx0aWYgKCR1bC5oYXNDbGFzcygnbWVnYS1tZW51JykpIHtcblx0XHRcdFx0XHRcdCR1bC5maW5kKCd1bCcpLmRhdGFTTSgnaW4tbWVnYScsIHRydWUpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQvLyBnZXQgbGV2ZWwgKG11Y2ggZmFzdGVyIHRoYW4sIGZvciBleGFtcGxlLCB1c2luZyBwYXJlbnRzVW50aWwpXG5cdFx0XHRcdFx0dmFyIGxldmVsID0gMixcblx0XHRcdFx0XHRcdHBhciA9ICR1bFswXTtcblx0XHRcdFx0XHR3aGlsZSAoKHBhciA9IHBhci5wYXJlbnROb2RlLnBhcmVudE5vZGUpICE9IHRoaXMuJHJvb3RbMF0pIHtcblx0XHRcdFx0XHRcdGxldmVsKys7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdC8vIGNhY2hlIHN0dWZmIGZvciBxdWljayBhY2Nlc3Ncblx0XHRcdFx0XHR2YXIgJGEgPSAkdWwucHJldkFsbCgnYScpLmVxKC0xKTtcblx0XHRcdFx0XHQvLyBpZiB0aGUgbGluayBpcyBuZXN0ZWQgKGUuZy4gaW4gYSBoZWFkaW5nKVxuXHRcdFx0XHRcdGlmICghJGEubGVuZ3RoKSB7XG5cdFx0XHRcdFx0XHQkYSA9ICR1bC5wcmV2QWxsKCkuZmluZCgnYScpLmVxKC0xKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0JGEuYWRkQ2xhc3MoJ2hhcy1zdWJtZW51JykuZGF0YVNNKCdzdWInLCAkdWwpO1xuXHRcdFx0XHRcdCR1bC5kYXRhU00oJ3BhcmVudC1hJywgJGEpXG5cdFx0XHRcdFx0XHQuZGF0YVNNKCdsZXZlbCcsIGxldmVsKVxuXHRcdFx0XHRcdFx0LnBhcmVudCgpLmRhdGFTTSgnc3ViJywgJHVsKTtcblx0XHRcdFx0XHQvLyBhY2Nlc3NpYmlsaXR5XG5cdFx0XHRcdFx0dmFyIGFJZCA9ICRhLmF0dHIoJ2lkJykgfHwgdGhpcy5hY2Nlc3NJZFByZWZpeCArICgrK3RoaXMuaWRJbmMpLFxuXHRcdFx0XHRcdFx0dWxJZCA9ICR1bC5hdHRyKCdpZCcpIHx8IHRoaXMuYWNjZXNzSWRQcmVmaXggKyAoKyt0aGlzLmlkSW5jKTtcblx0XHRcdFx0XHQkYS5hdHRyKHtcblx0XHRcdFx0XHRcdGlkOiBhSWQsXG5cdFx0XHRcdFx0XHQnYXJpYS1oYXNwb3B1cCc6ICd0cnVlJyxcblx0XHRcdFx0XHRcdCdhcmlhLWNvbnRyb2xzJzogdWxJZCxcblx0XHRcdFx0XHRcdCdhcmlhLWV4cGFuZGVkJzogJ2ZhbHNlJ1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdCR1bC5hdHRyKHtcblx0XHRcdFx0XHRcdGlkOiB1bElkLFxuXHRcdFx0XHRcdFx0J3JvbGUnOiAnZ3JvdXAnLFxuXHRcdFx0XHRcdFx0J2FyaWEtaGlkZGVuJzogJ3RydWUnLFxuXHRcdFx0XHRcdFx0J2FyaWEtbGFiZWxsZWRieSc6IGFJZCxcblx0XHRcdFx0XHRcdCdhcmlhLWV4cGFuZGVkJzogJ2ZhbHNlJ1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdC8vIGFkZCBzdWIgaW5kaWNhdG9yIHRvIHBhcmVudCBpdGVtXG5cdFx0XHRcdFx0aWYgKHRoaXMub3B0cy5zdWJJbmRpY2F0b3JzKSB7XG5cdFx0XHRcdFx0XHQkYVt0aGlzLm9wdHMuc3ViSW5kaWNhdG9yc1Bvc10odGhpcy4kc3ViQXJyb3cuY2xvbmUoKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0bWVudVBvc2l0aW9uOiBmdW5jdGlvbigkc3ViKSB7XG5cdFx0XHRcdHZhciAkYSA9ICRzdWIuZGF0YVNNKCdwYXJlbnQtYScpLFxuXHRcdFx0XHRcdCRsaSA9ICRhLmNsb3Nlc3QoJ2xpJyksXG5cdFx0XHRcdFx0JHVsID0gJGxpLnBhcmVudCgpLFxuXHRcdFx0XHRcdGxldmVsID0gJHN1Yi5kYXRhU00oJ2xldmVsJyksXG5cdFx0XHRcdFx0c3ViVyA9IHRoaXMuZ2V0V2lkdGgoJHN1YiksXG5cdFx0XHRcdFx0c3ViSCA9IHRoaXMuZ2V0SGVpZ2h0KCRzdWIpLFxuXHRcdFx0XHRcdGl0ZW1PZmZzZXQgPSAkYS5vZmZzZXQoKSxcblx0XHRcdFx0XHRpdGVtWCA9IGl0ZW1PZmZzZXQubGVmdCxcblx0XHRcdFx0XHRpdGVtWSA9IGl0ZW1PZmZzZXQudG9wLFxuXHRcdFx0XHRcdGl0ZW1XID0gdGhpcy5nZXRXaWR0aCgkYSksXG5cdFx0XHRcdFx0aXRlbUggPSB0aGlzLmdldEhlaWdodCgkYSksXG5cdFx0XHRcdFx0JHdpbiA9ICQod2luZG93KSxcblx0XHRcdFx0XHR3aW5YID0gJHdpbi5zY3JvbGxMZWZ0KCksXG5cdFx0XHRcdFx0d2luWSA9ICR3aW4uc2Nyb2xsVG9wKCksXG5cdFx0XHRcdFx0d2luVyA9IHRoaXMuZ2V0Vmlld3BvcnRXaWR0aCgpLFxuXHRcdFx0XHRcdHdpbkggPSB0aGlzLmdldFZpZXdwb3J0SGVpZ2h0KCksXG5cdFx0XHRcdFx0aG9yaXpvbnRhbFBhcmVudCA9ICR1bC5wYXJlbnQoKS5pcygnW2RhdGEtc20taG9yaXpvbnRhbC1zdWJdJykgfHwgbGV2ZWwgPT0gMiAmJiAhJHVsLmhhc0NsYXNzKCdzbS12ZXJ0aWNhbCcpLFxuXHRcdFx0XHRcdHJpZ2h0VG9MZWZ0ID0gdGhpcy5vcHRzLnJpZ2h0VG9MZWZ0U3ViTWVudXMgJiYgISRsaS5pcygnW2RhdGEtc20tcmV2ZXJzZV0nKSB8fCAhdGhpcy5vcHRzLnJpZ2h0VG9MZWZ0U3ViTWVudXMgJiYgJGxpLmlzKCdbZGF0YS1zbS1yZXZlcnNlXScpLFxuXHRcdFx0XHRcdHN1Yk9mZnNldFggPSBsZXZlbCA9PSAyID8gdGhpcy5vcHRzLm1haW5NZW51U3ViT2Zmc2V0WCA6IHRoaXMub3B0cy5zdWJNZW51c1N1Yk9mZnNldFgsXG5cdFx0XHRcdFx0c3ViT2Zmc2V0WSA9IGxldmVsID09IDIgPyB0aGlzLm9wdHMubWFpbk1lbnVTdWJPZmZzZXRZIDogdGhpcy5vcHRzLnN1Yk1lbnVzU3ViT2Zmc2V0WSxcblx0XHRcdFx0XHR4LCB5O1xuXHRcdFx0XHRpZiAoaG9yaXpvbnRhbFBhcmVudCkge1xuXHRcdFx0XHRcdHggPSByaWdodFRvTGVmdCA/IGl0ZW1XIC0gc3ViVyAtIHN1Yk9mZnNldFggOiBzdWJPZmZzZXRYO1xuXHRcdFx0XHRcdHkgPSB0aGlzLm9wdHMuYm90dG9tVG9Ub3BTdWJNZW51cyA/IC1zdWJIIC0gc3ViT2Zmc2V0WSA6IGl0ZW1IICsgc3ViT2Zmc2V0WTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR4ID0gcmlnaHRUb0xlZnQgPyBzdWJPZmZzZXRYIC0gc3ViVyA6IGl0ZW1XIC0gc3ViT2Zmc2V0WDtcblx0XHRcdFx0XHR5ID0gdGhpcy5vcHRzLmJvdHRvbVRvVG9wU3ViTWVudXMgPyBpdGVtSCAtIHN1Yk9mZnNldFkgLSBzdWJIIDogc3ViT2Zmc2V0WTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAodGhpcy5vcHRzLmtlZXBJblZpZXdwb3J0KSB7XG5cdFx0XHRcdFx0dmFyIGFic1ggPSBpdGVtWCArIHgsXG5cdFx0XHRcdFx0XHRhYnNZID0gaXRlbVkgKyB5O1xuXHRcdFx0XHRcdGlmIChyaWdodFRvTGVmdCAmJiBhYnNYIDwgd2luWCkge1xuXHRcdFx0XHRcdFx0eCA9IGhvcml6b250YWxQYXJlbnQgPyB3aW5YIC0gYWJzWCArIHggOiBpdGVtVyAtIHN1Yk9mZnNldFg7XG5cdFx0XHRcdFx0fSBlbHNlIGlmICghcmlnaHRUb0xlZnQgJiYgYWJzWCArIHN1YlcgPiB3aW5YICsgd2luVykge1xuXHRcdFx0XHRcdFx0eCA9IGhvcml6b250YWxQYXJlbnQgPyB3aW5YICsgd2luVyAtIHN1YlcgLSBhYnNYICsgeCA6IHN1Yk9mZnNldFggLSBzdWJXO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRpZiAoIWhvcml6b250YWxQYXJlbnQpIHtcblx0XHRcdFx0XHRcdGlmIChzdWJIIDwgd2luSCAmJiBhYnNZICsgc3ViSCA+IHdpblkgKyB3aW5IKSB7XG5cdFx0XHRcdFx0XHRcdHkgKz0gd2luWSArIHdpbkggLSBzdWJIIC0gYWJzWTtcblx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoc3ViSCA+PSB3aW5IIHx8IGFic1kgPCB3aW5ZKSB7XG5cdFx0XHRcdFx0XHRcdHkgKz0gd2luWSAtIGFic1k7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdC8vIGRvIHdlIG5lZWQgc2Nyb2xsaW5nP1xuXHRcdFx0XHRcdC8vIDAuNDkgdXNlZCBmb3IgYmV0dGVyIHByZWNpc2lvbiB3aGVuIGRlYWxpbmcgd2l0aCBmbG9hdCB2YWx1ZXNcblx0XHRcdFx0XHRpZiAoaG9yaXpvbnRhbFBhcmVudCAmJiAoYWJzWSArIHN1YkggPiB3aW5ZICsgd2luSCArIDAuNDkgfHwgYWJzWSA8IHdpblkpIHx8ICFob3Jpem9udGFsUGFyZW50ICYmIHN1YkggPiB3aW5IICsgMC40OSkge1xuXHRcdFx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XHRcdFx0aWYgKCEkc3ViLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpKSB7XG5cdFx0XHRcdFx0XHRcdCRzdWIuZGF0YVNNKCdzY3JvbGwtYXJyb3dzJywgJChbJCgnPHNwYW4gY2xhc3M9XCJzY3JvbGwtdXBcIj48c3BhbiBjbGFzcz1cInNjcm9sbC11cC1hcnJvd1wiPjwvc3Bhbj48L3NwYW4+JylbMF0sICQoJzxzcGFuIGNsYXNzPVwic2Nyb2xsLWRvd25cIj48c3BhbiBjbGFzcz1cInNjcm9sbC1kb3duLWFycm93XCI+PC9zcGFuPjwvc3Bhbj4nKVswXV0pXG5cdFx0XHRcdFx0XHRcdFx0Lm9uKHtcblx0XHRcdFx0XHRcdFx0XHRcdG1vdXNlZW50ZXI6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHQkc3ViLmRhdGFTTSgnc2Nyb2xsJykudXAgPSAkKHRoaXMpLmhhc0NsYXNzKCdzY3JvbGwtdXAnKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0c2VsZi5tZW51U2Nyb2xsKCRzdWIpO1xuXHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdG1vdXNlbGVhdmU6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0c2VsZi5tZW51U2Nyb2xsU3RvcCgkc3ViKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0c2VsZi5tZW51U2Nyb2xsT3V0KCRzdWIsIGUpO1xuXHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdCdtb3VzZXdoZWVsIERPTU1vdXNlU2Nyb2xsJzogZnVuY3Rpb24oZSkgeyBlLnByZXZlbnREZWZhdWx0KCk7IH1cblx0XHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHRcdC5pbnNlcnRBZnRlcigkc3ViKVxuXHRcdFx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0Ly8gYmluZCBzY3JvbGwgZXZlbnRzIGFuZCBzYXZlIHNjcm9sbCBkYXRhIGZvciB0aGlzIHN1YlxuXHRcdFx0XHRcdFx0dmFyIGVOUyA9ICcuc21hcnRtZW51c19zY3JvbGwnO1xuXHRcdFx0XHRcdFx0JHN1Yi5kYXRhU00oJ3Njcm9sbCcsIHtcblx0XHRcdFx0XHRcdFx0XHR5OiB0aGlzLmNzc1RyYW5zZm9ybXMzZCA/IDAgOiB5IC0gaXRlbUgsXG5cdFx0XHRcdFx0XHRcdFx0c3RlcDogMSxcblx0XHRcdFx0XHRcdFx0XHQvLyBjYWNoZSBzdHVmZiBmb3IgZmFzdGVyIHJlY2FsY3MgbGF0ZXJcblx0XHRcdFx0XHRcdFx0XHRpdGVtSDogaXRlbUgsXG5cdFx0XHRcdFx0XHRcdFx0c3ViSDogc3ViSCxcblx0XHRcdFx0XHRcdFx0XHRhcnJvd0Rvd25IOiB0aGlzLmdldEhlaWdodCgkc3ViLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLmVxKDEpKVxuXHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHQub24oZ2V0RXZlbnRzTlMoe1xuXHRcdFx0XHRcdFx0XHRcdCdtb3VzZW92ZXInOiBmdW5jdGlvbihlKSB7IHNlbGYubWVudVNjcm9sbE92ZXIoJHN1YiwgZSk7IH0sXG5cdFx0XHRcdFx0XHRcdFx0J21vdXNlb3V0JzogZnVuY3Rpb24oZSkgeyBzZWxmLm1lbnVTY3JvbGxPdXQoJHN1YiwgZSk7IH0sXG5cdFx0XHRcdFx0XHRcdFx0J21vdXNld2hlZWwgRE9NTW91c2VTY3JvbGwnOiBmdW5jdGlvbihlKSB7IHNlbGYubWVudVNjcm9sbE1vdXNld2hlZWwoJHN1YiwgZSk7IH1cblx0XHRcdFx0XHRcdFx0fSwgZU5TKSlcblx0XHRcdFx0XHRcdFx0LmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLmNzcyh7IHRvcDogJ2F1dG8nLCBsZWZ0OiAnMCcsIG1hcmdpbkxlZnQ6IHggKyAocGFyc2VJbnQoJHN1Yi5jc3MoJ2JvcmRlci1sZWZ0LXdpZHRoJykpIHx8IDApLCB3aWR0aDogc3ViVyAtIChwYXJzZUludCgkc3ViLmNzcygnYm9yZGVyLWxlZnQtd2lkdGgnKSkgfHwgMCkgLSAocGFyc2VJbnQoJHN1Yi5jc3MoJ2JvcmRlci1yaWdodC13aWR0aCcpKSB8fCAwKSwgekluZGV4OiAkc3ViLmNzcygnei1pbmRleCcpIH0pXG5cdFx0XHRcdFx0XHRcdFx0LmVxKGhvcml6b250YWxQYXJlbnQgJiYgdGhpcy5vcHRzLmJvdHRvbVRvVG9wU3ViTWVudXMgPyAwIDogMSkuc2hvdygpO1xuXHRcdFx0XHRcdFx0Ly8gd2hlbiBhIG1lbnUgdHJlZSBpcyBmaXhlZCBwb3NpdGlvbmVkIHdlIGFsbG93IHNjcm9sbGluZyB2aWEgdG91Y2ggdG9vXG5cdFx0XHRcdFx0XHQvLyBzaW5jZSB0aGVyZSBpcyBubyBvdGhlciB3YXkgdG8gYWNjZXNzIHN1Y2ggbG9uZyBzdWIgbWVudXMgaWYgbm8gbW91c2UgaXMgcHJlc2VudFxuXHRcdFx0XHRcdFx0aWYgKHRoaXMuaXNGaXhlZCgpKSB7XG5cdFx0XHRcdFx0XHRcdHZhciBldmVudHMgPSB7fTtcblx0XHRcdFx0XHRcdFx0ZXZlbnRzW3RvdWNoRXZlbnRzID8gJ3RvdWNoc3RhcnQgdG91Y2htb3ZlIHRvdWNoZW5kJyA6ICdwb2ludGVyZG93biBwb2ludGVybW92ZSBwb2ludGVydXAgTVNQb2ludGVyRG93biBNU1BvaW50ZXJNb3ZlIE1TUG9pbnRlclVwJ10gPSBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdFx0XHRcdFx0c2VsZi5tZW51U2Nyb2xsVG91Y2goJHN1YiwgZSk7XG5cdFx0XHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0XHRcdCRzdWIuY3NzKHsgJ3RvdWNoLWFjdGlvbic6ICdub25lJywgJy1tcy10b3VjaC1hY3Rpb24nOiAnbm9uZScgfSkub24oZ2V0RXZlbnRzTlMoZXZlbnRzLCBlTlMpKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdFx0JHN1Yi5jc3MoeyB0b3A6ICdhdXRvJywgbGVmdDogJzAnLCBtYXJnaW5MZWZ0OiB4LCBtYXJnaW5Ub3A6IHkgLSBpdGVtSCB9KTtcblx0XHRcdH0sXG5cdFx0XHRtZW51U2Nyb2xsOiBmdW5jdGlvbigkc3ViLCBvbmNlLCBzdGVwKSB7XG5cdFx0XHRcdHZhciBkYXRhID0gJHN1Yi5kYXRhU00oJ3Njcm9sbCcpLFxuXHRcdFx0XHRcdCRhcnJvd3MgPSAkc3ViLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLFxuXHRcdFx0XHRcdGVuZCA9IGRhdGEudXAgPyBkYXRhLnVwRW5kIDogZGF0YS5kb3duRW5kLFxuXHRcdFx0XHRcdGRpZmY7XG5cdFx0XHRcdGlmICghb25jZSAmJiBkYXRhLm1vbWVudHVtKSB7XG5cdFx0XHRcdFx0ZGF0YS5tb21lbnR1bSAqPSAwLjkyO1xuXHRcdFx0XHRcdGRpZmYgPSBkYXRhLm1vbWVudHVtO1xuXHRcdFx0XHRcdGlmIChkaWZmIDwgMC41KSB7XG5cdFx0XHRcdFx0XHR0aGlzLm1lbnVTY3JvbGxTdG9wKCRzdWIpO1xuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRkaWZmID0gc3RlcCB8fCAob25jZSB8fCAhdGhpcy5vcHRzLnNjcm9sbEFjY2VsZXJhdGUgPyB0aGlzLm9wdHMuc2Nyb2xsU3RlcCA6IE1hdGguZmxvb3IoZGF0YS5zdGVwKSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8gaGlkZSBhbnkgdmlzaWJsZSBkZWVwZXIgbGV2ZWwgc3ViIG1lbnVzXG5cdFx0XHRcdHZhciBsZXZlbCA9ICRzdWIuZGF0YVNNKCdsZXZlbCcpO1xuXHRcdFx0XHRpZiAodGhpcy5hY3RpdmF0ZWRJdGVtc1tsZXZlbCAtIDFdICYmIHRoaXMuYWN0aXZhdGVkSXRlbXNbbGV2ZWwgLSAxXS5kYXRhU00oJ3N1YicpICYmIHRoaXMuYWN0aXZhdGVkSXRlbXNbbGV2ZWwgLSAxXS5kYXRhU00oJ3N1YicpLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0dGhpcy5tZW51SGlkZVN1Yk1lbnVzKGxldmVsIC0gMSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZGF0YS55ID0gZGF0YS51cCAmJiBlbmQgPD0gZGF0YS55IHx8ICFkYXRhLnVwICYmIGVuZCA+PSBkYXRhLnkgPyBkYXRhLnkgOiAoTWF0aC5hYnMoZW5kIC0gZGF0YS55KSA+IGRpZmYgPyBkYXRhLnkgKyAoZGF0YS51cCA/IGRpZmYgOiAtZGlmZikgOiBlbmQpO1xuXHRcdFx0XHQkc3ViLmNzcyh0aGlzLmNzc1RyYW5zZm9ybXMzZCA/IHsgJy13ZWJraXQtdHJhbnNmb3JtJzogJ3RyYW5zbGF0ZTNkKDAsICcgKyBkYXRhLnkgKyAncHgsIDApJywgdHJhbnNmb3JtOiAndHJhbnNsYXRlM2QoMCwgJyArIGRhdGEueSArICdweCwgMCknIH0gOiB7IG1hcmdpblRvcDogZGF0YS55IH0pO1xuXHRcdFx0XHQvLyBzaG93IG9wcG9zaXRlIGFycm93IGlmIGFwcHJvcHJpYXRlXG5cdFx0XHRcdGlmIChtb3VzZSAmJiAoZGF0YS51cCAmJiBkYXRhLnkgPiBkYXRhLmRvd25FbmQgfHwgIWRhdGEudXAgJiYgZGF0YS55IDwgZGF0YS51cEVuZCkpIHtcblx0XHRcdFx0XHQkYXJyb3dzLmVxKGRhdGEudXAgPyAxIDogMCkuc2hvdygpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdC8vIGlmIHdlJ3ZlIHJlYWNoZWQgdGhlIGVuZFxuXHRcdFx0XHRpZiAoZGF0YS55ID09IGVuZCkge1xuXHRcdFx0XHRcdGlmIChtb3VzZSkge1xuXHRcdFx0XHRcdFx0JGFycm93cy5lcShkYXRhLnVwID8gMCA6IDEpLmhpZGUoKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dGhpcy5tZW51U2Nyb2xsU3RvcCgkc3ViKTtcblx0XHRcdFx0fSBlbHNlIGlmICghb25jZSkge1xuXHRcdFx0XHRcdGlmICh0aGlzLm9wdHMuc2Nyb2xsQWNjZWxlcmF0ZSAmJiBkYXRhLnN0ZXAgPCB0aGlzLm9wdHMuc2Nyb2xsU3RlcCkge1xuXHRcdFx0XHRcdFx0ZGF0YS5zdGVwICs9IDAuMjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XHRcdHRoaXMuc2Nyb2xsVGltZW91dCA9IHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbigpIHsgc2VsZi5tZW51U2Nyb2xsKCRzdWIpOyB9KTtcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdG1lbnVTY3JvbGxNb3VzZXdoZWVsOiBmdW5jdGlvbigkc3ViLCBlKSB7XG5cdFx0XHRcdGlmICh0aGlzLmdldENsb3Nlc3RNZW51KGUudGFyZ2V0KSA9PSAkc3ViWzBdKSB7XG5cdFx0XHRcdFx0ZSA9IGUub3JpZ2luYWxFdmVudDtcblx0XHRcdFx0XHR2YXIgdXAgPSAoZS53aGVlbERlbHRhIHx8IC1lLmRldGFpbCkgPiAwO1xuXHRcdFx0XHRcdGlmICgkc3ViLmRhdGFTTSgnc2Nyb2xsLWFycm93cycpLmVxKHVwID8gMCA6IDEpLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0XHQkc3ViLmRhdGFTTSgnc2Nyb2xsJykudXAgPSB1cDtcblx0XHRcdFx0XHRcdHRoaXMubWVudVNjcm9sbCgkc3ViLCB0cnVlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0fSxcblx0XHRcdG1lbnVTY3JvbGxPdXQ6IGZ1bmN0aW9uKCRzdWIsIGUpIHtcblx0XHRcdFx0aWYgKG1vdXNlKSB7XG5cdFx0XHRcdFx0aWYgKCEvXnNjcm9sbC0odXB8ZG93bikvLnRlc3QoKGUucmVsYXRlZFRhcmdldCB8fCAnJykuY2xhc3NOYW1lKSAmJiAoJHN1YlswXSAhPSBlLnJlbGF0ZWRUYXJnZXQgJiYgISQuY29udGFpbnMoJHN1YlswXSwgZS5yZWxhdGVkVGFyZ2V0KSB8fCB0aGlzLmdldENsb3Nlc3RNZW51KGUucmVsYXRlZFRhcmdldCkgIT0gJHN1YlswXSkpIHtcblx0XHRcdFx0XHRcdCRzdWIuZGF0YVNNKCdzY3JvbGwtYXJyb3dzJykuY3NzKCd2aXNpYmlsaXR5JywgJ2hpZGRlbicpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdG1lbnVTY3JvbGxPdmVyOiBmdW5jdGlvbigkc3ViLCBlKSB7XG5cdFx0XHRcdGlmIChtb3VzZSkge1xuXHRcdFx0XHRcdGlmICghL15zY3JvbGwtKHVwfGRvd24pLy50ZXN0KGUudGFyZ2V0LmNsYXNzTmFtZSkgJiYgdGhpcy5nZXRDbG9zZXN0TWVudShlLnRhcmdldCkgPT0gJHN1YlswXSkge1xuXHRcdFx0XHRcdFx0dGhpcy5tZW51U2Nyb2xsUmVmcmVzaERhdGEoJHN1Yik7XG5cdFx0XHRcdFx0XHR2YXIgZGF0YSA9ICRzdWIuZGF0YVNNKCdzY3JvbGwnKSxcblx0XHRcdFx0XHRcdFx0dXBFbmQgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgLSAkc3ViLmRhdGFTTSgncGFyZW50LWEnKS5vZmZzZXQoKS50b3AgLSBkYXRhLml0ZW1IO1xuXHRcdFx0XHRcdFx0JHN1Yi5kYXRhU00oJ3Njcm9sbC1hcnJvd3MnKS5lcSgwKS5jc3MoJ21hcmdpbi10b3AnLCB1cEVuZCkuZW5kKClcblx0XHRcdFx0XHRcdFx0LmVxKDEpLmNzcygnbWFyZ2luLXRvcCcsIHVwRW5kICsgdGhpcy5nZXRWaWV3cG9ydEhlaWdodCgpIC0gZGF0YS5hcnJvd0Rvd25IKS5lbmQoKVxuXHRcdFx0XHRcdFx0XHQuY3NzKCd2aXNpYmlsaXR5JywgJ3Zpc2libGUnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRtZW51U2Nyb2xsUmVmcmVzaERhdGE6IGZ1bmN0aW9uKCRzdWIpIHtcblx0XHRcdFx0dmFyIGRhdGEgPSAkc3ViLmRhdGFTTSgnc2Nyb2xsJyksXG5cdFx0XHRcdFx0dXBFbmQgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgLSAkc3ViLmRhdGFTTSgncGFyZW50LWEnKS5vZmZzZXQoKS50b3AgLSBkYXRhLml0ZW1IO1xuXHRcdFx0XHRpZiAodGhpcy5jc3NUcmFuc2Zvcm1zM2QpIHtcblx0XHRcdFx0XHR1cEVuZCA9IC0ocGFyc2VGbG9hdCgkc3ViLmNzcygnbWFyZ2luLXRvcCcpKSAtIHVwRW5kKTtcblx0XHRcdFx0fVxuXHRcdFx0XHQkLmV4dGVuZChkYXRhLCB7XG5cdFx0XHRcdFx0dXBFbmQ6IHVwRW5kLFxuXHRcdFx0XHRcdGRvd25FbmQ6IHVwRW5kICsgdGhpcy5nZXRWaWV3cG9ydEhlaWdodCgpIC0gZGF0YS5zdWJIXG5cdFx0XHRcdH0pO1xuXHRcdFx0fSxcblx0XHRcdG1lbnVTY3JvbGxTdG9wOiBmdW5jdGlvbigkc3ViKSB7XG5cdFx0XHRcdGlmICh0aGlzLnNjcm9sbFRpbWVvdXQpIHtcblx0XHRcdFx0XHRjYW5jZWxBbmltYXRpb25GcmFtZSh0aGlzLnNjcm9sbFRpbWVvdXQpO1xuXHRcdFx0XHRcdHRoaXMuc2Nyb2xsVGltZW91dCA9IDA7XG5cdFx0XHRcdFx0JHN1Yi5kYXRhU00oJ3Njcm9sbCcpLnN0ZXAgPSAxO1xuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0bWVudVNjcm9sbFRvdWNoOiBmdW5jdGlvbigkc3ViLCBlKSB7XG5cdFx0XHRcdGUgPSBlLm9yaWdpbmFsRXZlbnQ7XG5cdFx0XHRcdGlmIChpc1RvdWNoRXZlbnQoZSkpIHtcblx0XHRcdFx0XHR2YXIgdG91Y2hQb2ludCA9IHRoaXMuZ2V0VG91Y2hQb2ludChlKTtcblx0XHRcdFx0XHQvLyBuZWdsZWN0IGV2ZW50IGlmIHdlIHRvdWNoZWQgYSB2aXNpYmxlIGRlZXBlciBsZXZlbCBzdWIgbWVudVxuXHRcdFx0XHRcdGlmICh0aGlzLmdldENsb3Nlc3RNZW51KHRvdWNoUG9pbnQudGFyZ2V0KSA9PSAkc3ViWzBdKSB7XG5cdFx0XHRcdFx0XHR2YXIgZGF0YSA9ICRzdWIuZGF0YVNNKCdzY3JvbGwnKTtcblx0XHRcdFx0XHRcdGlmICgvKHN0YXJ0fGRvd24pJC9pLnRlc3QoZS50eXBlKSkge1xuXHRcdFx0XHRcdFx0XHRpZiAodGhpcy5tZW51U2Nyb2xsU3RvcCgkc3ViKSkge1xuXHRcdFx0XHRcdFx0XHRcdC8vIGlmIHdlIHdlcmUgc2Nyb2xsaW5nLCBqdXN0IHN0b3AgYW5kIGRvbid0IGFjdGl2YXRlIGFueSBsaW5rIG9uIHRoZSBmaXJzdCB0b3VjaFxuXHRcdFx0XHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHRcdFx0XHR0aGlzLiR0b3VjaFNjcm9sbGluZ1N1YiA9ICRzdWI7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy4kdG91Y2hTY3JvbGxpbmdTdWIgPSBudWxsO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdC8vIHVwZGF0ZSBzY3JvbGwgZGF0YSBzaW5jZSB0aGUgdXNlciBtaWdodCBoYXZlIHpvb21lZCwgZXRjLlxuXHRcdFx0XHRcdFx0XHR0aGlzLm1lbnVTY3JvbGxSZWZyZXNoRGF0YSgkc3ViKTtcblx0XHRcdFx0XHRcdFx0Ly8gZXh0ZW5kIGl0IHdpdGggdGhlIHRvdWNoIHByb3BlcnRpZXNcblx0XHRcdFx0XHRcdFx0JC5leHRlbmQoZGF0YSwge1xuXHRcdFx0XHRcdFx0XHRcdHRvdWNoU3RhcnRZOiB0b3VjaFBvaW50LnBhZ2VZLFxuXHRcdFx0XHRcdFx0XHRcdHRvdWNoU3RhcnRUaW1lOiBlLnRpbWVTdGFtcFxuXHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoL21vdmUkL2kudGVzdChlLnR5cGUpKSB7XG5cdFx0XHRcdFx0XHRcdHZhciBwcmV2WSA9IGRhdGEudG91Y2hZICE9PSB1bmRlZmluZWQgPyBkYXRhLnRvdWNoWSA6IGRhdGEudG91Y2hTdGFydFk7XG5cdFx0XHRcdFx0XHRcdGlmIChwcmV2WSAhPT0gdW5kZWZpbmVkICYmIHByZXZZICE9IHRvdWNoUG9pbnQucGFnZVkpIHtcblx0XHRcdFx0XHRcdFx0XHR0aGlzLiR0b3VjaFNjcm9sbGluZ1N1YiA9ICRzdWI7XG5cdFx0XHRcdFx0XHRcdFx0dmFyIHVwID0gcHJldlkgPCB0b3VjaFBvaW50LnBhZ2VZO1xuXHRcdFx0XHRcdFx0XHRcdC8vIGNoYW5nZWQgZGlyZWN0aW9uPyByZXNldC4uLlxuXHRcdFx0XHRcdFx0XHRcdGlmIChkYXRhLnVwICE9PSB1bmRlZmluZWQgJiYgZGF0YS51cCAhPSB1cCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0JC5leHRlbmQoZGF0YSwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR0b3VjaFN0YXJ0WTogdG91Y2hQb2ludC5wYWdlWSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0dG91Y2hTdGFydFRpbWU6IGUudGltZVN0YW1wXG5cdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFx0JC5leHRlbmQoZGF0YSwge1xuXHRcdFx0XHRcdFx0XHRcdFx0dXA6IHVwLFxuXHRcdFx0XHRcdFx0XHRcdFx0dG91Y2hZOiB0b3VjaFBvaW50LnBhZ2VZXG5cdFx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5tZW51U2Nyb2xsKCRzdWIsIHRydWUsIE1hdGguYWJzKHRvdWNoUG9pbnQucGFnZVkgLSBwcmV2WSkpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7IC8vIHRvdWNoZW5kL3BvaW50ZXJ1cFxuXHRcdFx0XHRcdFx0XHRpZiAoZGF0YS50b3VjaFkgIT09IHVuZGVmaW5lZCkge1xuXHRcdFx0XHRcdFx0XHRcdGlmIChkYXRhLm1vbWVudHVtID0gTWF0aC5wb3coTWF0aC5hYnModG91Y2hQb2ludC5wYWdlWSAtIGRhdGEudG91Y2hTdGFydFkpIC8gKGUudGltZVN0YW1wIC0gZGF0YS50b3VjaFN0YXJ0VGltZSksIDIpICogMTUpIHtcblx0XHRcdFx0XHRcdFx0XHRcdHRoaXMubWVudVNjcm9sbFN0b3AoJHN1Yik7XG5cdFx0XHRcdFx0XHRcdFx0XHR0aGlzLm1lbnVTY3JvbGwoJHN1Yik7XG5cdFx0XHRcdFx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRcdGRlbGV0ZSBkYXRhLnRvdWNoWTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdG1lbnVTaG93OiBmdW5jdGlvbigkc3ViKSB7XG5cdFx0XHRcdGlmICghJHN1Yi5kYXRhU00oJ2JlZm9yZWZpcnN0c2hvd2ZpcmVkJykpIHtcblx0XHRcdFx0XHQkc3ViLmRhdGFTTSgnYmVmb3JlZmlyc3RzaG93ZmlyZWQnLCB0cnVlKTtcblx0XHRcdFx0XHRpZiAodGhpcy4kcm9vdC50cmlnZ2VySGFuZGxlcignYmVmb3JlZmlyc3RzaG93LnNtYXBpJywgJHN1YlswXSkgPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdiZWZvcmVzaG93LnNtYXBpJywgJHN1YlswXSkgPT09IGZhbHNlKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdCRzdWIuZGF0YVNNKCdzaG93bi1iZWZvcmUnLCB0cnVlKTtcblx0XHRcdFx0aWYgKGNhbkFuaW1hdGUpIHtcblx0XHRcdFx0XHQkc3ViLnN0b3AodHJ1ZSwgdHJ1ZSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCEkc3ViLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0Ly8gaGlnaGxpZ2h0IHBhcmVudCBpdGVtXG5cdFx0XHRcdFx0dmFyICRhID0gJHN1Yi5kYXRhU00oJ3BhcmVudC1hJyksXG5cdFx0XHRcdFx0XHRjb2xsYXBzaWJsZSA9IHRoaXMuaXNDb2xsYXBzaWJsZSgpO1xuXHRcdFx0XHRcdGlmICh0aGlzLm9wdHMua2VlcEhpZ2hsaWdodGVkIHx8IGNvbGxhcHNpYmxlKSB7XG5cdFx0XHRcdFx0XHQkYS5hZGRDbGFzcygnaGlnaGxpZ2h0ZWQnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aWYgKGNvbGxhcHNpYmxlKSB7XG5cdFx0XHRcdFx0XHQkc3ViLnJlbW92ZUNsYXNzKCdzbS1ub3dyYXAnKS5jc3MoeyB6SW5kZXg6ICcnLCB3aWR0aDogJ2F1dG8nLCBtaW5XaWR0aDogJycsIG1heFdpZHRoOiAnJywgdG9wOiAnJywgbGVmdDogJycsIG1hcmdpbkxlZnQ6ICcnLCBtYXJnaW5Ub3A6ICcnIH0pO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHQvLyBzZXQgei1pbmRleFxuXHRcdFx0XHRcdFx0JHN1Yi5jc3MoJ3otaW5kZXgnLCB0aGlzLnpJbmRleEluYyA9ICh0aGlzLnpJbmRleEluYyB8fCB0aGlzLmdldFN0YXJ0WkluZGV4KCkpICsgMSk7XG5cdFx0XHRcdFx0XHQvLyBtaW4vbWF4LXdpZHRoIGZpeCAtIG5vIHdheSB0byByZWx5IHB1cmVseSBvbiBDU1MgYXMgYWxsIFVMJ3MgYXJlIG5lc3RlZFxuXHRcdFx0XHRcdFx0aWYgKHRoaXMub3B0cy5zdWJNZW51c01pbldpZHRoIHx8IHRoaXMub3B0cy5zdWJNZW51c01heFdpZHRoKSB7XG5cdFx0XHRcdFx0XHRcdCRzdWIuY3NzKHsgd2lkdGg6ICdhdXRvJywgbWluV2lkdGg6ICcnLCBtYXhXaWR0aDogJycgfSkuYWRkQ2xhc3MoJ3NtLW5vd3JhcCcpO1xuXHRcdFx0XHRcdFx0XHRpZiAodGhpcy5vcHRzLnN1Yk1lbnVzTWluV2lkdGgpIHtcblx0XHRcdFx0XHRcdFx0IFx0JHN1Yi5jc3MoJ21pbi13aWR0aCcsIHRoaXMub3B0cy5zdWJNZW51c01pbldpZHRoKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRpZiAodGhpcy5vcHRzLnN1Yk1lbnVzTWF4V2lkdGgpIHtcblx0XHRcdFx0XHRcdFx0IFx0dmFyIG5vTWF4V2lkdGggPSB0aGlzLmdldFdpZHRoKCRzdWIpO1xuXHRcdFx0XHRcdFx0XHQgXHQkc3ViLmNzcygnbWF4LXdpZHRoJywgdGhpcy5vcHRzLnN1Yk1lbnVzTWF4V2lkdGgpO1xuXHRcdFx0XHRcdFx0XHRcdGlmIChub01heFdpZHRoID4gdGhpcy5nZXRXaWR0aCgkc3ViKSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0JHN1Yi5yZW1vdmVDbGFzcygnc20tbm93cmFwJykuY3NzKCd3aWR0aCcsIHRoaXMub3B0cy5zdWJNZW51c01heFdpZHRoKTtcblx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdHRoaXMubWVudVBvc2l0aW9uKCRzdWIpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR2YXIgY29tcGxldGUgPSBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdC8vIGZpeDogXCJvdmVyZmxvdzogaGlkZGVuO1wiIGlzIG5vdCByZXNldCBvbiBhbmltYXRpb24gY29tcGxldGUgaW4galF1ZXJ5IDwgMS45LjAgaW4gQ2hyb21lIHdoZW4gZ2xvYmFsIFwiYm94LXNpemluZzogYm9yZGVyLWJveDtcIiBpcyB1c2VkXG5cdFx0XHRcdFx0XHQkc3ViLmNzcygnb3ZlcmZsb3cnLCAnJyk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0XHQvLyBpZiBzdWIgaXMgY29sbGFwc2libGUgKG1vYmlsZSB2aWV3KVxuXHRcdFx0XHRcdGlmIChjb2xsYXBzaWJsZSkge1xuXHRcdFx0XHRcdFx0aWYgKGNhbkFuaW1hdGUgJiYgdGhpcy5vcHRzLmNvbGxhcHNpYmxlU2hvd0Z1bmN0aW9uKSB7XG5cdFx0XHRcdFx0XHRcdHRoaXMub3B0cy5jb2xsYXBzaWJsZVNob3dGdW5jdGlvbi5jYWxsKHRoaXMsICRzdWIsIGNvbXBsZXRlKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdCRzdWIuc2hvdyh0aGlzLm9wdHMuY29sbGFwc2libGVTaG93RHVyYXRpb24sIGNvbXBsZXRlKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0aWYgKGNhbkFuaW1hdGUgJiYgdGhpcy5vcHRzLnNob3dGdW5jdGlvbikge1xuXHRcdFx0XHRcdFx0XHR0aGlzLm9wdHMuc2hvd0Z1bmN0aW9uLmNhbGwodGhpcywgJHN1YiwgY29tcGxldGUpO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0JHN1Yi5zaG93KHRoaXMub3B0cy5zaG93RHVyYXRpb24sIGNvbXBsZXRlKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0Ly8gYWNjZXNzaWJpbGl0eVxuXHRcdFx0XHRcdCRhLmF0dHIoJ2FyaWEtZXhwYW5kZWQnLCAndHJ1ZScpO1xuXHRcdFx0XHRcdCRzdWIuYXR0cih7XG5cdFx0XHRcdFx0XHQnYXJpYS1leHBhbmRlZCc6ICd0cnVlJyxcblx0XHRcdFx0XHRcdCdhcmlhLWhpZGRlbic6ICdmYWxzZSdcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHQvLyBzdG9yZSBzdWIgbWVudSBpbiB2aXNpYmxlIGFycmF5XG5cdFx0XHRcdFx0dGhpcy52aXNpYmxlU3ViTWVudXMucHVzaCgkc3ViKTtcblx0XHRcdFx0XHR0aGlzLiRyb290LnRyaWdnZXJIYW5kbGVyKCdzaG93LnNtYXBpJywgJHN1YlswXSk7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRwb3B1cEhpZGU6IGZ1bmN0aW9uKG5vSGlkZVRpbWVvdXQpIHtcblx0XHRcdFx0aWYgKHRoaXMuaGlkZVRpbWVvdXQpIHtcblx0XHRcdFx0XHRjbGVhclRpbWVvdXQodGhpcy5oaWRlVGltZW91dCk7XG5cdFx0XHRcdFx0dGhpcy5oaWRlVGltZW91dCA9IDA7XG5cdFx0XHRcdH1cblx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XHR0aGlzLmhpZGVUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRzZWxmLm1lbnVIaWRlQWxsKCk7XG5cdFx0XHRcdH0sIG5vSGlkZVRpbWVvdXQgPyAxIDogdGhpcy5vcHRzLmhpZGVUaW1lb3V0KTtcblx0XHRcdH0sXG5cdFx0XHRwb3B1cFNob3c6IGZ1bmN0aW9uKGxlZnQsIHRvcCkge1xuXHRcdFx0XHRpZiAoIXRoaXMub3B0cy5pc1BvcHVwKSB7XG5cdFx0XHRcdFx0YWxlcnQoJ1NtYXJ0TWVudXMgalF1ZXJ5IEVycm9yOlxcblxcbklmIHlvdSB3YW50IHRvIHNob3cgdGhpcyBtZW51IHZpYSB0aGUgXCJwb3B1cFNob3dcIiBtZXRob2QsIHNldCB0aGUgaXNQb3B1cDp0cnVlIG9wdGlvbi4nKTtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKHRoaXMuaGlkZVRpbWVvdXQpIHtcblx0XHRcdFx0XHRjbGVhclRpbWVvdXQodGhpcy5oaWRlVGltZW91dCk7XG5cdFx0XHRcdFx0dGhpcy5oaWRlVGltZW91dCA9IDA7XG5cdFx0XHRcdH1cblx0XHRcdFx0dGhpcy4kcm9vdC5kYXRhU00oJ3Nob3duLWJlZm9yZScsIHRydWUpO1xuXHRcdFx0XHRpZiAoY2FuQW5pbWF0ZSkge1xuXHRcdFx0XHRcdHRoaXMuJHJvb3Quc3RvcCh0cnVlLCB0cnVlKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoIXRoaXMuJHJvb3QuaXMoJzp2aXNpYmxlJykpIHtcblx0XHRcdFx0XHR0aGlzLiRyb290LmNzcyh7IGxlZnQ6IGxlZnQsIHRvcDogdG9wIH0pO1xuXHRcdFx0XHRcdC8vIHNob3cgbWVudVxuXHRcdFx0XHRcdHZhciBzZWxmID0gdGhpcyxcblx0XHRcdFx0XHRcdGNvbXBsZXRlID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRcdHNlbGYuJHJvb3QuY3NzKCdvdmVyZmxvdycsICcnKTtcblx0XHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0aWYgKGNhbkFuaW1hdGUgJiYgdGhpcy5vcHRzLnNob3dGdW5jdGlvbikge1xuXHRcdFx0XHRcdFx0dGhpcy5vcHRzLnNob3dGdW5jdGlvbi5jYWxsKHRoaXMsIHRoaXMuJHJvb3QsIGNvbXBsZXRlKTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy4kcm9vdC5zaG93KHRoaXMub3B0cy5zaG93RHVyYXRpb24sIGNvbXBsZXRlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dGhpcy52aXNpYmxlU3ViTWVudXNbMF0gPSB0aGlzLiRyb290O1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0cmVmcmVzaDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHRoaXMuZGVzdHJveSh0cnVlKTtcblx0XHRcdFx0dGhpcy5pbml0KHRydWUpO1xuXHRcdFx0fSxcblx0XHRcdHJvb3RLZXlEb3duOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdGlmICghdGhpcy5oYW5kbGVFdmVudHMoKSkge1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXHRcdFx0XHRzd2l0Y2ggKGUua2V5Q29kZSkge1xuXHRcdFx0XHRcdGNhc2UgMjc6IC8vIHJlc2V0IG9uIEVzY1xuXHRcdFx0XHRcdFx0dmFyICRhY3RpdmVUb3BJdGVtID0gdGhpcy5hY3RpdmF0ZWRJdGVtc1swXTtcblx0XHRcdFx0XHRcdGlmICgkYWN0aXZlVG9wSXRlbSkge1xuXHRcdFx0XHRcdFx0XHR0aGlzLm1lbnVIaWRlQWxsKCk7XG5cdFx0XHRcdFx0XHRcdCRhY3RpdmVUb3BJdGVtWzBdLmZvY3VzKCk7XG5cdFx0XHRcdFx0XHRcdHZhciAkc3ViID0gJGFjdGl2ZVRvcEl0ZW0uZGF0YVNNKCdzdWInKTtcblx0XHRcdFx0XHRcdFx0aWYgKCRzdWIpIHtcblx0XHRcdFx0XHRcdFx0XHR0aGlzLm1lbnVIaWRlKCRzdWIpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRjYXNlIDMyOiAvLyBhY3RpdmF0ZSBpdGVtJ3Mgc3ViIG9uIFNwYWNlXG5cdFx0XHRcdFx0XHR2YXIgJHRhcmdldCA9ICQoZS50YXJnZXQpO1xuXHRcdFx0XHRcdFx0aWYgKCR0YXJnZXQuaXMoJ2EnKSAmJiB0aGlzLmhhbmRsZUl0ZW1FdmVudHMoJHRhcmdldCkpIHtcblx0XHRcdFx0XHRcdFx0dmFyICRzdWIgPSAkdGFyZ2V0LmRhdGFTTSgnc3ViJyk7XG5cdFx0XHRcdFx0XHRcdGlmICgkc3ViICYmICEkc3ViLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5pdGVtQ2xpY2soeyBjdXJyZW50VGFyZ2V0OiBlLnRhcmdldCB9KTtcblx0XHRcdFx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0cm9vdE91dDogZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRpZiAoIXRoaXMuaGFuZGxlRXZlbnRzKCkgfHwgdGhpcy5pc1RvdWNoTW9kZSgpIHx8IGUudGFyZ2V0ID09IHRoaXMuJHJvb3RbMF0pIHtcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKHRoaXMuaGlkZVRpbWVvdXQpIHtcblx0XHRcdFx0XHRjbGVhclRpbWVvdXQodGhpcy5oaWRlVGltZW91dCk7XG5cdFx0XHRcdFx0dGhpcy5oaWRlVGltZW91dCA9IDA7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKCF0aGlzLm9wdHMuc2hvd09uQ2xpY2sgfHwgIXRoaXMub3B0cy5oaWRlT25DbGljaykge1xuXHRcdFx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdFx0XHR0aGlzLmhpZGVUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHsgc2VsZi5tZW51SGlkZUFsbCgpOyB9LCB0aGlzLm9wdHMuaGlkZVRpbWVvdXQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0cm9vdE92ZXI6IGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0aWYgKCF0aGlzLmhhbmRsZUV2ZW50cygpIHx8IHRoaXMuaXNUb3VjaE1vZGUoKSB8fCBlLnRhcmdldCA9PSB0aGlzLiRyb290WzBdKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0aGlzLmhpZGVUaW1lb3V0KSB7XG5cdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHRoaXMuaGlkZVRpbWVvdXQpO1xuXHRcdFx0XHRcdHRoaXMuaGlkZVRpbWVvdXQgPSAwO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0d2luUmVzaXplOiBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdGlmICghdGhpcy5oYW5kbGVFdmVudHMoKSkge1xuXHRcdFx0XHRcdC8vIHdlIHN0aWxsIG5lZWQgdG8gcmVzaXplIHRoZSBkaXNhYmxlIG92ZXJsYXkgaWYgaXQncyB2aXNpYmxlXG5cdFx0XHRcdFx0aWYgKHRoaXMuJGRpc2FibGVPdmVybGF5KSB7XG5cdFx0XHRcdFx0XHR2YXIgcG9zID0gdGhpcy4kcm9vdC5vZmZzZXQoKTtcblx0IFx0XHRcdFx0XHR0aGlzLiRkaXNhYmxlT3ZlcmxheS5jc3Moe1xuXHRcdFx0XHRcdFx0XHR0b3A6IHBvcy50b3AsXG5cdFx0XHRcdFx0XHRcdGxlZnQ6IHBvcy5sZWZ0LFxuXHRcdFx0XHRcdFx0XHR3aWR0aDogdGhpcy4kcm9vdC5vdXRlcldpZHRoKCksXG5cdFx0XHRcdFx0XHRcdGhlaWdodDogdGhpcy4kcm9vdC5vdXRlckhlaWdodCgpXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cdFx0XHRcdC8vIGhpZGUgc3ViIG1lbnVzIG9uIHJlc2l6ZSAtIG9uIG1vYmlsZSBkbyBpdCBvbmx5IG9uIG9yaWVudGF0aW9uIGNoYW5nZVxuXHRcdFx0XHRpZiAoISgnb25vcmllbnRhdGlvbmNoYW5nZScgaW4gd2luZG93KSB8fCBlLnR5cGUgPT0gJ29yaWVudGF0aW9uY2hhbmdlJykge1xuXHRcdFx0XHRcdHZhciBjb2xsYXBzaWJsZSA9IHRoaXMuaXNDb2xsYXBzaWJsZSgpO1xuXHRcdFx0XHRcdC8vIGlmIGl0IHdhcyBjb2xsYXBzaWJsZSBiZWZvcmUgcmVzaXplIGFuZCBzdGlsbCBpcywgZG9uJ3QgZG8gaXRcblx0XHRcdFx0XHRpZiAoISh0aGlzLndhc0NvbGxhcHNpYmxlICYmIGNvbGxhcHNpYmxlKSkgeyBcblx0XHRcdFx0XHRcdGlmICh0aGlzLmFjdGl2YXRlZEl0ZW1zLmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0XHR0aGlzLmFjdGl2YXRlZEl0ZW1zW3RoaXMuYWN0aXZhdGVkSXRlbXMubGVuZ3RoIC0gMV1bMF0uYmx1cigpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0dGhpcy5tZW51SGlkZUFsbCgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR0aGlzLndhc0NvbGxhcHNpYmxlID0gY29sbGFwc2libGU7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdH0pO1xuXG5cdCQuZm4uZGF0YVNNID0gZnVuY3Rpb24oa2V5LCB2YWwpIHtcblx0XHRpZiAodmFsKSB7XG5cdFx0XHRyZXR1cm4gdGhpcy5kYXRhKGtleSArICdfc21hcnRtZW51cycsIHZhbCk7XG5cdFx0fVxuXHRcdHJldHVybiB0aGlzLmRhdGEoa2V5ICsgJ19zbWFydG1lbnVzJyk7XG5cdH07XG5cblx0JC5mbi5yZW1vdmVEYXRhU00gPSBmdW5jdGlvbihrZXkpIHtcblx0XHRyZXR1cm4gdGhpcy5yZW1vdmVEYXRhKGtleSArICdfc21hcnRtZW51cycpO1xuXHR9O1xuXG5cdCQuZm4uc21hcnRtZW51cyA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcblx0XHRpZiAodHlwZW9mIG9wdGlvbnMgPT0gJ3N0cmluZycpIHtcblx0XHRcdHZhciBhcmdzID0gYXJndW1lbnRzLFxuXHRcdFx0XHRtZXRob2QgPSBvcHRpb25zO1xuXHRcdFx0QXJyYXkucHJvdG90eXBlLnNoaWZ0LmNhbGwoYXJncyk7XG5cdFx0XHRyZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgc21hcnRtZW51cyA9ICQodGhpcykuZGF0YSgnc21hcnRtZW51cycpO1xuXHRcdFx0XHRpZiAoc21hcnRtZW51cyAmJiBzbWFydG1lbnVzW21ldGhvZF0pIHtcblx0XHRcdFx0XHRzbWFydG1lbnVzW21ldGhvZF0uYXBwbHkoc21hcnRtZW51cywgYXJncyk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0XHRyZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0Ly8gW2RhdGEtc20tb3B0aW9uc10gYXR0cmlidXRlIG9uIHRoZSByb290IFVMXG5cdFx0XHR2YXIgZGF0YU9wdHMgPSAkKHRoaXMpLmRhdGEoJ3NtLW9wdGlvbnMnKSB8fCBudWxsO1xuXHRcdFx0aWYgKGRhdGFPcHRzKSB7XG5cdFx0XHRcdHRyeSB7XG5cdFx0XHRcdFx0ZGF0YU9wdHMgPSBldmFsKCcoJyArIGRhdGFPcHRzICsgJyknKTtcblx0XHRcdFx0fSBjYXRjaChlKSB7XG5cdFx0XHRcdFx0ZGF0YU9wdHMgPSBudWxsO1xuXHRcdFx0XHRcdGFsZXJ0KCdFUlJPUlxcblxcblNtYXJ0TWVudXMgalF1ZXJ5IGluaXQ6XFxuSW52YWxpZCBcImRhdGEtc20tb3B0aW9uc1wiIGF0dHJpYnV0ZSB2YWx1ZSBzeW50YXguJyk7XG5cdFx0XHRcdH07XG5cdFx0XHR9XG5cdFx0XHRuZXcgJC5TbWFydE1lbnVzKHRoaXMsICQuZXh0ZW5kKHt9LCAkLmZuLnNtYXJ0bWVudXMuZGVmYXVsdHMsIG9wdGlvbnMsIGRhdGFPcHRzKSk7XG5cdFx0fSk7XG5cdH07XG5cblx0Ly8gZGVmYXVsdCBzZXR0aW5nc1xuXHQkLmZuLnNtYXJ0bWVudXMuZGVmYXVsdHMgPSB7XG5cdFx0aXNQb3B1cDpcdFx0ZmFsc2UsXHRcdC8vIGlzIHRoaXMgYSBwb3B1cCBtZW51IChjYW4gYmUgc2hvd24gdmlhIHRoZSBwb3B1cFNob3cvcG9wdXBIaWRlIG1ldGhvZHMpIG9yIGEgcGVybWFuZW50IG1lbnUgYmFyXG5cdFx0bWFpbk1lbnVTdWJPZmZzZXRYOlx0MCxcdFx0Ly8gcGl4ZWxzIG9mZnNldCBmcm9tIGRlZmF1bHQgcG9zaXRpb25cblx0XHRtYWluTWVudVN1Yk9mZnNldFk6XHQwLFx0XHQvLyBwaXhlbHMgb2Zmc2V0IGZyb20gZGVmYXVsdCBwb3NpdGlvblxuXHRcdHN1Yk1lbnVzU3ViT2Zmc2V0WDpcdDAsXHRcdC8vIHBpeGVscyBvZmZzZXQgZnJvbSBkZWZhdWx0IHBvc2l0aW9uXG5cdFx0c3ViTWVudXNTdWJPZmZzZXRZOlx0MCxcdFx0Ly8gcGl4ZWxzIG9mZnNldCBmcm9tIGRlZmF1bHQgcG9zaXRpb25cblx0XHRzdWJNZW51c01pbldpZHRoOlx0JzEwZW0nLFx0XHQvLyBtaW4td2lkdGggZm9yIHRoZSBzdWIgbWVudXMgKGFueSBDU1MgdW5pdCkgLSBpZiBzZXQsIHRoZSBmaXhlZCB3aWR0aCBzZXQgaW4gQ1NTIHdpbGwgYmUgaWdub3JlZFxuXHRcdHN1Yk1lbnVzTWF4V2lkdGg6XHQnMjBlbScsXHRcdC8vIG1heC13aWR0aCBmb3IgdGhlIHN1YiBtZW51cyAoYW55IENTUyB1bml0KSAtIGlmIHNldCwgdGhlIGZpeGVkIHdpZHRoIHNldCBpbiBDU1Mgd2lsbCBiZSBpZ25vcmVkXG5cdFx0c3ViSW5kaWNhdG9yczogXHRcdHRydWUsXHRcdC8vIGNyZWF0ZSBzdWIgbWVudSBpbmRpY2F0b3JzIC0gY3JlYXRlcyBhIFNQQU4gYW5kIGluc2VydHMgaXQgaW4gdGhlIEFcblx0XHRzdWJJbmRpY2F0b3JzUG9zOiBcdCdhcHBlbmQnLFx0Ly8gcG9zaXRpb24gb2YgdGhlIFNQQU4gcmVsYXRpdmUgdG8gdGhlIG1lbnUgaXRlbSBjb250ZW50ICgnYXBwZW5kJywgJ3ByZXBlbmQnKVxuXHRcdHN1YkluZGljYXRvcnNUZXh0Olx0JycsXHRcdC8vIFtvcHRpb25hbGx5XSBhZGQgdGV4dCBpbiB0aGUgU1BBTiAoZS5nLiAnKycpICh5b3UgbWF5IHdhbnQgdG8gY2hlY2sgdGhlIENTUyBmb3IgdGhlIHN1YiBpbmRpY2F0b3JzIHRvbylcblx0XHRzY3JvbGxTdGVwOiBcdFx0MzAsXHRcdC8vIHBpeGVscyBzdGVwIHdoZW4gc2Nyb2xsaW5nIGxvbmcgc3ViIG1lbnVzIHRoYXQgZG8gbm90IGZpdCBpbiB0aGUgdmlld3BvcnQgaGVpZ2h0XG5cdFx0c2Nyb2xsQWNjZWxlcmF0ZTpcdHRydWUsXHRcdC8vIGFjY2VsZXJhdGUgc2Nyb2xsaW5nIG9yIHVzZSBhIGZpeGVkIHN0ZXBcblx0XHRzaG93VGltZW91dDpcdFx0MjUwLFx0XHQvLyB0aW1lb3V0IGJlZm9yZSBzaG93aW5nIHRoZSBzdWIgbWVudXNcblx0XHRoaWRlVGltZW91dDpcdFx0NTAwLFx0XHQvLyB0aW1lb3V0IGJlZm9yZSBoaWRpbmcgdGhlIHN1YiBtZW51c1xuXHRcdHNob3dEdXJhdGlvbjpcdFx0MCxcdFx0Ly8gZHVyYXRpb24gZm9yIHNob3cgYW5pbWF0aW9uIC0gc2V0IHRvIDAgZm9yIG5vIGFuaW1hdGlvbiAtIG1hdHRlcnMgb25seSBpZiBzaG93RnVuY3Rpb246bnVsbFxuXHRcdHNob3dGdW5jdGlvbjpcdFx0bnVsbCxcdFx0Ly8gY3VzdG9tIGZ1bmN0aW9uIHRvIHVzZSB3aGVuIHNob3dpbmcgYSBzdWIgbWVudSAodGhlIGRlZmF1bHQgaXMgdGhlIGpRdWVyeSAnc2hvdycpXG5cdFx0XHRcdFx0XHRcdC8vIGRvbid0IGZvcmdldCB0byBjYWxsIGNvbXBsZXRlKCkgYXQgdGhlIGVuZCBvZiB3aGF0ZXZlciB5b3UgZG9cblx0XHRcdFx0XHRcdFx0Ly8gZS5nLjogZnVuY3Rpb24oJHVsLCBjb21wbGV0ZSkgeyAkdWwuZmFkZUluKDI1MCwgY29tcGxldGUpOyB9XG5cdFx0aGlkZUR1cmF0aW9uOlx0XHQwLFx0XHQvLyBkdXJhdGlvbiBmb3IgaGlkZSBhbmltYXRpb24gLSBzZXQgdG8gMCBmb3Igbm8gYW5pbWF0aW9uIC0gbWF0dGVycyBvbmx5IGlmIGhpZGVGdW5jdGlvbjpudWxsXG5cdFx0aGlkZUZ1bmN0aW9uOlx0XHRmdW5jdGlvbigkdWwsIGNvbXBsZXRlKSB7ICR1bC5mYWRlT3V0KDIwMCwgY29tcGxldGUpOyB9LFx0Ly8gY3VzdG9tIGZ1bmN0aW9uIHRvIHVzZSB3aGVuIGhpZGluZyBhIHN1YiBtZW51ICh0aGUgZGVmYXVsdCBpcyB0aGUgalF1ZXJ5ICdoaWRlJylcblx0XHRcdFx0XHRcdFx0Ly8gZG9uJ3QgZm9yZ2V0IHRvIGNhbGwgY29tcGxldGUoKSBhdCB0aGUgZW5kIG9mIHdoYXRldmVyIHlvdSBkb1xuXHRcdFx0XHRcdFx0XHQvLyBlLmcuOiBmdW5jdGlvbigkdWwsIGNvbXBsZXRlKSB7ICR1bC5mYWRlT3V0KDI1MCwgY29tcGxldGUpOyB9XG5cdFx0Y29sbGFwc2libGVTaG93RHVyYXRpb246MCxcdFx0Ly8gZHVyYXRpb24gZm9yIHNob3cgYW5pbWF0aW9uIGZvciBjb2xsYXBzaWJsZSBzdWIgbWVudXMgLSBtYXR0ZXJzIG9ubHkgaWYgY29sbGFwc2libGVTaG93RnVuY3Rpb246bnVsbFxuXHRcdGNvbGxhcHNpYmxlU2hvd0Z1bmN0aW9uOmZ1bmN0aW9uKCR1bCwgY29tcGxldGUpIHsgJHVsLnNsaWRlRG93bigyMDAsIGNvbXBsZXRlKTsgfSxcdC8vIGN1c3RvbSBmdW5jdGlvbiB0byB1c2Ugd2hlbiBzaG93aW5nIGEgY29sbGFwc2libGUgc3ViIG1lbnVcblx0XHRcdFx0XHRcdFx0Ly8gKGkuZS4gd2hlbiBtb2JpbGUgc3R5bGVzIGFyZSB1c2VkIHRvIG1ha2UgdGhlIHN1YiBtZW51cyBjb2xsYXBzaWJsZSlcblx0XHRjb2xsYXBzaWJsZUhpZGVEdXJhdGlvbjowLFx0XHQvLyBkdXJhdGlvbiBmb3IgaGlkZSBhbmltYXRpb24gZm9yIGNvbGxhcHNpYmxlIHN1YiBtZW51cyAtIG1hdHRlcnMgb25seSBpZiBjb2xsYXBzaWJsZUhpZGVGdW5jdGlvbjpudWxsXG5cdFx0Y29sbGFwc2libGVIaWRlRnVuY3Rpb246ZnVuY3Rpb24oJHVsLCBjb21wbGV0ZSkgeyAkdWwuc2xpZGVVcCgyMDAsIGNvbXBsZXRlKTsgfSxcdC8vIGN1c3RvbSBmdW5jdGlvbiB0byB1c2Ugd2hlbiBoaWRpbmcgYSBjb2xsYXBzaWJsZSBzdWIgbWVudVxuXHRcdFx0XHRcdFx0XHQvLyAoaS5lLiB3aGVuIG1vYmlsZSBzdHlsZXMgYXJlIHVzZWQgdG8gbWFrZSB0aGUgc3ViIG1lbnVzIGNvbGxhcHNpYmxlKVxuXHRcdHNob3dPbkNsaWNrOlx0XHRmYWxzZSxcdFx0Ly8gc2hvdyB0aGUgZmlyc3QtbGV2ZWwgc3ViIG1lbnVzIG9uY2xpY2sgaW5zdGVhZCBvZiBvbm1vdXNlb3ZlciAoaS5lLiBtaW1pYyBkZXNrdG9wIGFwcCBtZW51cykgKG1hdHRlcnMgb25seSBmb3IgbW91c2UgaW5wdXQpXG5cdFx0aGlkZU9uQ2xpY2s6XHRcdHRydWUsXHRcdC8vIGhpZGUgdGhlIHN1YiBtZW51cyBvbiBjbGljay90YXAgYW55d2hlcmUgb24gdGhlIHBhZ2Vcblx0XHRub01vdXNlT3ZlcjpcdFx0ZmFsc2UsXHRcdC8vIGRpc2FibGUgc3ViIG1lbnVzIGFjdGl2YXRpb24gb25tb3VzZW92ZXIgKGkuZS4gYmVoYXZlIGxpa2UgaW4gdG91Y2ggbW9kZSAtIHVzZSBqdXN0IG1vdXNlIGNsaWNrcykgKG1hdHRlcnMgb25seSBmb3IgbW91c2UgaW5wdXQpXG5cdFx0a2VlcEluVmlld3BvcnQ6XHRcdHRydWUsXHRcdC8vIHJlcG9zaXRpb24gdGhlIHN1YiBtZW51cyBpZiBuZWVkZWQgdG8gbWFrZSBzdXJlIHRoZXkgYWx3YXlzIGFwcGVhciBpbnNpZGUgdGhlIHZpZXdwb3J0XG5cdFx0a2VlcEhpZ2hsaWdodGVkOlx0dHJ1ZSxcdFx0Ly8ga2VlcCBhbGwgYW5jZXN0b3IgaXRlbXMgb2YgdGhlIGN1cnJlbnQgc3ViIG1lbnUgaGlnaGxpZ2h0ZWQgKGFkZHMgdGhlICdoaWdobGlnaHRlZCcgY2xhc3MgdG8gdGhlIEEncylcblx0XHRtYXJrQ3VycmVudEl0ZW06XHRmYWxzZSxcdFx0Ly8gYXV0b21hdGljYWxseSBhZGQgdGhlICdjdXJyZW50JyBjbGFzcyB0byB0aGUgQSBlbGVtZW50IG9mIHRoZSBpdGVtIGxpbmtpbmcgdG8gdGhlIGN1cnJlbnQgVVJMXG5cdFx0bWFya0N1cnJlbnRUcmVlOlx0dHJ1ZSxcdFx0Ly8gYWRkIHRoZSAnY3VycmVudCcgY2xhc3MgYWxzbyB0byB0aGUgQSBlbGVtZW50cyBvZiBhbGwgYW5jZXN0b3IgaXRlbXMgb2YgdGhlIGN1cnJlbnQgaXRlbVxuXHRcdHJpZ2h0VG9MZWZ0U3ViTWVudXM6XHRmYWxzZSxcdFx0Ly8gcmlnaHQgdG8gbGVmdCBkaXNwbGF5IG9mIHRoZSBzdWIgbWVudXMgKGNoZWNrIHRoZSBDU1MgZm9yIHRoZSBzdWIgaW5kaWNhdG9ycycgcG9zaXRpb24pXG5cdFx0Ym90dG9tVG9Ub3BTdWJNZW51czpcdGZhbHNlLFx0XHQvLyBib3R0b20gdG8gdG9wIGRpc3BsYXkgb2YgdGhlIHN1YiBtZW51c1xuXHRcdGNvbGxhcHNpYmxlQmVoYXZpb3I6XHQnZGVmYXVsdCdcdC8vIHBhcmVudCBpdGVtcyBiZWhhdmlvciBpbiBjb2xsYXBzaWJsZSAobW9iaWxlKSB2aWV3ICgnZGVmYXVsdCcsICd0b2dnbGUnLCAnbGluaycsICdhY2NvcmRpb24nLCAnYWNjb3JkaW9uLXRvZ2dsZScsICdhY2NvcmRpb24tbGluaycpXG5cdFx0XHRcdFx0XHRcdC8vICdkZWZhdWx0JyAtIGZpcnN0IHRhcCBvbiBwYXJlbnQgaXRlbSBleHBhbmRzIHN1Yiwgc2Vjb25kIHRhcCBsb2FkcyBpdHMgbGlua1xuXHRcdFx0XHRcdFx0XHQvLyAndG9nZ2xlJyAtIHRoZSB3aG9sZSBwYXJlbnQgaXRlbSBhY3RzIGp1c3QgYXMgYSB0b2dnbGUgYnV0dG9uIGZvciBpdHMgc3ViIG1lbnUgKGV4cGFuZHMvY29sbGFwc2VzIG9uIGVhY2ggdGFwKVxuXHRcdFx0XHRcdFx0XHQvLyAnbGluaycgLSB0aGUgcGFyZW50IGl0ZW0gYWN0cyBhcyBhIHJlZ3VsYXIgaXRlbSAoZmlyc3QgdGFwIGxvYWRzIGl0cyBsaW5rKSwgdGhlIHN1YiBtZW51IGNhbiBiZSBleHBhbmRlZCBvbmx5IHZpYSB0aGUgKy8tIGJ1dHRvblxuXHRcdFx0XHRcdFx0XHQvLyAnYWNjb3JkaW9uJyAtIGxpa2UgJ2RlZmF1bHQnIGJ1dCBvbiBleHBhbmQgYWxzbyByZXNldHMgYW55IHZpc2libGUgc3ViIG1lbnVzIGZyb20gZGVlcGVyIGxldmVscyBvciBvdGhlciBicmFuY2hlc1xuXHRcdFx0XHRcdFx0XHQvLyAnYWNjb3JkaW9uLXRvZ2dsZScgLSBsaWtlICd0b2dnbGUnIGJ1dCBvbiBleHBhbmQgYWxzbyByZXNldHMgYW55IHZpc2libGUgc3ViIG1lbnVzIGZyb20gZGVlcGVyIGxldmVscyBvciBvdGhlciBicmFuY2hlc1xuXHRcdFx0XHRcdFx0XHQvLyAnYWNjb3JkaW9uLWxpbmsnIC0gbGlrZSAnbGluaycgYnV0IG9uIGV4cGFuZCBhbHNvIHJlc2V0cyBhbnkgdmlzaWJsZSBzdWIgbWVudXMgZnJvbSBkZWVwZXIgbGV2ZWxzIG9yIG90aGVyIGJyYW5jaGVzXG5cdH07XG5cblx0cmV0dXJuICQ7XG59KSk7Il0sInNvdXJjZVJvb3QiOiIifQ==