(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendors~NER/Classifier/Editor"],{

/***/ "./node_modules/ace-builds/src-noconflict/mode-drools.js":
/*!***************************************************************!*\
  !*** ./node_modules/ace-builds/src-noconflict/mode-drools.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {ace.define("ace/mode/doc_comment_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/text_highlight_rules"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var DocCommentHighlightRules = function() {
    this.$rules = {
        "start" : [ {
            token : "comment.doc.tag",
            regex : "@[\\w\\d_]+" // TODO: fix email addresses
        }, 
        DocCommentHighlightRules.getTagRule(),
        {
            defaultToken : "comment.doc",
            caseInsensitive: true
        }]
    };
};

oop.inherits(DocCommentHighlightRules, TextHighlightRules);

DocCommentHighlightRules.getTagRule = function(start) {
    return {
        token : "comment.doc.tag.storage.type",
        regex : "\\b(?:TODO|FIXME|XXX|HACK)\\b"
    };
};

DocCommentHighlightRules.getStartRule = function(start) {
    return {
        token : "comment.doc", // doc comment
        regex : "\\/\\*(?=\\*)",
        next  : start
    };
};

DocCommentHighlightRules.getEndRule = function (start) {
    return {
        token : "comment.doc", // closing comment
        regex : "\\*\\/",
        next  : start
    };
};


exports.DocCommentHighlightRules = DocCommentHighlightRules;

});

ace.define("ace/mode/java_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/doc_comment_highlight_rules","ace/mode/text_highlight_rules"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var DocCommentHighlightRules = require("./doc_comment_highlight_rules").DocCommentHighlightRules;
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var JavaHighlightRules = function() {
    var keywords = (
    "abstract|continue|for|new|switch|" +
    "assert|default|goto|package|synchronized|" +
    "boolean|do|if|private|this|" +
    "break|double|implements|protected|throw|" +
    "byte|else|import|public|throws|" +
    "case|enum|instanceof|return|transient|" +
    "catch|extends|int|short|try|" +
    "char|final|interface|static|void|" +
    "class|finally|long|strictfp|volatile|" +
    "const|float|native|super|while|" +
    "var"
    );

    var buildinConstants = ("null|Infinity|NaN|undefined");


    var langClasses = (
        "AbstractMethodError|AssertionError|ClassCircularityError|"+
        "ClassFormatError|Deprecated|EnumConstantNotPresentException|"+
        "ExceptionInInitializerError|IllegalAccessError|"+
        "IllegalThreadStateException|InstantiationError|InternalError|"+
        "NegativeArraySizeException|NoSuchFieldError|Override|Process|"+
        "ProcessBuilder|SecurityManager|StringIndexOutOfBoundsException|"+
        "SuppressWarnings|TypeNotPresentException|UnknownError|"+
        "UnsatisfiedLinkError|UnsupportedClassVersionError|VerifyError|"+
        "InstantiationException|IndexOutOfBoundsException|"+
        "ArrayIndexOutOfBoundsException|CloneNotSupportedException|"+
        "NoSuchFieldException|IllegalArgumentException|NumberFormatException|"+
        "SecurityException|Void|InheritableThreadLocal|IllegalStateException|"+
        "InterruptedException|NoSuchMethodException|IllegalAccessException|"+
        "UnsupportedOperationException|Enum|StrictMath|Package|Compiler|"+
        "Readable|Runtime|StringBuilder|Math|IncompatibleClassChangeError|"+
        "NoSuchMethodError|ThreadLocal|RuntimePermission|ArithmeticException|"+
        "NullPointerException|Long|Integer|Short|Byte|Double|Number|Float|"+
        "Character|Boolean|StackTraceElement|Appendable|StringBuffer|"+
        "Iterable|ThreadGroup|Runnable|Thread|IllegalMonitorStateException|"+
        "StackOverflowError|OutOfMemoryError|VirtualMachineError|"+
        "ArrayStoreException|ClassCastException|LinkageError|"+
        "NoClassDefFoundError|ClassNotFoundException|RuntimeException|"+
        "Exception|ThreadDeath|Error|Throwable|System|ClassLoader|"+
        "Cloneable|Class|CharSequence|Comparable|String|Object"
    );

    var keywordMapper = this.createKeywordMapper({
        "variable.language": "this",
        "keyword": keywords,
        "constant.language": buildinConstants,
        "support.function": langClasses
    }, "identifier");

    this.$rules = {
        "start" : [
            {
                token : "comment",
                regex : "\\/\\/.*$"
            },
            DocCommentHighlightRules.getStartRule("doc-start"),
            {
                token : "comment", // multi line comment
                regex : "\\/\\*",
                next : "comment"
            }, {
                token : "string", // single line
                regex : '["](?:(?:\\\\.)|(?:[^"\\\\]))*?["]'
            }, {
                token : "string", // single line
                regex : "['](?:(?:\\\\.)|(?:[^'\\\\]))*?[']"
            }, {
                token : "constant.numeric", // hex
                regex : /0(?:[xX][0-9a-fA-F][0-9a-fA-F_]*|[bB][01][01_]*)[LlSsDdFfYy]?\b/
            }, {
                token : "constant.numeric", // float
                regex : /[+-]?\d[\d_]*(?:(?:\.[\d_]*)?(?:[eE][+-]?[\d_]+)?)?[LlSsDdFfYy]?\b/
            }, {
                token : "constant.language.boolean",
                regex : "(?:true|false)\\b"
            }, {
                regex: "(open(?:\\s+))?module(?=\\s*\\w)",
                token: "keyword",
                next: [{
                    regex: "{",
                    token: "paren.lparen",
                    next: [{
                        regex: "}",
                        token: "paren.rparen",
                        next: "start"
                    }, {
                        regex: "\\b(requires|transitive|exports|opens|to|uses|provides|with)\\b",
                        token: "keyword" 
                    }]
                }, {
                    token : "text",
                    regex : "\\s+"
                }, {
                    token : "identifier",
                    regex : "\\w+"
                }, {
                    token : "punctuation.operator",
                    regex : "."
                }, {
                    token : "text",
                    regex : "\\s+"
                }, {
                    regex: "", // exit if there is anything else
                    next: "start"
                }]
            }, {
                token : keywordMapper,
                regex : "[a-zA-Z_$][a-zA-Z0-9_$]*\\b"
            }, {
                token : "keyword.operator",
                regex : "!|\\$|%|&|\\*|\\-\\-|\\-|\\+\\+|\\+|~|===|==|=|!=|!==|<=|>=|<<=|>>=|>>>=|<>|<|>|!|&&|\\|\\||\\?\\:|\\*=|%=|\\+=|\\-=|&=|\\^=|\\b(?:in|instanceof|new|delete|typeof|void)"
            }, {
                token : "lparen",
                regex : "[[({]"
            }, {
                token : "rparen",
                regex : "[\\])}]"
            }, {
                token : "text",
                regex : "\\s+"
            }
        ],
        "comment" : [
            {
                token : "comment", // closing comment
                regex : "\\*\\/",
                next : "start"
            }, {
                defaultToken : "comment"
            }
        ]
    };

    
    this.embedRules(DocCommentHighlightRules, "doc-",
        [ DocCommentHighlightRules.getEndRule("start") ]);
    this.normalizeRules();
};

oop.inherits(JavaHighlightRules, TextHighlightRules);

exports.JavaHighlightRules = JavaHighlightRules;
});

ace.define("ace/mode/drools_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/text_highlight_rules","ace/mode/java_highlight_rules","ace/mode/doc_comment_highlight_rules"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;
var JavaHighlightRules = require("./java_highlight_rules").JavaHighlightRules;
var DocCommentHighlightRules = require("./doc_comment_highlight_rules").DocCommentHighlightRules;

var identifierRe = "[a-zA-Z\\$_\u00a1-\uffff][a-zA-Z\\d\\$_\u00a1-\uffff]*";
var packageIdentifierRe = "[a-zA-Z\\$_\u00a1-\uffff][\\.a-zA-Z\\d\\$_\u00a1-\uffff]*";

var DroolsHighlightRules = function() {

    var keywords = ("date|effective|expires|lock|on|active|no|loop|auto|focus" +
        "|activation|group|agenda|ruleflow|duration|timer|calendars|refract|direct" +
        "|dialect|salience|enabled|attributes|extends|template" +
        "|function|contains|matches|eval|excludes|soundslike" +
        "|memberof|not|in|or|and|exists|forall|over|from|entry|point|accumulate|acc|collect" +
        "|action|reverse|result|end|init|instanceof|extends|super|boolean|char|byte|short" +
        "|int|long|float|double|this|void|class|new|case|final|if|else|for|while|do" +
        "|default|try|catch|finally|switch|synchronized|return|throw|break|continue|assert" +
        "|modify|static|public|protected|private|abstract|native|transient|volatile" +
        "|strictfp|throws|interface|enum|implements|type|window|trait|no-loop|str"
      );

      var langClasses = (
          "AbstractMethodError|AssertionError|ClassCircularityError|"+
          "ClassFormatError|Deprecated|EnumConstantNotPresentException|"+
          "ExceptionInInitializerError|IllegalAccessError|"+
          "IllegalThreadStateException|InstantiationError|InternalError|"+
          "NegativeArraySizeException|NoSuchFieldError|Override|Process|"+
          "ProcessBuilder|SecurityManager|StringIndexOutOfBoundsException|"+
          "SuppressWarnings|TypeNotPresentException|UnknownError|"+
          "UnsatisfiedLinkError|UnsupportedClassVersionError|VerifyError|"+
          "InstantiationException|IndexOutOfBoundsException|"+
          "ArrayIndexOutOfBoundsException|CloneNotSupportedException|"+
          "NoSuchFieldException|IllegalArgumentException|NumberFormatException|"+
          "SecurityException|Void|InheritableThreadLocal|IllegalStateException|"+
          "InterruptedException|NoSuchMethodException|IllegalAccessException|"+
          "UnsupportedOperationException|Enum|StrictMath|Package|Compiler|"+
          "Readable|Runtime|StringBuilder|Math|IncompatibleClassChangeError|"+
          "NoSuchMethodError|ThreadLocal|RuntimePermission|ArithmeticException|"+
          "NullPointerException|Long|Integer|Short|Byte|Double|Number|Float|"+
          "Character|Boolean|StackTraceElement|Appendable|StringBuffer|"+
          "Iterable|ThreadGroup|Runnable|Thread|IllegalMonitorStateException|"+
          "StackOverflowError|OutOfMemoryError|VirtualMachineError|"+
          "ArrayStoreException|ClassCastException|LinkageError|"+
          "NoClassDefFoundError|ClassNotFoundException|RuntimeException|"+
          "Exception|ThreadDeath|Error|Throwable|System|ClassLoader|"+
          "Cloneable|Class|CharSequence|Comparable|String|Object"
      );

    var keywordMapper = this.createKeywordMapper({
        "variable.language": "this",
        "keyword": keywords,
        "constant.language": "null",
        "support.class" : langClasses,
        "support.function" : "retract|update|modify|insert"
    }, "identifier");

    var stringRules = function() {
      return [{
        token : "string", // single line
        regex : '["](?:(?:\\\\.)|(?:[^"\\\\]))*?["]'
      }, {
        token : "string", // single line
        regex : "['](?:(?:\\\\.)|(?:[^'\\\\]))*?[']"
      }];
    };


      var basicPreRules = function(blockCommentRules) {
        return [{
            token : "comment",
            regex : "\\/\\/.*$"
        },
        DocCommentHighlightRules.getStartRule("doc-start"),
        {
            token : "comment", // multi line comment
            regex : "\\/\\*",
            next : blockCommentRules
        }, {
            token : "constant.numeric", // hex
            regex : "0[xX][0-9a-fA-F]+\\b"
        }, {
            token : "constant.numeric", // float
            regex : "[+-]?\\d+(?:(?:\\.\\d*)?(?:[eE][+-]?\\d+)?)?\\b"
        }, {
            token : "constant.language.boolean",
            regex : "(?:true|false)\\b"
          }];
      };

      var blockCommentRules = function(returnRule) {
        return [
            {
                token : "comment.block", // closing comment
                regex : "\\*\\/",
                next : returnRule
            }, {
                defaultToken : "comment.block"
            }
        ];
      };

      var basicPostRules = function() {
        return [{
            token : keywordMapper,
            regex : "[a-zA-Z_$][a-zA-Z0-9_$]*\\b"
        }, {
            token : "keyword.operator",
            regex : "!|\\$|%|&|\\*|\\-\\-|\\-|\\+\\+|\\+|~|===|==|=|!=|!==|<=|>=|<<=|>>=|>>>=|<>|<|>|!|&&|\\|\\||\\?\\:|\\*=|%=|\\+=|\\-=|&=|\\^=|\\b(?:in|instanceof|new|delete|typeof|void)"
        }, {
            token : "lparen",
            regex : "[[({]"
        }, {
            token : "rparen",
            regex : "[\\])}]"
        }, {
            token : "text",
            regex : "\\s+"
        }];
      };


    this.$rules = {
        "start" : [].concat(basicPreRules("block.comment"), [
              {
                token : "entity.name.type",
                regex : "@[a-zA-Z_$][a-zA-Z0-9_$]*\\b"
              }, {
                token : ["keyword","text","entity.name.type"],
                regex : "(package)(\\s+)(" + packageIdentifierRe +")"
              }, {
                token : ["keyword","text","keyword","text","entity.name.type"],
                regex : "(import)(\\s+)(function)(\\s+)(" + packageIdentifierRe +")"
              }, {
                token : ["keyword","text","entity.name.type"],
                regex : "(import)(\\s+)(" + packageIdentifierRe +")"
              }, {
                token : ["keyword","text","entity.name.type","text","variable"],
                regex : "(global)(\\s+)(" + packageIdentifierRe +")(\\s+)(" + identifierRe +")"
              }, {
                token : ["keyword","text","keyword","text","entity.name.type"],
                regex : "(declare)(\\s+)(trait)(\\s+)(" + identifierRe +")"
              }, {
                token : ["keyword","text","entity.name.type"],
                regex : "(declare)(\\s+)(" + identifierRe +")"
              }, {
                token : ["keyword","text","entity.name.type"],
                regex : "(extends)(\\s+)(" + packageIdentifierRe +")"
              }, {
                token : ["keyword","text"],
                regex : "(rule)(\\s+)",
                next :  "asset.name"
              }],
              stringRules(),
              [{
                token : ["variable.other","text","text"],
                regex : "(" + identifierRe + ")(\\s*)(:)"
              }, {
                token : ["keyword","text"],
                regex : "(query)(\\s+)",
                next :  "asset.name"
              }, {
                token : ["keyword","text"],
                regex : "(when)(\\s*)"
              }, {
                token : ["keyword","text"],
                regex : "(then)(\\s*)",
                next :  "java-start"
              }, {
                  token : "paren.lparen",
                  regex : /[\[({]/
              }, {
                  token : "paren.rparen",
                  regex : /[\])}]/
              }], basicPostRules()),
        "block.comment" : blockCommentRules("start"),
        "asset.name" : [
            {
                token : "entity.name",
                regex : '["](?:(?:\\\\.)|(?:[^"\\\\]))*?["]'
            }, {
                token : "entity.name",
                regex : "['](?:(?:\\\\.)|(?:[^'\\\\]))*?[']"
            }, {
                token : "entity.name",
                regex : identifierRe
            }, {
                regex: "",
                token: "empty",
                next: "start"
            }]
    };
    this.embedRules(DocCommentHighlightRules, "doc-",
        [ DocCommentHighlightRules.getEndRule("start") ]);

    this.embedRules(JavaHighlightRules, "java-", [
      {
       token : "support.function",
       regex: "\\b(insert|modify|retract|update)\\b"
     }, {
       token : "keyword",
       regex: "\\bend\\b",
       next  : "start"
    }]);

};

oop.inherits(DroolsHighlightRules, TextHighlightRules);

exports.DroolsHighlightRules = DroolsHighlightRules;
});

ace.define("ace/mode/folding/drools",["require","exports","module","ace/lib/oop","ace/range","ace/mode/folding/fold_mode","ace/token_iterator"], function(require, exports, module) {
"use strict";

var oop = require("../../lib/oop");
var Range = require("../../range").Range;
var BaseFoldMode = require("./fold_mode").FoldMode;
var TokenIterator = require("../../token_iterator").TokenIterator;

var FoldMode = exports.FoldMode = function() {};
oop.inherits(FoldMode, BaseFoldMode);

(function() {
    this.foldingStartMarker = /\b(rule|declare|query|when|then)\b/; 
    this.foldingStopMarker = /\bend\b/;

    this.getFoldWidgetRange = function(session, foldStyle, row) {
        var line = session.getLine(row);
        var match = line.match(this.foldingStartMarker);
        if (match) {
            var i = match.index;

            if (match[1]) {
                var position = {row: row, column: line.length};
                var iterator = new TokenIterator(session, position.row, position.column);
                var seek = "end";
                var token = iterator.getCurrentToken();
                if (token.value == "when") {
                    seek = "then";
                }
                while (token) {
                    if (token.value == seek) { 
                        return Range.fromPoints(position ,{
                            row: iterator.getCurrentTokenRow(),
                            column: iterator.getCurrentTokenColumn()
                        });
                    }
                    token = iterator.stepForward();
                }
            }

        }
    };

}).call(FoldMode.prototype);

});

ace.define("ace/mode/drools",["require","exports","module","ace/lib/oop","ace/mode/text","ace/mode/drools_highlight_rules","ace/mode/folding/drools"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextMode = require("./text").Mode;
var DroolsHighlightRules = require("./drools_highlight_rules").DroolsHighlightRules;
var DroolsFoldMode = require("./folding/drools").FoldMode;

var Mode = function() {
    this.HighlightRules = DroolsHighlightRules;
    this.foldingRules = new DroolsFoldMode();
    this.$behaviour = this.$defaultBehaviour;

};
oop.inherits(Mode, TextMode);

(function() {
    this.lineCommentStart = "//";
    this.$id = "ace/mode/drools";
    this.snippetFileId = "ace/snippets/drools";
}).call(Mode.prototype);

exports.Mode = Mode;

});                (function() {
                    ace.require(["ace/mode/drools"], function(m) {
                        if ( true && module) {
                            module.exports = m;
                        }
                    });
                })();
            
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/module.js */ "./node_modules/webpack/buildin/module.js")(module)))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYWNlLWJ1aWxkcy9zcmMtbm9jb25mbGljdC9tb2RlLWRyb29scy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7O0FBRUEsQ0FBQzs7QUFFRDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsYUFBYTtBQUNiO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLDZCQUE2QjtBQUM3QixhQUFhO0FBQ2I7QUFDQSwrQkFBK0I7QUFDL0IsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsV0FBVztBQUNYOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EseUJBQXlCO0FBQ3pCLFNBQVM7QUFDVDtBQUNBLDJCQUEyQjtBQUMzQixTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQSxnQ0FBZ0M7QUFDaEMsZUFBZTtBQUNmO0FBQ0EsZ0NBQWdDO0FBQ2hDLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7O0FBRUE7O0FBRUE7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1FO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGdDQUFnQztBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsQ0FBQzs7QUFFRCxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUEsQ0FBQyxFQUFFO0FBQ0g7QUFDQSw0QkFBNEIsS0FBdUQ7QUFDbkY7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQixpQkFBaUIiLCJmaWxlIjoidmVuZG9yc35ORVIvQ2xhc3NpZmllci9FZGl0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyJhY2UuZGVmaW5lKFwiYWNlL21vZGUvZG9jX2NvbW1lbnRfaGlnaGxpZ2h0X3J1bGVzXCIsW1wicmVxdWlyZVwiLFwiZXhwb3J0c1wiLFwibW9kdWxlXCIsXCJhY2UvbGliL29vcFwiLFwiYWNlL21vZGUvdGV4dF9oaWdobGlnaHRfcnVsZXNcIl0sIGZ1bmN0aW9uKHJlcXVpcmUsIGV4cG9ydHMsIG1vZHVsZSkge1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBvb3AgPSByZXF1aXJlKFwiLi4vbGliL29vcFwiKTtcbnZhciBUZXh0SGlnaGxpZ2h0UnVsZXMgPSByZXF1aXJlKFwiLi90ZXh0X2hpZ2hsaWdodF9ydWxlc1wiKS5UZXh0SGlnaGxpZ2h0UnVsZXM7XG5cbnZhciBEb2NDb21tZW50SGlnaGxpZ2h0UnVsZXMgPSBmdW5jdGlvbigpIHtcbiAgICB0aGlzLiRydWxlcyA9IHtcbiAgICAgICAgXCJzdGFydFwiIDogWyB7XG4gICAgICAgICAgICB0b2tlbiA6IFwiY29tbWVudC5kb2MudGFnXCIsXG4gICAgICAgICAgICByZWdleCA6IFwiQFtcXFxcd1xcXFxkX10rXCIgLy8gVE9ETzogZml4IGVtYWlsIGFkZHJlc3Nlc1xuICAgICAgICB9LCBcbiAgICAgICAgRG9jQ29tbWVudEhpZ2hsaWdodFJ1bGVzLmdldFRhZ1J1bGUoKSxcbiAgICAgICAge1xuICAgICAgICAgICAgZGVmYXVsdFRva2VuIDogXCJjb21tZW50LmRvY1wiLFxuICAgICAgICAgICAgY2FzZUluc2Vuc2l0aXZlOiB0cnVlXG4gICAgICAgIH1dXG4gICAgfTtcbn07XG5cbm9vcC5pbmhlcml0cyhEb2NDb21tZW50SGlnaGxpZ2h0UnVsZXMsIFRleHRIaWdobGlnaHRSdWxlcyk7XG5cbkRvY0NvbW1lbnRIaWdobGlnaHRSdWxlcy5nZXRUYWdSdWxlID0gZnVuY3Rpb24oc3RhcnQpIHtcbiAgICByZXR1cm4ge1xuICAgICAgICB0b2tlbiA6IFwiY29tbWVudC5kb2MudGFnLnN0b3JhZ2UudHlwZVwiLFxuICAgICAgICByZWdleCA6IFwiXFxcXGIoPzpUT0RPfEZJWE1FfFhYWHxIQUNLKVxcXFxiXCJcbiAgICB9O1xufTtcblxuRG9jQ29tbWVudEhpZ2hsaWdodFJ1bGVzLmdldFN0YXJ0UnVsZSA9IGZ1bmN0aW9uKHN0YXJ0KSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgdG9rZW4gOiBcImNvbW1lbnQuZG9jXCIsIC8vIGRvYyBjb21tZW50XG4gICAgICAgIHJlZ2V4IDogXCJcXFxcL1xcXFwqKD89XFxcXCopXCIsXG4gICAgICAgIG5leHQgIDogc3RhcnRcbiAgICB9O1xufTtcblxuRG9jQ29tbWVudEhpZ2hsaWdodFJ1bGVzLmdldEVuZFJ1bGUgPSBmdW5jdGlvbiAoc3RhcnQpIHtcbiAgICByZXR1cm4ge1xuICAgICAgICB0b2tlbiA6IFwiY29tbWVudC5kb2NcIiwgLy8gY2xvc2luZyBjb21tZW50XG4gICAgICAgIHJlZ2V4IDogXCJcXFxcKlxcXFwvXCIsXG4gICAgICAgIG5leHQgIDogc3RhcnRcbiAgICB9O1xufTtcblxuXG5leHBvcnRzLkRvY0NvbW1lbnRIaWdobGlnaHRSdWxlcyA9IERvY0NvbW1lbnRIaWdobGlnaHRSdWxlcztcblxufSk7XG5cbmFjZS5kZWZpbmUoXCJhY2UvbW9kZS9qYXZhX2hpZ2hsaWdodF9ydWxlc1wiLFtcInJlcXVpcmVcIixcImV4cG9ydHNcIixcIm1vZHVsZVwiLFwiYWNlL2xpYi9vb3BcIixcImFjZS9tb2RlL2RvY19jb21tZW50X2hpZ2hsaWdodF9ydWxlc1wiLFwiYWNlL21vZGUvdGV4dF9oaWdobGlnaHRfcnVsZXNcIl0sIGZ1bmN0aW9uKHJlcXVpcmUsIGV4cG9ydHMsIG1vZHVsZSkge1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBvb3AgPSByZXF1aXJlKFwiLi4vbGliL29vcFwiKTtcbnZhciBEb2NDb21tZW50SGlnaGxpZ2h0UnVsZXMgPSByZXF1aXJlKFwiLi9kb2NfY29tbWVudF9oaWdobGlnaHRfcnVsZXNcIikuRG9jQ29tbWVudEhpZ2hsaWdodFJ1bGVzO1xudmFyIFRleHRIaWdobGlnaHRSdWxlcyA9IHJlcXVpcmUoXCIuL3RleHRfaGlnaGxpZ2h0X3J1bGVzXCIpLlRleHRIaWdobGlnaHRSdWxlcztcblxudmFyIEphdmFIaWdobGlnaHRSdWxlcyA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBrZXl3b3JkcyA9IChcbiAgICBcImFic3RyYWN0fGNvbnRpbnVlfGZvcnxuZXd8c3dpdGNofFwiICtcbiAgICBcImFzc2VydHxkZWZhdWx0fGdvdG98cGFja2FnZXxzeW5jaHJvbml6ZWR8XCIgK1xuICAgIFwiYm9vbGVhbnxkb3xpZnxwcml2YXRlfHRoaXN8XCIgK1xuICAgIFwiYnJlYWt8ZG91YmxlfGltcGxlbWVudHN8cHJvdGVjdGVkfHRocm93fFwiICtcbiAgICBcImJ5dGV8ZWxzZXxpbXBvcnR8cHVibGljfHRocm93c3xcIiArXG4gICAgXCJjYXNlfGVudW18aW5zdGFuY2VvZnxyZXR1cm58dHJhbnNpZW50fFwiICtcbiAgICBcImNhdGNofGV4dGVuZHN8aW50fHNob3J0fHRyeXxcIiArXG4gICAgXCJjaGFyfGZpbmFsfGludGVyZmFjZXxzdGF0aWN8dm9pZHxcIiArXG4gICAgXCJjbGFzc3xmaW5hbGx5fGxvbmd8c3RyaWN0ZnB8dm9sYXRpbGV8XCIgK1xuICAgIFwiY29uc3R8ZmxvYXR8bmF0aXZlfHN1cGVyfHdoaWxlfFwiICtcbiAgICBcInZhclwiXG4gICAgKTtcblxuICAgIHZhciBidWlsZGluQ29uc3RhbnRzID0gKFwibnVsbHxJbmZpbml0eXxOYU58dW5kZWZpbmVkXCIpO1xuXG5cbiAgICB2YXIgbGFuZ0NsYXNzZXMgPSAoXG4gICAgICAgIFwiQWJzdHJhY3RNZXRob2RFcnJvcnxBc3NlcnRpb25FcnJvcnxDbGFzc0NpcmN1bGFyaXR5RXJyb3J8XCIrXG4gICAgICAgIFwiQ2xhc3NGb3JtYXRFcnJvcnxEZXByZWNhdGVkfEVudW1Db25zdGFudE5vdFByZXNlbnRFeGNlcHRpb258XCIrXG4gICAgICAgIFwiRXhjZXB0aW9uSW5Jbml0aWFsaXplckVycm9yfElsbGVnYWxBY2Nlc3NFcnJvcnxcIitcbiAgICAgICAgXCJJbGxlZ2FsVGhyZWFkU3RhdGVFeGNlcHRpb258SW5zdGFudGlhdGlvbkVycm9yfEludGVybmFsRXJyb3J8XCIrXG4gICAgICAgIFwiTmVnYXRpdmVBcnJheVNpemVFeGNlcHRpb258Tm9TdWNoRmllbGRFcnJvcnxPdmVycmlkZXxQcm9jZXNzfFwiK1xuICAgICAgICBcIlByb2Nlc3NCdWlsZGVyfFNlY3VyaXR5TWFuYWdlcnxTdHJpbmdJbmRleE91dE9mQm91bmRzRXhjZXB0aW9ufFwiK1xuICAgICAgICBcIlN1cHByZXNzV2FybmluZ3N8VHlwZU5vdFByZXNlbnRFeGNlcHRpb258VW5rbm93bkVycm9yfFwiK1xuICAgICAgICBcIlVuc2F0aXNmaWVkTGlua0Vycm9yfFVuc3VwcG9ydGVkQ2xhc3NWZXJzaW9uRXJyb3J8VmVyaWZ5RXJyb3J8XCIrXG4gICAgICAgIFwiSW5zdGFudGlhdGlvbkV4Y2VwdGlvbnxJbmRleE91dE9mQm91bmRzRXhjZXB0aW9ufFwiK1xuICAgICAgICBcIkFycmF5SW5kZXhPdXRPZkJvdW5kc0V4Y2VwdGlvbnxDbG9uZU5vdFN1cHBvcnRlZEV4Y2VwdGlvbnxcIitcbiAgICAgICAgXCJOb1N1Y2hGaWVsZEV4Y2VwdGlvbnxJbGxlZ2FsQXJndW1lbnRFeGNlcHRpb258TnVtYmVyRm9ybWF0RXhjZXB0aW9ufFwiK1xuICAgICAgICBcIlNlY3VyaXR5RXhjZXB0aW9ufFZvaWR8SW5oZXJpdGFibGVUaHJlYWRMb2NhbHxJbGxlZ2FsU3RhdGVFeGNlcHRpb258XCIrXG4gICAgICAgIFwiSW50ZXJydXB0ZWRFeGNlcHRpb258Tm9TdWNoTWV0aG9kRXhjZXB0aW9ufElsbGVnYWxBY2Nlc3NFeGNlcHRpb258XCIrXG4gICAgICAgIFwiVW5zdXBwb3J0ZWRPcGVyYXRpb25FeGNlcHRpb258RW51bXxTdHJpY3RNYXRofFBhY2thZ2V8Q29tcGlsZXJ8XCIrXG4gICAgICAgIFwiUmVhZGFibGV8UnVudGltZXxTdHJpbmdCdWlsZGVyfE1hdGh8SW5jb21wYXRpYmxlQ2xhc3NDaGFuZ2VFcnJvcnxcIitcbiAgICAgICAgXCJOb1N1Y2hNZXRob2RFcnJvcnxUaHJlYWRMb2NhbHxSdW50aW1lUGVybWlzc2lvbnxBcml0aG1ldGljRXhjZXB0aW9ufFwiK1xuICAgICAgICBcIk51bGxQb2ludGVyRXhjZXB0aW9ufExvbmd8SW50ZWdlcnxTaG9ydHxCeXRlfERvdWJsZXxOdW1iZXJ8RmxvYXR8XCIrXG4gICAgICAgIFwiQ2hhcmFjdGVyfEJvb2xlYW58U3RhY2tUcmFjZUVsZW1lbnR8QXBwZW5kYWJsZXxTdHJpbmdCdWZmZXJ8XCIrXG4gICAgICAgIFwiSXRlcmFibGV8VGhyZWFkR3JvdXB8UnVubmFibGV8VGhyZWFkfElsbGVnYWxNb25pdG9yU3RhdGVFeGNlcHRpb258XCIrXG4gICAgICAgIFwiU3RhY2tPdmVyZmxvd0Vycm9yfE91dE9mTWVtb3J5RXJyb3J8VmlydHVhbE1hY2hpbmVFcnJvcnxcIitcbiAgICAgICAgXCJBcnJheVN0b3JlRXhjZXB0aW9ufENsYXNzQ2FzdEV4Y2VwdGlvbnxMaW5rYWdlRXJyb3J8XCIrXG4gICAgICAgIFwiTm9DbGFzc0RlZkZvdW5kRXJyb3J8Q2xhc3NOb3RGb3VuZEV4Y2VwdGlvbnxSdW50aW1lRXhjZXB0aW9ufFwiK1xuICAgICAgICBcIkV4Y2VwdGlvbnxUaHJlYWREZWF0aHxFcnJvcnxUaHJvd2FibGV8U3lzdGVtfENsYXNzTG9hZGVyfFwiK1xuICAgICAgICBcIkNsb25lYWJsZXxDbGFzc3xDaGFyU2VxdWVuY2V8Q29tcGFyYWJsZXxTdHJpbmd8T2JqZWN0XCJcbiAgICApO1xuXG4gICAgdmFyIGtleXdvcmRNYXBwZXIgPSB0aGlzLmNyZWF0ZUtleXdvcmRNYXBwZXIoe1xuICAgICAgICBcInZhcmlhYmxlLmxhbmd1YWdlXCI6IFwidGhpc1wiLFxuICAgICAgICBcImtleXdvcmRcIjoga2V5d29yZHMsXG4gICAgICAgIFwiY29uc3RhbnQubGFuZ3VhZ2VcIjogYnVpbGRpbkNvbnN0YW50cyxcbiAgICAgICAgXCJzdXBwb3J0LmZ1bmN0aW9uXCI6IGxhbmdDbGFzc2VzXG4gICAgfSwgXCJpZGVudGlmaWVyXCIpO1xuXG4gICAgdGhpcy4kcnVsZXMgPSB7XG4gICAgICAgIFwic3RhcnRcIiA6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwiY29tbWVudFwiLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCJcXFxcL1xcXFwvLiokXCJcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBEb2NDb21tZW50SGlnaGxpZ2h0UnVsZXMuZ2V0U3RhcnRSdWxlKFwiZG9jLXN0YXJ0XCIpLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRva2VuIDogXCJjb21tZW50XCIsIC8vIG11bHRpIGxpbmUgY29tbWVudFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCJcXFxcL1xcXFwqXCIsXG4gICAgICAgICAgICAgICAgbmV4dCA6IFwiY29tbWVudFwiXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgdG9rZW4gOiBcInN0cmluZ1wiLCAvLyBzaW5nbGUgbGluZVxuICAgICAgICAgICAgICAgIHJlZ2V4IDogJ1tcIl0oPzooPzpcXFxcXFxcXC4pfCg/OlteXCJcXFxcXFxcXF0pKSo/W1wiXSdcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwic3RyaW5nXCIsIC8vIHNpbmdsZSBsaW5lXG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIlsnXSg/Oig/OlxcXFxcXFxcLil8KD86W14nXFxcXFxcXFxdKSkqP1snXVwiXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgdG9rZW4gOiBcImNvbnN0YW50Lm51bWVyaWNcIiwgLy8gaGV4XG4gICAgICAgICAgICAgICAgcmVnZXggOiAvMCg/Olt4WF1bMC05YS1mQS1GXVswLTlhLWZBLUZfXSp8W2JCXVswMV1bMDFfXSopW0xsU3NEZEZmWXldP1xcYi9cbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwiY29uc3RhbnQubnVtZXJpY1wiLCAvLyBmbG9hdFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogL1srLV0/XFxkW1xcZF9dKig/Oig/OlxcLltcXGRfXSopPyg/OltlRV1bKy1dP1tcXGRfXSspPyk/W0xsU3NEZEZmWXldP1xcYi9cbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwiY29uc3RhbnQubGFuZ3VhZ2UuYm9vbGVhblwiLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCIoPzp0cnVlfGZhbHNlKVxcXFxiXCJcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICByZWdleDogXCIob3Blbig/OlxcXFxzKykpP21vZHVsZSg/PVxcXFxzKlxcXFx3KVwiLFxuICAgICAgICAgICAgICAgIHRva2VuOiBcImtleXdvcmRcIixcbiAgICAgICAgICAgICAgICBuZXh0OiBbe1xuICAgICAgICAgICAgICAgICAgICByZWdleDogXCJ7XCIsXG4gICAgICAgICAgICAgICAgICAgIHRva2VuOiBcInBhcmVuLmxwYXJlblwiLFxuICAgICAgICAgICAgICAgICAgICBuZXh0OiBbe1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVnZXg6IFwifVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgdG9rZW46IFwicGFyZW4ucnBhcmVuXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXh0OiBcInN0YXJ0XCJcbiAgICAgICAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVnZXg6IFwiXFxcXGIocmVxdWlyZXN8dHJhbnNpdGl2ZXxleHBvcnRzfG9wZW5zfHRvfHVzZXN8cHJvdmlkZXN8d2l0aClcXFxcYlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgdG9rZW46IFwia2V5d29yZFwiIFxuICAgICAgICAgICAgICAgICAgICB9XVxuICAgICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICAgICAgdG9rZW4gOiBcInRleHRcIixcbiAgICAgICAgICAgICAgICAgICAgcmVnZXggOiBcIlxcXFxzK1wiXG4gICAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgICAgICB0b2tlbiA6IFwiaWRlbnRpZmllclwiLFxuICAgICAgICAgICAgICAgICAgICByZWdleCA6IFwiXFxcXHcrXCJcbiAgICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgICAgIHRva2VuIDogXCJwdW5jdHVhdGlvbi5vcGVyYXRvclwiLFxuICAgICAgICAgICAgICAgICAgICByZWdleCA6IFwiLlwiXG4gICAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgICAgICB0b2tlbiA6IFwidGV4dFwiLFxuICAgICAgICAgICAgICAgICAgICByZWdleCA6IFwiXFxcXHMrXCJcbiAgICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgICAgIHJlZ2V4OiBcIlwiLCAvLyBleGl0IGlmIHRoZXJlIGlzIGFueXRoaW5nIGVsc2VcbiAgICAgICAgICAgICAgICAgICAgbmV4dDogXCJzdGFydFwiXG4gICAgICAgICAgICAgICAgfV1cbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IGtleXdvcmRNYXBwZXIsXG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIlthLXpBLVpfJF1bYS16QS1aMC05XyRdKlxcXFxiXCJcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwia2V5d29yZC5vcGVyYXRvclwiLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCIhfFxcXFwkfCV8JnxcXFxcKnxcXFxcLVxcXFwtfFxcXFwtfFxcXFwrXFxcXCt8XFxcXCt8fnw9PT18PT18PXwhPXwhPT18PD18Pj18PDw9fD4+PXw+Pj49fDw+fDx8PnwhfCYmfFxcXFx8XFxcXHx8XFxcXD9cXFxcOnxcXFxcKj18JT18XFxcXCs9fFxcXFwtPXwmPXxcXFxcXj18XFxcXGIoPzppbnxpbnN0YW5jZW9mfG5ld3xkZWxldGV8dHlwZW9mfHZvaWQpXCJcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwibHBhcmVuXCIsXG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIltbKHtdXCJcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwicnBhcmVuXCIsXG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIltcXFxcXSl9XVwiXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgdG9rZW4gOiBcInRleHRcIixcbiAgICAgICAgICAgICAgICByZWdleCA6IFwiXFxcXHMrXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgXCJjb21tZW50XCIgOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdG9rZW4gOiBcImNvbW1lbnRcIiwgLy8gY2xvc2luZyBjb21tZW50XG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIlxcXFwqXFxcXC9cIixcbiAgICAgICAgICAgICAgICBuZXh0IDogXCJzdGFydFwiXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgZGVmYXVsdFRva2VuIDogXCJjb21tZW50XCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgIH07XG5cbiAgICBcbiAgICB0aGlzLmVtYmVkUnVsZXMoRG9jQ29tbWVudEhpZ2hsaWdodFJ1bGVzLCBcImRvYy1cIixcbiAgICAgICAgWyBEb2NDb21tZW50SGlnaGxpZ2h0UnVsZXMuZ2V0RW5kUnVsZShcInN0YXJ0XCIpIF0pO1xuICAgIHRoaXMubm9ybWFsaXplUnVsZXMoKTtcbn07XG5cbm9vcC5pbmhlcml0cyhKYXZhSGlnaGxpZ2h0UnVsZXMsIFRleHRIaWdobGlnaHRSdWxlcyk7XG5cbmV4cG9ydHMuSmF2YUhpZ2hsaWdodFJ1bGVzID0gSmF2YUhpZ2hsaWdodFJ1bGVzO1xufSk7XG5cbmFjZS5kZWZpbmUoXCJhY2UvbW9kZS9kcm9vbHNfaGlnaGxpZ2h0X3J1bGVzXCIsW1wicmVxdWlyZVwiLFwiZXhwb3J0c1wiLFwibW9kdWxlXCIsXCJhY2UvbGliL29vcFwiLFwiYWNlL21vZGUvdGV4dF9oaWdobGlnaHRfcnVsZXNcIixcImFjZS9tb2RlL2phdmFfaGlnaGxpZ2h0X3J1bGVzXCIsXCJhY2UvbW9kZS9kb2NfY29tbWVudF9oaWdobGlnaHRfcnVsZXNcIl0sIGZ1bmN0aW9uKHJlcXVpcmUsIGV4cG9ydHMsIG1vZHVsZSkge1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBvb3AgPSByZXF1aXJlKFwiLi4vbGliL29vcFwiKTtcbnZhciBUZXh0SGlnaGxpZ2h0UnVsZXMgPSByZXF1aXJlKFwiLi90ZXh0X2hpZ2hsaWdodF9ydWxlc1wiKS5UZXh0SGlnaGxpZ2h0UnVsZXM7XG52YXIgSmF2YUhpZ2hsaWdodFJ1bGVzID0gcmVxdWlyZShcIi4vamF2YV9oaWdobGlnaHRfcnVsZXNcIikuSmF2YUhpZ2hsaWdodFJ1bGVzO1xudmFyIERvY0NvbW1lbnRIaWdobGlnaHRSdWxlcyA9IHJlcXVpcmUoXCIuL2RvY19jb21tZW50X2hpZ2hsaWdodF9ydWxlc1wiKS5Eb2NDb21tZW50SGlnaGxpZ2h0UnVsZXM7XG5cbnZhciBpZGVudGlmaWVyUmUgPSBcIlthLXpBLVpcXFxcJF9cXHUwMGExLVxcdWZmZmZdW2EtekEtWlxcXFxkXFxcXCRfXFx1MDBhMS1cXHVmZmZmXSpcIjtcbnZhciBwYWNrYWdlSWRlbnRpZmllclJlID0gXCJbYS16QS1aXFxcXCRfXFx1MDBhMS1cXHVmZmZmXVtcXFxcLmEtekEtWlxcXFxkXFxcXCRfXFx1MDBhMS1cXHVmZmZmXSpcIjtcblxudmFyIERyb29sc0hpZ2hsaWdodFJ1bGVzID0gZnVuY3Rpb24oKSB7XG5cbiAgICB2YXIga2V5d29yZHMgPSAoXCJkYXRlfGVmZmVjdGl2ZXxleHBpcmVzfGxvY2t8b258YWN0aXZlfG5vfGxvb3B8YXV0b3xmb2N1c1wiICtcbiAgICAgICAgXCJ8YWN0aXZhdGlvbnxncm91cHxhZ2VuZGF8cnVsZWZsb3d8ZHVyYXRpb258dGltZXJ8Y2FsZW5kYXJzfHJlZnJhY3R8ZGlyZWN0XCIgK1xuICAgICAgICBcInxkaWFsZWN0fHNhbGllbmNlfGVuYWJsZWR8YXR0cmlidXRlc3xleHRlbmRzfHRlbXBsYXRlXCIgK1xuICAgICAgICBcInxmdW5jdGlvbnxjb250YWluc3xtYXRjaGVzfGV2YWx8ZXhjbHVkZXN8c291bmRzbGlrZVwiICtcbiAgICAgICAgXCJ8bWVtYmVyb2Z8bm90fGlufG9yfGFuZHxleGlzdHN8Zm9yYWxsfG92ZXJ8ZnJvbXxlbnRyeXxwb2ludHxhY2N1bXVsYXRlfGFjY3xjb2xsZWN0XCIgK1xuICAgICAgICBcInxhY3Rpb258cmV2ZXJzZXxyZXN1bHR8ZW5kfGluaXR8aW5zdGFuY2VvZnxleHRlbmRzfHN1cGVyfGJvb2xlYW58Y2hhcnxieXRlfHNob3J0XCIgK1xuICAgICAgICBcInxpbnR8bG9uZ3xmbG9hdHxkb3VibGV8dGhpc3x2b2lkfGNsYXNzfG5ld3xjYXNlfGZpbmFsfGlmfGVsc2V8Zm9yfHdoaWxlfGRvXCIgK1xuICAgICAgICBcInxkZWZhdWx0fHRyeXxjYXRjaHxmaW5hbGx5fHN3aXRjaHxzeW5jaHJvbml6ZWR8cmV0dXJufHRocm93fGJyZWFrfGNvbnRpbnVlfGFzc2VydFwiICtcbiAgICAgICAgXCJ8bW9kaWZ5fHN0YXRpY3xwdWJsaWN8cHJvdGVjdGVkfHByaXZhdGV8YWJzdHJhY3R8bmF0aXZlfHRyYW5zaWVudHx2b2xhdGlsZVwiICtcbiAgICAgICAgXCJ8c3RyaWN0ZnB8dGhyb3dzfGludGVyZmFjZXxlbnVtfGltcGxlbWVudHN8dHlwZXx3aW5kb3d8dHJhaXR8bm8tbG9vcHxzdHJcIlxuICAgICAgKTtcblxuICAgICAgdmFyIGxhbmdDbGFzc2VzID0gKFxuICAgICAgICAgIFwiQWJzdHJhY3RNZXRob2RFcnJvcnxBc3NlcnRpb25FcnJvcnxDbGFzc0NpcmN1bGFyaXR5RXJyb3J8XCIrXG4gICAgICAgICAgXCJDbGFzc0Zvcm1hdEVycm9yfERlcHJlY2F0ZWR8RW51bUNvbnN0YW50Tm90UHJlc2VudEV4Y2VwdGlvbnxcIitcbiAgICAgICAgICBcIkV4Y2VwdGlvbkluSW5pdGlhbGl6ZXJFcnJvcnxJbGxlZ2FsQWNjZXNzRXJyb3J8XCIrXG4gICAgICAgICAgXCJJbGxlZ2FsVGhyZWFkU3RhdGVFeGNlcHRpb258SW5zdGFudGlhdGlvbkVycm9yfEludGVybmFsRXJyb3J8XCIrXG4gICAgICAgICAgXCJOZWdhdGl2ZUFycmF5U2l6ZUV4Y2VwdGlvbnxOb1N1Y2hGaWVsZEVycm9yfE92ZXJyaWRlfFByb2Nlc3N8XCIrXG4gICAgICAgICAgXCJQcm9jZXNzQnVpbGRlcnxTZWN1cml0eU1hbmFnZXJ8U3RyaW5nSW5kZXhPdXRPZkJvdW5kc0V4Y2VwdGlvbnxcIitcbiAgICAgICAgICBcIlN1cHByZXNzV2FybmluZ3N8VHlwZU5vdFByZXNlbnRFeGNlcHRpb258VW5rbm93bkVycm9yfFwiK1xuICAgICAgICAgIFwiVW5zYXRpc2ZpZWRMaW5rRXJyb3J8VW5zdXBwb3J0ZWRDbGFzc1ZlcnNpb25FcnJvcnxWZXJpZnlFcnJvcnxcIitcbiAgICAgICAgICBcIkluc3RhbnRpYXRpb25FeGNlcHRpb258SW5kZXhPdXRPZkJvdW5kc0V4Y2VwdGlvbnxcIitcbiAgICAgICAgICBcIkFycmF5SW5kZXhPdXRPZkJvdW5kc0V4Y2VwdGlvbnxDbG9uZU5vdFN1cHBvcnRlZEV4Y2VwdGlvbnxcIitcbiAgICAgICAgICBcIk5vU3VjaEZpZWxkRXhjZXB0aW9ufElsbGVnYWxBcmd1bWVudEV4Y2VwdGlvbnxOdW1iZXJGb3JtYXRFeGNlcHRpb258XCIrXG4gICAgICAgICAgXCJTZWN1cml0eUV4Y2VwdGlvbnxWb2lkfEluaGVyaXRhYmxlVGhyZWFkTG9jYWx8SWxsZWdhbFN0YXRlRXhjZXB0aW9ufFwiK1xuICAgICAgICAgIFwiSW50ZXJydXB0ZWRFeGNlcHRpb258Tm9TdWNoTWV0aG9kRXhjZXB0aW9ufElsbGVnYWxBY2Nlc3NFeGNlcHRpb258XCIrXG4gICAgICAgICAgXCJVbnN1cHBvcnRlZE9wZXJhdGlvbkV4Y2VwdGlvbnxFbnVtfFN0cmljdE1hdGh8UGFja2FnZXxDb21waWxlcnxcIitcbiAgICAgICAgICBcIlJlYWRhYmxlfFJ1bnRpbWV8U3RyaW5nQnVpbGRlcnxNYXRofEluY29tcGF0aWJsZUNsYXNzQ2hhbmdlRXJyb3J8XCIrXG4gICAgICAgICAgXCJOb1N1Y2hNZXRob2RFcnJvcnxUaHJlYWRMb2NhbHxSdW50aW1lUGVybWlzc2lvbnxBcml0aG1ldGljRXhjZXB0aW9ufFwiK1xuICAgICAgICAgIFwiTnVsbFBvaW50ZXJFeGNlcHRpb258TG9uZ3xJbnRlZ2VyfFNob3J0fEJ5dGV8RG91YmxlfE51bWJlcnxGbG9hdHxcIitcbiAgICAgICAgICBcIkNoYXJhY3RlcnxCb29sZWFufFN0YWNrVHJhY2VFbGVtZW50fEFwcGVuZGFibGV8U3RyaW5nQnVmZmVyfFwiK1xuICAgICAgICAgIFwiSXRlcmFibGV8VGhyZWFkR3JvdXB8UnVubmFibGV8VGhyZWFkfElsbGVnYWxNb25pdG9yU3RhdGVFeGNlcHRpb258XCIrXG4gICAgICAgICAgXCJTdGFja092ZXJmbG93RXJyb3J8T3V0T2ZNZW1vcnlFcnJvcnxWaXJ0dWFsTWFjaGluZUVycm9yfFwiK1xuICAgICAgICAgIFwiQXJyYXlTdG9yZUV4Y2VwdGlvbnxDbGFzc0Nhc3RFeGNlcHRpb258TGlua2FnZUVycm9yfFwiK1xuICAgICAgICAgIFwiTm9DbGFzc0RlZkZvdW5kRXJyb3J8Q2xhc3NOb3RGb3VuZEV4Y2VwdGlvbnxSdW50aW1lRXhjZXB0aW9ufFwiK1xuICAgICAgICAgIFwiRXhjZXB0aW9ufFRocmVhZERlYXRofEVycm9yfFRocm93YWJsZXxTeXN0ZW18Q2xhc3NMb2FkZXJ8XCIrXG4gICAgICAgICAgXCJDbG9uZWFibGV8Q2xhc3N8Q2hhclNlcXVlbmNlfENvbXBhcmFibGV8U3RyaW5nfE9iamVjdFwiXG4gICAgICApO1xuXG4gICAgdmFyIGtleXdvcmRNYXBwZXIgPSB0aGlzLmNyZWF0ZUtleXdvcmRNYXBwZXIoe1xuICAgICAgICBcInZhcmlhYmxlLmxhbmd1YWdlXCI6IFwidGhpc1wiLFxuICAgICAgICBcImtleXdvcmRcIjoga2V5d29yZHMsXG4gICAgICAgIFwiY29uc3RhbnQubGFuZ3VhZ2VcIjogXCJudWxsXCIsXG4gICAgICAgIFwic3VwcG9ydC5jbGFzc1wiIDogbGFuZ0NsYXNzZXMsXG4gICAgICAgIFwic3VwcG9ydC5mdW5jdGlvblwiIDogXCJyZXRyYWN0fHVwZGF0ZXxtb2RpZnl8aW5zZXJ0XCJcbiAgICB9LCBcImlkZW50aWZpZXJcIik7XG5cbiAgICB2YXIgc3RyaW5nUnVsZXMgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBbe1xuICAgICAgICB0b2tlbiA6IFwic3RyaW5nXCIsIC8vIHNpbmdsZSBsaW5lXG4gICAgICAgIHJlZ2V4IDogJ1tcIl0oPzooPzpcXFxcXFxcXC4pfCg/OlteXCJcXFxcXFxcXF0pKSo/W1wiXSdcbiAgICAgIH0sIHtcbiAgICAgICAgdG9rZW4gOiBcInN0cmluZ1wiLCAvLyBzaW5nbGUgbGluZVxuICAgICAgICByZWdleCA6IFwiWyddKD86KD86XFxcXFxcXFwuKXwoPzpbXidcXFxcXFxcXF0pKSo/WyddXCJcbiAgICAgIH1dO1xuICAgIH07XG5cblxuICAgICAgdmFyIGJhc2ljUHJlUnVsZXMgPSBmdW5jdGlvbihibG9ja0NvbW1lbnRSdWxlcykge1xuICAgICAgICByZXR1cm4gW3tcbiAgICAgICAgICAgIHRva2VuIDogXCJjb21tZW50XCIsXG4gICAgICAgICAgICByZWdleCA6IFwiXFxcXC9cXFxcLy4qJFwiXG4gICAgICAgIH0sXG4gICAgICAgIERvY0NvbW1lbnRIaWdobGlnaHRSdWxlcy5nZXRTdGFydFJ1bGUoXCJkb2Mtc3RhcnRcIiksXG4gICAgICAgIHtcbiAgICAgICAgICAgIHRva2VuIDogXCJjb21tZW50XCIsIC8vIG11bHRpIGxpbmUgY29tbWVudFxuICAgICAgICAgICAgcmVnZXggOiBcIlxcXFwvXFxcXCpcIixcbiAgICAgICAgICAgIG5leHQgOiBibG9ja0NvbW1lbnRSdWxlc1xuICAgICAgICB9LCB7XG4gICAgICAgICAgICB0b2tlbiA6IFwiY29uc3RhbnQubnVtZXJpY1wiLCAvLyBoZXhcbiAgICAgICAgICAgIHJlZ2V4IDogXCIwW3hYXVswLTlhLWZBLUZdK1xcXFxiXCJcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgdG9rZW4gOiBcImNvbnN0YW50Lm51bWVyaWNcIiwgLy8gZmxvYXRcbiAgICAgICAgICAgIHJlZ2V4IDogXCJbKy1dP1xcXFxkKyg/Oig/OlxcXFwuXFxcXGQqKT8oPzpbZUVdWystXT9cXFxcZCspPyk/XFxcXGJcIlxuICAgICAgICB9LCB7XG4gICAgICAgICAgICB0b2tlbiA6IFwiY29uc3RhbnQubGFuZ3VhZ2UuYm9vbGVhblwiLFxuICAgICAgICAgICAgcmVnZXggOiBcIig/OnRydWV8ZmFsc2UpXFxcXGJcIlxuICAgICAgICAgIH1dO1xuICAgICAgfTtcblxuICAgICAgdmFyIGJsb2NrQ29tbWVudFJ1bGVzID0gZnVuY3Rpb24ocmV0dXJuUnVsZSkge1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRva2VuIDogXCJjb21tZW50LmJsb2NrXCIsIC8vIGNsb3NpbmcgY29tbWVudFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCJcXFxcKlxcXFwvXCIsXG4gICAgICAgICAgICAgICAgbmV4dCA6IHJldHVyblJ1bGVcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICBkZWZhdWx0VG9rZW4gOiBcImNvbW1lbnQuYmxvY2tcIlxuICAgICAgICAgICAgfVxuICAgICAgICBdO1xuICAgICAgfTtcblxuICAgICAgdmFyIGJhc2ljUG9zdFJ1bGVzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiBbe1xuICAgICAgICAgICAgdG9rZW4gOiBrZXl3b3JkTWFwcGVyLFxuICAgICAgICAgICAgcmVnZXggOiBcIlthLXpBLVpfJF1bYS16QS1aMC05XyRdKlxcXFxiXCJcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgdG9rZW4gOiBcImtleXdvcmQub3BlcmF0b3JcIixcbiAgICAgICAgICAgIHJlZ2V4IDogXCIhfFxcXFwkfCV8JnxcXFxcKnxcXFxcLVxcXFwtfFxcXFwtfFxcXFwrXFxcXCt8XFxcXCt8fnw9PT18PT18PXwhPXwhPT18PD18Pj18PDw9fD4+PXw+Pj49fDw+fDx8PnwhfCYmfFxcXFx8XFxcXHx8XFxcXD9cXFxcOnxcXFxcKj18JT18XFxcXCs9fFxcXFwtPXwmPXxcXFxcXj18XFxcXGIoPzppbnxpbnN0YW5jZW9mfG5ld3xkZWxldGV8dHlwZW9mfHZvaWQpXCJcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgdG9rZW4gOiBcImxwYXJlblwiLFxuICAgICAgICAgICAgcmVnZXggOiBcIltbKHtdXCJcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgdG9rZW4gOiBcInJwYXJlblwiLFxuICAgICAgICAgICAgcmVnZXggOiBcIltcXFxcXSl9XVwiXG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIHRva2VuIDogXCJ0ZXh0XCIsXG4gICAgICAgICAgICByZWdleCA6IFwiXFxcXHMrXCJcbiAgICAgICAgfV07XG4gICAgICB9O1xuXG5cbiAgICB0aGlzLiRydWxlcyA9IHtcbiAgICAgICAgXCJzdGFydFwiIDogW10uY29uY2F0KGJhc2ljUHJlUnVsZXMoXCJibG9jay5jb21tZW50XCIpLCBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwiZW50aXR5Lm5hbWUudHlwZVwiLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCJAW2EtekEtWl8kXVthLXpBLVowLTlfJF0qXFxcXGJcIlxuICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgdG9rZW4gOiBbXCJrZXl3b3JkXCIsXCJ0ZXh0XCIsXCJlbnRpdHkubmFtZS50eXBlXCJdLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCIocGFja2FnZSkoXFxcXHMrKShcIiArIHBhY2thZ2VJZGVudGlmaWVyUmUgK1wiKVwiXG4gICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFtcImtleXdvcmRcIixcInRleHRcIixcImtleXdvcmRcIixcInRleHRcIixcImVudGl0eS5uYW1lLnR5cGVcIl0sXG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIihpbXBvcnQpKFxcXFxzKykoZnVuY3Rpb24pKFxcXFxzKykoXCIgKyBwYWNrYWdlSWRlbnRpZmllclJlICtcIilcIlxuICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgdG9rZW4gOiBbXCJrZXl3b3JkXCIsXCJ0ZXh0XCIsXCJlbnRpdHkubmFtZS50eXBlXCJdLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCIoaW1wb3J0KShcXFxccyspKFwiICsgcGFja2FnZUlkZW50aWZpZXJSZSArXCIpXCJcbiAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgIHRva2VuIDogW1wia2V5d29yZFwiLFwidGV4dFwiLFwiZW50aXR5Lm5hbWUudHlwZVwiLFwidGV4dFwiLFwidmFyaWFibGVcIl0sXG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIihnbG9iYWwpKFxcXFxzKykoXCIgKyBwYWNrYWdlSWRlbnRpZmllclJlICtcIikoXFxcXHMrKShcIiArIGlkZW50aWZpZXJSZSArXCIpXCJcbiAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgIHRva2VuIDogW1wia2V5d29yZFwiLFwidGV4dFwiLFwia2V5d29yZFwiLFwidGV4dFwiLFwiZW50aXR5Lm5hbWUudHlwZVwiXSxcbiAgICAgICAgICAgICAgICByZWdleCA6IFwiKGRlY2xhcmUpKFxcXFxzKykodHJhaXQpKFxcXFxzKykoXCIgKyBpZGVudGlmaWVyUmUgK1wiKVwiXG4gICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFtcImtleXdvcmRcIixcInRleHRcIixcImVudGl0eS5uYW1lLnR5cGVcIl0sXG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIihkZWNsYXJlKShcXFxccyspKFwiICsgaWRlbnRpZmllclJlICtcIilcIlxuICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgdG9rZW4gOiBbXCJrZXl3b3JkXCIsXCJ0ZXh0XCIsXCJlbnRpdHkubmFtZS50eXBlXCJdLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCIoZXh0ZW5kcykoXFxcXHMrKShcIiArIHBhY2thZ2VJZGVudGlmaWVyUmUgK1wiKVwiXG4gICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFtcImtleXdvcmRcIixcInRleHRcIl0sXG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIihydWxlKShcXFxccyspXCIsXG4gICAgICAgICAgICAgICAgbmV4dCA6ICBcImFzc2V0Lm5hbWVcIlxuICAgICAgICAgICAgICB9XSxcbiAgICAgICAgICAgICAgc3RyaW5nUnVsZXMoKSxcbiAgICAgICAgICAgICAgW3tcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFtcInZhcmlhYmxlLm90aGVyXCIsXCJ0ZXh0XCIsXCJ0ZXh0XCJdLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCIoXCIgKyBpZGVudGlmaWVyUmUgKyBcIikoXFxcXHMqKSg6KVwiXG4gICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFtcImtleXdvcmRcIixcInRleHRcIl0sXG4gICAgICAgICAgICAgICAgcmVnZXggOiBcIihxdWVyeSkoXFxcXHMrKVwiLFxuICAgICAgICAgICAgICAgIG5leHQgOiAgXCJhc3NldC5uYW1lXCJcbiAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgIHRva2VuIDogW1wia2V5d29yZFwiLFwidGV4dFwiXSxcbiAgICAgICAgICAgICAgICByZWdleCA6IFwiKHdoZW4pKFxcXFxzKilcIlxuICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgdG9rZW4gOiBbXCJrZXl3b3JkXCIsXCJ0ZXh0XCJdLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogXCIodGhlbikoXFxcXHMqKVwiLFxuICAgICAgICAgICAgICAgIG5leHQgOiAgXCJqYXZhLXN0YXJ0XCJcbiAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgICAgdG9rZW4gOiBcInBhcmVuLmxwYXJlblwiLFxuICAgICAgICAgICAgICAgICAgcmVnZXggOiAvW1xcWyh7XS9cbiAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgICAgdG9rZW4gOiBcInBhcmVuLnJwYXJlblwiLFxuICAgICAgICAgICAgICAgICAgcmVnZXggOiAvW1xcXSl9XS9cbiAgICAgICAgICAgICAgfV0sIGJhc2ljUG9zdFJ1bGVzKCkpLFxuICAgICAgICBcImJsb2NrLmNvbW1lbnRcIiA6IGJsb2NrQ29tbWVudFJ1bGVzKFwic3RhcnRcIiksXG4gICAgICAgIFwiYXNzZXQubmFtZVwiIDogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRva2VuIDogXCJlbnRpdHkubmFtZVwiLFxuICAgICAgICAgICAgICAgIHJlZ2V4IDogJ1tcIl0oPzooPzpcXFxcXFxcXC4pfCg/OlteXCJcXFxcXFxcXF0pKSo/W1wiXSdcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwiZW50aXR5Lm5hbWVcIixcbiAgICAgICAgICAgICAgICByZWdleCA6IFwiWyddKD86KD86XFxcXFxcXFwuKXwoPzpbXidcXFxcXFxcXF0pKSo/WyddXCJcbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICB0b2tlbiA6IFwiZW50aXR5Lm5hbWVcIixcbiAgICAgICAgICAgICAgICByZWdleCA6IGlkZW50aWZpZXJSZVxuICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgIHJlZ2V4OiBcIlwiLFxuICAgICAgICAgICAgICAgIHRva2VuOiBcImVtcHR5XCIsXG4gICAgICAgICAgICAgICAgbmV4dDogXCJzdGFydFwiXG4gICAgICAgICAgICB9XVxuICAgIH07XG4gICAgdGhpcy5lbWJlZFJ1bGVzKERvY0NvbW1lbnRIaWdobGlnaHRSdWxlcywgXCJkb2MtXCIsXG4gICAgICAgIFsgRG9jQ29tbWVudEhpZ2hsaWdodFJ1bGVzLmdldEVuZFJ1bGUoXCJzdGFydFwiKSBdKTtcblxuICAgIHRoaXMuZW1iZWRSdWxlcyhKYXZhSGlnaGxpZ2h0UnVsZXMsIFwiamF2YS1cIiwgW1xuICAgICAge1xuICAgICAgIHRva2VuIDogXCJzdXBwb3J0LmZ1bmN0aW9uXCIsXG4gICAgICAgcmVnZXg6IFwiXFxcXGIoaW5zZXJ0fG1vZGlmeXxyZXRyYWN0fHVwZGF0ZSlcXFxcYlwiXG4gICAgIH0sIHtcbiAgICAgICB0b2tlbiA6IFwia2V5d29yZFwiLFxuICAgICAgIHJlZ2V4OiBcIlxcXFxiZW5kXFxcXGJcIixcbiAgICAgICBuZXh0ICA6IFwic3RhcnRcIlxuICAgIH1dKTtcblxufTtcblxub29wLmluaGVyaXRzKERyb29sc0hpZ2hsaWdodFJ1bGVzLCBUZXh0SGlnaGxpZ2h0UnVsZXMpO1xuXG5leHBvcnRzLkRyb29sc0hpZ2hsaWdodFJ1bGVzID0gRHJvb2xzSGlnaGxpZ2h0UnVsZXM7XG59KTtcblxuYWNlLmRlZmluZShcImFjZS9tb2RlL2ZvbGRpbmcvZHJvb2xzXCIsW1wicmVxdWlyZVwiLFwiZXhwb3J0c1wiLFwibW9kdWxlXCIsXCJhY2UvbGliL29vcFwiLFwiYWNlL3JhbmdlXCIsXCJhY2UvbW9kZS9mb2xkaW5nL2ZvbGRfbW9kZVwiLFwiYWNlL3Rva2VuX2l0ZXJhdG9yXCJdLCBmdW5jdGlvbihyZXF1aXJlLCBleHBvcnRzLCBtb2R1bGUpIHtcblwidXNlIHN0cmljdFwiO1xuXG52YXIgb29wID0gcmVxdWlyZShcIi4uLy4uL2xpYi9vb3BcIik7XG52YXIgUmFuZ2UgPSByZXF1aXJlKFwiLi4vLi4vcmFuZ2VcIikuUmFuZ2U7XG52YXIgQmFzZUZvbGRNb2RlID0gcmVxdWlyZShcIi4vZm9sZF9tb2RlXCIpLkZvbGRNb2RlO1xudmFyIFRva2VuSXRlcmF0b3IgPSByZXF1aXJlKFwiLi4vLi4vdG9rZW5faXRlcmF0b3JcIikuVG9rZW5JdGVyYXRvcjtcblxudmFyIEZvbGRNb2RlID0gZXhwb3J0cy5Gb2xkTW9kZSA9IGZ1bmN0aW9uKCkge307XG5vb3AuaW5oZXJpdHMoRm9sZE1vZGUsIEJhc2VGb2xkTW9kZSk7XG5cbihmdW5jdGlvbigpIHtcbiAgICB0aGlzLmZvbGRpbmdTdGFydE1hcmtlciA9IC9cXGIocnVsZXxkZWNsYXJlfHF1ZXJ5fHdoZW58dGhlbilcXGIvOyBcbiAgICB0aGlzLmZvbGRpbmdTdG9wTWFya2VyID0gL1xcYmVuZFxcYi87XG5cbiAgICB0aGlzLmdldEZvbGRXaWRnZXRSYW5nZSA9IGZ1bmN0aW9uKHNlc3Npb24sIGZvbGRTdHlsZSwgcm93KSB7XG4gICAgICAgIHZhciBsaW5lID0gc2Vzc2lvbi5nZXRMaW5lKHJvdyk7XG4gICAgICAgIHZhciBtYXRjaCA9IGxpbmUubWF0Y2godGhpcy5mb2xkaW5nU3RhcnRNYXJrZXIpO1xuICAgICAgICBpZiAobWF0Y2gpIHtcbiAgICAgICAgICAgIHZhciBpID0gbWF0Y2guaW5kZXg7XG5cbiAgICAgICAgICAgIGlmIChtYXRjaFsxXSkge1xuICAgICAgICAgICAgICAgIHZhciBwb3NpdGlvbiA9IHtyb3c6IHJvdywgY29sdW1uOiBsaW5lLmxlbmd0aH07XG4gICAgICAgICAgICAgICAgdmFyIGl0ZXJhdG9yID0gbmV3IFRva2VuSXRlcmF0b3Ioc2Vzc2lvbiwgcG9zaXRpb24ucm93LCBwb3NpdGlvbi5jb2x1bW4pO1xuICAgICAgICAgICAgICAgIHZhciBzZWVrID0gXCJlbmRcIjtcbiAgICAgICAgICAgICAgICB2YXIgdG9rZW4gPSBpdGVyYXRvci5nZXRDdXJyZW50VG9rZW4oKTtcbiAgICAgICAgICAgICAgICBpZiAodG9rZW4udmFsdWUgPT0gXCJ3aGVuXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgc2VlayA9IFwidGhlblwiO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB3aGlsZSAodG9rZW4pIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRva2VuLnZhbHVlID09IHNlZWspIHsgXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gUmFuZ2UuZnJvbVBvaW50cyhwb3NpdGlvbiAse1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogaXRlcmF0b3IuZ2V0Q3VycmVudFRva2VuUm93KCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sdW1uOiBpdGVyYXRvci5nZXRDdXJyZW50VG9rZW5Db2x1bW4oKVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdG9rZW4gPSBpdGVyYXRvci5zdGVwRm9yd2FyZCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG4gICAgfTtcblxufSkuY2FsbChGb2xkTW9kZS5wcm90b3R5cGUpO1xuXG59KTtcblxuYWNlLmRlZmluZShcImFjZS9tb2RlL2Ryb29sc1wiLFtcInJlcXVpcmVcIixcImV4cG9ydHNcIixcIm1vZHVsZVwiLFwiYWNlL2xpYi9vb3BcIixcImFjZS9tb2RlL3RleHRcIixcImFjZS9tb2RlL2Ryb29sc19oaWdobGlnaHRfcnVsZXNcIixcImFjZS9tb2RlL2ZvbGRpbmcvZHJvb2xzXCJdLCBmdW5jdGlvbihyZXF1aXJlLCBleHBvcnRzLCBtb2R1bGUpIHtcblwidXNlIHN0cmljdFwiO1xuXG52YXIgb29wID0gcmVxdWlyZShcIi4uL2xpYi9vb3BcIik7XG52YXIgVGV4dE1vZGUgPSByZXF1aXJlKFwiLi90ZXh0XCIpLk1vZGU7XG52YXIgRHJvb2xzSGlnaGxpZ2h0UnVsZXMgPSByZXF1aXJlKFwiLi9kcm9vbHNfaGlnaGxpZ2h0X3J1bGVzXCIpLkRyb29sc0hpZ2hsaWdodFJ1bGVzO1xudmFyIERyb29sc0ZvbGRNb2RlID0gcmVxdWlyZShcIi4vZm9sZGluZy9kcm9vbHNcIikuRm9sZE1vZGU7XG5cbnZhciBNb2RlID0gZnVuY3Rpb24oKSB7XG4gICAgdGhpcy5IaWdobGlnaHRSdWxlcyA9IERyb29sc0hpZ2hsaWdodFJ1bGVzO1xuICAgIHRoaXMuZm9sZGluZ1J1bGVzID0gbmV3IERyb29sc0ZvbGRNb2RlKCk7XG4gICAgdGhpcy4kYmVoYXZpb3VyID0gdGhpcy4kZGVmYXVsdEJlaGF2aW91cjtcblxufTtcbm9vcC5pbmhlcml0cyhNb2RlLCBUZXh0TW9kZSk7XG5cbihmdW5jdGlvbigpIHtcbiAgICB0aGlzLmxpbmVDb21tZW50U3RhcnQgPSBcIi8vXCI7XG4gICAgdGhpcy4kaWQgPSBcImFjZS9tb2RlL2Ryb29sc1wiO1xuICAgIHRoaXMuc25pcHBldEZpbGVJZCA9IFwiYWNlL3NuaXBwZXRzL2Ryb29sc1wiO1xufSkuY2FsbChNb2RlLnByb3RvdHlwZSk7XG5cbmV4cG9ydHMuTW9kZSA9IE1vZGU7XG5cbn0pOyAgICAgICAgICAgICAgICAoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIGFjZS5yZXF1aXJlKFtcImFjZS9tb2RlL2Ryb29sc1wiXSwgZnVuY3Rpb24obSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBtb2R1bGUgPT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgZXhwb3J0cyA9PSBcIm9iamVjdFwiICYmIG1vZHVsZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZHVsZS5leHBvcnRzID0gbTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSkoKTtcbiAgICAgICAgICAgICJdLCJzb3VyY2VSb290IjoiIn0=