// -----------------------------------------------------------------------------------------------------------
//   Configuration for the connection to the API
// -----------------------------------------------------------------------------------------------------------

// Object to fold all config information
const API_CONFIG = {
    HOST: "127.0.0.1",
    PORT: 8000,
    BASE_URI: "",
    COMPLETE_URI: "", // for production build: set value to "/cameri/api"
  };

// Function to build the API base url
function APIRoute(){
    if (API_CONFIG.COMPLETE_URI != "") {
      return API_CONFIG.COMPLETE_URI
    } 
    return `http://${API_CONFIG.HOST}:${API_CONFIG.PORT}${API_CONFIG.BASE_URI}`
}


export { APIRoute };