<template>
  <teleport to="#app">
    <div v-if="show" class="backdrop" @click="tryClose"></div>
    <transition name="dialog">
      <div class="dialog-wrapper" v-if="show">
        <div class="dialog-container">
          <div class="dialog-header">
            <slot name="header">
              <h2>{{ title }}</h2>
            </slot>
          </div>
          <div class="dialog-body">
            <slot></slot>
          </div>
          <div class="dialog-footer">
            <slot name="actions">
              <slot name="leftActions"><div></div></slot>
              <base-button icon="close" @click="tryClose">Close</base-button>
            </slot>
          </div>
        </div>
      </div>
    </transition>
  </teleport>
</template>

<script>
export default {
  props: {
    show: {
      type: Boolean,
      required: true,
    },
    title: {
      type: String,
      required: false,
    },
  },
  emits: ['close'],
  methods: {
    tryClose() {
      this.$emit('close');
    },
  },
};
</script>

<style scoped>
.backdrop {
  position: fixed;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.75);
  z-index: 80;
}

.dialog-wrapper {
  pointer-events:none;
  position: fixed;
  display: flex;
  flex-direction: column;
  align-items: center;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
  background-color: transparent;
  z-index: 100;
}
.dialog-container {
  pointer-events:all;
  margin-top: 10vh;
  max-width: 80vw;
  max-height: 78vh;
  border-radius: 16px;
  border: none;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.26);
  display: flex;
  flex-direction: column;
  overflow: hidden;
  background-color: transparent;
}

.dialog-header {
  width: 100%;
  padding: 1rem;
  background-color: var(--cameri-primary);
  color: white;
}

.dialog-body {
  padding: 1rem;
  background-color: white;
  overflow-x: auto;
  overflow-y: auto;
}

.dialog-footer {
  padding: 1rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 0;
  background-color: white;
}

.dialog-enter-from,
.dialog-leave-to {
  opacity: 0;
  transform: scale(0.8);
}

.dialog-enter-active {
  transition: all 0.3s ease-out;
}

.dialog-leave-active {
  transition: all 0.3s ease-in;
}

.dialog-enter-to,
.dialog-leave-from {
  opacity: 1;
  transform: scale(1);
}
</style>
