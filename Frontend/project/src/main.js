// -----------------------------------------------------------------------------------------------------------
//   Definition of the vue js app
// -----------------------------------------------------------------------------------------------------------

import { createApp } from 'vue';

import router from './router.js';
import store from './store/index.js';
import App from './App.vue';
import BaseButton from './components/ui/BaseButton.vue';
import BaseIcon from './components/ui/BaseIcon.vue';
import BaseTooltip from './components/ui/BaseTooltip.vue';
import BaseDropdown from './components/ui/BaseDropdown.vue';
import BaseDialog from './components/ui/BaseDialog.vue';
import BaseSpinner from './components/ui/BaseSpinner.vue';

// Create instance of the vue js app to hold their configuration
const app = createApp(App)

// add the router to the app
app.use(router);

// add the vuex state management to the app
app.use(store);

// add the base components to the app (to make them globally available without import)
app.component('base-button', BaseButton);
app.component('base-icon', BaseIcon);
app.component('base-tooltip', BaseTooltip);
app.component('base-dialog', BaseDialog);
app.component('base-dropdown', BaseDropdown);
app.component('base-spinner', BaseSpinner);

// mount to app the the HTML element with id "app"
app.mount('#app');
