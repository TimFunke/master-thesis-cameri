// -----------------------------------------------------------------------------------------------------------
//   Definition of state management with vuex
// -----------------------------------------------------------------------------------------------------------

import { createStore } from 'vuex';

import queriesModule from './modules/queries/index.js';
import dataAnnotationsModule from './modules/data_annotations/index.js';
import knowledgeModule from './modules/knowledge/index.js';

// create store object and include all modules
const store = createStore({
  modules: {
    queries: queriesModule,
    dataAnnotations: dataAnnotationsModule,
    knowledge: knowledgeModule
  },
});

export default store;