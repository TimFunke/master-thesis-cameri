// -----------------------------------------------------------------------------------------------------------
//   Definition of getters for queries
// -----------------------------------------------------------------------------------------------------------

export default {
  query(state, _) {
    return state.query;
  },
  queryResults(state, _) {
    return state.queryResults;
  },
  queries(state, _) {
    return [...state.queries];
  },
  hasQueries(_, getters) {
    return getters.queries && getters.queries.length > 0;
  }
};