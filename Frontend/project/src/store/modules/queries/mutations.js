// -----------------------------------------------------------------------------------------------------------
//   Definition of store mutations for queries
// -----------------------------------------------------------------------------------------------------------

export default {
  setQueries(state, payload) {
    state.queries = payload;
  },
  updateQueryStatus(state, payload) {
    state.queries.forEach((query) => {
      if (query.id === payload.id) {
        query.status = payload.status;
      }
    });
  },
  setQuery(state, payload) {
    state.query = payload;
  },
  setQueryResults(state, payload) {
    state.queryResults = payload;
  },
  setFetchTimestamp(state) {
    state.lastFetch = new Date().getTime();
  },
};
