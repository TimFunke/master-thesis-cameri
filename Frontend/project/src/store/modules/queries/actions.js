// -----------------------------------------------------------------------------------------------------------
//   Definition of actions for queries
// -----------------------------------------------------------------------------------------------------------

import { getQueryStatusFromText } from '../../../logic/QueryStatus.js';
import { E_RECOMMENDATION_TYPE, getRecommendationTypeFromText, getRecommendationTypeText } from '../../../logic/RecommendationType.js';
import { getDataAnnotationStatusFromText } from '../../../logic/DataAnnotationStatus.js';
import { APIRoute } from '../../../config.js';

export default {
  // load data for a query by id
  async loadQuery(context, payload) {

    // make API call
    const response = await fetch(`${APIRoute()}/queries/${payload.id}`);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // create result object
    var query = {
      name: responseData.Name,
      datasetID: responseData.DatasetID,
      datasetName: responseData.DatasetName,
      dataAnnotationStatus: getDataAnnotationStatusFromText(responseData.DataAnnotationStatus),
      selectedKnowledge: responseData.Knowledge,
    }

    // commit result object to store
    context.commit('setQuery', query);
  },
  // load result data for a query by id
  async loadResults(context, payload) {

    // make API call
    const response = await fetch(`${APIRoute()}/queries/${payload.id}/results`);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // create result object
    var queryResults = {queryName: responseData.QueryName, resultGroups: []}
    for (let recTypeKey in E_RECOMMENDATION_TYPE){
      const recType = E_RECOMMENDATION_TYPE[recTypeKey]
      var resultGroupEntry = {name: getRecommendationTypeText(recType), entries: []}
      for (let resultIndex in responseData.Results){
        const result = responseData.Results[resultIndex]
        if(recType != getRecommendationTypeFromText(result.Type)){
          continue
        }
        var resultEntry = {
          methodName: result.MethodName,
          methodDescription: result.MethodDescription,
          preconditions: [],
        }
        for (let preconditionIndex in result.Preconditions){
          const precondition = result.Preconditions[preconditionIndex]
          var preconditionEntry = {
            preconditionName: precondition.PreconditionName,
            preconditionDescription: precondition.PreconditionDescription,
            harmedByColumns: [],
            preprocessingName: precondition.PreprocessingName,
            preprocessingDescription: precondition.PreprocessingDescription,
          }
          for (let harmedByColumnsIndex in precondition.HarmedByColumns){
            preconditionEntry.harmedByColumns.push(precondition.HarmedByColumns[harmedByColumnsIndex])
          }
          resultEntry.preconditions.push(preconditionEntry)
        }
        resultGroupEntry.entries.push(resultEntry)
      }
      queryResults.resultGroups.push(resultGroupEntry)
    }

    // commit result object to store
    context.commit('setQueryResults', queryResults);
  },
  // load list of all exisiting queries
  async loadQueries(context) {   

    // make API call 
    const response = await fetch(`${APIRoute()}/queries/`);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // create result object
    var queries = []
    responseData.forEach((entry) => {
      queries.push({
        id: entry.QueryID,
        name: entry.Name,
        status: getQueryStatusFromText(entry.Status),
      })
    })

    // commit result object to store
    context.commit('setQueries', queries);
  },
  // create a new query
  async createQuery(context, payload) {

    // create object with query data for api endpoint
    const createData = {
      Name: payload.data.name,
      DatasetID: payload.data.datasetID,
      Knowledge: payload.data.selectedKnowledge
    }

    // make API call
    const response = await fetch(`${APIRoute()}/queries/`, {
      method: 'POST', body: JSON.stringify(createData),
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // execute instantly if set
    if (payload.runInstantly){
      await context.dispatch('executeQuery', {id: responseData.QueryID})
    }
  },
  // edit a existing query
  async editQuery(_, payload) {

    // create object with query data for api endpoint
    const updateData = {
      Name: payload.name,
    }

    // make API call
    const response = await fetch(`${APIRoute()}/queries/${payload.id}`, {
      method: 'PATCH', body: JSON.stringify(updateData),
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }
  },
  // delete a query
  async deleteQuery(_, payload) {

    // make API call
    const response = await fetch(`${APIRoute()}/queries/${payload.id}`, {method: 'DELETE'});
    const responseData = await response.json();
    if (!response.ok || !('message' in responseData) || responseData.message != 'success') {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }
  },
  // execute a query
  async executeQuery(_, payload) {
    
    // make API call
    const response = await fetch(`${APIRoute()}/recommender_system/execute_query/${payload.id}`, {
      method: 'PUT'
    });
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }
  },
  // stop the execution of a query
  async cancelQueryExecution(_, payload) {

    // make API call
    const response = await fetch(`${APIRoute()}/recommender_system/cancel_execution/${payload.id}`, {
      method: 'PUT'
    });
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }
  },
  // update the status of the current query of the store
  async updateQueryStatus(context, payload) {

    // make API call
    const response = await fetch(`${APIRoute()}/recommender_system/query_status/${payload.id}`, {
      method: 'GET'
    });
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // create result object
    var query = {
      id: responseData.QueryID,
      status: getQueryStatusFromText(responseData.Status),
    }

    // commit result object to store
    context.commit('updateQueryStatus', query);
  }
};
