// -----------------------------------------------------------------------------------------------------------
//   Definition of store module for queries
// -----------------------------------------------------------------------------------------------------------

import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
  namespaced: true,
  state() {
    return {
      lastFetch: null,
      queries: [],
      query: null,
      queryResults: null,
    };
  },
  mutations,
  actions,
  getters,
};
