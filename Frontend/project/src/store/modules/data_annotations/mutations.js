// -----------------------------------------------------------------------------------------------------------
//   Definition of store mutations for data annotations
// -----------------------------------------------------------------------------------------------------------

export default {
  setDataAnnotationOverview(state, payload) {
    state.dataAnnotationOverview = payload;
  },
  setDataAnnotations(state, payload) {
    state.dataAnnotations = payload;
  },
  setDatasetPeek(state, payload) {
    state.datasetPeek = payload;
  },
};
