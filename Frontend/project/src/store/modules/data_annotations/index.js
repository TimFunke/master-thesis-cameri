// -----------------------------------------------------------------------------------------------------------
//   Definition of store module for data annotations
// -----------------------------------------------------------------------------------------------------------

import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
  namespaced: true,
  state() {
    return {
      dataAnnotationOverview: [],
      dataAnnotations: null,
      datasetPeek: null,
    };
  },
  mutations,
  actions,
  getters,
};
