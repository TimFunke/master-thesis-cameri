// -----------------------------------------------------------------------------------------------------------
//   Definition of actions for data annotations
// -----------------------------------------------------------------------------------------------------------

import { getDataAnnotationStatusFromText } from '../../../logic/DataAnnotationStatus.js';
import { getScaleTypeFromText, getDBTextFromScaleType } from '../../../logic/DataAnnotationScaleType.js';
import { APIRoute } from '../../../config.js';

export default {
  // load list of all exisiting datasets with their data annotation status
  async loadDataAnnotationOverview(context) {
    
    // make API call
    const response = await fetch(`${APIRoute()}/data_annotations/`);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // create result object
    var dataAnnotations = []
    responseData.forEach((entry) => {
      dataAnnotations.push({
        id: entry.DatasetID,
        datasetName: entry.DatasetName,
        status: getDataAnnotationStatusFromText(entry.DataAnnotationStatus),
      })
    })

    // commit result object to store
    context.commit('setDataAnnotationOverview', dataAnnotations);
  },
  // load data annotations for a single dataset
  async loadDataAnnotations(context, payload) {
    
    // make API call
    const response = await fetch(`${APIRoute()}/data_annotations/${payload.id}`);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // create result object
    var result = { datasetName: responseData.DatasetName, annotations: [] };
    responseData.Annotations.forEach((entry) => {
      result.annotations.push({
        columnIndex: entry.ColumnIndex,
        columnName: entry.ColumnName,
        scaleType: getScaleTypeFromText(entry.ScaleType),
        isTarget: entry.IsTarget,
        isIgnore: entry.IsIgnore,
      })
    })

    // commit result object to store
    context.commit('setDataAnnotations', result);
  },
  // edit data annotations for a dataset
  async editDataAnnotations(_, payload) {
    
    // create object with query data for api endpoint
    const updateData = []
    payload.data.annotations.forEach((entry) => {
      updateData.push({
        ColumnIndex: entry.columnIndex,
        ColumnName: entry.columnName,
        ScaleType: getDBTextFromScaleType(entry.scaleType),
        IsTarget: entry.isTarget,
        IsIgnore: entry.isIgnore,
      })
    })

    // make API call
    const response = await fetch(`${APIRoute()}/data_annotations/${payload.id}`, {
      method: 'PATCH', body: JSON.stringify(updateData),
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }
  },
  // load a sample rows for a dataset
  async loadDatasetPeek(context, payload) {
    
    // make API call
    const response = await fetch(`${APIRoute()}/data_annotations/${payload.id}/peek?rows=${payload.rows}`);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // commit response data to store
    context.commit('setDatasetPeek', responseData);
  },
};
