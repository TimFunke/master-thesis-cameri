// -----------------------------------------------------------------------------------------------------------
//   Definition of getters for data annotations
// -----------------------------------------------------------------------------------------------------------

export default {
  dataAnnotationOverview(state, _) {
    return [...state.dataAnnotationOverview];
  },
  hasDataAnnotations(_, getters) {
    return getters.dataAnnotationOverview && getters.dataAnnotationOverview.length > 0;
  },
  dataAnnotations(state, _) {
    return state.dataAnnotations;
  },
  datasetPeek(state, _) {
    return state.datasetPeek;
  }
};