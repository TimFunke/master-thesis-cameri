// -----------------------------------------------------------------------------------------------------------
//   Definition of store mutations for knowledge entries
// -----------------------------------------------------------------------------------------------------------

export default {
  setKnowledgeData(state, payload) {
    state.knowledgeData = payload;
  },
  setKnowledgeEntry(state, payload) {
    state.knowledgeEntry = payload;
  },
  setFetchTimestamp(state) {
    state.lastFetch = new Date().getTime();
  }
};