// -----------------------------------------------------------------------------------------------------------
//   Definition of getters for knowledge entries
// -----------------------------------------------------------------------------------------------------------

export default {
  knowledgeEntry(state, _) {
    return state.knowledgeEntry;
  },
  knowledgeData(state, _) {
    return [...state.knowledgeData];
  }
};