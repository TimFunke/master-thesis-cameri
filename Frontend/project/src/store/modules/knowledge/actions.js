// -----------------------------------------------------------------------------------------------------------
//   Definition of actions for knowledge entries
// -----------------------------------------------------------------------------------------------------------

import { APIRoute } from '../../../config.js';

export default {
  // load data for a knowledge entry by id
  async loadKnowledgeEntry(context, payload) {
    
    // make API call
    const response = await fetch(`${APIRoute()}/knowledge/${payload.id}`);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // create result object
    var knowledgeEntry = {
      name: responseData.KnowledgeName,
      group: responseData.KnowledgeGroup,
      expression: responseData.Expression,
    }

    // commit result object to store
    context.commit('setKnowledgeEntry', knowledgeEntry);
  },
  // load list of all exisiting knowledge entries
  async loadKnowledgeData(context, payload) {
    
    // make API call
    const response = await fetch(`${APIRoute()}/knowledge/`);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }

    // create result object
    var knowledgeData = []
    for (const responseEntry of responseData){
      var groupName = responseEntry.KnowledgeGroup
      var groupEntry = {
        id: responseEntry.KnowledgeID,
        name: responseEntry.KnowledgeName,
      }
      var group = knowledgeData.find(knowledgeGroup => knowledgeGroup.name === groupName)
      if(!group){
        group = {"name": groupName, "entries": []}
        knowledgeData.push(group)
      }
      group.entries.push(groupEntry)
    }

    // commit result object to store
    context.commit('setKnowledgeData', knowledgeData);
  },
  // create a new knowledge entry
  async createKnowledge(_, payload) {

    // create object with knowledge data for api endpoint
    const createData = {
      Name: payload.data.name,
      DatasetID: payload.data.datasetID,
      Knowledge: payload.data.selectedKnowledge
    }

    // make API call
    const response = await fetch(`${APIRoute()}/knowledge/`, {
      method: 'POST', body: JSON.stringify(createData),
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }
  },
  // edit a existing knowledge entry
  async editKnowledge(_, payload) {

    // create object with query data for api endpoint
    const updateData = {
      Name: payload.name,
    }

    // make API call
    const response = await fetch(`${APIRoute()}/knowledge/${payload.id}`, {
      method: 'PATCH', body: JSON.stringify(updateData),
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }
  },
  // delete a knowledge entry
  async deleteQuery(_, payload) {
    
    // make API call
    const response = await fetch(`${APIRoute()}/knowledge/${payload.id}`, {method: 'DELETE'});
    const responseData = await response.json();
    if (!response.ok || !('message' in responseData) || responseData.message != 'success') {
      const error = new Error(JSON.stringify(responseData) || 'Failed to send request.');
      throw error;
    }
  }
};
