const E_SCALE_TYPE = {
    UNSET : 0,
    NUMERIC : 1,
    ORDINAL : 2,
    CATEGORICAL : 3,
}

const scale_type_to_text = {
    0: '- unset -',
    1: 'Numeric',
    2: 'Ordinal',
    3: 'Categorical',
}

const text_to_scale_type = {
  NUMERIC: 1,
  ORDINAL: 2,
  CATEGORICAL: 3,
};

function getScaleTypeText(status){
    return scale_type_to_text[status];
}

function getScaleTypeFromText(scale_type){
  if (!(scale_type)){
    return E_SCALE_TYPE.UNSET
  }
  if (!(scale_type in text_to_scale_type)) {
    return -1;
  }
  return text_to_scale_type[scale_type];
}

function getDBTextFromScaleType(scale_type){
  if (scale_type == E_SCALE_TYPE.UNSET){
    return null
  }
  for (const [key, value] of Object.entries(text_to_scale_type)) {
    if(scale_type == value){
      return key
    }
  }
}

export { E_SCALE_TYPE, getScaleTypeText, getScaleTypeFromText, getDBTextFromScaleType };