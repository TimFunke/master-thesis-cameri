const E_QUERY_STATUS = {
  CREATED: 0,
  RUNNING_FE: 1,
  RUNNING_INF: 2,
  FINISHED: 3,
};

const status_to_text = {
  0: 'created',
  1: 'running - feature extraction',
  2: 'running - inference',
  3: 'finished',
};

const text_to_status = {
  CREATED: 0,
  RUNNING_FEATURE_EXTRACTION: 1,
  RUNNING_INFERENCE: 2,
  FINISHED: 3,
};

const status_to_img_src = {
  0: '/cameri/api/resources/vector_graphics/status_created.svg',
  1: '/cameri/api/resources/vector_graphics/status_running.svg',
  2: '/cameri/api/resources/vector_graphics/status_running.svg',
  3: '/cameri/api/resources/vector_graphics/status_finished.svg',
};

function getStatusText(status) {
  return status_to_text[status];
}

function getStatusImgSrc(status) {
  return status_to_img_src[status];
}

function getQueryStatusFromText(status_text) {
  if (!(status_text in text_to_status)) {
    return -1;
  }
  return text_to_status[status_text];
}

export { E_QUERY_STATUS, getStatusText, getStatusImgSrc, getQueryStatusFromText };
