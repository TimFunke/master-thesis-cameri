import { E_SCALE_TYPE } from './DataAnnotationScaleType.js';

const E_DA_STATUS = {
  EMPTY: 0,
  PARTIAL: 1,
  COMPLETED: 2,
};

const status_to_text = {
  0: 'Empty',
  1: 'Partial',
  2: 'Completed',
};

const text_to_status = {
  EMPTY: 0,
  PARTIAL: 1,
  COMPLETED: 2,
};

function getStatusText(status) {
  return status_to_text[status];
}

function getDataAnnotationStatusFromText(status_text) {
  if (!(status_text in text_to_status)) {
    return -1;
  }
  return text_to_status[status_text];
}

function getStatusFromAnnotations(annotations) {
  var amount_unset = 0;
  for (const annotation in annotations) {
    if (annotation.scaleType == E_SCALE_TYPE.UNSET) {
      amount_unset += 1;
    }
  }
  if (amount_unset == annotations.length) {
    return E_DA_STATUS.EMPTY;
  }
  if (amount_unset == 0) {
    return E_DA_STATUS.COMPLETED;
  }
  return E_DA_STATUS.PARTIAL;
}

export { E_DA_STATUS, getStatusText, getDataAnnotationStatusFromText, getStatusFromAnnotations };
