const E_RECOMMENDATION_TYPE = {
  PRACTICABLE: 0,
  PREPROCESSING_NEEDED: 1,
  IMPRACTICABLE: 2,
};

const enum_to_text = {
  0: 'Practicable Methods out of the box',
  1: 'Practicable Methods after some Preprocessing',
  2: 'Impracticable Methods',
};

const rec_type_to_enum = {
  PRACTICABLE: 0,
  PREPROCESSING_NEEDED: 1,
  IMPRACTICABLE: 2,
};

function getRecommendationTypeText(enum_value) {
  return enum_to_text[enum_value];
}

function getRecommendationTypeFromText(rec_type_text) {
  if (!(rec_type_text in rec_type_to_enum)) {
    return -1;
  }
  return rec_type_to_enum[rec_type_text];
}

export { E_RECOMMENDATION_TYPE, getRecommendationTypeText, getRecommendationTypeFromText };
