// -----------------------------------------------------------------------------------------------------------
//   Definition of the possible frontend routes and wiring the components to them
// -----------------------------------------------------------------------------------------------------------

import { createRouter, createWebHistory } from 'vue-router';

import QueryOverview from './pages/query/QueryOverview.vue';
import QueryEditor from './pages/query/QueryEditor.vue';
import QueryResults from './pages/query/QueryResults.vue';
import DataAnnotationOverview from './pages/data_annotation/DataAnnotationOverview.vue';
import DataAnnotationEditor from './pages/data_annotation/DataAnnotationEditor.vue';
import KnowledgeCenter from './pages/knowledge/KnowledgeCenter.vue';
import KnowledgeEditor from './pages/knowledge/KnowledgeEditor.vue';
import NotFound from './pages/NotFound.vue';

const router = createRouter({
  // prefix all routes with "/cameri/" to match the CameriController of Symfony
  history: createWebHistory('/cameri/'),
  // define routes
  routes: [
    { path: '/', redirect: '/query' },
    { path: '/query', component: QueryOverview },
    { path: '/query/:id', component: QueryEditor, props: true },
    { path: '/query/:id/results', component: QueryResults, props: true },
    { path: '/data-annotation', component: DataAnnotationOverview },
    { path: '/data-annotation/:id', component: DataAnnotationEditor, props: true },
    { path: '/knowledge', component: KnowledgeCenter },
    { path: '/knowledge/:id', component: KnowledgeEditor, props: true },
    { path: '/:notFound(.*)', component: NotFound },
  ],
});

export default router;
