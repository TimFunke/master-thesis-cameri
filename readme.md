# Overview

The code is separated into Backend and Frontend. The Backend contains the code for the CAMeRI API which encapsulates all features of the application and the Celery Queue which handles the execution of the recommender system. The Frontend contains the code for the graphical user interface used for interacting with the application inside of the KM-EP. Since CAMeRI can run independently of the KM-EP you dont need an active KM-EP instance to use CAMeRI.

# Initial Setup

## Requirements

To use all features of the application you need a relational database (MariaDB recommended) and an instance of redis running. You can initially setup the relational database with the SQL script at `Backend/db_setup.sql`. Furthermore you can use the SQL script at `Backend/db_example_data.sql` to clear all tables and refill them with example data for testing the API and Frontend.

To run the Frontend of CAMeRI you need to have node.js installed. Nextup you need to install some node-packages used by the Frontend. To do so, please open a command prompt, navigate to `Frontend/project/` and run `npm install`.

To run the Backend of CAMeRI you need to have python installed. Nextup you need to install some python-packages used by the Backend. To do so, please open a command prompt and run the following install commands:
- `pip install fastapi`
- `pip install pydantic`
- `pip install sqlalchemy`
- `pip install -U celery[redis]`
- `pip install mysql-connector-python`
- `pip install mysqlclient`
- `pip install pandas`
- `pip install uvicorn`

Lastly to run the recommender system you need to have SWI-Prolog installed.

## Configuration

You can find 2 configuration files at `Backend/CAMeRI_API/config.cfg` and `Backend/Celery_Queue/config.cfg` for the configuration of the CAMeRI API and the Celery Queue respectively. There you need to change the configuration for database connection, redis connection and SWI-Prolog executable path to fit your local settings. The remaining parameters can be left unchanged.

# Usage

To run the application you need to run the 3 components CAMeRI API, Celery Queue and Frontend by executing the 3 scripts `Backend/run_api.cmd`, `Backend/run_celery.cmd` and `Frontend/run_serve.cmd`. If you got all 3 scripts running you can open your browser (Chrome recommended) with the url `http://localhost:8080`.